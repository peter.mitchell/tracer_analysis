import numpy as np
import h5py
import sys
sys.path.append("../.")
from utilities_statistics import *
from utilities_plotting import *
from matplotlib.colors import LogNorm

Zoom = "Zoom-7-5092"
Run = "RhdRun-tracer_radfix"
timestep_final = 155
OutDir = "/cral2/mitchell/PostProcess/"

redshift_lo = 2.99
redshift_hi = 4.0
print "using 3.08 zlo until rascas fixed for final snapshots"
redshift_lo = 3.08

OutDir_stack = OutDir + "Stack_Local/"

filename_local = "local_stack_"+ Zoom + "_" + Run + "_tsf" + str(timestep_final) +".hdf5"
File_local = h5py.File(OutDir_stack+filename_local,"r")
stack_group_path = "stack_z="+str(redshift_lo).replace(".","p")+"_"+str(redshift_hi).replace(".","p")

stack_group = File_local[stack_group_path]

hist_names = ["vrad", "nh", "temp", "xhi"]
weight_names = ["mass", "volume", "neutral_mass", "Lya_intrin", "Lya_escape"]
#weight_names = ["mass", "volume", "neutral_mass", "Lya_intrin", "Lya_escape","Lya_esc_scat", "Lya_esc_noscat"]
#weight_names = ["mass", "volume", "neutral_mass"]


names = []
H = {}
xedges = {}
yedges = {}
for hist_name in hist_names:
    for weight_name in weight_names:
        name = hist_name+"_"+weight_name
        names.append(name)
        
        H[name] = stack_group["H"+name][:]
        xedges[name] = stack_group["xedges"+name][:]
        yedges[name] = stack_group["yedges"+name][:]
        
File_local.close()

py.rcParams.update(latexParams)
py.rcParams.update({'figure.subplot.wspace':0.12,'figure.subplot.hspace':0.,
                    'figure.figsize':[6.64,4.98],
                    'figure.subplot.left':0.09,
                    'figure.subplot.bottom':0.1,
                    'figure.subplot.right':0.94,
                    'figure.subplot.top':0.95,
                    'legend.fontsize':7,
                    'font.size':12,
                    'axes.labelsize':12,
                    'axes.titlesize':8})

py.figure()
nrow = 2; ncol = 2

panel_names = ["xhi", "vrad", "nh", "temp"]
ylo_list = [-0.03, -150., -5.0, 2.0]
yhi_list = [1.03, 150., 3.9, 6.9]

ylabels = [r"$x_{\mathrm{HI}}$", r"$v_{\mathrm{rad}} \, / \mathrm{km s^{-1}}$"]
ylabels += [r"$\log_{10}(n_{\mathrm{H}} \, / \mathrm{cm^{-3}})$"]
ylabels += [r"$\log_{10}(T \, / \mathrm{K})$"]

subplots = panelplot(nrow,ncol)
for n,ax in enumerate(subplots):
    py.axes(ax)

    panel_name = panel_names[n]
    if panel_name == "vrad":
        py.axhline(0.0,c="k",alpha=0.3)
    
    for i_name in range(len(names)):
        name = names[i_name]
        if panel_name not in name:
            continue

        print name
        if "neutral_mass" in name:
            c="b"; offset = -0.05
            label = "Neutral hydrogen"
        elif "mass" in name:
            c="k";offset = -0.025
            label = "Mass"
        elif "volume" in name:
            c = "r"; offset = 0.0
            label = "Volume"
        elif "Lya_intrin" in name:
            c = "g"; offset = 0.025
            label = r"Ly$\alpha$ (emitted)"
        elif "Lya_esc_scat" in name:
            c = "c"; offset = 0.0375
            label = r"Ly$\alpha$ (scattering)"
        elif "Lya_esc_noscat" in name:
            c = "y"; offset = 0.0525
            label = r"Ly$\alpha$ (no scattering)"
        elif "Lya_escape" in name:
            c = "m"; offset = 0.05
            label = r"Ly$\alpha$ (last scattering)"
        H_i = H[name]
        xedges_i = xedges[name]
        yedges_i = yedges[name]

        xedges_mid = 0.5*(xedges_i[1:] + xedges_i[0:-1])
        yedges_mid = 0.5*(yedges_i[1:] + yedges_i[0:-1])

        nxbin = len(xedges_i) -1
        nybin = len(yedges_i) -1

        x_tiled = np.repeat(xedges_mid,nybin)
        y_tiled = np.tile(yedges_mid,nxbin)

        xbin = np.array([0.0, 0.2, 0.4, 0.6, 0.8, 1.0])

        # Compute and plot percentiles
        med, lo_perc, hi_perc, mid_bin =  Calculate_Percentiles(xbin,x_tiled,y_tiled,weights=np.ravel(H_i),min_count = 1,lohi=(0.16,0.84))
        py.errorbar(mid_bin+offset,med,yerr=[abs(med-lo_perc),abs(med-hi_perc)],fmt=c+"+",linewidth=1,mew=1,label=label)

        #py.plot(mid_bin, med, c=c, linewidth=1.2)
        #py.plot(mid_bin, lo_perc, c=c, linewidth=0.7, alpha=0.7)
        #py.plot(mid_bin, hi_perc, c=c, linewidth=0.7, alpha=0.7)

    py.ylabel(ylabels[n])

    ylo = ylo_list[n]; yhi = yhi_list[n]
    py.ylim((ylo,yhi))

    if n == 2:
        legend = py.legend(loc="upper right",frameon=False,numpoints=1)
    #for text in legend.get_texts():
    #    text.set_color(cbar_color)

    py.xlim((0.0,1.0))

    if n == 2 or n == 3:
        py.xlabel(r"$r \, / R_{\mathrm{vir}}$")
    
py.savefig("../Figures/phase_radial_profiles.pdf")
py.show()
