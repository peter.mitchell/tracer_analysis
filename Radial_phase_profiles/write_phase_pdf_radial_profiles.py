import numpy as np
import h5py
import sys
sys.path.append("../.")
import utilities_ramses as ur
import dm_utils as dm_utils
import cutils as cut
import neighbour_utils as nu
import flux_utils
from utilities_statistics import *
import os
sys.path.append("../Rascas_analysis/.")
import jphot as jp

Zoom = "Zoom-7-5092"
Run = "RhdRun-tracer_radfix"
timestep_final = 155
timesteps_show = np.arange(5,timestep_final+1)
RunDir = "/cral2/mitchell/"+Zoom+"/"+Run
InDir = "/cral2/mitchell/PostProcess/"
OutDir = "/cral2/mitchell/PostProcess/"

# Choose which rascas outputs to read
#rascas_analysis = "6arcsec"
rascas_analysis = "11arcsec"

redshift_lo = 2.99
redshift_hi = 4.0

print "Setting redshift_lo to 3.08 until rascas fixed on final outputs"
redshift_lo = 3.08

include_sub = False # Include gas within the tidal radius of satellites


HaloDir = RunDir + "/Halos/"
TreeDir = RunDir + "/Trees/"



############## Zoom file IO #########################
# Read in relevant tree information from the hdf5 file
filename_zoom = Zoom + "_" + Run + "_tsf" + str(timestep_final) +".hdf5"

File = h5py.File(OutDir+filename_zoom,"r")

# Open the subvolume file (containing all cell data)
subvol_filename = "subvol_"+filename_zoom
subvol_File = h5py.File(OutDir+subvol_filename,"r")

# Check the file with the tree information already exists
if not Zoom in File:
    print "Error: Tree information for ", Zoom, " in ", filename_zoom, " cannot be read"
    exit()

zoom_group = File[Zoom]

init_stack = True

# Loop over timesteps
for i_timestep, timestep in enumerate(timesteps_show):
    timestep_tree = timestep -1 # Stupid mismatch between merger tree files and everything else 

    # Get conversions for different units
    r2kpc, redshift, little_h, m2Msun,rho2msunpkpc3,v2kms = ur.Set_Units(RunDir, timestep)
    boxlen_pkpc = dm_utils.jeje_utils.get_param_real(RunDir,timestep,'boxlen') * r2kpc # Box size in proper kpc, using the correct box size

    if redshift < redshift_lo or redshift > redshift_hi:
        continue
    
    print "reading cell/photon data at timestep = ", timestep, "of", timestep_final, ", redshift is", redshift

    ######## Read Rascas photon data ###########
    snap_str = str(timestep).rjust(5,'0')

    if rascas_analysis == "Rvir":
        rascas_str = "_Rvir"
    elif rascas_analysis == "6arcsec":
        rascas_str = "_6arcsec"
    elif rascas_analysis == "11arcsec":
        rascas_str = "_11arcsec"
    else:
        print "rascas_analysis", rascas_analysis, "not recognised"
        quit()

    # Photons tracing collisional excitations
    ex_rc_path_col = RunDir +"/RASCAS/"+snap_str+"/ColLya_CS100"+rascas_str+"/"
    icFile_path_col = ex_rc_path_col + "001.IC" # For now I'm assuming there is only one rascas output per simulation output ("001")
    resFile_path_col = ex_rc_path_col + "001.res"

    # Photons tracing recombinations
    ex_rc_path_rec = RunDir +"/RASCAS/"+snap_str+"/RecLya_CS100"+rascas_str+"/"
    icFile_path_rec = ex_rc_path_rec + "001.IC" # For now I'm assuming there is only one rascas output per simulation output ("001")
    resFile_path_rec = ex_rc_path_rec + "001.res"

    coll_p = jp.photonlist(icFile_path_col, resFile_path_col)
    rec_p = jp.photonlist(icFile_path_rec, resFile_path_rec)

    # Get lya luminosity associated with a single rascas photon
    clight  = 2.99792458e10  # cm/s
    planck  = 6.626070040e-27  # [erg s] Planck's constant
    cmtoA   = 1.0e8 # cm to Angstrom
    lambda_0=1215.67e0                      # [A] Lya wavelength
    lambda_0_cm = lambda_0 / cmtoA          # cm
    nu_0 = clight / lambda_0_cm             #  Hz
    rec_L_p = rec_p.nRealPhotons / len(rec_p.x) * planck * nu_0 # erg s^-1 (I think)
    coll_L_p = coll_p.nRealPhotons / len(coll_p.x) * planck * nu_0
    
    # We only care about escaped photons
    esc = np.where(coll_p.status == 1)[0]
    coll_p = coll_p.extract_sample(esc)
    
    esc = np.where(rec_p.status == 1)[0]
    rec_p = rec_p.extract_sample(esc)

    px = np.append(rec_p.x,coll_p.x)
    py = np.append(rec_p.y,coll_p.y)
    pz = np.append(rec_p.z,coll_p.z)
    ptype = np.append(np.zeros(len(rec_p.x)),np.ones(len(coll_p.x))) # 0 = recombination, 1 = collisional excitation
    pL = np.append(np.zeros(len(rec_p.x))+rec_L_p,np.zeros(len(coll_p.x))+coll_L_p)
    pn = np.append(rec_p.nscat, coll_p.nscat) # Number of scattering events
    
    ########## Read cell data #########################
    ts_group = subvol_File["timestep_"+str(timestep)]
    cell_group = ts_group["Gas_leaf_cell_data"]
    rho_snap = cell_group["rho_c"][:] # Gas density
    pre_snap = cell_group["pre_c"][:] # Gas pressure
    cx = cell_group["x_c"][:] # Cell coordinates
    cy = cell_group["y_c"][:]
    cz = cell_group["z_c"][:]
    cvx = cell_group["vx_c"][:]
    cvy = cell_group["vy_c"][:]
    cvz = cell_group["vz_c"][:]
    clev = cell_group["lev"][:] # Cell refinement level
    c_component_index = cell_group["component_index"][:]
    xH1 = cell_group["xH1"][:]
    xHe1 = cell_group["xHe1"][:]
    xHe2 = cell_group["xHe2"][:]

    ######### Select cells/photons within R_vir ###############
    cxyz_min = min(cx.min(),cy.min(),cz.min())
    cxyz_max = max(cx.max(),cy.max(),cz.max())

    cx_rescale = (cx - cxyz_min) / (cxyz_max-cxyz_min)
    cy_rescale = (cy - cxyz_min) / (cxyz_max-cxyz_min)
    cz_rescale = (cz - cxyz_min) / (cxyz_max-cxyz_min)

    pxyz_min = min(px.min(),py.min(),pz.min())
    pxyz_max = max(px.max(),py.max(),pz.max())
    
    px_rescale = (px - pxyz_min) / (pxyz_max-pxyz_min)
    py_rescale = (py - pxyz_min) / (pxyz_max-pxyz_min)
    pz_rescale = (pz - pxyz_min) / (pxyz_max-pxyz_min)
    
    nchain_cells = 20
    nextincell_c,firstincell_c = nu.neighbour_utils.init_chain(cx_rescale,cy_rescale,cz_rescale,nchain_cells)
    nextincell_p,firstincell_p = nu.neighbour_utils.init_chain(px_rescale,py_rescale,pz_rescale,nchain_cells)

    # Factor to help convert into box units (note maxime's merger_tree reader has converted halo positions so be careful here!)
    r2kpc_ideal, redshift, little_h, m2Msun,junk,junk = ur.Set_Units(RunDir, timestep, "ideal")
    boxlen_cMpc = dm_utils.jeje_utils.get_param_real(RunDir,timestep,'boxlen') # Box size in box units
    boxlen_cMpc *= r2kpc_ideal
    boxlen_cMpc = boxlen_cMpc * (1+redshift) /1e3 # cMpc (comoving)
    boxlen_cMpc = boxlen_cMpc * little_h # h^-1 cMpc
    factor = 1./boxlen_cMpc

    # Get target halo properties
    for halo_group_str in zoom_group:
        # Only compute stuff for the main halo
        if "_main" in halo_group_str:
            halo_group_str_use= halo_group_str
            
    mp_group = zoom_group[halo_group_str_use+"/main_progenitors"]
    
    # Identify the main progenitor at this timestep
    ok_ts = np.array(mp_group["halo_ts"])==timestep_tree # haloes on this timestep
    
    xh = mp_group["x"][ok_ts] * factor + 0.5
    yh = mp_group["y"][ok_ts] * factor + 0.5
    zh = mp_group["z"][ok_ts] * factor + 0.5

    # Use r200 for central haloes, and rvir (for now) for satellite haloes
    if mp_group["host_halo_id"][ok_ts] == mp_group["halo_id"][ok_ts]:
        rvirh = mp_group["r200"][ok_ts] / r2kpc
    else:
        rvirh = mp_group["rvir"][ok_ts] * factor

    vxh = mp_group["vx"][ok_ts] / v2kms # Halo velocity in Code units
    vyh = mp_group["vy"][ok_ts] / v2kms
    vzh = mp_group["vz"][ok_ts] / v2kms

    # Select cells in the vicinity of the halo (using a chain algorithm)
    rmax = rvirh

    ###### Cells within virial radius #########
    # Rescale halo positions for efficient cell division in chain algorithm
    xh_rescale = (xh-cxyz_min)/(cxyz_max-cxyz_min)
    yh_rescale = (yh-cxyz_min)/(cxyz_max-cxyz_min)
    zh_rescale = (zh-cxyz_min)/(cxyz_max-cxyz_min)
    rmax_rescale = rmax/(cxyz_max-cxyz_min)

    ncell_in_rmax = nu.neighbour_utils.get_npart_in_sphere_with_chain(RunDir,timestep,cx_rescale,cy_rescale,cz_rescale,xh_rescale,yh_rescale,zh_rescale,rmax_rescale,firstincell_c,nextincell_c)
    if ncell_in_rmax > 0:
        select_c = nu.neighbour_utils.\
                   get_part_indicies_in_sphere_with_chain(\
                                                          RunDir,timestep,cx_rescale,cy_rescale,cz_rescale,\
                                                          xh_rescale,yh_rescale,zh_rescale,\
                                                          rmax_rescale,ncell_in_rmax,firstincell_c,nextincell_c)
    else:
        select_c = np.zeros_like(cx) < 0

    ###### Rascas photons within the virial radius #########
    xh_rescale = (xh-pxyz_min)/(pxyz_max-pxyz_min)
    yh_rescale = (yh-pxyz_min)/(pxyz_max-pxyz_min)
    zh_rescale = (zh-pxyz_min)/(pxyz_max-pxyz_min)
    rmax_rescale = rmax/(pxyz_max-pxyz_min)

    ncell_in_rmax = nu.neighbour_utils.get_npart_in_sphere_with_chain(RunDir,timestep,px_rescale,py_rescale,pz_rescale,xh_rescale,yh_rescale,zh_rescale,rmax_rescale,firstincell_p,nextincell_p)
    if ncell_in_rmax > 0:
        select_p = nu.neighbour_utils.get_part_indicies_in_sphere_with_chain(RunDir,timestep,px_rescale,py_rescale,pz_rescale,xh_rescale,yh_rescale,zh_rescale,rmax_rescale,ncell_in_rmax,firstincell_p,nextincell_p)
    else:
        select_p = np.zeros_like(px) < 0

    # Select cells
    cx_h = cx[select_c]
    cy_h = cy[select_c]
    cz_h = cz[select_c]
    rho_snap_h = rho_snap[select_c]
    pre_snap_h = pre_snap[select_c]
    cvx_h = cvx[select_c]
    cvy_h = cvy[select_c]
    cvz_h = cvz[select_c]
    clev_h = clev[select_c]
    c_comp_h = c_component_index[select_c]
    xH1_h = xH1[select_c]; xHe1_h = xHe1[select_c] ; xHe2_h = xHe2[select_c]

    # Select photons
    px_h = px[select_p]; py_h = py[select_p]; pz_h = pz[select_p]
    ptype_h = ptype[select_p]; pL_h = pL[select_p]
    pn_h = pn[select_p]
    
    ######## Compute leaf cell properties ############
    print "Computing cell/particle properties"

    csize_h = boxlen_pkpc / np.power(2,clev_h) # proper kpc
    cell_vol = np.power(csize_h,3) # kpc3
    cmass_h = rho_snap_h * rho2msunpkpc3 * cell_vol
    
    cell_vol *= (3.086e18*1e3)**3 # cm3
    
    # Compute other cell properties
    cx_h = cx_h - xh; cy_h = cy_h - yh; cz_h = cz_h -zh
    cvx_h = cvx_h - vxh; cvy_h = cvy_h - vyh; cvz_h = cvz_h - vzh
        
    cr_h = np.sqrt(np.square(cx_h) + np.square(cy_h) + np.square(cz_h))
    cv_radial_h = np.sum(np.array([cvx_h,cvy_h,cvz_h]) * np.array([cx_h,cy_h,cz_h]),axis=0) / cr_h
        
    cv_radial_h *= v2kms # km

    cut.jeje_utils.read_conversion_scales(RunDir,timestep)
    scale_temk = (cut.jeje_utils.dp_scale_l / cut.jeje_utils.dp_scale_t)**2 * cut.jeje_utils.mh / cut.jeje_utils.kb
    temp_mu_h = pre_snap_h/rho_snap_h * scale_temk

    # Compute neutral gas number density in atoms per cc and the temperature in Kelvin
    xhi_h = (1-xH1_h)# Hydrogen neutral fraction
    X_frac=0.76
    mu_h = 1./( X_frac*(1.+xH1_h) + 0.25*(1.-X_frac)*(1.+xHe1_h+2.*xHe2_h) )
    
    temp_h = temp_mu_h * mu_h
    nh_h = rho_snap_h * cut.jeje_utils.dp_scale_d / cut.jeje_utils.mh * cut.jeje_utils.xh

    # Compute intrinsic Lyman alpha luminosity
    Ta = np.copy(temp_h)
    Ta[Ta<100] = 100 # Put temperature floor
    prob_case_B = 0.686 - 0.106*np.log10(Ta/1e4) - 0.009*np.power(Ta/1e4,-0.44)
    nHII = xH1_h*nh_h # Ionized hydrogen density
    nHI = xhi_h * nh_h # Neutral hydrogen density
    X_frac = 0.76
    nHe  = 0.25*nh_h*(1.0-X_frac)/X_frac
    n_e = nHII+nHe*(xHe1_h+2.*xHe2_h)
    
    def get_AlphaB_HII(TK):
        lbda = 315614./TK
        return 2.753e-14 * np.power(lbda,1.5) / np.power(1.+np.power(lbda/2.74,0.407),2.242)

    def get_collExrate_HI(TK):
        kb = 1.38064852e-16
        return 2.41e-6/np.sqrt(TK) * np.power(TK/1.e4,0.22) * np.exp(-1.63e-11/(kb*TK))
    
    planck  = 6.626070040e-27  # [erg s] Planck's constant
    clight  = 2.99792458e10  # cm/s
    cmtoA   = 1.0e8 # cm to Angstrom
    lambda_0=1215.67e0
    lambda_0_cm = lambda_0 / cmtoA # cm
    nu_0 = clight / lambda_0_cm #  Hz
    e_lya = planck*nu_0 # 1.634e-11 # cgs

    RecLyaEmi  = prob_case_B * nHII * n_e * get_AlphaB_HII(temp_h) * e_lya
    CollLyaEmi = nHI * n_e * get_collExrate_HI(temp_h) * e_lya
    LyaEmi     = CollLyaEmi + RecLyaEmi

    LyaLum = LyaEmi * cell_vol # erg s^-1
    CollLyaLum = CollLyaEmi * cell_vol
    RecLyaLum = RecLyaEmi * cell_vol
    
    ########### Link rascas photons to cells, compute cell properties ################
    if len(px_h)>0:
        px_h -= xh; py_h -= yh; pz_h -= zh

    ind_c, ok_match = flux_utils.Match_Cell_Particle(csize_h, cx_h, cy_h, cz_h, px_h, py_h, pz_h)
    ind_c[ok_match==False] = -1

    # Escaping lya luminosity (erg s^-1) associated with each leaf cell
    cL_h, cL_rec_h = flux_utils.Sum_Common_ID([pL_h,pL_h*ptype_h], ind_c, np.arange(0,len(cx_h)) )

    # Same but only for photons that do not scatter
    ok = pn_h == 0
    cL_h_no_scat = flux_utils.Sum_Common_ID([pL_h[ok]], ind_c[ok], np.arange(0,len(cx_h)) )[0]
        
    ############# Select cells and compute distributions ###########

    # Satellite cut
    if include_sub:
        ok = np.zeros_like(cx_h) == 0
    else:
        ok = (c_comp_h==0) | (c_comp_h==3)
        
    cr_norm = cr_h[ok] / rvirh

    # Make a time-stacked set of histograms for this halo
    xlo = 0.0; xhi = 1.0
    xbins = np.linspace(xlo,xhi,20) # 20 xbins

    # Choose which quantities to make histograms for
    hist_names = ["vrad", "nh", "temp", "xhi"]
    hist_quants = [cv_radial_h, nh_h, temp_h, xhi_h]
    hist_scales = ["lin", "log", "log", "lin"]
    hist_spacing = [1, 0.07, 0.06, 0.01]
    hist_ymin = [ -200., -5.0, 1.0, 0.0]
    hist_ymax = [2000., 5.0, 8.0, 1.01]
    # Choose which weights to use
    weight_names = ["mass", "volume", "neutral_mass",  "Lya_intrin", "Lya_intrin_rec","Lya_escape","Lya_esc_scat",   "Lya_esc_noscat", "Lya_esc_rec"]
    weights =      [cmass_h, cell_vol, cmass_h * xhi_h, LyaLum,          RecLyaLum,   cL_h,        cL_h-cL_h_no_scat,cL_h_no_scat, cL_rec_h]

    if init_stack:
        H_stack = {}
        y_stack = {}
        x_stack = {}
        
    for hist_name, quant_in, scale, spacing, ymin, ymax in\
        zip(hist_names, hist_quants, hist_scales, hist_spacing, hist_ymin, hist_ymax):
        for weight_name, weight_in in zip(weight_names, weights):

            if scale == "lin":
                quant = quant_in[ok]
            elif scale == "log":
                quant = np.log10(quant_in[ok])
            else:
                print "WUT", scale
                quit()
                
            weight = weight_in[ok]
            
            ybins = np.arange(ymin, ymax, spacing)
            
            H, xedges, yedges = np.histogram2d( cr_norm, quant , weights=weight ,bins=(xbins,ybins))
            
            name = hist_name+"_"+weight_name
            if init_stack:
                H_stack[name] = np.zeros_like(H)
                x_stack[name] = xedges
                y_stack[name] = yedges
                
            H_stack[name] += H
            
    if init_stack:
        init_stack = False

File.close()
subvol_File.close()

print "Saving data to disk"

# Save histograms to disk for this halo
OutDir_stack = OutDir + "Stack_Local/"

# Write the output directory if necessary
try:
    os.makedirs(OutDir_stack)
except OSError:
    pass

filename_local = "local_stack_"+ Zoom + "_" + Run + "_tsf" + str(timestep_final) +".hdf5"
File_local = h5py.File(OutDir_stack+filename_local)

stack_group_path = "stack_z="+str(redshift_lo).replace(".","p")+"_"+str(redshift_hi).replace(".","p")

if stack_group_path in File_local:
    del File_local[stack_group_path]

stack_group = File_local.create_group(stack_group_path)

for name in H_stack:
    H = H_stack[name]
    xedges = x_stack[name]
    yedges = y_stack[name]

    # Write the x/y bins
    stack_group.create_dataset("xedges"+name, data= xedges)
    stack_group.create_dataset("yedges"+name, data=yedges)

    # Write the 2d histograms
    stack_group.create_dataset("H"+name, data=H)

File_local.close()
print "Made it :)"
