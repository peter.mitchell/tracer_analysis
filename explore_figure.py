import numpy as np
import h5py
import utilities_cosmology as uc
import dm_utils as dm

#np.random.seed(23231)
#np.random.seed(512312)
#np.random.seed(174283)
#np.random.seed(13426743)
np.random.seed(123612765)
i_part = 0

note that c_component_index (i.e. comp) got changed - so this script would needto be updated before itcan be used

# 619 was the cool example of outflow from ISM scales John was interested in

fig_name = "tracer_exploration_5092.pdf"

outpath = "/cral2/mitchell/PostProcess/"
File = h5py.File("saved_tracers_select_Zoom-7-5092.hdf5")

tid_select = File["tid"][:]
comp       = File["comp"][:]
cv         = File["cv"][:]
nh         = File["nh"][:]
temp       = File["temp"][:]
xhi        = File["xhi"][:]
radius = File["r"][:]
timestep = File["timestep"][:]
family = File["family"][:]
radius_p = File["r_physical"][:]
radius_norm = File["r"][:]
redshift = File["redshift"][:]

File.close()
n_select = len(tid_select)

# Get time from redshift
Zoom = "Zoom-7-5092"; Run = "RhdRun-tracer_norad_bug"; RunDir = "/cral2/mitchell/"+ Zoom + "/" + Run
dm.jeje_utils.read_conversion_scales(RunDir,timestep[0])
omega_0,lambda_0,little_h = dm.jeje_utils.read_cosmo_params(RunDir,timestep[0])
a = 1./(1.+redshift)
t, tlb = uc.t_Universe(a,omega_0,little_h)

# Get time-averaged radial velocity
dr = (radius_p[1:] - radius_p[0:-1]) * (3.09*10**16) # km
dt = (t[1:]-t[0:-1]) * 3.155*10**16 # s

t_av = 0.5 * (t[0:-1]+t[1:])
cv_av = dr/np.reshape(dt,(len(dt),1))

############ True selection ################
parts_show = np.random.randint(0,n_select,10)

############ Custom selections ###############
family_temp = np.copy(family)
family_temp[np.isnan(family_temp)] = 100

comp_temp = np.copy(comp)
comp_temp[np.isnan(comp_temp)] = 0

# Count consecutive number of outputs a particle is registered as a satellite
nsat_max = np.zeros(n_select)
nsat = np.zeros(n_select)
for n in range(len(comp_temp[:,0])):
    nsat[comp_temp[n]>0] +=1
    nsat[comp_temp[n]==0] = 0    
    nsat_max[nsat>nsat_max] = nsat[nsat>nsat_max]
nsat_final = nsat
    
cv_temp = np.copy(cv)
cv_temp[np.isnan(cv_temp)] = 0

rad_temp = np.copy(radius_norm)
rad_temp[np.isnan(rad_temp)] = 1e9

jim = np.max(comp_temp,axis=0)
tim = np.max(family_temp,axis=0)
rmin = np.min(rad_temp,axis=0)
vrmax = np.max(cv_temp,axis=0)

# Count particles that get ejected from the halo, and number of those that return
accreted = np.zeros(n_select)
ejected = np.zeros(n_select)
nej = np.zeros(n_select)
nret = np.zeros(n_select)-1
for n in range(len(comp_temp[:,0])):
    accreted[rad_temp[n]<0.9] = 1
    ejected = (accreted==1) & (rad_temp[n]>1)
    accreted[ejected] = 0

    nej[ejected] += 1
    if n > 0:
        nret[(rad_temp[n]<0.9) & (rad_temp[n-1]>0.9)] +=1
    else:
        nret[(rad_temp[n]<0.9)] += 1


    
nsat_thr = 2

# Identify stripped particles that have not been in the ISM (or back into satellite) after being stripped
nsat = np.zeros(n_select)
stripped = np.zeros(n_select)
stripped2 = np.zeros(n_select)
nism = np.zeros(n_select)
was_in_ism = np.zeros(n_select)

ninflow = np.zeros(n_select)
noutflow = np.zeros(n_select)
mono_inflow = np.zeros(n_select)

for n in range(len(comp_temp[:,0])):
    nsat[comp_temp[n]>0] +=1
    
    in_sat = nsat>2
    stripped[in_sat] = 0
    stripped2[in_sat] = 0
    
    stripped[in_sat & (comp_temp[n]==0)] += 1
    stripped2[in_sat & (comp_temp[n]==0)] += 1
    nsat[comp_temp[n]==0] = 0

    nism[rad_temp[n]<0.2] +=1
    nism[rad_temp[n]>0.2] = 0
    stripped[nism>1] = 0

    was_in_ism[nism==2] += 1

    ninflow[cv_temp[n] < 0] += 1
    ninflow[cv_temp[n]>0] = 0
    noutflow[cv_temp[n] > 0] += 1
    noutflow[cv_temp[n]<0] = 0

    mono_inflow[ninflow>2] = 1
    mono_inflow[noutflow>2] = 0
    
    test = 69
    #print comp[n,test], nsat[test], stripped[test], nism[test], was_in_ism[test], radius[n,test]


cgm_final = (rad_temp[-1]>0.2)&(rad_temp[-1]<1.0) & (tim==100)&(nsat_final<=nsat_thr)

a = (stripped>0) & cgm_final
b = cgm_final&(was_in_ism>0)
c = cgm_final&(mono_inflow==1)&(was_in_ism==0)&(stripped==0)
d = cgm_final & (a==False) & (b==False) & (c==False)

ok = np.arange(0,n_select) == False
ts_sel = np.zeros(len(ok)).astype("int")


for n in range(len(comp_temp[:,0])):
    ok2 = (rad_temp[n]>0.4) & (rad_temp[n]<0.5) & (cv_temp[n] > 0) & (xhi[n] > 0.3) & (comp_temp[n]==0)
    # 16 is an interesting example of orbiting trajectory - satellite inteference - and no evidence of random xhi fluctuations
    # 157762661
    # 17 is similar (good ex for sat influence), but with feedback influence also - and was clearly stripped upon entering halo
    # 32 is what I was looking for - an example of past-peri - ionized before reaching apocentre
    # 33 has good example of sig fluctations in ionized fraction
    # 35 is a good example of particle ejected at high-z that then falls back in later, before being fb pushed even later
    # 36 is a very good example of rapid fluctuation xhi fraction particle, as 33
    # 126017276
    # 38 is a good example of past-peri particle, similar to 32 (it is a little on the high-z side though)
    # 158547635
    # 51 is the feedback example I wanted - particle is ejected from ISM neutral but becomes ionized - then hits satellite and becomes neutral again
    # 334285010
    
    # Just do selection that particle has a neutral fraction above 0.5 when in rad_range
    ok2 = (rad_temp[n]>0.4) & (rad_temp[n]<0.5) & (xhi[n] > 0.3) & (comp_temp[n]==0)
    # For this selection
    # 1 shows a farily clean example of a monotonic inflow onto the CGM - can see neutral fraction increase as particle moves in
    # 261839563
    
    # So let's do 38, 51, 36, 16 from first group
    # & 1 from 2nd group
    
    ok = ok | ok2
    ts_sel[ok&(ts_sel==0)] = n

#parts_show = np.arange(0,n_select)[ok]
#np.random.shuffle(parts_show)
#print "n parts in selection is", len(parts_show)
#i_part = 1
#ishow = parts_show[i_part]
#print "tracer id is", tid_select[ishow]

#tid_choose = [261839563, 157762661, 126017276, 158547635, 334285010]
tid_choose = [126017276, 158547635, 334285010,261839563]
parts_show = []
label_list = ["1","2","3","4"]
#linestyle_list = ["-", "--", "-.", ":"]
linestyle_list = ["-", "-", "-", "-"] # The eternal struggle between color blind people (green/red), dodgy printers & what works best for me on the monitor...

for tid_i in tid_choose:
    parts_show.append( np.where(tid_select == tid_i)[0][0])

radius = radius[:,parts_show]
comp = comp[:,parts_show]
family = family[:,parts_show]
nh = nh[:,parts_show]
temp = temp[:,parts_show]
xhi = xhi[:,parts_show]
cv = cv[:,parts_show]
cv_av = cv_av[:,parts_show]

from utilities_plotting import *
py.rcParams.update(latexParams)
py.rcParams.update({'figure.subplot.wspace':0.0,'figure.subplot.hspace':0,
                    'figure.figsize':[6.64,4.98*1.2],
                    'figure.subplot.left':0.102,
                    'figure.subplot.bottom':0.09,
                    'figure.subplot.right':0.99,
                    'figure.subplot.top':0.98,
                    'legend.fontsize':6,
                    'font.size':12,
                    'axes.labelsize':12})

c_list = ["k","m","b","r","g","y","c","firebrick","darkgray","orange"]

xlo = 0.9
xmax = t.max()

nrow = 5; ncol = 1
py.figure()
subplots = panelplot(nrow,ncol)
for n,ax in enumerate(subplots):
    py.axes(ax)

    if n == 0:

        for i_part in range(len(tid_choose)):

            ok = family[:,i_part] < 101
            ok2 = ok & ((comp[:,i_part]==1) | (comp[:,i_part]==2))
        
            py.plot(t, radius[:,i_part], c=c_list[i_part], label = label_list[i_part], linestyle=linestyle_list[i_part])
            py.scatter(t[ok2], radius[:,i_part][ok2], c=c_list[i_part],edgecolors="none")

        py.legend(loc="upper left")
    
        py.xlabel("t")
        py.ylabel(r"$r \, / R_{\mathrm{vir}}$")
        py.xlim((xlo, xmax))
        py.ylim((0.0,1.0))

    if n == 1:
        for i_part in range(len(tid_choose)):

            ok = family[:,i_part] < 101
            ok2 = ok & ((comp[:,i_part]==1) | (comp[:,i_part]==2))
    
            py.plot(t[ok], np.log10(nh[:,i_part][ok]), c=c_list[i_part], linestyle=linestyle_list[i_part])
            py.scatter(t[ok2], np.log10(nh[:,i_part][ok2]), c=c_list[i_part],edgecolors="none")
            
        py.ylabel(r"$\log(n_{\mathrm{H}} \, / \mathrm{cm^{-3}})$")
        py.xlim((xlo, xmax))
        py.ylim((-2.9, 3.9))
        
    if n == 2:
        for i_part in range(len(tid_choose)):
            ok = family[:,i_part] < 101
            ok2 = ok & ((comp[:,i_part]==1) | (comp[:,i_part]==2))
            py.plot(t[ok], np.log10(temp[:,i_part][ok]), c=c_list[i_part], linestyle=linestyle_list[i_part])
            py.scatter(t[ok2], np.log10(temp[:,i_part][ok2]), c=c_list[i_part],edgecolors="none")
        py.ylabel(r"$\log(T \, / \mathrm{K})$")
        py.xlim((xlo, xmax))
        py.ylim((2.51,5.49))

    if n == 3:
        for i_part in range(len(tid_choose)):
            ok = family[:,i_part] < 101
            ok2 = ok & ((comp[:,i_part]==1) | (comp[:,i_part]==2))
            py.plot(t[ok], np.log10(xhi[:,i_part][ok]), c=c_list[i_part], linestyle=linestyle_list[i_part])
            py.scatter(t[ok2], np.log10(xhi[:,i_part][ok2]), c=c_list[i_part],edgecolors="none")
        py.ylabel(r"$\log(x_{\mathrm{HI}})$")
        py.ylim((-1.5,0.05))
        py.xlim((xlo, xmax))

    if n == 4:
        for i_part in range(len(tid_choose)):
            ok = family[:,i_part] < 101
            ok2 = ok & ((comp[:,i_part]==1) | (comp[:,i_part]==2))
            py.plot(t[ok], cv[:,i_part][ok], c=c_list[i_part], linestyle=linestyle_list[i_part])
            py.scatter(t[ok2], cv[:,i_part][ok2], c=c_list[i_part],edgecolors="none")
            #py.plot(t_av[ok[0:-1]], cv_av[:,i_part][ok[0:-1]], c=c_list[i_part],linestyle=':')
        py.axhline(0.0)

        py.ylim((-150, 198))
    
        py.ylabel(r"$v_{\mathrm{rad}} \, / \mathrm{kms^{-1}} $")
        py.xlabel(r"$t \, /\mathrm{Gyr}$")
        py.xlim((xlo, xmax))

py.savefig("Figures/ind_examples_neut_cgm.pdf")
py.show()

