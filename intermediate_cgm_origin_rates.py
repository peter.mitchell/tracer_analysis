import numpy as np
import h5py
import utilities_cosmology as uc
import dm_utils as dm
import utilities_ramses as ur

note that c_component_index (i.e. comp) got changed - so this script would need to be updated before it can be used

OutDir = "/cral2/mitchell/PostProcess/"

Zoom = "Zoom-7-5092"
Run = "RhdRun-tracer_norad_bug"
File = h5py.File(OutDir+"save_5092_backup.hdf5")

#Zoom = "Zoom-7-5092"
#Run = "RhdRun-tracer_radfix"
#File = h5py.File(OutDir+"saved_tracers_select_"+Zoom+"_"+Run+".hdf5")

RunDir = "/cral2/mitchell/"+ Zoom + "/" + Run

# Choose radial bins for computing exchange terms
delta_rprime = 0.4
#r_bins = np.arange(0.0, 1.1, delta_rprime)
r_bins = np.array([0.2,0.6,1.0])

########## Decide tracer exchanges to record (n -> n+1) ##############
# Tracers at timestep n
weight_n = ["neutral", "neutral", "ion",     "ion",   "ion",      "ion"]
T_lo_n =   [0.0,        0.0,      0.0,       0.0,      10.**4.5,  10.**4.5]
T_hi_n =   [1e20,       1e20,     10.**4.5,  10.**4.5, 1e20,      1e20]
xhi_lo_n = [0.0,        0.0,      0.0,       0.0,      0.0,       0.0]
xhi_hi_n = [1.0,        1.0,      1.0,       1.0,      1.0,       1.0]
part_n =   [1.0,        0.0,      1.0,       0.0,      1.0,       0.0]
r_lo_n =   [0.0,        0.0,      0.0,       0.0,      0.0,       0.0]
r_hi_n =   [1.0,        1.0,      1.0,       1.0,      1.0,       1.0]
select_type_list = ["mass","mass","mass",   "mass",   "mass",    "mass"]
name_n =   ["neut_out","neut_in","wion_out","wion_in","hion_out","hion_in"]

# Tracer partitions
weight_n1 = ["neutral", "all",        "ion",     "ion",     "neutral", "all",       "ion",     "ion"]
T_lo_n1 =   [0.0,        0.0,          0.0,       10.**4.5,  0.0,       0.0,         0.0,       10.**4.5]
T_hi_n1 =   [1e20,       1e20,         10.**4.5,  1e20,      1e20,      1e20,        10.**4.5,  1e20]
xhi_lo_n1 = [0.0,        0.0,          0.0,       0.0,       0.0,       0.0,         0.0,       0.0]
xhi_hi_n1 = [1.0,        1.0,          1.0,       1.0,       1.0,       1.0,         1.0,       1.0]
part_n1 =    [1.0,        1.0,          1.0,       1.0,       0.0,       0.0,         0.0,       0.0]
r_lo_n1 =   [0.0,        0.0,          0.0,       0.0,       0.0,       0.0,         0.0,       0.0]
r_hi_n1 =   [1.0,        1.0,          1.0,       1.0,       1.0,       1.0,         1.0,       1.0]
name_n1 =   ["neut_out","tot_outflow","wion_out","hion_out","neut_in", "tot_inflow","wion_in", "hion_in"]

tid_select = File["tid"][:]
comp_in       = File["comp"][:]
cv_in         = File["cv"][:]
nh_in         = File["nh"][:]
temp_in       = File["temp"][:]
xhi_in        = File["xhi"][:]
radius_in = File["r"][:]
timestep_in = File["timestep"][:]
family_in = File["family"][:]
radius_p_in = File["r_physical"][:]
redshift_in = File["redshift"][:]
ek_in = File["ek"][:]
egrav_in = File["egrav"][:]
eth_in = File["eth"][:]
LyaLum_in = File["LyaLum"][:]


File.close()
n_select = len(tid_select)

# Get time from redshift
dm.jeje_utils.read_conversion_scales(RunDir,timestep_in[0])
omega_0,lambda_0,little_h = dm.jeje_utils.read_cosmo_params(RunDir,timestep_in[0])
a_in = 1./(1.+redshift_in)
t_in, tlb_in = uc.t_Universe(a_in,omega_0,little_h)

n_type = 3
select_types = ["m_tot","m_neut","LyaLum"]

skip_to = 5
# "Gravity only" class
f_1st_infall_evo = np.zeros((n_type, len(t_in)-skip_to))
f_past_peri_evo = np.zeros((n_type, len(t_in)-skip_to))
f_past_apo_evo = np.zeros((n_type, len(t_in)-skip_to))

# Gravity only, but entered within a satellite
f_strip_1st_infall_evo = np.zeros((n_type, len(t_in)-skip_to))
f_strip_past_peri_evo = np.zeros((n_type, len(t_in)-skip_to))
f_strip_past_apo_evo = np.zeros((n_type, len(t_in)-skip_to))

# "feedback influenced" class
f_fb_1st_outflow_evo = np.zeros((n_type, len(t_in)-skip_to))
f_fb_past_apo_evo = np.zeros((n_type, len(t_in)-skip_to))
f_fb_past_peri_evo = np.zeros((n_type, len(t_in)-skip_to))


# Loop over final CGM redshifts

for i_ts in range(len(t_in)-skip_to):

    i_final = i_ts +skip_to
    
    #if i_final < 94:
    #    continue
    #else:
    #    print "hack skip"
        
    #if i_final < 5:
    #    continue
    
    comp = np.copy(comp_in[:i_final+1])
    cv = np.copy(cv_in[:i_final+1])
    nh = np.copy(nh_in[:i_final+1])
    temp = np.copy(temp_in[:i_final+1])
    xhi = np.copy(xhi_in[:i_final+1])
    radius = np.copy(radius_in[:i_final+1])
    timestep = np.copy(timestep_in[:i_final+1])
    family = np.copy(family_in[:i_final+1])
    radius_p = np.copy(radius_p_in[:i_final+1])
    ek = np.copy(ek_in[:i_final+1])
    eth = np.copy(eth_in[:i_final+1])
    egrav = np.copy(egrav_in[:i_final+1])
    LyaLum = np.copy(LyaLum_in[:i_final+1])
    
    t = t_in[:i_final+1]

    n_part = len(radius[0])
    n_ts = len(t)
    
    ########### First task is to identify particles that were accreted into the virial radius for the first time inside a satellite
    first_infall_ts = np.zeros(n_part) + np.nan

    for n in range(n_ts):
        ok = np.isnan(first_infall_ts) & (np.isnan(radius[n]) == False)
        first_infall_ts[ok] = n

    # Drop out particles that never enter the virial radius for this final timestep
    ok = (np.isnan(first_infall_ts) == False)
    
    comp = comp[:,ok]; cv = cv[:,ok]; nh = nh[:,ok]; temp = temp[:,ok]; xhi = xhi[:,ok];
    radius = radius[:,ok]; family = family[:,ok]; radius_p = radius_p[:,ok]
    ek = ek[:,ok]; eth = eth[:,ok]; egrav = egrav[:,ok]
    first_infall_ts = first_infall_ts[ok]; LyaLum = LyaLum[:,ok]
    
    n_part = len(comp[0])
    
    ############ Now identify particles that were in a satellite when they entered the virial radius (base this off first 3 timesteps inside halo, where possible)
    # Note in the future - I should definitely track particles outside virial radius! (that would require knowing if they were inside other halos that are non-satellites however)
    enter_in_sat_score = np.zeros(n_part)

    for n in range(n_ts):
        ok = (comp[n] > 0) & (first_infall_ts == n)
        enter_in_sat_score[ok] += 1

        ok = (comp[n] >0) & (first_infall_ts == n-1)
        enter_in_sat_score[ok] += 1

        ok = (comp[n] > 0) & (first_infall_ts == n-2)
        enter_in_sat_score[ok] += 1

    # Handle particles where we don't have enough outputs to be sure
    ok = (first_infall_ts == n_ts-1) & (comp[n_ts-1] > 0)
    enter_in_sat_score[ok] += 2

    ok = (first_infall_ts == n_ts-2) & (comp[n_ts-2] > 0)
    enter_in_sat_score[ok] += 1

    enter_in_sat = enter_in_sat_score >= 3

    ########### Now identify particles that are in a satellite at the final steps #########
    end_in_sat_score = np.zeros(n_part)
    for n in range(3):
        n1 = n_ts - 3 + n

        ok = comp[n1] > 0
        end_in_sat_score[ok] += 1
        
    # Handle particles where we don't have enough outputs to be sure 
    ok = (first_infall_ts == n_ts-1) & (comp[n_ts-1] > 0)
    end_in_sat_score[ok] += 2
 
    ok = (first_infall_ts == n_ts-2) & (comp[n_ts-2] > 0)
    end_in_sat_score[ok] += 1
 
    end_in_sat = end_in_sat_score >= 3

    '''# Remove those particles
    ok = end_in_sat == False
    comp = comp[:,ok]; cv = cv[:,ok]; nh = nh[:,ok]; temp = temp[:,ok]; xhi = xhi[:,ok];
    radius = radius[:,ok]; family = family[:,ok]; radius_p = radius_p[:,ok]
    ek = ek[:,ok]; eth = eth[:,ok]; egrav = egrav[:,ok]; LyaLum=LyaLum[:,ok]
    first_infall_ts = first_infall_ts[ok]
    n_part = len(comp[0]); enter_in_sat = enter_in_sat[ok]'''
    # We don't want the above block for the rate measurements, so it's been added to the fractions part separately


    ########### identify cases where feedback is playing a role    
    fb_flag = np.zeros(n_part) # Zero means not influenced by feedback, 1) means first fb-driven outflow event, 2) means past-apocentre infall, 3) means later past-peri outflow  
    ts_fb_flag = np.zeros(n_part)+np.nan
    vhi_flag = np.zeros(n_part)+np.nan
    vlo_flag = np.zeros(n_part)+np.nan
    rad_hi_flag = np.zeros(n_part)+np.nan
    rad_lo_flag = np.zeros(n_part)+np.nan

    temp2 = np.zeros((n_ts,n_part))+np.nan
    temp3 = np.zeros((n_ts,n_part))+np.nan

    fb_flag_evo = np.zeros((n_ts, n_part))

    cv_temp = np.copy(cv)
    cv_temp[family>100] = 0.0
    temp_temp = np.copy(temp)
    temp_temp[family>100] = 0.0
    ek[family>100] = 0.0
    eth[family>100] = 0.0
    
    for n in range(n_ts):
        if n == 0:
            continue

        
        # The energy threshold at the end here could be tweaked with
        ok = (cv_temp[n] > 50) & (abs(cv_temp[n]) > abs(cv_temp[n-1]) + 30) & (fb_flag==0) & ( (ek+eth)[n] > (ek+eth)[n-1]*1.25)
        
        # Check for rare cases where a satellite can come along and push a particle out of the ISM
        # Flag this by discarding situations where particle just joined a satellite
        ok = ok & ( ( (comp[n]>0) &(comp[n-1]==0) ) ==False)
        # Also flag if in satellite for 3 consecutive steps into the future
        try:
            ok = ok & ( ( (comp[n]>0) & (comp[n+1]>0) & (comp[n+2]>0) ) ==False)
        except:
            None
            
        # Check the tracer isn't attached to a star
        ok = ok & ( family[n] == 100 )
        
        # Hacky way to figure out if this is actually just an orbit transition
        ok_out = np.copy(ok)
        for m in range(len(cv_temp[n][ok])):
            go_back = True
            i_back = 1
            while go_back:
                if n-i_back < 0:
                    #ok_out[np.where(ok)[0][m]] = False # I'm not sure what the purpose of these lines is so I commented them out...
                    break
                if cv_temp[n - i_back][ok][m] < cv_temp[n - i_back+1][ok][m]:
                    i_back += 1
                else:
                    go_back = False

            go_forward = True
            i_forward = 1
            while go_forward:
                if n + i_forward == n_ts:
                    #ok_out[np.where(ok)[0][m]] = False
                    break
                if cv_temp[n+i_forward][ok][m] > cv_temp[n+i_forward-1][ok][m]:
                    i_forward+=1
                else:
                    go_forward = False

            ok2 = cv_temp[n+i_forward-1][ok][m] > abs(cv_temp[n-i_back+1][ok][m]) * 1.5
            if ok2:
                vhi_flag[np.where(ok)[0][m]] = cv_temp[n+i_forward-1][ok][m]
                vlo_flag[np.where(ok)[0][m]] = cv_temp[n-i_back+1][ok][m]
                temp2[n,np.where(ok)[0][m]] = 0.1 
            
            # now check rmax
            go_forward = True
            i_forward = 1
            while go_forward:
                if n + i_forward == n_ts:
                    #ok_out[np.where(ok)[0][m]] = False
                    break
                if radius[n+i_forward][ok][m] > radius[n+i_forward-1][ok][m] or np.isnan(radius[n+i_forward][ok][m]):
                    i_forward+=1
                else:
                    go_forward = False
                    
            ok3 = (radius[n+i_forward-1][ok][m] > radius[n][ok][m] +0.1) | np.isnan(radius[n+i_forward-1][ok][m]) | (radius[n][ok][m] > radius[n-1][ok][m] +0.1)
            
            if ok3:
                rad_lo_flag[np.where(ok)[0][m]] = radius[n][ok][m]
                rad_hi_flag[np.where(ok)[0][m]] = radius[n+i_forward-1][ok][m]
                temp3[n,np.where(ok)[0][m]] = 0.2
            ok_out[np.where(ok)[0][m]] = ok2 & ok3
            
        ok = ok_out
        fb_flag[ok] = 1
        ts_fb_flag[ok] = n

        fb_flag_evo[n:,ok] = 1
        
    ####### Track 1st-infall, past-peri, and past-apo
    n_inflow = np.zeros(n_part)
    n_outflow = np.zeros(n_part)
    n_change = np.zeros(n_part)
    part_orb_type = np.zeros(n_part) # 0 here means 1st-infall
    # 1 will be past-peri, 2 will be past-apo, nan means ejected
    left = np.zeros(n_part) < 0
    returned = np.zeros(n_part) < 0

    part_orb_type_evo = np.zeros((n_ts, n_part))
    
    for n in range(n_ts):
        inflow = cv_temp[n] < 0
        outflow = cv_temp[n] > 0 # cv_temp set such that 0 means attached to a star
        
        n_inflow[inflow] += 1
        n_inflow[outflow] = 0
        
        n_outflow[outflow] += 1
        n_outflow[inflow] = 0
 
        req_same = 3
        
        in_to_out = ((part_orb_type == 2 ) | (part_orb_type == 0)) & ((n_outflow > req_same) | ((n_outflow>=1)&(radius[n] - radius[(n-n_outflow).astype("int"),np.arange(0,n_part)]>0.1)))
        part_orb_type[in_to_out] = 1
        part_orb_type_evo[n-req_same:,in_to_out] = 1
        fb_flag[(fb_flag_evo[n]>1)&in_to_out] = 3
        fb_flag_evo[n-req_same:,(fb_flag_evo[n]>1)&in_to_out] = 3
        
        out_to_in = (part_orb_type == 1) & (n_inflow > req_same)
        part_orb_type[out_to_in] = 2
        part_orb_type_evo[n-req_same:,out_to_in] = 2
        fb_flag[(fb_flag_evo[n]>0)&out_to_in] = 2
        fb_flag_evo[n-req_same:,(fb_flag_evo[n]>0)&out_to_in] = 2
        
        left = ((n > first_infall_ts) & (np.isnan(cv_temp[n]))) | left
        part_orb_type[left] = np.nan
        part_orb_type_evo[n:,left] = np.nan
                
        returned = left & (np.isnan(cv_temp[n])==False)
        part_orb_type[returned] = 2
        part_orb_type_evo[n:,returned] = 2
        fb_flag[(fb_flag_evo[n]>0)&returned] = 2
        fb_flag_evo[n:,(fb_flag_evo[n]>0)&returned] = 2
        left[returned] = False
    #quit()
        #i_part = 130
        #print inflow[i_part], outflow[i_part], part_orb_type[i_part], fb_flag[i_part], part_orb_type_evo[n][i_part], fb_flag_evo[n][i_part]
        
    # Handle possible transitions at the end
    in_to_out = ((part_orb_type == 2 ) | (part_orb_type == 0)) & (n_outflow > 1)
    part_orb_type[in_to_out] = 1
    part_orb_type_evo[-2:,in_to_out] = 1
    
    out_to_in = (part_orb_type == 1) & (n_inflow > 1)
    part_orb_type[out_to_in] = 2
    part_orb_type_evo[-2:,out_to_in] = 2

    fb_flag[(fb_flag>0) & in_to_out] = 3
    fb_flag[(fb_flag>0) & out_to_in] = 2
    fb_flag_evo[-2:,(fb_flag>1) & in_to_out] = 3
    fb_flag_evo[-2:,(fb_flag>0) & out_to_in] = 2

    fb_test_cases = np.zeros(n_part)
    for n in range(n_ts):
        fb_test_cases[(fb_test_cases==0)&(family[n]>100)] = 1 
        fb_test_cases[(fb_test_cases==1)&(family[n]==100)] = 2
        fb_test_cases[(fb_test_cases==2)&(radius[n]>0.2)] = 3
        
    fb_test_cases2 = np.zeros(n_part)
    for n in range(n_ts):
        fb_test_cases2[ (family[n] == 100) & (cv_temp[n]>200)] = 1
        
    have_a_look = False
    if have_a_look:
        #show = np.where( fb_flag>0 )[0]
        #show = np.where(fb_test_cases==3)[0]
        show = np.where(fb_test_cases2==1)[0]
        print len(show)
        print show
        
        # Plotting to have a look at trajectories of different particle selections
        from utilities_plotting import *
        import os
        for n in range(len(show)):
            py.clf()

            py.subplot(411)
            py.ylabel(r"$r \, / r_{\mathrm{vir}}$")
            
            try:
                py.axvline(t[ts_fb_flag[show[n]]])
            except:
                pass
            py.plot(t, radius[:,show[n]], c="k")
            py.plot(t, comp[:,show[n]]*0.1, c="k", linestyle=':')

            py.plot(t, part_orb_type_evo[:,show[n]]*0.1, c="b",linestyle="--")
            py.plot(t, fb_flag_evo[:,show[n]]*0.12, c="r",linestyle="--")
            
            #py.axhline(rad_lo_flag[show[n]])
            #py.axhline(rad_hi_flag[show[n]])
            #py.axhline(rad_lo_flag[show[n]]+0.1,c="g")
            
            #py.scatter(t, temp2[:,show[n]], c="r",s=50,edgecolors="none")
            #py.scatter(t, temp3[:,show[n]], c="k",s=50,edgecolors="none")
            
            py.xlim((t[first_infall_ts[show[n]]], t[i_final]))
            
            py.ylim((0,1.))
            
            py.subplot(412)
            py.ylabel(r"$v_{\mathrm{rad}} \, / \mathrm{km s^{-1}}$")

            try:
                py.axvline(t[ts_fb_flag[show[n]]])
            except:
                pass
            py.plot(t, cv_temp[:,show[n]],c="k")
            py.axhline(0.0)
            py.axhline(vhi_flag[show[n]])
            py.axhline(vlo_flag[show[n]])
            py.axhline(abs(vlo_flag[show[n]])*1.5,c="g")
            py.xlim((t[first_infall_ts[show[n]]], t[i_final]))
            
            py.subplot(413)
            py.ylabel(r"$\log_{10}(T \, / \mathrm{K})$")
            try:
                py.axvline(t[ts_fb_flag[show[n]]])
            except:
                pass
            py.plot(t, np.log10(temp_temp[:,show[n]]),c="k")
            py.axhline(4.0)
            py.xlim((t[first_infall_ts[show[n]]], t[i_final]))

            '''py.subplot(414)
            py.ylabel(r"$\log_{10}(e \, / \mathrm{km^2 s^{-2}})$")
            py.xlabel(r"$t \, / mathrm{Gyr}$")
            try:
                py.axvline(t[ts_fb_flag[show[n]]])
            except:
                pass
            py.plot(t, np.log10(egrav[:,show[n]]),c="g",label="gravitational")
            py.plot(t, np.log10(eth[:,show[n]]),c="b",label="thermal",)
            py.plot(t, np.log10(ek[:,show[n]]),c="r",label="kinetic")
            py.plot(t, np.log10((ek+eth)[:,show[n]]),c="k",label="thermal+kinetic")
            #py.plot(t, np.log10((ek+eth-egrav)[:,show[n]]),c="k",label="tot")
            #py.plot(t, (ek+eth-egrav)[:,show[n]],c="k",label="tot")
            py.xlim((t[first_infall_ts[show[n]]], t[i_final]))'''
            #py.legend()

            py.subplot(414)
            py.ylabel(r"$x_{\mathrm{HI}}$")
            py.xlabel(r"$t \, / \mathrm{Gyr}$")
            try:
                py.axvline(t[ts_fb_flag[show[n]]])
            except:
                pass
            py.plot(t, xhi[:,show[n]],c="k")
            py.xlim((t[first_infall_ts[show[n]]], t[i_final]))

            
            #py.show()
            #quit()

            py.savefig("Temp/"+str(n)+".png")
            
            if n == 50:
                break
            
        quit()

    '''#Now remove those that are a star on final step or are not in the cgm on final step
    ok = (radius[-1] > 0.2) & (family[-1] == 100)

    comp = comp[:,ok]; cv = cv[:,ok]; nh = nh[:,ok]; temp = temp[:,ok]; xhi = xhi[:,ok];
    radius = radius[:,ok]; family = family[:,ok]; radius_p = radius_p[:,ok]
    ek = ek[:,ok]; eth = eth[:,ok]; egrav = egrav[:,ok]; LyaLum=LyaLum[:,ok]
    first_infall_ts = first_infall_ts[ok]
    n_part = len(comp[0]); enter_in_sat = enter_in_sat[ok]
    part_orb_type = part_orb_type[ok]; fb_flag = fb_flag[ok]
    part_orb_type_evo = part_orb_type_evo[:,ok]'''
    # The above block is no longer needed because I put the CGM check into the fractions plot
    # (and will put it into the rates measurements also)


    ############### Measure the basic fractions ##########################

    n_part = float(n_part)
    
    for n in range(n_type):

        if select_types[n] == "m_tot":
            quant = np.ones(n_part)
        elif select_types[n] == "m_neut":
            quant = xhi[-1]
        elif select_types[n] == "LyaLum":
            quant = LyaLum[-1]
        else:
            print "Nope", select_types
            quit()

        #print quant.shape, f_1st_infall_evo.shape, part_orb_type.shape, fb_flag.shape, enter_in_sat.shape
        #quit()

        ok = (radius[-1] > 0.2) & (radius[-1] < 1.0) & (family[-1] == 100) & (end_in_sat == False)

        # "Gravity only" class
        f_1st_infall_evo[n][i_ts] = np.sum( quant[(part_orb_type==0)&(fb_flag==0)&(enter_in_sat==False)&ok] ) / np.sum(quant[ok])
        f_past_peri_evo[n][i_ts] = np.sum( quant[(part_orb_type==1)&(fb_flag==0)&(enter_in_sat==False)&ok] ) / np.sum(quant[ok])
        f_past_apo_evo[n][i_ts] = np.sum( quant[(part_orb_type==2)&(fb_flag==0)&(enter_in_sat==False)&ok] ) / np.sum(quant[ok])

        # Gravity only, but entered within a satellite
        f_strip_1st_infall_evo[n][i_ts] = np.sum( quant[(part_orb_type==0)&(fb_flag==0)&(enter_in_sat)&ok] ) / np.sum(quant[ok])
        f_strip_past_peri_evo[n][i_ts] = np.sum( quant[(part_orb_type==1)&(fb_flag==0)&(enter_in_sat)&ok] ) / np.sum(quant[ok])
        f_strip_past_apo_evo[n][i_ts] = np.sum( quant[(part_orb_type==2)&(fb_flag==0)&(enter_in_sat)&ok] ) / np.sum(quant[ok])
        
        # "feedback influenced" class        
        f_fb_1st_outflow_evo[n][i_ts] = np.sum( quant[(fb_flag==1)&ok] ) / np.sum(quant[ok])
        f_fb_past_apo_evo[n][i_ts] = np.sum( quant[(fb_flag==2)&ok] ) / np.sum(quant[ok])
        f_fb_past_peri_evo[n][i_ts] = np.sum( quant[(fb_flag==3)&ok] ) / np.sum(quant[ok])


    # Now measure the exchange rates
    # Note will want to split this into radial bins later on

    # Only do this on the final run through the loop
    if i_final != len(t_in)-1: 
        continue
    
    ######## Compute monotonic xhi and monotonic T ##############

    # Figure out start/finish props for each kinematic episode
    xhi_start = np.zeros_like(xhi)
    xhi_stop = np.zeros_like(xhi)+np.nan
    xhi_start[0] = xhi[0]
    temp_start = np.zeros_like(temp)
    temp_stop = np.zeros_like(temp)+np.nan
    temp_start[0] = temp[0]

    mono_xhi_increase = np.zeros(n_part) # Number of outputs a quantity has been increasing
    mono_xhi_decrease = np.zeros(n_part)# Number of outputs a quantity has been decreasing
    mono_xhi_increase_evo = np.zeros((n_ts,n_part)) # >0 if quantity is considered monotonically increasing
    mono_xhi_decrease_evo = np.zeros((n_ts,n_part)) # >0 if quantity is considered monotonically decreasing
    
    mono_temp_increase = np.zeros(n_part)
    mono_temp_decrease = np.zeros(n_part)
    mono_temp_increase_evo = np.zeros((n_ts,n_part))
    mono_temp_decrease_evo = np.zeros((n_ts,n_part))

    for n in range(n_ts):
        if n == 0:
            continue
        ok = (part_orb_type_evo[n-1] != part_orb_type_evo[n]) & (np.isnan(xhi[n])==False)

        ok = ok | ((np.isnan(xhi[n])==False) & (np.isnan(xhi[n-1])))
        
        xhi_start[n,ok] = xhi[n,ok]
        #xhi_stop[n-1,ok] = xhi[n-1,ok]
        xhi_stop[n-1,ok] = xhi[n,ok]
        temp_start[n,ok] = temp[n,ok]
        #temp_stop[n-1,ok] = temp[n-1,ok]
        temp_stop[n-1,ok] = temp[n,ok]

        mono_xhi_increase[ok] = 0
        mono_xhi_decrease[ok] = 0
        mono_temp_increase[ok] = 0
        mono_temp_decrease[ok] = 0
        
        nok = (ok == False) & (np.isnan(xhi[n])==False)
        xhi_start[n,nok] = xhi_start[n-1,nok]
        xhi_stop[n,nok] = xhi_stop[n-1,nok]
        temp_start[n,nok] = temp_start[n-1,nok]
        temp_stop[n,nok] = temp_stop[n-1,nok]

        mono_xhi_increase[nok & (xhi[n] > xhi[n-1])] += 1
        mono_xhi_decrease[nok & (xhi[n] > xhi[n-1])] =0
        mono_xhi_decrease[nok & (xhi[n] < xhi[n-1])] += 1
        mono_xhi_increase[nok & (xhi[n] < xhi[n-1])] =0

        mono_temp_increase[nok & (temp[n] > temp[n-1])] += 1
        mono_temp_decrease[nok & (temp[n] > temp[n-1])] =0
        mono_temp_decrease[nok & (temp[n] < temp[n-1])] += 1
        mono_temp_increase[nok & (temp[n] < temp[n-1])] =0
       
        # Deal with ejected particles
        ok = np.isnan(xhi[n]) & (np.isnan(xhi[n-1])==False)
        #xhi_stop[n-1,ok] = xhi[n-1,ok]
        #temp_stop[n-1,ok] = temp[n-1,ok]
        xhi_stop[n-1,ok] = xhi[n,ok]
        temp_stop[n-1,ok] = temp[n,ok]

        mono_xhi_increase[ok] = 0
        mono_xhi_decrease[ok] = 0
        mono_temp_increase[ok] = 0
        mono_temp_decrease[ok] = 0

        n_mono_thr = 3
        
        ok = mono_xhi_increase > n_mono_thr
        n_loop = int(np.max(mono_xhi_increase))
        for i_n in range(n_loop):
            mono_xhi_increase_evo[n-i_n,ok] = np.max((mono_xhi_increase[ok]-i_n,np.zeros(len(ok[ok]))),axis=0)
        ok = mono_xhi_decrease > n_mono_thr
        n_loop = int(np.max(mono_xhi_decrease))
        for i_n in range(n_loop):
            mono_xhi_decrease_evo[n-i_n,ok] = np.max((mono_xhi_decrease[ok]-i_n,np.zeros(len(ok[ok]))),axis=0)

        ok = mono_temp_increase > n_mono_thr
        n_loop = int(np.max(mono_temp_increase))
        for i_n in range(n_loop):
            mono_temp_increase_evo[n-i_n,ok] = np.max((mono_temp_increase[ok]-i_n,np.zeros(len(ok[ok]))),axis=0)
        ok = mono_temp_decrease > n_mono_thr
        n_loop = int(np.max(mono_temp_decrease))
        for i_n in range(n_loop):
            mono_temp_decrease_evo[n-i_n,ok] = np.max((mono_temp_decrease[ok]-i_n,np.zeros(len(ok[ok]))),axis=0)
        
    xhi_stop[-1] = xhi[-1]
    temp_stop[-1] = temp[-1]
    
    # Finishing setting the end xhi/T for each kinematic episode (need to go backwards through loop)
    for m in range(n_ts):
        if m == 0:
            continue
        n = n_ts-1-m

        ok = (part_orb_type_evo[n+1] != part_orb_type_evo[n]) & (np.isnan(xhi[n])==False)
        nok = (ok == False) & (np.isnan(xhi[n])==False)
        xhi_stop[n,nok] = xhi_stop[n+1,nok]
        temp_stop[n,nok] = temp_stop[n+1,nok]
        
    # Compute monotonic version of props
    xhi_mono = np.copy(xhi) # 1st mono version - where all fluctuations inside a given kinetmatic period are kill
    temp_mono = np.copy(temp)
    for n in range(n_ts):
        if n == 0:
            continue
        
        increase = (xhi_start[n] <= xhi_stop[n]) & (np.isnan(xhi[n])==False) & (np.isnan(xhi[n-1])==False)
        increase = increase & (part_orb_type_evo[n] == part_orb_type_evo[n-1])
        decrease = (xhi_start[n] > xhi_stop[n]) & (np.isnan(xhi[n])==False) & (np.isnan(xhi[n-1])==False)
        decrease = decrease & (part_orb_type_evo[n] == part_orb_type_evo[n-1])
        
        xhi_mono[n,increase] = np.max( [xhi_mono[n-1,increase], xhi[n,increase]],axis=0)
        xhi_mono[n,increase] = np.min( [xhi_mono[n,increase], xhi_stop[n,increase]],axis=0)
        
        xhi_mono[n,decrease] = np.min( [xhi_mono[n-1,decrease], xhi[n,decrease]],axis=0)
        xhi_mono[n,decrease] = np.max( [xhi_mono[n,decrease], xhi_stop[n,decrease]],axis=0)

        increase = (temp_start[n] <= temp_stop[n]) & (np.isnan(temp[n])==False) & (np.isnan(temp[n-1])==False)
        decrease = (temp_start[n] > temp_stop[n]) & (np.isnan(temp[n])==False) & (np.isnan(temp[n-1])==False)
        
        temp_mono[n,increase] = np.max( [temp_mono[n-1,increase], temp[n,increase]],axis=0)
        temp_mono[n,increase] = np.min( [temp_mono[n,increase], temp_stop[n,increase]],axis=0)
        
        temp_mono[n,decrease] = np.min( [temp_mono[n-1,decrease], temp[n,decrease]],axis=0)
        temp_mono[n,decrease] = np.max( [temp_mono[n,decrease], temp_stop[n,decrease]],axis=0)

    # Allow long-term fluctuations (which monotonically increase or decrease for more than a given no of outputs)
    xhi_mono2 = np.copy(xhi_mono) # 2nd version - which permits longer term fluctuations
    temp_mono2 = np.copy(temp_mono)
    for n in range(n_ts):
        if n == 0:
            continue

        allow = (mono_xhi_increase_evo[n] > 0) | (mono_xhi_decrease_evo[n] >0)
        xhi_mono2[n,allow] = xhi[n,allow]
        allow = (mono_temp_increase_evo[n] > 0) | (mono_temp_decrease_evo[n] >0)
        temp_mono2[n,allow] = temp[n,allow]

        
    '''check = 0
    for n in range(n_ts):
        #print part_orb_type_evo[n,check], xhi[n,check], xhi_start[n,check], xhi_stop[n,check], xhi_mono[n,check]
        #print part_orb_type_evo[n,check], temp[n,check], temp_start[n,check], temp_stop[n,check], temp_mono[n,check]
        print part_orb_type_evo[n,check], xhi[n,check], mono_xhi_increase_evo[n,check], mono_xhi_decrease_evo[n,check], xhi_mono[n,check]
    quit()'''






    have_a_look2 = False
    if have_a_look2:
        show = np.where( fb_flag>0 )[0]
        #show = np.where(fb_test_cases==3)[0]
        #show = np.where(fb_test_cases2==1)[0]
        print len(show)
        print show
        
        # Plotting to have a look at trajectories of different particle selections
        from utilities_plotting import *
        import os
        for n in range(len(show)):
            py.clf()

            py.subplot(511)
            py.ylabel(r"$r \, / r_{\mathrm{vir}}$")
            
            try:
                py.axvline(t[ts_fb_flag[show[n]]])
            except:
                pass
            py.plot(t, radius[:,show[n]], c="k")
            py.plot(t, comp[:,show[n]]*0.1, c="k", linestyle=':')

            py.plot(t, part_orb_type_evo[:,show[n]]*0.1, c="b",linestyle="--")
            py.plot(t, fb_flag_evo[:,show[n]]*0.12, c="r",linestyle="--")
            
            py.xlim((t[first_infall_ts[show[n]]], t[i_final]))
            py.ylim((0,1.))
            
            py.subplot(512)
            py.ylabel(r"$v_{\mathrm{rad}} \, / \mathrm{km s^{-1}}$")

            try:
                py.axvline(t[ts_fb_flag[show[n]]])
            except:
                pass
            py.plot(t, cv_temp[:,show[n]],c="k")
            py.axhline(0.0)
            #py.axhline(vhi_flag[show[n]])
            #py.axhline(vlo_flag[show[n]])
            #py.axhline(abs(vlo_flag[show[n]])*1.5,c="g")
            py.xlim((t[first_infall_ts[show[n]]], t[i_final]))
            
            py.subplot(513)
            py.ylabel(r"$\log_{10}(T \, / \mathrm{K})$")
            try:
                py.axvline(t[ts_fb_flag[show[n]]])
            except:
                pass
            py.plot(t, np.log10(temp_temp[:,show[n]]),c="k")
            py.plot(t, np.log10(temp_mono[:,show[n]]),c="b",linestyle=':')
            py.plot(t, np.log10(temp_mono2[:,show[n]]),c="b",linestyle='--')
            py.axhline(4.0)
            py.xlim((t[first_infall_ts[show[n]]], t[i_final]))

            py.subplot(514)
            py.ylabel(r"$\log_{10}(\rho \, / \mathrm{Msun kmc^{-3}})$")
            try:
                py.axvline(t[ts_fb_flag[show[n]]])
            except:
                pass
            py.plot(t, np.log10(nh[:,show[n]]),c="k")
            py.axhline(4.0)
            py.xlim((t[first_infall_ts[show[n]]], t[i_final]))

            py.subplot(515)
            py.ylabel(r"$x_{\mathrm{HI}}$")
            py.xlabel(r"$t \, / \mathrm{Gyr}$")
            try:
                py.axvline(t[ts_fb_flag[show[n]]])
            except:
                pass
            py.plot(t, xhi[:,show[n]],c="k")
            py.plot(t, xhi_mono[:,show[n]],c="b",linestyle=':')
            py.plot(t, xhi_mono2[:,show[n]],c="b",linestyle='--')
            py.xlim((t[first_infall_ts[show[n]]], t[i_final]))

            
            #py.show()
            #quit()

            py.savefig("Temp/"+str(n)+".png")
            
            if n == 50:
                break
            
        quit()



    r_match_type = ["_mean", "_ps", "_ts"]
    exchange_terms = np.zeros(( n_ts, len(weight_n), len(weight_n1), len(r_match_type), len(r_bins[0:-1]) ))
    
    for m in range(n_ts):

        try:
            t_cosmic_prev = t_cosmic
        except:
            pass
            
        r2kpc, redshift, little_h, m2Msun,rho2msunpkpc3,v2kms = ur.Set_Units(RunDir, timestep[m])
        a = 1./(1.+redshift)
        omega_0,lambda_0,little_h = dm.jeje_utils.read_cosmo_params(RunDir,timestep[m])
        t_cosmic, junk = uc.t_Universe(a,omega_0,little_h)

        try:
            delta_t = (t_cosmic - t_cosmic_prev) * 1e9 # yr
        except:
            None

        if m == 0:
            continue
            
        # Could also set to temp, temp_mono, or temp_mono2
        temp_use = temp_mono
        xhi_use = xhi_mono
        
        for i_n in range(len(weight_n)):
            for i_n1 in range(len(weight_n1)):

                ok = (comp[m] == 0) & (comp[m-1]==0)# Don't add stuff inside satellites
                ok = ok & (family[m] == 100) & (family[m-1]==100) # No stars!
                
                # Radius range
                ok = ok & (radius[m] > r_lo_n1[i_n1]) & (radius[m] < r_hi_n1[i_n1])
                ok = ok & (radius[m-1] > r_lo_n[i_n]) & (radius[m-1] < r_hi_n[i_n])
                
                # Temperature range
                ok = ok & (temp_use[m] > T_lo_n1[i_n1]) & (temp_use[m] < T_hi_n1[i_n1])
                ok = ok & (temp_use[m-1] > T_lo_n[i_n]) & (temp_use[m-1] < T_hi_n[i_n])
                
                # Neutral range
                ok = ok & (xhi_use[m] > xhi_lo_n1[i_n1]) & (xhi_use[m] < xhi_hi_n1[i_n1])
                ok = ok & (xhi_use[m-1] > xhi_lo_n[i_n]) & (xhi_use[m-1] < xhi_hi_n[i_n])

                # Select kinematic state (based on Lagrangian trajectory)
                # Group inflows together from 1st infall and past-apo
                temp_kine = np.copy(part_orb_type_evo[m])
                temp_kine[temp_kine == 2] = 0
                temp_kine_prev = np.copy(part_orb_type_evo[m-1])
                temp_kine_prev[temp_kine_prev==2] = 0
                
                ok = ok & (temp_kine == part_n1[i_n1]) & (temp_kine_prev == part_n[i_n])
                
                # Choose what quantity to weight by
                if select_type_list[i_n] == "mass":
                    quant = np.ones(n_part) *1e3 # This should be tmass eventually
                    quant_prev = np.ones(n_part)*1e3 # This should be tmass eventually
                elif select_type_list[i_n] == "LyaLum":
                    quant = LyaLum[m]
                    quant_prev = LyaLum[m-1]
                else:
                    print "Nope, choose mass or LyaLum"
                    quit()
                    
                for i_r_match in range(len(r_match_type)):

                    if i_r_match == 0:
                        r_match = 0.5 * (radius[m] + radius[m-1])[ok]
                    elif i_r_match == 1:
                        r_match = radius[m-1][ok]
                    elif i_r_match == 2:
                        r_match = radius[m][ok]
                    else:
                        print "nope"
                        quit()
                        
                    for i_r in range(len(r_bins[0:-1])):
                        okr = (r_match > r_bins[i_r]) & (r_match < r_bins[i_r+1])

                        quant_xhi_prev = quant_prev[ok][okr] * xhi[m-1,ok][okr]
                        quant_xhii_prev =quant_prev[ok][okr] * (1-xhi[m-1,ok][okr])

                        quant_xhi = quant[ok][okr] * xhi_use[m,ok][okr]
                        quant_xhii = quant[ok][okr] * (1-xhi_use[m,ok][okr])

                        if weight_n[i_n] == "neutral" and weight_n1[i_n1] == "neutral":
                            exchange_terms[m][i_n][i_n1][i_r_match][i_r] = np.sum( np.min(( quant_xhi_prev, quant_xhi ),axis=0) ) / delta_t / delta_rprime # Msun yr^-1 (if quant is in Msun)
                        elif weight_n[i_n] == "neutral" and weight_n1[i_n1] == "all":
                            exchange_terms[m][i_n][i_n1][i_r_match][i_r] = np.sum( quant_xhi_prev ) / delta_t / delta_rprime
                        elif weight_n[i_n] == "ion" and weight_n1[i_n1] == "ion":
                            exchange_terms[m][i_n][i_n1][i_r_match][i_r] = np.sum( np.min(( quant_xhii_prev, quant_xhii ),axis=0) ) / delta_t / delta_rprime
                        elif weight_n[i_n] == "ion" and weight_n1[i_n1] == "all":
                            exchange_terms[m][i_n][i_n1][i_r_match][i_r] = np.sum( quant_xhii_prev ) / delta_t /delta_rprime
                        elif weight_n[i_n] == "neutral" and weight_n1[i_n1] == "ion":
                            exchange_terms[m][i_n][i_n1][i_r_match][i_r] = np.sum( np.max(( quant_xhi_prev - quant_xhi ,np.zeros_like(quant_xhi)),axis=0) ) / delta_t / delta_rprime
                        elif weight_n[i_n] == "ion" and weight_n1[i_n1] == "neutral":
                            exchange_terms[m][i_n][i_n1][i_r_match][i_r] = np.sum( np.max(( quant_xhii_prev - quant_xhii ,np.zeros_like(quant_xhi)),axis=0) ) / delta_t / delta_rprime
                        else:
                            print weight_n[i_n], "into", weight_n1[i_n1], "is not implemented"
                            quit()


from utilities_plotting import *


show_exchange_rate_figure = True
if show_exchange_rate_figure:

    py.figure()
    nrow = 2; ncol=2
    py.rcParams.update(latexParams)
    py.rcParams.update({'figure.subplot.wspace':0,'figure.subplot.hspace':0,
                        'figure.figsize':[3.32*ncol,2.49*nrow],
                        'figure.subplot.left':0.17,
                        'figure.subplot.bottom':0.1,
                        'figure.subplot.top':0.98,
                        'figure.subplot.right':0.99,
                        'legend.fontsize':6,
                        'font.size':12,
                        'axes.labelsize':11})

    subplots = panelplot(nrow,ncol)
    for n,ax in enumerate(subplots):
        py.axes(ax)

        r_mid = 0.5 * (r_bins[0:-1] + r_bins[1:])
        
        if n == 0:
            nono_stack_ps = np.zeros(len(r_mid))
            nowio_stack_ps = np.zeros(len(r_mid))
            nohio_stack_ps = np.zeros(len(r_mid))
            noin_stack_ps = np.zeros(len(r_mid))
            
            for i_r in range(len(r_mid)):
                # r_match_type is 0, mean, 1 ps, 2, ts
                nono_stack_ps[i_r] = np.sum( exchange_terms[:, 0, 0, 1, i_r]) / len(exchange_terms[:,0,0,0,0])
                nowio_stack_ps[i_r] = np.sum( exchange_terms[:, 0, 2, 1, i_r]) / len(exchange_terms[:,0,0,0,0])
                nohio_stack_ps[i_r] = np.sum( exchange_terms[:, 0, 3, 1, i_r]) / len(exchange_terms[:,0,0,0,0])
                noin_stack_ps[i_r] = np.sum( exchange_terms[:, 0, 5, 1, i_r]) / len(exchange_terms[:,0,0,0,0])
                
            py.plot(r_mid, np.log10(nono_stack_ps),label="Neut out > Neut out",c="b")
            py.plot(r_mid, np.log10(nowio_stack_ps),label="Neut out > Warm-ionized out",c="g")
            py.plot(r_mid, np.log10(nohio_stack_ps),label="Neut out > Hot-ionized out",c="m")
            py.plot(r_mid, np.log10(noin_stack_ps),label="Neut out > Inflow",c="k")

        if n == 1:
            nono_stack_ts = np.zeros(len(r_mid))
            wiono_stack_ts = np.zeros(len(r_mid))
            hiono_stack_ts = np.zeros(len(r_mid))
            inno_stack_ts = np.zeros(len(r_mid))
            
            for i_r in range(len(r_mid)):
                # r_match_type is 0, mean, 1 ps, 2, ts
                nono_stack_ts[i_r] = np.sum( exchange_terms[:, 0, 0, 2, i_r]) / len(exchange_terms[:,0,0,0,0])
                wiono_stack_ts[i_r] = np.sum( exchange_terms[:, 2, 0, 2, i_r]) / len(exchange_terms[:,0,0,0,0])
                hiono_stack_ts[i_r] = np.sum( exchange_terms[:, 4, 0, 2, i_r]) / len(exchange_terms[:,0,0,0,0])

                inno_tot = exchange_terms[:,1,0,2,i_r] + exchange_terms[:,3,0,2,i_r] +exchange_terms[:,5,0,2,i_r]
                inno_stack_ts[i_r] = np.sum( inno_tot)/ len(exchange_terms[:,0,0,0,0])
                
            py.plot(r_mid, np.log10(nono_stack_ts),label="Neut out > Neut out",c="b")
            py.plot(r_mid, np.log10(wiono_stack_ts),label="Warm-ionized out > Neut out",c="g")
            py.plot(r_mid, np.log10(hiono_stack_ts),label="Hot-ionized out > Neut out ",c="m")
            py.plot(r_mid, np.log10(inno_stack_ts),label="Inflow > Neut out ",c="k")

        if n == 2:
            nini_stack_ps = np.zeros_like(r_mid)
            niwii_stack_ps = np.zeros_like(r_mid)
            nihii_stack_ps = np.zeros_like(r_mid)
            niout_stack_ps = np.zeros_like(r_mid)
            for i_r in range(len(r_mid)):

                nini_stack_ps[i_r] = np.sum( exchange_terms[:, 1, 4, 1, i_r]) / len(exchange_terms[:,0,0,0,0])
                niwii_stack_ps[i_r] = np.sum( exchange_terms[:, 1, 6, 1, i_r]) / len(exchange_terms[:,0,0,0,0])
                nihii_stack_ps[i_r] = np.sum( exchange_terms[:, 1, 7, 1, i_r]) / len(exchange_terms[:,0,0,0,0])
                niout_stack_ps[i_r] = np.sum( exchange_terms[:, 1, 1, 1, i_r]) / len(exchange_terms[:,0,0,0,0])
                
            py.plot( r_mid, np.log10(nini_stack_ps), c="b",label=r"$\mathrm{\bf{Neutral \, inflow}} \, > \, \mathrm{Neutral \, inflow}$")
            py.plot( r_mid, np.log10(niwii_stack_ps), c="g",label=r"$\mathrm{\bf{Neutral \, inflow}} \, > \, \mathrm{Warm \, inflow")
            py.plot( r_mid, np.log10(nihii_stack_ps), c="m",label=r"$\mathrm{\bf{Neutral \, inflow}} \, > \, \mathrm{Hot \, inflow}$")
            py.plot( r_mid, np.log10(niout_stack_ps), c="k",label=r"$\mathrm{\bf{Neutral \, inflow}} \, > \, \mathrm{Outflow}$")

        if n == 3:
            nini_stack_ts = np.zeros_like(r_mid)
            wiini_stack_ts = np.zeros_like(r_mid)
            hiini_stack_ts = np.zeros_like(r_mid)
            outni_stack_ts = np.zeros_like(r_mid)
            for i_r in range(len(r_mid)):

            #exchange_terms = np.zeros(( n_ts, len(weight_n), len(weight_n1), len(r_match_type), len(r_bins[0:-1]) ))
            #name_n =   ["neut_out","neut_in","wion_out","wion_in","hion_out","hion_in"]
            #name_n1 =   ["neut_out","tot_outflow","wion_out","hion_out","neut_in", "tot_inflow","wion_in", "hion_in"]

                
                nini_stack_ts[i_r] = np.sum( exchange_terms[:, 1, 4, 2, i_r]) / len(exchange_terms[:,0,0,0,0])
                wiini_stack_ts[i_r] = np.sum( exchange_terms[:, 3, 4, 2, i_r]) / len(exchange_terms[:,0,0,0,0])
                hiini_stack_ts[i_r] = np.sum( exchange_terms[:, 5, 4, 2, i_r]) / len(exchange_terms[:,0,0,0,0])

                outni = exchange_terms[:,0,4,2,i_r] + exchange_terms[:,2,4,2,i_r] + exchange_terms[:,4,4,2,i_r]
                outni_stack_ts[i_r] = np.sum( outni) / len(exchange_terms[:,0,0,0,0])
                
            py.plot( r_mid, np.log10(nini_stack_ts), c="b",label=r"$\mathrm{Neutral \, inflow} \, > \, \mathrm{\bf{Neutral \, inflow}}$")
            py.plot( r_mid, np.log10(wiini_stack_ts), c="g",label=r"$\mathrm{Warm \, inflow}  \, > \, \mathrm{\bf{Neutral \, inflow}}")
            py.plot( r_mid, np.log10(hiini_stack_ts), c="m",label=r"$\mathrm{Hot \, inflow} \, > \, \mathrm{\bf{Neutral \, inflow}} $")
            py.plot( r_mid, np.log10(outni_stack_ts), c="k",label=r"$\mathrm{Outflow}  \, > \, \mathrm{\bf{Neutral \, inflow}}$")

        if n >= ncol*nrow- ncol:
            py.xlabel(r"$r \, / R_{\mathrm{vir}}$")
        if n % ncol == 0:
            py.ylabel(r"$\log_{10}\left(\frac{\mathrm{d}}{\mathrm{d} (r/r_{\mathrm{vir}})} \, \frac{\mathrm{d} m}{\mathrm{d} t} \, / \mathrm{M_\odot yr{-1}}\right)$")


            
            
        py.xlim((0.0,1.0))

    fig_name = "intermediate_lagrangian_exchange_rates.pdf"
    py.savefig("Figures/"+fig_name)
    py.show()
#print exchange_terms.shape
#print exchange_terms[-1, 0, 0, 0, 0]
#print timestep

quit()
            
    
t = t[skip_to:]


c_list = ["k","b","m","r","g","y","c","firebrick","darkgray","orange"]

nrow = 3; ncol=1
py.rcParams.update(latexParams)
py.rcParams.update({'figure.subplot.wspace':0,'figure.subplot.hspace':0,
                    'figure.figsize':[3.32*ncol,2.49*nrow],
                    'figure.subplot.left':0.17,
                    'figure.subplot.bottom':0.1,
                    'figure.subplot.top':0.98,
                    'figure.subplot.right':0.99,
                    'legend.fontsize':6,
                    'font.size':12,
                    'axes.labelsize':11})

py.figure()
subplots = panelplot(nrow,ncol)
for n,ax in enumerate(subplots):
    py.axes(ax)

    ps = 1
    if n == 0:

        f_strip_evo = f_strip_1st_infall_evo + f_strip_past_peri_evo + f_strip_past_apo_evo
        py.plot(t, f_strip_evo[0], c="k", label="Stripped")
        py.scatter(t, f_strip_evo[0], c="k",edgecolors="none",s=ps)

        f_fb_evo = f_fb_1st_outflow_evo + f_fb_past_apo_evo + f_fb_past_peri_evo
        py.plot(t, f_fb_evo[0], c="r", label="Feedback-driven")
        py.scatter(t, f_fb_evo[0], c="r",edgecolors="none",s=ps)
        
        py.plot(t, f_1st_infall_evo[0], c="b", label="Inflowing 1st-infall")
        py.scatter(t, f_1st_infall_evo[0], c="b",edgecolors="none",s=ps)

        f_after_infall_evo = f_past_peri_evo + f_past_apo_evo
        py.plot(t, f_after_infall_evo[0], c="g", label="Orbiting after 1st-infall")
        py.scatter(t, f_after_infall_evo[0], c="g",edgecolors="none",s=ps)


        py.legend(frameon=False)
        py.ylabel(r"$f_{\mathrm{comp,tot}}$")
        py.xlim((t.min(), t.max()))
        py.ylim((0.0,1.0))

    if n == 1:
        f_strip_evo = f_strip_1st_infall_evo + f_strip_past_peri_evo + f_strip_past_apo_evo
        py.plot(t, f_strip_evo[1], c="k", label="Stripped")
        py.scatter(t, f_strip_evo[1], c="k",edgecolors="none",s=ps)

        f_fb_evo = f_fb_1st_outflow_evo + f_fb_past_apo_evo + f_fb_past_peri_evo
        py.plot(t, f_fb_evo[1], c="r", label="Feedback-driven")
        py.scatter(t, f_fb_evo[1], c="r",edgecolors="none",s=ps)
        
        py.plot(t, f_1st_infall_evo[1], c="b", label="Inflowing 1st-infall")
        py.scatter(t, f_1st_infall_evo[1], c="b",edgecolors="none",s=ps)
        
        f_after_infall_evo = f_past_peri_evo + f_past_apo_evo
        py.plot(t, f_after_infall_evo[1], c="g", label="Orbiting after 1st-infall")
        py.scatter(t, f_after_infall_evo[1], c="g",edgecolors="none",s=ps)

        py.ylabel(r"$f_{\mathrm{comp,neutral}}$")
        py.xlim((t.min(), t.max()))
        py.ylim((0.0,0.99))

    if n == 2:

        f_strip_evo = f_strip_1st_infall_evo + f_strip_past_peri_evo + f_strip_past_apo_evo
        py.plot(t, f_strip_evo[2], c="k", label="Stripped")
        py.scatter(t, f_strip_evo[2], c="k",edgecolors="none",s=ps)
        
        f_fb_evo = f_fb_1st_outflow_evo + f_fb_past_apo_evo + f_fb_past_peri_evo
        py.plot(t, f_fb_evo[2], c="r", label="Feedback-driven")
        py.scatter(t, f_fb_evo[2], c="r",edgecolors="none",s=ps)

        py.plot(t, f_1st_infall_evo[2], c="b", label="Inflowing 1st-infall")
        py.scatter(t, f_1st_infall_evo[2], c="b",edgecolors="none",s=ps)
        
        f_after_infall_evo = f_past_peri_evo + f_past_apo_evo
        py.plot(t, f_after_infall_evo[2], c="g", label="Orbiting after 1st-infall")
        py.scatter(t, f_after_infall_evo[2], c="g",edgecolors="none",s=ps)
        
        py.ylabel(r"$f_{\mathrm{comp,Ly\alpha}}$")
        py.xlim((t.min(), t.max()))
        py.ylim((0.0,0.99))
        py.xlabel(r"$t \, / \mathrm{Gyr}$")

fig_name = "intermediate_cgm_fraction_evo.pdf"
py.savefig("Figures/"+fig_name)
py.show()
