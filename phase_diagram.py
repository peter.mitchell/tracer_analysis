import argparse
import sys
import numpy as np
import time
import os

# Set paths

# This is the halo used for Figure 1 in the paper
#Zoom = "Zoom-7-25186"
#Run = "hdRun-LSS150pkpc_ncycle_1112_MSN_5"
#timestep_final = 155
#timestep = 155

timestep = 155

Zoom = "Zoom-7-5092"
Run = "RhdRun-tracer_radfix"
timestep_final = 155
OutDir = "/cral2/mitchell/PostProcess/"
RunDir = "/cral2/mitchell/"+Zoom+"/"+Run
HaloDir = RunDir + "/Halos/"
TreeDir = RunDir + "/Trees/"


###### Phase diagram maps
nbins_phase = 500

plot_phase_lines = False
plot_art_frag = False

phase_weights    = "mh"
r_inner_aperture = 0.0
r_outer_aperture = 1.0
exc_sub_phase      = 0
phase_map_name =   "phase_tot_exc_sub"

phase_temp_lo = 10 # Kelvin
phase_temp_hi = 1e8 # Kelvin
phase_nh_lo = 1e-6 # Atoms per cc
phase_nh_hi = 1e5 # Atoms per cc

vmin_phase = 1e-2
vmax_phase_f = lambda mhalo, norm: mhalo * 1e-10 * norm
#cmap_phase = "YlOrRd"
cmap_phase = "CMRmap"
phase_hi_norm = 2.5e6

timestep_tree = timestep-1
timestep_tree_final = timestep_final -1 # At the moment, the tree files store "halo_ts" as timestep -1 (don't ask)

import dm_utils as dm_utils
import cutils as cut
import h5py
import utilities_ramses as ur
import neighbour_utils as nu
import matplotlib.pyplot as py
from matplotlib.colors import LogNorm
from mpl_toolkits.axes_grid1.inset_locator import inset_axes
from utilities_plotting import *

# Read in relevant tree information from the hdf5 file
filename = Zoom + "_" + Run + "_tsf" + str(timestep_final) +".hdf5"
print "reading", OutDir+filename
File = h5py.File(OutDir+filename)

# Open the subvolume file (containing all cell data)
subvol_filename = "subvol_"+filename
subvol_File = h5py.File(OutDir+subvol_filename)

# Check the file with the tree information already exists
if not Zoom in File:
    print "Error: Tree information for ", Zoom, " in ", filename, " cannot be read"
    exit()

zoom_group = File[Zoom]

# Get conversions for different units
r2kpc, redshift, little_h, m2Msun,rho2msunpkpc3,v2kms = ur.Set_Units(RunDir, timestep)
boxlen_pkpc = dm_utils.jeje_utils.get_param_real(RunDir,timestep,'boxlen') * r2kpc # Box size in proper kpc, using the correct box size

print "reading cell data"

########## Read cell data #########################        
# Hdf5 group for this timestep (contains cell data)
ts_group = subvol_File["timestep_"+str(timestep)]
cell_group = ts_group["Gas_leaf_cell_data"]

rho_snap = cell_group["rho_c"][:] # Gas density
pre_snap = cell_group["pre_c"][:] # Gas pressure
cx = cell_group["x_c"][:] # Cell coordinates
cy = cell_group["y_c"][:]
cz = cell_group["z_c"][:]
lmjt = cell_group["lmjt"][:] # turbulent jeans length
clev = cell_group["lev"][:] # Cell refinement level
lmax = clev.max() # Maximum refinement level
c_component_index = cell_group["component_index"][:] # Flag to indicate whether inside/outside a subhalo
xH1 = cell_group["xH1"][:]
xHe1 = cell_group["xHe1"][:]
xHe2 = cell_group["xHe2"][:]

# Get m_sph - shouldn't be necessary
dm_group = ts_group["Dark_matter_particle_data"]
dm = dm_group["m_dm"][:]
d_x = dm_group["x_dm"][:]
d_y = dm_group["y_dm"][:]
d_z = dm_group["z_dm"][:]
dmmax = np.min(dm) * m2Msun
import dm_utils as dm_utils
omega_0,lambda_0,little_h = dm_utils.jeje_utils.read_cosmo_params(RunDir,timestep_final)
omega_b = dm_utils.jeje_utils.get_param_real(RunDir,timestep_final,"omega_b")
fb = omega_b / omega_0
msph = dmmax * fb / (1-fb)

print "done, building chain"

# Build linked-list of cells (for neighbour finding later)
cxyz_min = min(cx.min(),cy.min(),cz.min())
cxyz_max = max(cx.max(),cy.max(),cz.max())

cx_rescale = (cx - cxyz_min) / (cxyz_max-cxyz_min)
cy_rescale = (cy - cxyz_min) / (cxyz_max-cxyz_min)
cz_rescale = (cz - cxyz_min) / (cxyz_max-cxyz_min)

nchain_cells = 20
nextincell,firstincell = nu.neighbour_utils.init_chain(cx_rescale,cy_rescale,cz_rescale,nchain_cells)

print "done, selecting cells for halo"

# Factor to help convert into box units (note maxime's merger_tree reader has converted halo positions so be careful here!)
r2kpc_ideal, redshift, little_h, m2Msun,junk,junk = ur.Set_Units(RunDir, timestep, "ideal")
boxlen_cMpc = dm_utils.jeje_utils.get_param_real(RunDir,timestep,'boxlen') # Box size in box units
boxlen_cMpc *= r2kpc_ideal
boxlen_cMpc = boxlen_cMpc * (1+redshift) /1e3 # cMpc (comoving)
boxlen_cMpc = boxlen_cMpc * little_h # h^-1 cMpc
factor = 1./boxlen_cMpc

# Loop over final timestep haloes
for halo_group_str in zoom_group:
    # Only write maps for main progenitors of the main halo
    if "_main" in halo_group_str:
        break

halo_group = zoom_group[halo_group_str]
mp_group = zoom_group[halo_group_str+"/main_progenitors"]

# Identify the main progenitor at this timestep
ok_ts = np.array(mp_group["halo_ts"])==timestep_tree # haloes on this timestep

xh = mp_group["x"][ok_ts] * factor + 0.5
yh = mp_group["y"][ok_ts] * factor + 0.5
zh = mp_group["z"][ok_ts] * factor + 0.5


r_dm = np.sqrt(np.square(xh-d_x) + np.square(yh-d_y) + np.square(zh-d_z))

# Use r200 for central haloes, and rvir (for now) for satellite haloes
if mp_group["host_halo_id"][ok_ts] == mp_group["halo_id"][ok_ts]:
    rvirh = mp_group["r200"][ok_ts] / r2kpc
else:
    rvirh = mp_group["rvir"][ok_ts] * factor

ok_final = np.array(mp_group["halo_ts"])==timestep_tree_final        
mhalo_final = mp_group["mvir"][ok_final][:] * 1e11 # Halo mass in msun

vmax_phase = vmax_phase_f(mhalo_final, phase_hi_norm)

########## Select cells in the vicinity of the halo (using a chain algorithm) ################
rmax = rvirh

# Rescale halo positions for efficient cell division in chain algorithm
xh_rescale = (xh-cxyz_min)/(cxyz_max-cxyz_min)
yh_rescale = (yh-cxyz_min)/(cxyz_max-cxyz_min)
zh_rescale = (zh-cxyz_min)/(cxyz_max-cxyz_min)
rmax_rescale = rmax/(cxyz_max-cxyz_min)

ncell_in_rmax = nu.neighbour_utils.get_npart_in_sphere_with_chain(RunDir,timestep,cx_rescale,cy_rescale,cz_rescale,xh_rescale,yh_rescale,zh_rescale,rmax_rescale,firstincell,nextincell)
if ncell_in_rmax > 0:
    select = nu.neighbour_utils.get_part_indicies_in_sphere_with_chain(RunDir,timestep,cx_rescale,cy_rescale,cz_rescale,xh_rescale,yh_rescale,zh_rescale,rmax_rescale,ncell_in_rmax,firstincell,nextincell)
else:
    select = np.zeros_like(cx) < 0

cx_h = cx[select]; cy_h = cy[select]; cz_h = cz[select]
rho_snap_h = rho_snap[select]; pre_snap_h = pre_snap[select]
clev_h = clev[select]; c_component_index_h = c_component_index[select]
lmjt_h = lmjt[select]

xH1_h = xH1[select]
xHe1_h = xHe1[select]
xHe2_h = xHe2[select]

#print "number of cells/dm within rvir", len(rho_snap_h)
#print "ndm", len(r_dm[r_dm<rvirh])
#exit()


print "done, computing cell properties"

# Set cell unit conversions for this timestep
cut.jeje_utils.read_conversion_scales(RunDir,timestep)

# Compute radial velocities (w.r.t. halo centre)
cr_h = np.sqrt(np.square(cx_h-xh)+np.square(cy_h-yh)+np.square(cz_h-zh))


# Compute number density in atoms per cc
nh_h = rho_snap_h * cut.jeje_utils.dp_scale_d / cut.jeje_utils.mh * cut.jeje_utils.xh

# Compute temperature over effective molecular weight in Kelvin
scale_temk = (cut.jeje_utils.dp_scale_l / cut.jeje_utils.dp_scale_t)**2 * cut.jeje_utils.mh / cut.jeje_utils.kb
temp_mu_h = pre_snap_h/rho_snap_h * scale_temk # T/mu in K

print "computing neutral fraction"

# Compute neutral gas number density in atoms per cc and the temperature in Kelvin
xhi_h = (1-xH1_h)# Hydrogen neutral fraction
X_frac=0.76
mu_h = 1./( X_frac*(1.+xH1_h) + 0.25*(1.-X_frac)*(1.+xHe1_h+2.*xHe2_h) )

temp_h = temp_mu_h * mu_h # K

print "computing cell mass"

# Compute cell mass
csize = boxlen_pkpc / np.power(2.,clev_h) # proper kpc
cmass = rho_snap_h * rho2msunpkpc3 * csize**3

test = (cr_h > rvirh * 0.5) & (rvirh < cr_h * 0.6)
print "mass-weighed average cell size at r = 0.55 rvir is", np.sum(csize * cmass) / np.sum(cmass)
print "volume-weighed average cell size at r = 0.55 rvir is", np.sum(csize**4) / np.sum(csize**3)

print "computing sound speed"

# Compute sound speed
kb = 1.38064852e-23 # m2 kg s-2 K-1
mh = 1.6726e-27 # kg
kms = 1e3
gamma = 5/3.0 # adiabatic index for monatomic ideal gas
c_sound = np.sqrt(gamma*kb*temp_mu_h/mh)/kms # kms

print "computing jeans length"

# Compute thermal jeans length
G = 6.67408e-11 # m3 kg-1 s-2
pc = 3.0857e16 # m
kpc = 1e3 * pc
Msun = 1.989e30 #kg
lambda_J = np.sqrt(np.pi * np.square(c_sound*kms) /(G*rho_snap_h*rho2msunpkpc3*Msun/kpc**3)) / kpc # kpc

J_factor = csize / lambda_J
fragment = J_factor > 0.25 # Truelove 97, gas in these regime will artificially fragment because the thermal Jeans length is not resolved
#fragment = J_factor > 1

# Compute turbulent jeans length
lambda_J_turb = lmjt_h * r2kpc # kpc
fragment_turb_1 = csize/lambda_J_turb > 1
fragment_turb_4 = csize/lambda_J_turb > 0.25



'''print "printing jeans length for paper"
print "density = 10 cm-3, temp = 100 K, range of mu/h"

ok = (nh_h > 8) & (nh_h < 12) & ( temp_h > 80) & (temp_h < 120)
print lambda_J[ok] * 1e3 
print np.median(lambda_J[ok] * 1e3)
exit()'''

print "done, creating figure"

######## Compute phase diagram maps ###################

py.rcParams.update(latexParams)
py.rcParams.update({'figure.subplot.wspace':0,'figure.subplot.hspace':0,
                    'figure.figsize':[3.32,2.49],
                    'figure.subplot.left':0.12,
                    'figure.subplot.bottom':0.14,
                    'figure.subplot.top':1.0,
                    'figure.subplot.right':0.98,
                    'font.size':8,
                    'axes.labelsize':8,
                    'legend.fontsize':7})

py.figure()
fig_phase=py.gcf()

ax_p = py.subplot(1,1,1)
image_p = ax_p.imshow(np.zeros((2,2)),interpolation='nearest',origin='lower') # Instantiate imshow object with dummy data


######### Radius/radial velocity cuts #############

# Radial cut
ok = (cr_h > rvirh * r_inner_aperture) & (cr_h < rvirh * r_outer_aperture)

# Radial velocity selection
#ok = ok & (cv_radial_h > vrad_lo_phase_list[n]) & (cv_radial_h < vrad_hi_phase_list[n])

# Subhalo cut
if exc_sub_phase == 0: # Exclude substructures
    ok = ok & ((c_component_index_h==0)|(c_component_index_h==3))
elif exc_sub_phase ==1: # Only include substructures
    ok = ok & ((c_component_index_h > 0)|(c_component_index_h==3))

# Choose mh (hydrogren mass), mHI (neutral hydrogen mass) or mHII (ionised hydrogen mass) weighting
if phase_weights == "mh":
    phase_weight = cmass # cell hydrogen mass
elif phase_weights == "mhi":
    phase_weight = xhi_h * cmass # neutral hydrogen mass
elif phase_weights == "mhii":
    phase_weight = (1-xhi_h) * cmass # ionised hydrogen mass
else:
    print "Error: write_maps_tree_main_memsafe, phase map weighting scheme = ", phase_weights, " is not recognised"
    quit()

phase_nh_lo = nh_h[ok].min()
phase_nh_hi = nh_h[ok].max()
phase_temp_lo = temp_h[ok].min()
phase_temp_hi = temp_h[ok].max()

ranges = [[np.log10(phase_nh_lo),np.log10(phase_nh_hi)],[np.log10(phase_temp_lo),np.log10(phase_temp_hi)]]
H_p, xedges_p, yedges_p = np.histogram2d(np.log10(nh_h[ok]),np.log10(temp_h[ok]),weights=phase_weight[ok],bins=[nbins_phase,nbins_phase],range=ranges)

py.axes(ax_p)
image_p.set_norm(LogNorm())
image_p.set_extent((np.log10(phase_nh_lo),np.log10(phase_nh_hi),np.log10(phase_temp_lo),np.log10(phase_temp_hi)))
#image_p.norm.vmin = vmin_phase
#image_p.norm.vmax = vmax_phase
image_p.norm.vmin = H_p[H_p>0].min()
image_p.norm.vmax = H_p.max()

phase_ymin = 10 # Kelvin / molecular weight
phase_ymax = 10**8.5 # Kelvin / molecular weight
phase_xmin = 1e-6 # Atoms per cc
phase_xmax = 1e4 # Atoms per cc


ax_p.set_xlim((np.log10(phase_xmin),np.log10(phase_xmax)))
ax_p.set_ylim((np.log10(phase_ymin),np.log10(phase_ymax)))
image_p.set_data(H_p.T)
image_p.set_cmap(cmap_phase)
py.xlabel(r'$\log(n_{\mathrm{H}} \, /\mathrm{cm^{-3}})$')
py.ylabel(r'$\log(T \, /\mathrm{K})$')

# Plot phase diagram lines if desired
if plot_phase_lines:
    '''py.axhline(phase_logT1,c="k")
    py.axhline(phase_logT2,c="k")
    py.axhline(phase_logT3,c="k")
    phase_eqn = lambda x,m,c: m*x+c
    py.plot([-7,-1.32],[phase_eqn(-7,phase_m,phase_c),phase_eqn(-1.32,phase_m,phase_c)],c="k")'''

    # Plot neutral gas box
    py.plot([np.log10(0.03),np.log10(0.03)], [-5, 5], linestyle='--', c="k")
    py.plot([np.log10(0.03),20], [5, 5], linestyle='--', c="k")
     
if plot_art_frag:
    H_p, xedges_p, yedges_p = np.histogram2d(np.log10(nh_h[ok&fragment]),np.log10(temp_h[ok&fragment]),weights=phase_weight[ok&fragment],bins=[nbins_phase,nbins_phase],range=ranges)
    H_p2, xedges_p2, yedges_p2 = np.histogram2d(np.log10(nh_h[ok&fragment_turb_1]),np.log10(temp_h[ok&fragment_turb_1]),weights=phase_weight[ok&fragment_turb_1],bins=[nbins_phase,nbins_phase],range=ranges)
    
    #py.contour(xx,yy,H_p,levels=(-1,0,1))
    extent = [np.log10(phase_nh_lo),np.log10(phase_nh_hi),np.log10(phase_temp_lo),np.log10(phase_temp_hi)]
    con = py.contour(H_p.transpose(),levels=(-1,0,1),extent=extent,colors="k",linewidths=(0.5),alpha=0.5)
    con2 = py.contour(H_p2.transpose(),levels=(-1,0,1),extent=extent,colors="b",linewidths=(0.5),alpha=0.5)
    
    print "mass fraction in fragmenting gas is", np.sum(cmass[ok&fragment])/np.sum(cmass[ok])
    print "mass fraction in turbulent fragmenting gas w.r.t 1 cell length is", np.sum(cmass[ok&fragment_turb_1])/np.sum(cmass[ok])
    print "mass fraction in turbulent fragmenting gas w.r.t 4 cell lengths is", np.sum(cmass[ok&fragment_turb_4])/np.sum(cmass[ok])
    print "mhalo_final", np.log10(mhalo_final)
    #print np.sum(cmass[ok&(nh_h>1)])/np.sum(cmass[ok])
    #print np.sum(cmass[ok&(temp_h>10**3.8)&(temp_h<10**4.25)])/np.sum(cmass[ok])

    '''py.figure()
    bins = np.arange(-6,6,12/100.0)
    py.hist(np.log10(nh_h[ok]),bins=bins,weights=phase_weight[ok],histtype="step",color="k")
    py.hist(np.log10(nh_h[ok&fragment]),bins=bins,weights=phase_weight[ok&fragment],histtype="step",color="r")

    py.figure()
    py.hist(np.log10(cmass[ok]),bins=100,histtype="step",color="g",weights=cmass[ok])
    py.axvline(np.log10(msph*8))
    py.xlabel(r"$\log(m_{\mathrm{cell}} \, / \mathrm{M_\odot})$")
    print "fraction of mass in cells with mass > 8 msph is", np.sum(cmass[ok&(cmass>8*msph)])/np.sum(cmass[ok])

    py.show()
    exit()'''

fig_path = "Figures/"+phase_map_name+"_"+Zoom+"_ts_"+str(timestep)+'.pdf'
py.savefig(fig_path)
py.show()

subvol_File.close()
File.close()
