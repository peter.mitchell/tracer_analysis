import numpy as np
import h5py
import utilities_cosmology as uc
import dm_utils as dm

note that c_component_index (i.e. comp) got changed - so this script would needto be updated before itcan be used

#np.random.seed(23231)
#np.random.seed(512312)
#np.random.seed(174283)
#np.random.seed(13426743)
np.random.seed(123612765)
i_part = 0

# 619 was the cool example of outflow from ISM scales John was interested in

OutDir = "/cral2/mitchell/PostProcess/"

#File = h5py.File(OutDir+"save_5092_backup.hdf5")
Run = "RhdRun-tracer_radfix"
File = h5py.File(OutDir+"saved_tracers_select_"+Zoom+"_"+Run+".hdf5")

tid_select = File["tid"][:]
comp       = File["comp"][:]
cv         = File["cv"][:]
nh         = File["nh"][:]
temp       = File["temp"][:]
xhi        = File["xhi"][:]
radius = File["r"][:]
timestep = File["timestep"][:]
family = File["family"][:]
radius_p = File["r_physical"][:]
radius_norm = File["r"][:]
redshift = File["redshift"][:]

File.close()
n_select = len(tid_select)

# Get time from redshift
Zoom = "Zoom-7-1290"; Run = "tracer_run_lr"; RunDir = "/cral4/mitchell/data/Tracer_Test/"+ Zoom + "/" + Run
dm.jeje_utils.read_conversion_scales(RunDir,timestep[0])
omega_0,lambda_0,little_h = dm.jeje_utils.read_cosmo_params(RunDir,timestep[0])
a = 1./(1.+redshift)
t, tlb = uc.t_Universe(a,omega_0,little_h)

# Get time-averaged radial velocity
dr = (radius_p[1:] - radius_p[0:-1]) * (3.09*10**16) # km
dt = (t[1:]-t[0:-1]) * 3.155*10**16 # s

t_av = 0.5 * (t[0:-1]+t[1:])
cv_av = dr/np.reshape(dt,(len(dt),1))

############ True selection ################
parts_show = np.random.randint(0,n_select,10)

############ Custom selections ###############
family_temp = np.copy(family)
family_temp[np.isnan(family_temp)] = 100

comp_temp = np.copy(comp)
comp_temp[np.isnan(comp_temp)] = 0

# Count consecutive number of outputs a particle is registered as a satellite
nsat_max = np.zeros(n_select)
nsat = np.zeros(n_select)
for n in range(len(comp_temp[:,0])):
    nsat[comp_temp[n]>0] +=1
    nsat[comp_temp[n]==0] = 0    
    nsat_max[nsat>nsat_max] = nsat[nsat>nsat_max]
nsat_final = nsat
    
cv_temp = np.copy(cv)
cv_temp[np.isnan(cv_temp)] = 0

rad_temp = np.copy(radius_norm)
rad_temp[np.isnan(rad_temp)] = 1e9

jim = np.max(comp_temp,axis=0)
tim = np.max(family_temp,axis=0)
rmin = np.min(rad_temp,axis=0)
vrmax = np.max(cv_temp,axis=0)

# Count particles that get ejected from the halo, and number of those that return
accreted = np.zeros(n_select)
ejected = np.zeros(n_select)
nej = np.zeros(n_select)
nret = np.zeros(n_select)-1
for n in range(len(comp_temp[:,0])):
    accreted[rad_temp[n]<0.9] = 1
    ejected = (accreted==1) & (rad_temp[n]>1)
    accreted[ejected] = 0

    nej[ejected] += 1
    if n > 0:
        nret[(rad_temp[n]<0.9) & (rad_temp[n-1]>0.9)] +=1
    else:
        nret[(rad_temp[n]<0.9)] += 1


    
nsat_thr = 2
print "frac that was ever in sat", len(jim[nsat_max>nsat_thr]) / float(len(jim))
print "frac that was in sat, and ISM", len(jim[(nsat_max>nsat_thr) & (rmin<0.2)])/ float(len(jim))
print "frac that was in sat, and ISM at the end", len(jim[(nsat_max>nsat_thr) & (rad_temp[-1]<0.2)])/ float(len(jim))
print "frac that was in sat (as gas) at end", len(jim[(nsat_max>nsat_thr)&(rad_temp[-1]<0.2)&(tim==100)]) / float(len(jim))

print ""
print "frac that become stars", len(tim[tim>100]) / float(len(tim))
print "frac in stars in satellites at the end", len(tim[(tim>100) & (nsat_final>nsat_thr)]) / float(len(jim))
print "frac in stars in ISM at the end", len(tim[(tim>100)&(rad_temp[-1]<0.2)]) / float(len(jim))
print "frac of stars that have ever been in a satellite", len(tim[(tim>100)&(nsat_max>nsat_thr)])/float(len(jim))

print ""
print "frac that never become stars and are never in a sat", len(jim[(nsat_max<=nsat_thr)&(tim==100)]) / float(len(tim))

print ""
print "frac in ISM (as gas) by the end", len(jim[(rad_temp[-1]<0.2)&(tim==100)]) / float(len(jim))
                                        

print ""
print "frac that are ejected at the end", len(radius_p[-1][np.isnan(radius_p[-1])]) / float(len(radius_p[-1]))
print "frac that ever get ejected", len(jim[nej>0]) / float(len(jim))
print "frac that ever get ejected and got to < 0.75 rvir (possibly after returning)", len(jim[(nej>0)&(rmin<0.75)]) / float(len(jim))
print "frac that ever get ejected, got to < 0.75 rvir, and returned at least once", len(jim[(nej>0)&(rmin<0.75)&(nret>0)]) / float(len(jim))
print "frac that were ever in an outflow", len(jim[(vrmax>150)&(tim==100)]) / float(len(radius_p[-1]))

print ""
print "frac that is in the CGM at the end", len(jim[(rad_temp[-1]>0.2)&(rad_temp[-1]<1.0)&(tim==100)&(nsat_final<=nsat_thr)]) / float(len(jim))
print "frac in the CGM at the end that was in a a satellite previously", len(jim[(rad_temp[-1]>0.2)&(rad_temp[-1]<1.0)&(tim==100)&(nsat_final<=nsat_thr)&(nsat_max>nsat_thr)]) / float(len(jim))
print "frac in the CGM at the end, was in a satellite previously, and was in the ISM previously", len(jim[(rad_temp[-1]>0.2)&(rad_temp[-1]<1.0)&(tim==100)&(comp_temp[-1]==0)&(nsat_max>nsat_thr)&(rmin<0.2)]) / float(len(jim))
print "frac in the CGM at the end, NOT in sat before, but was in ISM before", len(jim[(rad_temp[-1]>0.2)&(rad_temp[-1]<1.0)&(tim==100)&(nsat_max<=nsat_thr)&(rmin<0.2)]) / float(len(jim))
print "frac in the CGM at the end, NOT in sat before, and was never in the ISM", len(jim[(rad_temp[-1]>0.2)&(rad_temp[-1]<1.0)&(tim==100)&(nsat_max<=nsat_thr)&(rmin>0.2)]) / float(len(jim))
print "frac in the CGM at the end, that has been an outflow at some point", len(jim[(rad_temp[-1]>0.2)&(rad_temp[-1]<1.0)&(tim==100)&(nsat_final<=nsat_thr)&(vrmax>150)]) / float(len(jim))


print ""
print "preliminary breakdown of ts final CGM"





# Identify stripped particles that have not been in the ISM (or back into satellite) after being stripped
nsat = np.zeros(n_select)
stripped = np.zeros(n_select)
stripped2 = np.zeros(n_select)
nism = np.zeros(n_select)
was_in_ism = np.zeros(n_select)

ninflow = np.zeros(n_select)
noutflow = np.zeros(n_select)
mono_inflow = np.zeros(n_select)

for n in range(len(comp_temp[:,0])):
    nsat[comp_temp[n]>0] +=1
    
    in_sat = nsat>2
    stripped[in_sat] = 0
    stripped2[in_sat] = 0
    
    stripped[in_sat & (comp_temp[n]==0)] += 1
    stripped2[in_sat & (comp_temp[n]==0)] += 1
    nsat[comp_temp[n]==0] = 0

    nism[rad_temp[n]<0.2] +=1
    nism[rad_temp[n]>0.2] = 0
    stripped[nism>1] = 0

    was_in_ism[nism==2] += 1

    ninflow[cv_temp[n] < 0] += 1
    ninflow[cv_temp[n]>0] = 0
    noutflow[cv_temp[n] > 0] += 1
    noutflow[cv_temp[n]<0] = 0

    mono_inflow[ninflow>2] = 1
    mono_inflow[noutflow>2] = 0
    
    test = 69
    print comp[n,test], nsat[test], stripped[test], nism[test], was_in_ism[test], radius[n,test]


cgm_final = (rad_temp[-1]>0.2)&(rad_temp[-1]<1.0) & (tim==100)&(nsat_final<=nsat_thr)
print "fraction of final CGM that has been stripped from satellites", len(jim[(stripped>0) & cgm_final]) / float(len(jim[cgm_final]))
print "fraction of final CGM that came from the central ISM", len(jim[cgm_final&(was_in_ism>0)]) / float(len(jim[cgm_final]))
print "fraction of final CGM that is diffuse, and inflowing for first time", len(jim[cgm_final&(mono_inflow==1)&(was_in_ism==0)&(stripped==0)]) / float(len(jim[cgm_final]))

a = (stripped>0) & cgm_final
b = cgm_final&(was_in_ism>0)
c = cgm_final&(mono_inflow==1)&(was_in_ism==0)&(stripped==0)
d = cgm_final & (a==False) & (b==False) & (c==False)

print np.where(d)

print len(a[a])
print len(b[b])
print len(c[c])
print len(d[d])

#quit()

print "Outflow selection"
#parts_show = np.arange(0,n_select)[(np.max(family_temp,axis=0)==100) & (np.max(comp_temp,axis=0)==0) & (np.max(cv_temp,axis=0)>200)]

# relax criteria that they have never been in a satellite
#parts_show = np.arange(0,n_select)[(np.max(family_temp,axis=0)==100) & (np.max(cv_temp,axis=0)>100)]

#parts_show = np.arange(0,n_select)[(np.max(family_temp,axis=0)==100) & (np.max(cv_temp,axis=0)>250) &(rmin<0.05)]

# Trying to figure out neutral in > out being same as neutral out > in (with former being slightly larger!)

#ts_sel = 17
#parts_show = np.arange(0,n_select)[(rad_temp[-ts_sel]>0.5) & (rad_temp[-ts_sel]<0.6) & (cv_temp[-ts_sel]<0) & (cv_temp[-ts_sel+1]>0)]
#parts_show = np.arange(0,n_select)[(rad_temp[-ts_sel]>0.5) & (rad_temp[-ts_sel]<0.6) & (cv_temp[-ts_sel]>0) & (cv_temp[-ts_sel+1]<0)]

#parts_show = np.arange(0,n_select)

ok = np.arange(0,n_select) == False
ts_sel = np.zeros(len(ok)).astype("int")


for n in range(len(comp_temp[:,0])):
    ok2 = (rad_temp[n]>0.4) & (rad_temp[n]<0.5) & (cv_temp[n] > 0) & (xhi[n] > 0.3) & (comp_temp[n]==0)
    ok = ok | ok2
    ts_sel[ok&(ts_sel==0)] = n

parts_show = np.arange(0,n_select)[ok]

np.random.shuffle(parts_show)

print "n parts in selection is", len(parts_show)
i_part = 50

ishow = parts_show[i_part]
print "this particle is", parts_show[i_part], tid_select[i_part]
print ishow
print i_part, "family, component", tid_select[ishow]
print family[:,ishow]
print comp[:,ishow] # note comp 0 = bound central, comp 1 = bound low-mass satellite, comp 2 = bound higher-mass satellite

#print "Hello", np.argmin(rad_temp[:,ishow])
#exit()

ts_sel_show = ts_sel[ishow]

from utilities_plotting import *

c_list = ["k","b","m","r","g","y","c","firebrick","darkgray","orange"]

py.figure(figsize=(20,10))
py.subplot(321)

py.axvline(t[ts_sel_show])

for i_part in range(1):
    py.plot(t, radius[:,ishow], c=c_list[i_part])
    py.scatter(t, radius[:,ishow], c=c_list[i_part],edgecolors="none")

    if 1 in comp[:,ishow] or 2 in comp[:,ishow]:
        py.plot(t, comp[:,ishow]*0.1, c=c_list[i_part],linestyle=":")

    if 101 in family[:,ishow]:
        py.plot(t, (t*0+0.9)*(family[:,ishow]-100), c=c_list[i_part],linestyle="--")

py.xlabel("t")
py.ylabel(r"$r \, / R_{\mathrm{vir}}$")
py.xlim((t.min(), t.max()))
py.ylim((0.0,1.0))

py.subplot(322)
for i_part in range(1):
    py.plot(t, np.log10(nh[:,ishow]), c=c_list[i_part])
    py.scatter(t, np.log10(nh[:,ishow]), c=c_list[i_part],edgecolors="none")
py.ylabel(r"$\log(n_{\mathrm{H}})$")
py.xlim((t.min(), t.max()))
py.axvline(t[ts_sel_show])

py.subplot(323)
for i_part in range(1):
    py.plot(t, np.log10(temp[:,ishow]), c=c_list[i_part])
    py.scatter(t, np.log10(temp[:,ishow]), c=c_list[i_part],edgecolors="none")
py.ylabel(r"$\log(T)$")
py.xlim((t.min(), t.max()))
py.axvline(t[ts_sel_show])

py.subplot(324)
for i_part in range(1):
    py.plot(t, xhi[:,ishow], c=c_list[i_part])
    py.scatter(t, xhi[:,ishow], c=c_list[i_part],edgecolors="none")
py.ylabel(r"$x_{\mathrm{HI}}$")
py.ylim((-0.05,1.05))
py.xlim((t.min(), t.max()))
py.axvline(t[ts_sel_show])

py.subplot(325)
for i_part in range(1):
    py.plot(t, cv[:,ishow], c=c_list[i_part])
    py.scatter(t, cv[:,ishow], c=c_list[i_part],edgecolors="none")
    py.plot(t_av, cv_av[:,ishow], c=c_list[i_part],linestyle=':')
    py.axhline(0.0)
py.axvline(t[ts_sel_show])
    
py.ylabel(r"$v_{\mathrm{rad}}$")

py.xlim((t.min(), t.max()))
py.show()



quit()




py.figure(figsize=(20,10))
py.subplot(321)

py.plot(radius[61:,ishow], cv[61:,ishow], c=c_list[i_part])
py.scatter(radius[61:,ishow], cv[61:,ishow], c=c_list[i_part],edgecolors="none")

py.ylabel(r"v_rad")
py.xlabel(r"$r \, / R_{\mathrm{vir}}$")
#py.xlim((t.min(), t.max()))
#py.ylim((0.0,1.0))

py.subplot(322)

py.plot(radius[61:,ishow], np.log10(nh[61:,ishow]), c=c_list[i_part])
py.scatter(radius[61:,ishow], np.log10(nh[61:,ishow]), c=c_list[i_part],edgecolors="none")


py.ylabel(r"$\log10(n_h)$")
py.xlabel(r"$r \, / R_{\mathrm{vir}}$")

#minimum density join can still detect absorption
py.axhline( -2 )

py.subplot(323)

py.plot(radius[61:,ishow], np.log10(temp[61:,ishow]), c=c_list[i_part])
py.scatter(radius[61:,ishow], np.log10(temp[61:,ishow]), c=c_list[i_part],edgecolors="none")

py.ylabel(r"$\log10(T)$")
py.xlabel(r"$r \, / R_{\mathrm{vir}}$")

py.subplot(324)

py.plot(radius_p[61:,ishow], cv[61:,ishow], c=c_list[i_part])
py.scatter(radius_p[61:,ishow], cv[61:,ishow], c=c_list[i_part],edgecolors="none")

py.ylabel(r"v_rad")
py.xlabel(r"$r \, / pkpc$")
#py.xlim((t.min(), t.max()))
#py.ylim((0.0,1.0))


py.show()
