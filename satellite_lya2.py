import sys
import numpy as np
import dm_utils as dm
import cutils as cut
import h5py
import utilities_ramses as ur
import os

z_choose = 3.5

Zoom = "Zoom-7-5092"
Run = "RhdRun-tracer_radfix"
timestep_final = 155
RunDir = "/cral2/mitchell/"+Zoom+"/"+Run
InDir = "/cral2/mitchell/PostProcess/"
OutDir = "/cral2/mitchell/PostProcess/"
HaloDir = RunDir + "/Halos/"

filename = Zoom + "_" + Run + "_tsf" + str(timestep_final) +".hdf5"
File = h5py.File(OutDir+filename,"r")

# Open the subvolume file (containing all cell data)
subvol_filename = "subvol_"+filename
subvol_File = h5py.File(OutDir+subvol_filename,"r")


zoom_group = File[Zoom]

for thing in zoom_group:
    if "main" in thing:
        mp_group = zoom_group[thing+"/main_progenitors"]
        ap_group = zoom_group[thing+"/all_progenitors"]

timesteps_tree = mp_group["halo_ts"][:]
timesteps = timesteps_tree +1

redshifts = np.zeros_like(timesteps)

for i, timestep in enumerate(timesteps):
    r2kpc, redshift, little_h, m2Msun,rho2msunpkpc3,v2kms = ur.Set_Units(RunDir, timestep)
    redshifts[i] = redshift

i_choose = np.argmin(abs(redshifts-z_choose))        
timestep = timesteps[i_choose]
redshift = redshifts[i_choose]
timestep_tree = timestep -1

########## Read info for main halo #####################
ok_ts = np.array(mp_group["halo_ts"])==timestep_tree # haloes on this timestep

# Get conversions for different units
r2kpc, redshift, little_h, m2Msun,rho2msunpkpc3, v2kms = ur.Set_Units(RunDir, timestep)
boxlen_pkpc = dm.jeje_utils.get_param_real(RunDir,timestep,'boxlen') * r2kpc # Box size in proper kpc, using the correct box size

r2kpc_ideal, redshift, little_h, m2Msun,junk,junk = ur.Set_Units(RunDir, timestep, "ideal")
boxlen_cMpc = dm.jeje_utils.get_param_real(RunDir,timestep,'boxlen') # Box size in box units
boxlen_cMpc *= r2kpc_ideal
boxlen_cMpc = boxlen_cMpc * (1+redshift) /1e3 # cMpc (comoving)
boxlen_cMpc = boxlen_cMpc * little_h # h^-1 cMpc
factor = 1./boxlen_cMpc

xh = mp_group["x"][ok_ts] * factor + 0.5
yh = mp_group["y"][ok_ts] * factor + 0.5
zh = mp_group["z"][ok_ts] * factor + 0.5
rvirh = mp_group["r200"][ok_ts] / r2kpc

halo_num = mp_group["halo_num"][ok_ts]


########## Read cell/particle data #########################        
print "Reading particle/cell data"
# Hdf5 group for this timestep (contains cell data)
ts_group = subvol_File["timestep_"+str(int(timestep))]

cell_group = ts_group["Gas_leaf_cell_data"]

rho_snap = cell_group["rho_c"][:] # Gas density
pre_snap = cell_group["pre_c"][:] # Gas pressure
cx = cell_group["x_c"][:] # Cell coordinates
cy = cell_group["y_c"][:]
cz = cell_group["z_c"][:]
lev = cell_group["lev"][:] # Cell refinement level
xH1 = cell_group["xH1"][:]
xHe1 = cell_group["xHe1"][:]
xHe2 = cell_group["xHe2"][:]

subvol_File.close()
File.close()

file_temp = h5py.File("temp_dump.hdf5","r")
in_sat = file_temp["in_sat"][:]
index_sat = file_temp["index_sat"][:]
logmstar_sat = file_temp["logmstar_sat"][:]
file_temp.close()

print "Done: performing unit conversions"

cx -= xh
cy -= yh
cz -= zh
cr = np.sqrt(np.square(cx) + np.square(cy) + np.square(cz))
cr *= 1./rvirh
print np.sort(cr), "here"

cr_proj = np.sqrt(np.square(cx) + np.square(cy))
cr_proj *= 1./rvirh

cut.jeje_utils.read_conversion_scales(RunDir,timestep)

nh = rho_snap * cut.jeje_utils.dp_scale_d / cut.jeje_utils.mh * cut.jeje_utils.xh # Number density: Atoms cm^-3

scale_temk = (cut.jeje_utils.dp_scale_l / cut.jeje_utils.dp_scale_t)**2 * cut.jeje_utils.mh / cut.jeje_utils.kb
temp_mu = pre_snap/rho_snap * scale_temk # T/mu in K

# Compute cell size using amr refinement level and box size
csize = boxlen_pkpc / np.power(2.,lev) # proper kpc
cmass = rho_snap * rho2msunpkpc3 * csize**3 # Msun - gas mass in each cell
cell_vol = np.power(csize,3) # kpc3
cell_vol *= (3.086e18*1e3)**3 # cm3 

xhi = (1-xH1)# Hydrogen neutral fraction
X_frac=0.76
mu = 1./( X_frac*(1.+xH1) + 0.25*(1.-X_frac)*(1.+xHe1+2.*xHe2) )

temp = temp_mu * mu

Ta = np.copy(temp)
Ta[Ta<100] = 100 # Put temperature floor
prob_case_B = 0.686 - 0.106*np.log10(Ta/1e4) - 0.009*np.power(Ta/1e4,-0.44)
nHII = xH1*nh # Ionized hydrogen density
nHI = xhi * nh # Neutral hydrogen density
X_frac = 0.76
nHe  = 0.25*nh*(1.0-X_frac)/X_frac
n_e = nHII+nHe*(xHe1+2.*xHe2)

def get_AlphaB_HII(TK):
    lbda = 315614./TK
    return 2.753e-14 * np.power(lbda,1.5) / np.power(1.+np.power(lbda/2.74,0.407),2.242)

def get_collExrate_HI(TK):
    kb = 1.38064852e-16
    return 2.41e-6/np.sqrt(TK) * np.power(TK/1.e4,0.22) * np.exp(-1.63e-11/(kb*TK))

planck  = 6.626070040e-27  # [erg s] Planck's constant
clight  = 2.99792458e10  # cm/s
cmtoA   = 1.0e8 # cm to Angstrom
lambda_0=1215.67e0
lambda_0_cm = lambda_0 / cmtoA          # cm
nu_0 = clight / lambda_0_cm             #  Hz
e_lya = planck*nu_0 # 1.634e-11 # cgs

RecLyaEmi  = prob_case_B * nHII * n_e * get_AlphaB_HII(temp) * e_lya
CollLyaEmi = nHI * n_e * get_collExrate_HI(temp) * e_lya
LyaEmi     = CollLyaEmi + RecLyaEmi

LyaLum = LyaEmi * cell_vol # erg s^-1


print "Binning"

rbins = np.arange(0, 2.05, 0.05)
rmid = 0.5*(rbins[1:] + rbins[0:-1])
dr = 0.05


LyaLum_tot = np.histogram(cr, bins=rbins, weights=LyaLum)[0]
LyaLum_sat = np.histogram(cr[in_sat], bins=rbins, weights=LyaLum[in_sat])[0]
LyaLum_tot_proj = np.histogram(cr_proj, bins=rbins, weights=LyaLum)[0]
LyaLum_sat_proj = np.histogram(cr_proj[in_sat], bins=rbins, weights=LyaLum[in_sat])[0]

mass_tot = np.histogram(cr, bins=rbins, weights=cmass)[0]
mass_sat = np.histogram(cr[in_sat], bins=rbins, weights=cmass[in_sat])[0]

order = np.argsort(index_sat)
LyaLum = LyaLum[order]
index_sat = index_sat[order]
logmstar_sat = logmstar_sat[order]
cr = cr[order]
cr_proj = cr_proj[order]
in_sat = in_sat[order]

junk, ind = np.unique(index_sat, return_index=True)
n_sat = len(junk)-1
LyaLum_ind = []
LyaLum_ind_proj = []
logmstar_sat_ind = []

LyaLum_darksat = np.zeros_like(LyaLum_tot)
LyaLum_darksat_proj = np.zeros_like(LyaLum_tot)

nsat_bright = np.zeros_like(LyaLum_tot)
nsat_dark = np.zeros_like(LyaLum_tot)
nsat_bright_proj = np.zeros_like(LyaLum_tot)
nsat_dark_proj = np.zeros_like(LyaLum_tot)

for n in range(n_sat+1):
    if n == 1:
        continue
    
    ind_i1 = ind[n]
    if n < n_sat:
        ind_i2 = ind[n+1]
    else:
        ind_i2 = -1

    logmstar_sat_n = logmstar_sat[ind_i1]

    if logmstar_sat_n > 0:
        count = np.histogram(cr[ind_i1:ind_i2], bins=rbins, weights=LyaLum[ind_i1:ind_i2])[0]
        LyaLum_ind.append( np.copy(count))
        count[count > 0] =1
        nsat_bright += count
        
        count_proj = np.histogram(cr_proj[ind_i1:ind_i2], bins=rbins, weights=LyaLum[ind_i1:ind_i2])[0] 
        LyaLum_ind_proj.append( np.copy(count_proj) )
        count_proj[count_proj > 0] = 1
        nsat_bright_proj += count_proj
        
        logmstar_sat_ind.append( logmstar_sat[ind_i1] )
    elif in_sat[ind_i1]:
        count =np.histogram(cr[ind_i1:ind_i2], bins=rbins, weights=LyaLum[ind_i1:ind_i2])[0]
        LyaLum_darksat += count
        count[count>0] = 1
        nsat_dark += count
        
        count_proj = np.histogram(cr_proj[ind_i1:ind_i2], bins=rbins, weights=LyaLum[ind_i1:ind_i2])[0]
        LyaLum_darksat_proj += count_proj
        count_proj[count_proj>0] = 1
        nsat_dark_proj += count_proj

print "Number of satellites (projection) in 0.05 Rvir bins for bright"
print nsat_bright_proj
print "Number of satellites (projection) in 0.05 Rvir bins for dark"
print nsat_dark_proj
        
        
n_sat = len(LyaLum_ind)

logmstar_sat_ind = np.array(logmstar_sat_ind)
logmstar_sat_ind_normed = logmstar_sat_ind - logmstar_sat_ind.min()
logmstar_sat_ind_normed *= 1./logmstar_sat_ind_normed.max()

print "Finished, now plotting"

from utilities_plotting import *

'''py.subplot(211)
py.plot(rmid, np.log10(LyaLum_tot),c="k")
py.plot(rmid, np.log10(LyaLum_sat),c="g")
py.plot(rmid, np.log10(LyaLum_darksat),c="b")

ls_list = ["--",":","-."]
i_ls = 0
for n in range(n_sat):
    py.plot(rmid, np.log10(LyaLum_ind[n]), linestyle=ls_list[i_ls])

    i_ls += 1
    if i_ls == 3:
        i_ls = 0

py.subplot(212)'''

py.plot(rmid, np.log10(LyaLum_tot_proj/dr),c="k")
py.plot(rmid, np.log10(LyaLum_sat_proj/dr),c="g")
py.plot(rmid, np.log10(LyaLum_darksat_proj/dr),c="b")

ls_list = ["--",":","-."]
i_ls = 0
for n in range(n_sat):
    py.plot(rmid, np.log10(LyaLum_ind_proj[n]/dr), linestyle=ls_list[i_ls])

    i_ls += 1
    if i_ls == 3:
        i_ls = 0

py.ylim((37.,43.))
        
py.show()
