import sys
sys.path.append("./Pipeline/")
import numpy as np
import dm_utils as dm
import cutils as cut
import h5py
import utilities_ramses as ur
from halotils import py_halo_utils as hutils
import utilities_profiles as up
import neighbour_utils as nu
import pandas as pd
import os

z_choose = 3.5

Zoom = "Zoom-7-5092"
Run = "RhdRun-tracer_radfix"
timestep_final = 155
RunDir = "/cral2/mitchell/"+Zoom+"/"+Run
InDir = "/cral2/mitchell/PostProcess/"
OutDir = "/cral2/mitchell/PostProcess/"
HaloDir = RunDir + "/Halos/"

filename = Zoom + "_" + Run + "_tsf" + str(timestep_final) +".hdf5"
File = h5py.File(OutDir+filename,"r")

# Open the subvolume file (containing all cell data)
subvol_filename = "subvol_"+filename
subvol_File = h5py.File(OutDir+subvol_filename,"r")


zoom_group = File[Zoom]

for thing in zoom_group:
    if "main" in thing:
        mp_group = zoom_group[thing+"/main_progenitors"]
        ap_group = zoom_group[thing+"/all_progenitors"]

timesteps_tree = mp_group["halo_ts"][:]
timesteps = timesteps_tree +1

redshifts = np.zeros_like(timesteps)

for i, timestep in enumerate(timesteps):
    r2kpc, redshift, little_h, m2Msun,rho2msunpkpc3,v2kms = ur.Set_Units(RunDir, timestep)
    redshifts[i] = redshift

i_choose = np.argmin(abs(redshifts-z_choose))        
timestep = timesteps[i_choose]
redshift = redshifts[i_choose]
timestep_tree = timestep -1

########## Read info for main halo #####################
ok_ts = np.array(mp_group["halo_ts"])==timestep_tree # haloes on this timestep

# Get conversions for different units
r2kpc, redshift, little_h, m2Msun,rho2msunpkpc3, v2kms = ur.Set_Units(RunDir, timestep)
boxlen_pkpc = dm.jeje_utils.get_param_real(RunDir,timestep,'boxlen') * r2kpc # Box size in proper kpc, using the correct box size

r2kpc_ideal, redshift, little_h, m2Msun,junk,junk = ur.Set_Units(RunDir, timestep, "ideal")
boxlen_cMpc = dm.jeje_utils.get_param_real(RunDir,timestep,'boxlen') # Box size in box units
boxlen_cMpc *= r2kpc_ideal
boxlen_cMpc = boxlen_cMpc * (1+redshift) /1e3 # cMpc (comoving)
boxlen_cMpc = boxlen_cMpc * little_h # h^-1 cMpc
factor = 1./boxlen_cMpc

xh = mp_group["x"][ok_ts] * factor + 0.5
yh = mp_group["y"][ok_ts] * factor + 0.5
zh = mp_group["z"][ok_ts] * factor + 0.5
rvirh = mp_group["r200"][ok_ts] / r2kpc

halo_num = mp_group["halo_num"][ok_ts]

########## Read info for satellites #####################

# Read in halo catalogue (to find subhalo positions)
treefile = "%s%i/tree_bricks%3.3i"%(HaloDir,timestep,timestep)
nh,ns = hutils.get_nb_halos(treefile) # nhalo, nsubhalo
halos = hutils.read_all_halos(treefile,nh+ns)
halo_keys = ('nbpart','hnum','ts','level',
             'host', 'hostsub', 'nbsub', 'nextsub', 'mass',
             'x', 'y', 'z', 'vx', 'vy', 'vz', 'Lx', 'Ly', 'Lz', 'r',
             'a', 'b', 'c', 'ek', 'ep', 'et',
             'spin', 'rvir', 'mvir', 'tvir', 'cvel', 'rho0', 'r_c')
halos = pd.DataFrame(columns=halo_keys,data=halos)

subhalo_host_num_list = np.array(halos.host)
subhalo_num_list = np.array(halos.hnum)
subhalo_rvir_list = np.array(halos.rvir)
subhalo_x_list = np.array(halos.x)
subhalo_y_list = np.array(halos.y)
subhalo_z_list = np.array(halos.z)
subhalo_nbpart_list = np.array(halos.nbpart)

factor_subhalo = 3.08e24 / dm.jeje_utils.dp_scale_l # Conversion to code units for the Halo finder unit system (as opposed to the tree unit system)

# Find satellites of the main halo
is_subhalo = (halo_num == subhalo_host_num_list) & (halo_num != subhalo_num_list)
sat = halo_num != subhalo_num_list
n_sat = len(subhalo_host_num_list[sat])

subhalo_x_list = subhalo_x_list * factor_subhalo + 0.5
subhalo_y_list = subhalo_y_list * factor_subhalo + 0.5
subhalo_z_list = subhalo_z_list * factor_subhalo + 0.5

r_subhalo = np.sqrt(np.square(subhalo_x_list-xh) + np.square(subhalo_y_list-yh) + np.square(subhalo_z_list-zh)) / rvirh

'''mdm_highres = 12862.35870899 # Mass of high-res DM particles

# Estimate of subhalo masses
m_sub = subhalo_nbpart_list[is_subhalo] * mdm_highres

bin_mh = np.arange(5, 11.1, 0.1)
bin_mh_mid = 0.5 * (bin_mh[1:] + bin_mh[0:-1])
count = np.histogram(np.log10(m_sub), bins=bin_mh)[0]

from utilities_plotting import *

#py.plot(bin_mh_mid, np.log10(count))
#py.scatter(bin_mh_mid, np.log10(count))
#py.show()

order = np.argsort(m_sub)[::-1]
norm = np.sum(m_sub)
py.plot(np.log10(m_sub)[order], np.cumsum(m_sub[order])/ norm)
py.show()'''



'''r_bin = np.arange(0,2.0,0.05)
r_bin_mid = 0.5 * (r_bin[1:] + r_bin[0:-1])
count = np.histogram(r_subhalo, bins = r_bin)[0]
count2 = np.histogram(r_subhalo[is_subhalo], bins = r_bin)[0]
count3 = np.histogram(r_subhalo[sat], bins = r_bin, weights = subhalo_nbpart_list[sat])[0]

from utilities_plotting import *
py.subplot(211)
py.plot(r_bin_mid, count)
py.plot(r_bin_mid, count2)
py.subplot(212)
py.plot(r_bin_mid, count3)

py.show()

quit()'''

########## Read cell/particle data #########################        
print "Reading particle/cell data"
# Hdf5 group for this timestep (contains cell data)
ts_group = subvol_File["timestep_"+str(int(timestep))]

# Read in DM particle data
dm_group = ts_group["Dark_matter_particle_data"]
dx = dm_group["x_dm"][:]
dy = dm_group["y_dm"][:]
dz = dm_group["z_dm"][:]
dmass = dm_group["m_dm"][:]

# scale dark matter particle masses into Msun
dmass *= m2Msun
dind = np.zeros_like(dx) + 2

# Read in star particle data
star_group = ts_group["Stellar_particle_data"]

sm = star_group["m_s"][:]
sx = star_group["x_s"][:]
sy = star_group["y_s"][:]
sz = star_group["z_s"][:]
sind = np.ones_like(sx)

# Scale stellar masses into solar mass units
sm *= m2Msun

cell_group = ts_group["Gas_leaf_cell_data"]

rho_snap = cell_group["rho_c"][:] # Gas density
pre_snap = cell_group["pre_c"][:] # Gas pressure
cx = cell_group["x_c"][:] # Cell coordinates
cy = cell_group["y_c"][:]
cz = cell_group["z_c"][:]
lev = cell_group["lev"][:] # Cell refinement level
xH1 = cell_group["xH1"][:]
xHe1 = cell_group["xHe1"][:]
xHe2 = cell_group["xHe2"][:]
cind = np.zeros_like(cx)

print "Done: performing unit conversions and setting up chains"

cut.jeje_utils.read_conversion_scales(RunDir,timestep)
ndensity = rho_snap * cut.jeje_utils.dp_scale_d / cut.jeje_utils.mh * cut.jeje_utils.xh # Number density: Atoms cm^-3
scale_temk = (cut.jeje_utils.dp_scale_l / cut.jeje_utils.dp_scale_t)**2 * cut.jeje_utils.mh / cut.jeje_utils.kb
temp = pre_snap/rho_snap * scale_temk # T/mu in K

# Compute cell size using amr refinement level and box size
csize = boxlen_pkpc / np.power(2.,lev) # proper kpc
cmass = rho_snap * rho2msunpkpc3 * csize**3 # Msun - gas mass in each cell

nstar = len(sx); ncell = len(cx); ndm = len(dx)

# Combine leaf cells, stars and dm particles into a single set of arrays
cx = np.concatenate([cx, sx, dx])
cy = np.concatenate([cy, sy, dy])
cz = np.concatenate([cz, sz, dz])
cmass = np.concatenate([cmass, sm, dmass])
cind = np.concatenate([cind, sind, dind])

# Build linked-list of cells
cxyz_min = min(cx.min(),cy.min(),cz.min()) 
cxyz_max = max(cx.max(),cy.max(),cz.max())

cx_rescale = (cx - cxyz_min) / (cxyz_max-cxyz_min)
cy_rescale = (cy - cxyz_min) / (cxyz_max-cxyz_min)
cz_rescale = (cz - cxyz_min) / (cxyz_max-cxyz_min)

nleafcells = len(cx)
nchain_cells = 20
nextincell,firstincell = nu.neighbour_utils.init_chain(cx_rescale,cy_rescale,cz_rescale,nchain_cells)
        
print "Done"

xh_rescale = (xh-cxyz_min)/(cxyz_max-cxyz_min)
yh_rescale = (yh-cxyz_min)/(cxyz_max-cxyz_min)
zh_rescale = (zh-cxyz_min)/(cxyz_max-cxyz_min)

rmax_rescale = rvirh * 2 /(cxyz_max-cxyz_min)

# Find cells within rmax of this halo (using chain algorithm)                              
ncell_in_rmax = nu.neighbour_utils.get_npart_in_sphere_with_chain(RunDir,timestep,cx_rescale,cy_rescale,cz_rescale,xh_rescale,yh_rescale,zh_rescale,rmax_rescale,firstincell,nextincell)
if ncell_in_rmax > 0:
    ok = nu.neighbour_utils.get_part_indicies_in_sphere_with_chain(RunDir,timestep,cx_rescale,cy_rescale,cz_rescale,xh_rescale,yh_rescale,zh_rescale,rmax_rescale,ncell_in_rmax,firstincell,nextincell)
else:
    ok = np.zeros_like(cx) < 0

######## Compute the mass profile of the host halo #######################
# Use r200 for central haloes, skip for satellite haloes
mhost_enclosed_f, logslope_host_f, rmax_mhost, rmax_slope = up.Calculate_Host_Profile(cx[ok], cy[ok], cz[ok], cmass[ok], xh, yh, zh,rvirh)
                
########### Compute 
in_sat = np.zeros_like(cx) < 0
logmstar_sat = np.zeros_like(cx)
index_sat = np.zeros_like(cx)
for isub in range(n_sat):

    sub_num = subhalo_num_list[sat][isub]

    sub_x = subhalo_x_list[sat][isub]
    sub_y = subhalo_y_list[sat][isub]
    sub_z = subhalo_z_list[sat][isub]

    # computed the radius from the centre of the host to the subhalo
    sub_r = r_subhalo[sat][isub] * rvirh

    # Skip subhaloes that are definitely outside the halo
    if sub_r > 2* rvirh:
        continue

    print "Looping over satellites", isub, "of", n_sat

    sub_x_rescale = (sub_x-cxyz_min)/(cxyz_max-cxyz_min)
    sub_y_rescale = (sub_y-cxyz_min)/(cxyz_max-cxyz_min)
    sub_z_rescale = (sub_z-cxyz_min)/(cxyz_max-cxyz_min)
    
    subhalo_rmax = 2 * subhalo_rvir_list[sat][isub] # In this case, we use this as an efficient preselection
    sub_rvir = subhalo_rvir_list[sat][isub] * factor_subhalo # Keep track so we can use this as the subhalo radius when tidal radius calculation fails and we don't have r200 calculated
                    
    sat_temp = subhalo_num_list[sat][isub] == ap_group["halo_num"][:][ok_ts]
    if ap_group["r200"][:][ok_ts][sat_temp] > 0.0:
        sub_rvir = ap_group["r200"][:][ok_ts][sat_temp]/r2kpc
        
    sub_rvir_rescale = (subhalo_rmax*factor_subhalo)/(cxyz_max-cxyz_min)

    ncell_in_sub_rvir = nu.neighbour_utils.get_npart_in_sphere_with_chain(RunDir,timestep,cx_rescale,cy_rescale,cz_rescale,sub_x_rescale,sub_y_rescale,sub_z_rescale,sub_rvir_rescale,firstincell,nextincell)
    
    if ncell_in_sub_rvir <= 0:
        continue

    ind_sub = nu.neighbour_utils.get_part_indicies_in_sphere_with_chain(RunDir,timestep,cx_rescale,cy_rescale,cz_rescale,sub_x_rescale,sub_y_rescale,sub_z_rescale,sub_rvir_rescale,ncell_in_sub_rvir,firstincell,nextincell)

    # For subhaloes that are beyond the virial radius, it doesn't make sense to compute a tidal radius. Assign their virial radius instead
    sub_r_use = min(rmax_mhost,sub_r) # Use endpoint values when interpolating beyond the grid
    mhost_enclosed = mhost_enclosed_f(sub_r_use) # Host mass enclosed with the subhalo
    sub_r_use = min(rmax_slope, sub_r)
    logslope_host_r = logslope_host_f(sub_r_use) # Logarithmic slope of the host mass profile at the radius of the subhalo

    # Centre the frame on the subhalo
    cxs = cx[ind_sub] - sub_x; cys = cy[ind_sub] - sub_y; czs = cz[ind_sub] - sub_z
    # Radius to each cell/particle
    crs = np.sqrt(np.square(cxs) + np.square(cys) + np.square(czs))

    # Flag cells that are closer to the host than a subhalo (when the subhalo is outside the virial radius) as NOT in a subhalo
    # This deals with the cases where there is a major halo merger going on and it is hard to identify whether a particle/cell belongs to one or the other
    if sub_r > rvirh:
        r_host = np.sqrt(np.square(xh-cx[ind_sub]) + np.square(yh-cy[ind_sub]) + np.square(zh-cz[ind_sub]))
        flag_host = r_host < crs
        nb4 = len(ind_sub)
        ind_sub = ind_sub[flag_host==False]

        crs = crs[flag_host==False]

    ind_sub, r_tidal, ndm_in_rtidal = up.Calculate_Rtidal(crs, cmass[ind_sub], ind_sub, sub_x, sub_y, sub_z, sub_r, mhost_enclosed, logslope_host_r, sub_rvir)
    in_sat[ind_sub] = True

    mstar_sat = np.sum(cmass[ind_sub][cind[ind_sub]==1])

    if mstar_sat > 0:
        logmstar_sat[ind_sub] = np.log10(mstar_sat)
    else:
        logmstar_sat[ind_sub] = -99

    index_sat[ind_sub] = sub_num

    
    #if ndm_in_rtidal > sub_threshold_no:
    #    c_component_index[ind_sub] = 2
    #else:
    #    c_component_index[ind_sub] = 1

in_sat = in_sat[cind==0]
logmstar_sat = logmstar_sat[cind==0]
index_sat = index_sat[cind==0]

subvol_File.close()
File.close()

filename = "temp_dump.hdf5"
if os.path.isfile(filename):
    os.remove(filename)

file_temp = h5py.File("temp_dump.hdf5")

file_temp.create_dataset("in_sat", data=in_sat)
file_temp.create_dataset("logmstar_sat", data=logmstar_sat)
file_temp.create_dataset("index_sat", data=index_sat)

file_temp.close()
print "Finished"
