import numpy as np

timesteps_show = np.arange(10,140)
#timesteps_show = np.arange(10,12)

fig_name = "tracer_exploration_1290.pdf"

Zoom = "Zoom-7-1290"
Run = "tracer_run_lr"
timestep_final = 155

RunDir = "/cral4/mitchell/data/Tracer_Test/"+ Zoom + "/" + Run
OutDir = "/cral4/mitchell/data/Tracer_Test/PostProcess/"

HaloDir = RunDir + "/Halos/"
TreeDir = RunDir + "/Trees/"

import sys
import dm_utils as dm
from scipy import spatial
import h5py
import utilities_ramses as ur
import neighbour_utils as nu
import match_searchsorted as ms

File = h5py.File("saved_tracers_select.hdf5")
tid_select = File["tid"][:]
File.close()
n_select = len(tid_select)

############## IO #########################
# Read in relevant tree information from the hdf5 file
filename = Zoom + "_" + Run + "_tsf" + str(timestep_final) +".hdf5"

File = h5py.File(OutDir+filename)

# Open the subvolume file (containing all cell data)
subvol_filename = "subvol_"+filename
subvol_File = h5py.File(OutDir+subvol_filename)

# Check the file with the tree information already exists
if not Zoom in File:
    print "Error: Tree information for ", Zoom, " in ", filename, " cannot be read"
    exit()

zoom_group = File[Zoom]

############## Track selected tracers ###################

# Loop over timesteps
for i_timestep, timestep_tree in enumerate(timesteps_show):
    timestep = timestep_tree + 1 # Stupid mismatch between merger tree files and everything else

    # Get conversions for different units
    r2kpc, redshift, little_h, m2Msun,rho2msunpkpc3,v2kms = ur.Set_Units(RunDir, timestep)
    boxlen_pkpc = dm.jeje_utils.get_param_real(RunDir,timestep,'boxlen') * r2kpc # Box size in proper kpc, using the correct box size

    print "reading cell data at timestep = ", timestep, "of", timestep_final, ", redshift is", redshift

    ########## Read tracer data ###################
    # Hdf5 group for this timestep (contains cell data)
    ts_group = subvol_File["timestep_"+str(timestep)]
    
    tracer_group = ts_group["Tracer_particle_data"]

    tx = tracer_group["x_tr"][:]
    ty = tracer_group["y_tr"][:]
    tz = tracer_group["z_tr"][:]
    fam_tr = tracer_group["fam_tr"][:]
    id_tr = tracer_group["id_tr"][:]
    
    ######### Efficiently find tracers inside rvir ###########

    txyz_min = min(tx.min(),ty.min(),tz.min())
    txyz_max = max(tx.max(),ty.max(),tz.max())

    tx_rescale = (tx - txyz_min) / (txyz_max-txyz_min)
    ty_rescale = (ty - txyz_min) / (txyz_max-txyz_min)
    tz_rescale = (tz - txyz_min) / (txyz_max-txyz_min)

    nchain_cells = 20
    nextincell_t,firstincell_t = nu.neighbour_utils.init_chain(tx_rescale,ty_rescale,tz_rescale,nchain_cells)
    # Factor to help convert into box units (note maxime's merger_tree reader has converted halo positions so be careful here!)
    r2kpc_ideal, redshift, little_h, m2Msun,junk,junk = ur.Set_Units(RunDir, timestep, "ideal")
    boxlen_cMpc = dm.jeje_utils.get_param_real(RunDir,timestep,'boxlen') # Box size in box units
    boxlen_cMpc *= r2kpc_ideal
    boxlen_cMpc = boxlen_cMpc * (1+redshift) /1e3 # cMpc (comoving)
    boxlen_cMpc = boxlen_cMpc * little_h # h^-1 cMpc
    factor = 1./boxlen_cMpc

    # Get target halo properties
    for halo_group_str in zoom_group:

        # Only compute fluxes for the main halo
        if not "_main" in halo_group_str:
            continue
        
        mp_group = zoom_group[halo_group_str+"/main_progenitors"]
        
        # Identify the main progenitor at this timestep
        ok_ts = np.array(mp_group["halo_ts"])==timestep_tree # haloes on this timestep
        
        xh = mp_group["x"][ok_ts] * factor + 0.5
        yh = mp_group["y"][ok_ts] * factor + 0.5
        zh = mp_group["z"][ok_ts] * factor + 0.5
        
        # Use r200 for central haloes, and rvir (for now) for satellite haloes
        if mp_group["host_halo_id"][ok_ts] == mp_group["halo_id"][ok_ts]:
            rvirh = mp_group["r200"][ok_ts] / r2kpc
            
        else:
            rvirh = mp_group["rvir"][ok_ts] * factor

        if "_main" in halo_group_str:
            break
            
    # Select cells in the vicinity of the halo (using a chain algorithm)
    rmax = rvirh
    
    # Rescale halo positions for efficient cell division in chain algorithm
    xh_rescale = (xh-txyz_min)/(txyz_max-txyz_min)
    yh_rescale = (yh-txyz_min)/(txyz_max-txyz_min)
    zh_rescale = (zh-txyz_min)/(txyz_max-txyz_min)
    rmax_rescale = rmax/(txyz_max-txyz_min)
    
    ncell_in_rmax = nu.neighbour_utils.get_npart_in_sphere_with_chain(RunDir,timestep,tx_rescale,ty_rescale,tz_rescale,xh_rescale,yh_rescale,zh_rescale,rmax_rescale,firstincell_t,nextincell_t)
    if ncell_in_rmax > 0:
        select_t = nu.neighbour_utils.get_part_indicies_in_sphere_with_chain(RunDir,timestep,tx_rescale,ty_rescale,tz_rescale,xh_rescale,yh_rescale,zh_rescale,rmax_rescale,ncell_in_rmax,firstincell_t,nextincell_t)
    else:
        select_t = np.zeros_like(tx) < 0

    ######## Do tracer preselection ############
    tx_h = tx[select_t]
    ty_h = ty[select_t]
    tz_h = tz[select_t]
    tid_h = id_tr[select_t]
    tfam_h = fam_tr[select_t]
    
    # Put the halo at the centre of the particle coordinate system
    tx_h = tx_h - xh; ty_h = ty_h - yh; tz_h = tz_h -zh
    # Compute the radius to each tracer
    tr_h = np.sqrt(np.square(tx_h) + np.square(ty_h) + np.square(tz_h)) / rvirh
    
    if i_timestep == 0:
        tr_track = np.zeros((len(timesteps_show), n_select)) + np.nan
        fam_track = np.zeros((len(timesteps_show), n_select)) + np.nan

    ptr = ms.match(tid_select, tid_h)
    ok = ptr >= 0
    
    tr_track[i_timestep][ok] = tr_h[ptr][ok]
    fam_track[i_timestep][ok] = tfam_h[ptr][ok]
    
subvol_File.close()
File.close()

print ""
print ""

np.random.seed(23231)
order = np.arange(0,n_select)
np.random.shuffle(order)

parts_show = np.arange(0,10)

print ""
print "radii"
for i_part in parts_show:
    print tr_track[:,order[i_part]]

from utilities_plotting import *

#py.axvline(timesteps_select[1],c="k")

c_list = ["k","b","m","r","g","y","c","firebrick","darkgray","orange"]

for i_part in parts_show:
    py.plot(timesteps_show, tr_track[:,order[i_part]],c=c_list[i_part])

    fam = fam_track[:,order[i_part]]
    ok = fam > 100
    py.scatter(timesteps_show[ok],tr_track[:,order[i_part]][ok])

py.xlabel("timestep")
py.ylabel(r"$r \, / R_{\mathrm{vir}}$")
    
py.savefig("Figures/"+fig_name)
py.show()
