# This script:
# 1) reads in a sample of pre-processed tracers (with Lagrangian status already assigned)
# 2) links those tracers with cells (from one timestep only) from hdf5 dump
# 3) compute the Lagrangian status of each cell
# 4) dumps the data to temporary file

import numpy as np

timestep = 155
#timestep = 50
#timestep = 100
#timestep = 10

Zoom = "Zoom-7-5092"
Run = "RhdRun-tracer_radfix"
timestep_final = 155
RT = True
RunDir = "/cral2/mitchell/"+Zoom+"/"+Run
InDir = "/cral2/mitchell/PostProcess/"
OutDir = "/cral2/mitchell/PostProcess/"


HaloDir = RunDir + "/Halos/"
TreeDir = RunDir + "/Trees/"

import sys
sys.path.append("../.")
import dm_utils as dm
from scipy import spatial
import h5py
import utilities_ramses as ur
import neighbour_utils as nu
import match_searchsorted as ms
import cutils as cut
import flux_utils
sys.path.append("../Rascas_analysis/.")
import jphot as jp
import os

# Get conversions for different units
r2kpc, redshift, little_h, m2Msun,rho2msunpkpc3,v2kms = ur.Set_Units(RunDir, timestep)
boxlen_pkpc = dm.jeje_utils.get_param_real(RunDir,timestep,'boxlen') * r2kpc # Box size in proper kpc, using the correct box size

print "Dumping cell properties (including matched tracer and rascas props) for ",Zoom,Run,", z=", redshift

############## Zoom file IO #########################
# Read in relevant tree information from the hdf5 file
filename_zoom = Zoom + "_" + Run + "_tsf" + str(timestep_final) +".hdf5"
    
File = h5py.File(OutDir+filename_zoom,"r")
    
# Open the subvolume file (containing all cell data)
subvol_filename = "subvol_"+filename_zoom
subvol_File = h5py.File(OutDir+subvol_filename,"r")

# Check the file with the tree information already exists
if not Zoom in File:
    print "Error: Tree information for ", Zoom, " in ", filename_zoom, " cannot be read"
    exit()

zoom_group = File[Zoom]

######### Setup the output arrays ##########
# Get number of timesteps
for halo_group_str in zoom_group:
    if "_main" in halo_group_str:
        halo_group_str_use= halo_group_str
mp_group = zoom_group[halo_group_str_use+"/main_progenitors"]

ts_group = subvol_File["timestep_"+str(timestep)]
cell_group = ts_group["Gas_leaf_cell_data"]

print "Reading cell properties"
rho_snap = cell_group["rho_c"][:]
pre_snap = cell_group["pre_c"][:]
cx = cell_group["x_c"][:] # Cell coordinates
cy = cell_group["y_c"][:]
cz = cell_group["z_c"][:]
clev = cell_group["lev"][:]
cvx = cell_group["vx_c"][:]
cvy = cell_group["vy_c"][:]
cvz = cell_group["vz_c"][:]
c_component_index = cell_group["component_index"][:]

if RT:
    xH1 = cell_group["xH1"][:]
    xHe1 = cell_group["xHe1"][:]
    xHe2 = cell_group["xHe2"][:]

print "Reading tracers"
tracer_group = ts_group["Tracer_particle_data"]

tx = tracer_group["x_tr"][:]
ty = tracer_group["y_tr"][:]
tz = tracer_group["z_tr"][:]
fam_tr = tracer_group["fam_tr"][:]
id_tr = tracer_group["id_tr"][:]
tmass = tracer_group["mass_tr"][:]

print "Reading stars"

star_group = ts_group["Stellar_particle_data"]
sx = star_group["x_s"][:]
sy = star_group["y_s"][:]
sz = star_group["z_s"][:]
sm = star_group["m_s"][:] * m2Msun

print "Done"

######### Efficiently find cells inside rvir ###########
cxyz_min = min(cx.min(),cy.min(),cz.min())
cxyz_max = max(cx.max(),cy.max(),cz.max())

cx_rescale = (cx - cxyz_min) / (cxyz_max-cxyz_min)
cy_rescale = (cy - cxyz_min) / (cxyz_max-cxyz_min)
cz_rescale = (cz - cxyz_min) / (cxyz_max-cxyz_min)

txyz_min = min(tx.min(),ty.min(),tz.min())
txyz_max = max(tx.max(),ty.max(),tz.max())

tx_rescale = (tx - txyz_min) / (txyz_max-txyz_min)
ty_rescale = (ty - txyz_min) / (txyz_max-txyz_min)
tz_rescale = (tz - txyz_min) / (txyz_max-txyz_min)

sxyz_min = min(sx.min(),sy.min(),sz.min())
sxyz_max = max(sx.max(),sy.max(),sz.max())

sx_rescale = (sx - sxyz_min) / (sxyz_max-sxyz_min)
sy_rescale = (sy - sxyz_min) / (sxyz_max-sxyz_min)
sz_rescale = (sz - sxyz_min) / (sxyz_max-sxyz_min)

nchain_cells = 20
nextincell_c,firstincell_c = nu.neighbour_utils.init_chain(cx_rescale,cy_rescale,cz_rescale,nchain_cells)
nextincell_t,firstincell_t = nu.neighbour_utils.init_chain(tx_rescale,ty_rescale,tz_rescale,nchain_cells)
nextincell_s,firstincell_s = nu.neighbour_utils.init_chain(sx_rescale,sy_rescale,sz_rescale,nchain_cells)

# Factor to help convert into box units (note maxime's merger_tree reader has converted halo positions so be careful here!)
r2kpc_ideal, redshift, little_h, m2Msun,junk,junk = ur.Set_Units(RunDir, timestep, "ideal")
boxlen_cMpc = dm.jeje_utils.get_param_real(RunDir,timestep,'boxlen') # Box size in box units
boxlen_cMpc *= r2kpc_ideal
boxlen_cMpc = boxlen_cMpc * (1+redshift) /1e3 # cMpc (comoving)
boxlen_cMpc = boxlen_cMpc * little_h # h^-1 cMpc
factor = 1./boxlen_cMpc

# Identify the main progenitor at this timestep
timestep_tree = timestep -1
ok_ts = np.array(mp_group["halo_ts"])==timestep_tree # haloes on this timestep

xh = mp_group["x"][ok_ts] * factor + 0.5
yh = mp_group["y"][ok_ts] * factor + 0.5
zh = mp_group["z"][ok_ts] * factor + 0.5

# Use r200 for central haloes, and rvir (for now) for satellite haloes
if mp_group["host_halo_id"][ok_ts] == mp_group["halo_id"][ok_ts]:
    rvirh = mp_group["r200"][ok_ts] / r2kpc           
else:
    rvirh = mp_group["rvir"][ok_ts] * factor

# Select cells in the vicinity of the halo (using a chain algorithm)
rmax = rvirh

print "Selecting cells within rvir"

###### Cells within virial radius #########
# Rescale halo positions for efficient cell division in chain algorithm
xh_rescale = (xh-cxyz_min)/(cxyz_max-cxyz_min)
yh_rescale = (yh-cxyz_min)/(cxyz_max-cxyz_min)
zh_rescale = (zh-cxyz_min)/(cxyz_max-cxyz_min)
rmax_rescale = rmax/(cxyz_max-cxyz_min)

ncell_in_rmax = nu.neighbour_utils.get_npart_in_sphere_with_chain(RunDir,timestep,cx_rescale,cy_rescale,cz_rescale,xh_rescale,yh_rescale,zh_rescale,rmax_rescale,firstincell_c,nextincell_c)
if ncell_in_rmax > 0:
    select_c = nu.neighbour_utils.\
               get_part_indicies_in_sphere_with_chain(\
                                                      RunDir,timestep,cx_rescale,cy_rescale,cz_rescale,\
                                                      xh_rescale,yh_rescale,zh_rescale,\
                                                      rmax_rescale,ncell_in_rmax,firstincell_c,nextincell_c)
else:
    select_c = np.zeros_like(cx) < 0

xh_rescale = (xh-txyz_min)/(txyz_max-txyz_min)
yh_rescale = (yh-txyz_min)/(txyz_max-txyz_min)
zh_rescale = (zh-txyz_min)/(txyz_max-txyz_min)
rmax_rescale = rmax/(txyz_max-txyz_min)

ncell_in_rmax = nu.neighbour_utils.get_npart_in_sphere_with_chain(RunDir,timestep,tx_rescale,ty_rescale,tz_rescale,xh_rescale,yh_rescale,zh_rescale,rmax_rescale,firstincell_t,nextincell_t)
if ncell_in_rmax > 0:
    select_t = nu.neighbour_utils.get_part_indicies_in_sphere_with_chain(RunDir,timestep,tx_rescale,ty_rescale,tz_rescale,xh_rescale,yh_rescale,zh_rescale,rmax_rescale,ncell_in_rmax,firstincell_t,nextincell_t)
else:
    select_t = np.zeros_like(tx) < 0

xh_rescale = (xh-sxyz_min)/(sxyz_max-sxyz_min)
yh_rescale = (yh-sxyz_min)/(sxyz_max-sxyz_min)
zh_rescale = (zh-sxyz_min)/(sxyz_max-sxyz_min)
rmax_rescale = rmax/(sxyz_max-sxyz_min)

ncell_in_rmax = nu.neighbour_utils.get_npart_in_sphere_with_chain(RunDir,timestep,sx_rescale,sy_rescale,sz_rescale,xh_rescale,yh_rescale,zh_rescale,rmax_rescale,firstincell_s,nextincell_s)
if ncell_in_rmax > 0:
    select_s = nu.neighbour_utils.get_part_indicies_in_sphere_with_chain(RunDir,timestep,sx_rescale,sy_rescale,sz_rescale,xh_rescale,yh_rescale,zh_rescale,rmax_rescale,ncell_in_rmax,firstincell_s,nextincell_s)
else:
    select_s = np.zeros_like(sx) < 0

######## Select cells inside rvir ############
cx_h = cx[select_c]
cy_h = cy[select_c]
cz_h = cz[select_c]
rho_snap_h = rho_snap[select_c]; pre_snap_h = pre_snap[select_c]
cvx_h = cvx[select_c]; cvy_h = cvy[select_c]; cvz_h = cvz[select_c]
clev_h = clev[select_c]; c_component_index_h = c_component_index[select_c]

if RT:
    xH1_h = xH1[select_c]
    xHe1_h = xHe1[select_c]
    xHe2_h = xHe2[select_c]

csize_h = boxlen_pkpc / 2.**clev_h / r2kpc # code units

tx = tx[select_t]
ty = ty[select_t]
tz = tz[select_t]
tid = id_tr[select_t]
tfam = fam_tr[select_t]
tmass = tmass[select_t]

ok = tfam != 100
print "Checking total tracer mass in stars, sum of 1) tr, 2) parts = ", np.log10(np.sum(tmass[ok]*m2Msun)), np.log10(np.sum(sm[select_s]))

ok = (tfam==100)
tfam = tfam[ok]
tx = tx[ok]
ty = ty[ok]
tz = tz[ok]
tid = tid[ok]
tmass = tmass[ok]

csize = csize_h * r2kpc # proper kpc
cmass = rho_snap_h * rho2msunpkpc3 * np.power(csize,3)
print "Checking total tracer mass in gas, sum of 1) tr, 2) parts = ", np.log10(np.sum(tmass*m2Msun)), np.log10(np.sum(cmass))
quit()

print "Grouping pre-processed tracer data"

######## Compute mass weighted tracer status of each cell ###########
print "Matching cells and tracers"

ind_c, ok_match = flux_utils.Match_Cell_Particle(csize_h, cx_h, cy_h, cz_h, tx, ty, tz)
        
ctmass = flux_utils.Sum_Common_ID( [tmass[ok_match]], ind_c[ok_match], np.arange(len(cx_h)) )[0] # Code units

cell_vol = np.power(csize_h*r2kpc,3) # kpc3
ctrho = ctmass*m2Msun / cell_vol / rho2msunpkpc3 # code units

subvol_File.close()
File.close()




################### Dump data to disk ######################

dump_dir = "/cral2/mitchell/PostProcess/Validation_Dumps/"
dump_file_path = dump_dir + "dump_"+Zoom+"_"+Run+"_ts"+str(timestep)+".hdf5"

if os.path.isfile(dump_file_path):
    os.remove(dump_file_path)
    print "deleted previous hdf5 file at ", dump_file_path

dump_file = h5py.File(dump_file_path)

dump_file.create_dataset("x",data=cx_h)
dump_file.create_dataset("y",data=cy_h)
dump_file.create_dataset("z",data=cz_h)
dump_file.create_dataset("vx",data=cvx_h)
dump_file.create_dataset("vy",data=cvy_h)
dump_file.create_dataset("vz",data=cvz_h)
dump_file.create_dataset("lev",data=clev_h)
dump_file.create_dataset("rho",data=rho_snap_h)
dump_file.create_dataset("pre",data=pre_snap_h)
dump_file.create_dataset("comp",data=c_component_index_h)

if RT:
    dump_file.create_dataset("xH1",data=xH1_h)
    dump_file.create_dataset("xHe1",data=xHe1_h)
    dump_file.create_dataset("xHe2",data=xHe2_h)


dump_file.create_dataset("ctmass",data=ctmass)
dump_file.create_dataset("ctrho",data=ctrho)

dump_file.create_dataset("tmass",data=tmass)
dump_file.create_dataset("tx",data=tx)
dump_file.create_dataset("ty",data=ty)
dump_file.create_dataset("tz",data=tz)

print "Got to the end :)"
