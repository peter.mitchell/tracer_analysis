import argparse
import sys
sys.path.append("../")
import numpy as np
import time
import os
import h5py
import utilities_ramses as ur
from utilities_plotting import *
import cutils as cut
import dm_utils

#timestep = 155
timestep = 50
#timestep = 100
#timestep = 10

Zoom = "Zoom-7-5092"
Run = "RhdRun-tracer_radfix"
RT = True
RunDir = "/cral2/mitchell/"+Zoom+"/"+Run
InDir = "/cral2/mitchell/PostProcess/"
OutDir = "/cral2/mitchell/PostProcess/"
timestep_final = 155

dump_dir = "/cral2/mitchell/PostProcess/Validation_Dumps/"


# Read in relevant tree information from the hdf5 file
dump_file_path = dump_dir + "dump_"+Zoom+"_"+Run+"_ts"+str(timestep)+".hdf5"

print "Performing IO"
dump_file = h5py.File(dump_file_path,"r")

rho = dump_file["rho"][:]
lev = dump_file["lev"][:]
x = dump_file["x"][:]
y = dump_file["y"][:]
z = dump_file["z"][:]

tmass = dump_file["ctmass"][:]
trho = dump_file["ctrho"][:]

tmass_tr = dump_file["tmass"][:]
x_tr = dump_file["tx"][:]
y_tr = dump_file["ty"][:]
z_tr = dump_file["tz"][:]

temp = tmass / tmass_tr[0]

for n in range(50000):
    print trho[n], temp[n], lev[n]

quit()


print "reading halo catalogue"

filename = Zoom + "_" + Run + "_tsf" + str(timestep_final) +".hdf5"
File = h5py.File(OutDir+filename,"r")

# Check the file with the tree information already exists
if not Zoom in File:
    print "Error: Tree information for ", Zoom, " in ", filename, " cannot be read"
    exit()

zoom_group = File[Zoom]

timestep_tree = timestep-1

# Get conversions for different units
r2kpc, redshift, little_h, m2Msun,rho2msunpkpc3,v2kms = ur.Set_Units(RunDir, timestep)
boxlen_pkpc = dm_utils.jeje_utils.get_param_real(RunDir,timestep,'boxlen') * r2kpc # Box size in proper kpc, using the correct box size

print "redshift is", redshift




# Factor to help convert into box units (note maxime's merger_tree reader has converted halo positions so be careful here!)
r2kpc_ideal, redshift, little_h, m2Msun,junk,junk = ur.Set_Units(RunDir, timestep, "ideal")
boxlen_cMpc = dm_utils.jeje_utils.get_param_real(RunDir,timestep,'boxlen') # Box size in box units
boxlen_cMpc *= r2kpc_ideal
boxlen_cMpc = boxlen_cMpc * (1+redshift) /1e3 # cMpc (comoving)
boxlen_cMpc = boxlen_cMpc * little_h # h^-1 cMpc
factor = 1./boxlen_cMpc

# Loop over final timestep haloes
for halo_group_str in zoom_group:
    # Only write maps for main progenitors of the main halo
    if "_main" in halo_group_str:
        break

halo_group = zoom_group[halo_group_str]
mp_group = zoom_group[halo_group_str+"/main_progenitors"]

# Identify the main progenitor at this timestep
ok_ts = np.array(mp_group["halo_ts"])==timestep_tree # haloes on this timestep

print "done, computing cell quantities"



print "Computing cell masses"
csize = boxlen_pkpc / np.power(2,lev) # proper kpc
cmass = rho * rho2msunpkpc3 * np.power(csize,3)

tmass *= m2Msun
print "Summed tracer mass, cell mass", np.log10(np.sum(tmass)), np.log10(np.sum(cmass))

# Set cell unit conversions for this timestep
cut.jeje_utils.read_conversion_scales(RunDir,timestep)

nh = rho * cut.jeje_utils.dp_scale_d / cut.jeje_utils.mh * cut.jeje_utils.xh
nh_tr = trho * cut.jeje_utils.dp_scale_d / cut.jeje_utils.mh * cut.jeje_utils.xh

a,b = np.unique((tmass / (tmass_tr[0]*m2Msun)).astype("int"), return_counts=True)
py.plot(a,b)
py.xlim((0,100))
py.show()
quit()

'''ind = np.argmax(nh_tr)
print tmass[ind]/ (tmass_tr[0]*m2Msun)

sep = csize[ind]*0.5 / r2kpc

ok = (x_tr > x[ind] -sep) & (x_tr < x[ind] +sep) & (y_tr > y[ind] -sep) & (y_tr < y[ind] +sep) &(z_tr > z[ind] -sep) & (z_tr < z[ind] +sep)
print len(x_tr[ok])
print np.log10(rho[ind]), np.log10(trho[ind])
quit()
'''

print "plotting"

py.rcParams.update(latexParams)
py.rcParams.update({'figure.subplot.wspace':0.1,'figure.subplot.hspace':0.1,
                    'figure.figsize':[3.32*1.5,2.49*1.5*2],
                    'figure.subplot.left':0.15,
                    'figure.subplot.bottom':0.18,
                    'figure.subplot.top':0.94,
                    'figure.subplot.right':0.94,
                    'legend.fontsize':6,
                    'font.size':12,
                    'axes.labelsize':11})

py.subplot(211)
bin_nh = np.arange(-6,6,0.1)
mid = 0.5*(bin_nh[1:] + bin_nh[0:-1])

a,b = np.histogram(np.log10(nh), bins=bin_nh)

ok = tmass > 0
a2,b2 = np.histogram(np.log10(nh_tr[ok]), bins=bin_nh)

py.plot(mid, a,c="k")
py.plot(mid, a2,c="r")

py.subplot(212)
bin_nh = np.arange(-6,6,0.1)
mid = 0.5*(bin_nh[1:] + bin_nh[0:-1])

a,b = np.histogram(np.log10(nh), bins=bin_nh,weights = cmass)

ok = tmass > 0
a2,b2 = np.histogram(np.log10(nh_tr[ok]), bins=bin_nh, weights = tmass[ok])

py.plot(mid, a,c="k")
py.plot(mid, a2,c="r")

py.savefig("../Figures/tracer_validation_ts"+str(timestep)+".pdf")
py.show()
