import numpy as np
import h5py
import utilities_cosmology as uc
import dm_utils as dm

note that c_component_index (i.e. comp) got changed - so this script would needto be updated before itcan be used

np.random.seed(123612765)
i_part = 2
cgm_ts = 106

File = h5py.File("saved_tracers_select_Zoom-7-5092.hdf5")

tid_select = File["tid"][:]
comp       = File["comp"][:cgm_ts]
cv         = File["cv"][:cgm_ts]
nh         = File["nh"][:cgm_ts]
temp       = File["temp"][:cgm_ts]
xhi        = File["xhi"][:cgm_ts]
radius = File["r"][:cgm_ts]
timestep = File["timestep"][:cgm_ts]
family = File["family"][:cgm_ts]
radius_p = File["r_physical"][:cgm_ts]
radius_norm = File["r"][:cgm_ts]
redshift = File["redshift"][:cgm_ts]

File.close()
n_select = len(tid_select)
# Get time from redshift
Zoom = "Zoom-7-1290"; Run = "tracer_run_lr"; RunDir = "/cral4/mitchell/data/Tracer_Test/"+ Zoom + "/" + Run
dm.jeje_utils.read_conversion_scales(RunDir,timestep[0])
omega_0,lambda_0,little_h = dm.jeje_utils.read_cosmo_params(RunDir,timestep[0])
a = 1./(1.+redshift)
t, tlb = uc.t_Universe(a,omega_0,little_h)

# Get time-averaged radial velocity
dr = (radius_p[1:] - radius_p[0:-1]) * (3.09*10**16) # km
dt = (t[1:]-t[0:-1]) * 3.155*10**16 # s

t_av = 0.5 * (t[0:-1]+t[1:])
cv_av = dr/np.reshape(dt,(len(dt),1))

############ Custom selections ###############
family_temp = np.copy(family)
family_temp[np.isnan(family_temp)] = 100

comp_temp = np.copy(comp)
comp_temp[np.isnan(comp_temp)] = 0

# Count consecutive number of outputs a particle is registered as a satellite
nsat_max = np.zeros(n_select)
nsat = np.zeros(n_select)
for n in range(len(comp_temp[:,0])):
    nsat[comp_temp[n]>0] +=1
    nsat[comp_temp[n]==0] = 0    
    nsat_max[nsat>nsat_max] = nsat[nsat>nsat_max]
nsat_final = nsat
    
cv_temp = np.copy(cv)
cv_temp[np.isnan(cv_temp)] = 0

rad_temp = np.copy(radius_norm)
rad_temp[np.isnan(rad_temp)] = 1e9

jim = np.max(comp_temp,axis=0)
tim = np.max(family_temp,axis=0)
rmin = np.min(rad_temp,axis=0)
vrmax = np.max(cv_temp,axis=0)
vrmin = np.min(cv_temp,axis=0)

ninhalo = np.ones_like(comp)
ninhalo[np.isnan(comp)] = 0
ninhalo = np.sum(ninhalo,axis=0)

# Count particles that get ejected from the halo, and number of those that return
accreted = np.zeros(n_select)
ejected = np.zeros(n_select)
nej = np.zeros(n_select)
nret = np.zeros(n_select)-1
for n in range(len(comp_temp[:,0])):
    accreted[rad_temp[n]<0.9] = 1
    ejected = (accreted==1) & (rad_temp[n]>1)
    accreted[ejected] = 0

    nej[ejected] += 1
    if n > 0:
        nret[(rad_temp[n]<0.9) & (rad_temp[n-1]>0.9)] +=1
    else:
        nret[(rad_temp[n]<0.9)] += 1

    
    
nsat_thr = 2

print ""
print "preliminary breakdown of ts final CGM"

# Identify stripped particles that have not been in the ISM (or back into satellite) after being stripped
nsat = np.zeros(n_select)
n_diff_cgm = np.zeros(n_select)
n_diff_cgm_max = np.zeros(n_select)
stripped = np.zeros(n_select)
nism = np.zeros(n_select)
was_in_ism = np.zeros(n_select)

ninflow = np.zeros(n_select)
noutflow = np.zeros(n_select)
mono_inflow = np.zeros(n_select)-1

noutflow_max = np.zeros(n_select)

# This was is for detecting past-peri particles
dv_max = np.zeros(n_select)
logdT_at_dv_max = np.zeros(n_select)
r_at_dv_max = np.zeros(n_select)
ts_dv_max = np.zeros(n_select)
dEk_dv_max = np.zeros(n_select)
rmax_after_dvmax = np.zeros(n_select)

# This is one is for detecting fb outflows
dv_max2 = np.zeros(n_select)
logdT_at_dv_max2 = np.zeros(n_select)
r_at_dv_max2 = np.zeros(n_select)
ts_dv_max2 = np.zeros(n_select)

static = np.zeros(n_select)

for n in range(len(comp_temp[:,0])):
    nsat[comp_temp[n]>0] +=1

    n_diff_cgm[(comp_temp[n]==0) & (rad_temp[n]<=1.0)] +=1
    n_diff_cgm[comp_temp[n]>0] = 0
    
    in_sat = nsat>2
    stripped[in_sat] = 0
    
    stripped[in_sat & (n_diff_cgm>2) & (n_diff_cgm_max<3)] += 1
    nsat[(comp_temp[n]==0) & (n_diff_cgm>2)] = 0

    n_diff_cgm_max = np.maximum(n_diff_cgm, n_diff_cgm_max)

    nism[rad_temp[n]<0.2] +=1
    nism[rad_temp[n]>0.2] = 0
    stripped[nism>1] = 0

    was_in_ism[(nism==5) | (rad_temp[n]<0.1)] += 1

    if n > 0:
        check = (was_in_ism > 0) & (rad_temp[n-1]<0.2) & (rad_temp[n]>0.2) & (comp_temp[n]>0)
        was_in_ism[check] = 0
    
    ninflow[cv_temp[n] < 0] += 1
    ninflow[cv_temp[n]>0] = 0
    noutflow[cv_temp[n] > 0] += 1
    noutflow[cv_temp[n]<0] = 0

    noutflow_max = np.maximum(noutflow, noutflow_max)
    
    mono_inflow[(ninflow>2)&(mono_inflow==-1)] = 1
    mono_inflow[noutflow>1] = 0

    if n > 0:
        dv = cv_temp[n] - cv_temp[n-1]

        greater = (dv>dv_max) & (cv_temp[n]!=0) & (cv_temp[n-1]!=0) & (cv_temp[n]>0)
        logdT_at_dv_max[greater] = (np.log10(temp[n]) - np.log10(temp[n-1]))[greater]
        dv_max[greater] = dv[greater]
        r_at_dv_max[greater] = rad_temp[n][greater]
        ts_dv_max[greater] = n
        dEk_dv_max[greater] = (cv_temp[n]**2 - cv_temp[n-1]**2)[greater]
        rmax_after_dvmax[greater] = np.max(rad_temp[n:],axis=0)[greater]
        
        greater = (dv>dv_max) & (cv_temp[n]!=0) & (cv_temp[n-1]!=0)
        logdT_at_dv_max2[greater] = (np.log10(temp[n]) - np.log10(temp[n-1]))[greater]
        dv_max2[greater] = dv[greater]    
        r_at_dv_max2[greater] = rad_temp[n][greater]
        ts_dv_max2[greater] = n

               
    if n == len(comp_temp[:,0])-1:
        static = (rad_temp[n] < 1.0) & (rad_temp[n] > rad_temp[n-5]-0.025) & (rad_temp[n] < rad_temp[n-5]+0.025) & (rad_temp[n]>0.2)
        if len(static[static]) > 0:
            for i in range(4):
                static[(rad_temp[n][static] < rad_temp[n-4+i][static]-0.025) | (rad_temp[n][static] > rad_temp[n-4+i][static]+0.025) ] = False
                    
        
    #test = 617
    #print comp[n,test], nsat[test], stripped[test], nism[test], n_diff_cgm[test], in_sat[test]
    
# correction for mono_inflow forparticles that are first being accreted
correct = (mono_inflow==1) | ((vrmax < 0.001) & (vrmin<-30) & (ninhalo<3))
mono_inflow[correct] = 1


cgm_final = (rad_temp[-1]>0.2)&(rad_temp[-1]<1.0) & (tim==100)&(nsat_final<=nsat_thr)

a = (stripped>0) & cgm_final
b = cgm_final&(was_in_ism>0)
c = cgm_final&(mono_inflow==1)&(was_in_ism==0)&(stripped==0)&(vrmin < -30)&(nej==0)
d = cgm_final & (ninhalo<=10) & (vrmin >= -30)

e = cgm_final & (dv_max>20) & (logdT_at_dv_max<0.5) & (rmax_after_dvmax >0.2 ) & (vrmax>10) & (vrmax < 150) & (r_at_dv_max<0.5)

f = cgm_final & (dv_max>30) & (logdT_at_dv_max2>0.3) & (vrmax > 50) & ((rmin > 0.2) | (was_in_ism==0))
f = f | ( cgm_final & (vrmax > abs(vrmin)*1.5) & (vrmax > 100) & (was_in_ism==0))

g = cgm_final & static & (a==False) & (b==False) & (c==False) & (d==False) & (e==False) & (f==False)

h = cgm_final & (a==False) & (b==False) & (c==False) & (d==False) & (e==False) & (f==False) & (g==False)

print "fraction of final CGM that has been stripped from satellites", len(jim[a]) / float(len(jim[cgm_final]))
#print "fraction of final CGM that came from the central ISM", len(jim[b]) / float(len(jim[cgm_final]))
print "fraction of final CGM was ejected from central ISM as fb", len(jim[b&(e==False)]) / float(len(jim[cgm_final]))
print "fraction of final CGM that is diffuse, and inflowing for first time", len(jim[c]) / float(len(jim[cgm_final]))
print "fraction of final CGM classed as past-peri", len(jim[e])/ float(len(jim[cgm_final]))
print "fraction of final CGM loaded into Fb-driven outflow on CGM scales", len(jim[f]) / float(len(jim[cgm_final]))
print "fraction of final CGM that is static (but not anything else) at the end", len(jim[g]) / float(len(jim[cgm_final]))
print "fraction of final CGM classed as transient", len(jim[d]) / float(len(jim[cgm_final]))
print "fraction of final CGM unclassified", len(jim[h]) / float(len(jim[cgm_final]))

print ""
print "now for neutral CGM"

xhi_final = xhi[-1]
xhi_final[np.isnan(xhi_final)==0]

print "fraction of final CGM that has been stripped from satellites", np.sum(xhi_final[a]) / float(np.sum(xhi_final[cgm_final]))
#print "fraction of final CGM that came from the central ISM", np.sum(xhi_final[b]) / float(np.sum(xhi_final[cgm_final]))
print "fraction of final CGM was ejected from central ISM as fb", np.sum(xhi_final[b&(e==False)]) / float(np.sum(xhi_final[cgm_final]))
print "fraction of final CGM that is diffuse, and inflowing for first time", np.sum(xhi_final[c]) / float(np.sum(xhi_final[cgm_final]))
print "fraction of final CGM classed as past-peri", np.sum(xhi_final[e])/ float(np.sum(xhi_final[cgm_final]))
print "fraction of final CGM loaded into Fb-driven outflow on CGM scales", np.sum(xhi_final[f]) / float(np.sum(xhi_final[cgm_final]))
print "fraction of final CGM that is static (but not anything else) at the end", np.sum(xhi_final[g]) / float(np.sum(xhi_final[cgm_final]))
print "fraction of final CGM classed as transient", np.sum(xhi_final[d]) / float(np.sum(xhi_final[cgm_final]))
print "fraction of final CGM unclassified", np.sum(xhi_final[h]) / float(np.sum(xhi_final[cgm_final]))


parts_show = np.arange(0,n_select)

print "n in selection", len(parts_show)

np.random.shuffle(parts_show)

#check = 222
#print dv_max[check], logdT_at_dv_max[check], rmax_after_dvmax[check], vrmax[check], r_at_dv_max[check]

i_part = 14

ishow = parts_show[i_part]
print "this particle is", parts_show[i_part], tid_select[i_part]
print ishow
print i_part, "family, component", tid_select[ishow]
print family[:,ishow]
print comp[:,ishow] # note comp 0 = bound central, comp 1 = bound low-mass satellite, comp 2 = bound higher-mass satellite

#print "Hello", np.argmin(rad_temp[:,ishow])
#exit()

from utilities_plotting import *

c_list = ["k","b","m","r","g","y","c","firebrick","darkgray","orange"]

py.figure(figsize=(20,10))
py.subplot(321)

for i_part in range(1):
    py.plot(t, radius[:,ishow], c=c_list[i_part])
    py.scatter(t, radius[:,ishow], c=c_list[i_part],edgecolors="none")

    if 1 in comp[:,ishow] or 2 in comp[:,ishow]:
        py.plot(t, comp[:,ishow]*0.1, c=c_list[i_part],linestyle=":")

    if 101 in family[:,ishow]:
        py.plot(t, (t*0+0.9)*(family[:,ishow]-100), c=c_list[i_part],linestyle="--")
        
py.xlabel("t")
py.ylabel(r"$r \, / R_{\mathrm{vir}}$")
py.xlim((t.min(), t.max()))
py.ylim((0.0,1.0))

py.subplot(322)
for i_part in range(1):
    py.plot(t, np.log10(nh[:,ishow]), c=c_list[i_part])
    py.scatter(t, np.log10(nh[:,ishow]), c=c_list[i_part],edgecolors="none")

py.ylabel(r"$\log(n_{\mathrm{H}})$")
py.xlim((t.min(), t.max()))

py.subplot(323)
for i_part in range(1):
    py.plot(t, np.log10(temp[:,ishow]), c=c_list[i_part])
    py.scatter(t, np.log10(temp[:,ishow]), c=c_list[i_part],edgecolors="none")
py.ylabel(r"$\log(T)$")
py.xlim((t.min(), t.max()))

py.subplot(324)
for i_part in range(1):
    py.plot(t, xhi[:,ishow], c=c_list[i_part])
    py.scatter(t, xhi[:,ishow], c=c_list[i_part],edgecolors="none")
py.ylabel(r"$x_{\mathrm{HI}}$")
py.ylim((-0.05,1.05))
py.xlim((t.min(), t.max()))

py.subplot(325)
for i_part in range(1):
    py.plot(t, cv[:,ishow], c=c_list[i_part])
    py.scatter(t, cv[:,ishow], c=c_list[i_part],edgecolors="none")
    py.plot(t_av, cv_av[:,ishow], c=c_list[i_part],linestyle=':')

    py.axvline(t[ts_dv_max[ishow]])

    py.axhline(0.0)
    
py.ylabel(r"$v_{\mathrm{rad}}$")

py.xlim((t.min(), t.max()))
py.show()
