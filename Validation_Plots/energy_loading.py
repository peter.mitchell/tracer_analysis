import numpy as np
import h5py
import sys
sys.path.append("../../Analysis/Obs_Data")
sys.path.append("../")
from utilities_plotting import *
import dm_utils as dm

Zm = "Zoom-7-"
run_nfb = "hdRun-LSS150pkpc-new-new-prime"
run_P13  = "hdRun-LSS150pkpc-new-new-prime-fbk"
run_1112 ="hdRun-LSS150pkpc_ncycle_1112"
run_1112_MSN_5 = "hdRun-LSS150pkpc_ncycle_1112_MSN_5"
run_tr = "RhdRun-tracer_radfix"

Zoom_List = [Zm+"5092"]
Run_List =  [run_tr]
timestep_list = [155]
c_list = ["k"]
OutDir_List = ["/cral4/mitchell/data/5092_RT_Tracer/PostProcess/"]
RunDir_List = ["/cral4/mitchell/data/5092_RT_Tracer/"]
name_list = ["Mitchell et al. (2021)"]
timesteps_out_list = ["all"]

fig_name = "energy_loading_m21_eagle_comp.pdf"

nrow = 1; ncol = 1

for Zoom, Run, timestep_final, OutDir, RunDir, name,timesteps_out in zip(Zoom_List, Run_List, timestep_list, OutDir_List, RunDir_List, name_list,timesteps_out_list):

    print Zoom, Run, name

    RunDir += Zoom +"/"+Run


    dm.jeje_utils.read_conversion_scales(RunDir,timestep_final)
    
    omega_0,lambda_0,little_h = dm.jeje_utils.read_cosmo_params(RunDir,timestep_final)
    omega_b = dm.jeje_utils.get_param_real(RunDir,timestep_final,"omega_b")
    fb = omega_b / omega_0
    
    if timesteps_out != "final":
        all_redshifts = np.zeros(timestep_final-1)
        for n, timestep in enumerate(np.arange(2,timestep_final+1)):
            all_redshifts[n] = dm.jeje_utils.get_snap_redshift(RunDir,timestep)
    else:
        all_redshifts = np.array([dm.jeje_utils.get_snap_redshift(RunDir,timestep_final)])

    # Read the hdf5 file
    if timesteps_out == "all":
        filename = Zoom + "_" + Run + "_tsf" + str(timestep_final) +".hdf5"
    elif timesteps_out == "final":
        filename = Zoom + "_" + Run + "_tsf" + str(timestep_final) +"_ts"+str(timestep_final)+".hdf5"
    else:
        try:
            timestep_out = int(timesteps_out)
            filename = Zoom + "_" + Run + "_tsf" + str(timestep_final) +"_ts"+str(timestep_out)+".hdf5"
        except:
            print "timesteps_out",timesteps_out,",is not valid, sorry!"
            quit()
    File = h5py.File(OutDir+filename,"r")
    zoom_group = File[Zoom]

    for halo_group_str in zoom_group:
        # Check this is a halo
        if "main" in halo_group_str:
            break

    halo_group = zoom_group[halo_group_str]
    mp_group = halo_group["main_progenitors"]

    dEkdr = []
    dUdr = []
    vrad_fw = []

    ts_list = [150,151,152,153,154,155]

    for ts in ts_list:
        flux_group = halo_group["fluxes/timestep_"+str(ts)]
        rad_pro = flux_group["radial_profiles"]

        r_surface = rad_pro["r_surface"][:]
    
        dEkdr.append(np.mean(rad_pro["dEkdr_outflow_exc_sub"][:][5:16]))
        dUdr.append( np.mean(rad_pro["dUdr_outflow_exc_sub"][:][5:16]))
        vrad_fw.append( np.mean(rad_pro["cv_radial_fw_outflow_exc_sub"][:][5:16]))

    dEkdr = np.mean(dEkdr)
    dUdr = np.mean(dUdr)
    vrad_fw = np.mean(vrad_fw)
        
    m200 = mp_group["m200"][:][-1]
    sfr = mp_group["sfr_r0p2_200p0"][:][-1] # Msun / Myr
    
py.rcParams.update(latexParams)
py.rcParams.update({'figure.subplot.wspace':0,'figure.subplot.hspace':0,
                    'figure.figsize':[3.32,2.49],
                    'figure.subplot.left':0.18,
                    'figure.subplot.bottom':0.17,
                    'figure.subplot.top':0.96,
                    'legend.fontsize':8,
                    'font.size':10,
                    'axes.labelsize':10})

nrow = 1; ncol = 1
py.figure()
subplots = panelplot(nrow,ncol)
for n,ax in enumerate(subplots):
    py.axes(ax)

    py.axhline(0.0, c="grey",alpha=0.2)
    py.axhline(-1, c="grey",alpha=0.2)
    py.axhline(-2, c="grey",alpha=0.2)

    # kg kpc^-1 m^2 s^-2 km s^-1 = J /kpc km s^-1
    dEdt = vrad_fw * (dUdr+dEkdr)

    kpc_2_km = 3.09e16 
    yr2s = 365.25*24*60**2
    dEdt *= 1/kpc_2_km * yr2s * 1e7 # erg yr^-1
    
    print dEdt
    
    # msun /yr
    sfr *= 1e-6
    energy_per_gram = 2e16 # erg injected per gram of stars formed
    Msun_gram = 2e33
    efr = energy_per_gram * (sfr*Msun_gram) # erg /yr

    logm200_eagle, logel_eagle = np.loadtxt("energy_loading_eagle_z3.dat", unpack=True)
    py.plot(logm200_eagle, logel_eagle, c="b", linewidth=1.1, label=r"Eagle, $2.5<z<3$")
    
    py.scatter( np.log10(m200), np.log10(dEdt/efr), c="k", s=10, edgecolors="none", label="Mitchell et al. (2021)")

    py.ylim((-2.2, 0.2))

    py.xlabel(r"$\log_{10}(M_{200} \, / \mathrm{M_\odot})$")
    py.ylabel(r"$\log_{10}(\dot{E}_{\mathrm{out}} \, / \dot{E}_{\mathrm{injected}})$")

    py.legend(frameon=False, numpoints=1,scatterpoints=1)

py.savefig("../Figures/energy_loading_eagle_m21_comp.pdf")
py.show()
