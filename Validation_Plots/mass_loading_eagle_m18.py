import numpy as np
import sys
sys.path.append("../")
from utilities_plotting import *

py.rcParams.update(latexParams)
py.rcParams.update({'figure.subplot.wspace':0,'figure.subplot.hspace':0,
                    'figure.figsize':[3.32,2.49],
                    'figure.subplot.left':0.18,
                    'figure.subplot.bottom':0.17,
                    'figure.subplot.top':0.96,
                    'legend.fontsize':8,
                    'font.size':10,
                    'axes.labelsize':10})

nrow = 1; ncol = 1

py.figure()
subplots = panelplot(nrow,ncol)
for n,ax in enumerate(subplots):
    py.axes(ax)

    py.xlabel(r"$\log_{10}(M_{200} \, / \mathrm{M_\odot})$")
    py.ylabel(r"$\log_{10}(\dot{M}_{\mathrm{out}} \, / \dot{M}_\star)$")

    logm200_eagle, logml_eagle = np.loadtxt("mass_loading_eagle_z3.dat",unpack=True)
    py.plot(logm200_eagle, logml_eagle, c="b", label="Eagle", linewidth=1.1)

    logm200_ramses, logml_ramses = np.loadtxt("mass_loading_m18.dat",unpack=True)
    py.scatter(logm200_ramses, logml_ramses,  label="Mitchell et al. (2018)",c="k",edgecolors="None",s=10)

    
    py.legend(frameon=False, numpoints=1,scatterpoints=1)

py.savefig("../Figures/mass_loading_eagle_m18_comp.pdf")
py.show()
