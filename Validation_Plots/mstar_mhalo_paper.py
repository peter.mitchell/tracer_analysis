import numpy as np
import h5py
import sys
sys.path.append("../../Analysis/Obs_Data")
sys.path.append("../")
from utilities_plotting import *
import dm_utils as dm
from mstar_mhalo_empirical import add_parametric_fits

Zm = "Zoom-7-"
run_nfb = "hdRun-LSS150pkpc-new-new-prime"
run_P13  = "hdRun-LSS150pkpc-new-new-prime-fbk"
run_1112 ="hdRun-LSS150pkpc_ncycle_1112"
run_1112_MSN_5 = "hdRun-LSS150pkpc_ncycle_1112_MSN_5"
run_tr = "RhdRun-tracer_radfix"

Zoom_List = [Zm+"5092",Zm+"5092", Zm+"5092"]
Run_List =  [run_nfb, run_1112_MSN_5, run_tr]
timestep_list = [152, 155,155]
c_list = ["m", "k", "r"]
OutDir_List = ["/cral4/mitchell/data/Temp5/" , "/cral4/mitchell/data/Sphinx_Prep/PostProcess/" , "/cral2/mitchell/PostProcess/"]
RunDir_List = ["/cral4/simulations/P13-20h-1Mpc-MUSIC/", "/cral4/simulations/P13-20h-1Mpc-MUSIC/" , "/cral2/mitchell/"]
name_list = ["HD, no feedback", "HD, feedback", "RHD, feedback"]
timesteps_out_list = ["all", "all","all"]

show_eagle = False
show_sats = False

fig_name = "mstar_mhalo_vrot.pdf"

r_aperture = 0.2
r_ap_str = "sm_r" + str(r_aperture).replace(".","p")

# List of the desired redshifts that you want to show (will find closest possible outputs to these)
redshift_desired_list = [3.0]

nrow = 2; ncol = 1

xlo = 9.5; xhi = 11.2
ylo = 6.0; yhi = 10.0

redshifts_show_list = []
timesteps_show_list = []
mstar_list = []
mhalo_list = []
halo_ts_list = []

for Zoom, Run, timestep_final, OutDir, RunDir, name,timesteps_out in zip(Zoom_List, Run_List, timestep_list, OutDir_List, RunDir_List, name_list,timesteps_out_list):

    print Zoom, Run, name

    mstar_list.append([])
    mhalo_list.append([])
    halo_ts_list.append([])

    RunDir += Zoom +"/"+Run


    dm.jeje_utils.read_conversion_scales(RunDir,timestep_final)
    
    omega_0,lambda_0,little_h = dm.jeje_utils.read_cosmo_params(RunDir,timestep_final)
    omega_b = dm.jeje_utils.get_param_real(RunDir,timestep_final,"omega_b")
    fb = omega_b / omega_0
    
    if timesteps_out != "final":
        all_redshifts = np.zeros(timestep_final-1)
        for n, timestep in enumerate(np.arange(2,timestep_final+1)):
            all_redshifts[n] = dm.jeje_utils.get_snap_redshift(RunDir,timestep)
    else:
        all_redshifts = np.array([dm.jeje_utils.get_snap_redshift(RunDir,timestep_final)])

    # Work out which redshifts and timesteps we are going to show for this Zoom,Run
    redshifts_show = np.zeros_like(redshift_desired_list)
    timesteps_show = np.zeros_like(redshift_desired_list)
    for n, redshift_desired in enumerate(redshift_desired_list):
        ind = np.argmin(abs(all_redshifts - redshift_desired))
        redshifts_show[n] = all_redshifts[ind]
        timesteps_show[n] = ind + 2
        
        if timesteps_out == "final" and n == len(redshifts_show)-1:
            timesteps_show[n] = timestep_final

    redshifts_show_list.append(redshifts_show)
    timesteps_show_list.append(timesteps_show)

    # Read the hdf5 file
    if timesteps_out == "all":
        filename = Zoom + "_" + Run + "_tsf" + str(timestep_final) +".hdf5"
    elif timesteps_out == "final":
        filename = Zoom + "_" + Run + "_tsf" + str(timestep_final) +"_ts"+str(timestep_final)+".hdf5"
    else:
        try:
            timestep_out = int(timesteps_out)
            filename = Zoom + "_" + Run + "_tsf" + str(timestep_final) +"_ts"+str(timestep_out)+".hdf5"
        except:
            print "timesteps_out",timesteps_out,",is not valid, sorry!"
            quit()
    File = h5py.File(OutDir+filename,"r")
    print OutDir+filename
    zoom_group = File[Zoom]

    print filename, Zoom

    for halo_group_str in zoom_group:
        # Check this is a halo
        if not "Halo_" in halo_group_str:
            continue

        halo_group = zoom_group[halo_group_str]
        ap_group = halo_group["all_progenitors"]
        mp_group = halo_group["main_progenitors"]

        mstar_list[-1].append(ap_group[r_ap_str][:])
        if show_sats:
            mhalo_list[-1].append(ap_group["m200_max_past"][:]) # Halo mass defined as sum of baryon + dm halo masses
        else:
            mhalo_list[-1].append(ap_group["m200"][:])
        halo_ts_list[-1].append(ap_group["halo_ts"][:])

        #if "main" in halo_group_str:
        #    print "hello", mp_group[r_ap_str][:]#[0:94]
            
py.rcParams.update(latexParams)
py.rcParams.update({'figure.subplot.wspace':0.2,'figure.subplot.hspace':0.2,
                    'figure.figsize':[3.32,4.98*1.1],
                    'figure.subplot.left':0.18,
                    'figure.subplot.bottom':0.09,
                    'figure.subplot.top':0.98,
                    'legend.fontsize':6,
                    'font.size':12,
                    'axes.labelsize':12})

label1=True
label2=True
label3=True

py.figure()
subplots = panelplot(nrow,ncol)
for n,ax in enumerate(subplots):
    py.axes(ax)

    for tick in ax.xaxis.get_major_ticks():
        tick.label1On=True
    
    if n == 0:
    
        if show_eagle:
            z_eagle, m200_eagle, lo_eagle, med_eagle, hi_eagle = np.loadtxt("../Sim_Data/Eagle_mstar_mhalo.dat",unpack=True)
            z_eagle_unique = np.unique(z_eagle)
            ind = np.argmin(abs(z_eagle_unique-redshift_desired_list[n]))
            ok = (np.isnan(lo_eagle)==False) & (z_eagle==z_eagle_unique[ind])
            py.errorbar(m200_eagle[ok],med_eagle[ok],yerr=[abs(med_eagle[ok]-lo_eagle[ok]),abs(med_eagle[ok]-hi_eagle[ok])],fmt="b+",linewidth=1,mew=1,label=r"Eagle",alpha=0.6)

        # Loop over Zoom,Run
        for i_zoom, c in enumerate(c_list):

            # Loop over haloes
            for i, halo_ts in enumerate(halo_ts_list[i_zoom]):
                ok = halo_ts+1 == timesteps_show_list[i_zoom][n] # The +1 is to deal with the mismatch between halomaker and ramses timestep convention

                if i==0 and label1 and c=="m":
                    py.scatter(np.log10(mhalo_list[i_zoom][i][ok]), np.log10(mstar_list[i_zoom][i][ok]), c=c,label=name_list[i_zoom], edgecolors="none",s=5)
                    label1=False
                elif i==0 and label2 and c=="k":
                    py.scatter(np.log10(mhalo_list[i_zoom][i][ok]), np.log10(mstar_list[i_zoom][i][ok]), c=c,label=name_list[i_zoom], edgecolors="none",s=15.0)
                    label2=False
                elif i==0 and label3 and c=="r":
                    py.scatter(np.log10(mhalo_list[i_zoom][i][ok]), np.log10(mstar_list[i_zoom][i][ok]), c=c,label=name_list[i_zoom], edgecolors="none",s=15.0)
                    label3=False
                else:
                    if c=="m":
                        py.scatter(np.log10(mhalo_list[i_zoom][i][ok]), np.log10(mstar_list[i_zoom][i][ok]), c=c, edgecolors="none",s=5)
                    else:
                        py.scatter(np.log10(mhalo_list[i_zoom][i][ok]), np.log10(mstar_list[i_zoom][i][ok]), c=c, edgecolors="none",s=15.0)

        py.annotate((r"$z=%1.1f$" % redshifts_show_list[1][n]),(xlo+0.1*(xhi-xlo),ylo+0.9*(yhi-ylo))) 
        py.xlim((xlo,xhi)); py.ylim((ylo,yhi))
    
        py.xlabel(r"$\log(M_{200} \, / \mathrm{M_\odot})$")
        py.ylabel(r"$\log(M_{\mathrm{*}} \, / \mathrm{M_\odot})$")

        py.plot([0,15],[0,15]+np.log10(fb),label=r"$M_\star = f_{\mathrm{B}} M_{\mathrm{H}}$" )

        add_parametric_fits(redshifts_show_list[0][n],xlo,xhi)

        py.legend(loc="lower right",frameon=True,numpoints=1,scatterpoints=1,handlelength=3)

    else:
        py.rcParams.update({'legend.fontsize':7})

        filename = "rotation_curve.hdf5"
        File_out = h5py.File(filename,"r")
        log_rad_grid = File_out["log_rad_grid"][:]
        vrot_tot_grid = File_out["vrot_tot_grid"][:]
        vrot_gas_grid = File_out["vrot_gas_grid"][:]
        vrot_star_grid = File_out["vrot_star_grid"][:]
        vrot_dm_grid = File_out["vrot_dm_grid"][:]
        File_out.close()

        py.xlabel(r"$\log_{10}(r \, / \mathrm{kpc})$")
        py.ylabel(r"$V_{\mathrm{rot}} \, \mathrm{kms^{-1}}$")

        py.plot(log_rad_grid, vrot_tot_grid,c="k", label = "Total")
        py.plot(log_rad_grid, vrot_gas_grid,c="b", label = "Gas")
        py.plot(log_rad_grid, vrot_star_grid,c="g", label = "Stars")
        py.plot(log_rad_grid, vrot_dm_grid,c="m",label = "Dark matter")

        
        py.legend(loc="upper right",frameon=False,numpoints=1,scatterpoints=1)

        xlo = -3.0; xhi = 2.0
        ylo = 0; yhi = 250.0
        py.xlim((xlo,xhi))
        py.ylim((ylo,yhi))
        py.annotate((r"$z=%1.1f$" % 3.0),(xlo+0.1*(xhi-xlo),ylo+0.9*(yhi-ylo))) 
        
py.savefig("../Figures/"+fig_name)
py.show()
