import numpy as np

timestep = 155
#timestep = 10

Zoom = "Zoom-7-5092"
Run = "RhdRun-tracer_radfix"
timestep_final = 155
timesteps_show = np.arange(5,timestep_final+1)
RT = True
RunDir = "/cral2/mitchell/"+Zoom+"/"+Run
InDir = "/cral2/mitchell/PostProcess/"
OutDir = "/cral2/mitchell/PostProcess/"

HaloDir = RunDir + "/Halos/"
TreeDir = RunDir + "/Trees/"

import sys
import sys
sys.path.append("../")
import dm_utils as dm
from scipy import spatial
import h5py
import utilities_ramses as ur
import neighbour_utils as nu
import match_searchsorted as ms
import cutils as cut
import os 

############## Zoom file IO #########################
# Read in relevant tree information from the hdf5 file
filename_zoom = Zoom + "_" + Run + "_tsf" + str(timestep_final) +".hdf5"

File = h5py.File(OutDir+filename_zoom,"r")

# Open the subvolume file (containing all cell data)
subvol_filename = "subvol_"+filename_zoom
subvol_File = h5py.File(OutDir+subvol_filename,"r")

# Check the file with the tree information already exists
if not Zoom in File:
    print "Error: Tree information for ", Zoom, " in ", filename_zoom, " cannot be read"
    exit()

zoom_group = File[Zoom]

timestep_tree = timestep -1 # Stupid mismatch between merger tree files and everything else
        
# Get conversions for different units
r2kpc, redshift, little_h, m2Msun,rho2msunpkpc3,v2kms = ur.Set_Units(RunDir, timestep)
boxlen_pkpc = dm.jeje_utils.get_param_real(RunDir,timestep,'boxlen') * r2kpc # Box size in proper kpc, using the correct box size
            
print "reading cell data at timestep = ", timestep, "of", timestep_final, ", redshift is", redshift

########## Read cell data #########################
ts_group = subvol_File["timestep_"+str(timestep)]
cell_group = ts_group["Gas_leaf_cell_data"]
print "Reading rho"
rho_snap = cell_group["rho_c"][:] # Gas density
print "Reading xx"
cx = cell_group["x_c"][:] # Cell coordinates
print "Reading cy"
cy = cell_group["y_c"][:]
print "Reading cz"
cz = cell_group["z_c"][:]
print "Reading clev"
clev = cell_group["lev"][:] # Cell refinement level

csize = boxlen_pkpc / 2.**clev # proper kpc
cmass = rho_snap * rho2msunpkpc3 * csize**3
    
# Add stars and dark matter to estimate the specific gravitional potential energy of each tracer
print "Reading stars"
star_group = ts_group["Stellar_particle_data"]
sx = star_group["x_s"][:]
sy = star_group["y_s"][:]
sz = star_group["z_s"][:]
sm = star_group["m_s"][:] * m2Msun

print "Reading dark matter"
dm_group = ts_group["Dark_matter_particle_data"]
dx = dm_group["x_dm"][:] # particle coordinates
dy = dm_group["y_dm"][:]
dz = dm_group["z_dm"][:]
dmass = dm_group["m_dm"][:] * m2Msun # particle mass in Msun
n_dm = len(dx)

print "IO finished"

# Add cells, gas and stars together
dx = np.concatenate([cx, sx, dx])
dy = np.concatenate([cy, sy, dy])
dz = np.concatenate([cz, sz, dz])
dmass = np.concatenate([cmass,sm,dmass])

dindex = np.arange(0,len(dmass))
cindex = np.arange(0,len(cmass))
sindex = np.arange(0,len(sm))+cindex.max()+1
dm_index = np.arange(0,n_dm)+sindex.max()+1

######### Efficiently find tracers inside rvir ###########

dxyz_min = min(dx.min(),dy.min(),dz.min())
dxyz_max = max(dx.max(),dy.max(),dz.max())

dx_rescale = (dx - dxyz_min) / (dxyz_max-dxyz_min)
dy_rescale = (dy - dxyz_min) / (dxyz_max-dxyz_min)
dz_rescale = (dz - dxyz_min) / (dxyz_max-dxyz_min)
        
nchain_cells = 20
nextincell_d,firstincell_d = nu.neighbour_utils.init_chain(dx_rescale,dy_rescale,dz_rescale,nchain_cells)

# Factor to help convert into box units (note maxime's merger_tree reader has converted halo positions so be careful here!)
r2kpc_ideal, redshift, little_h, m2Msun,junk,junk = ur.Set_Units(RunDir, timestep, "ideal")
boxlen_cMpc = dm.jeje_utils.get_param_real(RunDir,timestep,'boxlen') # Box size in box units
boxlen_cMpc *= r2kpc_ideal
boxlen_cMpc = boxlen_cMpc * (1+redshift) /1e3 # cMpc (comoving)
boxlen_cMpc = boxlen_cMpc * little_h # h^-1 cMpc
factor = 1./boxlen_cMpc

# Get target halo properties
for halo_group_str in zoom_group:
    # Only compute stuff for the main halo
    if "_main" in halo_group_str:
        halo_group_str_use= halo_group_str

mp_group = zoom_group[halo_group_str_use+"/main_progenitors"]
        
# Identify the main progenitor at this timestep
ok_ts = np.array(mp_group["halo_ts"])==timestep_tree # haloes on this timestep

xh = mp_group["x"][ok_ts] * factor + 0.5
yh = mp_group["y"][ok_ts] * factor + 0.5
zh = mp_group["z"][ok_ts] * factor + 0.5

# Use r200 for central haloes, and rvir (for now) for satellite haloes
if mp_group["host_halo_id"][ok_ts] == mp_group["halo_id"][ok_ts]:
    rvirh = mp_group["r200"][ok_ts] / r2kpc           
else:
    rvirh = mp_group["rvir"][ok_ts] * factor

# Select cells in the vicinity of the halo (using a chain algorithm)
rmax = rvirh * 2 # Go out to twice halo virial radius now

#### Cells + DM + stars within the virial radius ####
xh_rescale = (xh-dxyz_min)/(dxyz_max-dxyz_min)
yh_rescale = (yh-dxyz_min)/(dxyz_max-dxyz_min)
zh_rescale = (zh-dxyz_min)/(dxyz_max-dxyz_min)
rmax_rescale = rmax/(dxyz_max-dxyz_min) * 0.5 # No need to beyond r_vir for grav potential (rmax now set to 2 rvir)

ncell_in_rmax = nu.neighbour_utils.get_npart_in_sphere_with_chain(RunDir,timestep,dx_rescale,dy_rescale,dz_rescale,xh_rescale,yh_rescale,zh_rescale,rmax_rescale,firstincell_d,nextincell_d)
if ncell_in_rmax > 0:
    select_d = nu.neighbour_utils.get_part_indicies_in_sphere_with_chain(RunDir,timestep,dx_rescale,dy_rescale,dz_rescale,xh_rescale,yh_rescale,zh_rescale,rmax_rescale,ncell_in_rmax,firstincell_d,nextincell_d)
else:
    select_d = np.zeros_like(dx) < 0

# Estimate gravitational potential within each cell
dx_h = dx[select_d]
dy_h = dy[select_d]
dz_h = dz[select_d]
dmass_h = dmass[select_d]
dindex_h = dindex[select_d]

dx_h = dx_h - xh; dy_h = dy_h - yh; dz_h = dz_h -zh
drad_h = np.sqrt(np.square(dx_h) + np.square(dy_h) + np.square(dz_h))
drad_h *= r2kpc

order = np.argsort(drad_h)
dmass_h_sorted = dmass_h[order]

# Work out mass interior to each radius (slightly approximate because of cell extents)
dmass_h_cum = np.cumsum(dmass_h_sorted)

drad_h = np.sort(drad_h)
dindex_h = dindex_h[order]
dmass_h = dmass_h[order]

kpc = 3.0857e19
G = 6.67408e-11 # m^3 kg^-1 s^-2
Msun = 1.98847e30 # kg
vrot = np.sqrt( G * dmass_h_cum*Msun / (drad_h*kpc)) / 1e3 # kms

subvol_File.close()
File.close()

# Compute separate rotation curves only including mass from gas/stars/dark matter
# Gas
ptr = ms.match(cindex, dindex_h)
ok_match = ptr >= 0

crad = drad_h[ptr][ok_match]
order = np.argsort(crad)
crad = crad[order]
cmass = dmass_h[ptr][ok_match][order]
cmass_cum = np.cumsum(cmass)
vrot_gas = np.sqrt( G * cmass_cum*Msun / (crad*kpc)) / 1e3

# Stars
ptr = ms.match(sindex, dindex_h)
ok_match = ptr >= 0

srad = drad_h[ptr][ok_match]
order = np.argsort(srad)
srad = srad[order]
smass = dmass_h[ptr][ok_match][order]
smass_cum = np.cumsum(smass)
vrot_star = np.sqrt( G * smass_cum*Msun / (srad*kpc)) / 1e3

# Dark matter
ptr = ms.match(dm_index, dindex_h)
ok_match = ptr >= 0

dm_rad = drad_h[ptr][ok_match]
order = np.argsort(dm_rad)
dm_rad = dm_rad[order]
dm_mass = dmass_h[ptr][ok_match][order]
dm_mass_cum = np.cumsum(dm_mass)
vrot_dm = np.sqrt( G * dm_mass_cum*Msun / (dm_rad*kpc)) / 1e3

# Put back onto a grid
logr_min = np.min(drad_h)
logr_max = np.max(drad_h)
log_rad_grid = np.arange(np.log10(logr_min), np.log10(logr_max), 0.05)

vrot_tot_grid = np.zeros_like(log_rad_grid) + np.nan
vrot_gas_grid = np.zeros_like(log_rad_grid) + np.nan
vrot_star_grid = np.zeros_like(log_rad_grid) + np.nan
vrot_dm_grid = np.zeros_like(log_rad_grid) + np.nan

for i, logr in enumerate(log_rad_grid):
    vrot_tot_grid[i] = vrot[np.argmin(abs(np.log10(drad_h)-logr))]
    vrot_gas_grid[i] = vrot_gas[np.argmin(abs(np.log10(crad)-logr))]
    vrot_star_grid[i] = vrot_star[np.argmin(abs(np.log10(srad)-logr))]
    vrot_dm_grid[i] = vrot_dm[np.argmin(abs(np.log10(dm_rad)-logr))]

# Write data to disk
filename = "rotation_curve.hdf5"
if os.path.isfile(filename):
    os.remove(filename)
File_out = h5py.File(filename,"a")
File_out.create_dataset("log_rad_grid", data=log_rad_grid)
File_out.create_dataset("vrot_tot_grid", data=vrot_tot_grid)
File_out.create_dataset("vrot_gas_grid", data=vrot_gas_grid)
File_out.create_dataset("vrot_star_grid", data=vrot_star_grid)
File_out.create_dataset("vrot_dm_grid", data=vrot_dm_grid)
File_out.close()

# Plot the rotation curves
from utilities_plotting import *

py.rcParams.update(latexParams)
py.rcParams.update({'figure.subplot.wspace':0,'figure.subplot.hspace':0,
                    'figure.figsize':[3.32,2.49],
                    'figure.subplot.left':0.17,
                    'figure.subplot.bottom':0.17,
                    'figure.subplot.top':0.98,
                    'legend.fontsize':8,
                    'font.size':10,
                    'axes.labelsize':10})

nrow = 1; ncol = 1

py.figure()
subplots = panelplot(nrow,ncol)
for n,ax in enumerate(subplots):
    py.axes(ax)

    py.xlabel(r"$\log_{10}(r \, / \mathrm{kpc})$")
    py.ylabel(r"$V_{\mathrm{rot}} \, \mathrm{kms^{-1}}$")
    
    py.plot(log_rad_grid, vrot_tot_grid,c="k", label = "Total")
    py.plot(log_rad_grid, vrot_gas_grid,c="b", label = "Gas")
    py.plot(log_rad_grid, vrot_star_grid,c="g", label = "Stars")
    py.plot(log_rad_grid, vrot_dm_grid,c="m",label = "Dark matter")

    py.legend(frameon=False)

py.savefig("../Figures/rotation_curve_"+str(timestep)+".pdf")
py.show()
