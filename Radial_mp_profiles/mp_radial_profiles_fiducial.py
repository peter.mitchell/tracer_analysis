import numpy as np
import h5py
from utilities_plotting import *
import matplotlib.backends.backend_agg
from matplotlib.colors import LogNorm
from mpl_toolkits.axes_grid1.inset_locator import inset_axes
import utilities_ramses as ur
import cutils as cut
import dm_utils as dm
import utilities_cosmology as uc

redshift_lo = 3.0
redshift_hi = 4.0
show_sub = False
weight_scheme = "none"
weight_noutput = True # Weight haloes inversely on how many outputs they contribute to the stack
radial_stack_scheme = "dmdr_R"

yhi_m = 8.2
yhi_p = 10.0
yhi_e = 11.95

if radial_stack_scheme == "dmdr_R":
    yhi_m += 1.3; yhi_p += 1.3; yhi_e += 1.3

ylo_m = yhi_m -3.49
ylo_p = yhi_p -3.49
ylo_e = yhi_e -3.49

'''Zoom_List_fb = ["Zoom-7-5092"]
Run_List_fb = ["hdRun-LSS150pkpc_ncycle_1112_MSN_5"]
timestep_list_fb = [155]
OutDir = "/cral4/mitchell/data/Sphinx_Prep/PostProcess/"
RunDir_base = "/cral4/simulations/P13-20h-1Mpc-MUSIC/"'''

#Zoom_List_fb = ["Zoom-7-5092"]
#Run_List_fb = ["RhdRun-tracer_norad_bug"]
#timestep_list_fb = [105]
#OutDir = "/cral4/mitchell/data/Tracer_Test/PostProcess/"
#RunDir_base = "/cral2/mitchell/"

Zoom_List_fb = ["Zoom-7-5092"]
Run_List_fb = ["RhdRun-tracer_radfix"]
timestep_list_fb = [155]
OutDir = "/cral2/mitchell/PostProcess/"
RunDir_base = "/cral2/mitchell/"

fig_name = "/home/cral/mitchell/Tracer_Analysis/Figures/mp_radial_profiles_"+Run_List_fb[0]+".pdf"

############ Compute the number of simulation outputs that are being stacked ###########
n_stack = 0 # Total number of simulation outputs in the stack
n_haloes = 0
for Zoom, Run, timestep_final in zip(Zoom_List_fb, Run_List_fb, timestep_list_fb):
    
    RunDir = RunDir_base + Zoom+"/"+Run
    filename = Zoom + "_" + Run + "_tsf" + str(timestep_final) +".hdf5"
    File = h5py.File(OutDir+filename)
    zoom_group = File[Zoom]
    for halo_group_str in zoom_group:
        # Check this is the desired halo                                                               
        if not "main" in halo_group_str:
            continue
        halo_group = zoom_group[halo_group_str]

    mp_group = halo_group["main_progenitors"]
    mhalo = mp_group["m200_max_past"][:]
    halo_ts = mp_group["halo_ts"][:]
    # Compute halo masses to check if this halo will be included in the stack
    all_redshifts = np.zeros(len(halo_ts))
    for n, timestep_tree in enumerate(halo_ts):
        timestep = timestep_tree+1
        all_redshifts[n] = dm.jeje_utils.get_snap_redshift(RunDir,timestep)
    select = (all_redshifts >= redshift_lo) & (all_redshifts <= redshift_hi)
    mhalo_final = mhalo[-1]
    mhalo = mhalo[select]

    # Compute which timesteps to read in based on the desired redshift range
    all_redshifts = np.zeros(timestep_final-1)
    for n, timestep in enumerate(np.arange(2,timestep_final+1)):
        all_redshifts[n] = dm.jeje_utils.get_snap_redshift(RunDir,timestep)
    select = (all_redshifts >= redshift_lo) & (all_redshifts <= redshift_hi)
    n_stack += len(all_redshifts[select])
    n_haloes += 1


omega_0,lambda_0,little_h = dm.jeje_utils.read_cosmo_params(RunDir,timestep)
# Compute time window for stacking
a_start = 1./(1.+redshift_hi)
t_start,junk = uc.t_Universe(a_start, omega_0,little_h)
a_stop = 1./(1.+redshift_lo)
t_stop,junk = uc.t_Universe(a_stop, omega_0, little_h)
dt_stack = t_stop - t_start # Gyr
print "Stacking interval is ", dt_stack, "Gyr"    


########## Read the data and do the stacking ####################

init_stack = True

for Zoom, Run, timestep_final in zip(Zoom_List_fb, Run_List_fb, timestep_list_fb):
    # Read the hdf5 file
    filename = Zoom + "_" + Run + "_tsf" + str(timestep_final) +".hdf5"
    File = h5py.File(OutDir+filename)
    zoom_group = File[Zoom]
    RunDir = RunDir_base+Zoom+"/"+Run

    for halo_group_str in zoom_group:
        # Check this is the desired halo
        if not "main" in halo_group_str:
            continue
        halo_group = zoom_group[halo_group_str]

    mp_group = halo_group["main_progenitors"]
    mhalo = mp_group["m200_max_past"][:]
    rhalo = mp_group["r200_max_past"][:]
    halo_ts = mp_group["halo_ts"][:]
    # Compute halo masses to read in based on desired redshift range
    all_redshifts = np.zeros(len(halo_ts))
    for n, timestep_tree in enumerate(halo_ts):
        timestep = timestep_tree+1
        all_redshifts[n] = dm.jeje_utils.get_snap_redshift(RunDir,timestep)
    select = (all_redshifts >= redshift_lo) & (all_redshifts <= redshift_hi)
    mhalo_final = mhalo[-1]
    mhalo = mhalo[select]; rhalo = rhalo[select]
    
    print "Reading", Zoom, Run
        
    # Compute which flux timesteps to read in based on the desired redshift range
    all_redshifts = np.zeros(timestep_final-1)
    all_ts = np.zeros(timestep_final-1)
    for n, timestep in enumerate(np.arange(2,timestep_final+1)):
        all_redshifts[n] = dm.jeje_utils.get_snap_redshift(RunDir,timestep)
        all_ts[n] = timestep
    select = (all_redshifts >= redshift_lo) & (all_redshifts <= redshift_hi)
    
    n_outputs = len(all_ts[select])

    for nts, timestep in enumerate(all_ts[select]):
        
        radial_group = halo_group["fluxes/timestep_"+str(int(timestep))+"/radial_profiles"]
        r_surface = radial_group["r_surface"][:]

        selections = ["", "_n", "_i", "_wi", "_Zn", "_Zi", "_Zwi"]

        for i_select, select in enumerate(selections):

            select_str1 = "_inflow"
            select_str2 = "_outflow"
            
            select_str1 += select
            select_str2 += select

            if not show_sub:
                select_str1 += "_exc_sub"
                select_str2 += "_exc_sub"

            g2Msun = 5.02739933e-34
            Msun2kg = 1e-3 * 1./g2Msun

            dmdt_in = radial_group["dmdt"+select_str1][:]
            dmdr_in = radial_group["dmdr"+select_str1][:]

            if "Z" not in select_str1:
                dEkrdr_in = radial_group["dEkrdr"+select_str1][:] / Msun2kg / (1.0e6) # Msun km^2 s^-2 kpc^-1
                dEkdr_in = radial_group["dEkdr"+select_str1][:] / Msun2kg / (1.0e6) # Msun km^2 s^-2 kpc^-1
                dUdr_in = radial_group["dUdr"+select_str1][:] / Msun2kg / (1.0e6) # Msun km^2 s^-2 kpc^-1

            dmdt_out = radial_group["dmdt"+select_str2][:]
            dmdr_out = radial_group["dmdr"+select_str2][:]
            if "Z" not in select_str2:
                dEkrdr_out = radial_group["dEkrdr"+select_str2][:] / Msun2kg / (1.0e6) # Msun km^2 s^-2 kpc^-1
                dEkdr_out = radial_group["dEkdr"+select_str2][:] / Msun2kg / (1.0e6) # Msun km^2 s^-2 kpc^-1
                dUdr_out = radial_group["dUdr"+select_str2][:] / Msun2kg / (1.0e6) # Msun km^2 s^-2 kpc^-1

            if weight_noutput: # Weight haloes inversely proportional to their number of outputs
                n_weight = n_haloes * n_outputs
            else:
                n_weight = n_stack

            if weight_scheme == "none":
                norm = 1./float(n_weight)
            elif weight_scheme == "mhalo":
                norm = 1./float(n_weight) * mhalo_final / mhalo[nts] # weight inversely by halo mass, normalized by final halo mass
            elif weight_scheme == "mhalo_11": # weight by final halo mass, normalized to a 10^11 halo
                norm = 1./float(n_weight) * (10**11 / mhalo_final)
            elif weight_scheme == "mhalo_11_zdep": # weight by halo mass, normalized to a 10^11 halo
                norm = 1./float(n_weight) * 10**11/mhalo[nts]

            # Stack in pkpc
            if radial_stack_scheme == "none":
                print "not implemented yet"
                quit()
            # Keep dmdr in Msun kpc^-1 but stack in r/R
            elif radial_stack_scheme == "dmdr":
                do_nothing = 1 # ;)                                
            # Change dmdr to dmd(r/R) in Msun, this ensures that the integral under the curve will be actually correct.
            elif radial_stack_scheme == "dmdr_R":
                norm *= rhalo[nts]
            else:
                print "Panic"
                quit()

            if init_stack:
                if i_select == 0:
                    dmdt_in_stack = norm*dmdt_in; dmdr_in_stack = norm*dmdr_in; dEkrdr_in_stack = norm*dEkrdr_in; dEkdr_in_stack = norm*dEkdr_in; dUdr_in_stack = norm*dUdr_in
                    dmdt_out_stack = norm*dmdt_out; dmdr_out_stack = norm*dmdr_out; dEkrdr_out_stack = norm*dEkrdr_out; dEkdr_out_stack = norm*dEkdr_out; dUdr_out_stack = norm*dUdr_out
                elif i_select ==1:
                    dmdt_in_n_stack = norm*dmdt_in; dmdr_in_n_stack = norm*dmdr_in; dEkrdr_in_n_stack = norm*dEkrdr_in; dEkdr_in_n_stack = norm*dEkdr_in; dUdr_in_n_stack = norm*dUdr_in
                    dmdt_out_n_stack = norm*dmdt_out; dmdr_out_n_stack = norm*dmdr_out; dEkrdr_out_n_stack = norm*dEkrdr_out; dEkdr_out_n_stack = norm*dEkdr_out; dUdr_out_n_stack = norm*dUdr_out
                elif i_select ==2:
                    dmdt_in_i_stack = norm*dmdt_in; dmdr_in_i_stack = norm*dmdr_in; dEkrdr_in_i_stack = norm*dEkrdr_in; dEkdr_in_i_stack = norm*dEkdr_in; dUdr_in_i_stack = norm*dUdr_in
                    dmdt_out_i_stack = norm*dmdt_out; dmdr_out_i_stack = norm*dmdr_out; dEkrdr_out_i_stack = norm*dEkrdr_out; dEkdr_out_i_stack = norm*dEkdr_out; dUdr_out_i_stack = norm*dUdr_out
                elif i_select ==3:
                    dmdt_in_wi_stack = norm*dmdt_in; dmdr_in_wi_stack = norm*dmdr_in; dEkrdr_in_wi_stack = norm*dEkrdr_in; dEkdr_in_wi_stack = norm*dEkdr_in; dUdr_in_wi_stack = norm*dUdr_in
                    dmdt_out_wi_stack = norm*dmdt_out; dmdr_out_wi_stack = norm*dmdr_out; dEkrdr_out_wi_stack = norm*dEkrdr_out; dEkdr_out_wi_stack = norm*dEkdr_out; dUdr_out_wi_stack = norm*dUdr_out
                elif i_select ==4:
                    dmdt_in_Zn_stack = norm*dmdt_in; dmdr_in_Zn_stack = norm*dmdr_in
                    dmdt_out_Zn_stack = norm*dmdt_out; dmdr_out_Zn_stack = norm*dmdr_out
                elif i_select ==5:
                    dmdt_in_Zi_stack = norm*dmdt_in; dmdr_in_Zi_stack = norm*dmdr_in
                    dmdt_out_Zi_stack = norm*dmdt_out; dmdr_out_Zi_stack = norm*dmdr_out
                else:
                    dmdt_in_Zwi_stack = norm*dmdt_in; dmdr_in_Zwi_stack = norm*dmdr_in
                    dmdt_out_Zwi_stack = norm*dmdt_out; dmdr_out_Zwi_stack = norm*dmdr_out
            else:
                if i_select == 0:
                    dmdt_in_stack += norm*dmdt_in; dmdr_in_stack += norm*dmdr_in; dEkrdr_in_stack += norm*dEkrdr_in; dEkdr_in_stack += norm*dEkdr_in; dUdr_in_stack += norm*dUdr_in
                    dmdt_out_stack += norm*dmdt_out; dmdr_out_stack += norm*dmdr_out; dEkrdr_out_stack += norm*dEkrdr_out; dEkdr_out_stack += norm*dEkdr_out; dUdr_out_stack += norm*dUdr_out
                elif i_select == 1:
                    dmdt_in_n_stack += norm*dmdt_in; dmdr_in_n_stack += norm*dmdr_in; dEkrdr_in_n_stack += norm*dEkrdr_in; dEkdr_in_n_stack += norm*dEkdr_in; dUdr_in_n_stack += norm*dUdr_in
                    dmdt_out_n_stack += norm*dmdt_out; dmdr_out_n_stack += norm*dmdr_out; dEkrdr_out_n_stack += norm*dEkrdr_out; dEkdr_out_n_stack += norm*dEkdr_out; dUdr_out_n_stack += norm*dUdr_out
                elif i_select ==2:
                    dmdt_in_i_stack += norm*dmdt_in; dmdr_in_i_stack += norm*dmdr_in; dEkrdr_in_i_stack += norm*dEkrdr_in; dEkdr_in_i_stack += norm*dEkdr_in; dUdr_in_i_stack += norm*dUdr_in
                    dmdt_out_i_stack += norm*dmdt_out; dmdr_out_i_stack += norm*dmdr_out; dEkrdr_out_i_stack += norm*dEkrdr_out; dEkdr_out_i_stack += norm*dEkdr_out; dUdr_out_i_stack += norm*dUdr_out
                elif i_select==3:
                    dmdt_in_wi_stack += norm*dmdt_in; dmdr_in_wi_stack += norm*dmdr_in; dEkrdr_in_wi_stack += norm*dEkrdr_in; dEkdr_in_wi_stack += norm*dEkdr_in; dUdr_in_wi_stack += norm*dUdr_in
                    dmdt_out_wi_stack += norm*dmdt_out; dmdr_out_wi_stack += norm*dmdr_out; dEkrdr_out_wi_stack += norm*dEkrdr_out; dEkdr_out_wi_stack += norm*dEkdr_out; dUdr_out_wi_stack += norm*dUdr_out
                elif i_select ==4:
                    dmdt_in_Zn_stack += norm*dmdt_in; dmdr_in_Zn_stack += norm*dmdr_in
                    dmdt_out_Zn_stack += norm*dmdt_out; dmdr_out_Zn_stack += norm*dmdr_out
                elif i_select ==5:
                    dmdt_in_Zi_stack += norm*dmdt_in; dmdr_in_Zi_stack += norm*dmdr_in
                    dmdt_out_Zi_stack += norm*dmdt_out; dmdr_out_Zi_stack += norm*dmdr_out
                else:
                    dmdt_in_Zwi_stack += norm*dmdt_in; dmdr_in_Zwi_stack += norm*dmdr_in
                    dmdt_out_Zwi_stack += norm*dmdt_out; dmdr_out_Zwi_stack += norm*dmdr_out

        init_stack = False


# Plotting
ncol = 1; nrow = 2

py.rcParams.update(latexParams)
py.rcParams.update({'figure.subplot.wspace':0.0,'figure.subplot.hspace':0.,
                    'figure.figsize':[3.32*ncol,2.49*nrow],
                    'figure.subplot.left':0.2,
                    'figure.subplot.right':0.955,
                    'figure.subplot.bottom':0.09,
                    'figure.subplot.top':0.97,
                    'legend.fontsize':7.0,
                    'axes.labelsize':10})

fig = py.figure()
subplots = panelplot(nrow,ncol)
for n,ax in enumerate(subplots):
    py.axes(ax)
    
    xlo = 0.1; xhi = 1.0

    if n == 0:

        ylo = ylo_m; yhi = yhi_m
       
        py.plot(r_surface, np.log10(dmdr_out_n_stack), c="b", label = "Neutral hydrogen", linewidth = 1.2)
        py.plot(r_surface, np.log10(dmdr_out_wi_stack), c="g", label = "Warm-ionized hydrogen", linewidth = 1.2)      
        py.plot(r_surface, np.log10(dmdr_out_i_stack-dmdr_out_wi_stack), c="m", label = "Hot-ionized hydrogen", linewidth = 1.2)      
        

        py.plot(r_surface, np.log10(dmdr_in_n_stack), c="b", linewidth = 1.2, linestyle='--')
        py.plot(r_surface, np.log10(dmdr_in_i_stack-dmdr_in_wi_stack), c="m", linestyle='--',linewidth=1.2)
        py.plot(r_surface, np.log10(dmdr_in_wi_stack), c="g", linewidth = 1.2, linestyle='--')

        if radial_stack_scheme == "dmdr":
            py.ylabel(r"$\log(\, \left<\frac{\mathrm{d}m}{\mathrm{d}r}\right> \, / \mathrm{M_\odot kpc^{-1}})$")
        else:
            py.ylabel(r"$\log(\, \left<\frac{\mathrm{d}m}{\mathrm{d}(r/R_{\mathrm{vir}})}\right> \, / \mathrm{M_\odot})$")
    
    if n == 1:

        ylo = ylo_p; yhi = yhi_p
       
        py.plot(r_surface, np.log10(dmdt_out_n_stack), c="b", linewidth = 1.2)
        py.plot(r_surface, np.log10(dmdt_out_wi_stack), c="g", linewidth = 1.2)      
        py.plot(r_surface, np.log10(dmdt_out_i_stack-dmdt_out_wi_stack), c="m", linewidth = 1.2)      
        

        py.plot(r_surface, np.log10(dmdt_in_n_stack), c="b", linewidth = 1.2, linestyle='--')
        py.plot(r_surface, np.log10(dmdt_in_i_stack-dmdt_in_wi_stack), c="m", linestyle='--',linewidth=1.2)
        py.plot(r_surface, np.log10(dmdt_in_wi_stack), c="g", linewidth = 1.2, linestyle='--')

        if radial_stack_scheme == "dmdr":
            py.ylabel(r"$\log(\, \left<\frac{\mathrm{d}p}{\mathrm{d}r}\right> \, / \mathrm{M_\odot kms^{-1} kpc^{-1}})$")
        else:
            py.ylabel(r"$\log(\, \left<\frac{\mathrm{d}p}{\mathrm{d}(r/R_{\mathrm{vir}})}\right> \, / \mathrm{M_\odot kms^{-1}})$")
    
        py.xlabel("$r \, / R_{\mathrm{vir}}$")

        py.plot(r_surface-99,r_surface-99,c="k", linewidth=1.2,label="Outflow")
        py.plot(r_surface-99,r_surface-99,c="k", linewidth=1.2,label="Inflow",linestyle='--')
        
    py.xlim((xlo, xhi))
    py.ylim((ylo,yhi))

    py.legend(loc="lower left",frameon=False,numpoints=1,handlelength=3)


py.savefig(fig_name)
py.show()
