import matplotlib.pyplot as py
import numpy as np
import h5py

im_path = "/cral4/mitchell/data/Tracer_Test/PostProcess/Maps/Zoom-7-1290_tracer_run_lr/all_ts/rho_gas_tot/rho_gas_tot_ts_"
timestep_show = 30
im_path += str(timestep_show)+".png"
img_in = py.imread(im_path)

#img_in = py.imread("rho_gas_neutral_exc_sub_ts_90.png")
#img_in = py.imread("rho_gas_tot_ts_10.png")


File = h5py.File("saved_tracers_select.hdf5")
tx = File["x"][:]
ty = File["y"][:]
tz = File["z"][:]
timesteps = File["timestep"][:]
File.close()
n_select = len(tx[0])

i_show = np.where(timesteps==timestep_show)[0]

# Empericically found halo centres and virial radii
panel1_x = 0.718; panel1_y = 0.5; rvir = 0.31
panel2_x = 1.539
panel3_x = 2.359

tx *= 0.31
ty *= 0.31
tz *= 0.31

#np.random.seed(23231)
#np.random.seed(512312)
np.random.seed(174283)
order = np.arange(0,n_select)
np.random.shuffle(order)
parts_show = np.arange(0,10)

i_part = 7

py.rcParams.update({'figure.subplot.wspace':0,'figure.subplot.hspace':0,
                    'figure.figsize':[12,4],
                    'figure.subplot.left':0.00,
                    'figure.subplot.bottom':0.0,
                    'figure.subplot.top':1.0,
                    'figure.subplot.right':1.0,
                    'font.size':6,
                    'axes.labelsize':8,
                    'legend.fontsize':7})


def Plot_Circle(x0,y0,r,ax,c="k"):
    theta = np.arange(0,2*np.pi+0.01,0.01)
    x = x0 + np.cos(theta)*r
    y = y0 + np.sin(theta)*r
    Circle = ax.plot(x,y,c=c,linewidth=0.3)
    return Circle


fig, ax = py.subplots(dpi=160)#figsize=(6,2))
img = ax.imshow(img_in)

img.set_extent((0,3,0,1))

#Cicle1 = Plot_Circle(panel1_x, panel1_y, rvir, ax,c="r")
#py.scatter([panel1_x],[panel1_y])

#Cicle1 = Plot_Circle(panel2_x, panel1_y, rvir, ax,c="r")
#py.scatter([panel2_x],[panel1_y])

#Cicle1 = Plot_Circle(panel3_x, panel1_y, rvir, ax,c="r")
#py.scatter([panel3_x],[panel1_y])

# Plot tracer particle track xy zx yz
py.plot(tx[:,order[i_part]]+panel1_x, ty[:,order[i_part]]+ panel1_y, c="k")
py.scatter(tx[i_show,order[i_part]]+panel1_x, ty[i_show,order[i_part]]+ panel1_y)

py.plot(tz[:,order[i_part]]+panel2_x, tx[:,order[i_part]]+ panel1_y, c="k")
py.scatter(tz[i_show,order[i_part]]+panel2_x, tx[i_show,order[i_part]]+ panel1_y)

py.plot(ty[:,order[i_part]]+panel3_x, tz[:,order[i_part]]+ panel1_y, c="k")
py.scatter(ty[i_show,order[i_part]]+panel3_x, tz[i_show,order[i_part]]+ panel1_y)


py.xlim((0.15,2.95))
py.ylim((0.033,0.967))

py.savefig("test.png")
py.show()
