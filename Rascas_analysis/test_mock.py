print "imports..."
import numpy as np
import jphot as jp
import mocks
import sys
sys.path.append("../.")
import dm_utils as dm
#from utilities_plotting import *
print "done"

# Choose simulation/snapshot
Zoom = "Zoom-7-5092"
Run = "RhdRun-tracer_radfix"
RunDir = "/cral2/mitchell/"+Zoom+"/"+Run
timestep = 149

# Choose which viewing angle to look at (between 1 and 12 by default)
idirection = 12


timestep_str = str(timestep).rjust(5,'0')
ex_rc_path = "/cral2/mitchell/Zoom-7-5092/RhdRun-tracer_radfix/RASCAS/"+timestep_str+"/ColLya_CS100_6arcsec/"
icFile_path = ex_rc_path + "001.IC"
resFile_path = ex_rc_path + "001.res"

p = jp.photonlist(icFile_path, resFile_path)

# Checking something..
ex_rc_path2 = "/cral2/mitchell/Zoom-7-5092/RhdRun-tracer_radfix/RASCAS/"+timestep_str+"/ColLya_CS100_Rvir/"
icFile_path2 = ex_rc_path2 + "001.IC"
resFile_path2 = ex_rc_path2 + "001.res"

p2 = jp.photonlist(icFile_path2, resFile_path2)


# Set units/constants
dm.jeje_utils.read_conversion_scales(RunDir,timestep)
omega_0,lambda_0,little_h = dm.jeje_utils.read_cosmo_params(RunDir,timestep)
redshift = dm.jeje_utils.get_snap_redshift(RunDir,timestep)

from astropy.cosmology import FlatLambdaCDM
cosmo = FlatLambdaCDM(H0=little_h*100,Om0=omega_0)
a =  cosmo.arcsec_per_kpc_proper(redshift)
unit_l_pkpc = dm.jeje_utils.dp_scale_l * dm.jeje_utils.cm2pc / 1000. # converts comoving code units to phys. kpc
unit_l_arcsec = unit_l_pkpc * a.value

# Checking
#print unit_l_pkpc * (1+redshift) * little_h * 1e-3 # Should be roughly the boxsize in cMpc/h - so this one is ok
#print 40 * unit_l_arcsec / unit_l_pkpc # This should be about 5 arcsec at z=3

print len(p.x)
print (p.x.max()-np.median(p.x_ic)) * unit_l_pkpc # 6 arcsec run
print len(p2.x_ic)
print (p2.x_ic.max()-np.median(p2.x_ic)) * unit_l_pkpc # Rvir run
#print p.x_ic.min()*unit_l_pkpc, p2.x_ic.min()*unit_l_pkpc,
print np.median(p.x_ic)*unit_l_pkpc
quit()

lumdist = cosmo.luminosity_distance(redshift)
lumdist_cm = lumdist.cgs.value

# Read data
mock = mocks.mockobs(ex_rc_path, "001", ICFile="001.IC", load_cube=True, idirection=idirection, \
                     unit_l_arcsec=unit_l_arcsec, lumdist_cm=lumdist_cm, redshift=redshift)

print "Total cube dimensions", mock.cube.shape
print "n Wavelengths [Ang]", len(mock.cube_lbda_Angstrom)
print "n x [arcsec]", len(mock.cube_x_arcsec), mock.cube_x_arcsec

print np.sum(mock.cube)

import matplotlib.pyplot as plt
mock.show_cube_image()
plt.show()




