print "imports..."
import numpy as np
import jphot as jp
import mocks
import sys
sys.path.append("../.")
import dm_utils as dm
from astropy.cosmology import FlatLambdaCDM
from utilities_plotting import *
import h5py
print "done"

next step is to convolve by the MUSE PSF
and to add the Lutz data points
(save figure also ;))

# Stack LAEs along given directions if they have flux above some cut
# See Fig.1 from Leclercq 17 to see a flux distribution of UDF LAEs
flux_cuts = np.array([0.0, 1e-18, 10.0**-17.5]) # erg s-1 cm^-2

# Choose simulation/snapshot
Zoom = "Zoom-7-5092"
Run = "RhdRun-tracer_radfix"
timestep_final = 155

RunDir = "/cral2/mitchell/"+Zoom+"/"+Run
OutDir = "/cral2/mitchell/PostProcess/"

# Choose redshift interval for stacking
zlo = 3.08
zhi = 4.0

# Number of viewing directions availabel for each mock
n_directions = 12

rad_bins_proj_phys = np.arange(0.0,42.,2.)
n_rad_bins_proj_phys = len(rad_bins_proj_phys[0:-1])
rmid = 0.5*(rad_bins_proj_phys[1:] + rad_bins_proj_phys[0:-1])

SB_stack = np.zeros((len(flux_cuts),n_rad_bins_proj_phys))
norm_stack = np.zeros(len(flux_cuts))

# Read some info from simulation hdf5 file
filename_zoom = Zoom + "_" + Run + "_tsf" + str(timestep_final) +".hdf5"
File = h5py.File(OutDir+filename_zoom,"r")
zoom_group = File[Zoom]

for halo_group_str in zoom_group:
    if "_main" in halo_group_str:
        halo_group_str_use= halo_group_str

mp_group = zoom_group[halo_group_str_use+"/main_progenitors"]
r200 = mp_group["r200"][:]
timesteps = mp_group["halo_ts"][:].astype("int") +1

# Set units/constants
dm.jeje_utils.read_conversion_scales(RunDir,timestep_final)
omega_0,lambda_0,little_h = dm.jeje_utils.read_cosmo_params(RunDir,timestep_final)
cosmo = FlatLambdaCDM(H0=little_h*100,Om0=omega_0)

n_cubes_stack = np.zeros_like(flux_cuts)

for timestep in timesteps:
    
    redshift = dm.jeje_utils.get_snap_redshift(RunDir,timestep)
        
    a =  cosmo.arcsec_per_kpc_proper(redshift)
    unit_l_pkpc = dm.jeje_utils.dp_scale_l * dm.jeje_utils.cm2pc / 1000. # converts comoving code units to phys. kpc
    unit_l_arcsec = unit_l_pkpc * a.value

    lumdist = cosmo.luminosity_distance(redshift)
    lumdist_cm = lumdist.cgs.value

    if redshift >= zlo and redshift <= zhi:
        pass
    else:
        continue

    print "Processing timestep", timestep

    for i in range(n_directions):
        idirection = i+1
        
        # Read data
        timestep_str = str(timestep).rjust(5,'0')
        ex_rc_path = "/cral2/mitchell/Zoom-7-5092/RhdRun-tracer_radfix/RASCAS/"+timestep_str+"/ColLya_CS100_6arcsec/"
        ex_rc_path2 = "/cral2/mitchell/Zoom-7-5092/RhdRun-tracer_radfix/RASCAS/"+timestep_str+"/RecLya_CS100_6arcsec/"

        mock = mocks.mockobs(ex_rc_path, "001", ICFile="001.IC", load_cube=True, idirection=idirection, \
                             unit_l_arcsec=unit_l_arcsec, lumdist_cm=lumdist_cm, redshift=redshift)
        mock2 = mocks.mockobs(ex_rc_path2, "001", ICFile="001.IC", load_cube=True, idirection=idirection, \
                             unit_l_arcsec=unit_l_arcsec, lumdist_cm=lumdist_cm, redshift=redshift)

        # Sum wavelength dimension
    # mock.cube has units of  [erg / s / A / arcsec2 / cm2 ]
        image = np.sum(mock.cube,axis=2) * mock.cube_dlbda_Angstrom # [erg / s / arcsec2 / cm2 ]
        image += np.sum(mock2.cube,axis=2) * mock2.cube_dlbda_Angstrom # [erg / s / arcsec2 / cm2 ]

        # Compute total flux to see if it passes our flux cuts
        flux = np.sum(image) * mock.cube_dx_arcsec**2 # erg /s /cm2

        ok_cut = flux > flux_cuts
        
        x_image = mock.cube_x_arcsec * unit_l_pkpc / unit_l_arcsec
        y_image = mock.cube_y_arcsec * unit_l_pkpc / unit_l_arcsec

        r_image = np.zeros_like(image)
        for n in range(len(x_image)):
            r_image[n] = np.sqrt(np.square(x_image[n]) + np.square(y_image))

        # Create circular surface brightness profile
        image_flat = np.ravel(image)
        r_image_flat = np.ravel(r_image)

        # Only include pixels within the bounding circle of the cube
        rad_max = x_image.max()
        ok = r_image_flat < rad_max

        SB = np.histogram(r_image_flat[ok], bins=rad_bins_proj_phys, weights=image_flat[ok])[0]
        norm = np.histogram(r_image_flat[ok], bins=rad_bins_proj_phys)[0]
        SB *= 1./norm

        SB_stack[ok_cut] += SB
        n_cubes_stack[ok_cut] += 1
        
for n in range(len(flux_cuts)):
    SB_stack[n] *= 1./n_cubes_stack[n]

for n in range(len(flux_cuts)):
    py.plot(rmid, np.log10(SB_stack[n]))
py.show()
