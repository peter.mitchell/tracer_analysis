import numpy as np

escape_only = True # Exclude photons that don't escape
ic_nu = False # Use frequency of photons at emission

Zoom = "Zoom-7-5092"
Run = "RhdRun-tracer_radfix"
timestep_final = 155
RT = True
RunDir = "/cral2/mitchell/"+Zoom+"/"+Run
InDir = "/cral2/mitchell/PostProcess/"
OutDir = "/cral2/mitchell/PostProcess/"

stack = True
if stack:
    zlo = 3
    zhi = 4
    output_choose = None
else:
    output_choose = 140
    zlo = None; zhi = None
    
######## Wavelength bins for spectra - should match rascas setup ################
n_wvl_bins = 200
# Radial bins (normalised) for spectra (3d for now)
delta_rprime = 0.1
rad_bins = np.arange(0.2,1.1,delta_rprime)
n_rad_bins = len(rad_bins[0:-1])

HaloDir = RunDir + "/Halos/"
TreeDir = RunDir + "/Trees/"

import sys
sys.path.append("../.")
import dm_utils as dm
from scipy import spatial
import h5py
import utilities_ramses as ur
import neighbour_utils as nu
import match_searchsorted as ms
import cutils as cut
import flux_utils
import jphot as jp

############## Zoom file IO #########################
# Read in relevant tree information from the hdf5 file
filename_zoom = Zoom + "_" + Run + "_tsf" + str(timestep_final) +".hdf5"
    
File = h5py.File(OutDir+filename_zoom,"r")
    
# Open the subvolume file (containing all cell data)
subvol_filename = "subvol_"+filename_zoom
subvol_File = h5py.File(OutDir+subvol_filename,"r")

# Check the file with the tree information already exists
if not Zoom in File:
    print "Error: Tree information for ", Zoom, " in ", filename_zoom, " cannot be read"
    exit()

zoom_group = File[Zoom]


######### Setup the output arrays ##########
# Get number of timesteps
for halo_group_str in zoom_group:
    if "_main" in halo_group_str:
        halo_group_str_use= halo_group_str
mp_group = zoom_group[halo_group_str_use+"/main_progenitors"]

n_ts = len(mp_group["halo_ts"][:])
timesteps = (mp_group["halo_ts"][:]).astype("int")+1 # tree/sim mismatch

spectrum_rec_tot = np.zeros((n_ts, n_wvl_bins)) # Note the total also includes photons that cannot be linked to tracers, so this is NOT the sum of the other components
spectrum_col_tot = np.zeros((n_ts, n_wvl_bins))
spectrum_rec_tot_corr = np.zeros((n_ts, n_wvl_bins)) # Note the total also includes photons that cannot be linked to tracers, so this is NOT the sum of the other components
spectrum_col_tot_corr = np.zeros((n_ts, n_wvl_bins))
redshifts = np.zeros((n_ts))

# Loop over timesteps
for i_timestep, timestep in enumerate(timesteps):
    timestep_tree = timestep -1 # Stupid mismatch between merger tree files and everything else

    if not stack and timestep != output_choose:
        continue
    
    #if timestep < 140:
    #    if i_timestep == 0:
    #        print "skip"
    #    continue

    # Get conversions for different units
    r2kpc, redshift, little_h, m2Msun,rho2msunpkpc3,v2kms = ur.Set_Units(RunDir, timestep)
    boxlen_pkpc = dm.jeje_utils.get_param_real(RunDir,timestep,'boxlen') * r2kpc # Box size in proper kpc, using the correct box size

    redshifts[i_timestep] = redshift

    if stack:
        if redshift > zhi or redshift < zlo:
            continue
    
    #if i_timestep < 145:
    #    print "skipping"
    #    continue

    print "processing timestep = ", timestep, "of", timestep_final, ", redshift is", redshift

    ######## Read Rascas photon data ###########
    snap_str = str(timestep)
    if len(snap_str) == 1:
        snap_str = "0000"+snap_str
    elif len(snap_str) == 2:
        snap_str = "000"+snap_str
    elif len(snap_str) == 3:
        snap_str = "00"+snap_str
    else:
        print "Error", snap_str
        quit()

    print "Reading rascas data"
    # Photons tracing collisional excitations
    #ex_rc_path_col = RunDir +"/RASCAS/"+snap_str+"/ColLya_CS100_Rvir/"
    ex_rc_path_col = RunDir +"/RASCAS/"+snap_str+"/ColLya_CS100_11arcsec/"
    
    icFile_path_col = ex_rc_path_col + "001.IC" # For now I'm assuming there is only one rascas output per simulation output ("001")
    resFile_path_col = ex_rc_path_col + "001.res"

    # Photons tracing recombinations
    #ex_rc_path_rec = RunDir +"/RASCAS/"+snap_str+"/RecLya_CS100_Rvir/"
    ex_rc_path_rec = RunDir +"/RASCAS/"+snap_str+"/RecLya_CS100_11arcsec/"

    icFile_path_rec = ex_rc_path_rec + "001.IC" # For now I'm assuming there is only one rascas output per simulation output ("001")
    resFile_path_rec = ex_rc_path_rec + "001.res"

    try:
        coll_p = jp.photonlist(icFile_path_col, resFile_path_col)
        rec_p = jp.photonlist(icFile_path_rec, resFile_path_rec)
        coll_p_corr = jp.photonlist(icFile_path_col, resFile_path_col)
        rec_p_corr = jp.photonlist(icFile_path_rec, resFile_path_rec)
    except:
        print "Couldn't read rascas data for this timestep, moving on"
        continue

    if escape_only:
        esc = np.where(coll_p.status == 1)[0]
        n_col_b4 = len(coll_p.x)
        coll_p = coll_p.extract_sample(esc)
        
        esc = np.where(rec_p.status == 1)[0]
        n_rec_b4 = len(rec_p.x)
        rec_p = rec_p.extract_sample(esc)

        esc = np.where(coll_p_corr.status == 1)[0]
        n_col_b4 = len(coll_p_corr.x)
        coll_p_corr = coll_p_corr.extract_sample(esc)
        
        esc = np.where(rec_p_corr.status == 1)[0]
        n_rec_b4 = len(rec_p_corr.x)
        rec_p_corr = rec_p_corr.extract_sample(esc)

    ok_ts = np.array(mp_group["halo_ts"])==timestep_tree # haloes on this timestep

    # Put the spectra into the rest frame of the halo
    vxh = mp_group["vx"][ok_ts] # Halo velocity in kms^-1
    vyh = mp_group["vy"][ok_ts]
    vzh = mp_group["vz"][ok_ts]

    clight    = 2.99792458e10  # [cm/s]
    lambda0   = 1215.6701e-8   # [cm]
    nu0       = clight/lambda0 # [Hz]
        
    rec_p_corr.correct_halo_velocity(vxh,vyh,vzh)
    coll_p_corr.correct_halo_velocity(vxh,vyh,vzh)

    i_ts = np.where(np.array(mp_group["halo_ts"])==timestep_tree)

    wvl_lo = 1210.; wvl_hi = 1220. # Use a fixed binning scheme so that we onlt need to save the wavelength data once

    if ic_nu:
        frame = "ic"
    else:
        frame = "obs"
    wvl, temp = rec_p.spectrum(nbins=n_wvl_bins, frame=frame, lmin=wvl_lo,lmax=wvl_hi)
    spectrum_rec_tot[i_ts] += temp

    wvl, temp = coll_p.spectrum(nbins=n_wvl_bins, frame=frame, lmin=wvl_lo,lmax=wvl_hi)
    spectrum_col_tot[i_ts] += temp

    
    '''print "tehmp hckec"
    #v = clight*1e-5 * ((nu0 / rec_p.nu_ic) -1)
    #v2 = clight*1e-5 * ((nu0 / rec_p_corr.nu_ic) -1)
    #for n in range(10):
    #    print v[n], v2[n]
    #quit()
    
    from utilities_plotting import *
    v = clight*1e-5 * ((nu0 / rec_p.nu_ic) -1)

    a,v_edge = np.histogram(v,bins=200)
    v = 0.5*(v_edge[0:-1]+v_edge[1:])
    py.plot(v,a)

    v2 = clight*1e-5 * ((nu0 / rec_p_corr.nu_ic) -1)
    a,v_edge = np.histogram(v2,bins=200)
    v = 0.5*(v_edge[0:-1]+v_edge[1:])
    py.plot(v,a,linestyle='--')
    py.show()
    quit()'''
    
    wvl, temp = rec_p_corr.spectrum(nbins=n_wvl_bins, frame=frame, lmin=wvl_lo,lmax=wvl_hi)
    spectrum_rec_tot_corr[i_ts] += temp

    wvl, temp = coll_p_corr.spectrum(nbins=n_wvl_bins, frame=frame, lmin=wvl_lo,lmax=wvl_hi)
    spectrum_col_tot_corr[i_ts] += temp

if stack:
    ok = (redshifts > zlo) & (redshifts < zhi)
    spectrum_rec = np.sum(spectrum_rec_tot[ok],axis=0) / len(redshifts[ok])
    spectrum_col = np.sum(spectrum_col_tot[ok],axis=0) / len(redshifts[ok])
    spectrum_rec_corr = np.sum(spectrum_rec_tot_corr[ok],axis=0) / len(redshifts[ok])
    spectrum_col_corr = np.sum(spectrum_col_tot_corr[ok],axis=0) / len(redshifts[ok])
else:
    ok = timesteps == output_choose
    spectrum_rec = spectrum_rec_tot[ok][0]
    spectrum_col = spectrum_col_tot[ok][0]
    spectrum_rec_corr = spectrum_rec_tot_corr[ok][0]
    spectrum_col_corr = spectrum_col_tot_corr[ok][0]
    
spectrum = spectrum_rec + spectrum_col
spectrum_corr = spectrum_rec_corr + spectrum_col_corr

from utilities_plotting import *

py.plot(wvl, spectrum)
py.plot(wvl, spectrum_corr,linestyle='--')
py.axvline(1215.6701)

py.show()
