import h5py
import numpy as np

# Choose whether to bin SB profiles on physical kpc (True), or on angular separation (False)
bin_rad = False

median = True # Choose median/mean stack (True/False respectively)
#cuts_show = [0,2] # Choose which flux cut - see flux_cuts array for values
cuts_show = [0,3] # Choose which flux cut - see flux_cuts array for values
#cuts_show = [0]

filename = "./stacked_mock_SB_profile.hdf5"
output_file = h5py.File(filename,"r")

if bin_rad:
    rmid_pkpc = output_file["rad_mid_pkpc"][:]
    rmid_pkpc_fine = output_file["rad_mid_pkpc_fine"][:]

    rmid = rmid_pkpc; rmid_fine = rmid_pkpc_fine
    
    # Angular bins at median redshift
    rmid_arc = output_file["rad_mid_arcsec_zmed"][:]
    rmid_arc_fine = output_file["rad_mid_arcsec_zmed_fine"][:]
else:
    rmid_arc = output_file["rad_mid_arcsec"][:]
    rmid_arc_fine = output_file["rad_mid_arcsec_fine"][:]

    rmid = rmid_arc; rmid_fine = rmid_arc_fine

    # Angular bins at median redshift
    rmid_pkpc = output_file["rad_mid_pkpc_zmed"][:]
    rmid_pkpc_fine = output_file["rad_mid_pkpc_zmed_fine"][:]

z_lo_med_hi = output_file["z_lo_med_hi"][:]
flux_cuts = output_file["flux_cuts"][:]

SB_mean = output_file["SB_mean"][:]
SB_convolve_mean = output_file["SB_convolve_mean"][:]

SB_med = output_file["SB_med"][:]
SB_convolve_med = output_file["SB_convolve_med"][:]

rmax = output_file["rmax"][()]

output_file.close()

# Arcsecond on axis
# For the simulation cosmo params, Ned CosmoCalc gives the angular conversion as 7.904 kpc/" at z=3
pkpc_ticks = np.array([0, 10, 20, 30, 40])
arc_ticks = pkpc_ticks  * rmid_arc[0] / rmid_pkpc[0]

n_ticks = len(arc_ticks)
tick_labels = []
for m in range(n_ticks):
    tick_labels.append(str(pkpc_ticks[m]))

rad_W18, logSB_W18 = np.loadtxt("lutz_data.dat",unpack=True)

if not bin_rad:
    rad_W18 *= rmid_arc[0] / rmid_pkpc[0] # Convert to arcseconds
    # note Lutz stack was binned in angular separation - so this is actually the correct x-axis for the stack






    
########## Visualize the PSF by convolving a delta function at r=0 ###################

lambda0 = 1215.67 # Rest-frame wvl of Lya in Angstrom (not dark energy paramter! - see just above)
# For MUDF value of beta, see Bacon 17, summary and conclusion section (and search "moffat")
beta = 2.8 # MOFFAT profile shape exponent

# For dependence of FWHM on wavelength, see fig. 14, Bacon 17 (I do a by-eye average of the UDF fields)
x1 = 6000; y1 = 0.675
x2 = 9000; y2 = 0.575
grad = (y2-y1)/(x2-x1)
intercept = y1 - grad*x1

fwhm_f = lambda wvl: wvl * grad + intercept # Arcseconds - typicaly MUSE UDF FWHM, as function of lambda_obs

def moffat(x, fwhm, beta):
    # Note: I will do the normalisation after the fact since we want to combine two mirror-versions

    # From a quick google search
    #(http://web.ipac.caltech.edu/staff/fmasci/home/astro_refs/PSFsAndSampling.pdf)
    alpha = fwhm / np.sqrt(2**(1/beta)-1) / (2)

    y = (1 + np.square(x)/np.square(alpha))**(-beta)
    ymid = 0.5*(y[1:] + y[0:-1])
    return y

redshift = 3.5 # Middle of bin
lambda_obs = lambda0 * (1+redshift) # Observer frame wvl of lya
fwhm = fwhm_f(lambda_obs) # Arcsec

#rmid_fine = np.linspace(rmid[0],rmid[-1],1000)
nrad_fine = len(rmid_fine)
drad_fine = rmid_fine[1]-rmid_fine[0]
r_pos = np.arange(0,nrad_fine)*drad_fine
moff_pos = moffat(r_pos, fwhm,beta)
r_neg = np.arange(-nrad_fine,0)*drad_fine
moff_neg = moffat(r_neg, fwhm,beta)
moff_shift = np.concatenate((moff_pos,moff_neg)) # arcsec
moff_shift *= 1./np.sum(moff_shift)

SB_fine = np.zeros_like(r_pos)
SB_fine[0] = 1.0
SB_fine = np.concatenate((SB_fine[::-1], SB_fine))
SB_convolve = np.fft.irfft( np.fft.rfft(SB_fine) * np.fft.rfft(moff_shift) )[1000:]
PSF = SB_convolve


#################################################################







import sys
sys.path.append("../.")
from utilities_plotting import *
c_list = ["k", "b", "r", "g"]

nrow = 1; ncol=1
py.rcParams.update(latexParams)
py.rcParams.update({'figure.subplot.wspace':0.15,'figure.subplot.hspace':0.,
                    'figure.figsize':[3.32*ncol,2.49*nrow*1.2],
                    'figure.subplot.left':0.2,
                    'figure.subplot.bottom':0.14,
                    'figure.subplot.top':0.88,
                    'figure.subplot.right':0.98,
                    'legend.fontsize':8,
                    'font.size':9,
                    'axes.labelsize':9})

py.figure()
subplots = panelplot(nrow,ncol)
for n,ax in enumerate(subplots):
    py.axes(ax)

    for i_cut in cuts_show:

        if i_cut == 0:
            label = r"No flux cut"
        else:
            label = r"$\log_{10}(F_{\mathrm{Ly\alpha}} \, / \mathrm{erg \, s^{-1} \, cm^{-2}})  > %3.1f$" % np.log10(flux_cuts[i_cut])

        if median:
            py.plot(rmid, np.log10(SB_med[i_cut]), c=c_list[i_cut], linestyle='-', label=label)
            py.plot(rmid_fine, np.log10(SB_convolve_med[i_cut]), c=c_list[i_cut], linestyle='--')
        else:
            py.plot(rmid, np.log10(SB_mean[i_cut]), c=c_list[i_cut],label=label)
            py.plot(rmid_fine, np.log10(SB_convolve_mean[i_cut]), c=c_list[i_cut], linestyle='--')

        if i_cut == 0:
            if median:
                ok = np.isnan(SB_convolve_med[i_cut])==False
                PSF_norm = np.sum(SB_convolve_med[i_cut][ok])/ np.sum(PSF)
                
            else:
                ok = np.isnan(SB_convolve_mean[i_cut])==False
                PSF_norm = np.sum(SB_convolve_mean[i_cut][0][ok]) / np.sum(PSF)

    py.plot(rmid_fine, np.log10(PSF*PSF_norm), c="r",linestyle='--')
    py.plot(rmid_fine,rmid_fine-99,c="r",label="Point source")
            
    py.scatter(rad_W18, logSB_W18, marker = "x",label="MUSE, Wisotzki et al. (2018)")

    hl = 1
    legend1 = py.legend(frameon=False,loc="upper right",numpoints=1,scatterpoints=1,handlelength=hl)
    py.gca().add_artist(legend1)
    
    py.xlim((0.0, rmax))
    ylo = -20.75; yhi = -17.0
    py.ylim((ylo,yhi))

    py.xlabel(r'$r_{\mathrm{projected}} \, / \mathrm{arcsec}$')
    py.ylabel(r"$\log_{10}(\mathrm{d}F / \mathrm{d}A \, / \mathrm{erg \, s^{-1} \, cm^{-2} arcsec^{-2}})$")

    py.annotate(r"$3 < z < 4$",(4.25,-18.55),fontsize=11)

    l1, = py.plot([-20,-20],[-20,-20], c="k",linestyle='-')
    l2, = py.plot([-20,-20],[-20,-20], c="k",linestyle='--')
    lines = [l1,l2]
    labels = ["Not convolved", "PSF-convolved"]
    
    py.legend(handles=lines,labels=labels, loc="lower left",frameon=False,numpoints=1,handlelength=3.)
    
    ax_z = ax.twiny()
    ax_z.set_xticks(arc_ticks)
    ax_z.set_xticklabels(tick_labels)
    ax_z.set_xlabel(r'$r_{\mathrm{projected}}(z=%3.1f) \, / \mathrm{pkpc}$' % z_lo_med_hi[1])
    ax_z.set_xlim((0,rmax))

    
fig_name = "Lya_projected_SB_stacked_mocks.pdf"
py.savefig("../Figures/"+fig_name)
py.show()
