print "imports..."
import numpy as np
import jphot as jp
import mocks
import sys
import os
sys.path.append("../.")
import dm_utils as dm
from astropy.cosmology import FlatLambdaCDM
import h5py
import utilities_statistics as us
print "done"

rad_cut = 1.0 # arcsec

n_spectra = 9
direc_list = [10,  2,  8,  8,  7,  9,  3, 10,  5]
#snap_list = [ 97, 119, 110, 123, 101,  95, 114,  99, 121]
snap_list = [123, 124, 140, 130, 119, 141, 113, 99, 126, 138, 131, 143]
# Note 95 is z=4, and 148 is max right now

# Pulled from 001.mockparams
direc_xyz = np.array([[5.2704627669e-01, 5.2704627669e-01, 6.6666666667e-01],
                      [ -5.2704627669e-01, 5.2704627669e-01, 6.6666666667e-01],
                      [ -5.2704627669e-01, -5.2704627669e-01, 6.6666666667e-01],
                      [ 5.2704627669e-01, -5.2704627669e-01, 6.6666666667e-01],
                      [ 1.0000000000e+00, 0.0000000000e+00, 0.0000000000e+00],
                      [ 6.1232339957e-17, 1.0000000000e+00, 0.0000000000e+00],
                      [ -1.0000000000e+00, 1.2246467991e-16, 0.0000000000e+00],
                      [ -1.8369701987e-16, -1.0000000000e+00, 0.0000000000e+00],
                      [ 5.2704627669e-01, 5.2704627669e-01, -6.6666666667e-01],
                      [ -5.2704627669e-01, 5.2704627669e-01, -6.6666666667e-01],
                      [ -5.2704627669e-01, -5.2704627669e-01, -6.6666666667e-01],
                      [ 5.2704627669e-01, -5.2704627669e-01, -6.6666666667e-01]])


# Choose simulation/snapshot
Zoom = "Zoom-7-5092"
Run = "RhdRun-tracer_radfix"
timestep_final = 155

RunDir = "/cral2/mitchell/"+Zoom+"/"+Run
OutDir = "/cral2/mitchell/PostProcess/"

# Read some info from simulation hdf5 file
filename_zoom = Zoom + "_" + Run + "_tsf" + str(timestep_final) +".hdf5"
File = h5py.File(OutDir+filename_zoom,"r")
zoom_group = File[Zoom]

for halo_group_str in zoom_group:
    if "_main" in halo_group_str:
        halo_group_str_use= halo_group_str

mp_group = zoom_group[halo_group_str_use+"/main_progenitors"]
r200 = mp_group["r200"][:]
vxh = mp_group["vx"][:]
vyh = mp_group["vx"][:]
vzh = mp_group["vx"][:]
timesteps = mp_group["halo_ts"][:].astype("int") +1

# Set units/constants
dm.jeje_utils.read_conversion_scales(RunDir,timestep_final)
omega_0,lambda_0,little_h = dm.jeje_utils.read_cosmo_params(RunDir,timestep_final)
cosmo = FlatLambdaCDM(H0=little_h*100,Om0=omega_0)

lambda0 = 1215.67 # Rest-frame wvl of Lya in Angstrom (not dark energy paramter! - see just above)
# For MUDF value of beta, see Bacon 17, summary and conclusion section (and search "moffat")
beta = 2.8 # MOFFAT profile shape exponent

# For dependence of FWHM on wavelength, see fig. 14, Bacon 17 (I do a by-eye average of the UDF fields)
x1 = 6000; y1 = 0.675
x2 = 9000; y2 = 0.575
grad = (y2-y1)/(x2-x1)
intercept = y1 - grad*x1

fwhm_f = lambda wvl: wvl * grad + intercept # Arcseconds - typicaly MUSE UDF FWHM, as function of lambda_obs

def moffat(x, fwhm, beta):
    # Note: I will do the normalisation after the fact since we want to combine two mirror-versions

    # From a quick google search
    #(http://web.ipac.caltech.edu/staff/fmasci/home/astro_refs/PSFsAndSampling.pdf)
    alpha = fwhm / np.sqrt(2**(1/beta)-1) / (2)

    y = (1 + np.square(x)/np.square(alpha))**(-beta)
    ymid = 0.5*(y[1:] + y[0:-1])    
    return y


# Compute closest snpashot to middle redshift
redshifts = []
for timestep in timesteps:   
    redshifts.append(dm.jeje_utils.get_snap_redshift(RunDir,timestep))


spectrum_list = []
spectrum_tot_list = []
wvl_list = []
z_list = []
for i in range(n_spectra):
    timestep = snap_list[i]
    direc = direc_list[i]
    
    redshift = dm.jeje_utils.get_snap_redshift(RunDir,timestep)
    z_list.append(redshift)


    a =  cosmo.arcsec_per_kpc_proper(redshift)
    unit_l_pkpc = dm.jeje_utils.dp_scale_l * dm.jeje_utils.cm2pc / 1000. # converts comoving code units to phys. kpc
    unit_l_arcsec = unit_l_pkpc * a.value
    
    lumdist = cosmo.luminosity_distance(redshift)
    lumdist_cm = lumdist.cgs.value
    
    print "Processing spectrum", i+1, "of", n_spectra

    # Read data
    timestep_str = str(timestep).rjust(5,'0')
    ex_rc_path = "/cral2/mitchell/Zoom-7-5092/RhdRun-tracer_radfix/RASCAS/"+timestep_str+"/ColLya_CS100_11arcsec/"
    ex_rc_path2 = "/cral2/mitchell/Zoom-7-5092/RhdRun-tracer_radfix/RASCAS/"+timestep_str+"/RecLya_CS100_11arcsec/"

    mock = mocks.mockobs(ex_rc_path, "001", ICFile="001.IC", load_cube=True, idirection=direc, \
                         unit_l_arcsec=unit_l_arcsec, lumdist_cm=lumdist_cm, redshift=redshift)
    mock2 = mocks.mockobs(ex_rc_path2, "001", ICFile="001.IC", load_cube=True, idirection=direc, \
                          unit_l_arcsec=unit_l_arcsec, lumdist_cm=lumdist_cm, redshift=redshift)

    # Subtract the redshift associated with the halo peculiar velocity

    xyz_mock = direc_xyz[i] # Unit vector of the direction of the mock
    i_ts = timesteps == timestep
    vxh_i = vxh[i_ts] # kms
    vyh_i = vyh[i_ts]
    vzh_i = vzh[i_ts]
    vhalo_proj = np.sum(xyz_mock * np.array([vxh_i,vyh_i,vzh_i]))
    # Positive value implies halo is moving towards observer - meaning the spectrum has been blueshifted
    # This means we should redshift it back
    clight = 2.99792458e10  # [cm/s]
    zshift = vhalo_proj / (clight / 1e5)
    zshift *= 0 # Scratch that - by eye it looks like the halo peculiar velocity was already subtracted - waiting for J confirmation
    
    wvl_list.append(mock.cube_lbda_Angstrom * (1+zshift) / (1+redshift))
    
    # Sum wavelength dimension
    # mock.cube has units of  [erg / s / A / arcsec2 / cm2 ]
    #image = np.sum(mock.cube,axis=2) * mock.cube_dlbda_Angstrom # [erg / s / arcsec2 / cm2 ]
    #image += np.sum(mock2.cube,axis=2) * mock2.cube_dlbda_Angstrom # [erg / s / arcsec2 / cm2 ]

    x_image = mock.cube_x_arcsec # arcsec
    y_image = mock.cube_y_arcsec

    # Compute spectrum within rad_cut
    xhi = x_image.max()

    spectrum = np.zeros_like(mock.cube[0,0])
    spectrum_tot = np.zeros_like(mock.cube[0,0])
    r_image = np.zeros_like(mock.cube[:,:,0])
    for n in range(len(x_image)):
        r_image[n] = np.sqrt(np.square(x_image[n]) + np.square(y_image))

        ok = r_image[n] < rad_cut
        spectrum += np.sum(mock.cube[n][ok],axis=0)
        spectrum += np.sum(mock2.cube[n][ok],axis=0)

        spectrum_tot += np.sum(mock.cube[n],axis=0)
        spectrum_tot += np.sum(mock2.cube[n],axis=0)

    spectrum *= mock.cube_dx_arcsec**2 # erg /A /s /cm2 
    spectrum_tot *= mock.cube_dx_arcsec**2 # erg /A /s /cm2 
    spectrum_list.append(spectrum)
    spectrum_tot_list.append(spectrum_tot)

    '''    # Convolve with the MUSE PSF at this redshift
        lambda_obs = lambda0 * (1+redshift) # Observer frame wvl of lya
        fwhm = fwhm_f(lambda_obs) # Arcsec
        if bin_rad:
            fwhm *= unit_l_pkpc / unit_l_arcsec # Do the convolution with the image in units of pkpc
        
        r_pos = np.arange(0,nrad_fine)*drad_fine
        moff_pos = moffat(r_pos, fwhm,beta)
        r_neg = np.arange(-nrad_fine,0)*drad_fine
        moff_neg = moffat(r_neg, fwhm,beta)
        
        moff_shift = np.concatenate((moff_pos,moff_neg)) # arcsec
        moff_shift *= 1./np.sum(moff_shift)
        
        SB_fine = us.Linear_Interpolation(rmid, SB, rmid_fine)
        SB_fine[np.isnan(SB_fine)] = 0.0
        SB_fine = np.concatenate((SB_fine[::-1], SB_fine)) # Need to make profile symmetric for FFT convolution
        SB_convolve = np.fft.irfft( np.fft.rfft(SB_fine) * np.fft.rfft(moff_shift) )[1000:]
                
        SB_stack[ok_cut] += SB
        SB_stack_convolve[ok_cut] += SB_convolve
        n_cubes_stack[ok_cut] += 1

        for i_cut in range(len(flux_cuts)):
            if ok_cut[i_cut]:
                SB_tot[i_cut].append(SB)
                SB_tot_convolve[i_cut].append(SB_convolve)'''

from utilities_plotting import *
py.rcParams.update(latexParams)
py.rcParams.update({'figure.subplot.wspace':0,'figure.subplot.hspace':0,
                    'figure.figsize':[6.64,4.98],
                    'figure.subplot.left':0.09,
                    'figure.subplot.bottom':0.11,
                    'figure.subplot.top':0.98,
                    'legend.fontsize':7,
                    'font.size':10,
                    'axes.labelsize':10})
    

py.figure()
nrow = 3; ncol = 3
subplots = panelplot(nrow,ncol)
for n,ax in enumerate(subplots):
    py.axes(ax)

    lambda0   = 1215.6701   # [cm]
    py.axvline(lambda0,c="k",linestyle='-',alpha=0.5)
    
    py.plot(wvl_list[n], spectrum_list[n]/1e-19,c="k",label=r"$r_{\mathrm{projected}}< 1''$")
    py.plot(wvl_list[n], spectrum_tot_list[n]/1e-19,c="k",alpha=0.3,label="Total")
    py.legend(frameon=False, loc="upper left")
    
    if n >= ncol*nrow- ncol:
        py.xlabel(r"$\lambda \, / \mathrm{\AA}$")
    if n ==3:
        py.ylabel(r"$F_{\mathrm{\lambda}} \, / \mathrm{10^{-19} \, erg s^{-1} cm^{-2} \AA^{-1}}$")

    py.xlim((1211.,1219.))
    label = r"$z=%3.2f$" % z_list[n]
    py.annotate(label,(1211.5,1))

fig_name = "random_Lya_spectra.pdf"
py.savefig("../Figures/"+fig_name)
py.show()
