import h5py
import numpy as np

# Choose whether to bin SB profiles on physical kpc (True), or on angular separation (False)
bin_rad = False

#cuts_show = [0,2] # Choose which flux cut - see flux_cuts array for values
#cuts_show = [0,3] # Choose which flux cut - see flux_cuts array for values
cuts_show = [0]

filename = "./stacked_mock_SB_profile.hdf5"
output_file = h5py.File(filename,"r")

if bin_rad:
    rmid_pkpc = output_file["rad_mid_pkpc"][:]
    rmid_pkpc_fine = output_file["rad_mid_pkpc_fine"][:]

    rmid = rmid_pkpc; rmid_fine = rmid_pkpc_fine
    
    # Angular bins at median redshift
    rmid_arc = output_file["rad_mid_arcsec_zmed"][:]
    rmid_arc_fine = output_file["rad_mid_arcsec_zmed_fine"][:]
else:
    rmid_arc = output_file["rad_mid_arcsec"][:]
    rmid_arc_fine = output_file["rad_mid_arcsec_fine"][:]

    rmid = rmid_arc; rmid_fine = rmid_arc_fine

    # Angular bins at median redshift
    rmid_pkpc = output_file["rad_mid_pkpc_zmed"][:]
    rmid_pkpc_fine = output_file["rad_mid_pkpc_zmed_fine"][:]

z_lo_med_hi = output_file["z_lo_med_hi"][:]
flux_cuts = output_file["flux_cuts"][:]

SB_mean = output_file["SB_mean"][:]
SB_convolve_mean = output_file["SB_convolve_mean"][:]

SB_med = output_file["SB_med"][:]
SB_convolve_med = output_file["SB_convolve_med"][:]

rmax = output_file["rmax"][()]

output_file.close()

# Arcsecond on axis
# For the simulation cosmo params, Ned CosmoCalc gives the angular conversion as 7.904 kpc/" at z=3
pkpc_ticks = np.array([0, 10, 20, 30, 40])
arc_ticks = pkpc_ticks  * rmid_arc[0] / rmid_pkpc[0]

n_ticks = len(arc_ticks)
tick_labels = []
for m in range(n_ticks):
    tick_labels.append(str(pkpc_ticks[m]))

rad_W18, logSB_W18 = np.loadtxt("lutz_data.dat",unpack=True)

if not bin_rad:
    rad_W18 *= rmid_arc[0] / rmid_pkpc[0] # Convert to arcseconds
    # note Lutz stack was binned in angular separation - so this is actually the correct x-axis for the stack

import sys
sys.path.append("../.")
from utilities_plotting import *
c_list = ["k", "b", "r", "g"]

nrow = 1; ncol=1
py.rcParams.update(latexParams)
py.rcParams.update({'figure.subplot.wspace':0.15,'figure.subplot.hspace':0.,
                    'figure.figsize':[3.32*ncol,2.49*nrow*1.2],
                    'figure.subplot.left':0.2,
                    'figure.subplot.bottom':0.14,
                    'figure.subplot.top':0.88,
                    'figure.subplot.right':0.98,
                    'legend.fontsize':8,
                    'font.size':9,
                    'axes.labelsize':9})

py.figure()
subplots = panelplot(nrow,ncol)
for n,ax in enumerate(subplots):
    py.axes(ax)

    for i_cut in cuts_show:

        label1 = "Median (no flux cut)"
        label2 = "Mean (no flux cut)"

        py.plot(rmid, np.log10(SB_med[i_cut]), c="k", linestyle='-', label=label1)
        py.plot(rmid_fine, np.log10(SB_convolve_med[i_cut]), c="k", linestyle='--')
        py.plot(rmid, np.log10(SB_mean[i_cut]), c="g",label=label2)
        py.plot(rmid_fine, np.log10(SB_convolve_mean[i_cut]), c="g", linestyle='--')

    py.scatter(rad_W18, logSB_W18, marker = "x",label="MUSE, Wisotzki et al. (2018)")

    hl = 1
    legend1 = py.legend(frameon=False,loc="upper right",numpoints=1,scatterpoints=1,handlelength=hl)
    py.gca().add_artist(legend1)
    
    py.xlim((0.0, rmax))
    ylo = -20.75; yhi = -17.0
    py.ylim((ylo,yhi))

    py.xlabel(r'$r_{\mathrm{projected}} \, / \mathrm{arcsec}$')
    py.ylabel(r"$\log_{10}(\mathrm{d}F / \mathrm{d}A \, / \mathrm{erg \, s^{-1} \, cm^{-2} arcsec^{-2}})$")

    py.annotate(r"$3 < z < 4$",(4.25,-18.35),fontsize=11)

    l1, = py.plot([-20,-20],[-20,-20], c="k",linestyle='-')
    l2, = py.plot([-20,-20],[-20,-20], c="k",linestyle='--')
    lines = [l1,l2]
    labels = ["Not convolved", "PSF-convolved"]
    
    py.legend(handles=lines,labels=labels, loc="lower left",frameon=False,numpoints=1,handlelength=3.)
    
    ax_z = ax.twiny()
    ax_z.set_xticks(arc_ticks)
    ax_z.set_xticklabels(tick_labels)
    ax_z.set_xlabel(r'$r_{\mathrm{projected}}(z=%3.1f) \, / \mathrm{pkpc}$' % z_lo_med_hi[1])
    ax_z.set_xlim((0,rmax))

    
fig_name = "Lya_projected_mocks_mean_median_comparison.pdf"
py.savefig("../Figures/"+fig_name)
py.show()
