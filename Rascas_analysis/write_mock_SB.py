print "imports..."
import numpy as np
import jphot as jp
import mocks
import sys
import os
sys.path.append("../.")
import dm_utils as dm
from astropy.cosmology import FlatLambdaCDM
import h5py
import utilities_statistics as us
print "done"

# Choose whether to bin SB profiles on physical kpc (True), or on angular separation (False)
bin_rad = False

# Stack LAEs along given directions if they have flux above some cut
# See Fig.1 from Leclercq 17 to see a flux distribution of UDF LAEs
flux_cuts = np.array([0.0, 1e-18, 10.0**-17.8, 10**-17.6, 10.0**-17.5]) # erg s-1 cm^-2

# Choose simulation/snapshot
Zoom = "Zoom-7-5092"
Run = "RhdRun-tracer_radfix"
timestep_final = 155

RunDir = "/cral2/mitchell/"+Zoom+"/"+Run
OutDir = "/cral2/mitchell/PostProcess/"

# Choose redshift interval for stacking
zlo = 3.08
#zhi = 3.12
zhi = 4.0

z_med = 3.5 # Only used to decide which physical radius/angular separation to store when we stack on the other variable

# Number of viewing directions availabel for each mock
n_directions = 12

if bin_rad:
    rad_bins_proj_phys = np.arange(0.0,42.,1.)
    n_rad_bins_proj_phys = len(rad_bins_proj_phys[0:-1])
    rmid = 0.5*(rad_bins_proj_phys[1:] + rad_bins_proj_phys[0:-1])
else:
    #rad_bins_angle = np.linspace(0.0, 6.0, 40.0)
    rad_bins_angle = np.linspace(0.0, 10.0, 60.0)
    n_rad_bins_angle = len(rad_bins_angle[0:-1])
    rmid = 0.5*(rad_bins_angle[1:] + rad_bins_angle[0:-1])
    
# Use a finer grid for convolution - via interpolation
rmid_fine = np.linspace(rmid[0],rmid[-1],1000)
nrad_fine = len(rmid_fine)
drad_fine = rmid_fine[1]-rmid_fine[0]

if bin_rad:
    SB_stack = np.zeros((len(flux_cuts),n_rad_bins_proj_phys))
else:
    SB_stack = np.zeros((len(flux_cuts),n_rad_bins_angle))
    
SB_stack_convolve = np.zeros((len(flux_cuts),nrad_fine))
norm_stack = np.zeros(len(flux_cuts))

SB_tot = [] # For median stacking after the main loop
SB_tot_convolve = []
for flux_cut in flux_cuts:
    SB_tot.append([])
    SB_tot_convolve.append([])

# Read some info from simulation hdf5 file
filename_zoom = Zoom + "_" + Run + "_tsf" + str(timestep_final) +".hdf5"
File = h5py.File(OutDir+filename_zoom,"r")
zoom_group = File[Zoom]

for halo_group_str in zoom_group:
    if "_main" in halo_group_str:
        halo_group_str_use= halo_group_str

mp_group = zoom_group[halo_group_str_use+"/main_progenitors"]
r200 = mp_group["r200"][:]
timesteps = mp_group["halo_ts"][:].astype("int") +1

# Set units/constants
dm.jeje_utils.read_conversion_scales(RunDir,timestep_final)
omega_0,lambda_0,little_h = dm.jeje_utils.read_cosmo_params(RunDir,timestep_final)
cosmo = FlatLambdaCDM(H0=little_h*100,Om0=omega_0)

lambda0 = 1215.67 # Rest-frame wvl of Lya in Angstrom (not dark energy paramter! - see just above)
# For MUDF value of beta, see Bacon 17, summary and conclusion section (and search "moffat")
beta = 2.8 # MOFFAT profile shape exponent

# For dependence of FWHM on wavelength, see fig. 14, Bacon 17 (I do a by-eye average of the UDF fields)
x1 = 6000; y1 = 0.675
x2 = 9000; y2 = 0.575
grad = (y2-y1)/(x2-x1)
intercept = y1 - grad*x1

fwhm_f = lambda wvl: wvl * grad + intercept # Arcseconds - typicaly MUSE UDF FWHM, as function of lambda_obs

def moffat(x, fwhm, beta):
    # Note: I will do the normalisation after the fact since we want to combine two mirror-versions

    # From a quick google search
    #(http://web.ipac.caltech.edu/staff/fmasci/home/astro_refs/PSFsAndSampling.pdf)
    alpha = fwhm / np.sqrt(2**(1/beta)-1) / (2)

    y = (1 + np.square(x)/np.square(alpha))**(-beta)
    ymid = 0.5*(y[1:] + y[0:-1])    
    return y


n_cubes_stack = np.zeros_like(flux_cuts)

# Compute closest snpashot to middle redshift
redshifts = []
for timestep in timesteps:   
    redshifts.append(dm.jeje_utils.get_snap_redshift(RunDir,timestep))
i_ts_med = np.argmin(abs(np.array(redshifts)-z_med))

for i_ts, timestep in enumerate(timesteps):
    
    redshift = dm.jeje_utils.get_snap_redshift(RunDir,timestep)
        
    a =  cosmo.arcsec_per_kpc_proper(redshift)
    unit_l_pkpc = dm.jeje_utils.dp_scale_l * dm.jeje_utils.cm2pc / 1000. # converts comoving code units to phys. kpc
    unit_l_arcsec = unit_l_pkpc * a.value

    if i_ts == i_ts_med:
        unit_l_pkpc_zmed = unit_l_pkpc
        unit_l_arcsec_zmed = unit_l_arcsec
    
    lumdist = cosmo.luminosity_distance(redshift)
    lumdist_cm = lumdist.cgs.value
    
    if redshift >= zlo and redshift <= zhi:
        pass
    else:
        continue

    print "Processing timestep", timestep

    for i in range(n_directions):
        idirection = i+1
        
        # Read data
        timestep_str = str(timestep).rjust(5,'0')
        #ex_rc_path = "/cral2/mitchell/Zoom-7-5092/RhdRun-tracer_radfix/RASCAS/"+timestep_str+"/ColLya_CS100_6arcsec/"
        #ex_rc_path2 = "/cral2/mitchell/Zoom-7-5092/RhdRun-tracer_radfix/RASCAS/"+timestep_str+"/RecLya_CS100_6arcsec/"
        ex_rc_path = "/cral2/mitchell/Zoom-7-5092/RhdRun-tracer_radfix/RASCAS/"+timestep_str+"/ColLya_CS100_11arcsec/"
        ex_rc_path2 = "/cral2/mitchell/Zoom-7-5092/RhdRun-tracer_radfix/RASCAS/"+timestep_str+"/RecLya_CS100_11arcsec/"

        #for n in range(12):
        #    mock = mocks.mockobs(ex_rc_path, "001", ICFile="001.IC", load_cube=True, idirection=n+1, \
        #                                                      unit_l_arcsec=unit_l_arcsec, lumdist_cm=lumdist_cm, redshift=redshift)
        #quit()
        
        mock = mocks.mockobs(ex_rc_path, "001", ICFile="001.IC", load_cube=True, idirection=idirection, \
                             unit_l_arcsec=unit_l_arcsec, lumdist_cm=lumdist_cm, redshift=redshift)
        mock2 = mocks.mockobs(ex_rc_path2, "001", ICFile="001.IC", load_cube=True, idirection=idirection, \
                             unit_l_arcsec=unit_l_arcsec, lumdist_cm=lumdist_cm, redshift=redshift)

        # Sum wavelength dimension
        # mock.cube has units of  [erg / s / A / arcsec2 / cm2 ]
        image = np.sum(mock.cube,axis=2) * mock.cube_dlbda_Angstrom # [erg / s / arcsec2 / cm2 ]
        image += np.sum(mock2.cube,axis=2) * mock2.cube_dlbda_Angstrom # [erg / s / arcsec2 / cm2 ]

        # Compute total flux to see if it passes our flux cuts
        flux = np.sum(image) * mock.cube_dx_arcsec**2 # erg /s /cm2
        
        ok_cut = flux > flux_cuts

        if bin_rad:
            x_image = mock.cube_x_arcsec * unit_l_pkpc / unit_l_arcsec # pkpc
            y_image = mock.cube_y_arcsec * unit_l_pkpc / unit_l_arcsec
        else:
            x_image = mock.cube_x_arcsec # arcsec
            y_image = mock.cube_y_arcsec
            
        xhi = x_image.max()

        r_image = np.zeros_like(image)
        for n in range(len(x_image)):
            r_image[n] = np.sqrt(np.square(x_image[n]) + np.square(y_image))

        # Create circular surface brightness profile
        image_flat = np.ravel(image)
        r_image_flat = np.ravel(r_image)

        # Only include pixels within the bounding circle of the cube
        rad_max = x_image.max()
        ok = r_image_flat < rad_max

        #print rad_max
        #quit()
        
        if bin_rad:
            SB = np.histogram(r_image_flat[ok], bins=rad_bins_proj_phys, weights=image_flat[ok])[0]
            norm = np.histogram(r_image_flat[ok], bins=rad_bins_proj_phys)[0]
        else:
            SB = np.histogram(r_image_flat[ok], bins=rad_bins_angle, weights=image_flat[ok])[0]
            norm = np.histogram(r_image_flat[ok], bins=rad_bins_angle)[0]
        SB *= 1./norm

        #jimbo = np.isnan(SB)==False
        #print "redshift, SB[0]/flux, log10(SB[0]), flux", redshift, SB[0] / flux, np.log10(SB[0]), flux, np.log10(np.sum(SB[jimbo]))

        #print mock.cube_dx_arcsec, len(x_image), mock.cube_dx_arcsec * len(x_image)
        #print x_image
        #quit()
        
        # Convolve with the MUSE PSF at this redshift
        lambda_obs = lambda0 * (1+redshift) # Observer frame wvl of lya
        fwhm = fwhm_f(lambda_obs) # Arcsec
        if bin_rad:
            fwhm *= unit_l_pkpc / unit_l_arcsec # Do the convolution with the image in units of pkpc
        
        r_pos = np.arange(0,nrad_fine)*drad_fine
        moff_pos = moffat(r_pos, fwhm,beta)
        r_neg = np.arange(-nrad_fine,0)*drad_fine
        moff_neg = moffat(r_neg, fwhm,beta)
        
        moff_shift = np.concatenate((moff_pos,moff_neg)) # arcsec
        moff_shift *= 1./np.sum(moff_shift)
        
        SB_fine = us.Linear_Interpolation(rmid, SB, rmid_fine)
        SB_fine[np.isnan(SB_fine)] = 0.0
        SB_fine = np.concatenate((SB_fine[::-1], SB_fine)) # Need to make profile symmetric for FFT convolution
        SB_convolve = np.fft.irfft( np.fft.rfft(SB_fine) * np.fft.rfft(moff_shift) )[1000:]
                
        SB_stack[ok_cut] += SB
        SB_stack_convolve[ok_cut] += SB_convolve
        n_cubes_stack[ok_cut] += 1

        for i_cut in range(len(flux_cuts)):
            if ok_cut[i_cut]:
                SB_tot[i_cut].append(SB)
                SB_tot_convolve[i_cut].append(SB_convolve)

# Finalise mean stacks
for n in range(len(flux_cuts)):
    SB_stack[n] *= 1./n_cubes_stack[n]
    SB_stack_convolve[n] *= 1./n_cubes_stack[n]

# Compute median stacks
SB_med = np.zeros_like(SB_stack)
SB_med_convolve = np.zeros_like(SB_stack_convolve)
for i_cut in range(len(flux_cuts)):
    SB = np.array(SB_tot[i_cut])
    SB = np.median(SB, axis=0)
    SB_med[i_cut] = SB

    SB = np.array(SB_tot_convolve[i_cut])
    SB = np.median(SB, axis=0)
    SB_med_convolve[i_cut] = SB
    
# Save SB profiles to disk
filename = "./stacked_mock_SB_profile.hdf5"
if os.path.isfile(filename):
    os.remove(filename)
    print "deleted previous hdf5 file at ", filename
    
output_file = h5py.File(filename)
if bin_rad:
    output_file.create_dataset("rad_mid_pkpc",data=rmid)
    output_file.create_dataset("rad_mid_pkpc_fine",data=rmid_fine)
    output_file.create_dataset("rad_mid_arcsec_zmed",data=rmid*unit_l_arcsec_zmed/unit_l_pkpc_zmed)
    output_file.create_dataset("rad_mid_arcsec_zmed_fine",data=rmid_fine*unit_l_arcsec_zmed/unit_l_pkpc_zmed)
else:
    output_file.create_dataset("rad_mid_arcsec",data=rmid)
    output_file.create_dataset("rad_mid_arcsec_fine",data=rmid_fine)
    output_file.create_dataset("rad_mid_pkpc_zmed",data=rmid*unit_l_pkpc_zmed/unit_l_arcsec_zmed)
    output_file.create_dataset("rad_mid_pkpc_zmed_fine",data=rmid_fine*unit_l_pkpc_zmed/unit_l_arcsec_zmed)

output_file.create_dataset("rmax", data=xhi)
output_file.create_dataset("z_lo_med_hi",data=np.array([zlo,z_med,zhi]))
output_file.create_dataset("flux_cuts", data=flux_cuts)
output_file.create_dataset("SB_mean", data=SB_stack)
output_file.create_dataset("SB_convolve_mean", data=SB_stack_convolve)
output_file.create_dataset("SB_med", data=SB_med)
output_file.create_dataset("SB_convolve_med", data=SB_med_convolve)
output_file.close()
    
from utilities_plotting import *
c_list = ["k", "b", "g", "r", "c"]
for n in range(len(flux_cuts)):
    py.plot(rmid, np.log10(SB_stack[n]), c=c_list[n])
    py.plot(rmid_fine, np.log10(SB_stack_convolve[n]), c=c_list[n], linestyle='--')
    
    py.plot(rmid, np.log10(SB_med[n]), c=c_list[n], linestyle=':')
    py.plot(rmid_fine, np.log10(SB_med_convolve[n]), c=c_list[n], linestyle='-.')

py.xlim((0.0, xhi))
ylo = -21; yhi = -17
py.ylim((ylo,yhi))
py.show()
