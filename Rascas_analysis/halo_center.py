import numpy as np
import h5py
import sys
sys.path.append("../.")
import dm_utils as dm
import utilities_ramses as ur

Zoom = "Zoom-7-5092"
Run = "RhdRun-tracer_radfix"
RunDir = "/cral2/mitchell/"
OutDir = "/cral2/mitchell/PostProcess/"
timestep_final = 155

RunDir += Zoom+"/"+Run

# Read the hdf5 file
filename = Zoom +"_" +Run+"_tsf" + str(timestep_final)+ ".hdf5"
File = h5py.File(OutDir+filename)
zoom_group = File[Zoom]

for halo_group_str in zoom_group:
    # Only read the main halo
    if not "main" in halo_group_str:
        continue

    halo_group = zoom_group[halo_group_str]

r2kpc, redshift, little_h, m2Msun,rho2msunpkpc3,v2kms = ur.Set_Units(RunDir, timestep_final)
r2kpc_ideal, redshift, little_h, m2Msun,junk,junk = ur.Set_Units(RunDir, timestep_final, "ideal")
boxlen_cMpc = dm.jeje_utils.get_param_real(RunDir,timestep_final,'boxlen') # Box size in box units
boxlen_cMpc *= r2kpc_ideal
boxlen_cMpc = boxlen_cMpc * (1+redshift) /1e3 # cMpc (comoving)
boxlen_cMpc = boxlen_cMpc * little_h # h^-1 cMpc
factor = 1./boxlen_cMpc
    
# Read halo mass
mp_group = halo_group["main_progenitors"]
x = mp_group["x"][:] * factor + 0.5
y = mp_group["y"][:] * factor + 0.5
z = mp_group["z"][:] * factor + 0.5

for n in range(len(x)):
    print n+2, x[n], y[n], z[n]
