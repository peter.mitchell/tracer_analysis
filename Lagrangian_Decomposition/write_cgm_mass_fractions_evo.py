# Note "_updated" means adapted to use the tracer particles when split into different hdf5 files trick

import numpy as np
import h5py
import utilities_cosmology as uc
import dm_utils as dm
import utilities_ramses as ur
import ldecomp_functions as ldf


'''Zoom = "Zoom-7-1290"
Run = "tracer_run_lr"
timestep_final = 155
InDir = "/cral2/mitchell/PostProcess/"
RunDir = "/cral4/mitchell/data/Tracer_Test/"+ Zoom + "/" + Run
OutDir = "/cral4/mitchell/data/Tracer_Test/PostProcess/"
RT = False
downsample = False
n_downsample = "1000"'''

#Zoom = "Zoom-7-5092"
#Run = "RhdRun-tracer_norad_bug"
#File = h5py.File(OutDir+"save_5092_backup.hdf5")

'''Zoom = "Zoom-7-5092"
Run = "RhdRun-tracer_radfix"
OutDir = "/cral2/mitchell/PostProcess/"
InDir = OutDir
timestep_final = 107
RunDir = "/cral2/mitchell/"+ Zoom + "/" + Run
RT = True
downsample = True
n_downsample = "1000"'''

Zoom = "Zoom-7-5092"
Run = "RhdRun-tracer_radfix"
#timestep_final = 107
#timestep_final = 130
timestep_final = 155
RunDir = "/cral2/mitchell/"+ Zoom + "/" + Run
OutDir = "/cral2/mitchell/PostProcess/"
InDir = OutDir
RT = True
rascas = True
downsample = True
n_downsample = "3000000.0"
#n_downsample = "1000"

do_lya_phase_split = False # A hack implemented late in the paper development to look at intrinsic lya luminosity split by gas phase
    
# Choose radial bins for computing exchange terms
delta_rprime = 0.1
#r_bins = np.arange(0.0, 1.1, delta_rprime)
r_bins = np.arange(0.0,1.1,delta_rprime)

########## Figure out which input files of tracer trajectories we will need to read in ###########
if downsample:
    n_tr_samples = 1
    filename = "saved_tracers_select_"+Zoom+"_"+Run + "_ds_"+ n_downsample + ".hdf5"
    filenames_list = [filename]

    example_File = h5py.File(InDir+filename,"r")
    
else:
    import os
    files = os.listdir(InDir)
    filename_base = "saved_tracers_select_"+Zoom+"_"+Run

    filenames_list = []
    for file_n in files:
        if filename_base in file_n and "subset" in file_n:
            filenames_list.append(file_n)
            n_tr_samples = len(filenames_list)

            example_File = h5py.File(InDir+file_n,"r")

    
# Count recorded number of tracer trajectory timesteps so we can shape output array
if downsample and n_downsample == "3000000.0":
    n_ts = len(example_File["timestep"][:])
else:
    for group in example_File:
        if "timestep" in group:
            n_ts = len(example_File[group+"/timestep"][:])
            break
example_File.close()


######## Stuff we are going to measure ########################
n_type = 4
select_types = ["m_tot","m_neut","RecLyaLum","CollLyaLum"]

if rascas:
    n_type = 10
    rascas_name_list = ["cL_h_rec_ism", "cL_h_rec_sat", "cL_h_rec_cgm", "cL_h_col_ism", "cL_h_col_sat", "cL_h_col_cgm"]
    select_types += rascas_name_list

if do_lya_phase_split: # Instead compute intrinsic lya luminosity in neutral/warm-ionized/hot-ionized phases
    select_types = ["LyaNeut","LyaWarmI", "LyaHotI"]
    n_type = 3
    if rascas:
        select_types += ["LyaEscNeut", "LyaEscWarmI", "LyaEscHotI"]
        n_type = 6
        
# "Gravity only" class
f_1st_infall_evo = np.zeros((n_type, n_ts, len(r_bins[1:]) ))
f_past_peri_evo = np.zeros((n_type, n_ts, len(r_bins[1:]) ))
f_past_apo_evo = np.zeros((n_type, n_ts, len(r_bins[1:]) ))

# Gravity only, but entered within a satellite
f_strip_1st_infall_evo = np.zeros((n_type, n_ts, len(r_bins[1:]) ))
f_strip_past_peri_evo = np.zeros((n_type, n_ts, len(r_bins[1:]) ))
f_strip_past_apo_evo = np.zeros((n_type, n_ts, len(r_bins[1:]) ))

# "feedback influenced" class
f_fb_1st_outflow_evo = np.zeros((n_type, n_ts, len(r_bins[1:]) ))
f_fb_past_apo_evo = np.zeros((n_type, n_ts, len(r_bins[1:]) ))
f_fb_past_peri_evo = np.zeros((n_type, n_ts, len(r_bins[1:]) ))

# Tot - note I switched from being fractions when I added radial bins - because you want to be able to normalise in multiple ways later, depending on whether you slice in radius or in time
f_tot_evo = np.zeros((n_type, n_ts, len(r_bins[1:]) ))

# Inside satellites
f_sat_evo = np.zeros((n_type, n_ts, len(r_bins[1:]) ))


############### Loop over tracer sub-samples (to avoid running out of memory) ###########
# Note we don't need to loop if downsample = True (provided you didn't select too many tracers)

for ifile, filename in enumerate(filenames_list):

    if not downsample:
        print "Reading/Computing for tracer subset", ifile+1, "of", len(filenames_list)
        
    File = h5py.File(InDir+filename,"r")
    
    tr_var_names = ["radius", "mass", "xhi", "RecLyaLum", "CollLyaLum", "family", "fb_flag_evo", "end_in_sat_evo", "enter_in_sat", "first_infall_ts", "part_orb_type_evo"]
    if do_lya_phase_split:
        tr_var_names = ["radius", "xhi", "RecLyaLum", "CollLyaLum", "family", "fb_flag_evo", "end_in_sat_evo", "enter_in_sat", "first_infall_ts", "part_orb_type_evo","temp"]

    if rascas:
        tr_var_names += rascas_name_list

    t = ldf.tracer_sample_class(File,tr_var_names,verbose=True,RT=RT)
    
    File.close()
    
    # Get time from redshift
    dm.jeje_utils.read_conversion_scales(RunDir,t.timestep[0])
    omega_0,lambda_0,little_h = dm.jeje_utils.read_cosmo_params(RunDir,t.timestep[0])
    a_in = 1./(1.+t.redshift)
    t.t, t.tlb = uc.t_Universe(a_in,omega_0,little_h)

    # Drop out tracers that never enter the virial radius for this final timestep
    ok = (np.isnan(t.tr_vars["first_infall_ts"]) == False)
    t.select_bool(ok)
        
    ############### Measure the basic fractions ##########################

    # Refactored version with fewer for loops
    ok = (t.tr_vars["radius"] > 0.2) & (t.tr_vars["radius"] < 1.0) & (t.tr_vars["family"] == 100) & (t.tr_vars["end_in_sat_evo"] == 0)
    ok_tot = (t.tr_vars["radius"] < 1.0) & (t.tr_vars["family"] == 100)
    ok_sat = (t.tr_vars["radius"] > 0.2) & (t.tr_vars["radius"] < 1.0) & (t.tr_vars["family"] == 100) & (t.tr_vars["end_in_sat_evo"] > 0)
    
    # Split into radial bins
    for i_r in range(len(r_bins[0:-1])):
        okr = ok & (t.tr_vars["radius"] > r_bins[i_r]) & (t.tr_vars["radius"] < r_bins[i_r+1])
        okr_tot = ok_tot & (t.tr_vars["radius"] > r_bins[i_r]) & (t.tr_vars["radius"] < r_bins[i_r+1])
        okr_sat = ok_sat & (t.tr_vars["radius"] > r_bins[i_r]) & (t.tr_vars["radius"] < r_bins[i_r+1])
        
        print "Working on radial bin", i_r, "of", len(r_bins[0:-1])
        
        for n in range(n_type):

            print "Working on type",n,"of",n_type
            if select_types[n] == "m_tot":
                quant = t.tr_vars["mass"]
            elif select_types[n] == "m_neut":
                quant = t.tr_vars["xhi"]*t.tr_vars["mass"]
            elif select_types[n] == "RecLyaLum":
                if RT:
                    quant = t.tr_vars["RecLyaLum"]
                else:
                    # Placeholder
                    quant = t.tr_vars["mass"]
            elif select_types[n] == "CollLyaLum":
                if RT:
                    quant = t.tr_vars["CollLyaLum"]
                else:
                    # Placeholder
                    quant = t.tr_vars["mass"]
            elif rascas and select_types[n] in rascas_name_list:
                quant = t.tr_vars[select_types[n]]
            elif select_types[n] == "LyaNeut":
                quant = (t.tr_vars["RecLyaLum"]+t.tr_vars["CollLyaLum"]) * t.tr_vars["xhi"]
            elif select_types[n] == "LyaWarmI":
                warm = t.tr_vars["temp"] < 10.**4.5 
                quant = (t.tr_vars["RecLyaLum"]+t.tr_vars["CollLyaLum"]) * (1.-t.tr_vars["xhi"])
                quant[warm==False] = 0.0
            elif select_types[n] == "LyaHotI":
                warm = t.tr_vars["temp"] < 10.**4.5 
                quant = (t.tr_vars["RecLyaLum"]+t.tr_vars["CollLyaLum"]) * (1.-t.tr_vars["xhi"])
                quant[warm] = 0.0

            elif select_types[n] == "LyaEscNeut":
                quant = np.zeros_like(t.tr_vars["xhi"])
                for name in rascas_name_list:
                    quant += t.tr_vars[name]
                quant *= t.tr_vars["xhi"]

            elif select_types[n] == "LyaEscWarmI":
                quant = np.zeros_like(t.tr_vars["xhi"])
                for name in rascas_name_list:
                    quant += t.tr_vars[name]
                warm = t.tr_vars["temp"] < 10.**4.5 
                quant *= (1-t.tr_vars["xhi"])
                quant[warm==False] = 0.0

            elif select_types[n] == "LyaEscHotI":
                quant = np.zeros_like(t.tr_vars["xhi"])
                for name in rascas_name_list:
                    quant += t.tr_vars[name]
                warm = t.tr_vars["temp"] < 10.**4.5 
                quant *= (1-t.tr_vars["xhi"])
                quant[warm] = 0.0
                
            else:
                print "Nope", select_types
                quit()

            okq = np.isnan(quant) == False
                
            # "Gravity only" class
            print "Doing grav only"
            # Note the += here comes from the tracer subsample loop - works because I don't do fractions now

            # Multi-dimensional sum of masked array requires special "masked" numpy input class
            mask = (t.tr_vars["part_orb_type_evo"] != 0) | (t.tr_vars["fb_flag_evo"] != 0) | (t.tr_vars["enter_in_sat"] != 0) | (okr==False) | (okq==False)
            temp = np.ma.array( quant, mask = mask )
            f_1st_infall_evo[n,:,i_r] += np.sum( temp,axis=1 )
            
            mask = (t.tr_vars["part_orb_type_evo"] != 1) | (t.tr_vars["fb_flag_evo"] != 0) | (t.tr_vars["enter_in_sat"] != 0) | (okr==False) | (okq==False)
            temp = np.ma.array( quant, mask = mask )
            f_past_peri_evo[n,:,i_r] += np.sum( temp ,axis=1 )
            
            mask = (t.tr_vars["part_orb_type_evo"] != 2) | (t.tr_vars["fb_flag_evo"] != 0) | (t.tr_vars["enter_in_sat"] != 0) | (okr==False) | (okq==False)
            temp = np.ma.array( quant, mask = mask )
            f_past_apo_evo[n,:,i_r] += np.sum( temp ,axis=1 )

            # Gravity only, but entered within a satellite
            print "Doing stripped"
            mask = (t.tr_vars["part_orb_type_evo"] != 0) | (t.tr_vars["fb_flag_evo"] != 0) | (t.tr_vars["enter_in_sat"] != 1) | (okr==False) | (okq==False)
            temp = np.ma.array( quant, mask = mask )
            f_strip_1st_infall_evo[n,:,i_r] += np.sum( temp ,axis=1 )

            mask = (t.tr_vars["part_orb_type_evo"] != 1) | (t.tr_vars["fb_flag_evo"] != 0) | (t.tr_vars["enter_in_sat"] != 1) | (okr==False) | (okq==False)
            temp = np.ma.array( quant, mask = mask )
            f_strip_past_peri_evo[n,:,i_r] += np.sum( temp ,axis=1 )

            mask = (t.tr_vars["part_orb_type_evo"] != 2) | (t.tr_vars["fb_flag_evo"] != 0) | (t.tr_vars["enter_in_sat"] != 1) | (okr==False) | (okq==False)
            temp = np.ma.array( quant, mask = mask )
            f_strip_past_apo_evo[n,:,i_r] += np.sum( temp ,axis=1 )
        
            # "feedback influenced" class        
            print "Doing fb influenced"
            mask = (t.tr_vars["fb_flag_evo"] != 1) | (okr==False) | (okq==False)
            temp = np.ma.array( quant, mask = mask )
            f_fb_1st_outflow_evo[n,:,i_r] += np.sum( temp,axis=1 )

            mask = (t.tr_vars["fb_flag_evo"] != 2) | (okr==False) | (okq==False)
            temp = np.ma.array( quant, mask = mask )
            f_fb_past_apo_evo[n,:,i_r] += np.sum( temp, axis=1 )

            mask = (t.tr_vars["fb_flag_evo"] != 3) | (okr==False) | (okq==False)
            temp = np.ma.array( quant, mask = mask )
            f_fb_past_peri_evo[n,:,i_r] += np.sum( temp, axis=1)

            temp = np.ma.array( quant, mask = (okr_tot==False)|(okq==False) )
            f_tot_evo[n,:,i_r] += np.sum(temp,axis=1)

            temp = np.ma.array( quant, mask = (okr_sat==False)|(okq==False) )
            f_sat_evo[n,:,i_r] += np.sum(temp,axis=1)

# Write the cgm fractions to disk

# Read in relevant tree information from the hdf5 file
filename = Zoom + "_" + Run + "_tsf" + str(timestep_final) +".hdf5"

output_File = h5py.File(OutDir+filename)
zoom_group = output_File[Zoom]

for halo_group_str in zoom_group:
    # Only compute fluxes for the main halo
    if not "_main" in halo_group_str:
        continue
    mp_group = zoom_group[halo_group_str+"/main_progenitors"]

timesteps = t.timestep
timesteps_out = mp_group["halo_ts"][:] +1 # +1 is for the tree/simulation indexing mismatch

n_ts = t.n_ts()
n_ts_out = len(timesteps_out)

f_1st_infall_evo_out = np.zeros((n_type, n_ts_out, len(r_bins[1:]) ))
f_past_peri_evo_out  = np.zeros_like(f_1st_infall_evo_out)
f_past_apo_evo_out = np.zeros_like(f_1st_infall_evo_out)
f_strip_1st_infall_evo_out = np.zeros_like(f_1st_infall_evo_out)
f_strip_past_peri_evo_out = np.zeros_like(f_1st_infall_evo_out)
f_strip_past_apo_evo_out = np.zeros_like(f_1st_infall_evo_out)
f_fb_1st_outflow_evo_out = np.zeros_like(f_1st_infall_evo_out)
f_fb_past_apo_evo_out = np.zeros_like(f_1st_infall_evo_out)
f_fb_past_peri_evo_out = np.zeros_like(f_1st_infall_evo_out)
f_tot_evo_out = np.zeros_like(f_1st_infall_evo_out)
f_sat_evo_out = np.zeros_like(f_1st_infall_evo_out)

for n in range(n_ts):
    i_ts_out = np.where(timesteps[n] == timesteps_out)[0][0]

    f_1st_infall_evo_out[:, i_ts_out] = f_1st_infall_evo[:,n]
    f_past_peri_evo_out[:, i_ts_out] = f_past_peri_evo[:,n]
    f_past_apo_evo_out[:, i_ts_out] = f_past_apo_evo[:,n]
    f_strip_1st_infall_evo_out[:, i_ts_out] = f_strip_1st_infall_evo[:,n]
    f_strip_past_peri_evo_out[:, i_ts_out] = f_strip_past_peri_evo[:,n]
    f_strip_past_apo_evo_out[:, i_ts_out] = f_strip_past_apo_evo[:,n]
    f_fb_1st_outflow_evo_out[:, i_ts_out] = f_fb_1st_outflow_evo[:,n]
    f_fb_past_apo_evo_out[:, i_ts_out] = f_fb_past_apo_evo[:,n]
    f_fb_past_peri_evo_out[:, i_ts_out] = f_fb_past_peri_evo[:,n]
    f_tot_evo_out[:, i_ts_out] = f_tot_evo[:,n]
    f_sat_evo_out[:, i_ts_out] = f_sat_evo[:,n]
    
names_base = ["1st_infall_", "past_peri_", "past_apo_"]
names = [] 
for n in range(3):
    for m in range(len(select_types)):
        for o in range(3):
            name_base = names_base[n]
            if o == 1:
                name_base += "stripped_"
            if o == 2:
                name_base += "fb_"
            name_base += select_types[m] + "_evo"

            names.append(name_base)

names_tot = []
for n in range(len(select_types)):
    name_base = "tot_"+select_types[n] + "_evo"
    names_tot.append(name_base)

names_sat = []
for n in range(len(select_types)):
    name_base = "sat_"+select_types[n] + "_evo"
    names_sat.append(name_base)

for dset in mp_group:
    for name in names:
        if name == dset:
            del mp_group[dset]
    for name in names_tot:
        if name == dset:
            del mp_group[dset]
    for name in names_sat:
        if name == dset:
            del mp_group[dset]
            
i_name = 0
for n in range(3):
    for m in range(len(select_types)):
        for o in range(3):

            if n == 0 and o == 0:
                dout = f_1st_infall_evo_out                
            elif n == 1 and o ==0:
                dout = f_past_peri_evo_out
            elif n == 2 and o ==0:
                dout = f_past_apo_evo_out
            elif n == 0 and o ==1:
                dout = f_strip_1st_infall_evo_out
            elif n == 1 and o ==1:
                dout = f_strip_past_peri_evo_out
            elif n == 2 and o ==1:
                dout = f_strip_past_apo_evo_out
            elif n == 0 and o ==2:
                dout = f_fb_1st_outflow_evo_out
            elif n == 1 and o ==2:
                dout = f_fb_past_peri_evo_out
            elif n == 2 and o ==2:
                dout = f_fb_past_apo_evo_out

            name = names[i_name]
            mp_group.create_dataset(name,data=dout[m])
            i_name += 1

for n in range(len(select_types)):
    name = names_tot[n]
    mp_group.create_dataset(name,data=f_tot_evo_out[n])

for n in range(len(select_types)):
    name = names_sat[n]
    mp_group.create_dataset(name,data=f_sat_evo_out[n])

print "got to the end :-)"
