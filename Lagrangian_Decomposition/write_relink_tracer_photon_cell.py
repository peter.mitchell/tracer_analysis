# This script:
# 1) reads in a sample of pre-processed tracers (with Lagrangian status already assigned)
# 2) links those tracers with cells > photons from hdf5 dump
# 3) computes the fraction of escaped luminosity for each escaping photons associated with a given lagrangian component
# 4) sums the photon spectra in bins of radius for each lagrangian component

import numpy as np

Zoom = "Zoom-7-5092"
Run = "RhdRun-tracer_radfix"
timestep_final = 155
RT = True
RunDir = "/cral2/mitchell/"+Zoom+"/"+Run
InDir = "/cral2/mitchell/PostProcess/"
OutDir = "/cral2/mitchell/PostProcess/"

#rascas_analysis = "Rvir" # Older set of rascas runs that went out to Rvir (adaptahop def)
#rascas_analysis = "6arcsec" # Newer runs that go out to 6 arcseconds
rascas_analysis = "11arcsec" # Newer runs that go out to 11 arcseconds

downsample = True
#n_downsample = "1000"
n_downsample = "3000000.0"

######## Wavelength bins for spectra - should match rascas setup ################
n_wvl_bins = 200
# Radial bins for spectra are now change set the same as the rad_bins_proj_phys below
#rad_bins = np.array([0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0, 1.25, 1.5, 2.0])
#n_rad_bins = len(rad_bins[0:-1])


######## Radius bins for projected radial profiles ############################
# Normalised by Rvir
delta_rprime = 0.05
rad_bins_proj = np.arange(0.0,2.05,delta_rprime)
n_rad_bins_proj = len(rad_bins_proj[0:-1])

# Physical (note r200 is 40 kpc at z=3 for 5092)
rad_bins_proj_phys = np.arange(0.0,42.,2.)
n_rad_bins_proj_phys = len(rad_bins_proj_phys[0:-1])


if downsample:
    batch_no = 1
    
    filename = "saved_tracers_select_"+Zoom+"_"+Run + "_ds_"+ n_downsample + ".hdf5"
    filenames_list = [filename]
else:
    import os
    files = os.listdir(InDir)
    filename_base = "saved_tracers_select_"+Zoom+"_"+Run
    
    filenames_list = []
    for file_n in files:
        if filename_base in file_n and "subset" in file_n:
            filenames_list.append(file_n)
    batch_no = len(filenames_list)



HaloDir = RunDir + "/Halos/"
TreeDir = RunDir + "/Trees/"

import sys
sys.path.append("../.")
import dm_utils as dm
from scipy import spatial
import h5py
import utilities_ramses as ur
import neighbour_utils as nu
import match_searchsorted as ms
import cutils as cut
import flux_utils
sys.path.append("../Rascas_analysis/.")
import jphot as jp





############## Zoom file IO #########################
# Read in relevant tree information from the hdf5 file
filename_zoom = Zoom + "_" + Run + "_tsf" + str(timestep_final) +".hdf5"
    
File = h5py.File(OutDir+filename_zoom)
    
# Open the subvolume file (containing all cell data)
subvol_filename = "subvol_"+filename_zoom
subvol_File = h5py.File(OutDir+subvol_filename,"r")

# Check the file with the tree information already exists
if not Zoom in File:
    print "Error: Tree information for ", Zoom, " in ", filename_zoom, " cannot be read"
    exit()

zoom_group = File[Zoom]


######### Setup the output arrays ##########
# Get number of timesteps
for halo_group_str in zoom_group:
    if "_main" in halo_group_str:
        halo_group_str_use= halo_group_str
mp_group = zoom_group[halo_group_str_use+"/main_progenitors"]
n_ts = len(mp_group["halo_ts"][:])
timesteps = (mp_group["halo_ts"][:]).astype("int")+1 # tree/sim mismatch

n_g = 3 # Splits on point of origin

spectrum_rec_tot = np.zeros((n_ts, n_rad_bins_proj_phys, n_wvl_bins)) # Note the total also includes photons that cannot be linked to tracers, so this is NOT the sum of the other components
spectrum_rec_1st_infall = np.zeros((n_g,n_ts, n_rad_bins_proj_phys, n_wvl_bins))
spectrum_rec_orbit = np.zeros((n_g,n_ts, n_rad_bins_proj_phys, n_wvl_bins))
spectrum_rec_stripped = np.zeros((n_g,n_ts, n_rad_bins_proj_phys, n_wvl_bins))
spectrum_rec_fb = np.zeros((n_g,n_ts, n_rad_bins_proj_phys, n_wvl_bins))
spectrum_rec_ism = np.zeros((n_g,n_ts, n_rad_bins_proj_phys, n_wvl_bins))
spectrum_rec_sat = np.zeros((n_g,n_ts, n_rad_bins_proj_phys, n_wvl_bins))

spectrum_col_tot = np.zeros((n_ts, n_rad_bins_proj_phys, n_wvl_bins))
spectrum_col_1st_infall = np.zeros((n_g,n_ts, n_rad_bins_proj_phys, n_wvl_bins))
spectrum_col_orbit = np.zeros((n_g,n_ts, n_rad_bins_proj_phys, n_wvl_bins))
spectrum_col_stripped = np.zeros((n_g,n_ts, n_rad_bins_proj_phys, n_wvl_bins))
spectrum_col_fb = np.zeros((n_g,n_ts, n_rad_bins_proj_phys, n_wvl_bins))
spectrum_col_ism = np.zeros((n_g,n_ts, n_rad_bins_proj_phys, n_wvl_bins))
spectrum_col_sat = np.zeros((n_g,n_ts, n_rad_bins_proj_phys, n_wvl_bins))

# Projected profiles (normalised to rvir)
proj_rec_tot = np.zeros((n_ts, n_rad_bins_proj))
proj_rec_1st_infall = np.zeros((n_g,n_ts, n_rad_bins_proj))
proj_rec_orbit = np.zeros((n_g,n_ts, n_rad_bins_proj))
proj_rec_stripped = np.zeros((n_g,n_ts, n_rad_bins_proj))
proj_rec_fb = np.zeros((n_g,n_ts, n_rad_bins_proj))
proj_rec_ism = np.zeros((n_g,n_ts, n_rad_bins_proj))
proj_rec_sat = np.zeros((n_g,n_ts, n_rad_bins_proj))

proj_col_tot = np.zeros((n_ts, n_rad_bins_proj))
proj_col_1st_infall = np.zeros((n_g,n_ts, n_rad_bins_proj))
proj_col_orbit = np.zeros((n_g,n_ts, n_rad_bins_proj))
proj_col_stripped = np.zeros((n_g,n_ts, n_rad_bins_proj))
proj_col_fb = np.zeros((n_g,n_ts, n_rad_bins_proj))
proj_col_ism = np.zeros((n_g,n_ts, n_rad_bins_proj))
proj_col_sat = np.zeros((n_g,n_ts, n_rad_bins_proj))

# Projected profiles (not normalised)
proj_rec_tot_phys = np.zeros((n_ts, n_rad_bins_proj_phys))
proj_rec_1st_infall_phys = np.zeros((n_g,n_ts, n_rad_bins_proj_phys))
proj_rec_orbit_phys = np.zeros((n_g,n_ts, n_rad_bins_proj_phys))
proj_rec_stripped_phys = np.zeros((n_g,n_ts, n_rad_bins_proj_phys))
proj_rec_fb_phys = np.zeros((n_g,n_ts, n_rad_bins_proj_phys))
proj_rec_ism_phys = np.zeros((n_g,n_ts, n_rad_bins_proj_phys))
proj_rec_sat_phys = np.zeros((n_g,n_ts, n_rad_bins_proj_phys))

proj_col_tot_phys = np.zeros((n_ts, n_rad_bins_proj_phys))
proj_col_1st_infall_phys = np.zeros((n_g,n_ts, n_rad_bins_proj_phys))
proj_col_orbit_phys = np.zeros((n_g,n_ts, n_rad_bins_proj_phys))
proj_col_stripped_phys = np.zeros((n_g,n_ts, n_rad_bins_proj_phys))
proj_col_fb_phys = np.zeros((n_g,n_ts, n_rad_bins_proj_phys))
proj_col_ism_phys = np.zeros((n_g,n_ts, n_rad_bins_proj_phys))
proj_col_sat_phys = np.zeros((n_g,n_ts, n_rad_bins_proj_phys))

# Loop over pre-processed tracer files
print "Reading pre-processed tracer data"

for ifile, filename in enumerate(filenames_list):

    if not downsample:
        print "Processing tracer subset", ifile, "of", batch_no

    print "Reading", InDir+filename
    File_tr = h5py.File(InDir+filename,"r")

    for group in File_tr:
        if "timestep" in group and "_" not in group:
            t_timesteps = File_tr[group][:]

    print "Reading tid"
    tid = File_tr["tid"][:]
    n_tr = len(tid)

    trad_list = np.zeros((len(t_timesteps), n_tr))
    tx_list = np.zeros_like(trad_list)
    ty_list = np.zeros_like(trad_list)
    tz_list = np.zeros_like(trad_list)
    tcomp_list = np.zeros_like(trad_list)
    tfam_list = np.zeros_like(trad_list)
    trec_list_ism = np.zeros_like(trad_list)
    trec_list_cgm = np.zeros_like(trad_list)
    trec_list_sat = np.zeros_like(trad_list)
    tcol_list_ism = np.zeros_like(trad_list)
    tcol_list_cgm = np.zeros_like(trad_list)
    tcol_list_sat = np.zeros_like(trad_list)

    print "Reading stripped status"
    t_stripped_flag_list = File_tr["enter_in_sat"][:]
    print "Reading orbital status"
    t_orb_flag_list = File_tr["part_orb_type_evo"][:]
    print "Reading feedback status"
    t_fb_flag_list = File_tr["fb_flag_evo"][:]

    for i_ts, timestep in enumerate(t_timesteps):
        print "Reading tracers for",i_ts,"of",len(t_timesteps)
        trad_list[i_ts] = File_tr["timestep_"+str(timestep)+"/r"][:]

        tx_list[i_ts] = File_tr["timestep_"+str(timestep)+"/x"][:]
        ty_list[i_ts] = File_tr["timestep_"+str(timestep)+"/y"][:]
        tz_list[i_ts] = File_tr["timestep_"+str(timestep)+"/z"][:]
        tcomp_list[i_ts] = File_tr["timestep_"+str(timestep)+"/comp"][:]
        tfam_list[i_ts] = File_tr["timestep_"+str(timestep)+"/family"][:]
        trec_list_ism[i_ts] = File_tr["timestep_"+str(timestep)+"/cL_h_rec_ism"][:]
        trec_list_sat[i_ts] += File_tr["timestep_"+str(timestep)+"/cL_h_rec_sat"][:]
        trec_list_cgm[i_ts] += File_tr["timestep_"+str(timestep)+"/cL_h_rec_cgm"][:]
        tcol_list_ism[i_ts] = File_tr["timestep_"+str(timestep)+"/cL_h_col_ism"][:]
        tcol_list_sat[i_ts] += File_tr["timestep_"+str(timestep)+"/cL_h_col_sat"][:]
        tcol_list_cgm[i_ts] += File_tr["timestep_"+str(timestep)+"/cL_h_col_cgm"][:]
        
            
    # Loop over timesteps
    for i_timestep, timestep in enumerate(timesteps):
        timestep_tree = timestep -1 # Stupid mismatch between merger tree files and everything else

        #if timestep < 140:
        #    if i_timestep == 0:
        #        print "skip"
        #    continue
        
        if timestep not in t_timesteps:
            print "No pre-processed tracer data for this timestep, skipping"
            continue
        
        i_ts = np.argmin(abs(timestep-t_timesteps))
        
        # Get conversions for different units
        r2kpc, redshift, little_h, m2Msun,rho2msunpkpc3,v2kms = ur.Set_Units(RunDir, timestep)
        boxlen_pkpc = dm.jeje_utils.get_param_real(RunDir,timestep,'boxlen') * r2kpc # Box size in proper kpc, using the correct box size

        #if i_timestep < 145:
        #    print "skipping"
        #    continue

        print "processing timestep = ", timestep, "of", timestep_final, ", redshift is", redshift

        ######## Read Rascas photon data ###########
        snap_str = str(timestep)
        if len(snap_str) == 1:
            snap_str = "0000"+snap_str
        elif len(snap_str) == 2:
            snap_str = "000"+snap_str
        elif len(snap_str) == 3:
            snap_str = "00"+snap_str
        else:
            print "Error", snap_str
            quit()

        print "Reading rascas data"

        if rascas_analysis == "Rvir":
            rascas_str = "_Rvir"
        elif rascas_analysis == "6arcsec":
            rascas_str = "_6arcsec"
        elif rascas_analysis == "11arcsec":
            rascas_str = "_11arcsec"
        else:
            print "rascas_analysis", rascas_analysis, "not recognised"
            quit()

        # Photons tracing collisional excitations
        ex_rc_path_col = RunDir +"/RASCAS/"+snap_str+"/ColLya_CS100"+rascas_str+"/"
        icFile_path_col = ex_rc_path_col + "001.IC" # For now I'm assuming there is only one rascas output per simulation output ("001")
        resFile_path_col = ex_rc_path_col + "001.res"

        # Photons tracing recombinations
        ex_rc_path_rec = RunDir +"/RASCAS/"+snap_str+"/RecLya_CS100"+rascas_str+"/"
        icFile_path_rec = ex_rc_path_rec + "001.IC" # For now I'm assuming there is only one rascas output per simulation output ("001")
        resFile_path_rec = ex_rc_path_rec + "001.res"

        try:
            coll_p = jp.photonlist(icFile_path_col, resFile_path_col)
            rec_p = jp.photonlist(icFile_path_rec, resFile_path_rec)
        except:
            print "Couldn't read rascas data for this timestep, moving on"
            continue
            
        # We only care about escaped photons
        esc = np.where(coll_p.status == 1)[0]
        n_col_b4 = len(coll_p.x)
        coll_p = coll_p.extract_sample(esc)
        
        esc = np.where(rec_p.status == 1)[0]
        n_rec_b4 = len(rec_p.x)
        rec_p = rec_p.extract_sample(esc)

        print "Number of rascas photons", n_col_b4, n_rec_b4, ", number that escape is", len(coll_p.x), len(rec_p.x)
        
        ####### Select (preprocessed) tracer data ###########
        print "Selecing tracer data for this timestep"
        tx = []; ty = []; tz = []; tcomp = []; tfam = []; trec = []; tcol = []; trad = []
        t_stripped_flag = []; t_orb_flag = []; t_fb_flag = []

        trad = trad_list[i_ts]
        tfam = tfam_list[i_ts]

        ok = (np.isnan(trad) == False) & (tfam==100)
        trad = trad[ok]
        tfam = tfam[ok]

        tx = tx_list[i_ts][ok]
        ty = ty_list[i_ts][ok]
        tz = tz_list[i_ts][ok]
        t_stripped_flag = t_stripped_flag_list[ok]
        t_orb_flag = t_orb_flag_list[i_ts,ok]
        t_fb_flag = t_fb_flag_list[i_ts,ok]
        tcomp = tcomp_list[i_ts][ok]
        trec_ism = trec_list_ism[i_ts][ok]
        trec_cgm = trec_list_cgm[i_ts][ok]
        trec_sat = trec_list_sat[i_ts][ok]
        tcol_ism = tcol_list_ism[i_ts][ok]
        tcol_cgm = tcol_list_cgm[i_ts][ok]
        tcol_sat = tcol_list_sat[i_ts][ok]

        print "Grouping pre-processed tracer data"

        # Group the Lagrangian status that we are interested in
        t_1st_infall = np.zeros_like(tx)
        t_orbit = np.zeros_like(tx)
        t_stripped = np.zeros_like(tx)
        t_fb = np.zeros_like(tx)
        t_ism = np.zeros_like(tx)
        t_sat = np.zeros_like(tx)

        # Relabel tracers that are in neighbouring central haloes as satellites
        tcomp[(tcomp==3)&(trad>1.0)] = 2
        
        ok = (t_orb_flag==0) & (t_fb_flag == 0) & (t_stripped_flag == 0) & (tfam == 100) & ((tcomp == 0)|(tcomp == 3)) & (trad>0.2)
        t_1st_infall[ok] += 1

        ok = (t_orb_flag>0) & (t_fb_flag == 0) & (t_stripped_flag == 0) & (tfam == 100) & ((tcomp == 0)|(tcomp == 3)) & (trad>0.2)
        t_orbit[ok] += 1

        ok = (t_fb_flag == 0) & (t_stripped_flag == 1) & (tfam == 100) & ((tcomp == 0)|(tcomp == 3)) & (trad>0.2)
        t_stripped[ok] += 1

        ok = (t_fb_flag > 0) & (tfam == 100) & ((tcomp == 0)|(tcomp == 3)) & (trad>0.2)
        t_fb[ok] += 1

        ok = (tfam == 100) & (trad<=0.2)
        t_ism[ok] += 1

        ok = (tfam == 100) & ((tcomp > 0)&(tcomp<3)) & (trad>0.2)
        t_sat[ok] += 1


        ########## Read cell data #########################
        print "Reading cell data"

        ts_group = subvol_File["timestep_"+str(timestep)]
        cell_group = ts_group["Gas_leaf_cell_data"]
        cx = cell_group["x_c"][:] # Cell coordinates
        cy = cell_group["y_c"][:]
        cz = cell_group["z_c"][:]
        clev = cell_group["lev"][:]


        ######### Efficiently find cells inside rvir ###########
        cxyz_min = min(cx.min(),cy.min(),cz.min())
        cxyz_max = max(cx.max(),cy.max(),cz.max())

        cx_rescale = (cx - cxyz_min) / (cxyz_max-cxyz_min)
        cy_rescale = (cy - cxyz_min) / (cxyz_max-cxyz_min)
        cz_rescale = (cz - cxyz_min) / (cxyz_max-cxyz_min)

        nchain_cells = 20
        nextincell_c,firstincell_c = nu.neighbour_utils.init_chain(cx_rescale,cy_rescale,cz_rescale,nchain_cells)

        # Factor to help convert into box units (note maxime's merger_tree reader has converted halo positions so be careful here!)
        r2kpc_ideal, redshift, little_h, m2Msun,junk,junk = ur.Set_Units(RunDir, timestep, "ideal")
        boxlen_cMpc = dm.jeje_utils.get_param_real(RunDir,timestep,'boxlen') # Box size in box units
        boxlen_cMpc *= r2kpc_ideal
        boxlen_cMpc = boxlen_cMpc * (1+redshift) /1e3 # cMpc (comoving)
        boxlen_cMpc = boxlen_cMpc * little_h # h^-1 cMpc
        factor = 1./boxlen_cMpc

        # Identify the main progenitor at this timestep
        ok_ts = np.array(mp_group["halo_ts"])==timestep_tree # haloes on this timestep

        xh = mp_group["x"][ok_ts] * factor + 0.5
        yh = mp_group["y"][ok_ts] * factor + 0.5
        zh = mp_group["z"][ok_ts] * factor + 0.5

        # Put the spectra into the rest frame of the halo
        vxh = mp_group["vx"][ok_ts] # Halo velocity in kms^-1
        vyh = mp_group["vy"][ok_ts] 
        vzh = mp_group["vz"][ok_ts]

        #wvl, flux = rec_p.spectrum(nbins=n_wvl_bins)

        rec_p.correct_halo_velocity(vxh,vyh,vzh)

        # Block for checking the effect of correcting the halo velocity on the spectrum
        '''wvl2, flux2 = rec_p.spectrum(nbins=n_wvl_bins)

        temp = np.cumsum(flux)
        wvl_50 = wvl[np.argmin(abs(temp - np.sum(flux)*0.5))]
        temp2 = np.cumsum(flux2)
        wvl_50_2 = wvl2[np.argmin(abs(temp2 - np.sum(flux2)*0.5))]
        
        from utilities_plotting import *
        
        py.plot(wvl,flux,linestyle='-')
        py.plot(wvl2,flux2,linestyle='--')
        py.axvline(1215.6701,c="k",linewidth=1)
        py.axvline(wvl_50,c="b",linewidth=1)
        py.axvline(wvl_50_2,c="g",linewidth=1)
        py.show()
        quit()'''

        coll_p.correct_halo_velocity(vxh,vyh,vzh)

        # Use r200 for central haloes, and rvir (for now) for satellite haloes
        if mp_group["host_halo_id"][ok_ts] == mp_group["halo_id"][ok_ts]:
            rvirh = mp_group["r200"][ok_ts] / r2kpc           
        else:
            rvirh = mp_group["rvir"][ok_ts] * factor

        # Select cells in the vicinity of the halo (using a chain algorithm)
        rmax = 2*rvirh # 2 R_vir matches the initial selection of tracers earlier in the pipeline (at least at time of writing!)

        print "Selecting cells within rvir"

        ###### Cells within virial radius #########
        # Rescale halo positions for efficient cell division in chain algorithm
        xh_rescale = (xh-cxyz_min)/(cxyz_max-cxyz_min)
        yh_rescale = (yh-cxyz_min)/(cxyz_max-cxyz_min)
        zh_rescale = (zh-cxyz_min)/(cxyz_max-cxyz_min)
        rmax_rescale = rmax/(cxyz_max-cxyz_min)

        ncell_in_rmax = nu.neighbour_utils.get_npart_in_sphere_with_chain(RunDir,timestep,cx_rescale,cy_rescale,cz_rescale,xh_rescale,yh_rescale,zh_rescale,rmax_rescale,firstincell_c,nextincell_c)
        if ncell_in_rmax > 0:
            select_c = nu.neighbour_utils.\
                       get_part_indicies_in_sphere_with_chain(\
                                                              RunDir,timestep,cx_rescale,cy_rescale,cz_rescale,\
                                                              xh_rescale,yh_rescale,zh_rescale,\
                                                              rmax_rescale,ncell_in_rmax,firstincell_c,nextincell_c)
        else:
            select_c = np.zeros_like(cx) < 0

        ######## Select cells inside rvir ############
        cx_h = cx[select_c]
        cy_h = cy[select_c]
        cz_h = cz[select_c]

        cx_h = cx_h - xh; cy_h = cy_h - yh; cz_h = cz_h -zh
        cr_h = np.sqrt(np.square(cx_h) + np.square(cy_h) + np.square(cz_h))

        clev_h = clev[select_c]
        csize_h = boxlen_pkpc / np.power(2.,clev_h) / r2kpc

        tx *= rvirh; ty *= rvirh; tz *= rvirh

        ######## Compute escaping Lyman alpha luminosity-weighted tracer status of each cell ###########
        print "Matching cells, tracers, and photons, computing spectra"

        ind_c, ok_match = flux_utils.Match_Cell_Particle(csize_h, cx_h, cy_h, cz_h, tx, ty, tz)

        '''print "cell radius distn", np.sort(cr_h) / rvirh
        tr = np.sqrt( np.square(tx) + np.square(ty) + np.square(tz) ) / rvirh
        print "tracer radius distn", np.sort(tr)
        print "matched tracer readius distn", np.sort(tr[ok_match]), np.sort(cr_h[ind_c][ok_match]) / rvirh
        quit()'''
        
        crec_ism_1st_infall = flux_utils.Sum_Common_ID( [trec_ism[ok_match] * t_1st_infall[ok_match]], ind_c[ok_match], np.arange(len(cx_h)) )[0]
        crec_ism_orbit = flux_utils.Sum_Common_ID( [trec_ism[ok_match] * t_orbit[ok_match]], ind_c[ok_match], np.arange(len(cx_h)) )[0]
        crec_ism_stripped = flux_utils.Sum_Common_ID( [trec_ism[ok_match] * t_stripped[ok_match]], ind_c[ok_match], np.arange(len(cx_h)) )[0]
        crec_ism_fb = flux_utils.Sum_Common_ID( [trec_ism[ok_match] * t_fb[ok_match]], ind_c[ok_match], np.arange(len(cx_h)) )[0]
        crec_ism_ism = flux_utils.Sum_Common_ID( [trec_ism[ok_match] * t_ism[ok_match]], ind_c[ok_match], np.arange(len(cx_h)) )[0]
        crec_ism_sat = flux_utils.Sum_Common_ID( [trec_ism[ok_match] * t_sat[ok_match]], ind_c[ok_match], np.arange(len(cx_h)) )[0]

        crec_cgm_1st_infall = flux_utils.Sum_Common_ID( [trec_cgm[ok_match] * t_1st_infall[ok_match]], ind_c[ok_match], np.arange(len(cx_h)) )[0]
        crec_cgm_orbit = flux_utils.Sum_Common_ID( [trec_cgm[ok_match] * t_orbit[ok_match]], ind_c[ok_match], np.arange(len(cx_h)) )[0]
        crec_cgm_stripped = flux_utils.Sum_Common_ID( [trec_cgm[ok_match] * t_stripped[ok_match]], ind_c[ok_match], np.arange(len(cx_h)) )[0]
        crec_cgm_fb = flux_utils.Sum_Common_ID( [trec_cgm[ok_match] * t_fb[ok_match]], ind_c[ok_match], np.arange(len(cx_h)) )[0]
        crec_cgm_ism = flux_utils.Sum_Common_ID( [trec_cgm[ok_match] * t_ism[ok_match]], ind_c[ok_match], np.arange(len(cx_h)) )[0]
        crec_cgm_sat = flux_utils.Sum_Common_ID( [trec_cgm[ok_match] * t_sat[ok_match]], ind_c[ok_match], np.arange(len(cx_h)) )[0]

        crec_sat_1st_infall = flux_utils.Sum_Common_ID( [trec_sat[ok_match] * t_1st_infall[ok_match]], ind_c[ok_match], np.arange(len(cx_h)) )[0]
        crec_sat_orbit = flux_utils.Sum_Common_ID( [trec_sat[ok_match] * t_orbit[ok_match]], ind_c[ok_match], np.arange(len(cx_h)) )[0]
        crec_sat_stripped = flux_utils.Sum_Common_ID( [trec_sat[ok_match] * t_stripped[ok_match]], ind_c[ok_match], np.arange(len(cx_h)) )[0]
        crec_sat_fb = flux_utils.Sum_Common_ID( [trec_sat[ok_match] * t_fb[ok_match]], ind_c[ok_match], np.arange(len(cx_h)) )[0]
        crec_sat_ism = flux_utils.Sum_Common_ID( [trec_sat[ok_match] * t_ism[ok_match]], ind_c[ok_match], np.arange(len(cx_h)) )[0]
        crec_sat_sat = flux_utils.Sum_Common_ID( [trec_sat[ok_match] * t_sat[ok_match]], ind_c[ok_match], np.arange(len(cx_h)) )[0]

        ccol_ism_1st_infall = flux_utils.Sum_Common_ID( [tcol_ism[ok_match] * t_1st_infall[ok_match]], ind_c[ok_match], np.arange(len(cx_h)) )[0]
        ccol_ism_orbit = flux_utils.Sum_Common_ID( [tcol_ism[ok_match] * t_orbit[ok_match]], ind_c[ok_match], np.arange(len(cx_h)) )[0]
        ccol_ism_stripped = flux_utils.Sum_Common_ID( [tcol_ism[ok_match] * t_stripped[ok_match]], ind_c[ok_match], np.arange(len(cx_h)) )[0]
        ccol_ism_fb = flux_utils.Sum_Common_ID( [tcol_ism[ok_match] * t_fb[ok_match]], ind_c[ok_match], np.arange(len(cx_h)) )[0]
        ccol_ism_ism = flux_utils.Sum_Common_ID( [tcol_ism[ok_match] * t_ism[ok_match]], ind_c[ok_match], np.arange(len(cx_h)) )[0]
        ccol_ism_sat = flux_utils.Sum_Common_ID( [tcol_ism[ok_match] * t_sat[ok_match]], ind_c[ok_match], np.arange(len(cx_h)) )[0]

        ccol_cgm_1st_infall = flux_utils.Sum_Common_ID( [tcol_cgm[ok_match] * t_1st_infall[ok_match]], ind_c[ok_match], np.arange(len(cx_h)) )[0]
        ccol_cgm_orbit = flux_utils.Sum_Common_ID( [tcol_cgm[ok_match] * t_orbit[ok_match]], ind_c[ok_match], np.arange(len(cx_h)) )[0]
        ccol_cgm_stripped = flux_utils.Sum_Common_ID( [tcol_cgm[ok_match] * t_stripped[ok_match]], ind_c[ok_match], np.arange(len(cx_h)) )[0]
        ccol_cgm_fb = flux_utils.Sum_Common_ID( [tcol_cgm[ok_match] * t_fb[ok_match]], ind_c[ok_match], np.arange(len(cx_h)) )[0]
        ccol_cgm_ism = flux_utils.Sum_Common_ID( [tcol_cgm[ok_match] * t_ism[ok_match]], ind_c[ok_match], np.arange(len(cx_h)) )[0]
        ccol_cgm_sat = flux_utils.Sum_Common_ID( [tcol_cgm[ok_match] * t_sat[ok_match]], ind_c[ok_match], np.arange(len(cx_h)) )[0]

        ccol_sat_1st_infall = flux_utils.Sum_Common_ID( [tcol_sat[ok_match] * t_1st_infall[ok_match]], ind_c[ok_match], np.arange(len(cx_h)) )[0]
        ccol_sat_orbit = flux_utils.Sum_Common_ID( [tcol_sat[ok_match] * t_orbit[ok_match]], ind_c[ok_match], np.arange(len(cx_h)) )[0]
        ccol_sat_stripped = flux_utils.Sum_Common_ID( [tcol_sat[ok_match] * t_stripped[ok_match]], ind_c[ok_match], np.arange(len(cx_h)) )[0]
        ccol_sat_fb = flux_utils.Sum_Common_ID( [tcol_sat[ok_match] * t_fb[ok_match]], ind_c[ok_match], np.arange(len(cx_h)) )[0]
        ccol_sat_ism = flux_utils.Sum_Common_ID( [tcol_sat[ok_match] * t_ism[ok_match]], ind_c[ok_match], np.arange(len(cx_h)) )[0]
        ccol_sat_sat = flux_utils.Sum_Common_ID( [tcol_sat[ok_match] * t_sat[ok_match]], ind_c[ok_match], np.arange(len(cx_h)) )[0]

        crec_tot = flux_utils.Sum_Common_ID( [trec_ism[ok_match]+trec_cgm[ok_match]+trec_sat[ok_match]], ind_c[ok_match], np.arange(len(cx_h)) )[0]
        ccol_tot = flux_utils.Sum_Common_ID( [tcol_ism[ok_match]+tcol_cgm[ok_match]+tcol_sat[ok_match]], ind_c[ok_match], np.arange(len(cx_h)) )[0]

        print "number of rec/col lya photons", len(rec_p.x), len(coll_p.x), ", no of tracers inside rvir", len(trad[trad<1]), "no of cells within rvir", len(cx_h)

        print "number of tracers that have lya photons (either rec or col) associated"
        print len(trec_ism[(trec_ism+trec_cgm+trec_sat>0)|(tcol_ism+tcol_cgm+tcol_sat>0)])

        print "number of cells that have lya-linked-tracers inside"
        print len(crec_tot[(crec_tot>0)|(ccol_tot>0)])
        
        ok = crec_tot > 0
        crec_ism_1st_infall[ok] *= 1./crec_tot[ok]
        crec_ism_orbit[ok] *= 1./crec_tot[ok]
        crec_ism_stripped[ok] *= 1./crec_tot[ok]
        crec_ism_fb[ok] *= 1./crec_tot[ok]
        crec_ism_ism[ok] *= 1./crec_tot[ok]
        crec_ism_sat[ok] *= 1./crec_tot[ok]

        crec_sat_1st_infall[ok] *= 1./crec_tot[ok]
        crec_sat_orbit[ok] *= 1./crec_tot[ok]
        crec_sat_stripped[ok] *= 1./crec_tot[ok]
        crec_sat_fb[ok] *= 1./crec_tot[ok]
        crec_sat_ism[ok] *= 1./crec_tot[ok]
        crec_sat_sat[ok] *= 1./crec_tot[ok]

        crec_cgm_1st_infall[ok] *= 1./crec_tot[ok]
        crec_cgm_orbit[ok] *= 1./crec_tot[ok]
        crec_cgm_stripped[ok] *= 1./crec_tot[ok]
        crec_cgm_fb[ok] *= 1./crec_tot[ok]
        crec_cgm_ism[ok] *= 1./crec_tot[ok]
        crec_cgm_sat[ok] *= 1./crec_tot[ok]

        ok = ccol_tot > 0
        ccol_ism_1st_infall[ok] *= 1./ccol_tot[ok]
        ccol_ism_orbit[ok] *= 1./ccol_tot[ok]
        ccol_ism_stripped[ok] *= 1./ccol_tot[ok]
        ccol_ism_fb[ok] *= 1./ccol_tot[ok]
        ccol_ism_ism[ok] *= 1./ccol_tot[ok]
        ccol_ism_sat[ok] *= 1./ccol_tot[ok]

        ccol_cgm_1st_infall[ok] *= 1./ccol_tot[ok]
        ccol_cgm_orbit[ok] *= 1./ccol_tot[ok]
        ccol_cgm_stripped[ok] *= 1./ccol_tot[ok]
        ccol_cgm_fb[ok] *= 1./ccol_tot[ok]
        ccol_cgm_ism[ok] *= 1./ccol_tot[ok]
        ccol_cgm_sat[ok] *= 1./ccol_tot[ok]

        ccol_sat_1st_infall[ok] *= 1./ccol_tot[ok]
        ccol_sat_orbit[ok] *= 1./ccol_tot[ok]
        ccol_sat_stripped[ok] *= 1./ccol_tot[ok]
        ccol_sat_fb[ok] *= 1./ccol_tot[ok]
        ccol_sat_ism[ok] *= 1./ccol_tot[ok]
        ccol_sat_sat[ok] *= 1./ccol_tot[ok]

        ####### Compute lagrangian status of each escaping rascas photon #########

        # Centre photons on halo
        coll_p.x -= xh; coll_p.y -= yh; coll_p.z -= zh
        rec_p.x -= xh; rec_p.y -= yh; rec_p.z -= zh

        # Link photon final positions to cells
        ind_rec, ok_match_rec = flux_utils.Match_Cell_Particle(csize_h, cx_h, cy_h, cz_h, rec_p.x, rec_p.y, rec_p.z)
        ind_col, ok_match_col = flux_utils.Match_Cell_Particle(csize_h, cx_h, cy_h, cz_h, coll_p.x, coll_p.y, coll_p.z)

        print "Fraction of rec/col rascas photons that can be matched to a tracer", len(ind_rec[crec_tot[ind_rec]>0]) / float(len(ind_rec)), len(ind_col[ccol_tot[ind_col]>0]) / float(len(ind_col))
        
        r_rec = np.sqrt(np.square(rec_p.x) + np.square(rec_p.y) + np.square(rec_p.z))
        r_col = np.sqrt(np.square(coll_p.x) + np.square(coll_p.y) + np.square(coll_p.z))

        i_ts = np.where(np.array(mp_group["halo_ts"])==timestep_tree)

        # The commented out block is a relic of when I was stacking spectra based on 3d radius - this has changed to projected radius in pkpc
        '''# Loop over a set of radius bins and compute the summed escaping spectrum for different lagrangian components
        for i_r in range(n_rad_bins):

            ok = (r_rec > rad_bins[i_r]*rvirh) & (r_rec < rad_bins[i_r+1]*rvirh) & ok_match_rec

            wvl_lo = 1210.; wvl_hi = 1220. # Use a fixed binning scheme so that we onlt need to save the wavelength data once
            
            weights = crec_ism_1st_infall[ind_rec]; weights[ok==False] = 0.0
            wvl, temp = rec_p.spectrum(nbins=n_wvl_bins,weights=weights,lmin=wvl_lo, lmax=wvl_hi)
            spectrum_rec_1st_infall[0,i_ts, i_r] += temp
            weights = crec_ism_orbit[ind_rec]; weights[ok==False] = 0.0
            wvl, temp = rec_p.spectrum(nbins=n_wvl_bins,weights=weights,lmin=wvl_lo,lmax=wvl_hi)
            spectrum_rec_orbit[0,i_ts, i_r] += temp
            weights = crec_ism_stripped[ind_rec]; weights[ok==False] = 0.0
            wvl, temp = rec_p.spectrum(nbins=n_wvl_bins,weights=weights,lmin=wvl_lo,lmax=wvl_hi)
            spectrum_rec_stripped[0,i_ts, i_r] += temp
            weights = crec_ism_fb[ind_rec]; weights[ok==False] = 0.0
            wvl, temp =rec_p.spectrum(nbins=n_wvl_bins,weights=weights,lmin=wvl_lo,lmax=wvl_hi)
            spectrum_rec_fb[0,i_ts, i_r] += temp

            weights = crec_cgm_1st_infall[ind_rec]; weights[ok==False] = 0.0
            wvl, temp = rec_p.spectrum(nbins=n_wvl_bins,weights=weights,lmin=wvl_lo, lmax=wvl_hi)
            spectrum_rec_1st_infall[1,i_ts, i_r] += temp
            weights = crec_cgm_orbit[ind_rec]; weights[ok==False] = 0.0
            wvl, temp = rec_p.spectrum(nbins=n_wvl_bins,weights=weights,lmin=wvl_lo,lmax=wvl_hi)
            spectrum_rec_orbit[1,i_ts, i_r] += temp
            weights = crec_cgm_stripped[ind_rec]; weights[ok==False] = 0.0
            wvl, temp = rec_p.spectrum(nbins=n_wvl_bins,weights=weights,lmin=wvl_lo,lmax=wvl_hi)
            spectrum_rec_stripped[1,i_ts, i_r] += temp
            weights = crec_cgm_fb[ind_rec]; weights[ok==False] = 0.0
            wvl, temp =rec_p.spectrum(nbins=n_wvl_bins,weights=weights,lmin=wvl_lo,lmax=wvl_hi)
            spectrum_rec_fb[1,i_ts, i_r] += temp

            weights = crec_sat_1st_infall[ind_rec]; weights[ok==False] = 0.0
            wvl, temp = rec_p.spectrum(nbins=n_wvl_bins,weights=weights,lmin=wvl_lo, lmax=wvl_hi)
            spectrum_rec_1st_infall[2,i_ts, i_r] += temp
            weights = crec_sat_orbit[ind_rec]; weights[ok==False] = 0.0
            wvl, temp = rec_p.spectrum(nbins=n_wvl_bins,weights=weights,lmin=wvl_lo,lmax=wvl_hi)
            spectrum_rec_orbit[2,i_ts, i_r] += temp
            weights = crec_sat_stripped[ind_rec]; weights[ok==False] = 0.0
            wvl, temp = rec_p.spectrum(nbins=n_wvl_bins,weights=weights,lmin=wvl_lo,lmax=wvl_hi)
            spectrum_rec_stripped[2,i_ts, i_r] += temp
            weights = crec_sat_fb[ind_rec]; weights[ok==False] = 0.0
            wvl, temp =rec_p.spectrum(nbins=n_wvl_bins,weights=weights,lmin=wvl_lo,lmax=wvl_hi)
            spectrum_rec_fb[2,i_ts, i_r] += temp

            ok = (r_col > rad_bins[i_r]*rvirh) & (r_col < rad_bins[i_r+1]*rvirh) & ok_match_col

            weights = ccol_ism_1st_infall[ind_col]; weights[ok==False] = 0.0
            wvl, temp = coll_p.spectrum(nbins=n_wvl_bins,weights=weights,lmin=wvl_lo,lmax=wvl_hi)
            spectrum_col_1st_infall[0,i_ts, i_r] += temp
            weights = ccol_ism_orbit[ind_col]; weights[ok==False] = 0.0
            wvl, temp =coll_p.spectrum(nbins=n_wvl_bins,weights=weights,lmin=wvl_lo,lmax=wvl_hi)
            spectrum_col_orbit[0,i_ts, i_r] += temp
            weights = ccol_ism_stripped[ind_col]; weights[ok==False] = 0.0
            wvl, temp = coll_p.spectrum(nbins=n_wvl_bins,weights=weights,lmin=wvl_lo,lmax=wvl_hi)
            spectrum_col_stripped[0,i_ts, i_r] += temp
            weights = ccol_ism_fb[ind_col]; weights[ok==False] = 0.0
            wvl, temp =coll_p.spectrum(nbins=n_wvl_bins,weights=weights,lmin=wvl_lo,lmax=wvl_hi) 
            spectrum_col_fb[0,i_ts, i_r] += temp

            weights = ccol_cgm_1st_infall[ind_col]; weights[ok==False] = 0.0
            wvl, temp = coll_p.spectrum(nbins=n_wvl_bins,weights=weights,lmin=wvl_lo,lmax=wvl_hi)
            spectrum_col_1st_infall[1,i_ts, i_r] += temp
            weights = ccol_cgm_orbit[ind_col]; weights[ok==False] = 0.0
            wvl, temp =coll_p.spectrum(nbins=n_wvl_bins,weights=weights,lmin=wvl_lo,lmax=wvl_hi)
            spectrum_col_orbit[1,i_ts, i_r] += temp
            weights = ccol_cgm_stripped[ind_col]; weights[ok==False] = 0.0
            wvl, temp = coll_p.spectrum(nbins=n_wvl_bins,weights=weights,lmin=wvl_lo,lmax=wvl_hi)
            spectrum_col_stripped[1,i_ts, i_r] += temp
            weights = ccol_cgm_fb[ind_col]; weights[ok==False] = 0.0
            wvl, temp =coll_p.spectrum(nbins=n_wvl_bins,weights=weights,lmin=wvl_lo,lmax=wvl_hi) 
            spectrum_col_fb[1,i_ts, i_r] += temp

            weights = ccol_sat_1st_infall[ind_col]; weights[ok==False] = 0.0
            wvl, temp = coll_p.spectrum(nbins=n_wvl_bins,weights=weights,lmin=wvl_lo,lmax=wvl_hi)
            spectrum_col_1st_infall[2,i_ts, i_r] += temp
            weights = ccol_sat_orbit[ind_col]; weights[ok==False] = 0.0
            wvl, temp =coll_p.spectrum(nbins=n_wvl_bins,weights=weights,lmin=wvl_lo,lmax=wvl_hi)
            spectrum_col_orbit[2,i_ts, i_r] += temp
            weights = ccol_sat_stripped[ind_col]; weights[ok==False] = 0.0
            wvl, temp = coll_p.spectrum(nbins=n_wvl_bins,weights=weights,lmin=wvl_lo,lmax=wvl_hi)
            spectrum_col_stripped[2,i_ts, i_r] += temp
            weights = ccol_sat_fb[ind_col]; weights[ok==False] = 0.0
            wvl, temp =coll_p.spectrum(nbins=n_wvl_bins,weights=weights,lmin=wvl_lo,lmax=wvl_hi) 
            spectrum_col_fb[2,i_ts, i_r] += temp

        weights = np.ones_like(r_rec)
        wvl, temp = rec_p.spectrum(nbins=n_wvl_bins, weights=weights,lmin=wvl_lo,lmax=wvl_hi)
        spectrum_rec_tot[i_ts] += temp

        
        weights = crec_ism_ism[ind_rec]; weights[ok_match_rec==False] = 0.0            
        wvl, temp = rec_p.spectrum(nbins=n_wvl_bins,weights=weights,lmin=wvl_lo,lmax=wvl_hi)
        spectrum_rec_ism[0,i_ts] += temp

        weights = crec_cgm_ism[ind_rec]; weights[ok_match_rec==False] = 0.0            
        wvl, temp = rec_p.spectrum(nbins=n_wvl_bins,weights=weights,lmin=wvl_lo,lmax=wvl_hi)
        spectrum_rec_ism[1,i_ts] += temp

        weights = crec_sat_ism[ind_rec]; weights[ok_match_rec==False] = 0.0            
        wvl, temp = rec_p.spectrum(nbins=n_wvl_bins,weights=weights,lmin=wvl_lo,lmax=wvl_hi)
        spectrum_rec_ism[2,i_ts] += temp

        
        weights = crec_ism_sat[ind_rec]; weights[ok_match_rec==False] = 0.0            
        wvl, temp =rec_p.spectrum(nbins=n_wvl_bins,weights=weights,lmin=wvl_lo,lmax=wvl_hi)
        spectrum_rec_sat[0,i_ts] += temp 

        weights = crec_cgm_sat[ind_rec]; weights[ok_match_rec==False] = 0.0            
        wvl, temp =rec_p.spectrum(nbins=n_wvl_bins,weights=weights,lmin=wvl_lo,lmax=wvl_hi)
        spectrum_rec_sat[1,i_ts] += temp 

        weights = crec_sat_sat[ind_rec]; weights[ok_match_rec==False] = 0.0            
        wvl, temp =rec_p.spectrum(nbins=n_wvl_bins,weights=weights,lmin=wvl_lo,lmax=wvl_hi)
        spectrum_rec_sat[2,i_ts] += temp 

        
        weights = np.ones_like(r_col)
        wvl, temp = coll_p.spectrum(nbins=n_wvl_bins, weights=weights,lmin=wvl_lo,lmax=wvl_hi)
        spectrum_col_tot[i_ts] += temp

        
        weights = ccol_ism_ism[ind_col]; weights[ok_match_col==False] = 0.0
        wvl, temp =coll_p.spectrum(nbins=n_wvl_bins,weights=weights,lmin=wvl_lo,lmax=wvl_hi)
        spectrum_col_ism[0,i_ts] += temp 

        weights = ccol_cgm_ism[ind_col]; weights[ok_match_col==False] = 0.0
        wvl, temp =coll_p.spectrum(nbins=n_wvl_bins,weights=weights,lmin=wvl_lo,lmax=wvl_hi)
        spectrum_col_ism[1,i_ts] += temp 

        weights = ccol_sat_ism[ind_col]; weights[ok_match_col==False] = 0.0
        wvl, temp =coll_p.spectrum(nbins=n_wvl_bins,weights=weights,lmin=wvl_lo,lmax=wvl_hi)
        spectrum_col_ism[2,i_ts] += temp 

        
        weights = ccol_ism_sat[ind_col]; weights[ok_match_col==False] = 0.0
        wvl, temp = coll_p.spectrum(nbins=n_wvl_bins,weights=weights,lmin=wvl_lo,lmax=wvl_hi)
        spectrum_col_sat[0,i_ts] += temp

        weights = ccol_cgm_sat[ind_col]; weights[ok_match_col==False] = 0.0
        wvl, temp = coll_p.spectrum(nbins=n_wvl_bins,weights=weights,lmin=wvl_lo,lmax=wvl_hi)
        spectrum_col_sat[1,i_ts] += temp

        weights = ccol_sat_sat[ind_col]; weights[ok_match_col==False] = 0.0
        wvl, temp = coll_p.spectrum(nbins=n_wvl_bins,weights=weights,lmin=wvl_lo,lmax=wvl_hi)
        spectrum_col_sat[2,i_ts] += temp'''

        ########### Now compute projected radial profiles ################################

        planck  = 6.626070040e-27  # [erg s] Planck's constant
        rec_L_p = rec_p.nRealPhotons / len(rec_p.x) * planck * rec_p.nu # erg s^-1 (note nRealPhotons is number of photons per second - it is a luminosity)
        col_L_p = coll_p.nRealPhotons / len(coll_p.x) * planck * coll_p.nu # erg s^-1
        
        # Get the radial unit vector
        x_rec = rec_p.x / r_rec
        y_rec = rec_p.y / r_rec
        z_rec = rec_p.z / r_rec
        
        cos_theta_rec = rec_p.kx*x_rec + rec_p.ky*y_rec + rec_p.kz*z_rec
        sin_theta_rec = np.sin(np.arccos(cos_theta_rec))        
        r_rec_proj = sin_theta_rec * r_rec # box units
        
        x_col = coll_p.x / r_col
        y_col = coll_p.y / r_col
        z_col = coll_p.z / r_col
        
        cos_theta_col = coll_p.kx*x_col + coll_p.ky*y_col + coll_p.kz*z_col
        sin_theta_col = np.sin(np.arccos(cos_theta_col))        
        r_col_proj = sin_theta_col * r_col # box units

        #### Compute spectra within projected radius bins #####
        # Loop over a set of radius bins and compute the summed escaping spectrum for different lagrangian components
        for i_r in range(n_rad_bins_proj_phys):

            ok_r = (r_rec_proj*r2kpc > rad_bins_proj_phys[i_r]) & (r_rec_proj*r2kpc < rad_bins_proj_phys[i_r+1])
            ok = ok_r & ok_match_rec

            wvl_lo = 1210.; wvl_hi = 1220. # Use a fixed binning scheme so that we only need to save the wavelength data once

            weights = np.ones_like(r_rec); weights[ok_r==False] = 0.0
            wvl, temp = rec_p.spectrum(nbins=n_wvl_bins,weights=weights,lmin=wvl_lo, lmax=wvl_hi)
            spectrum_rec_tot[i_ts, i_r] += temp
            
            weights = crec_ism_1st_infall[ind_rec]; weights[ok==False] = 0.0
            wvl, temp = rec_p.spectrum(nbins=n_wvl_bins,weights=weights,lmin=wvl_lo, lmax=wvl_hi)
            spectrum_rec_1st_infall[0,i_ts, i_r] += temp
            weights = crec_ism_orbit[ind_rec]; weights[ok==False] = 0.0
            wvl, temp = rec_p.spectrum(nbins=n_wvl_bins,weights=weights,lmin=wvl_lo,lmax=wvl_hi)
            spectrum_rec_orbit[0,i_ts, i_r] += temp
            weights = crec_ism_stripped[ind_rec]; weights[ok==False] = 0.0
            wvl, temp = rec_p.spectrum(nbins=n_wvl_bins,weights=weights,lmin=wvl_lo,lmax=wvl_hi)
            spectrum_rec_stripped[0,i_ts, i_r] += temp
            weights = crec_ism_fb[ind_rec]; weights[ok==False] = 0.0
            wvl, temp =rec_p.spectrum(nbins=n_wvl_bins,weights=weights,lmin=wvl_lo,lmax=wvl_hi)
            spectrum_rec_fb[0,i_ts, i_r] += temp

            weights = crec_cgm_1st_infall[ind_rec]; weights[ok==False] = 0.0
            wvl, temp = rec_p.spectrum(nbins=n_wvl_bins,weights=weights,lmin=wvl_lo, lmax=wvl_hi)
            spectrum_rec_1st_infall[1,i_ts, i_r] += temp
            weights = crec_cgm_orbit[ind_rec]; weights[ok==False] = 0.0
            wvl, temp = rec_p.spectrum(nbins=n_wvl_bins,weights=weights,lmin=wvl_lo,lmax=wvl_hi)
            spectrum_rec_orbit[1,i_ts, i_r] += temp
            weights = crec_cgm_stripped[ind_rec]; weights[ok==False] = 0.0
            wvl, temp = rec_p.spectrum(nbins=n_wvl_bins,weights=weights,lmin=wvl_lo,lmax=wvl_hi)
            spectrum_rec_stripped[1,i_ts, i_r] += temp
            weights = crec_cgm_fb[ind_rec]; weights[ok==False] = 0.0
            wvl, temp =rec_p.spectrum(nbins=n_wvl_bins,weights=weights,lmin=wvl_lo,lmax=wvl_hi)
            spectrum_rec_fb[1,i_ts, i_r] += temp

            weights = crec_sat_1st_infall[ind_rec]; weights[ok==False] = 0.0
            wvl, temp = rec_p.spectrum(nbins=n_wvl_bins,weights=weights,lmin=wvl_lo, lmax=wvl_hi)
            spectrum_rec_1st_infall[2,i_ts, i_r] += temp
            weights = crec_sat_orbit[ind_rec]; weights[ok==False] = 0.0
            wvl, temp = rec_p.spectrum(nbins=n_wvl_bins,weights=weights,lmin=wvl_lo,lmax=wvl_hi)
            spectrum_rec_orbit[2,i_ts, i_r] += temp
            weights = crec_sat_stripped[ind_rec]; weights[ok==False] = 0.0
            wvl, temp = rec_p.spectrum(nbins=n_wvl_bins,weights=weights,lmin=wvl_lo,lmax=wvl_hi)
            spectrum_rec_stripped[2,i_ts, i_r] += temp
            weights = crec_sat_fb[ind_rec]; weights[ok==False] = 0.0
            wvl, temp =rec_p.spectrum(nbins=n_wvl_bins,weights=weights,lmin=wvl_lo,lmax=wvl_hi)
            spectrum_rec_fb[2,i_ts, i_r] += temp

            weights = crec_ism_ism[ind_rec]; weights[ok==False] = 0.0            
            wvl, temp = rec_p.spectrum(nbins=n_wvl_bins,weights=weights,lmin=wvl_lo,lmax=wvl_hi)
            spectrum_rec_ism[0,i_ts, i_r] += temp
            weights = crec_cgm_ism[ind_rec]; weights[ok==False] = 0.0            
            wvl, temp = rec_p.spectrum(nbins=n_wvl_bins,weights=weights,lmin=wvl_lo,lmax=wvl_hi)
            spectrum_rec_ism[1,i_ts, i_r] += temp
            weights = crec_sat_ism[ind_rec]; weights[ok==False] = 0.0            
            wvl, temp = rec_p.spectrum(nbins=n_wvl_bins,weights=weights,lmin=wvl_lo,lmax=wvl_hi)
            spectrum_rec_ism[2,i_ts, i_r] += temp

            weights = crec_ism_sat[ind_rec]; weights[ok==False] = 0.0            
            wvl, temp =rec_p.spectrum(nbins=n_wvl_bins,weights=weights,lmin=wvl_lo,lmax=wvl_hi)
            spectrum_rec_sat[0,i_ts, i_r] += temp 
            weights = crec_cgm_sat[ind_rec]; weights[ok==False] = 0.0            
            wvl, temp =rec_p.spectrum(nbins=n_wvl_bins,weights=weights,lmin=wvl_lo,lmax=wvl_hi)
            spectrum_rec_sat[1,i_ts, i_r] += temp 
            weights = crec_sat_sat[ind_rec]; weights[ok==False] = 0.0            
            wvl, temp =rec_p.spectrum(nbins=n_wvl_bins,weights=weights,lmin=wvl_lo,lmax=wvl_hi)
            spectrum_rec_sat[2,i_ts, i_r] += temp 

            
            ok_r = (r_col_proj > rad_bins_proj_phys[i_r]) & (r_col_proj < rad_bins_proj_phys[i_r+1])
            ok = ok_r & ok_match_col

            weights = np.ones_like(r_col); weights[ok_r==False] = 0.0
            wvl, temp = coll_p.spectrum(nbins=n_wvl_bins, weights=weights,lmin=wvl_lo,lmax=wvl_hi)
            spectrum_col_tot[i_ts, i_r] += temp

            weights = ccol_ism_1st_infall[ind_col]; weights[ok==False] = 0.0
            wvl, temp = coll_p.spectrum(nbins=n_wvl_bins,weights=weights,lmin=wvl_lo,lmax=wvl_hi)
            spectrum_col_1st_infall[0,i_ts, i_r] += temp
            weights = ccol_ism_orbit[ind_col]; weights[ok==False] = 0.0
            wvl, temp =coll_p.spectrum(nbins=n_wvl_bins,weights=weights,lmin=wvl_lo,lmax=wvl_hi)
            spectrum_col_orbit[0,i_ts, i_r] += temp
            weights = ccol_ism_stripped[ind_col]; weights[ok==False] = 0.0
            wvl, temp = coll_p.spectrum(nbins=n_wvl_bins,weights=weights,lmin=wvl_lo,lmax=wvl_hi)
            spectrum_col_stripped[0,i_ts, i_r] += temp
            weights = ccol_ism_fb[ind_col]; weights[ok==False] = 0.0
            wvl, temp =coll_p.spectrum(nbins=n_wvl_bins,weights=weights,lmin=wvl_lo,lmax=wvl_hi) 
            spectrum_col_fb[0,i_ts, i_r] += temp

            weights = ccol_cgm_1st_infall[ind_col]; weights[ok==False] = 0.0
            wvl, temp = coll_p.spectrum(nbins=n_wvl_bins,weights=weights,lmin=wvl_lo,lmax=wvl_hi)
            spectrum_col_1st_infall[1,i_ts, i_r] += temp
            weights = ccol_cgm_orbit[ind_col]; weights[ok==False] = 0.0
            wvl, temp =coll_p.spectrum(nbins=n_wvl_bins,weights=weights,lmin=wvl_lo,lmax=wvl_hi)
            spectrum_col_orbit[1,i_ts, i_r] += temp
            weights = ccol_cgm_stripped[ind_col]; weights[ok==False] = 0.0
            wvl, temp = coll_p.spectrum(nbins=n_wvl_bins,weights=weights,lmin=wvl_lo,lmax=wvl_hi)
            spectrum_col_stripped[1,i_ts, i_r] += temp
            weights = ccol_cgm_fb[ind_col]; weights[ok==False] = 0.0
            wvl, temp =coll_p.spectrum(nbins=n_wvl_bins,weights=weights,lmin=wvl_lo,lmax=wvl_hi) 
            spectrum_col_fb[1,i_ts, i_r] += temp

            weights = ccol_sat_1st_infall[ind_col]; weights[ok==False] = 0.0
            wvl, temp = coll_p.spectrum(nbins=n_wvl_bins,weights=weights,lmin=wvl_lo,lmax=wvl_hi)
            spectrum_col_1st_infall[2,i_ts, i_r] += temp
            weights = ccol_sat_orbit[ind_col]; weights[ok==False] = 0.0
            wvl, temp =coll_p.spectrum(nbins=n_wvl_bins,weights=weights,lmin=wvl_lo,lmax=wvl_hi)
            spectrum_col_orbit[2,i_ts, i_r] += temp
            weights = ccol_sat_stripped[ind_col]; weights[ok==False] = 0.0
            wvl, temp = coll_p.spectrum(nbins=n_wvl_bins,weights=weights,lmin=wvl_lo,lmax=wvl_hi)
            spectrum_col_stripped[2,i_ts, i_r] += temp
            weights = ccol_sat_fb[ind_col]; weights[ok==False] = 0.0
            wvl, temp =coll_p.spectrum(nbins=n_wvl_bins,weights=weights,lmin=wvl_lo,lmax=wvl_hi) 
            spectrum_col_fb[2,i_ts, i_r] += temp

            weights = ccol_ism_ism[ind_col]; weights[ok==False] = 0.0
            wvl, temp =coll_p.spectrum(nbins=n_wvl_bins,weights=weights,lmin=wvl_lo,lmax=wvl_hi)
            spectrum_col_ism[0,i_ts, i_r] += temp 
            weights = ccol_cgm_ism[ind_col]; weights[ok==False] = 0.0
            wvl, temp =coll_p.spectrum(nbins=n_wvl_bins,weights=weights,lmin=wvl_lo,lmax=wvl_hi)
            spectrum_col_ism[1,i_ts, i_r] += temp 
            weights = ccol_sat_ism[ind_col]; weights[ok==False] = 0.0
            wvl, temp =coll_p.spectrum(nbins=n_wvl_bins,weights=weights,lmin=wvl_lo,lmax=wvl_hi)
            spectrum_col_ism[2,i_ts, i_r] += temp 

            weights = ccol_ism_sat[ind_col]; weights[ok==False] = 0.0
            wvl, temp = coll_p.spectrum(nbins=n_wvl_bins,weights=weights,lmin=wvl_lo,lmax=wvl_hi)
            spectrum_col_sat[0,i_ts, i_r] += temp
            weights = ccol_cgm_sat[ind_col]; weights[ok==False] = 0.0
            wvl, temp = coll_p.spectrum(nbins=n_wvl_bins,weights=weights,lmin=wvl_lo,lmax=wvl_hi)
            spectrum_col_sat[1,i_ts, i_r] += temp
            weights = ccol_sat_sat[ind_col]; weights[ok==False] = 0.0
            wvl, temp = coll_p.spectrum(nbins=n_wvl_bins,weights=weights,lmin=wvl_lo,lmax=wvl_hi)
            spectrum_col_sat[2,i_ts, i_r] += temp

        
        # Profiles normalised by Rvir
        for i_r in range(n_rad_bins_proj):
            ok = (r_rec_proj > rad_bins_proj[i_r]*rvirh) & (r_rec_proj < rad_bins_proj[i_r+1]*rvirh) & ok_match_rec

            proj_rec_tot[i_ts, i_r] +=  np.sum(rec_L_p[ok])

            proj_rec_1st_infall[0,i_ts, i_r] +=  np.sum(rec_L_p[ok] * crec_ism_1st_infall[ind_rec][ok])
            proj_rec_orbit[0,i_ts, i_r] += np.sum(rec_L_p[ok] * crec_ism_orbit[ind_rec][ok])
            proj_rec_stripped[0,i_ts, i_r] += np.sum(rec_L_p[ok] * crec_ism_stripped[ind_rec][ok])
            proj_rec_fb[0,i_ts, i_r] += np.sum(rec_L_p[ok] * crec_ism_fb[ind_rec][ok])
            proj_rec_ism[0,i_ts, i_r] += np.sum(rec_L_p[ok] * crec_ism_ism[ind_rec][ok])
            proj_rec_sat[0,i_ts, i_r] +=  np.sum(rec_L_p[ok] * crec_ism_sat[ind_rec][ok])

            proj_rec_1st_infall[1,i_ts, i_r] +=  np.sum(rec_L_p[ok] * crec_cgm_1st_infall[ind_rec][ok])
            proj_rec_orbit[1,i_ts, i_r] += np.sum(rec_L_p[ok] * crec_cgm_orbit[ind_rec][ok])
            proj_rec_stripped[1,i_ts, i_r] += np.sum(rec_L_p[ok] * crec_cgm_stripped[ind_rec][ok])
            proj_rec_fb[1,i_ts, i_r] += np.sum(rec_L_p[ok] * crec_cgm_fb[ind_rec][ok])
            proj_rec_ism[1,i_ts, i_r] += np.sum(rec_L_p[ok] * crec_cgm_ism[ind_rec][ok])
            proj_rec_sat[1,i_ts, i_r] +=  np.sum(rec_L_p[ok] * crec_cgm_sat[ind_rec][ok])

            proj_rec_1st_infall[2,i_ts, i_r] +=  np.sum(rec_L_p[ok] * crec_sat_1st_infall[ind_rec][ok])
            proj_rec_orbit[2,i_ts, i_r] += np.sum(rec_L_p[ok] * crec_sat_orbit[ind_rec][ok])
            proj_rec_stripped[2,i_ts, i_r] += np.sum(rec_L_p[ok] * crec_sat_stripped[ind_rec][ok])
            proj_rec_fb[2,i_ts, i_r] += np.sum(rec_L_p[ok] * crec_sat_fb[ind_rec][ok])
            proj_rec_ism[2,i_ts, i_r] += np.sum(rec_L_p[ok] * crec_sat_ism[ind_rec][ok])
            proj_rec_sat[2,i_ts, i_r] +=  np.sum(rec_L_p[ok] * crec_sat_sat[ind_rec][ok])

            ok = (r_col_proj > rad_bins_proj[i_r]*rvirh) & (r_col_proj < rad_bins_proj[i_r+1]*rvirh) & ok_match_col

            proj_col_tot[i_ts, i_r] +=  np.sum(col_L_p[ok])

            proj_col_1st_infall[0,i_ts, i_r] +=  np.sum(col_L_p[ok] * ccol_ism_1st_infall[ind_col][ok])
            proj_col_orbit[0,i_ts, i_r] += np.sum(col_L_p[ok] * ccol_ism_orbit[ind_col][ok])
            proj_col_stripped[0,i_ts, i_r] += np.sum(col_L_p[ok] * ccol_ism_stripped[ind_col][ok])
            proj_col_fb[0,i_ts, i_r] += np.sum(col_L_p[ok] * ccol_ism_fb[ind_col][ok])
            proj_col_ism[0,i_ts, i_r] += np.sum(col_L_p[ok] * ccol_ism_ism[ind_col][ok])
            proj_col_sat[0,i_ts, i_r] +=  np.sum(col_L_p[ok] * ccol_ism_sat[ind_col][ok])

            proj_col_1st_infall[1,i_ts, i_r] +=  np.sum(col_L_p[ok] * ccol_cgm_1st_infall[ind_col][ok])
            proj_col_orbit[1,i_ts, i_r] += np.sum(col_L_p[ok] * ccol_cgm_orbit[ind_col][ok])
            proj_col_stripped[1,i_ts, i_r] += np.sum(col_L_p[ok] * ccol_cgm_stripped[ind_col][ok])
            proj_col_fb[1,i_ts, i_r] += np.sum(col_L_p[ok] * ccol_cgm_fb[ind_col][ok])
            proj_col_ism[1,i_ts, i_r] += np.sum(col_L_p[ok] * ccol_cgm_ism[ind_col][ok])
            proj_col_sat[1,i_ts, i_r] +=  np.sum(col_L_p[ok] * ccol_cgm_sat[ind_col][ok])

            proj_col_1st_infall[2,i_ts, i_r] +=  np.sum(col_L_p[ok] * ccol_sat_1st_infall[ind_col][ok])
            proj_col_orbit[2,i_ts, i_r] += np.sum(col_L_p[ok] * ccol_sat_orbit[ind_col][ok])
            proj_col_stripped[2,i_ts, i_r] += np.sum(col_L_p[ok] * ccol_sat_stripped[ind_col][ok])
            proj_col_fb[2,i_ts, i_r] += np.sum(col_L_p[ok] * ccol_sat_fb[ind_col][ok])
            proj_col_ism[2,i_ts, i_r] += np.sum(col_L_p[ok] * ccol_sat_ism[ind_col][ok])
            proj_col_sat[2,i_ts, i_r] +=  np.sum(col_L_p[ok] * ccol_sat_sat[ind_col][ok])

        # Non-normalised profiles
        for i_r in range(n_rad_bins_proj_phys):
            ok = (r_rec_proj*r2kpc > rad_bins_proj_phys[i_r]) & (r_rec_proj*r2kpc < rad_bins_proj_phys[i_r+1]) & ok_match_rec

            proj_rec_tot_phys[i_ts, i_r] +=  np.sum(rec_L_p[ok])

            proj_rec_1st_infall_phys[0,i_ts, i_r] +=  np.sum(rec_L_p[ok] * crec_ism_1st_infall[ind_rec][ok])
            proj_rec_orbit_phys[0,i_ts, i_r] += np.sum(rec_L_p[ok] * crec_ism_orbit[ind_rec][ok])
            proj_rec_stripped_phys[0,i_ts, i_r] += np.sum(rec_L_p[ok] * crec_ism_stripped[ind_rec][ok])
            proj_rec_fb_phys[0,i_ts, i_r] += np.sum(rec_L_p[ok] * crec_ism_fb[ind_rec][ok])
            proj_rec_ism_phys[0,i_ts, i_r] += np.sum(rec_L_p[ok] * crec_ism_ism[ind_rec][ok])
            proj_rec_sat_phys[0,i_ts, i_r] +=  np.sum(rec_L_p[ok] * crec_ism_sat[ind_rec][ok])

            proj_rec_1st_infall_phys[1,i_ts, i_r] +=  np.sum(rec_L_p[ok] * crec_cgm_1st_infall[ind_rec][ok])
            proj_rec_orbit_phys[1,i_ts, i_r] += np.sum(rec_L_p[ok] * crec_cgm_orbit[ind_rec][ok])
            proj_rec_stripped_phys[1,i_ts, i_r] += np.sum(rec_L_p[ok] * crec_cgm_stripped[ind_rec][ok])
            proj_rec_fb_phys[1,i_ts, i_r] += np.sum(rec_L_p[ok] * crec_cgm_fb[ind_rec][ok])
            proj_rec_ism_phys[1,i_ts, i_r] += np.sum(rec_L_p[ok] * crec_cgm_ism[ind_rec][ok])
            proj_rec_sat_phys[1,i_ts, i_r] +=  np.sum(rec_L_p[ok] * crec_cgm_sat[ind_rec][ok])

            proj_rec_1st_infall_phys[2,i_ts, i_r] +=  np.sum(rec_L_p[ok] * crec_sat_1st_infall[ind_rec][ok])
            proj_rec_orbit_phys[2,i_ts, i_r] += np.sum(rec_L_p[ok] * crec_sat_orbit[ind_rec][ok])
            proj_rec_stripped_phys[2,i_ts, i_r] += np.sum(rec_L_p[ok] * crec_sat_stripped[ind_rec][ok])
            proj_rec_fb_phys[2,i_ts, i_r] += np.sum(rec_L_p[ok] * crec_sat_fb[ind_rec][ok])
            proj_rec_ism_phys[2,i_ts, i_r] += np.sum(rec_L_p[ok] * crec_sat_ism[ind_rec][ok])
            proj_rec_sat_phys[2,i_ts, i_r] +=  np.sum(rec_L_p[ok] * crec_sat_sat[ind_rec][ok])

            ok = (r_col_proj*r2kpc > rad_bins_proj_phys[i_r]) & (r_col_proj*r2kpc < rad_bins_proj_phys[i_r+1]) & ok_match_col
                        
            proj_col_tot_phys[i_ts, i_r] +=  np.sum(col_L_p[ok])

            proj_col_1st_infall_phys[0,i_ts, i_r] +=  np.sum(col_L_p[ok] * ccol_ism_1st_infall[ind_col][ok])
            proj_col_orbit_phys[0,i_ts, i_r] += np.sum(col_L_p[ok] * ccol_ism_orbit[ind_col][ok])
            proj_col_stripped_phys[0,i_ts, i_r] += np.sum(col_L_p[ok] * ccol_ism_stripped[ind_col][ok])
            proj_col_fb_phys[0,i_ts, i_r] += np.sum(col_L_p[ok] * ccol_ism_fb[ind_col][ok])
            proj_col_ism_phys[0,i_ts, i_r] += np.sum(col_L_p[ok] * ccol_ism_ism[ind_col][ok])
            proj_col_sat_phys[0,i_ts, i_r] +=  np.sum(col_L_p[ok] * ccol_ism_sat[ind_col][ok])

            proj_col_1st_infall_phys[1,i_ts, i_r] +=  np.sum(col_L_p[ok] * ccol_cgm_1st_infall[ind_col][ok])
            proj_col_orbit_phys[1,i_ts, i_r] += np.sum(col_L_p[ok] * ccol_cgm_orbit[ind_col][ok])
            proj_col_stripped_phys[1,i_ts, i_r] += np.sum(col_L_p[ok] * ccol_cgm_stripped[ind_col][ok])
            proj_col_fb_phys[1,i_ts, i_r] += np.sum(col_L_p[ok] * ccol_cgm_fb[ind_col][ok])
            proj_col_ism_phys[1,i_ts, i_r] += np.sum(col_L_p[ok] * ccol_cgm_ism[ind_col][ok])
            proj_col_sat_phys[1,i_ts, i_r] +=  np.sum(col_L_p[ok] * ccol_cgm_sat[ind_col][ok])

            proj_col_1st_infall_phys[2,i_ts, i_r] +=  np.sum(col_L_p[ok] * ccol_sat_1st_infall[ind_col][ok])
            proj_col_orbit_phys[2,i_ts, i_r] += np.sum(col_L_p[ok] * ccol_sat_orbit[ind_col][ok])
            proj_col_stripped_phys[2,i_ts, i_r] += np.sum(col_L_p[ok] * ccol_sat_stripped[ind_col][ok])
            proj_col_fb_phys[2,i_ts, i_r] += np.sum(col_L_p[ok] * ccol_sat_fb[ind_col][ok])
            proj_col_ism_phys[2,i_ts, i_r] += np.sum(col_L_p[ok] * ccol_sat_ism[ind_col][ok])
            proj_col_sat_phys[2,i_ts, i_r] +=  np.sum(col_L_p[ok] * ccol_sat_sat[ind_col][ok])

print "Writing data to disk"
    
# Write data to disk
names = ["1st_infall","orbit","stripped","fb","ism","sat","tot"]
for name in names:
    if "spectrum_col_"+name in mp_group:
        del mp_group["spectrum_col_"+name]
    if "spectrum_rec_"+name in mp_group:
        del mp_group["spectrum_rec_"+name]

    if "proj_col_"+name in mp_group:
        del mp_group["proj_col_"+name]
    if "proj_rec_"+name in mp_group:
        del mp_group["proj_rec_"+name]

    if "proj_col_"+name+"_phys" in mp_group:
        del mp_group["proj_col_"+name+"_phys"]
    if "proj_rec_"+name+"_phys" in mp_group:
        del mp_group["proj_rec_"+name+"_phys"]

if "spectrum_wvl" in mp_group:
    del mp_group["spectrum_wvl"]

mp_group.create_dataset("spectrum_wvl", data = wvl)

mp_group.create_dataset("spectrum_rec_tot", data = spectrum_rec_tot)
mp_group.create_dataset("spectrum_rec_1st_infall", data = spectrum_rec_1st_infall)
mp_group.create_dataset("spectrum_rec_orbit", data = spectrum_rec_orbit)
mp_group.create_dataset("spectrum_rec_stripped", data = spectrum_rec_stripped)
mp_group.create_dataset("spectrum_rec_fb", data = spectrum_rec_fb)
mp_group.create_dataset("spectrum_rec_ism", data = spectrum_rec_ism)
mp_group.create_dataset("spectrum_rec_sat", data = spectrum_rec_sat)

mp_group.create_dataset("spectrum_col_tot", data = spectrum_col_tot)
mp_group.create_dataset("spectrum_col_1st_infall", data = spectrum_col_1st_infall)
mp_group.create_dataset("spectrum_col_orbit", data = spectrum_col_orbit)
mp_group.create_dataset("spectrum_col_stripped", data = spectrum_col_stripped)
mp_group.create_dataset("spectrum_col_fb", data = spectrum_col_fb)
mp_group.create_dataset("spectrum_col_ism", data = spectrum_col_ism)
mp_group.create_dataset("spectrum_col_sat", data = spectrum_col_sat)

mp_group.create_dataset("proj_rec_tot", data = proj_rec_tot)
mp_group.create_dataset("proj_rec_1st_infall", data = proj_rec_1st_infall)
mp_group.create_dataset("proj_rec_orbit", data = proj_rec_orbit)
mp_group.create_dataset("proj_rec_stripped", data = proj_rec_stripped)
mp_group.create_dataset("proj_rec_fb", data = proj_rec_fb)
mp_group.create_dataset("proj_rec_ism", data = proj_rec_ism)
mp_group.create_dataset("proj_rec_sat", data = proj_rec_sat)

mp_group.create_dataset("proj_col_tot", data = proj_col_tot)
mp_group.create_dataset("proj_col_1st_infall", data = proj_col_1st_infall)
mp_group.create_dataset("proj_col_orbit", data = proj_col_orbit)
mp_group.create_dataset("proj_col_stripped", data = proj_col_stripped)
mp_group.create_dataset("proj_col_fb", data = proj_col_fb)
mp_group.create_dataset("proj_col_ism", data = proj_col_ism)
mp_group.create_dataset("proj_col_sat", data = proj_col_sat)

mp_group.create_dataset("proj_rec_tot_phys", data = proj_rec_tot_phys)
mp_group.create_dataset("proj_rec_1st_infall_phys", data = proj_rec_1st_infall_phys)
mp_group.create_dataset("proj_rec_orbit_phys", data = proj_rec_orbit_phys)
mp_group.create_dataset("proj_rec_stripped_phys", data = proj_rec_stripped_phys)
mp_group.create_dataset("proj_rec_fb_phys", data = proj_rec_fb_phys)
mp_group.create_dataset("proj_rec_ism_phys", data = proj_rec_ism_phys)
mp_group.create_dataset("proj_rec_sat_phys", data = proj_rec_sat_phys)

mp_group.create_dataset("proj_col_tot_phys", data = proj_col_tot_phys)
mp_group.create_dataset("proj_col_1st_infall_phys", data = proj_col_1st_infall_phys)
mp_group.create_dataset("proj_col_orbit_phys", data = proj_col_orbit_phys)
mp_group.create_dataset("proj_col_stripped_phys", data = proj_col_stripped_phys)
mp_group.create_dataset("proj_col_fb_phys", data = proj_col_fb_phys)
mp_group.create_dataset("proj_col_ism_phys", data = proj_col_ism_phys)
mp_group.create_dataset("proj_col_sat_phys", data = proj_col_sat_phys)

subvol_File.close()
File.close()

print "Got to the end :)"
