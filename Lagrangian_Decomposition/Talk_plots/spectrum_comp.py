import sys
sys.path.append("../.")
import numpy as np
import h5py
import dm_utils as dm
import utilities_ramses as ur
import utilities_cosmology as uc
import utilities_statistics as us

normed = True

# This script relies on rascas photons being matched to tracers. Photons that are not matched (i.e. are in a cell with zero tracers inside) are missed.
tracer_sample_correct = True # If true, apply a global correction to the normalisation

# Stacking interval
zlo = 3.0
zhi = 4.0

# Split spectrum into inner/outer regions (in projected radius)
rad_bins_proj_phys = np.arange(0.0,42.,2.)
rmid = 0.5*(rad_bins_proj_phys[1:] + rad_bins_proj_phys[0:-1])
boundary = 5 # pkpc - projected seperation
        

Zoom = "Zoom-7-5092"
Run = "RhdRun-tracer_radfix"
timestep_final = 155
OutDir = "/cral2/mitchell/PostProcess/"
RunDir = "/cral2/mitchell/"+ Zoom + "/" + Run

############## Zoom file IO #########################
# Read in relevant tree information from the hdf5 file
filename_zoom = Zoom + "_" + Run + "_tsf" + str(timestep_final) +".hdf5"

File = h5py.File(OutDir+filename_zoom,"r")

# Open the subvolume file (containing all cell data)
subvol_filename = "subvol_"+filename_zoom
subvol_File = h5py.File(OutDir+subvol_filename,"r")

# Check the file with the tree information already exists
if not Zoom in File:
    print "Error: Tree information for ", Zoom, " in ", filename_zoom, " cannot be read"
    exit()

zoom_group = File[Zoom]

######### Setup the output arrays ##########
# Get number of timesteps
for halo_group_str in zoom_group:
    if "_main" in halo_group_str:
        halo_group_str_use= halo_group_str
mp_group = zoom_group[halo_group_str_use+"/main_progenitors"]
timesteps = mp_group["halo_ts"][:] +1 # +1 is for the tree/simulation indexing mismatch
z = np.zeros_like(timesteps)

dm.jeje_utils.read_conversion_scales(RunDir,timestep_final)
omega_0,lambda_0,little_h = dm.jeje_utils.read_cosmo_params(RunDir,timestep_final)

r2kpc_evo = np.zeros(len(z))
for n in range(len(z)):
    r2kpc_ideal, redshift, little_h, m2Msun,junk,junk = ur.Set_Units(RunDir, timesteps[n], "ideal")
    z[n] = redshift
    
# Read spectra
wvl = mp_group["spectrum_wvl"][:] # Ang

# Compute corresponding velocity shift
clight    = 2.99792458e10 # cm s^-1
lambda0   = 1215.6701e-8 # cm
nu0 = clight / lambda0
nu = clight / (wvl*1e-8)
v_bin = lambda0 * (nu0-nu) * 1e-5 # kms^-1

vel_ticks = np.arange(-2000,2000,500)
wvl_ticks = clight / (nu0 - (vel_ticks *1e5) / lambda0)  * 1e8 # Ang
n_ticks = len(vel_ticks)
tick_labels = []
for m in range(n_ticks):
    tick_labels.append(str(vel_ticks[m]))

spectra = {}
names = ["1st_infall","orbit","stripped","fb","ism","sat"]
g_names = ["_ism","_cgm","_sat"]

spectra["spectrum_rec_tot"] = mp_group["spectrum_rec_tot"][:] # erg s^-1 Ang^-1
spectra["spectrum_col_tot"] = mp_group["spectrum_col_tot"][:]

'''for i, z_i in enumerate(z):
    dL, dA = uc.Luminosity_Angular_Distances(z_i,little_h,omega_0,lambda_0) # Mpc
    cm2Mpc = 3.0857*10**24
    conversion = 1/(4*np.pi*(dL*cm2Mpc)**2) # cm^-2

    spectra["spectrum_rec_tot"][i] *= conversion # erg s^-1 cm^-2 Ang^-1
    spectra["spectrum_col_tot"][i] *= conversion # erg s^-1 cm^-2 Ang^-1'''
    
ok = (z>zlo) & (z<zhi)

spectra["spectrum_rec_tot"] = np.sum(spectra["spectrum_rec_tot"][ok],axis=0) / len(z[ok])
spectra["spectrum_col_tot"] = np.sum(spectra["spectrum_col_tot"][ok],axis=0) / len(z[ok])

spectra["spectrum_rec_tot_inner"] = np.sum(spectra["spectrum_rec_tot"][rmid<=boundary],axis=0)
spectra["spectrum_col_tot_inner"] = np.sum(spectra["spectrum_col_tot"][rmid<=boundary],axis=0)
spectra["spectrum_rec_tot_outer"] = np.sum(spectra["spectrum_rec_tot"][rmid>boundary],axis=0)
spectra["spectrum_col_tot_outer"] = np.sum(spectra["spectrum_col_tot"][rmid>boundary],axis=0)   
spectra["spectrum_rec_tot_all"] = np.sum(spectra["spectrum_rec_tot"],axis=0)
spectra["spectrum_col_tot_all"] = np.sum(spectra["spectrum_col_tot"],axis=0)

for name in names:

    for i, g_name in enumerate(g_names):
        if i == 0:
            spectra["spectrum_rec_"+name] = mp_group["spectrum_rec_"+name][:][i]
            spectra["spectrum_col_"+name] = mp_group["spectrum_col_"+name][:][i]
        else:
            spectra["spectrum_rec_"+name] += mp_group["spectrum_rec_"+name][:][i]
            spectra["spectrum_col_"+name] += mp_group["spectrum_col_"+name][:][i]

        '''for i_z, z_i in enumerate(z):
            dL, dA = uc.Luminosity_Angular_Distances(z_i,little_h,omega_0,lambda_0) # Mpc
            cm2Mpc = 3.0857*10**24
            conversion = 1/(4*np.pi*(dL*cm2Mpc)**2) # cm^-2

            spectra["spectrum_rec_"+name][i_z] *= conversion # erg s^-1 cm^-2 Ang^-1
            spectra["spectrum_col_"+name][i_z] *= conversion # erg s^-1 cm^-2 Ang^-1'''
            
    # Temporal stacking
    ok = (z>zlo) & (z<zhi)
    spectra["spectrum_rec_"+name] = np.sum(spectra["spectrum_rec_"+name][ok],axis=0) / len(z[ok])
    spectra["spectrum_col_"+name] = np.sum(spectra["spectrum_col_"+name][ok],axis=0) / len(z[ok])
        
    # Radius stacking
    spectra["spectrum_rec_"+name+"_inner"] = np.sum(spectra["spectrum_rec_"+name][rmid<=boundary],axis=0)
    spectra["spectrum_rec_"+name+"_outer"] = np.sum(spectra["spectrum_rec_"+name][rmid>boundary],axis=0)
    spectra["spectrum_col_"+name+"_inner"] = np.sum(spectra["spectrum_col_"+name][rmid<=boundary],axis=0)
    spectra["spectrum_col_"+name+"_outer"] = np.sum(spectra["spectrum_col_"+name][rmid>boundary],axis=0)
        
    spectra["spectrum_rec_"+name+"_all"] = np.sum(spectra["spectrum_rec_"+name],axis=0)
    spectra["spectrum_col_"+name+"_all"] = np.sum(spectra["spectrum_col_"+name],axis=0)
            
if tracer_sample_correct:
    spectra_rec_matched = np.zeros_like(spectra["spectrum_rec_1st_infall_all"])
    spectra_col_matched = np.zeros_like(spectra["spectrum_rec_1st_infall_all"])
    for name in names:
        if "tot" in name:
            continue
        spectra_rec_matched += spectra["spectrum_rec_"+name+"_all"]
        spectra_col_matched += spectra["spectrum_col_"+name+"_all"]

    correction_rec = np.sum(spectra["spectrum_rec_tot_all"]) / np.sum(spectra_rec_matched)
    correction_col = np.sum(spectra["spectrum_col_tot_all"]) / np.sum(spectra_col_matched)
        
    for name in names:
        if "tot" in name:
            continue
        spectra["spectrum_rec_"+name+"_all"] *= correction_rec
        spectra["spectrum_col_"+name+"_all"] *= correction_col
        spectra["spectrum_rec_"+name+"_inner"] *= correction_rec
        spectra["spectrum_col_"+name+"_inner"] *= correction_col
        spectra["spectrum_rec_"+name+"_outer"] *= correction_rec
        spectra["spectrum_col_"+name+"_outer"] *= correction_col

def Compute_Vsep_FWHM(vel, spectrum):
    v_med = us.Weighted_Percentile(vel,spectrum,perc=0.5) 
    imax = np.argmax(spectrum)
    vpeak = vel[imax]

    hi = vel > vpeak
    vfwhm_hi = vel[hi][np.argmin(abs(spectrum[hi]-0.5*spectrum[imax]))]
    lo = vel < vpeak
    vfwhm_lo = vel[lo][np.argmin(abs(spectrum[lo]-0.5*spectrum[imax]))]
    
    vfwhm = vfwhm_hi - vfwhm_lo
    return v_med, vpeak, vfwhm
    
from utilities_plotting import *

nrow = 1; ncol=1
py.rcParams.update(latexParams)
py.rcParams.update({'figure.subplot.wspace':0.15,'figure.subplot.hspace':0.,
                    'figure.figsize':[3.32*nrow,2.49*ncol*1.2],
                    'figure.subplot.left':0.18,
                    'figure.subplot.bottom':0.16,
                    'figure.subplot.top':0.85,
                    'figure.subplot.right':0.94,
                    'legend.fontsize':6,
                    'font.size':12,
                    'axes.labelsize':11})

py.figure()
subplots = panelplot(nrow,ncol)
for n,ax in enumerate(subplots):
    py.axes(ax)

    label = r"$r_{\mathrm{projected}} < 5 \, \mathrm{kpc}$"

    if normed:
        norm = np.sum(spectra["spectrum_rec_tot_inner"]+spectra["spectrum_col_tot_inner"])
        spectra["spectrum_rec_tot_inner"] *= 1./norm
        spectra["spectrum_col_tot_inner"] *= 1./norm
        
    py.plot(wvl, 1e-41*(spectra["spectrum_rec_tot_inner"]+spectra["spectrum_col_tot_inner"]),c="k",label=label,linewidth=1.5)
    v_med, vpeak, vfwhm_tot = Compute_Vsep_FWHM(v_bin, spectra["spectrum_rec_tot_inner"]+spectra["spectrum_col_tot_inner"])
    print "tot", v_med, vpeak, vfwhm_tot

    if normed:
        norm = np.sum(spectra["spectrum_rec_tot_outer"]+spectra["spectrum_col_tot_outer"])
        spectra["spectrum_rec_tot_outer"] *= 1./norm
        spectra["spectrum_col_tot_outer"] *= 1./norm

    
    label = r"$r_{\mathrm{projected}} > 5 \, \mathrm{kpc}$"
    py.plot(wvl, 1e-41*(spectra["spectrum_rec_tot_outer"]+spectra["spectrum_col_tot_outer"]),c="k",label=label,linewidth=1.5,linestyle=':')

    if normed:
        norm = np.sum(spectra["spectrum_rec_tot_all"]+spectra["spectrum_col_tot_all"])
        spectra["spectrum_rec_tot_all"] *= 1./norm
        spectra["spectrum_col_tot_all"] *= 1./norm

    label = r"$Total$"
    py.plot(wvl, 1e-41*(spectra["spectrum_rec_tot_all"]+spectra["spectrum_col_tot_all"]),c="k",label=label,linewidth=1.5,linestyle='-',alpha=0.2)

    # Show the peak wavelength predicted by the Verhamme 18 correlation between FWHM and v_peak shift
    vpeak_v18 = 0.9 * vfwhm_tot -34
    print "vpeak_v18", vpeak_v18
    wvl_peak_v18 = wvl[np.argmin(abs(vpeak_v18-v_bin))] 
    py.axvline(wvl_peak_v18,c="k",alpha=0.5,linestyle='--')

    #py.plot(wvl, 1e-41*(spectra["spectrum_rec_ism"]+spectra["spectrum_col_ism"]),c="orange",label="ISM")
    #v_med, vpeak, vfwhm = Compute_Vsep_FWHM(v_bin, spectra["spectrum_rec_ism"]+spectra["spectrum_col_ism"])
    #print "ism", v_med, vpeak, vfwhm

    #py.plot(wvl, 1e-41*(spectra["spectrum_rec_1st_infall"]+spectra["spectrum_col_1st_infall"]),c="b",label="First infall")
    #v_med, vpeak, vfwhm = Compute_Vsep_FWHM(v_bin, spectra["spectrum_rec_1st_infall"]+spectra["spectrum_col_1st_infall"])
    #print "1st_infall", v_med, vpeak, vfwhm
    
    #py.plot(wvl, 1e-41*(spectra["spectrum_rec_orbit"]+spectra["spectrum_col_orbit"]),c="g",label="Post-pericenter")
    #v_med, vpeak, vfwhm = Compute_Vsep_FWHM(v_bin, spectra["spectrum_rec_orbit"]+spectra["spectrum_col_orbit"])
    #print "orbit", v_med, vpeak, vfwhm

    #py.plot(wvl, 1e-41*(spectra["spectrum_rec_stripped"]+spectra["spectrum_col_stripped"]),c="c",label="Stripped")
    #v_med, vpeak, vfwhm = Compute_Vsep_FWHM(v_bin, spectra["spectrum_rec_stripped"]+spectra["spectrum_col_stripped"])
    #print "stripped", v_med, vpeak, vfwhm

    #py.plot(wvl, 1e-41*(spectra["spectrum_rec_fb"]+spectra["spectrum_col_fb"]),c="r",label="Feedback influenced")
    #v_med, vpeak, vfwhm = Compute_Vsep_FWHM(v_bin, spectra["spectrum_rec_fb"]+spectra["spectrum_col_fb"])
    #print "fb", v_med, vpeak, vfwhm

    #py.plot(wvl, 1e-41*(spectra["spectrum_rec_sat"]+spectra["spectrum_col_sat"]),c="y",label="Satellites")
    #v_med, vpeak, vfwhm = Compute_Vsep_FWHM(v_bin, spectra["spectrum_rec_sat"]+spectra["spectrum_col_sat"])
    #print "sat", v_med, vpeak, vfwhm

    py.axvline(1215.67,c="k",alpha=0.5)
    py.legend(frameon=False,loc="upper left")

    

    py.xlabel(r"$\lambda \, / \mathrm{Ang}$")
    if normed:
        py.ylabel(r"$L_{\mathrm{\lambda}}$")
    else:
        py.ylabel(r"$L_{\mathrm{\lambda}} \, / \mathrm{10^{41} erg s^{-1} \AA^{-1}}$")
        
    #py.xlim((1211, 1220))

    ax_z = ax.twiny()
    ax_z.set_xticks(wvl_ticks)
    ax_z.set_xticklabels(tick_labels)
    ax_z.set_xlabel(r'$\mathrm{Velocity} \, / \mathrm{km s^{-1}}$')
    ax_z.set_xlim((wvl.min(),wvl.max()))
    
fig_name = "spectrum_rad.pdf"
py.savefig(fig_name)   
py.show()
