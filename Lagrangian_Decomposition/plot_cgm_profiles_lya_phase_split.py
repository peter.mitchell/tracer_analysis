import numpy as np
import h5py
import utilities_cosmology as uc
import dm_utils as dm
import utilities_ramses as ur
import ldecomp_functions as ldf


Zoom = "Zoom-7-5092"
Run = "RhdRun-tracer_radfix"
timestep_final = 155
RunDir = "/cral2/mitchell/"+ Zoom + "/" + Run
OutDir = "/cral2/mitchell/PostProcess/"
RT = True

# Edit: this should now be redundant
# If tracer sample is downsample, need to up-weight the mass/luminosity etc of each tracer
#downsampling = True
#ds_correction_factor = 31400640 / (3.0*10**6) # Appropriate for Rfix 3e6 ds

# Choose radial bins for computing exchange terms
delta_rprime = 0.1
r_bins = np.arange(0.0,1.1,delta_rprime)
r_mid = 0.5*(r_bins[1:]+r_bins[0:-1])

xlo = 0.2; xhi = 1.0

# Choose redshift interval to stack over
zlo_stack = 3.08 # Hack until rascas run at lower z
zhi_stack = 4.0

# Read the measurments
filename = Zoom + "_" + Run + "_tsf" + str(timestep_final) +".hdf5"

output_File = h5py.File(OutDir+filename)
zoom_group = output_File[Zoom]

for halo_group_str in zoom_group:
    # Only compute fluxes for the main halo
    if not "_main" in halo_group_str:
        continue
    mp_group = zoom_group[halo_group_str+"/main_progenitors"]

timesteps = mp_group["halo_ts"][:] +1 # +1 is for the tree/simulation indexing mismatch
z = np.zeros_like(timesteps)

dm.jeje_utils.read_conversion_scales(RunDir,timestep_final)
omega_0,lambda_0,little_h = dm.jeje_utils.read_cosmo_params(RunDir,timestep_final)

r2kpc_evo = np.zeros(len(z))
for n in range(len(z)):
    r2kpc_ideal, redshift, little_h, m2Msun,junk,junk = ur.Set_Units(RunDir, timesteps[n], "ideal")
    z[n] = redshift

    
    r2kpc_evo[n], redshift, little_h, m2Msun,rho2msunpkpc3,v2kms = ur.Set_Units(RunDir, timesteps[n])
    
#np.zeros((n_type, n_ts_out, len(r_bins[1:]) ))
f_1st_infall_evo = []
f_past_peri_evo  = []
f_past_apo_evo = []
f_strip_1st_infall_evo = []
f_strip_past_peri_evo = []
f_strip_past_apo_evo = []
f_fb_1st_outflow_evo = []
f_fb_past_peri_evo = []
f_fb_past_apo_evo = []
f_tot_evo = []
f_sat_evo = []

select_types = ["LyaNeut","LyaWarmI", "LyaHotI"]
select_types += ["LyaEscNeut","LyaEscWarmI", "LyaEscHotI"]
n_type = 6

for n in range(n_type):

    f_tot_evo.append(mp_group["tot_"+select_types[n]+"_evo"][:])
    f_sat_evo.append(mp_group["sat_"+select_types[n]+"_evo"][:])
    
    f_1st_infall_evo.append(mp_group["1st_infall_"+select_types[n]+"_evo"][:])
    f_past_peri_evo.append(mp_group["past_peri_"+select_types[n]+"_evo"][:])
    f_past_apo_evo.append(mp_group["past_apo_"+select_types[n]+"_evo"][:])
    f_strip_1st_infall_evo.append(mp_group["1st_infall_stripped_"+select_types[n]+"_evo"][:])
    f_strip_past_peri_evo.append(mp_group["past_peri_stripped_"+select_types[n]+"_evo"][:])
    f_strip_past_apo_evo.append(mp_group["past_apo_stripped_"+select_types[n]+"_evo"][:])

    # Note called this one inflow in hdf5 by mistake - it is 1st outflow gas
    f_fb_1st_outflow_evo.append(mp_group["1st_infall_fb_"+select_types[n]+"_evo"][:])

    f_fb_past_peri_evo.append(mp_group["past_peri_fb_"+select_types[n]+"_evo"][:])
    f_fb_past_apo_evo.append(mp_group["past_apo_fb_"+select_types[n]+"_evo"][:])

# Stack radial bins together for this script - note this only works with radial bins starting at 0.2

def summation( array, z ):

    # Hack to correct bugged lyman alpha luminosity units
    #print "temp hack"
    #redshift_correction = r2kpc_evo**3

    #for i in range(len(z)):
    #    array[2][i] *= redshift_correction[i]
        
    ok = np.where((z > zlo_stack) & (z<zhi_stack))[0]

    array = np.array(array)
    array = array[:,ok]
    
    ok = np.isnan(array)
    array[ok] = 0.0
    
    array = np.sum(array,axis=1) / len(array[0,:,0])
    array *= 1/delta_rprime

    # Now units are d quant d (r/R_vir), mean stacked over specified redshift interval

    # If we are using a down-sampled tracer sample, correct
    #if downsampling:
    #    array *= ds_correction_factor        
    return array

f_tot_evo = np.array(f_tot_evo)
f_sat_evo = np.array(f_sat_evo)
f_tot_evo -= f_sat_evo # Exclude satellite contribution to this figure for now
ok = np.isnan(f_tot_evo)
f_tot_evo[ok] = 0


f_tot_evo = summation(f_tot_evo,z)
f_1st_infall_evo = summation(f_1st_infall_evo,z)
f_past_peri_evo  = summation(f_past_peri_evo,z)
f_past_apo_evo = summation(f_past_apo_evo,z)
f_strip_1st_infall_evo = summation(f_strip_1st_infall_evo,z)
f_strip_past_peri_evo = summation(f_strip_past_peri_evo,z)
f_strip_past_apo_evo = summation(f_strip_past_apo_evo,z)
f_fb_1st_outflow_evo = summation(f_fb_1st_outflow_evo,z)
f_fb_past_peri_evo = summation(f_fb_past_peri_evo,z)
f_fb_past_apo_evo = summation(f_fb_past_apo_evo,z)

f_strip_evo = f_strip_1st_infall_evo + f_strip_past_peri_evo + f_strip_past_apo_evo
f_fb_evo = f_fb_1st_outflow_evo + f_fb_past_apo_evo + f_fb_past_peri_evo
f_after_infall_evo = f_past_peri_evo + f_past_apo_evo

from utilities_plotting import *

c_list = ["k","b","m","r","g","y","c","firebrick","darkgray","orange"]

nrow = 2; ncol=1
py.rcParams.update(latexParams)
py.rcParams.update({'figure.subplot.wspace':0.15,'figure.subplot.hspace':0.,
                    'figure.figsize':[3.32*ncol,2.49*nrow],
                    'figure.subplot.left':0.19,
                    'figure.subplot.bottom':0.09,
                    'figure.subplot.top':0.98,
                    'figure.subplot.right':0.94,
                    'legend.fontsize':6,
                    'font.size':12,
                    'axes.labelsize':11})

def plot(r_mid, y, c, label=None,linestyle='-',linewidth=1.1):

    if label is None:
        py.plot(r_mid, y,c=c,linestyle=linestyle,linewidth=linewidth)
    else:
        py.plot(r_mid, y,c=c,label=label,linestyle=linestyle,linewidth=linewidth)

    ps = 1
    if linestyle=='-':
        py.scatter(r_mid, y,c=c,edgecolors="none",s=ps)

py.figure()
subplots = panelplot(nrow,ncol)
for n,ax in enumerate(subplots):
    py.axes(ax)

    c_list = ["b","g","m","b","g","m"]

    if n == 0:
        m_list = [0,1,2]
        py.plot(r_mid, np.log10(f_tot_evo[0]+f_tot_evo[1]+f_tot_evo[2]),"k",label="Total")
    if n == 1:
        m_list = [3,4,5]
        py.plot(r_mid, np.log10(f_tot_evo[3]+f_tot_evo[4]+f_tot_evo[5]),"k",label="Total")

        
    for m in m_list:
        plot(r_mid, np.log10(f_tot_evo[m]),c_list[m],label=select_types[m])
        #plot(r_mid, np.log10(f_1st_infall_evo[m]), "b",label= "First infall")
        #plot(r_mid, np.log10(f_after_infall_evo[m]), "g",label= "Post-pericenter")
        #plot(r_mid, np.log10(f_strip_evo[m]), "c", label="Stripped")
        #plot(r_mid, np.log10(f_fb_evo[m]), "r", label="Feedback influenced")

    if n == 0:
        py.legend(frameon=False,loc="lower left")
        py.ylabel(r"$\log(\, \left<\frac{\mathrm{d}L_{\mathrm{intrin}}}{\mathrm{d}(r/R_{\mathrm{vir}})}\right> \, / \mathrm{erg \, s^{-1}})$")

    py.ylim((39.0,41.5))

    if n == 1:
        py.xlabel(r"$r \, / R_{\mathrm{vir}}$")
        py.ylabel(r"$\log(\, \left<\frac{\mathrm{d}L_{\mathrm{esc}}}{\mathrm{d}(r/R_{\mathrm{vir}})}\right> \, / \mathrm{erg \, s^{-1}})$")

    py.xlim((xlo,xhi))
        
fig_name = "cgm_profiles_lya_phase_split_"+Zoom+"_"+Run+".pdf"
py.savefig("../Figures/"+fig_name)
py.show()
