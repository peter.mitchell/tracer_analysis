import matplotlib as mpl

# Set the backend 
#valid strings are [u'pgf', u'cairo', u'MacOSX', u'CocoaAgg', u'gdk', u'ps', u'GTKAgg', u'nbAgg', u'GTK', u'Qt5Agg', u'template', u'emf', u'GTK3Cairo', u'GTK3Agg', u'WX', u'Qt4Agg', u'TkAgg', u'agg', u'svg', u'#GTKCairo', u'WXAgg', u'WebAgg', u'pdf']

mpl.use('tkagg') # This one stopped working as of 25/11/19 (it is working again as of 28/11/19)
#mpl.use('Agg') # This one works (as of 25/11/19) for savefig but not for py.show()
# As far as I can tell, none of the interactive plotting backends work at the moment

import matplotlib.pyplot as py

import numpy as np
#import pylab as py


def panelplot(numRows, numCols):
    """ draw a panel of (numRows, numCols) plots with axes in the right places """
    subplots = []

    # no space between the panels
    py.rcParams.update({'figure.subplot.wspace':0,'figure.subplot.hspace':0})
    
    for i in range(numRows*numCols):
        ax= py.subplot(numRows,numCols,i+1)
        
        # only put left ticks on the left plots, right ticks on the right, lower on the lower
        #n%m is modulo operation. i.e. remainder after how many m go into n, 4%3=2,4%2=0,4%6=4,8%3=2
        left_tick = i%numCols==0  #True if i is a factor of numCols
        right_tick = (i+1)%numCols==0
        
        for tick in ax.yaxis.get_major_ticks():
            tick.label1On=left_tick
            if numCols > 1:
                tick.label2On=right_tick

        lower_tick = i>=(numRows-1)*numCols
        for tick in ax.xaxis.get_major_ticks():
            tick.label1On=lower_tick

        #Uncomment if you want the x-axis labels to appear on top of each panel instead of below.
        #ax.xaxis.set_label_position('top')    
        
        if i==0:
            label =ax.yaxis
            #print dir(label)
            
            for meth in dir(label):
                attr = getattr(label, meth)
                #print meth, attr.__doc__
                
            #print ax.yaxis.set_label_position.__doc__
        
    
        subplots.append(ax)
        
    return subplots

def get_nrows_ncols(npar):
    ncol = max(int(np.sqrt(npar)), 1)
    nrow = ncol
    while npar > (nrow * ncol):
        nrow += 1
    return nrow, ncol

def panelplot_flexible(numPanels):
    '''This is identical to panelplot but where you specify the number of subplots as an input'''
    subplots = []

    numRows, numCols = get_nrows_ncols(numPanels)

    # no space between the panels
    py.rcParams.update({'figure.subplot.wspace':0,'figure.subplot.hspace':0})
    
    for i in range(numPanels):
        ax= py.subplot(numRows,numCols,i+1)
        
        # only put left ticks on the left plots, right ticks on the right, lower on the lower
        #n%m is modulo operation. i.e. remainder after how many m go into n, 4%3=2,4%2=0,4%6=4,8%3=2
        left_tick = i%numCols==0  #True if i is a factor of numCols
        right_tick = (i+1)%numCols==0
        
        for tick in ax.yaxis.get_major_ticks():
            tick.label1On=left_tick
            if numCols > 1:
               tick.label2On=right_tick

        lower_tick = i>= numPanels - numCols 
        for tick in ax.xaxis.get_major_ticks():
            tick.label1On=lower_tick

        #Uncomment if you want the x-axis labels to appear on top of each panel instead of below.
        #ax.xaxis.set_label_position('top')    
        
        if i==0:
            label =ax.yaxis
            #print dir(label)
            
            for meth in dir(label):
                attr = getattr(label, meth)
                #print meth, attr.__doc__
                
            #print ax.yaxis.set_label_position.__doc__
        
    
        subplots.append(ax)
        
    return subplots

# 240 pt figures (3.32 inches)
latexParams = {'figure.dpi':150,
               'figure.figsize':[240.0/72.27,240.0/72.27*3.0/4.0],
               'text.usetex':False,
               'font.size':8,
               'axes.labelsize':8,
               'xtick.labelsize' : 8,
               'ytick.labelsize' : 8,
               'figure.subplot.top':0.95,
               'figure.subplot.right':0.95,
               'figure.subplot.bottom':0.15,
               'figure.subplot.left':0.15,
               'lines.linewidth':0.5,
               'lines.markersize':3,
               'savefig.dpi':300,
               'legend.fontsize':8}

def hist_stepped(data,bins,weights=None,normed=False,log=False,ymin=None,color='k',linewidth=1,linestyle='-',label=None):
    counts,bins = np.histogram(data,bins,weights=weights,normed=normed)
    x_points = np.append(bins,bins*1.00000001)
    y_points = np.append(np.append(0.0,np.append(counts,counts)),0.0)[np.argsort(x_points)]
    x_points = np.sort(x_points)

    if log:
        y_points[y_points==0.0] = ymin * np.ones_like(y_points[y_points==0.0]) * 0.1
        py.semilogy()
    py.plot(x_points,y_points,c=color,linewidth=linewidth,linestyle=linestyle,label=label)

if __name__=='__main__':
    nrow = 5 ; ncol = 3
    subplots = panelplot(nrow,ncol)
    x = np.arange(20)

    for i, ax in enumerate(subplots):
        # set this to be the current axes
        py.axes(ax)
        py.plot(x,x)

        py.ylim(0,19.9)
        py.xlim(0,19.9)

        if i >= ncol*nrow- ncol:
            py.xlabel('lo')
        if i % ncol == 0:
            py.ylabel('hi')

    py.show()
