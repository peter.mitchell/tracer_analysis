import numpy as np
import h5py
import utilities_cosmology as uc
import dm_utils as dm
import utilities_ramses as ur
import ldecomp_functions as ldf
import utilities_statistics as us

Zoom = "Zoom-7-5092"
Run = "RhdRun-tracer_radfix"
timestep_final = 155
RunDir = "/cral2/mitchell/"+ Zoom + "/" + Run
OutDir = "/cral2/mitchell/PostProcess/"
InDir = OutDir
RT = True
downsample = True
n_downsample = "3000000.0"
#n_downsample = "1000"

if not downsample:
    print "I'm pretty sure this script doesn't work correctly if downsampling is not enabled"
    print "Need to think about stuff like NaN of incomplete outputs as we loop through"
    quit()

# Set redshift interval over which we search for z_cross for each particle
z_lo = 3.5
z_hi = 4.0

########## Figure out which input files of tracer trajectories we will need to read in ###########
if downsample:
    n_tr_samples = 1
    filename = "saved_tracers_select_"+Zoom+"_"+Run + "_ds_"+ n_downsample + ".hdf5"
    filenames_list = [filename]

    example_File = h5py.File(InDir+filename,"r")
    
else:
    import os
    files = os.listdir(InDir)
    filename_base = "saved_tracers_select_"+Zoom+"_"+Run

    filenames_list = []
    for file_n in files:
        if filename_base in file_n and "subset" in file_n:
            filenames_list.append(file_n)
            n_tr_samples = len(filenames_list)

            example_File = h5py.File(InDir+file_n,"r")

    
# Count recorded number of tracer trajectory timesteps so we can shape output array
if downsample and n_downsample == "3000000.0":
    n_ts = len(example_File["timestep"][:])
else:
    for group in example_File:
        if "timestep" in group:
            n_ts = len(example_File[group+"/timestep"][:])
            break
example_File.close()



######## Stuff we are going to measure ########################

weight_list = ["pneut", "ptot", "pneut", "ptot"]
weight_ind_list = [0         ,1,      0,       1] # This is only used to output "weight_list" to disk
radius_lo_list = [0.25, 0.95, 0.25, 0.95]
radius_hi_list = [0.3, 1.0, 0.3, 1.0]
cv_lo_list = [0.0, 0.0, -1e9, -1e9]
cv_hi_list = [1e9, 1e9, 0.0, 0.0]

n_type = len(weight_list)

output_list = []
for n in weight_list:
    output_list.append({})

############### Loop over tracer sub-samples (to avoid running out of memory) ###########
# Note we don't need to loop if downsample = True (provided you didn't select too many tracers)

for ifile, filename in enumerate(filenames_list):

    if not downsample:
        print "Reading/Computing for tracer subset", ifile+1, "of", len(filenames_list)

    #tr_var_names = ["radius","family","cv","comp","xhi","temp","nh","part_orb_type_evo","fb_flag_evo", "enter_in_sat", "mass"]
    tr_var_names = ["radius","family","cv","comp","xhi","temp","nh","part_orb_type_evo","fb_flag_evo", "enter_in_sat", "mass"]
    
    File = h5py.File(InDir+filename,"r")
    t = ldf.tracer_sample_class(File,tr_var_names,verbose=True,RT=RT)
    File.close()

    # Get time from redshift
    dm.jeje_utils.read_conversion_scales(RunDir,t.timestep[0])
    omega_0,lambda_0,little_h = dm.jeje_utils.read_cosmo_params(RunDir,t.timestep[0])
    a_in = 1./(1.+t.redshift)
    t.t, t.tlb = uc.t_Universe(a_in,omega_0,little_h)

    t_mid = t.t[int(n_ts*0.5)]
    time_stack = t.t - t_mid # approximately the time of each stacked t bin
        
    n_part = t.n_part()
    
    keep = np.zeros(n_part)!=0

    zrange = (t.redshift > z_lo) & (t.redshift < z_hi)

    iz_lo = np.argmin(abs(z_lo - t.redshift))
    iz_hi = np.argmin(abs(z_hi - t.redshift))
    
    # Select tracers that pass a given selection at a given time
    print "Pre-selecting which tracers that will enter at least one of the stacks"
    for n in range(n_type):
        
        ok = (t.tr_vars["cv"][iz_hi:iz_lo] > cv_lo_list[n]) & (t.tr_vars["cv"][iz_hi:iz_lo] < cv_hi_list[n]) 
        ok = ok & (t.tr_vars["radius"][iz_hi:iz_lo] > radius_lo_list[n]) & (t.tr_vars["radius"][iz_hi:iz_lo] < radius_hi_list[n])
        ok = ok & (t.tr_vars["family"][iz_hi:iz_lo] == 100) & ((t.tr_vars["comp"][iz_hi:iz_lo] == 0)|(t.tr_vars["comp"][iz_hi:iz_lo] == 3))

        keep[np.sum(ok,axis=0)>0] = True

    # Trim the initial sample
    t.select_bool(keep)
    n_part = t.n_part()
    
    # Select tracers that pass a given selection at a given time
    for n in range(n_type):
        print "Processing stack",n+1,"of",n_type
        keep = np.zeros(n_part)!=0
        
        ok = (t.tr_vars["cv"][iz_hi:iz_lo] > cv_lo_list[n]) & (t.tr_vars["cv"][iz_hi:iz_lo] < cv_hi_list[n]) 
        ok = ok & (t.tr_vars["radius"][iz_hi:iz_lo] > radius_lo_list[n]) & (t.tr_vars["radius"][iz_hi:iz_lo] < radius_hi_list[n])
        ok = ok & (t.tr_vars["family"][iz_hi:iz_lo] == 100) & ((t.tr_vars["comp"][iz_hi:iz_lo] == 0)|(t.tr_vars["comp"][iz_hi:iz_lo] == 3))

        keep[np.sum(ok,axis=0)>0] = True

        r_med = 0.5*(radius_hi_list[n] + radius_lo_list[n])

        rad_temp = np.copy(t.tr_vars["radius"][iz_hi:iz_lo])
        rad_temp[ok==False] = 1e9

        i_cross = np.argmin(abs(r_med - rad_temp),axis=0)[keep] + iz_hi

        ind_keep = np.arange(n_part)[keep]

        # Work out weights for averages
        if weight_list[n] == "ptot":
            weights = abs(t.tr_vars["cv"][i_cross,ind_keep]) * t.tr_vars["mass"][i_cross,ind_keep]
        elif weight_list[n] == "pneut":
            weights = abs(t.tr_vars["cv"][i_cross,ind_keep]) * t.tr_vars["mass"][i_cross,ind_keep] * t.tr_vars["xhi"][i_cross,ind_keep]
        else:
            print "weighting scheme", weight_list[n], "is not implemented"
        
        # Variables we would like to compute percentiles for
        names_perc = ["radius","xhi","temp","nh"]
        exc_stars = [False,True,True,True] # Can each quantity be included if the tracer is attached to a star
        for name in names_perc:
            output_list[n][name+"_lo"] = np.zeros(n_ts)
            output_list[n][name+"_med"] = np.zeros(n_ts)
            output_list[n][name+"_hi"] = np.zeros(n_ts)

        output_list[n]["f_first_infall"] = np.zeros(n_ts)
        output_list[n]["f_post_infall"] = np.zeros(n_ts)
        output_list[n]["f_fb_inf"] = np.zeros(n_ts)
        output_list[n]["f_stripped"] = np.zeros(n_ts)
        output_list[n]["f_star"] = np.zeros(n_ts)
        output_list[n]["f_sat"] = np.zeros(n_ts)
        output_list[n]["f_ism"] = np.zeros(n_ts)
        
        for m in range(n_ts):
            ind_m = m + i_cross - int(n_ts*0.5)
            ind_m[ind_m>=n_ts] -= n_ts

            # We want to mask out timesteps that cross the end of the rolled timestep array for each particle
            ok_m_r = (abs(ind_m - i_cross) < n_ts * 0.5)
            ok_m = ok_m_r & (np.isnan(t.tr_vars["radius"][ind_m,ind_keep])==False)

            # Compute percentiles for this timestep
            for name,exc_stars_i in zip(names_perc, exc_stars):

                if exc_stars_i:
                    ok_m_i = ok_m & (t.tr_vars["family"][ind_m,ind_keep]==100)
                else:
                    if name == "radius":
                        ok_m_i = ok_m_r
                    else:
                        ok_m_i = ok_m

                if np.sum(weights[ok_m_i]) < np.sum(weights)*0.8:
                    output_list[n][name+"_lo"][m] += np.nan
                    output_list[n][name+"_med"][m] += np.nan
                    output_list[n][name+"_hi"][m] += np.nan

                    continue
                    
                bin_dummy = np.array([-0.5,0.5])
                var_temp = t.tr_vars[name][ind_m,ind_keep][ok_m_i]
                dummy = np.zeros(len(var_temp))
                weights_m = weights[ok_m_i]

                if name == "radius":
                    igm = np.isnan(var_temp)
                    var_temp[igm] = 1e9
                    
                med, lo, hi, mid = us.Calculate_Percentiles(bin_dummy,dummy,var_temp,weights=weights_m,min_count = 1,lohi=(0.16,0.84))
                
                output_list[n][name+"_lo"][m] += lo / len(filenames_list)
                output_list[n][name+"_hi"][m] += hi / len(filenames_list)
                output_list[n][name+"_med"][m] += med / len(filenames_list)

            if np.sum(weights[ok_m_r]) < np.sum(weights)*0.8:
                output_list[n]["f_first_infall"][m] += np.nan
                output_list[n]["f_post_infall"][m] += np.nan
                output_list[n]["f_fb_inf"][m] += np.nan
                output_list[n]["f_stripped"][m] += np.nan
                output_list[n]["f_star"][m] += np.nan
                output_list[n]["f_sat"][m] += np.nan
                output_list[n]["f_ism"][m] += np.nan

            #part = 4
            #if n == 0:
            #    print m, t.tr_vars["radius"][ind_m,ind_keep][part], t.tr_vars["enter_in_sat"][ind_keep][part], t.tr_vars["comp"][ind_m, ind_keep][part]
                
            orb = t.tr_vars["part_orb_type_evo"][ind_m,ind_keep][ok_m_r]
            fb = t.tr_vars["fb_flag_evo"][ind_m,ind_keep][ok_m_r]
            strip = t.tr_vars["enter_in_sat"][ind_keep][ok_m_r]
            fam = t.tr_vars["family"][ind_m,ind_keep][ok_m_r]
            comp = t.tr_vars["comp"][ind_m,ind_keep][ok_m_r]
            radius = t.tr_vars["radius"][ind_m,ind_keep][ok_m_r]
            
            igm = np.isnan(radius)
            radius[igm] = 1e9
            comp[igm] = 0 
            fam[igm] = 100 # Assume anything outside 2rvir is gas

            # Relabel tracers that are in neighbouring central haloes as satellites
            comp[(comp==3)&(radius>1.0)] = 2
            
            first_infall = (orb==0) & (fb == 0) & (strip == 0) & (fam == 100) & ((comp == 0)|(comp==3)) & (radius>0.2)
            post_infall = (orb>0) & (fb == 0) & (strip == 0) & (fam == 100) & ((comp == 0)|(comp==3)) & (radius>0.2)
            fb_inf = (fb>0) & (fam == 100) & ((comp == 0)|(comp==3)) & (radius>0.2)
            stripped = (fb == 0) & (strip == 1) & (fam == 100) & ((comp == 0)|(comp==3)) & (radius>0.2)
            star = (fam > 100)
            sat = ((comp > 0)&(comp < 3)) & (fam == 100) & (radius>0.2)
            ism = (fam==100) & (radius<=0.2)
            
            weights_m = weights[ok_m_r]
            norm = np.sum(weights_m)
            
            output_list[n]["f_first_infall"][m] += np.sum(weights_m[first_infall])/norm
            output_list[n]["f_post_infall"][m] += np.sum(weights_m[post_infall])/norm
            output_list[n]["f_fb_inf"][m] += np.sum(weights_m[fb_inf])/norm
            output_list[n]["f_stripped"][m] += np.sum(weights_m[stripped])/norm
            output_list[n]["f_star"][m] += np.sum(weights_m[star])/norm
            output_list[n]["f_sat"][m] += np.sum(weights_m[sat])/norm
            output_list[n]["f_ism"][m] += np.sum(weights_m[ism])/norm
            
path = "Fate_Distns/"
output_file = path + "fate_distn_tstack_"+Zoom+"_"+Run
if downsample:
    output_file += "_"+n_downsample
output_file += ".hdf5"

import os
if os.path.isfile(output_file):
    os.remove(output_file)
    print "deleted previous hdf5 file at ", output_file

File = h5py.File(output_file)

n_type = len(weight_list)

File.create_dataset("radius_lo_select", data=radius_lo_list)
File.create_dataset("radius_hi_select", data=radius_hi_list)
File.create_dataset("cv_lo_select", data=cv_lo_list)
File.create_dataset("cv_hi_select", data=cv_hi_list)
File.create_dataset("weight_ind_select", data=weight_ind_list)

for n in range(n_type):
    stack = File.create_group("Stack_"+str(n))

    for name in names_perc:
        stack.create_dataset(name+"_lo", data=output_list[n][name+"_lo"])
        stack.create_dataset(name+"_med", data=output_list[n][name+"_med"])
        stack.create_dataset(name+"_hi", data=output_list[n][name+"_hi"])

    names_f = ["f_first_infall", "f_post_infall", "f_fb_inf", "f_stripped", "f_star", "f_sat", "f_ism"]
    for name in names_f:
        stack.create_dataset(name, data=output_list[n][name])

File.create_dataset("time",data=time_stack)

File.close()
print "Got to the end :)"

quit()



from utilities_plotting import *

i_type = 0
r_lo = output_list[i_type]["radius_lo"]
r_med = output_list[i_type]["radius_med"]
r_hi = output_list[i_type]["radius_hi"]

xhi_lo = output_list[i_type]["xhi_lo"]
xhi_med = output_list[i_type]["xhi_med"]
xhi_hi = output_list[i_type]["xhi_hi"]

temp_lo = output_list[i_type]["temp_lo"]
temp_med = output_list[i_type]["temp_med"]
temp_hi = output_list[i_type]["temp_hi"]

nh_lo = output_list[i_type]["nh_lo"]
nh_med = output_list[i_type]["nh_med"]
nh_hi = output_list[i_type]["nh_hi"]

py.subplot(221)
py.plot(time_stack, r_lo)
py.plot(time_stack, r_med)
py.plot(time_stack, r_hi)
py.ylim((0.0, 1.0))
py.axvline(0.0)

py.subplot(222)
py.plot(time_stack , xhi_lo)
py.plot(time_stack , xhi_med)
py.plot(time_stack , xhi_hi)
py.ylim((0.0, 1.0))
py.axvline(0.0)

py.subplot(223)
py.plot(time_stack, np.log10(temp_lo))
py.plot(time_stack, np.log10(temp_med))
py.plot(time_stack, np.log10(temp_hi))
py.ylim((3.0, 6.0))
py.axvline(0.0)

py.subplot(224)
py.plot(time_stack, np.log10(nh_lo))
py.plot(time_stack, np.log10(nh_med))
py.plot(time_stack, np.log10(nh_hi))
py.ylim((-3.0, 3.0))
py.axvline(0.0)

py.show()
