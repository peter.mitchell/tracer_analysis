import numpy as np
import h5py
import utilities_cosmology as uc
import dm_utils as dm
import utilities_ramses as ur
import ldecomp_functions as ldf
import utilities_statistics as us

Zoom = "Zoom-7-5092"
Run = "RhdRun-tracer_radfix"
timestep_final = 107
RunDir = "/cral2/mitchell/"+ Zoom + "/" + Run
OutDir = "/cral2/mitchell/PostProcess/"
InDir = OutDir
RT = True
downsample = False
#n_downsample = "3e6"
#n_downsample = "1000"

z_choose = 4.75

########## Figure out which input files of tracer trajectories we will need to read in ###########
if downsample:
    n_tr_samples = 1
    filename = "saved_tracers_select_"+Zoom+"_"+Run + "_ds_"+ n_downsample + ".hdf5"
    filenames_list = [filename]

    example_File = h5py.File(InDir+filename)
    
else:
    import os
    files = os.listdir(InDir)
    filename_base = "saved_tracers_select_"+Zoom+"_"+Run

    filenames_list = []
    for file_n in files:
        if filename_base in file_n and "subset" in file_n:
            filenames_list.append(file_n)
            n_tr_samples = len(filenames_list)

            example_File = h5py.File(InDir+file_n)

    
# Count recorded number of tracer trajectory timesteps so we can shape output array
n_ts = len(example_File["timestep"][:])
example_File.close()



######## Stuff we are going to measure ########################

weight_list = ["mneut", "mneut", "mneut"]
radius_lo_list = [0.375, 0.775, 0.375]
radius_hi_list = [0.425, 0.825, 0.425]
cv_lo_list = [0.0, -1e9, -1e9]
cv_hi_list = [1e9, 0.0, 0.0]

n_type = len(weight_list)

radius_fate_lo = np.zeros(( n_type, n_ts ))
radius_fate_med = np.zeros(( n_type, n_ts ))
radius_fate_hi = np.zeros(( n_type, n_ts ))

xhi_fate_lo = np.zeros(( n_type, n_ts ))
xhi_fate_med = np.zeros(( n_type, n_ts ))
xhi_fate_hi = np.zeros(( n_type, n_ts ))

nh_fate_lo = np.zeros(( n_type, n_ts ))
nh_fate_med = np.zeros(( n_type, n_ts ))
nh_fate_hi = np.zeros(( n_type, n_ts ))

temp_fate_lo = np.zeros(( n_type, n_ts ))
temp_fate_med = np.zeros(( n_type, n_ts ))
temp_fate_hi = np.zeros(( n_type, n_ts ))

############### Loop over tracer sub-samples (to avoid running out of memory) ###########
# Note we don't need to loop if downsample = True (provided you didn't select too many tracers)

for ifile, filename in enumerate(filenames_list):

    if not downsample:
        print "Reading/Computing for tracer subset", ifile+1, "of", len(filenames_list)

    File = h5py.File(InDir+filename)

    print "Doing particle IO"

    print "tid"
    tid = File["tid"][:]
    print "r"
    radius = File["r"][:]
    print "mass"
    mass = File["mass"][:]
    print "comp"
    comp = File["comp"][:]
    print "family"
    family = File["family"][:]
    print "xhi"
    xhi = File["xhi"][:]
    print "cv"
    cv = File["cv"][:]
    print "temp"
    temp = File["temp"][:]
    print "nh"
    nh = File["nh"][:]

    print "Done"
    
    redshifts = File["redshift"][:]
    timesteps = File["timestep"][:]
    
    File.close()
    
    dm.jeje_utils.read_conversion_scales(RunDir,timesteps)
    omega_0,lambda_0,little_h = dm.jeje_utils.read_cosmo_params(RunDir,timesteps[0])
    aexp = 1./(1+redshifts)
    t, tlb = uc.t_Universe(aexp, omega_0, little_h)

    iz_sel = np.argmin(abs(z_choose - redshifts))

    n_ts = len(timesteps)
    
    # Select tracers that pass a given selection at a given time
    for n in range(n_type):
        print "Looping over particle selections", n+1, "of", n_type
        ok = (cv[iz_sel] > cv_lo_list[n]) & (cv[iz_sel] < cv_hi_list[n])
        ok = ok & (radius[iz_sel] > radius_lo_list[n]) & (radius[iz_sel] < radius_hi_list[n])
        ok = ok & (family[iz_sel] == 100) & ((comp[iz_sel] == 0)|(comp[iz_sel] == 3))

        # Weight particles in the percentiles according to their neutral fraction at the output of selection
        if weight_list[n] == "mneut":
            weight = xhi[iz_sel] * mass[iz_sel]
        else:
            print "nope", weight_list[n]
            quit()
        
        for m in range(n_ts):
            
            print "Looping over timesteps, computing statistics", m+1, "of", n_ts
            # For radius, we are happy to include particles that are in stars/satellites
            okr = np.where(ok)[0]
            
            n_part = len(radius[0][okr])
            dummy = np.zeros(n_part)
            bin_dummy = np.array([-0.5, 0.5])
            
            radius_temp = np.copy(radius[m,okr])
            radius_temp[np.isnan(radius_temp)] = 1e9
            
            med, lo, hi, mid = us.Calculate_Percentiles(bin_dummy,dummy,radius_temp,weights=weight[okr],min_count = 1,lohi=(0.16,0.84))

            # Hacky way to get percentiles across all tracer samples
            # Could do this properly later by finely binning the data into say 1000 bins (of e.g. radius at a given timestep), then taking the median outside of the sub-sample loop at the end
            radius_fate_lo[n,m] += lo[0] / len(filenames_list)
            radius_fate_med[n,m] += med[0] / len(filenames_list)
            radius_fate_hi[n,m] += hi[0] / len(filenames_list)

            # For other properties, we want to exclude particles that are outside the halo, and those within stars from the averaging (but keep those in satellites)
            okq = ok & (family[m] == 100) & (np.isnan(radius[m])==False) 

            # Don't compute an average for these properties if a significant fraction are outside the halo
            if len(okq[ ok & (np.isnan(radius[m])==False) ]) / float(len(okq[ ok])) < 0.8:
                temp_fate_lo[n,m] += np.nan; temp_fate_med[n,m] += np.nan; temp_fate_hi[n,m] += np.nan
                xhi_fate_lo[n,m] += np.nan; xhi_fate_med[n,m] += np.nan; xhi_fate_hi[n,m] += np.nan
                nh_fate_lo[n,m] += np.nan; nh_fate_med[n,m] += np.nan; nh_fate_hi[n,m] += np.nan
                continue

            okq = np.where(okq)[0]
            
            n_part = len(radius[0][okq])
            dummy = np.zeros(n_part)
            
            med, lo, hi, mid = us.Calculate_Percentiles(bin_dummy,dummy,temp[m,okq],weights=weight[okq],min_count = 1,lohi=(0.16,0.84))
            temp_fate_lo[n,m] += lo[0] / len(filenames_list)
            temp_fate_med[n,m] += med[0] / len(filenames_list)
            temp_fate_hi[n,m] += hi[0] / len(filenames_list)

            med, lo, hi, mid = us.Calculate_Percentiles(bin_dummy,dummy,nh[m,okq],weights=weight[okq],min_count = 1,lohi=(0.16,0.84))
            nh_fate_lo[n,m] += lo[0] / len(filenames_list)
            nh_fate_med[n,m] += med[0] / len(filenames_list)
            nh_fate_hi[n,m] += hi[0] / len(filenames_list)

            med, lo, hi, mid = us.Calculate_Percentiles(bin_dummy,dummy,xhi[m,okq],weights=weight[okq],min_count = 1,lohi=(0.16,0.84))
            xhi_fate_lo[n,m] += lo[0] / len(filenames_list)
            xhi_fate_med[n,m] += med[0] / len(filenames_list)
            xhi_fate_hi[n,m] += hi[0] / len(filenames_list)


path = "Fate_Distns/"
output_file = path + "fate_distn_"+Zoom+"_"+Run
if downsample:
    output_file += "_"+n_downsample
output_file += ".hdf5"

import os
if os.path.isfile(output_file):
    os.remove(output_file)
    print "deleted previous hdf5 file at ", output_file

File = h5py.File(output_file)

File.create_dataset("radius_lo", data=radius_fate_lo)
File.create_dataset("radius_med", data=radius_fate_med)
File.create_dataset("radius_hi", data=radius_fate_hi)

File.create_dataset("xhi_lo", data=xhi_fate_lo)
File.create_dataset("xhi_med", data=xhi_fate_med)
File.create_dataset("xhi_hi", data=xhi_fate_hi)

File.create_dataset("temp_lo", data=temp_fate_lo)
File.create_dataset("temp_med", data=temp_fate_med)
File.create_dataset("temp_hi", data=temp_fate_hi)

File.create_dataset("nh_lo", data=nh_fate_lo)
File.create_dataset("nh_med", data=nh_fate_med)
File.create_dataset("nh_hi", data=nh_fate_hi)

File.create_dataset("redshifts",data=redshifts)
File.create_dataset("time",data=t)
File.create_dataset("timesteps",data=timesteps)

File.close()
print "Got to the end :)"

quit()



from utilities_plotting import *
py.subplot(221)
py.plot(t, radius_fate_lo[0])
py.plot(t, radius_fate_med[0])
py.plot(t, radius_fate_hi[0])
py.ylim((0.0, 1.0))
py.axvline(t[iz_sel])

py.subplot(222)
py.plot(t, xhi_fate_lo[0])
py.plot(t, xhi_fate_med[0])
py.plot(t, xhi_fate_hi[0])
py.ylim((0.0, 1.0))
py.axvline(t[iz_sel])

py.subplot(223)
py.plot(t, np.log10(temp_fate_lo[0]))
py.plot(t, np.log10(temp_fate_med[0]))
py.plot(t, np.log10(temp_fate_hi[0]))
py.ylim((3.0, 6.0))
py.axvline(t[iz_sel])

py.subplot(224)
py.plot(t, np.log10(nh_fate_lo[0]))
py.plot(t, np.log10(nh_fate_med[0]))
py.plot(t, np.log10(nh_fate_hi[0]))
py.ylim((-3.0, 3.0))
py.axvline(t[iz_sel])

py.show()
