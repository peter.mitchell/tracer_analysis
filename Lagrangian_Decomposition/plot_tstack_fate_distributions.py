import numpy as np
import h5py
import utilities_cosmology as uc
import dm_utils as dm
import utilities_ramses as ur
import ldecomp_functions as ldf
import utilities_statistics as us
from utilities_plotting import *

Zoom = "Zoom-7-5092"
Run = "RhdRun-tracer_radfix"
downsample = True
#n_downsample = "1000"
n_downsample = "3000000.0"

path = "Fate_Distns/"
output_file = path + "fate_distn_tstack_"+Zoom+"_"+Run
if downsample:
    output_file += "_"+n_downsample
output_file += ".hdf5"

File = h5py.File(output_file)

weight_list = ["pneut", "ptot", "pneut", "ptot"]
weight_ind_list = [0         ,1,      0,       1]
radius_lo_list = [0.25, 0.95, 0.25, 0.95]
radius_hi_list = [0.3, 1.0, 0.3, 1.0]
cv_lo_list = [0.0, 0.0, -1e9, -1e9]
cv_hi_list = [1e9, 1e9, 0.0, 0.0]
label_list = [r"Neutral outflow, $r=0.3 R_{\mathrm{vir}}$",r"Outflow, $r= R_{\mathrm{vir}}$",r"Neutral inflow, $r=0.3 R_{\mathrm{vir}}$",r"Inflow, $r= R_{\mathrm{vir}}$"]

n_type = len(weight_list)

names_perc = ["radius","xhi","temp","nh"]
names_f = ["f_first_infall", "f_post_infall", "f_fb_inf", "f_stripped", "f_star", "f_sat","f_ism"]

inputs = []
for n in range(n_type):
    inputs.append({})
    for name in names_perc:
        inputs[n][name+"_lo"] = File["Stack_"+str(n)+"/"+name+"_lo"][:]
        inputs[n][name+"_med"] = File["Stack_"+str(n)+"/"+name+"_med"][:]
        inputs[n][name+"_hi"] = File["Stack_"+str(n)+"/"+name+"_hi"][:]
    for name in names_f:
        inputs[n][name] = File["Stack_"+str(n)+"/"+name][:]
        
t = File["time"][:] # Gyr

File.close()

#xlo = t[np.where(radius_lo[0]<=1)[0][0]]
#xhi = t.max()


nrow = 5; ncol = 4
py.rcParams.update(latexParams)
py.rcParams.update({'figure.subplot.wspace':0.03,'figure.subplot.hspace':0,
                    'figure.figsize':[3.32*2,2.49*2*1.2],
                    'figure.subplot.left':0.08,
                    'figure.subplot.bottom':0.08,
                    'figure.subplot.top':0.96,
                    'figure.subplot.right':0.96,
                    'legend.fontsize':6,
                    'font.size':12,
                    'axes.labelsize':11})

py.figure()
subplots = panelplot(nrow,ncol)

col_order = [2, 0, 3, 1]

i_s = -1
for n,ax in enumerate(subplots):
    py.axes(ax)

    i_s += 1
    if i_s > 3:
        i_s = 0

    i_c = col_order[i_s]
        
    py.axvline(0.0,c="k")

    
    if n < 4:
        py.plot(t, inputs[i_c]["radius_med"],c="k")
        py.plot(t, inputs[i_c]["radius_lo"],c="k")
        py.plot(t, inputs[i_c]["radius_hi"],c="k")
        fix = inputs[i_c]["radius_hi"] > 10
        inputs[i_c]["radius_hi"][fix] = 10
        fix = inputs[i_c]["radius_lo"] > 10
        inputs[i_c]["radius_lo"][fix] = 10
        py.fill_between(t, inputs[i_c]["radius_lo"], inputs[i_c]["radius_hi"],color="r",alpha=0.2)
        py.ylim((0.0, 2.0))
        if n == 0:
            py.ylabel(r"$r \, / R_{\mathrm{vir}}$")
        py.title(label_list[i_c],fontsize=8)
            
    elif n < 8:
        log = False
        if log:
            py.plot(t, np.log10(inputs[i_c]["xhi_med"]),c="k")
            py.plot(t, np.log10(inputs[i_c]["xhi_lo"]),c="k")
            py.plot(t, np.log10(inputs[i_c]["xhi_hi"]),c="k")
            py.fill_between(t, np.log10(inputs[i_c]["xhi_lo"]), np.log10(inputs[i_c]["xhi_hi"]),color="r",alpha=0.2)
            py.ylim((-1.5, 0.1))
            if n == 4:
                py.ylabel(r"$\log(x_{\mathrm{HI}})$")
        else:
            py.plot(t, inputs[i_c]["xhi_med"],c="k")
            py.plot(t, inputs[i_c]["xhi_lo"],c="k")
            py.plot(t, inputs[i_c]["xhi_hi"],c="k")
            py.fill_between(t, inputs[i_c]["xhi_lo"], inputs[i_c]["xhi_hi"],color="r",alpha=0.2)
            py.ylim((-0.05, 1.09))
            if n == 4:
                py.ylabel(r"$x_{\mathrm{HI}}$")
        
    elif n < 12:
        py.plot(t, np.log10(inputs[i_c]["temp_med"]),c="k")
        py.plot(t, np.log10(inputs[i_c]["temp_lo"]),c="k")
        py.plot(t, np.log10(inputs[i_c]["temp_hi"]),c="k")
        py.fill_between(t, np.log10(inputs[i_c]["temp_lo"]), np.log10(inputs[i_c]["temp_hi"]),color="r",alpha=0.2)
        py.ylim((2.0, 6.4))
        if n == 8:
            py.ylabel(r"$\log(T \, / \mathrm{K})$")
    
    elif n < 16:
        py.plot(t, np.log10(inputs[i_c]["nh_med"]),c="k")
        py.plot(t, np.log10(inputs[i_c]["nh_lo"]),c="k")
        py.plot(t, np.log10(inputs[i_c]["nh_hi"]),c="k")
        py.fill_between(t, np.log10(inputs[i_c]["nh_lo"]), np.log10(inputs[i_c]["nh_hi"]),color="r",alpha=0.2)
        py.ylim((-5.0, 2.9))
        if n == 12:
            py.ylabel(r"$\log(n_{\mathrm{H}} \, / \mathrm{cm^{-3}})$")



    else:
        # Apply some corrections
        #col_order = [2, 0, 3, 1]
        if i_c == 2:
            ok = (t > 0) | (np.isnan(inputs[i_c]["nh_med"])==False)
        elif i_c == 3 or i_c == 1:
            ok = np.isnan(inputs[i_c]["nh_med"])==False
        else:
            ok = t > -999
            
        py.plot(t[ok], inputs[i_c]["f_first_infall"][ok],c="b",linewidth=1)
        py.plot(t[ok], inputs[i_c]["f_post_infall"][ok],c="g",linewidth=1)
        py.plot(t[ok], inputs[i_c]["f_fb_inf"][ok],c="r",linewidth=1)
        py.plot(t[ok], inputs[i_c]["f_stripped"][ok],c="c",linewidth=1)
        py.plot(t[ok], inputs[i_c]["f_star"][ok],c="k",linewidth=1)
        py.plot(t[ok], inputs[i_c]["f_sat"][ok],c="y",linewidth=1)
        py.plot(t[ok], inputs[i_c]["f_ism"][ok],c="orange",linewidth=1)
        
        py.ylim((0.0,0.95))
        if n == 16:
            py.ylabel(r"$f_{\mathrm{mass}}$")

        if i_s == 2:
            py.plot(t-99, t-99, c="orange",label="ISM",linewidth=1)
            py.plot(t-99, t-99, c="k",label="Star",linewidth=1)
            py.plot(t-99, t-99, c="b",label="First infall",linewidth=1)
            py.plot(t-99, t-99, c="g",label="Post-pericenter",linewidth=1)
            py.plot(t-99, t-99, c="r",label="Feedback influenced",linewidth=1)
            py.plot(t-99, t-99, c="y",label="Satellite",linewidth=1) 
            py.plot(t-99, t-99, c="c",label="Stripped",linewidth=1)
            py.legend(frameon=False,loc="center left")
            
    if n >= ncol*nrow- ncol:
        py.xlabel(r"$t-t_{\mathrm{cross}} \, / \mathrm{Gyr}$")
    py.xticks([-0.3,-0.15,0.0,0.15,0.3], (r'$-0.3$', r'$-0.15$', r'$0.0$', r'$0.15$', r'$0.3$'))

    py.xlim((-350*1e-3,350*1e-3))

fig_name = "fate_distribution_tstacks_"+Zoom+"_"+Run+".pdf"
py.savefig("../Figures/"+fig_name)
py.show()
