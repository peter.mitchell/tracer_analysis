import numpy as np

class tracer_sample_class:
    def __init__(self,File,tr_var_names,verbose=False,RT=True,tr_format="new"):
        self.tr_var_names = tr_var_names
        self.tr_vars = {}

        if tr_format == "old":        
            for var_name in self.tr_var_names:

                if verbose:
                    print "reading array", var_name

                if var_name == "radius":
                    array = File["r"][:]
                elif var_name == "radius_p":
                    array = File["r_physical"][:]
                elif "LyaLum" in var_name and RT==False:
                    array = File["mass"][:]# Placeholder
                #elif var_name in [ "fb_flag_evo", "end_in_sat_evo", "enter_in_sat", "first_infall_ts"]:
                #    array = File[var_name][:].astype("int")
                else:
                    array = File[var_name][:]

                if len(array.shape) == 1 or len(array.shape) == 2:
                    self.tr_vars[var_name] = array
                else:
                    print "nope, wrong shape", var_name, array.shape
                    quit()

            self.timestep = File["timestep"][:]
            self.redshift = File["redshift"][:]
        elif tr_format == "new":

            for group in File:
                if "timestep" in group and "timestep_" not in group:
                    self.timestep = File[group][:]

            self.tr_vars["tid"] = File["tid"][:]

            self.redshift = np.zeros(len(self.timestep))                
            for i_ts, timestep in enumerate(self.timestep):
                self.redshift[i_ts] = File["timestep_"+str(timestep)+"/redshift"][()]

            # Due to bad practice I now store some arrays in 2d (time first axis) (lagrangian status variables), and some in separate timestep groups... (from link_tracer_cell_photon - except for tracer ID)
            lagrangian_array_names = ["fb_flag_evo",'end_in_sat_evo','enter_in_sat','first_infall_ts','part_orb_type_evo']
                
            for var_name in self.tr_var_names:

                if verbose:
                    print "reading array", var_name
                            
                if var_name in lagrangian_array_names:
                    array = File[var_name][:]
                else:
                    array = np.zeros((len(self.timestep),len(self.tr_vars["tid"])))

                    for i_ts, timestep in enumerate(self.timestep):
                        if var_name == "radius":
                            array[i_ts] = File["timestep_"+str(timestep)+"/r"][:]
                        elif var_name == "radius_p":
                            array[i_ts] = File["timestep_"+str(timestep)+"/r_physical"][:]
                        elif "LyaLum" in var_name and RT==False:
                            array[i_ts] = File["timestep_"+str(timestep)+"/mass"][:]# Placeholder
                        else:
                            array[i_ts] = File["timestep_"+str(timestep)+"/"+var_name][:]
        
                if len(array.shape) == 1 or len(array.shape) == 2:
                    self.tr_vars[var_name] = array
                else:
                    print "nope, wrong shape", var_name, array.shape
                    quit()
                    
            self.tr_var_names.append("tid")
                    
        else:
            print "Error in ldecomp tracer_sample.__init__, tracer format", tr_format, "is not recognised"
            quit()
            
    def n_part(self):
        return len(self.tr_vars["radius"][0])
    def n_ts(self):
        return len(self.tr_vars["radius"][:,0])

    def select_bool(self,ok):
        
        for var_name in self.tr_var_names:
            if len(self.tr_vars[var_name].shape) == 2: # Variables where value at each timestep is stored
                self.tr_vars[var_name] = self.tr_vars[var_name][:,ok]
            elif len(self.tr_vars[var_name].shape) == 1: # Variables where no evolution is tracked
                self.tr_vars[var_name] = self.tr_vars[var_name][ok]
            else:
                print "NOPE............", var_name, "wrong shape"
                quit()
                
    def add_var(self,var,var_name):
        self.tr_var_names.append(var_name)
        self.tr_vars[var_name] = var

    def descope_vars(self,var_names_keep):
        for var_name in self.tr_var_names:
            if var_name not in var_names_keep:
                self.tr_vars[var_name] = None
        
def Identify_First_Infall(tracer_sample,verbose=False):

    n_part = tracer_sample.n_part()
    n_ts = tracer_sample.n_ts()
    
    first_infall_ts = np.zeros(n_part) + np.nan

    for n in range(n_ts):
        if verbose:
            print "Computing when tracers first entered the halo for", n,"of",n_ts
        ok = np.isnan(first_infall_ts) & (np.isnan(tracer_sample.tr_vars["radius"][n]) == False) & (tracer_sample.tr_vars["radius"][n] < 1.0)
        first_infall_ts[ok] = n

    return first_infall_ts
        
def Enter_In_Sat(tracer_sample,verbose=False):
    # Identify particles that came into the host halo within a satellite
    
    n_part = tracer_sample.n_part()
    n_ts = tracer_sample.n_ts()
    
    ############ Identify particles that were in a satellite when they entered the virial radius (base this off first 3 timesteps inside halo, where possible)
    # Note in the future - I should definitely track particles outside virial radius! (that would require knowing if they were inside other halos that are non-satellites however)
    enter_in_sat_score = np.zeros(n_part)

    for n in range(n_ts):
        if verbose:
            print "Computing if tracer entered the halo within a satellite for ts", n,"of",n_ts 
        
        ok = (tracer_sample.tr_vars["comp"][n] > 0) & (tracer_sample.tr_vars["comp"][n]<3) & (tracer_sample.tr_vars["first_infall_ts"] ==n)
        enter_in_sat_score[ok] += 1

        ok = (tracer_sample.tr_vars["comp"][n] >0) & (tracer_sample.tr_vars["comp"][n]<3) & (tracer_sample.tr_vars["first_infall_ts"] == n-1)
        enter_in_sat_score[ok] += 1

        ok = (tracer_sample.tr_vars["comp"][n] > 0) & (tracer_sample.tr_vars["comp"][n]<3) & (tracer_sample.tr_vars["first_infall_ts"] == n-2)
        enter_in_sat_score[ok] += 1

    # Handle particles where we don't have enough outputs to be sure
    ok = (tracer_sample.tr_vars["first_infall_ts"] == n_ts-1) & (tracer_sample.tr_vars["comp"][n_ts-1] > 0) & (tracer_sample.tr_vars["comp"][n_ts-1] < 3)
    enter_in_sat_score[ok] += 2

    ok = (tracer_sample.tr_vars["first_infall_ts"] == n_ts-2) & (tracer_sample.tr_vars["comp"][n_ts-2] > 0) & (tracer_sample.tr_vars["comp"][n_ts-2] < 3)
    enter_in_sat_score[ok] += 1

    # Now I'm writing these flags to disk, more straightforward to not use booleans
    enter_in_sat = np.zeros((n_part))
    enter_in_sat[enter_in_sat_score >= 3] += 1

    return enter_in_sat

def End_In_Sat(tracer_sample,verbose=False):
    # identify particles that are in a satellite at the final step #

    n_part = tracer_sample.n_part()
    n_ts = tracer_sample.n_ts()

    end_in_sat_evo = np.zeros((n_ts, n_part)) < 0
    
    for i_ts in range(n_ts):

        if verbose:
            print "Computing if tracer is in a satellte for ts", i_ts,"of",n_ts
        
        n_ts_temp = i_ts+1
        if n_ts_temp < 3:
            continue

        end_in_sat_score = np.zeros(n_part)

        for n in range(3):
            n1 = n_ts_temp - 3 + n

            ok = ((tracer_sample.tr_vars["comp"][n1] > 0) & (tracer_sample.tr_vars["comp"][n1] <3)) | np.isnan(tracer_sample.tr_vars["comp"][n1])
            end_in_sat_score[ok] += 1

            # For satellites that get close to the 0.2 rvir boundary, we can lose track of them - which can be important for Lya
            # Make it such that these particles are flagged as satellite material if they were in a satellite for any output
            ok2 = (tracer_sample.tr_vars["radius"][n1] < 0.25) & ok
            end_in_sat_score[ok2] += 20
            
        # Handle particles where we don't have enough outputs to be sure 
        ok = (tracer_sample.tr_vars["first_infall_ts"] == n_ts_temp-1) & (tracer_sample.tr_vars["comp"][n_ts_temp-1] > 0) & (tracer_sample.tr_vars["comp"][n_ts_temp-1] < 3)
        end_in_sat_score[ok] += 2
        
        ok = (tracer_sample.tr_vars["first_infall_ts"] == n_ts_temp-2) & (tracer_sample.tr_vars["comp"][n_ts_temp-2] > 0) & (tracer_sample.tr_vars["comp"][n_ts_temp-2] < 3)
        end_in_sat_score[ok] += 1

        # For particles in satellites that are moving out of the 0.2 inner boundary, looking backwards in time can miss that they were in a satellite
        # For this material, try to look forwards in time instead
        if i_ts < n_ts-3:
            for n in range(3):
                n1 = i_ts + n
                ok = (tracer_sample.tr_vars["radius"][n1] < 0.25) & (((tracer_sample.tr_vars["comp"][n1] < 3) & (tracer_sample.tr_vars["comp"][n1] > 0)) | np.isnan(tracer_sample.tr_vars["comp"][n1]))
                end_in_sat_score[ok] += 20
                        
        
        end_in_sat_evo[i_ts] = end_in_sat_score >= 3
        
    end_in_sat_evo[0:3] = (tracer_sample.tr_vars["comp"][0:3] > 0) & (tracer_sample.tr_vars["comp"][0:3] < 3)

    # Now I'm writing these flags to disk, more straightforward to not use booleans
    end_in_sat_evo_out = np.zeros((n_ts,n_part))
    end_in_sat_evo_out[end_in_sat_evo] += 1

    print "check", np.sort(end_in_sat_evo_out)[40]
    
    return end_in_sat_evo_out
    

def Feedback_Influence(tracer_sample,verbose=False):
    # identify cases where feedback is playing a role on the tracer trajectory

    n_part = tracer_sample.n_part()
    n_ts = tracer_sample.n_ts()

    # Zero means not influenced by feedback, 1) means first fb-driven outflow event, 2) means past-apocentre infall, 3) means later past-peri outflow  
    fb_flag = np.zeros(n_part)

    # Timestep when the fb_flag was first activated
    ts_fb_flag = np.zeros(n_part)+np.nan

    # Diagnostics for testing the method
    vhi_flag = np.zeros(n_part)+np.nan # Maximum outflow velocity forwards in time from outflow event 
    vlo_flag = np.zeros(n_part)+np.nan # Minimum (equiv to max abs inflow) velocity backwards in time from event 
    rad_hi_flag = np.zeros(n_part)+np.nan
    rad_lo_flag = np.zeros(n_part)+np.nan
    temp2 = np.zeros((n_ts,n_part))+np.nan
    temp3 = np.zeros((n_ts,n_part))+np.nan

    # Whether flag was activated for a given particle on a given timestep
    fb_flag_evo = np.zeros((n_ts, n_part))

    # These copy functions will be very heavy when running on large tracer samples - consider reworking this later
    cv_temp = np.copy(tracer_sample.tr_vars["cv"])
    cv_temp[(np.isnan(tracer_sample.tr_vars["family"])==False) & (tracer_sample.tr_vars["family"]>100)] = 0.0

    tracer_sample.tr_vars["ek"][tracer_sample.tr_vars["family"]>100] = 0.0
    tracer_sample.tr_vars["eth"][tracer_sample.tr_vars["family"]>100] = 0.0
    
    for n in range(n_ts):
        if n == 0:
            continue

        if verbose:
            print "Computing Feedback influence for timestep", n,"of",n_ts
        
        # Initial selection criteria for feedback influence
        ok = (cv_temp[n] > 50) # Must have a non-neglible outflowing radial velocity
        ok = ok & (abs(cv_temp[n]) > abs(cv_temp[n-1]) + 30) # Radial velocity should have increased
        ok = ok & (fb_flag==0) # No point processing particles that are already flagged
        ok = ok & ( (tracer_sample.tr_vars["ek"][n]+tracer_sample.tr_vars["eth"][n]) > (tracer_sample.tr_vars["ek"][n-1]+tracer_sample.tr_vars["eth"][n-1])*1.25) # Kinetic+Thermal energy must increase
        # The energy threshold here could be tweaked with
                
        # Check for rare cases where a satellite can come along and push a particle out of the ISM
        # Flag this by discarding situations where particle just joined a satellite
        ok = ok & ( ( (tracer_sample.tr_vars["comp"][n]>0) & (tracer_sample.tr_vars["comp"][n]<3) &(tracer_sample.tr_vars["comp"][n-1]==0) ) ==False)

        # Also flag if in satellite for 3 consecutive steps into the future
        try:
            ok = ok & ( ( (tracer_sample.tr_vars["comp"][n]>0) & (tracer_sample.tr_vars["comp"][n]<3) & (tracer_sample.tr_vars["comp"][n+1]>0) & (tracer_sample.tr_vars["comp"][n+1]<3) & (tracer_sample.tr_vars["comp"][n+2]>0)& (tracer_sample.tr_vars["comp"][n+2]<3) ) ==False)
        except:
            None
            
        # Check the tracer isn't attached to a star
        ok = ok & ( tracer_sample.tr_vars["family"][n] == 100 )
        
        # Identify cases where outflow is actually just an orbit transition
        # This currently works by looking back and forard and until reaching maximum absolute radial velocity in both temporal directions.
        # Then check magnitude of outflowing velocity exceeds maximum inflowing velocity
        # There is then a check whether the maximum radius the particle gets to before changing to inflow exceeds a given threshold
        ok_out = np.copy(ok)

        n_temp = len(cv_temp[n][ok])
        if n_temp == 0:
            continue

        go_back = True
        i_back = 1
        back_max = 10 # This corresponds to ~ 0.5-0.75 of halo dynamical time at z=3
        finished_part = np.ones(n_temp) < 0
        cv_lo = np.zeros(n_temp)
        
        while go_back:
            if n-i_back < 0:
                break

            lower = cv_temp[n-i_back][ok] < cv_temp[n-i_back+1][ok]
            cv_lo[(lower==False)&(finished_part==False)] = cv_temp[n-i_back+1][ok][(lower==False)&(finished_part==False)]

            finished_part[(lower==False)] = True
            i_back += 1
            
            if i_back > back_max:
                go_back = False
            if len(finished_part)==len(finished_part[finished_part]):
                go_back = False

        go_forward = True
        i_forward = 1
        forward_max = 10
        finished_part =np.ones(n_temp) < 0
        cv_hi = np.zeros(n_temp)
        
        while go_forward:
            if n + i_forward == n_ts:
                break

            higher = cv_temp[n+i_forward][ok] > cv_temp[n+i_forward-1][ok]
            cv_hi[(higher==False)&(finished_part==False)] = cv_temp[n+i_forward-1][ok][(higher==False)&(finished_part==False)]

            i_forward += 1

            finished_part[(higher==False)] = True
            
            if i_forward > forward_max:
                go_forward = False
            if len(finished_part)==len(finished_part[finished_part]):
                go_forward = False

        # Here is the actual cut in max outflow versus max inflow velocity
        ok2 = cv_hi > abs(cv_lo) * 1.5
        
        # now check rmax
        go_forward = True
        i_forward = 1
        forward_max = 10
        finished_part =np.ones(n_temp) < 0
        r_hi = np.zeros(n_temp)

        while go_forward:
            if n + i_forward == n_ts:
                break
            higher = (tracer_sample.tr_vars["radius"][n+i_forward][ok] > tracer_sample.tr_vars["radius"][n+i_forward-1][ok]) | np.isnan(tracer_sample.tr_vars["radius"][n+i_forward][ok])
            r_hi[(higher==False)&(finished_part==False)] = cv_temp[n+i_forward-1][ok][(higher==False)&(finished_part==False)]
            
            i_forward += 1

            finished_part[(higher==False)] = True
            
            if i_forward > forward_max:
                go_forward = False                
            if len(finished_part)==len(finished_part[finished_part]):
                go_forward = False

        ok3 = (r_hi > tracer_sample.tr_vars["radius"][n][ok] +0.1) | np.isnan(r_hi) | (tracer_sample.tr_vars["radius"][n][ok] > tracer_sample.tr_vars["radius"][n-1][ok] +0.1)

        ok_out[np.where(ok)[0]] = ok2 & ok3
        
        ok = ok_out
        fb_flag[ok] = 1
        ts_fb_flag[ok] = n

        fb_flag_evo[n:,ok] = 1

    return fb_flag_evo, ts_fb_flag


def Feedback_Influence_Slow(tracer_sample):
    # identify cases where feedback is playing a role on the tracer trajectory
    # This is the older version (replaced on 29/4/19) that was very slow as it contained a double for loop
    
    n_part = tracer_sample.n_part()
    n_ts = tracer_sample.n_ts()

    # Zero means not influenced by feedback, 1) means first fb-driven outflow event, 2) means past-apocentre infall, 3) means later past-peri outflow  
    fb_flag = np.zeros(n_part)

    # Timestep when the fb_flag was first activated
    ts_fb_flag = np.zeros(n_part)+np.nan

    # Diagnostics for testing the method
    vhi_flag = np.zeros(n_part)+np.nan # Maximum outflow velocity forwards in time from outflow event 
    vlo_flag = np.zeros(n_part)+np.nan # Minimum (equiv to max abs inflow) velocity backwards in time from event 
    rad_hi_flag = np.zeros(n_part)+np.nan
    rad_lo_flag = np.zeros(n_part)+np.nan
    temp2 = np.zeros((n_ts,n_part))+np.nan
    temp3 = np.zeros((n_ts,n_part))+np.nan

    # Whether flag was activated for a given particle on a given timestep
    fb_flag_evo = np.zeros((n_ts, n_part))

    # These copy functions will be very heavy when running on large tracer samples - consider reworking this later
    cv_temp = np.copy(tracer_sample.tr_vars["cv"])
    cv_temp[(np.isnan(tracer_sample.tr_vars["family"])==False) & (tracer_sample.tr_vars["family"]>100)] = 0.0

    tracer_sample.tr_vars["ek"][tracer_sample.tr_vars["family"]>100] = 0.0
    tracer_sample.tr_vars["eth"][tracer_sample.tr_vars["family"]>100] = 0.0
    
    for n in range(n_ts):
        if n == 0:
            continue

        # Initial selection criteria for feedback influence
        ok = (cv_temp[n] > 50) # Must have a non-neglible outflowing radial velocity
        ok = ok & (abs(cv_temp[n]) > abs(cv_temp[n-1]) + 30) # Radial velocity should have increased
        ok = ok & (fb_flag==0) # No point processing particles that are already flagged
        ok = ok & ( (tracer_sample.tr_vars["ek"]+tracer_sample.tr_vars["eth"])[n] > (tracer_sample.tr_vars["ek"]+tracer_sample.tr_vars["eth"])[n-1]*1.25) # Kinetic+Thermal energy must increase
        # The energy threshold here could be tweaked with
                
        # Check for rare cases where a satellite can come along and push a particle out of the ISM
        # Flag this by discarding situations where particle just joined a satellite
        ok = ok & ( ( (tracer_sample.tr_vars["comp"][n]>0) & (tracer_sample.tr_vars["comp"][n]<3) &((tracer_sample.tr_vars["comp"][n-1]==0)|(tracer_sample.tr_vars["comp"][n-1]==3)) ) ==False)

        # Also flag if in satellite for 3 consecutive steps into the future
        try:
            ok = ok & ( ( (tracer_sample.tr_vars["comp"][n]>0) & (tracer_sample.tr_vars["comp"][n]<3) & (tracer_sample.tr_vars["comp"][n+1]>0) & (tracer_sample.tr_vars["comp"][n+1]<3) & (tracer_sample.tr_vars["comp"][n+2]>0)& (tracer_sample.tr_vars["comp"][n+2]<3) ) ==False)
        except:
            None
            
        # Check the tracer isn't attached to a star
        ok = ok & ( tracer_sample.tr_vars["family"][n] == 100 )
        
        # Identify cases where outflow is actually just an orbit transition
        # This currently works by looking back and forard and until reaching maximum absolute radial velocity in both temporal directions.
        # Then check magnitude of outflowing velocity exceeds maximum inflowing velocity
        # There is then a check whether the maximum radius the particle gets to before changing to inflow exceeds a given threshold
        ok_out = np.copy(ok)
        for m in range(len(cv_temp[n][ok])):
            go_back = True
            i_back = 1
            while go_back:
                if n-i_back < 0:
                    #ok_out[np.where(ok)[0][m]] = False # I'm not sure what the purpose of these lines is so I commented them out...
                    break
                if cv_temp[n - i_back][ok][m] < cv_temp[n - i_back+1][ok][m]:
                    i_back += 1
                else:
                    go_back = False

            go_forward = True
            i_forward = 1
            while go_forward:
                if n + i_forward == n_ts:
                    #ok_out[np.where(ok)[0][m]] = False
                    break
                if cv_temp[n+i_forward][ok][m] > cv_temp[n+i_forward-1][ok][m]:
                    i_forward+=1
                else:
                    go_forward = False

            # Here is the actual cut in max outflow versus max inflow velocity
            ok2 = cv_temp[n+i_forward-1][ok][m] > abs(cv_temp[n-i_back+1][ok][m]) * 1.5
            if ok2:
                vhi_flag[np.where(ok)[0][m]] = cv_temp[n+i_forward-1][ok][m]
                vlo_flag[np.where(ok)[0][m]] = cv_temp[n-i_back+1][ok][m]
                temp2[n,np.where(ok)[0][m]] = 0.1 
            
            # now check rmax
            go_forward = True
            i_forward = 1
            while go_forward:
                if n + i_forward == n_ts:
                    #ok_out[np.where(ok)[0][m]] = False
                    break
                if tracer_sample.tr_vars["radius"][n+i_forward][ok][m] > tracer_sample.tr_vars["radius"][n+i_forward-1][ok][m] or np.isnan(tracer_sample.tr_vars["radius"][n+i_forward][ok][m]):
                    i_forward+=1
                else:
                    go_forward = False
                    
            ok3 = (tracer_sample.tr_vars["radius"][n+i_forward-1][ok][m] > tracer_sample.tr_vars["radius"][n][ok][m] +0.1) | np.isnan(tracer_sample.tr_vars["radius"][n+i_forward-1][ok][m]) | (tracer_sample.tr_vars["radius"][n][ok][m] > tracer_sample.tr_vars["radius"][n-1][ok][m] +0.1)
            
            if ok3:
                rad_lo_flag[np.where(ok)[0][m]] = tracer_sample.tr_vars["radius"][n][ok][m]
                rad_hi_flag[np.where(ok)[0][m]] = tracer_sample.tr_vars["radius"][n+i_forward-1][ok][m]
                temp3[n,np.where(ok)[0][m]] = 0.2
            ok_out[np.where(ok)[0][m]] = ok2 & ok3
            
        ok = ok_out
        fb_flag[ok] = 1
        ts_fb_flag[ok] = n

        fb_flag_evo[n:,ok] = 1

    return fb_flag_evo, ts_fb_flag


def Identify_Kine_Episode(tracer_sample,verbose=False):
    # Track 1st-infall (inflow), past-peri (outflow), and past-apo (inflow)

    n_part = tracer_sample.n_part()
    n_ts = tracer_sample.n_ts()

    n_inflow = np.zeros(n_part)
    n_outflow = np.zeros(n_part)
    n_change = np.zeros(n_part)

     # 0 here means 1st-infall
    # 1 will be past-peri, 2 will be past-apo, nan means ejected/not accreted
    part_orb_type = np.zeros(n_part)

    left = np.zeros(n_part) < 0
    returned = np.zeros(n_part) < 0

    # Keep track of particles kinematic history
    part_orb_type_evo = np.zeros((n_ts, n_part))

    cv_temp = np.copy(tracer_sample.tr_vars["cv"])
    cv_temp[tracer_sample.tr_vars["family"]>100] = 0.0

    
    for n in range(n_ts):
        if verbose:
            print "Computing the orbital episode for tracers on ts", n,"of",n_ts
        inflow = cv_temp[n] < 0
        outflow = cv_temp[n] > 0 # cv_temp set such that 0 means attached to a star
        
        n_inflow[inflow] += 1
        n_inflow[outflow] = 0
        
        n_outflow[outflow] += 1
        n_outflow[inflow] = 0
 
        req_same = 3
        
        in_to_out = ((part_orb_type == 2 ) | (part_orb_type == 0)) & ((n_outflow > req_same) | ((n_outflow>=1)&(tracer_sample.tr_vars["radius"][n] - tracer_sample.tr_vars["radius"][(n-n_outflow).astype("int"),np.arange(0,n_part)]>0.1)))
        part_orb_type[in_to_out] = 1
        part_orb_type_evo[n-req_same:,in_to_out] = 1

        tracer_sample.tr_vars["fb_flag_evo"][n-req_same:,(tracer_sample.tr_vars["fb_flag_evo"][n]>1)&in_to_out] = 3
        
        out_to_in = (part_orb_type == 1) & (n_inflow > req_same)
        part_orb_type[out_to_in] = 2
        part_orb_type_evo[n-req_same:,out_to_in] = 2
        tracer_sample.tr_vars["fb_flag_evo"][n-req_same:,(tracer_sample.tr_vars["fb_flag_evo"][n]>0)&out_to_in] = 2
        
        left = ((n > tracer_sample.tr_vars["first_infall_ts"]) & (np.isnan(cv_temp[n]))) | left
        part_orb_type[left] = np.nan
        part_orb_type_evo[n:,left] = np.nan
                
        returned = left & (np.isnan(cv_temp[n])==False)
        part_orb_type[returned] = 2
        part_orb_type_evo[n:,returned] = 2
        tracer_sample.tr_vars["fb_flag_evo"][n:,(tracer_sample.tr_vars["fb_flag_evo"][n]>0)&returned] = 2
        left[returned] = False
        
    # Handle possible transitions at the end
    in_to_out = ((part_orb_type == 2 ) | (part_orb_type == 0)) & (n_outflow > 1)
    part_orb_type[in_to_out] = 1
    part_orb_type_evo[-2:,in_to_out] = 1
    
    out_to_in = (part_orb_type == 1) & (n_inflow > 1)
    part_orb_type[out_to_in] = 2
    part_orb_type_evo[-2:,out_to_in] = 2

    tracer_sample.tr_vars["fb_flag_evo"][-2:,(tracer_sample.tr_vars["fb_flag_evo"][-1]>1) & in_to_out] = 3
    tracer_sample.tr_vars["fb_flag_evo"][-2:,(tracer_sample.tr_vars["fb_flag_evo"][-1]>0) & out_to_in] = 2

    return part_orb_type_evo


def Compute_xhi_temp_mono(tracer_sample):

    part_orb_type_evo = tracer_sample.tr_vars["part_orb_type_evo"]
    
    n_part = tracer_sample.n_part()
    n_ts = tracer_sample.n_ts()
    
    # Figure out start/finish props for each kinematic episode
    xhi_start = np.zeros_like(tracer_sample.tr_vars["xhi"])
    xhi_stop = np.zeros_like(tracer_sample.tr_vars["xhi"])+np.nan
    xhi_start[0] = tracer_sample.tr_vars["xhi"][0]
    temp_start = np.zeros_like(tracer_sample.tr_vars["temp"])
    temp_stop = np.zeros_like(tracer_sample.tr_vars["temp"])+np.nan
    temp_start[0] = tracer_sample.tr_vars["temp"][0]

    mono_xhi_increase = np.zeros(n_part) # Number of outputs a quantity has been increasing
    mono_xhi_decrease = np.zeros(n_part)# Number of outputs a quantity has been decreasing
    mono_xhi_increase_evo = np.zeros((n_ts,n_part)) # >0 if quantity is considered monotonically increasing
    mono_xhi_decrease_evo = np.zeros((n_ts,n_part)) # >0 if quantity is considered monotonically decreasing

    mono_temp_increase = np.zeros(n_part)
    mono_temp_decrease = np.zeros(n_part)
    mono_temp_increase_evo = np.zeros((n_ts,n_part))
    mono_temp_decrease_evo = np.zeros((n_ts,n_part))

    for n in range(n_ts):
        if n == 0:
            continue
        ok = (part_orb_type_evo[n-1] != part_orb_type_evo[n]) & (np.isnan(tracer_sample.tr_vars["xhi"][n])==False)
        
        ok = ok | ((np.isnan(tracer_sample.tr_vars["xhi"][n])==False) & (np.isnan(tracer_sample.tr_vars["xhi"][n-1])))

        xhi_start[n,ok] = tracer_sample.tr_vars["xhi"][n,ok]
        #xhi_stop[n-1,ok] = tracer_sample.tr_vars["xhi"][n-1,ok]
        xhi_stop[n-1,ok] = tracer_sample.tr_vars["xhi"][n,ok]
        temp_start[n,ok] = tracer_sample.tr_vars["temp"][n,ok]
        #temp_stop[n-1,ok] = tracer_sample.tr_vars["temp"][n-1,ok]
        temp_stop[n-1,ok] = tracer_sample.tr_vars["temp"][n,ok]
        
        mono_xhi_increase[ok] = 0
        mono_xhi_decrease[ok] = 0
        mono_temp_increase[ok] = 0
        mono_temp_decrease[ok] = 0
        
        nok = (ok == False) & (np.isnan(tracer_sample.tr_vars["xhi"][n])==False)
        xhi_start[n,nok] = xhi_start[n-1,nok]
        xhi_stop[n,nok] = xhi_stop[n-1,nok]
        temp_start[n,nok] = temp_start[n-1,nok]
        temp_stop[n,nok] = temp_stop[n-1,nok]

        mono_xhi_increase[nok & (tracer_sample.tr_vars["xhi"][n] > tracer_sample.tr_vars["xhi"][n-1])] += 1
        mono_xhi_decrease[nok & (tracer_sample.tr_vars["xhi"][n] > tracer_sample.tr_vars["xhi"][n-1])] =0
        mono_xhi_decrease[nok & (tracer_sample.tr_vars["xhi"][n] < tracer_sample.tr_vars["xhi"][n-1])] += 1
        mono_xhi_increase[nok & (tracer_sample.tr_vars["xhi"][n] < tracer_sample.tr_vars["xhi"][n-1])] =0
        
        mono_temp_increase[nok & (tracer_sample.tr_vars["temp"][n] > tracer_sample.tr_vars["temp"][n-1])] += 1
        mono_temp_decrease[nok & (tracer_sample.tr_vars["temp"][n] > tracer_sample.tr_vars["temp"][n-1])] =0
        mono_temp_decrease[nok & (tracer_sample.tr_vars["temp"][n] < tracer_sample.tr_vars["temp"][n-1])] += 1
        mono_temp_increase[nok & (tracer_sample.tr_vars["temp"][n] < tracer_sample.tr_vars["temp"][n-1])] =0

        # Deal with ejected particles
        ok = np.isnan(tracer_sample.tr_vars["xhi"][n]) & (np.isnan(tracer_sample.tr_vars["xhi"][n-1])==False)
        #xhi_stop[n-1,ok] = tracer_sample.tr_vars["xhi"][n-1,ok]
        #temp_stop[n-1,ok] = tracer_sample.tr_vars["temp"][n-1,ok]
        xhi_stop[n-1,ok] = tracer_sample.tr_vars["xhi"][n,ok]
        temp_stop[n-1,ok] = tracer_sample.tr_vars["temp"][n,ok]

        mono_xhi_increase[ok] = 0
        mono_xhi_decrease[ok] = 0
        mono_temp_increase[ok] = 0
        mono_temp_decrease[ok] = 0

        n_mono_thr = 3
        
        ok = mono_xhi_increase > n_mono_thr
        n_loop = int(np.max(mono_xhi_increase))
        for i_n in range(n_loop):
            mono_xhi_increase_evo[n-i_n,ok] = np.max((mono_xhi_increase[ok]-i_n,np.zeros(len(ok[ok]))),axis=0)
        ok = mono_xhi_decrease > n_mono_thr
        n_loop = int(np.max(mono_xhi_decrease))
        for i_n in range(n_loop):
            mono_xhi_decrease_evo[n-i_n,ok] = np.max((mono_xhi_decrease[ok]-i_n,np.zeros(len(ok[ok]))),axis=0)

        ok = mono_temp_increase > n_mono_thr
        n_loop = int(np.max(mono_temp_increase))
        for i_n in range(n_loop):
            mono_temp_increase_evo[n-i_n,ok] = np.max((mono_temp_increase[ok]-i_n,np.zeros(len(ok[ok]))),axis=0)
        ok = mono_temp_decrease > n_mono_thr
        n_loop = int(np.max(mono_temp_decrease))
        for i_n in range(n_loop):
            mono_temp_decrease_evo[n-i_n,ok] = np.max((mono_temp_decrease[ok]-i_n,np.zeros(len(ok[ok]))),axis=0)
            
    xhi_stop[-1] = tracer_sample.tr_vars["xhi"][-1]
    temp_stop[-1] = tracer_sample.tr_vars["temp"][-1]

    # Finishing setting the end xhi/T for each kinematic episode (need to go backwards through loop)
    for m in range(n_ts):
        if m == 0:
            continue
        n = n_ts-1-m

        ok = (part_orb_type_evo[n+1] != part_orb_type_evo[n]) & (np.isnan(tracer_sample.tr_vars["xhi"][n])==False)
        nok = (ok == False) & (np.isnan(tracer_sample.tr_vars["xhi"][n])==False)
        xhi_stop[n,nok] = xhi_stop[n+1,nok]
        temp_stop[n,nok] = temp_stop[n+1,nok]

    # Compute monotonic version of props
    xhi_mono = np.copy(tracer_sample.tr_vars["xhi"]) # 1st mono version - where all fluctuations inside a given kinetmatic period are kill
    temp_mono = np.copy(tracer_sample.tr_vars["temp"])
    for n in range(n_ts):
        if n == 0:
            continue

        increase = (xhi_start[n] <= xhi_stop[n]) & (np.isnan(tracer_sample.tr_vars["xhi"][n])==False) & (np.isnan(tracer_sample.tr_vars["xhi"][n-1])==False)
        increase = increase & (part_orb_type_evo[n] == part_orb_type_evo[n-1])
        decrease = (xhi_start[n] > xhi_stop[n]) & (np.isnan(tracer_sample.tr_vars["xhi"][n])==False) & (np.isnan(tracer_sample.tr_vars["xhi"][n-1])==False)
        decrease = decrease & (part_orb_type_evo[n] == part_orb_type_evo[n-1])

        xhi_mono[n,increase] = np.max( [xhi_mono[n-1,increase], tracer_sample.tr_vars["xhi"][n,increase]],axis=0)
        xhi_mono[n,increase] = np.min( [xhi_mono[n,increase], xhi_stop[n,increase]],axis=0)

        xhi_mono[n,decrease] = np.min( [xhi_mono[n-1,decrease], tracer_sample.tr_vars["xhi"][n,decrease]],axis=0)
        xhi_mono[n,decrease] = np.max( [xhi_mono[n,decrease], xhi_stop[n,decrease]],axis=0)

        increase = (temp_start[n] <= temp_stop[n]) & (np.isnan(tracer_sample.tr_vars["temp"][n])==False) & (np.isnan(tracer_sample.tr_vars["temp"][n-1])==False)
        decrease = (temp_start[n] > temp_stop[n]) & (np.isnan(tracer_sample.tr_vars["temp"][n])==False) & (np.isnan(tracer_sample.tr_vars["temp"][n-1])==False)

        temp_mono[n,increase] = np.max( [temp_mono[n-1,increase], tracer_sample.tr_vars["temp"][n,increase]],axis=0)
        temp_mono[n,increase] = np.min( [temp_mono[n,increase], temp_stop[n,increase]],axis=0)

        temp_mono[n,decrease] = np.min( [temp_mono[n-1,decrease], tracer_sample.tr_vars["temp"][n,decrease]],axis=0)
        temp_mono[n,decrease] = np.max( [temp_mono[n,decrease], temp_stop[n,decrease]],axis=0)

    # Allow long-term fluctuations (which monotonically increase or decrease for more than a given no of outputs)
    # Note this was not particularly well implemented - needs more development and testing
    '''xhi_mono2 = np.copy(xhi_mono) # 2nd version - which permits longer term fluctuations
    temp_mono2 = np.copy(temp_mono)
    for n in range(n_ts):
        if n == 0:
            continue

        allow = (mono_xhi_increase_evo[n] > 0) | (mono_xhi_decrease_evo[n] >0)
        xhi_mono2[n,allow] = tracer_sample.tr_vars["xhi"][n,allow]
        allow = (mono_temp_increase_evo[n] > 0) | (mono_temp_decrease_evo[n] >0)
        temp_mono2[n,allow] = tracer_sample.tr_vars["temp"][n,allow]'''

    return xhi_mono, temp_mono
