import numpy as np
import h5py
import utilities_cosmology as uc
import dm_utils as dm
import utilities_ramses as ur
import ldecomp_functions as ldf
import sys

'''Zoom = "Zoom-7-1290"
Run = "tracer_run_lr"
timestep_final = 155
InDir = "/cral2/mitchell/PostProcess/"
RunDir = "/cral4/mitchell/data/Tracer_Test/"+ Zoom + "/" + Run
OutDir = "/cral4/mitchell/data/Tracer_Test/PostProcess/"
RT = False
downsample = True
n_downsample = "1000"'''

# This one won't work without paths/filenames being tweaked anymore
#Zoom = "Zoom-7-5092"
#Run = "RhdRun-tracer_norad_bug"
#timestep_final = 105
#OutDir = "/cral2/mitchell/PostProcess/"
#File = h5py.File(OutDir+"save_5092_backup.hdf5")

Zoom = "Zoom-7-5092"
Run = "RhdRun-tracer_radfix"
timestep_final = 107
RunDir = "/cral2/mitchell/"+ Zoom + "/" + Run
OutDir = "/cral2/mitchell/PostProcess/"
InDir = OutDir
RT = True
downsample = True
n_downsample = "3e6"

####### Choose fraction of halo dynamical time to use for previous output ########
tdyn_frac = 0.25

# Choose radial bins for computing exchange terms
delta_rprime = 0.1
r_bins = np.arange(0.0, 1.1, delta_rprime)
#delta_rprime = 0.4
#r_bins = np.array([0.2,0.6,1.0])

########## Decide tracer exchanges to record (n -> n+1) ##############
# Tracers at timestep n
weight_n = ["neutral", "neutral", "ion",     "ion",   "ion",      "ion"]
T_lo_n =   [0.0,        0.0,      0.0,       0.0,      10.**4.5,  10.**4.5]
T_hi_n =   [1e20,       1e20,     10.**4.5,  10.**4.5, 1e20,      1e20]
xhi_lo_n = [0.0,        0.0,      0.0,       0.0,      0.0,       0.0]
xhi_hi_n = [1.0,        1.0,      1.0,       1.0,      1.0,       1.0]
part_n =   [1.0,        0.0,      1.0,       0.0,      1.0,       0.0]
r_lo_n =   [0.0,        0.0,      0.0,       0.0,      0.0,       0.0]
r_hi_n =   [1.0,        1.0,      1.0,       1.0,      1.0,       1.0]
select_type_list = ["mass","mass","mass",   "mass",   "mass",    "mass"]
name_n =   ["neut_out","neut_in","wion_out","wion_in","hion_out","hion_in"]

for name in select_type_list:
    if name != "mass":
        print "Output naming convention for anything other than mass weighting is not yet implemented"
        quit()

# Tracer partitions
weight_n1 = ["neutral", "all",        "ion",     "ion",     "neutral", "all",       "ion",     "ion"]
T_lo_n1 =   [0.0,        0.0,          0.0,       10.**4.5,  0.0,       0.0,         0.0,       10.**4.5]
T_hi_n1 =   [1e20,       1e20,         10.**4.5,  1e20,      1e20,      1e20,        10.**4.5,  1e20]
xhi_lo_n1 = [0.0,        0.0,          0.0,       0.0,       0.0,       0.0,         0.0,       0.0]
xhi_hi_n1 = [1.0,        1.0,          1.0,       1.0,       1.0,       1.0,         1.0,       1.0]
part_n1 =    [1.0,        1.0,          1.0,       1.0,       0.0,       0.0,         0.0,       0.0]
r_lo_n1 =   [0.0,        0.0,          0.0,       0.0,       0.0,       0.0,         0.0,       0.0]
r_hi_n1 =   [1.0,        1.0,          1.0,       1.0,       1.0,       1.0,         1.0,       1.0]
name_n1 =   ["neut_out","tot_outflow","wion_out","hion_out","neut_in", "tot_inflow","wion_in", "hion_in"]





########## Figure out which input files of tracer trajectories we will need to read in ###########
if downsample:
    n_tr_samples = 1
    filename = "saved_tracers_select_"+Zoom+"_"+Run + "_ds_"+ n_downsample + ".hdf5"
    filenames_list = [filename]

    example_File = h5py.File(InDir+filename)

else:
    import os
    files = os.listdir(InDir)
    filename_base = "saved_tracers_select_"+Zoom+"_"+Run
    
    filenames_list = []
    for file_n in files:
        if filename_base in file_n and "subset" in file_n:
            filenames_list.append(file_n)
            n_tr_samples = len(filenames_list)
            
            example_File = h5py.File(InDir+file_n)

# Count recorded number of tracer trajectory timesteps so we can shape output array
n_ts = len(example_File["timestep"][:])
example_File.close()

##### Set shape of output array #########
r_match_type = ["_mean", "_ps", "_ts"]
exchange_terms = np.zeros(( n_ts, len(weight_n), len(weight_n1), len(r_match_type), len(r_bins[0:-1]) ))






############### Loop over tracer sub-samples (to avoid running out of memory) ###########
# Note we don't need to loop if downsample = True (provided you didn't select too many tracers)

for ifile, filename in enumerate(filenames_list):

    if not downsample:
        print "Reading/Computing for tracer subset", ifile+1, "of", len(filenames_list)
        # This is supposed to help with getting python to write something to the nohup log file
        sys.stdout.flush()
        
    File = h5py.File(InDir+filename)

    tracer_sample = ldf.tracer_sample_class(File,verbose=True,RT=RT)
    File.close()

    n_select = len(tracer_sample.tr_vars["tid"])

    # Get time from redshift
    dm.jeje_utils.read_conversion_scales(RunDir,tracer_sample.timestep[0])
    omega_0,lambda_0,little_h = dm.jeje_utils.read_cosmo_params(RunDir,tracer_sample.timestep[0])
    a_in = 1./(1.+tracer_sample.redshift)
    tracer_sample.t, tracer_sample.tlb = uc.t_Universe(a_in,omega_0,little_h)
    
    n_part = tracer_sample.n_part()
    n_ts = tracer_sample.n_ts()

    # First task is to identify particles that were accreted into the virial radius for the first time inside a satellite
    first_infall_ts = ldf.Identify_First_Infall(tracer_sample)
    tracer_sample.add_var(first_infall_ts,"first_infall_ts")

    # Drop out particles that never enter the virial radius for this final timestep
    ok = (np.isnan(first_infall_ts) == False)
    tracer_sample.select_bool(ok)

    n_part = tracer_sample.n_part()

    # Now identify particles that were in a satellite when they entered the virial radius
    print "finding particles that enter in satellites"
    enter_in_sat = ldf.Enter_In_Sat(tracer_sample)
    tracer_sample.add_var(enter_in_sat,"enter_in_sat")

    # Now identify particles that are in a satellite at the final steps #
    print "finding particles that finish in satellites"
    end_in_sat_evo = ldf.End_In_Sat(tracer_sample)
    tracer_sample.add_var(end_in_sat_evo,"end_in_sat_evo")

    # Identify particles that have been influenced by feedback
    print "computing feedback influence"
    fb_flag_evo, ts_fb_flag = ldf.Feedback_Influence(tracer_sample)
    tracer_sample.add_var(fb_flag_evo, "fb_flag_evo")
    tracer_sample.add_var(ts_fb_flag, "ts_fb_flag")

    # Identify orbital kinematic episodes
    print "Computing kine episodes"
    part_orb_type_evo = ldf.Identify_Kine_Episode(tracer_sample)
    tracer_sample.add_var(part_orb_type_evo,"part_orb_type_evo")



    # Compute monotonic xhi and monotonic T #
    print "Computing mono xhi and mono T"
    xhi_mono, temp_mono = ldf.Compute_xhi_temp_mono(tracer_sample)
    
    for m in range(n_ts):

        if m == 0:
            continue
        print "Now looping over timesteps, computing exchange rates for step", m+1,"of",n_ts

        timestep = tracer_sample.timestep[m]
    
        r2kpc, redshift, little_h, m2Msun,rho2msunpkpc3,v2kms = ur.Set_Units(RunDir, timestep)
        a = 1./(1.+redshift)
        omega_0,lambda_0,little_h = dm.jeje_utils.read_cosmo_params(RunDir,timestep)
        t_cosmic, junk = uc.t_Universe(a,omega_0,little_h)
        
        #### Work out which previous timestep we are going to use #####

        # Proxy for halo dynamical time
        tdyn = t_cosmic * 0.1

        t_cosmic_prev_desired = t_cosmic - tdyn_frac*tdyn
        
        i_choose = np.argmin(abs(t_cosmic_prev_desired-tracer_sample.t))
        if i_choose == m:
            print "Warning, closest output was current, shifting back one"
            i_choose = m-1
        else:
            print "Offset in outputs is", m - i_choose

        t_cosmic_prev = tracer_sample.t[i_choose]
        delta_t = (t_cosmic - t_cosmic_prev) * 1e9 # yr
             
        # Could also set to temp, temp_mono, or temp_mono2
        temp_use = temp_mono
        xhi_use = xhi_mono
    
        for i_n in range(len(weight_n)):
            for i_n1 in range(len(weight_n1)):

                # Don't add stuff inside satellites
                ok = ((tracer_sample.tr_vars["comp"][m] == 0)|(tracer_sample.tr_vars["comp"][m]==3))
                ok = ok & ((tracer_sample.tr_vars["comp"][i_choose]==0) | (tracer_sample.tr_vars["comp"][i_choose]==3))
                
                ok = ok & (tracer_sample.tr_vars["family"][m] == 100) & (tracer_sample.tr_vars["family"][i_choose]==100) # No stars!
                
                # Radius range
                ok = ok & (tracer_sample.tr_vars["radius"][m] > r_lo_n1[i_n1]) & (tracer_sample.tr_vars["radius"][m] < r_hi_n1[i_n1])
                ok = ok & (tracer_sample.tr_vars["radius"][i_choose] > r_lo_n[i_n]) & (tracer_sample.tr_vars["radius"][i_choose] < r_hi_n[i_n])
            
                # Temperature range
                ok = ok & (temp_use[m] > T_lo_n1[i_n1]) & (temp_use[m] < T_hi_n1[i_n1])
                ok = ok & (temp_use[i_choose] > T_lo_n[i_n]) & (temp_use[i_choose] < T_hi_n[i_n])
                
                # Neutral range
                ok = ok & (xhi_use[m] > xhi_lo_n1[i_n1]) & (xhi_use[m] < xhi_hi_n1[i_n1])
                ok = ok & (xhi_use[i_choose] > xhi_lo_n[i_n]) & (xhi_use[i_choose] < xhi_hi_n[i_n])

                # Select kinematic state (based on Lagrangian trajectory)
                # Group inflows together from 1st infall and past-apo
                temp_kine = np.copy(part_orb_type_evo[m])
                temp_kine[temp_kine == 2] = 0
                temp_kine_prev = np.copy(part_orb_type_evo[i_choose])
                temp_kine_prev[temp_kine_prev==2] = 0
                
                ok = ok & (temp_kine == part_n1[i_n1]) & (temp_kine_prev == part_n[i_n])
                
                # Choose what quantity to weight by
                if select_type_list[i_n] == "mass":
                    quant = tracer_sample.tr_vars["mass"][m]
                    quant_prev = tracer_sample.tr_vars["mass"][m]
                elif select_type_list[i_n] == "LyaLum":
                    quant = tracer_sample.tr_vars["LyaLum"][m]
                    quant_prev = tracer_sample.tr_vars["LyaLum"][i_choose]
                else:
                    print "Nope, choose mass or LyaLum"
                    quit()

                ##### Loop over matching radius binning ##########
                for i_r_match in range(len(r_match_type)):
                    
                    if i_r_match == 0:
                        r_match = 0.5 * (tracer_sample.tr_vars["radius"][m] + tracer_sample.tr_vars["radius"][i_choose])[ok]
                    elif i_r_match == 1:
                        r_match = tracer_sample.tr_vars["radius"][i_choose][ok]
                    elif i_r_match == 2:
                        r_match = tracer_sample.tr_vars["radius"][m][ok]
                    else:
                        print "nope"
                        quit()

                    ######### Loop over radius bins ##############
                    for i_r in range(len(r_bins[0:-1])):
                        okr = (r_match > r_bins[i_r]) & (r_match < r_bins[i_r+1])

                        quant_xhi_prev = quant_prev[ok][okr] * xhi_use[i_choose,ok][okr]
                        quant_xhii_prev =quant_prev[ok][okr] * (1-xhi_use[i_choose,ok][okr])

                        quant_xhi = quant[ok][okr] * xhi_use[m,ok][okr]
                        quant_xhii = quant[ok][okr] * (1-xhi_use[m,ok][okr])

                        # Note += here because we are looping over tracer sub-samples if downsample = False
                        if weight_n[i_n] == "neutral" and weight_n1[i_n1] == "neutral":
                            exchange_terms[m][i_n][i_n1][i_r_match][i_r] += np.sum( np.min(( quant_xhi_prev, quant_xhi ),axis=0) ) / delta_t / delta_rprime # Msun yr^-1 (if quant is in Msun)
                        elif weight_n[i_n] == "neutral" and weight_n1[i_n1] == "all":
                            exchange_terms[m][i_n][i_n1][i_r_match][i_r] += np.sum( quant_xhi_prev ) / delta_t / delta_rprime
                        elif weight_n[i_n] == "ion" and weight_n1[i_n1] == "ion":
                            exchange_terms[m][i_n][i_n1][i_r_match][i_r] += np.sum( np.min(( quant_xhii_prev, quant_xhii ),axis=0) ) / delta_t / delta_rprime
                        elif weight_n[i_n] == "ion" and weight_n1[i_n1] == "all":
                            exchange_terms[m][i_n][i_n1][i_r_match][i_r] += np.sum( quant_xhii_prev ) / delta_t /delta_rprime
                        elif weight_n[i_n] == "neutral" and weight_n1[i_n1] == "ion":
                            exchange_terms[m][i_n][i_n1][i_r_match][i_r] += np.sum( np.max(( quant_xhi_prev - quant_xhi ,np.zeros_like(quant_xhi)),axis=0) ) / delta_t / delta_rprime
                        elif weight_n[i_n] == "ion" and weight_n1[i_n1] == "neutral":
                            exchange_terms[m][i_n][i_n1][i_r_match][i_r] += np.sum( np.max(( quant_xhii_prev - quant_xhii ,np.zeros_like(quant_xhi)),axis=0) ) / delta_t / delta_rprime
                        else:
                            print weight_n[i_n], "into", weight_n1[i_n1], "is not implemented"
                            quit()


# Write the exchange rates to disk

# Read in relevant tree information from the hdf5 file
filename = Zoom + "_" + Run + "_tsf" + str(timestep_final) +".hdf5"
    
output_File = h5py.File(OutDir+filename)
zoom_group = output_File[Zoom]

for halo_group_str in zoom_group:
    # Only compute fluxes for the main halo
    if not "_main" in halo_group_str:
        continue
    mp_group = zoom_group[halo_group_str+"/main_progenitors"]

timesteps = tracer_sample.timestep
timesteps_out = mp_group["halo_ts"][:] +1 # +1 is for the tree/simulation indexing mismatch

n_ts = tracer_sample.n_ts()
n_ts_out = len(timesteps_out)

exchange_names = []
exchange_out_list = []
n_r = len(r_bins[1:])
for i_n in range(len(weight_n)):
    for i_n1 in range(len(weight_n1)):
        for i_r_match in range(len(r_match_type)):
            exchange_names.append("lagrangian_exchange_"+name_n[i_n]+"_"+name_n1[i_n1]+r_match_type[i_r_match])
            exchange_out_list.append(np.zeros((n_ts_out, n_r)) * np.nan)

for dset in mp_group:
    for name in exchange_names:
        if name == dset:
            del mp_group[dset]
            
# Reshape data array into desired output_form
for i_ts in range(n_ts):
    i_ts_out = np.where(timesteps[i_ts] == timesteps_out)[0][0]
    
    i_name = 0
    for i_n in range(len(weight_n)):
        for i_n1 in range(len(weight_n1)):
            for i_r_match in range(len(r_match_type)):
                exchange_out_list[i_name][i_ts_out] = exchange_terms[i_ts][i_n][i_n1][i_r_match]

                #print i_name, exchange_terms_list[i_ts][i_n][i_n1]
                i_name += 1

for name, data in zip(exchange_names, exchange_out_list):
    mp_group.create_dataset(name,data=data)

print "got to the end :-)"
