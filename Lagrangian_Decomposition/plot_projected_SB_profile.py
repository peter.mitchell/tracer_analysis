import numpy as np
import h5py
import dm_utils as dm
import utilities_ramses as ur
import utilities_cosmology as uc
import utilities_statistics as us

# This script relies on rascas photons being matched to tracers. Photons that are not matched (i.e. are in a cell with zero tracers inside) are missed. Note this limitation does not apply to the total Lya profile though.
tracer_sample_correct = True # If true, apply a global correction to the normalisation

#rascas_radius = 6 # arcsec
rascas_radius = 11 # arcsec

z_psf = 3.5 # Redshift to show the PSF (by convolving a scaled point source)

angle_av_lum_cut = False # Apply a luminosity cut to the total Lya signal - compared to limiting luminosity for Wisotzki
lum_cut = 1e42
#lum_cut = 5e41

angle_av_flux_cut = False # Apply a flux cut to the total Lya signal
flux_cut = 10.0**-17.8

if angle_av_flux_cut and angle_av_lum_cut:
    print "Error - can't run with both a luminosity and a flux cut"
    quit()

psf_convolve = True # Convolve with MUSE PSF
lambda0 = 1215.67 # Rest-frame lya wavelength

# For MUDF value of beta, see Bacon 17, summary and conclusion section (and search "moffat")
beta = 2.8 # MOFFAT profile shape exponent

# For dependence of FWHM on wavelength, see fig. 14, Bacon 17 (I do a by-eye average of the UDF fields)
x1 = 6000; y1 = 0.675
x2 = 9000; y2 = 0.575
grad = (y2-y1)/(x2-x1)
intercept = y1 - grad*x1

fwhm_f = lambda wvl: wvl * grad + intercept # Arcseconds - typicaly MUSE UDF FWHM, as function of lambda_obs

def moffat(x, fwhm, beta):
    # Note: I will do the normalisation after the fact since we want to combine two mirror-versions

    # From a quick google search
    #(http://web.ipac.caltech.edu/staff/fmasci/home/astro_refs/PSFsAndSampling.pdf)
    alpha = fwhm / np.sqrt(2**(1/beta)-1) / (2)

    y = (1 + np.square(x)/np.square(alpha))**(-beta)
    ymid = 0.5*(y[1:] + y[0:-1])
    #y *= 1./np.sum(ymid)
    return y


# Stacking interval
zlo = 3.0
zhi = 4.0

print ""
print "Temp hack while rascas not run after 148"
zlo = 3.08
print ""

rad_bins_proj_phys = np.arange(0.0,42.,2.)
rmid = 0.5 * (rad_bins_proj_phys[0:-1] + rad_bins_proj_phys[1:])
drad = 2.

Zoom = "Zoom-7-5092"
Run = "RhdRun-tracer_radfix"
timestep_final = 155
OutDir = "/cral2/mitchell/PostProcess/"
RunDir = "/cral2/mitchell/"+ Zoom + "/" + Run

# Load Lutz 2018 nature paper data
rad_W18, logSB_W18 = np.loadtxt("lutz_data.dat",unpack=True)

# Arcsecond on axis
# For the simulation cosmo params, Ned CosmoCalc gives the angular conversion as 7.503 kpc/" at z=3.5
as_ticks = np.array([0, 1, 2, 3, 4, 5, 6, 8, 10])
t_ticks = as_ticks * 7.503
n_ticks = len(as_ticks)
tick_labels = []
for m in range(n_ticks):
    tick_labels.append(str(as_ticks[m]))

############## Zoom file IO #########################
# Read in relevant tree information from the hdf5 file
filename_zoom = Zoom + "_" + Run + "_tsf" + str(timestep_final) +".hdf5"

File = h5py.File(OutDir+filename_zoom,"r")

# Open the subvolume file (containing all cell data)
subvol_filename = "subvol_"+filename_zoom
subvol_File = h5py.File(OutDir+subvol_filename)

# Check the file with the tree information already exists
if not Zoom in File:
    print "Error: Tree information for ", Zoom, " in ", filename_zoom, " cannot be read"
    exit()

zoom_group = File[Zoom]

######### Setup the output arrays ##########
# Get number of timesteps
for halo_group_str in zoom_group:
    if "_main" in halo_group_str:
        halo_group_str_use= halo_group_str
mp_group = zoom_group[halo_group_str_use+"/main_progenitors"]
r200 = mp_group["r200"][:]
timesteps = mp_group["halo_ts"][:] +1 # +1 is for the tree/simulation indexing mismatch
z = np.zeros_like(timesteps)

dm.jeje_utils.read_conversion_scales(RunDir,timestep_final)
omega_0,lambda_0,little_h = dm.jeje_utils.read_cosmo_params(RunDir,timestep_final)

r2kpc_evo = np.zeros(len(z))
for n in range(len(z)):
    r2kpc_ideal, redshift, little_h, m2Msun,junk,junk = ur.Set_Units(RunDir, timesteps[n], "ideal")
    z[n] = redshift


# Read luminosity profiles
lum = {}
names = ["1st_infall","orbit","stripped","fb","ism","sat"]
n_g = 3

lum["proj_rec_tot"] = mp_group["proj_rec_tot_phys"][:]
lum["proj_col_tot"] = mp_group["proj_col_tot_phys"][:]

lum["proj_tot_median"] = lum["proj_rec_tot"] + lum["proj_col_tot"]

if angle_av_lum_cut:
    lum_tot = np.sum(lum["proj_rec_tot"]+lum["proj_col_tot"],axis=1)
    ok2 = lum_tot > lum_cut
elif angle_av_flux_cut:
    ok2 = np.zeros_like(np.sum(lum["proj_rec_tot"],axis=1))>0
    
if psf_convolve:
    lum["proj_rec_tot_conv"] = np.zeros((len(z),974))
    lum["proj_col_tot_conv"] = np.zeros((len(z),974))

i_z_psf = np.argmin(abs(z-z_psf))

# Conversion into surface brightness (and convolve with MUSE PSF if desired)
for i, z_i in enumerate(z):
    dL, dA = uc.Luminosity_Angular_Distances(z_i,little_h,omega_0,lambda_0) # cMpc (I think)
    
    cm2Mpc = 3.0857*10**24
    arcsec2radian = 4.84814*10**-6
    con2arcsec = 1 / (dA*1e3) / arcsec2radian # Converts pkpc into arcsec at this redshift
    
    conversion = 1/(4*np.pi*(dL*cm2Mpc)**2) / ((np.pi*(rmid+0.5*drad)**2 - np.pi*(rmid-0.5*drad)**2) *con2arcsec**2)

    # If J's script was correct, we redshift the photon energies before applying the luminosity distance - let's see
    #print "TEmp hack" 
    #conversion *= 1./(1+z_i)
    
    if angle_av_flux_cut:
        flux_tot = np.sum(lum["proj_rec_tot"][i] + lum["proj_col_tot"][i]) /(4*np.pi*(dL*cm2Mpc)**2) # erg s^-1 cm^-2
        ok2[i] = flux_tot > flux_cut

        #jimbo = rmid * con2arcsec < 3.0
        #flux_tot_re = np.sum(lum["proj_rec_tot"][i][jimbo] + lum["proj_col_tot"][i][jimbo]) /(4*np.pi*(dL*cm2Mpc)**2) # erg s^-1 cm^-2

    #lum_jim = np.sum(lum["proj_col_tot"][i])

    lum["proj_rec_tot"][i] *= conversion # erg s^-1 cm^-2 arcsec^-2
    lum["proj_col_tot"][i] *= conversion # erg s^-1 cm^-2 arcsec^-2

    lum["proj_tot_median"][i] *= conversion

    
    if psf_convolve:
        lambda_obs = lambda0 * (1+z_i) # Observer frame wvl of lya
        fwhm = fwhm_f(lambda_obs) # Arcsec
        
        rmid_arcsec_coarse = rmid / 7.904 # arcsec (note this is the conversion at z=3 - for convenience reasons)
        drad_arcsec_coarse = drad / 7.904 # arcsec

        # Use a finer grid for convolution - via interpolation
        rmid_arcsec = np.arange(26,1000) * (rmid_arcsec_coarse[-1]/(1000))
        rmid_fine = rmid_arcsec * 7.904 # pkpc
        nrad = len(rmid_arcsec)
        drad_arcsec = rmid_arcsec[1]-rmid_arcsec[0]

        r_pos = np.arange(0,nrad)*drad_arcsec
        moff_pos = moffat(r_pos, fwhm,beta)
        r_neg = np.arange(-nrad,0)*drad_arcsec
        moff_neg = moffat(r_neg, fwhm,beta)

        moff_shift = np.concatenate((moff_pos,moff_neg)) # arcsec
        moff_shift *= 1./np.sum(moff_shift)

        lum_i = us.Linear_Interpolation(rmid_arcsec_coarse, lum["proj_rec_tot"][i], rmid_arcsec)
        lum_i = np.concatenate((lum_i[::-1], lum_i)) # Need to make profile symmetric for FFT
        lum["proj_rec_tot_conv"][i] = np.fft.irfft( np.fft.rfft(lum_i) * np.fft.rfft(moff_shift) )[974:]

        lum_i = us.Linear_Interpolation(rmid_arcsec_coarse, lum["proj_col_tot"][i], rmid_arcsec)
        lum_i = np.concatenate((lum_i[::-1], lum_i))
        lum["proj_col_tot_conv"][i] = np.fft.irfft( np.fft.rfft(lum_i) * np.fft.rfft(moff_shift) )[974:]

        if i == i_z_psf:
            SB = np.zeros_like(rmid_arcsec)
            SB[0] = 1.0
            SB = np.concatenate((SB[::-1], SB))
            PSF = np.fft.irfft( np.fft.rfft(SB) * np.fft.rfft(moff_shift) )[974:]
        
ok = (z>zlo) & (z<zhi)

if angle_av_lum_cut or angle_av_flux_cut:
    print "Redshifts that passed luminosity (or flux) cut"
    print z[ok2], "fraction of outputs in range", len(ok[ok&ok2]) / float(len(ok[ok]))

    ok[ok2==False] = False

print ""
print "r200 values over selected redshift/brightness range"
print r200[ok]
print ""

ind = np.argmin(abs(z[ok] -3.5))
r200_3p5 = r200[ok][ind]

# Indicate the radius at which we will start suffering from edge/projection problems
rmax_ok = np.sqrt(0.5*np.square(2*r200_3p5))

if rascas_radius == 6:
    print "Note in projection 6 arcsec at z=3.5 means we would get edge effects at"
    rmax_arcsec = np.sqrt(0.5*np.square(6))
    print rmax_arcsec
elif rascas_radius == 11:
    print "Note in projection 11 arcsec at z=3.5 means we would get edge effects at"
    rmax_arcsec = np.sqrt(0.5*np.square(11))
    print rmax_arcsec
else:
    print "Nah mate"
    quit()
# For the simulation cosmo params, Ned CosmoCalc gives the angular conversion as 7.503 kpc/" at z=3.5
rmax_rascas = rmax_arcsec * 7.503

    
lum["proj_rec_tot"] = np.sum(lum["proj_rec_tot"][ok],axis=0) / len(z[ok])
lum["proj_col_tot"] = np.sum(lum["proj_col_tot"][ok],axis=0) / len(z[ok])
if psf_convolve:
    lum["proj_rec_tot_conv"] = np.sum(lum["proj_rec_tot_conv"][ok],axis=0) / len(z[ok])
    lum["proj_col_tot_conv"] = np.sum(lum["proj_col_tot_conv"][ok],axis=0) / len(z[ok])

temp = np.zeros_like(lum["proj_rec_tot"])
for i in range(len(rmid)):
    temp[i] += np.median(lum["proj_tot_median"][ok][:,i])
lum["proj_tot_median"] = temp
    
for name in names:

    lum["proj_rec_"+name+"_ism"] = mp_group["proj_rec_"+name+"_phys"][:][0]
    lum["proj_rec_"+name+"_cgm"] = mp_group["proj_rec_"+name+"_phys"][:][1]
    lum["proj_rec_"+name+"_sat"] = mp_group["proj_rec_"+name+"_phys"][:][2]

    lum["proj_col_"+name+"_ism"] = mp_group["proj_col_"+name+"_phys"][:][0]
    lum["proj_col_"+name+"_cgm"] = mp_group["proj_col_"+name+"_phys"][:][1]
    lum["proj_col_"+name+"_sat"] = mp_group["proj_col_"+name+"_phys"][:][2]

    if psf_convolve:
        
        lum["proj_rec_"+name+"_ism_conv"] =np.zeros((len(z),974))
        lum["proj_rec_"+name+"_cgm_conv"] =np.zeros((len(z),974))
        lum["proj_rec_"+name+"_sat_conv"] =np.zeros((len(z),974))

        lum["proj_col_"+name+"_ism_conv"] =np.zeros((len(z),974))
        lum["proj_col_"+name+"_cgm_conv"] =np.zeros((len(z),974))
        lum["proj_col_"+name+"_sat_conv"] =np.zeros((len(z),974))

    
    # Conversion into surface brightness
    for i, z_i in enumerate(z):
        dL, dA = uc.Luminosity_Angular_Distances(z_i,little_h,omega_0,lambda_0)

        cm2Mpc = 3.0857*10**24
        arcsec2radian = 4.84814*10**-6
        con2arcsec = 1 / (dA*1e3) / arcsec2radian

        # 22/2/21 - In the accepted version of the paper this was the line
        #conversion = 1/(4*np.pi*(dL*cm2Mpc)**2) / ((4*np.pi*(rmid+0.5*drad)**2 - 4*np.pi*(rmid-0.5*drad)**2) *con2arcsec**2)
        # Which should have been
        conversion = 1/(4*np.pi*(dL*cm2Mpc)**2) / ((np.pi*(rmid+0.5*drad)**2 - np.pi*(rmid-0.5*drad)**2) *con2arcsec**2)
        # I caught this bug higher up in the total SB profile calculation - I guess I just renormalise later on?
        # Yes - I think this happens because of the "tracer_sample_select" block below, which normalises these to total (bug-fixed) SB profile.
        # This helps to explain the puzzle of why the trijective/bijective matching rates seemed so low. (I guess)
        
        lum["proj_rec_"+name+"_ism"][i] *= conversion # erg s^-1 cm^-2 arcsec^-2
        lum["proj_rec_"+name+"_cgm"][i] *= conversion # erg s^-1 cm^-2 arcsec^-2
        lum["proj_rec_"+name+"_sat"][i] *= conversion # erg s^-1 cm^-2 arcsec^-2

        lum["proj_col_"+name+"_ism"][i] *= conversion # erg s^-1 cm^-2 arcsec^-2
        lum["proj_col_"+name+"_cgm"][i] *= conversion # erg s^-1 cm^-2 arcsec^-2
        lum["proj_col_"+name+"_sat"][i] *= conversion # erg s^-1 cm^-2 arcsec^-2

        if psf_convolve:
            lambda_obs = lambda0 * (1+z_i) # Observer frame wvl of lya
            fwhm = fwhm_f(lambda_obs) # Arcsec

            rmid_arcsec_coarse = rmid / 7.904 # arcsec
            drad_arcsec_coarse = drad / 7.904 # arcsec

            # Use a finer grid for convolution - via interpolation
            rmid_arcsec = np.arange(26,1000) * (rmid_arcsec_coarse[-1]/(1000))
            rmid_fine = rmid_arcsec * 7.904 # pkpc
            nrad = len(rmid_arcsec)
            drad_arcsec = rmid_arcsec[1]-rmid_arcsec[0]

            r_pos = np.arange(0,nrad)*drad_arcsec
            moff_pos = moffat(r_pos, fwhm,beta)
            r_neg = np.arange(-nrad,0)*drad_arcsec
            moff_neg = moffat(r_neg, fwhm,beta)

            moff_shift = np.concatenate((moff_pos,moff_neg)) # arcsec
            moff_shift *= 1./np.sum(moff_shift)

            lum_i = us.Linear_Interpolation(rmid_arcsec_coarse, lum["proj_rec_"+name+"_ism"][i], rmid_arcsec)
            lum_i = np.concatenate((lum_i[::-1], lum_i)) # Need to make profile symmetric for FFT
            lum["proj_rec_"+name+"_ism_conv"][i] = np.fft.irfft( np.fft.rfft(lum_i) * np.fft.rfft(moff_shift) )[974:]

            lum_i = us.Linear_Interpolation(rmid_arcsec_coarse, lum["proj_rec_"+name+"_cgm"][i], rmid_arcsec)
            lum_i = np.concatenate((lum_i[::-1], lum_i)) # Need to make profile symmetric for FFT
            lum["proj_rec_"+name+"_cgm_conv"][i] = np.fft.irfft( np.fft.rfft(lum_i) * np.fft.rfft(moff_shift) )[974:]

            lum_i = us.Linear_Interpolation(rmid_arcsec_coarse, lum["proj_rec_"+name+"_sat"][i], rmid_arcsec)
            lum_i = np.concatenate((lum_i[::-1], lum_i)) # Need to make profile symmetric for FFT
            lum["proj_rec_"+name+"_sat_conv"][i] = np.fft.irfft( np.fft.rfft(lum_i) * np.fft.rfft(moff_shift) )[974:]

            lum_i = us.Linear_Interpolation(rmid_arcsec_coarse, lum["proj_col_"+name+"_ism"][i], rmid_arcsec)
            lum_i = np.concatenate((lum_i[::-1], lum_i)) # Need to make profile symmetric for FFT
            lum["proj_col_"+name+"_ism_conv"][i] = np.fft.irfft( np.fft.rfft(lum_i) * np.fft.rfft(moff_shift) )[974:]

            lum_i = us.Linear_Interpolation(rmid_arcsec_coarse, lum["proj_col_"+name+"_cgm"][i], rmid_arcsec)
            lum_i = np.concatenate((lum_i[::-1], lum_i)) # Need to make profile symmetric for FFT
            lum["proj_col_"+name+"_cgm_conv"][i] = np.fft.irfft( np.fft.rfft(lum_i) * np.fft.rfft(moff_shift) )[974:]

            lum_i = us.Linear_Interpolation(rmid_arcsec_coarse, lum["proj_col_"+name+"_sat"][i], rmid_arcsec)
            lum_i = np.concatenate((lum_i[::-1], lum_i)) # Need to make profile symmetric for FFT
            lum["proj_col_"+name+"_sat_conv"][i] = np.fft.irfft( np.fft.rfft(lum_i) * np.fft.rfft(moff_shift) )[974:]

    # Temporal stacking
    lum["proj_rec_"+name+"_ism"] = np.sum(lum["proj_rec_"+name+"_ism"][ok],axis=0) / len(z[ok])
    lum["proj_rec_"+name+"_cgm"] = np.sum(lum["proj_rec_"+name+"_cgm"][ok],axis=0) / len(z[ok])
    lum["proj_rec_"+name+"_sat"] = np.sum(lum["proj_rec_"+name+"_sat"][ok],axis=0) / len(z[ok])

    lum["proj_col_"+name+"_ism"] = np.sum(lum["proj_col_"+name+"_ism"][ok],axis=0) / len(z[ok])
    lum["proj_col_"+name+"_cgm"] = np.sum(lum["proj_col_"+name+"_cgm"][ok],axis=0) / len(z[ok])
    lum["proj_col_"+name+"_sat"] = np.sum(lum["proj_col_"+name+"_sat"][ok],axis=0) / len(z[ok])

    if psf_convolve:
        lum["proj_rec_"+name+"_ism_conv"] = np.sum(lum["proj_rec_"+name+"_ism_conv"][ok],axis=0) / len(z[ok])
        lum["proj_rec_"+name+"_cgm_conv"] = np.sum(lum["proj_rec_"+name+"_cgm_conv"][ok],axis=0) / len(z[ok])
        lum["proj_rec_"+name+"_sat_conv"] = np.sum(lum["proj_rec_"+name+"_sat_conv"][ok],axis=0) / len(z[ok])

        lum["proj_col_"+name+"_ism_conv"] = np.sum(lum["proj_col_"+name+"_ism_conv"][ok],axis=0) / len(z[ok])
        lum["proj_col_"+name+"_cgm_conv"] = np.sum(lum["proj_col_"+name+"_cgm_conv"][ok],axis=0) / len(z[ok])
        lum["proj_col_"+name+"_sat_conv"] = np.sum(lum["proj_col_"+name+"_sat_conv"][ok],axis=0) / len(z[ok])
            
if tracer_sample_correct:
    lum_rec_matched = np.zeros_like(lum["proj_rec_1st_infall_ism"])
    lum_col_matched = np.zeros_like(lum["proj_rec_1st_infall_ism"])
    g_names = ["_ism","_cgm","_sat"]
    for name in names:
        for g_name in g_names:
            lum_rec_matched += lum["proj_rec_"+name+g_name]
            lum_col_matched += lum["proj_col_"+name+g_name]

    # Choose whether to apply the correction globally or per radius bin - this choice seems to make virtually no perceptible difference
    #correction_rec = np.sum(lum["proj_rec_tot"]) / np.sum(lum_rec_matched)
    #correction_col = np.sum(lum["proj_col_tot"]) / np.sum(lum_col_matched)
    correction_rec = lum["proj_rec_tot"] / lum_rec_matched
    correction_col = lum["proj_col_tot"] / lum_col_matched
    
    print "Applying a correction factor to rec/col lum of", correction_rec, correction_col, "to account for photons that couldn't be matched to tracer particles"

    #print lum["proj_rec_tot"] / lum_rec_matched
    #print lum["proj_col_tot"] / lum_col_matched
    #quit()
    
    for name in names:
        for g_name in g_names:
            lum["proj_rec_"+name+g_name] *= correction_rec
            lum["proj_col_"+name+g_name] *= correction_col
            
from utilities_plotting import *

nrow = 2; ncol=1
py.rcParams.update(latexParams)
py.rcParams.update({'figure.subplot.wspace':0.15,'figure.subplot.hspace':0.,
                    'figure.figsize':[3.32*ncol,2.49*nrow*1.05],
                    'figure.subplot.left':0.18,
                    'figure.subplot.bottom':0.09,
                    'figure.subplot.top':0.925,
                    'figure.subplot.right':0.95,
                    'legend.fontsize':5,
                    'font.size':9,
                    'axes.labelsize':9})

py.figure()
subplots = panelplot(nrow,ncol)
for n,ax in enumerate(subplots):
    py.axes(ax)

    ok = rmid <= rmax_rascas
    py.plot(rmid[ok], np.log10(lum["proj_rec_tot"]+lum["proj_col_tot"])[ok],c="k",label="Total",linewidth=1.5)

    # I realise this is not a median in the observations sense
    # Since a single bright satellite will be smeared over all projected radii in the angle-averaging
    # As such this line is useless
    #py.plot(rmid[ok], np.log10(lum["proj_tot_median"])[ok],c="g",label="Median",linewidth=1.5)

    ok = rmid_fine <= rmax_rascas
    py.plot(rmid_fine[ok], np.log10(lum["proj_rec_tot_conv"]+lum["proj_col_tot_conv"])[ok],c="k",label="Total (PSF-convolved)",linewidth=1.5,linestyle='--')

    
    if n == 0:
        rmax = min(rmax_rascas, rmax_ok)
        ok = rmid <= rmax
        ism = lum["proj_rec_ism_ism"]+lum["proj_rec_ism_cgm"]+lum["proj_rec_ism_sat"]+lum["proj_col_ism_ism"]+lum["proj_col_ism_cgm"]+lum["proj_col_ism_sat"]
        py.plot(rmid[ok], np.log10(ism)[ok],c="orange",label="ISM")

        finf = lum["proj_rec_1st_infall_ism"]+lum["proj_rec_1st_infall_cgm"]+lum["proj_rec_1st_infall_sat"]+lum["proj_col_1st_infall_ism"]+lum["proj_col_1st_infall_cgm"]+lum["proj_col_1st_infall_sat"]
        py.plot(rmid[ok], np.log10(finf)[ok],c="b",label="First infall")

        orbit = lum["proj_rec_orbit_ism"]+lum["proj_rec_orbit_cgm"]+lum["proj_rec_orbit_sat"]+lum["proj_col_orbit_ism"]+lum["proj_col_orbit_cgm"]+lum["proj_col_orbit_sat"]
        py.plot(rmid[ok], np.log10(orbit)[ok],c="g",label="Post-pericentrer")

        stripped = lum["proj_rec_stripped_ism"]+lum["proj_rec_stripped_cgm"]+lum["proj_rec_stripped_sat"]+lum["proj_col_stripped_ism"]+lum["proj_col_stripped_cgm"]+lum["proj_col_stripped_sat"]
        py.plot(rmid[ok], np.log10(stripped)[ok],c="c",label="Stripped")

        fb = lum["proj_rec_fb_ism"]+lum["proj_rec_fb_cgm"]+lum["proj_rec_fb_sat"]+lum["proj_col_fb_ism"]+lum["proj_col_fb_cgm"]+lum["proj_col_fb_sat"]
        py.plot(rmid[ok], np.log10(fb)[ok],c="r",label="Feedback influenced")

        sat = lum["proj_rec_sat_ism"]+lum["proj_rec_sat_cgm"]+lum["proj_rec_sat_sat"]+lum["proj_col_sat_ism"]+lum["proj_col_sat_cgm"]+lum["proj_col_sat_sat"]
        py.plot(rmid[ok], np.log10(sat)[ok],c="y",label="Satellites")

    if n == 1:
        rmax = min(rmax_rascas, rmax_ok)
        ok = rmid <= rmax
        from_ism = lum["proj_rec_ism_ism"] +lum["proj_col_ism_ism"] + lum["proj_rec_1st_infall_ism"] +lum["proj_col_1st_infall_ism"] +lum["proj_rec_orbit_ism"] +lum["proj_col_orbit_ism"]
        from_ism += lum["proj_rec_stripped_ism"] + lum["proj_col_stripped_ism"] + lum["proj_rec_fb_ism"] +lum["proj_col_fb_ism"] +lum["proj_rec_sat_ism"] +lum["proj_col_sat_ism"]

        from_cgm = lum["proj_rec_ism_cgm"] +lum["proj_col_ism_cgm"] + lum["proj_rec_1st_infall_cgm"] +lum["proj_col_1st_infall_cgm"] +lum["proj_rec_orbit_cgm"] +lum["proj_col_orbit_cgm"]
        from_cgm += lum["proj_rec_stripped_cgm"] + lum["proj_col_stripped_cgm"] + lum["proj_rec_fb_cgm"] +lum["proj_col_fb_cgm"] +lum["proj_rec_sat_cgm"] +lum["proj_col_sat_cgm"]

        from_sat = lum["proj_rec_ism_sat"] +lum["proj_col_ism_sat"] + lum["proj_rec_1st_infall_sat"] +lum["proj_col_1st_infall_sat"] +lum["proj_rec_orbit_sat"] +lum["proj_col_orbit_sat"]
        from_sat += lum["proj_rec_stripped_sat"] + lum["proj_col_stripped_sat"] + lum["proj_rec_fb_sat"] +lum["proj_col_fb_sat"] +lum["proj_rec_sat_sat"] +lum["proj_col_sat_sat"]

        py.plot(rmid[ok],np.log10(from_ism)[ok],c="k",linestyle='--',label="From ISM")
        py.plot(rmid[ok],np.log10(from_cgm)[ok],c="k",linestyle='-.',label="From CGM")
        py.plot(rmid[ok],np.log10(from_sat)[ok],c="k",linestyle=':',label="From satellites")

    rad_W18, logSB_W18 = np.loadtxt("lutz_data.dat",unpack=True)
    py.scatter(rad_W18, logSB_W18, marker = "x",label="Wisotzki et al. (2018)")

    # To be consistent with how this is done in Lutz's paper, we would need to integrate out to larger radii than what I computed here, compensate via this fudge factor
    fudge = 1.15
    ok = np.isnan(lum["proj_rec_tot_conv"]+lum["proj_col_tot_conv"]) == False
    PSF_norm = np.sum((lum["proj_rec_tot_conv"]+lum["proj_col_tot_conv"])[ok])/ np.sum(PSF[ok])  /fudge
    #PSF_norm = np.sum(10**logSB_W18)/ np.sum(PSF[ok])
    if n == 0:
        py.plot(rmid_fine[ok], np.log10(PSF[ok]*PSF_norm), c="k",linestyle=":",label="Point source")

    if n == 0:
        hl = 3
    else:
        hl = 3


    if n == 0:
        handles, labels = py.gca().get_legend_handles_labels()
        order = [0,1,2,3,4,5,6,7,9,8]

        handles_sort = []
        labels_sort = []
        for idx in order:
            handles_sort.append(handles[idx])
            labels_sort.append(labels[idx])
        py.legend(handles_sort,labels_sort, frameon=False,loc="upper right", numpoints=1, scatterpoints=1, handlelength=hl)
    else:
        py.legend(frameon=False,loc="upper right",numpoints=1,scatterpoints=1,handlelength=hl)

    py.xlabel(r"$r_{\mathrm{projected}} \, / \mathrm{kpc}$")
    py.ylabel(r"$\log_{10}(\mathrm{d}F / \mathrm{d}A \, / \mathrm{erg s^{-1} cm^{-2} arcsec^{-2}})$")

    py.xlim((0,40))
    ylo = -22
    if i == 0:
        py.ylim((-22,-17))
    else:
        py.ylim((-22,-17.1))

    #py.axvline(rmax_ok,c="k",alpha=0.3,linestyle='--')

    # Mark location of R_200 at z=3.5
    #py.axvline(r200_3p5, c="k", alpha=0.5)
    py.plot([r200_3p5,r200_3p5], [ylo,ylo+0.3], c="k", alpha=0.5)
        
    if n == 0:
        ax_z = ax.twiny()
        ax_z.set_xticks(t_ticks)
        ax_z.set_xticklabels(tick_labels)
        ax_z.set_xlabel(r'$r(z=3.5) \, / \mathrm{arcsec}$')
        ax_z.set_xlim((0,40))
        
        for tick in ax.xaxis.get_major_ticks():
            tick.label1On=False

fig_name = "Lya_projected_SB_stacked_lagrangian_components_"+Zoom+"_"+Run+".pdf"
py.savefig("../Figures/"+fig_name)
py.show()
