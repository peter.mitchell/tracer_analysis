import numpy as np
import h5py
import utilities_cosmology as uc
import dm_utils as dm
import utilities_ramses as ur
import ldecomp_functions as ldf
import sys
sys.path.append("../.")
import match_searchsorted as ms

'''Zoom = "Zoom-7-1290"
Run = "tracer_run_lr"
RunDir = "/cral4/mitchell/data/Tracer_Test/"+ Zoom + "/" + Run
OutDir = "/cral2/mitchell/PostProcess/"
InDir = OutDir
RT = False
downsample = True
n_downsample = "1000"'''


Zoom = "Zoom-7-5092"
Run = "RhdRun-tracer_radfix"
RunDir = "/cral2/mitchell/data/5092_RT_Tracer/"+ Zoom + "/" + Run
OutDir = "/cral2/mitchell/data/5092_RT_Tracer/PostProcess/"
InDir = OutDir
RT = True
downsample = True
#n_downsample = "3000000.0"
n_downsample = "1000"

verbose = True

########## Figure out which input files of tracer trajectories we will need to read in ###########
if downsample:
    n_tr_samples = 1
    filename = "saved_tracers_select_"+Zoom+"_"+Run + "_ds_"+ n_downsample + ".hdf5"
    filenames_list = [filename]
    
else:
    import os
    files = os.listdir(InDir)
    filename_base = "saved_tracers_select_"+Zoom+"_"+Run

    filenames_list = []
    for file_n in files:
        if filename_base in file_n and "subset" in file_n:
            filenames_list.append(file_n)
            n_tr_samples = len(filenames_list)

############### Loop over tracer sub-samples (to avoid running out of memory) ###########

for ifile, filename in enumerate(filenames_list):

    if not downsample:
        print "Reading/Computing for tracer subset", ifile+1, "of", len(filenames_list)

    tr_var_names = ["comp", "cv", "radius", "family", "ek", "eth"]

    File = h5py.File(InDir+filename)
    tracer_sample = ldf.tracer_sample_class(File,tr_var_names,verbose=True,RT=RT)
    File.close()
    
    # Get time from redshift
    dm.jeje_utils.read_conversion_scales(RunDir,tracer_sample.timestep[0])
    omega_0,lambda_0,little_h = dm.jeje_utils.read_cosmo_params(RunDir,tracer_sample.timestep[0])
    a_in = 1./(1.+tracer_sample.redshift)
    tracer_sample.t, tracer_sample.tlb = uc.t_Universe(a_in,omega_0,little_h)

    # Block for helping to pin down the origin of the weird lya luminosity spikes
    '''print "temp hack"
    first_infall_ts = ldf.Identify_First_Infall(tracer_sample)
    tracer_sample.add_var(first_infall_ts,"first_infall_ts")
    end_in_sat_evo = ldf.End_In_Sat(tracer_sample) # Remove lines later

    for i_ts in range(len(tracer_sample.t)):
        LyaLum = tracer_sample.tr_vars["RecLyaLum"][i_ts] + tracer_sample.tr_vars["CollLyaLum"][i_ts]
        comp = tracer_sample.tr_vars["comp"][i_ts]
        ok =  (tracer_sample.tr_vars["comp"][i_ts] == 0) & (np.isnan(tracer_sample.tr_vars["comp"][i_ts])==False)
        ok2 = (tracer_sample.tr_vars["radius"][i_ts] > 0.2) & (tracer_sample.tr_vars["radius"][i_ts] < 1.0) & (tracer_sample.tr_vars["family"][i_ts] == 100)
        ok3 = end_in_sat_evo[i_ts] == False
        
        print i_ts, np.sum(LyaLum[ok&ok2]), np.sum(LyaLum[ok2]), np.sum(LyaLum[ok2&ok3])
    continue'''

    n_select = len(tracer_sample.tr_vars["tid"])
    
    n_part = tracer_sample.n_part()
    n_ts = tracer_sample.n_ts()
    
    # First task is to identify particles that were accreted into the virial radius for the first time inside a satellite
    if verbose:
        print "Computing when tracers first entered the virial radius"
    first_infall_ts = ldf.Identify_First_Infall(tracer_sample,verbose=verbose)
    tracer_sample.add_var(first_infall_ts,"first_infall_ts")
    
    # Drop out particles that never enter the virial radius for this final timestep
    ok = (np.isnan(first_infall_ts) == False)
    tracer_sample.select_bool(ok)
    
    n_part = tracer_sample.n_part()

    if verbose:
        print "Computing if tracers were in satellite when they entered"

    # Now identify particles that were in a satellite when they entered the virial radius
    enter_in_sat = ldf.Enter_In_Sat(tracer_sample,verbose=verbose)
    tracer_sample.add_var(enter_in_sat,"enter_in_sat")
    
    if verbose:
        print "Computing if tracers are in satellites (at all steps)"

    # Now identify particles that are in a satellite at the final steps #
    end_in_sat_evo = ldf.End_In_Sat(tracer_sample,verbose=verbose)
    tracer_sample.add_var(end_in_sat_evo,"end_in_sat_evo")

    if verbose:
        print "Computing tracers that have been influenced by feedback"

    #fb_flag_evo, ts_fb_flag = ldf.Feedback_Influence_Slow(tracer_sample) # Now outdated version that contained a double for loop
    fb_flag_evo, ts_fb_flag = ldf.Feedback_Influence(tracer_sample,verbose=verbose)
    
    tracer_sample.add_var(fb_flag_evo, "fb_flag_evo")
    tracer_sample.add_var(ts_fb_flag, "ts_fb_flag")

    if verbose:
        print "Computing orbital stage"

    part_orb_type_evo = ldf.Identify_Kine_Episode(tracer_sample,verbose=verbose)
    tracer_sample.add_var(part_orb_type_evo,"part_orb_type_evo")

    ###### Write the Lagrangian tracer flags to disk #########

    if verbose:
        print "De-scoping to try and reduce memory usage at write step"

    output_names = ["fb_flag_evo","ts_fb_flag", "end_in_sat_evo", "enter_in_sat", "first_infall_ts", "part_orb_type_evo"]
    tracer_sample.descope_vars(output_names + ["tid"]) # De-scope data apart from the data whose names are passed in.

    if verbose:
        print "Relating pruned sample back to full sample, and writing to disk"
    File = h5py.File(InDir+filename)

    # Delete any previous data
    if verbose:
        print "Deleting any pre-existing Lagrangian status data"
        for output_name in output_names:
            try:
                del File[output_name]
            except:
                pass
    
    # Now we need to relate back to the original tracer sample (before we cut away things that never entered the virial radius)
    tid = File["tid"][:]
    ptr = ms.match(tid, tracer_sample.tr_vars["tid"])
    ok_match = ptr>=0
    
    for output_name in output_names:
        print "Writing", output_name
        if len(tracer_sample.tr_vars[output_name].shape) > 1:
            array = np.zeros((n_ts,len(tid)))
            array[:,ok_match] = tracer_sample.tr_vars[output_name][:,ptr][:,ok_match]
        else:
            array = np.zeros(len(tid))
            array[ok_match] = tracer_sample.tr_vars[output_name][ptr][ok_match]
        File.create_dataset(output_name, data=array)

    File.close()
print "got to the end :-)"
