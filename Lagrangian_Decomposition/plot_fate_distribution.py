import numpy as np
import h5py
import utilities_cosmology as uc
import dm_utils as dm
import utilities_ramses as ur
import ldecomp_functions as ldf
import utilities_statistics as us
from utilities_plotting import *

Zoom = "Zoom-7-5092"
Run = "RhdRun-tracer_radfix"
downsample = False
n_downsample = "3e6"
#n_downsample = "1000"

z_choose = 4.75
# Choose which tracer selection to use
i_s = 0 # At present 0 is outflow, inner, 1 is inflow, outer, 2 is inflow, inner

path = "Fate_Distns/"
output_file = path + "fate_distn_"+Zoom+"_"+Run
if downsample:
    output_file += "_"+n_downsample
output_file += ".hdf5"

File = h5py.File(output_file)

radius_lo = File["radius_lo"][:]
radius_med = File["radius_med"][:]
radius_hi = File["radius_hi"][:]

temp_lo = File["temp_lo"][:]
temp_med = File["temp_med"][:]
temp_hi = File["temp_hi"][:]

xhi_lo = File["xhi_lo"][:]
xhi_med = File["xhi_med"][:]
xhi_hi = File["xhi_hi"][:]

nh_lo = File["nh_lo"][:]
nh_med = File["nh_med"][:]
nh_hi = File["nh_hi"][:]

redshifts = File["redshifts"][:]
timesteps = File["timesteps"][:]
t = File["time"][:]
File.close()

i_ch = np.argmin(abs(redshifts-z_choose))

xlo = t[np.where(radius_lo[0]<=1)[0][0]]
xhi = t.max()


nrow = 2; ncol = 2
py.rcParams.update(latexParams)
py.rcParams.update({'figure.subplot.wspace':0.12,'figure.subplot.hspace':0,
                    'figure.figsize':[3.32*ncol,2.49*nrow],
                    'figure.subplot.left':0.09,
                    'figure.subplot.bottom':0.18/nrow,
                    'figure.subplot.top':0.96,
                    'legend.fontsize':6,
                    'font.size':12,
                    'axes.labelsize':11})

py.figure()
subplots = panelplot(nrow,ncol)
for n,ax in enumerate(subplots):
    py.axes(ax)

    py.axvline(t[i_ch],c="k")

    if n == 0:
        py.plot(t, radius_med[i_s],c="k")
        py.plot(t, radius_lo[i_s],c="k")
        py.plot(t, radius_hi[i_s],c="k")
        py.fill_between(t, radius_lo[i_s], radius_hi[i_s],color="r",alpha=0.2)
        py.ylim((0.0, 1.0))
        py.ylabel(r"$r \, / R_{\mathrm{vir}}$")

    if n == 1:
        py.plot(t, np.log10(xhi_med[i_s]),c="k")
        py.plot(t, np.log10(xhi_lo[i_s]),c="k")
        py.plot(t, np.log10(xhi_hi[i_s]),c="k")
        py.fill_between(t, np.log10(xhi_lo[i_s]), np.log10(xhi_hi[i_s]),color="r",alpha=0.2)
        py.ylim((-1.5, 0.1))
        py.ylabel(r"$\log(x_{\mathrm{HI}})$")
    
    if n ==2 :
        py.plot(t, np.log10(temp_med[i_s]),c="k")
        py.plot(t, np.log10(temp_lo[i_s]),c="k")
        py.plot(t, np.log10(temp_hi[i_s]),c="k")
        py.fill_between(t, np.log10(temp_lo[i_s]), np.log10(temp_hi[i_s]),color="r",alpha=0.2)
        py.ylim((2.0, 5.99))
        py.ylabel(r"$\log(T \, / \mathrm{K})$")
    
    if n == 3:
        py.plot(t, np.log10(nh_med[i_s]),c="k")
        py.plot(t, np.log10(nh_lo[i_s]),c="k")
        py.plot(t, np.log10(nh_hi[i_s]),c="k")
        py.fill_between(t, np.log10(nh_lo[i_s]), np.log10(nh_hi[i_s]),color="r",alpha=0.2)
        py.ylim((-3.0, 3.95))
        py.ylabel(r"$\log(n_{\mathrm{H}} \, / \mathrm{cm^{-3}})$")
        
    if n >= ncol*nrow- ncol:
        py.xlabel(r"$t \, / \mathrm{Gyr}$")
        
    py.xlim((xlo,xhi))


fig_name = "fate_distribution_select_"+str(i_s)+"_"+Zoom+"_"+Run+".pdf"
py.savefig("../Figures/"+fig_name)
py.show()
