import numpy as np
import h5py
from utilities_plotting import *
import dm_utils as dm

#Zoom = "Zoom-7-5092"
#Run = "RhdRun-tracer_norad_bug"
#timestep_final = 105
#OutDir = "/cral4/mitchell/data/Tracer_Test/PostProcess/"

Zoom = "Zoom-7-5092"
Run = "RhdRun-tracer_radfix"
timestep_final = 107
OutDir = "/cral2/mitchell/PostProcess/"

downsample = True
downsample_correction = 31400640 / (3.0*10**6) # Appropriate for Rfix 3e6 ds 

# Choose redshifts to mean stack over
zlo = 4
zhi = 6

RunDir = "/cral2/mitchell/" + Zoom +"/"+Run

nrow = 2; ncol = 2

xlo = 0.0; xhi = 0.99

dm.jeje_utils.read_conversion_scales(RunDir,timestep_final)
    
omega_0,lambda_0,little_h = dm.jeje_utils.read_cosmo_params(RunDir,timestep_final)
omega_b = dm.jeje_utils.get_param_real(RunDir,timestep_final,"omega_b")
fb = omega_b / omega_0
    
all_redshifts = np.zeros(timestep_final-1)
for n, timestep in enumerate(np.arange(2,timestep_final+1)):
    all_redshifts[n] = dm.jeje_utils.get_snap_redshift(RunDir,timestep)


# Read the hdf5 file
filename = Zoom + "_" + Run + "_tsf" + str(timestep_final) +".hdf5"
File = h5py.File(OutDir+filename)
print OutDir+filename
zoom_group = File[Zoom]
print filename, Zoom

for halo_group_str in zoom_group:
    # Check this is a halo
    if "_main" in halo_group_str:
        break

halo_group = zoom_group[halo_group_str]
mp_group = halo_group["main_progenitors"]

flux_str   = "fluxes/flux_history"
flux_group = halo_group[flux_str]

redshifts = flux_group["redshift"][:]

example = mp_group["lagrangian_exchange_neut_out_neut_out_ps"][:]

if len(redshifts) != len(example[:,0]):
    print "error: redshift grid and exchange rate grid not the same"
    quit()
    
okz = (redshifts <= zhi) & (redshifts >= zlo)

# neutral outflow > component
panel1_list = ["neut_out_neut_out_ps", "neut_out_wion_out_ps", "neut_out_hion_out_ps", "neut_out_tot_inflow_ps"]
panel1_labels = [r"$\mathrm{\bf{Neutral \, outflow}} \, > \, \mathrm{Neutral \, outflow}$",\
                 r"$\mathrm{\bf{Neutral \, outflow}} \, > \, \mathrm{Warm \, outflow}$",\
                 r"$\mathrm{\bf{Neutral \, outflow}} \, > \, \mathrm{Hot \, outflow}$",\
                 r"$\mathrm{\bf{Neutral \, outflow}} \, > \, \mathrm{Inflow}$"]

panel_c_list = ["b","g","m","k"]

panel1_data = []
for quant in panel1_list:
    panel1_data.append( mp_group["lagrangian_exchange_" + quant][:][okz] )

# component > neutral outflow
panel2_list = ["neut_out_neut_out_ts", "wion_out_neut_out_ts", "hion_out_neut_out_ts", "all_in_neut_out_ts"]
panel2_labels = [r"$\mathrm{Neutral \, outflow} \, > \, \mathrm{\bf Neutral \, outflow}$",\
                 r"$\mathrm{Warm \, outflow} \, > \, \mathrm{\bf Neutral \, outflow}$",\
                 r"$\mathrm{Hot \, outflow} \, > \, \mathrm{\bf Neutral \, outflow}$",\
                 r"$\mathrm{Inflow} \, > \, \mathrm{\bf Neutral \, outflow}$"]

panel2_data = []
for quant in panel2_list:
    if not "all_" in quant:
        panel2_data.append( mp_group["lagrangian_exchange_" + quant][:][okz] )
    else:
        if "all_in_" in quant:
            quant2 = quant[7:]
            panel2_data.append( mp_group["lagrangian_exchange_neut_in_"+quant2][:][okz] +\
                                mp_group["lagrangian_exchange_wion_in_"+quant2][:][okz] +\
                                mp_group["lagrangian_exchange_hion_in_"+quant2][:][okz])

# neutral inflow > component
panel3_list = ["neut_in_neut_in_ps", "neut_in_wion_in_ps", "neut_in_hion_in_ps", "neut_in_tot_outflow_ps"]
panel3_labels = [r"$\mathrm{\bf{Neutral \, inflow}} \, > \, \mathrm{Neutral \, inflow}$",\
                 r"$\mathrm{\bf{Neutral \, inflow}} \, > \, \mathrm{Warm \, inflow}$",\
                 r"$\mathrm{\bf{Neutral \, inflow}} \, > \, \mathrm{Hot \, inflow}$",\
                 r"$\mathrm{\bf{Neutral \, inflow}} \, > \, \mathrm{Outflow}$"]

panel3_data = []
for quant in panel3_list:
    panel3_data.append( mp_group["lagrangian_exchange_" + quant][:][okz] )
    

# component > neutral inflow
panel4_list = ["neut_in_neut_in_ts", "wion_in_neut_in_ts", "hion_in_neut_in_ts", "all_out_neut_in_ts"]
panel4_labels = [r"$\mathrm{Neutral \, inflow} \, > \, \mathrm{\bf Neutral \, inflow}$",\
                 r"$\mathrm{Warm \, inflow} \, > \, \mathrm{\bf Neutral \, inflow}$",\
                 r"$\mathrm{Hot \, inflow} \, > \, \mathrm{\bf Neutral \, inflow}$",\
                 r"$\mathrm{Outflow} \, > \, \mathrm{\bf Neutral \, inflow}$"]

panel4_data = []
for quant in panel4_list:
    if not "all_" in quant:
        panel4_data.append( mp_group["lagrangian_exchange_" + quant][:][okz] )
    else:
        if "all_out_" in quant:
            quant2 = quant[8:]
            panel4_data.append( mp_group["lagrangian_exchange_neut_out_"+quant2][:][okz] +\
                                mp_group["lagrangian_exchange_wion_out_"+quant2][:][okz] +\
                                mp_group["lagrangian_exchange_hion_out_"+quant2][:][okz])

File.close()

py.rcParams.update(latexParams)
py.rcParams.update({'figure.subplot.wspace':0,'figure.subplot.hspace':0,
                    'figure.figsize':[3.32*ncol,2.49*nrow],
                    'figure.subplot.left':0.24/ncol,
                    'figure.subplot.bottom':0.18/nrow,
                    'figure.subplot.top':0.96,
                    'legend.fontsize':6,
                    'font.size':12,
                    'axes.labelsize':11})

py.figure()
subplots = panelplot(nrow,ncol)
for n,ax in enumerate(subplots):
    py.axes(ax)

    # Stack different radii in time
    r_bins = np.arange(0.0,1.1,0.1)
    #r_bins = np.array([0.2,0.6,1.0])
    r_mid = 0.5 * (r_bins[1:] + r_bins[0:-1])

    if n == 0:
        panel_list = panel1_list; panel_data = panel1_data; panel_labels = panel1_labels
    elif n == 1:
        panel_list = panel2_list; panel_data = panel2_data; panel_labels = panel2_labels
    elif n == 2:
        panel_list = panel3_list; panel_data = panel3_data; panel_labels = panel3_labels
    else:
        panel_list = panel4_list; panel_data = panel4_data; panel_labels = panel4_labels
        
    for i in range(len(panel_list)):
        quant = np.zeros_like(r_bins[1:])
                   
        for i_r in range(len(r_bins[1:])):
            
            # Then stack outputs at a given radius
            ok = np.isnan(panel_data[i][:,i_r])==False
            quant[i_r] = np.sum( panel_data[i][:,i_r][ok] ) / len(panel_data[i][:,i_r][ok])

        # If I subsampled the tracers, correct for this now
        if downsample:
            quant *= downsample_correction
            
        py.plot( r_mid, np.log10(quant), c=panel_c_list[i],label=panel_labels[i])
            
    if n >= ncol*nrow- ncol:
        py.xlabel(r"$r \, / R_{\mathrm{vir}}$")
    if n % ncol == 0:
        py.ylabel(r"$\log_{10}\left(\frac{\mathrm{d}}{\mathrm{d} (r/r_{\mathrm{vir}})} \, \frac{\mathrm{d} m}{\mathrm{d} t} \, / \mathrm{M_\odot yr{-1}}\right)$")

    py.xlim((xlo,xhi))
    ylo = -2.99; yhi = 2
    py.ylim((ylo,yhi))

    py.legend(loc="upper right",frameon=False,numpoints=1,scatterpoints=1)

fig_name = "exchange_rates_neut_lagrangian_tstack_"+Zoom+"_"+Run+".pdf"
py.savefig("../Figures/"+fig_name)
py.show()
