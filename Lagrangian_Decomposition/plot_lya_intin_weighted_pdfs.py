import argparse
import sys
import numpy as np
import time
import os

timestep = 141
#timestep = 10

Zoom = "Zoom-7-5092"
Run = "RhdRun-tracer_radfix"
RT = True
RunDir = "/cral2/mitchell/"+Zoom+"/"+Run
InDir = "/cral2/mitchell/PostProcess/"
OutDir = "/cral2/mitchell/PostProcess/"
timestep_final = 155

dump_dir = "/cral2/mitchell/PostProcess/Map_Dumps/"

subhalo_radius = "r_tidal"
scale_size = 10.0 # pkpc
scale_size_pkpc = scale_size

############ Define list of maps to write ###############################
map_size = 1.1 # Size in 1 dimension of the map in units of the virial radius
figure_size = 10 # Factor determining size of the figures
dpi_factor = 1.0 # Scaling factor to up/down scale the dpi (resolution)

import dm_utils as dm_utils
import cutils as cut
import h5py
import utilities_ramses as ur
from utilities_plotting import *



# Read in relevant tree information from the hdf5 file
dump_file_path = dump_dir + "dump_"+Zoom+"_"+Run+"_ts"+str(timestep)+".hdf5"

    
print "reading halo catalogue"

filename = Zoom + "_" + Run + "_tsf" + str(timestep_final) +".hdf5"
File = h5py.File(OutDir+filename,"r")

# Check the file with the tree information already exists
if not Zoom in File:
    print "Error: Tree information for ", Zoom, " in ", filename, " cannot be read"
    exit()

zoom_group = File[Zoom]



timestep_tree = timestep-1

# Get conversions for different units
r2kpc, redshift, little_h, m2Msun,rho2msunpkpc3,v2kms = ur.Set_Units(RunDir, timestep)
boxlen_pkpc = dm_utils.jeje_utils.get_param_real(RunDir,timestep,'boxlen') * r2kpc # Box size in proper kpc, using the correct box size

print "redshift is", redshift

scale_size *= 1./r2kpc # code units



# Factor to help convert into box units (note maxime's merger_tree reader has converted halo positions so be careful here!)
r2kpc_ideal, redshift, little_h, m2Msun,junk,junk = ur.Set_Units(RunDir, timestep, "ideal")
boxlen_cMpc = dm_utils.jeje_utils.get_param_real(RunDir,timestep,'boxlen') # Box size in box units
boxlen_cMpc *= r2kpc_ideal
boxlen_cMpc = boxlen_cMpc * (1+redshift) /1e3 # cMpc (comoving)
boxlen_cMpc = boxlen_cMpc * little_h # h^-1 cMpc
factor = 1./boxlen_cMpc

# Loop over final timestep haloes
for halo_group_str in zoom_group:
    # Only write maps for main progenitors of the main halo
    if "_main" in halo_group_str:
        break

halo_group = zoom_group[halo_group_str]
mp_group = zoom_group[halo_group_str+"/main_progenitors"]

# Identify the main progenitor at this timestep
ok_ts = np.array(mp_group["halo_ts"])==timestep_tree # haloes on this timestep

print "done, computing cell quantities"


rvirh = mp_group["r200"][ok_ts] / r2kpc

########## Select cells in the vicinity of the halo (using a chain algorithm) ################

# Set cell unit conversions for this timestep
cut.jeje_utils.read_conversion_scales(RunDir,timestep)



print "Performing IO"
dump_file = h5py.File(dump_file_path,"r")


comp = dump_file["comp"][:]
x = dump_file["x"][:]
y = dump_file["y"][:]
z = dump_file["z"][:]
r = np.sqrt(np.square(x)+np.square(y)+np.square(z)) / rvirh

ok = ((comp == 3)|(comp == 0)) & (r > 0.2) & (r<1)

rho_snap_h = dump_file["rho"][:][ok]
pre_snap_h = dump_file["pre"][:][ok]
clev_h = dump_file["lev"][:][ok]

xH1_h = dump_file["xH1"][:][ok]
xHe1_h = dump_file["xHe1"][:][ok]
xHe2_h = dump_file["xHe2"][:][ok]






# Compute number density in atoms per cc
nh_h = rho_snap_h * cut.jeje_utils.dp_scale_d / cut.jeje_utils.mh * cut.jeje_utils.xh

# Compute temperature over effective molecular weight in Kelvin
scale_temk = (cut.jeje_utils.dp_scale_l / cut.jeje_utils.dp_scale_t)**2 * cut.jeje_utils.mh / cut.jeje_utils.kb
temp_mu_h = pre_snap_h/rho_snap_h * scale_temk # T/mu in K

if RT:
    xhi_h = (1-xH1_h)# Hydrogen neutral fraction
    X_frac=0.76
    mu_h = 1./( X_frac*(1.+xH1_h) + 0.25*(1.-X_frac)*(1.+xHe1_h+2.*xHe2_h) )
else:
    mu_h, xhi_h = cut.cell_utils.ramses_get_mu_xhi(RunDir,timestep,rho_snap_h, pre_snap_h, True)
temp_h = temp_mu_h * mu_h

print "Computing cell masses"
csize = boxlen_pkpc / 2.**clev_h # proper kpc
cmass = rho_snap_h * rho2msunpkpc3 * csize**3

cell_vol = np.power(csize,3) # kpc3
cell_vol *= (3.086e18*1e3)**3 # cm3  


Ta = np.copy(temp_h)
Ta[Ta<100] = 100 # Put temperature floor
prob_case_B = 0.686 - 0.106*np.log10(Ta/1e4) - 0.009*np.power(Ta/1e4,-0.44)
nHII = xH1_h*nh_h # Ionized hydrogen density
nHI = xhi_h * nh_h # Neutral hydrogen density
X_frac = 0.76
nHe  = 0.25*nh_h*(1.0-X_frac)/X_frac
n_e = nHII+nHe*(xHe1_h+2.*xHe2_h)

def get_AlphaB_HII(TK):
    lbda = 315614./TK
    return 2.753e-14 * np.power(lbda,1.5) / np.power(1.+np.power(lbda/2.74,0.407),2.242)

def get_collExrate_HI(TK):
    kb = 1.38064852e-16
    return 2.41e-6/np.sqrt(TK) * np.power(TK/1.e4,0.22) * np.exp(-1.63e-11/(kb*TK))

planck  = 6.626070040e-27  # [erg s] Planck's constant
clight  = 2.99792458e10  # cm/s
cmtoA   = 1.0e8 # cm to Angstrom
lambda_0=1215.67e0
lambda_0_cm = lambda_0 / cmtoA          # cm
nu_0 = clight / lambda_0_cm             #  Hz
e_lya = planck*nu_0 # 1.634e-11 # cgs

RecLyaEmi  = prob_case_B * nHII * n_e * get_AlphaB_HII(temp_h) * e_lya
CollLyaEmi = nHI * n_e * get_collExrate_HI(temp_h) * e_lya
LyaEmi     = CollLyaEmi + RecLyaEmi










print "plotting"

bin_nh = np.arange(-5,4,0.1)
py.figure()
py.subplot(211)

py.hist(np.log10(nh_h), bins=bin_nh,histtype="step", weights=cmass,color="k",linewidth=1.2,normed=True)
py.hist(np.log10(nh_h), bins=bin_nh,histtype="step", weights=RecLyaEmi,color="b",linewidth=1.2,normed=True)
py.hist(np.log10(nh_h), bins=bin_nh,histtype="step", weights=CollLyaEmi,color="r",linewidth=1.2,normed=True)

py.subplot(212)

bin_temp = np.arange(1,8,0.1)

py.hist(np.log10(temp_h), bins=bin_temp,histtype="step", weights=cmass,color="k",linewidth=1.2,normed=True,label="Mass")
py.hist(np.log10(temp_h), bins=bin_temp,histtype="step", weights=RecLyaEmi,color="b",linewidth=1.2,normed=True,label="Recombination")
py.hist(np.log10(temp_h), bins=bin_temp,histtype="step", weights=CollLyaEmi,color="r",linewidth=1.2,normed=True,label="Coll Ex")

py.legend()

py.show()
