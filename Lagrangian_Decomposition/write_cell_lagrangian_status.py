# This script:
# 1) reads in a sample of pre-processed tracers (with Lagrangian status already assigned)
# 2) links those tracers with cells (from one timestep only) from hdf5 dump
# 3) compute the Lagrangian status of each cell
# 4) dumps the data to temporary file

# Edit I'm pretty sure this script is used to (later) create maps for a single snapshot

import numpy as np

timestep = 141
#timestep = 10

Zoom = "Zoom-7-5092"
Run = "RhdRun-tracer_radfix"
timestep_final = 155
RT = True
RunDir = "/cral4/mitchell/data/5092_RT_Tracer/"+Zoom+"/"+Run
InDir = "/cral4/mitchell/data/5092_RT_Tracer/PostProcess/"
OutDir = "/cral4/mitchell/data/5092_RT_Tracer/PostProcess/"

downsample = False
n_downsample = "1000"
#n_downsample = "3000000.0"

if downsample:
    batch_no = 1
    
    filename = "saved_tracers_select_"+Zoom+"_"+Run + "_ds_"+ n_downsample + ".hdf5"
    filenames_list = [filename]
else:
    import os
    files = os.listdir(InDir)
    filename_base = "saved_tracers_select_"+Zoom+"_"+Run
    
    filenames_list = []
    for file_n in files:
        if filename_base in file_n and "subset" in file_n:
            filenames_list.append(file_n)
    batch_no = len(filenames_list)



HaloDir = RunDir + "/Halos/"
TreeDir = RunDir + "/Trees/"

import sys
sys.path.append("../.")
import dm_utils as dm
from scipy import spatial
import h5py
import utilities_ramses as ur
import neighbour_utils as nu
import match_searchsorted as ms
import cutils as cut
import flux_utils
sys.path.append("../Rascas_analysis/.")
import jphot as jp
import os

# Get conversions for different units
r2kpc, redshift, little_h, m2Msun,rho2msunpkpc3,v2kms = ur.Set_Units(RunDir, timestep)
boxlen_pkpc = dm.jeje_utils.get_param_real(RunDir,timestep,'boxlen') * r2kpc # Box size in proper kpc, using the correct box size

print "Dumping cell properties (including matched tracer and rascas props) for ",Zoom,Run,", z=", redshift

############## Zoom file IO #########################
# Read in relevant tree information from the hdf5 file
filename_zoom = Zoom + "_" + Run + "_tsf" + str(timestep_final) +".hdf5"
    
File = h5py.File(OutDir+filename_zoom,"r")
    
# Open the subvolume file (containing all cell data)
subvol_filename = "subvol_"+filename_zoom
subvol_File = h5py.File(OutDir+subvol_filename,"r")

# Check the file with the tree information already exists
if not Zoom in File:
    print "Error: Tree information for ", Zoom, " in ", filename_zoom, " cannot be read"
    exit()

zoom_group = File[Zoom]

######### Setup the output arrays ##########
# Get number of timesteps
for halo_group_str in zoom_group:
    if "_main" in halo_group_str:
        halo_group_str_use= halo_group_str
mp_group = zoom_group[halo_group_str_use+"/main_progenitors"]

ts_group = subvol_File["timestep_"+str(timestep)]
cell_group = ts_group["Gas_leaf_cell_data"]

print "Reading cell properties"
rho_snap = cell_group["rho_c"][:]
pre_snap = cell_group["pre_c"][:]
cx = cell_group["x_c"][:] # Cell coordinates
cy = cell_group["y_c"][:]
cz = cell_group["z_c"][:]
clev = cell_group["lev"][:]
cvx = cell_group["vx_c"][:]
cvy = cell_group["vy_c"][:]
cvz = cell_group["vz_c"][:]
c_component_index = cell_group["component_index"][:]

if RT:
    xH1 = cell_group["xH1"][:]
    xHe1 = cell_group["xHe1"][:]
    xHe2 = cell_group["xHe2"][:]

print "Done"

######### Efficiently find cells inside rvir ###########
cxyz_min = min(cx.min(),cy.min(),cz.min())
cxyz_max = max(cx.max(),cy.max(),cz.max())

cx_rescale = (cx - cxyz_min) / (cxyz_max-cxyz_min)
cy_rescale = (cy - cxyz_min) / (cxyz_max-cxyz_min)
cz_rescale = (cz - cxyz_min) / (cxyz_max-cxyz_min)

nchain_cells = 20
nextincell_c,firstincell_c = nu.neighbour_utils.init_chain(cx_rescale,cy_rescale,cz_rescale,nchain_cells)

# Factor to help convert into box units (note maxime's merger_tree reader has converted halo positions so be careful here!)
r2kpc_ideal, redshift, little_h, m2Msun,junk,junk = ur.Set_Units(RunDir, timestep, "ideal")
boxlen_cMpc = dm.jeje_utils.get_param_real(RunDir,timestep,'boxlen') # Box size in box units
boxlen_cMpc *= r2kpc_ideal
boxlen_cMpc = boxlen_cMpc * (1+redshift) /1e3 # cMpc (comoving)
boxlen_cMpc = boxlen_cMpc * little_h # h^-1 cMpc
factor = 1./boxlen_cMpc

# Identify the main progenitor at this timestep
timestep_tree = timestep -1
ok_ts = np.array(mp_group["halo_ts"])==timestep_tree # haloes on this timestep

xh = mp_group["x"][ok_ts] * factor + 0.5
yh = mp_group["y"][ok_ts] * factor + 0.5
zh = mp_group["z"][ok_ts] * factor + 0.5

# Use r200 for central haloes, and rvir (for now) for satellite haloes
if mp_group["host_halo_id"][ok_ts] == mp_group["halo_id"][ok_ts]:
    rvirh = mp_group["r200"][ok_ts] / r2kpc           
else:
    rvirh = mp_group["rvir"][ok_ts] * factor

# Select cells in the vicinity of the halo (using a chain algorithm)
rmax = rvirh*1.5

print "Selecting cells within rvir"

###### Cells within virial radius #########
# Rescale halo positions for efficient cell division in chain algorithm
xh_rescale = (xh-cxyz_min)/(cxyz_max-cxyz_min)
yh_rescale = (yh-cxyz_min)/(cxyz_max-cxyz_min)
zh_rescale = (zh-cxyz_min)/(cxyz_max-cxyz_min)
rmax_rescale = rmax/(cxyz_max-cxyz_min)

ncell_in_rmax = nu.neighbour_utils.get_npart_in_sphere_with_chain(RunDir,timestep,cx_rescale,cy_rescale,cz_rescale,xh_rescale,yh_rescale,zh_rescale,rmax_rescale,firstincell_c,nextincell_c)
if ncell_in_rmax > 0:
    select_c = nu.neighbour_utils.\
               get_part_indicies_in_sphere_with_chain(\
                                                      RunDir,timestep,cx_rescale,cy_rescale,cz_rescale,\
                                                      xh_rescale,yh_rescale,zh_rescale,\
                                                      rmax_rescale,ncell_in_rmax,firstincell_c,nextincell_c)
else:
    select_c = np.zeros_like(cx) < 0

######## Select cells inside rvir ############
cx_h = cx[select_c]
cy_h = cy[select_c]
cz_h = cz[select_c]
rho_snap_h = rho_snap[select_c]; pre_snap_h = pre_snap[select_c]
cvx_h = cvx[select_c]; cvy_h = cvy[select_c]; cvz_h = cvz[select_c]
clev_h = clev[select_c]; c_component_index_h = c_component_index[select_c]

if RT:
    xH1_h = xH1[select_c]
    xHe1_h = xHe1[select_c]
    xHe2_h = xHe2[select_c]

cx_h = cx_h - xh; cy_h = cy_h - yh; cz_h = cz_h -zh
cr_h = np.sqrt(np.square(cx_h) + np.square(cy_h) + np.square(cz_h))

csize_h = boxlen_pkpc / 2.**clev_h / r2kpc

######## Read Rascas photon data ###########
snap_str = str(timestep)
if len(snap_str) == 1:
    snap_str = "0000"+snap_str
elif len(snap_str) == 2:
    snap_str = "000"+snap_str
elif len(snap_str) == 3:
    snap_str = "00"+snap_str
else:
    print "Error", snap_str
    quit()

print "Reading Rascas photons"

# Photons tracing collisional excitations
ex_rc_path_col = RunDir +"/RASCAS/"+snap_str+"/ColLya_CS100_Rvir/"
icFile_path_col = ex_rc_path_col + "001.IC" # For now I'm assuming there is only one rascas output per simulation output ("001")
resFile_path_col = ex_rc_path_col + "001.res"

# Photons tracing recombinations
ex_rc_path_rec = RunDir +"/RASCAS/"+snap_str+"/RecLya_CS100_Rvir/"
icFile_path_rec = ex_rc_path_rec + "001.IC" # For now I'm assuming there is only one rascas output per simulation output ("001")
resFile_path_rec = ex_rc_path_rec + "001.res"

coll_p = jp.photonlist(icFile_path_col, resFile_path_col)
rec_p = jp.photonlist(icFile_path_rec, resFile_path_rec)

# We only care about escaped photons
esc = np.where(coll_p.status == 1)[0]
n_col_b4 = len(coll_p.x)
coll_p = coll_p.extract_sample(esc)

esc = np.where(rec_p.status == 1)[0]
n_rec_b4 = len(rec_p.x)
rec_p = rec_p.extract_sample(esc)

print "Number of rascas photons", n_col_b4, n_rec_b4, ", number that escape is", len(coll_p.x), len(rec_p.x)

# Loop over pre-processed tracer files
print "Reading pre-processed tracer data"

tx = []; ty = []; tz = []; t_stripped_flag = []; t_orb_flag = []; t_fb_flag = []; tcomp = []; tfam = []
tmass = []; trad = []

for ifile, filename in enumerate(filenames_list):

    if not downsample:
        print "Processing tracer subset", ifile, "of", batch_no

    print "Reading", InDir+filename
    File_tr = h5py.File(InDir+filename,"r")

    ts_group = File_tr["timestep_"+str(timestep)]
    
    t_timesteps = ts_group["timestep"][:]
    i_ts = np.argmin(abs(timestep-t_timesteps))
    
    print "Reading x"
    tx = np.concatenate((ts_group["x"],tx))
    print "Reading y"
    ty = np.concatenate((ts_group["y"],ty))
    print "Reading z"
    tz = np.concatenate((ts_group["z"],tz))
    print "Reading mass"
    tmass = np.concatenate((ts_group["mass"],tmass))
    print "Read radius"
    trad = np.concatenate((ts_group["r"],trad))
    
    print "Reading enter in sat"
    t_stripped_flag = np.concatenate((File_tr["enter_in_sat"],t_stripped_flag))
    print "Reading orb"
    t_orb_flag = np.concatenate((File_tr["part_orb_type_evo"][i_ts],t_orb_flag))
    print "Reading fb"
    t_fb_flag = np.concatenate((File_tr["fb_flag_evo"][i_ts],t_fb_flag))

    print "Reading comp"
    tcomp = np.concatenate((ts_group["comp"],tcomp))
    print "Reading family"
    tfam = np.concatenate((ts_group["family"],tfam))
    
    File_tr.close()

ok = (np.isnan(trad) == False) & (trad <= 1) & (tfam==100)
trad = trad[ok]
tfam = tfam[ok]
tx = tx[ok]
ty = ty[ok]
tz = tz[ok]
t_stripped_flag = t_stripped_flag[ok]
t_orb_flag = t_orb_flag[ok]
t_fb_flag = t_fb_flag[ok]
tcomp = tcomp[ok]
tmass = tmass[ok]

print "Grouping pre-processed tracer data"

# Group the Lagrangian status that we are interested in
t_1st_infall = np.zeros_like(tx)
t_orbit = np.zeros_like(tx)
t_stripped = np.zeros_like(tx)
t_fb = np.zeros_like(tx)
t_ism = np.zeros_like(tx)
t_sat = np.zeros_like(tx)

# Flag particles that inside R_200 of neihbouring central haloes - call them satellites
tcomp[(tcomp==3)&(trad>1.0)] = 2

ok = (t_orb_flag==0) & (t_fb_flag == 0) & (t_stripped_flag == 0) & (tfam == 100) & ((tcomp == 0)|(tcomp==3)) & (trad>0.2)
t_1st_infall[ok] += 1

ok = (t_orb_flag>0) & (t_fb_flag == 0) & (t_stripped_flag == 0) & (tfam == 100) & ((tcomp == 0)|(tcomp == 3)) & (trad>0.2)
t_orbit[ok] += 1

ok = (t_fb_flag == 0) & (t_stripped_flag == 1) & (tfam == 100) & ((tcomp == 0)|(tcomp == 3)) & (trad>0.2)
t_stripped[ok] += 1

ok = (t_fb_flag > 0) & (tfam == 100) & ((tcomp == 0)|(tcomp == 3)) & (trad>0.2)
t_fb[ok] += 1

ok = (tfam == 100) & (trad<=0.2)
t_ism[ok] += 1

ok = (tfam == 100) & ((tcomp > 0)&(tcomp<3)) & (trad>0.2)
t_sat[ok] += 1

tx *= rvirh; ty *= rvirh; tz *= rvirh # Yes the tracer x/y/z are stored in units of r/rvir - so this puts them into box units

######## Compute mass weighted tracer status of each cell ###########
print "Matching cells and tracers"

ind_c, ok_match = flux_utils.Match_Cell_Particle(csize_h, cx_h, cy_h, cz_h, tx, ty, tz)
        
c_1st_infall = flux_utils.Sum_Common_ID( [tmass[ok_match] * t_1st_infall[ok_match]], ind_c[ok_match], np.arange(len(cx_h)) )[0]
c_orbit = flux_utils.Sum_Common_ID( [tmass[ok_match] * t_orbit[ok_match]], ind_c[ok_match], np.arange(len(cx_h)) )[0]
c_stripped = flux_utils.Sum_Common_ID( [tmass[ok_match] * t_stripped[ok_match]], ind_c[ok_match], np.arange(len(cx_h)) )[0]
c_fb = flux_utils.Sum_Common_ID( [tmass[ok_match] * t_fb[ok_match]], ind_c[ok_match], np.arange(len(cx_h)) )[0]
c_ism = flux_utils.Sum_Common_ID( [tmass[ok_match] * t_ism[ok_match]], ind_c[ok_match], np.arange(len(cx_h)) )[0]
c_sat = flux_utils.Sum_Common_ID( [tmass[ok_match] * t_sat[ok_match]], ind_c[ok_match], np.arange(len(cx_h)) )[0]

c_tot = flux_utils.Sum_Common_ID( [tmass[ok_match]], ind_c[ok_match], np.arange(len(cx_h)) )[0]

ctmass = np.copy(c_tot)

ok = c_tot > 0
c_1st_infall[ok] *= 1./c_tot[ok]
c_orbit[ok]      *= 1./c_tot[ok]
c_stripped[ok]   *= 1./c_tot[ok]
c_fb[ok]         *= 1./c_tot[ok]
c_ism[ok]        *= 1./c_tot[ok]
c_sat[ok]        *= 1./c_tot[ok]
c_tot[ok] *= 1./c_tot[ok]

####### Compute escaping Lya luminosity of each cell #########

print "Matching cells and photons"
# Centre photons on halo
coll_p.x -= xh; coll_p.y -= yh; coll_p.z -= zh
rec_p.x -= xh; rec_p.y -= yh; rec_p.z -= zh

# Link photon final positions to cells
ind_rec, ok_match_rec = flux_utils.Match_Cell_Particle(csize_h, cx_h, cy_h, cz_h, rec_p.x, rec_p.y, rec_p.z)
ind_col, ok_match_col = flux_utils.Match_Cell_Particle(csize_h, cx_h, cy_h, cz_h, coll_p.x, coll_p.y, coll_p.z)

planck  = 6.626070040e-27  # [erg s] Planck's constant
rec_L_p = rec_p.nRealPhotons / len(rec_p.x) * planck * rec_p.nu # erg s^-1 (note nRealPhotons is number of photons per second - it is a luminosity)
col_L_p = coll_p.nRealPhotons / len(coll_p.x) * planck * coll_p.nu # erg s^-1

c_rec_L_p = flux_utils.Sum_Common_ID( [rec_L_p[ok_match_rec]], ind_rec[ok_match_rec], np.arange(len(cx_h)) )[0]
c_col_L_p = flux_utils.Sum_Common_ID( [col_L_p[ok_match_col]], ind_col[ok_match_col], np.arange(len(cx_h)) )[0]

subvol_File.close()
File.close()




################### Dump data to disk ######################

dump_dir = "/cral2/mitchell/PostProcess/Map_Dumps/"
dump_file_path = dump_dir + "dump_"+Zoom+"_"+Run+"_ts"+str(timestep)+".hdf5"

if os.path.isfile(dump_file_path):
    os.remove(dump_file_path)
    print "deleted previous hdf5 file at ", dump_file_path

dump_file = h5py.File(dump_file_path)

dump_file.create_dataset("x",data=cx_h)
dump_file.create_dataset("y",data=cy_h)
dump_file.create_dataset("z",data=cz_h)
dump_file.create_dataset("vx",data=cvx_h)
dump_file.create_dataset("vy",data=cvy_h)
dump_file.create_dataset("vz",data=cvz_h)
dump_file.create_dataset("lev",data=clev_h)
dump_file.create_dataset("rho",data=rho_snap_h)
dump_file.create_dataset("pre",data=pre_snap_h)
dump_file.create_dataset("comp",data=c_component_index_h)

if RT:
    dump_file.create_dataset("xH1",data=xH1_h)
    dump_file.create_dataset("xHe1",data=xHe1_h)
    dump_file.create_dataset("xHe2",data=xHe2_h)


dump_file.create_dataset("L_Lya_rec",data=c_rec_L_p)
dump_file.create_dataset("L_Lya_col",data=c_col_L_p)

dump_file.create_dataset("state_1st_infall",data=c_1st_infall)
dump_file.create_dataset("state_orbit",data=c_orbit)
dump_file.create_dataset("state_stripped",data=c_stripped)
dump_file.create_dataset("state_fb",data=c_fb)
dump_file.create_dataset("state_ism",data=c_ism)
dump_file.create_dataset("state_sat",data=c_sat)
dump_file.create_dataset("state_tot",data=c_tot)
dump_file.create_dataset("tmass",data=ctmass)

print "Got to the end :)"
