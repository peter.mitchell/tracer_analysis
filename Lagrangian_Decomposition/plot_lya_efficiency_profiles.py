import numpy as np
import h5py
import utilities_cosmology as uc
import dm_utils as dm
import utilities_ramses as ur
import ldecomp_functions as ldf


Zoom = "Zoom-7-5092"
Run = "RhdRun-tracer_radfix"
timestep_final = 155
RunDir = "/cral2/mitchell/"+ Zoom + "/" + Run
OutDir = "/cral2/mitchell/PostProcess/"
RT = True

# Edit: this should now be redundant
# If tracer sample is downsample, need to up-weight the mass/luminosity etc of each tracer
#downsampling = True
#ds_correction_factor = 31400640 / (3.0*10**6) # Appropriate for Rfix 3e6 ds

# Choose radial bins for computing exchange terms
delta_rprime = 0.1
r_bins = np.arange(0.0,1.1,delta_rprime)
r_mid = 0.5*(r_bins[1:]+r_bins[0:-1])

xlo = 0.2; xhi = 1.0

# Choose redshift interval to stack over
zlo_stack = 3.0
zhi_stack = 4.0

# Read the measurments
filename = Zoom + "_" + Run + "_tsf" + str(timestep_final) +".hdf5"

output_File = h5py.File(OutDir+filename)
zoom_group = output_File[Zoom]

for halo_group_str in zoom_group:
    # Only compute fluxes for the main halo
    if not "_main" in halo_group_str:
        continue
    mp_group = zoom_group[halo_group_str+"/main_progenitors"]

timesteps = mp_group["halo_ts"][:] +1 # +1 is for the tree/simulation indexing mismatch
z = np.zeros_like(timesteps)

dm.jeje_utils.read_conversion_scales(RunDir,timestep_final)
omega_0,lambda_0,little_h = dm.jeje_utils.read_cosmo_params(RunDir,timestep_final)

r2kpc_evo = np.zeros(len(z))
for n in range(len(z)):
    r2kpc_ideal, redshift, little_h, m2Msun,junk,junk = ur.Set_Units(RunDir, timesteps[n], "ideal")
    z[n] = redshift

    
    r2kpc_evo[n], redshift, little_h, m2Msun,rho2msunpkpc3,v2kms = ur.Set_Units(RunDir, timesteps[n])
    
#np.zeros((n_type, n_ts_out, len(r_bins[1:]) ))
f_1st_infall_evo = []
f_past_peri_evo  = []
f_past_apo_evo = []
f_strip_1st_infall_evo = []
f_strip_past_peri_evo = []
f_strip_past_apo_evo = []
f_fb_1st_outflow_evo = []
f_fb_past_peri_evo = []
f_fb_past_apo_evo = []
f_tot_evo = []
f_sat_evo = []

n_type = 10
select_types = ["m_tot","m_neut","CollLyaLum","RecLyaLum"]

rascas_name_list = ["cL_h_rec_ism", "cL_h_rec_sat", "cL_h_rec_cgm", "cL_h_col_ism", "cL_h_col_sat", "cL_h_col_cgm"]
select_types += rascas_name_list

for n in range(n_type):

    f_tot_evo.append(mp_group["tot_"+select_types[n]+"_evo"][:])
    f_sat_evo.append(mp_group["sat_"+select_types[n]+"_evo"][:])
    
    f_1st_infall_evo.append(mp_group["1st_infall_"+select_types[n]+"_evo"][:])
    f_past_peri_evo.append(mp_group["past_peri_"+select_types[n]+"_evo"][:])
    f_past_apo_evo.append(mp_group["past_apo_"+select_types[n]+"_evo"][:])
    f_strip_1st_infall_evo.append(mp_group["1st_infall_stripped_"+select_types[n]+"_evo"][:])
    f_strip_past_peri_evo.append(mp_group["past_peri_stripped_"+select_types[n]+"_evo"][:])
    f_strip_past_apo_evo.append(mp_group["past_apo_stripped_"+select_types[n]+"_evo"][:])

    # Note called this one inflow in hdf5 by mistake - it is 1st outflow gas
    f_fb_1st_outflow_evo.append(mp_group["1st_infall_fb_"+select_types[n]+"_evo"][:])

    f_fb_past_peri_evo.append(mp_group["past_peri_fb_"+select_types[n]+"_evo"][:])
    f_fb_past_apo_evo.append(mp_group["past_apo_fb_"+select_types[n]+"_evo"][:])

# Stack radial bins together for this script - note this only works with radial bins starting at 0.2

def summation( array, z ):

    # Hack to correct bugged lyman alpha luminosity units
    #print "temp hack"
    #redshift_correction = r2kpc_evo**3

    #for i in range(len(z)):
    #    array[2][i] *= redshift_correction[i]
        
    ok = np.where((z > zlo_stack) & (z<zhi_stack))[0]

    array = np.array(array)
    array = array[:,ok]
    
    ok = np.isnan(array)
    array[ok] = 0.0
    
    array = np.sum(array,axis=1) / len(array[0,:,0])
    array *= 1/delta_rprime

    # Now units are d quant d (r/R_vir), mean stacked over specified redshift interval

    # If we are using a down-sampled tracer sample, correct
    #if downsampling:
    #    array *= ds_correction_factor        
    return array

f_tot_evo = np.array(f_tot_evo)
f_sat_evo = np.array(f_sat_evo)
f_tot_evo -= f_sat_evo # Exclude satellite material for now
ok = np.isnan(f_tot_evo)
f_tot_evo[ok] = 0

#print ""
#print ""
#print "Another Big hack for rfix 3d6 ds only, Lyman alpha luminosity needs to be corrected"
#print ""
#print ""

f_tot_evo = summation(f_tot_evo,z)
f_1st_infall_evo = summation(f_1st_infall_evo,z)
f_past_peri_evo  = summation(f_past_peri_evo,z)
f_past_apo_evo = summation(f_past_apo_evo,z)
f_strip_1st_infall_evo = summation(f_strip_1st_infall_evo,z)
f_strip_past_peri_evo = summation(f_strip_past_peri_evo,z)
f_strip_past_apo_evo = summation(f_strip_past_apo_evo,z)
f_fb_1st_outflow_evo = summation(f_fb_1st_outflow_evo,z)
f_fb_past_peri_evo = summation(f_fb_past_peri_evo,z)
f_fb_past_apo_evo = summation(f_fb_past_apo_evo,z)

f_strip_evo = f_strip_1st_infall_evo + f_strip_past_peri_evo + f_strip_past_apo_evo
f_fb_evo = f_fb_1st_outflow_evo + f_fb_past_apo_evo + f_fb_past_peri_evo
f_after_infall_evo = f_past_peri_evo + f_past_apo_evo

from utilities_plotting import *

c_list = ["k","b","m","r","g","y","c","firebrick","darkgray","orange"]

nrow = 3; ncol=1
py.rcParams.update(latexParams)
py.rcParams.update({'figure.subplot.wspace':0.15,'figure.subplot.hspace':0.,
                    'figure.figsize':[3.32*ncol,2.49*nrow],
                    'figure.subplot.left':0.19,
                    'figure.subplot.bottom':0.1,
                    'figure.subplot.top':0.98,
                    'figure.subplot.right':0.94,
                    'legend.fontsize':6,
                    'font.size':12,
                    'axes.labelsize':10})

def plot(r_mid, y, c, label=None,linestyle='-',linewidth=1.1):

    if label is None:
        py.plot(r_mid, y,c=c,linestyle=linestyle,linewidth=linewidth)
    else:
        py.plot(r_mid, y,c=c,label=label,linestyle=linestyle,linewidth=linewidth)

    ps = 1
    if linestyle=='-':
        py.scatter(r_mid, y,c=c,edgecolors="none",s=ps)

py.figure()
subplots = panelplot(nrow,ncol)
for n,ax in enumerate(subplots):
    py.axes(ax)

    if n == 0: # Total intrinsic
        tot = (f_tot_evo[2] + f_tot_evo[3])
        infall = (f_1st_infall_evo[2]+f_1st_infall_evo[3])
        peri = (f_after_infall_evo[2]+f_after_infall_evo[3])
        strip = (f_strip_evo[2]+f_strip_evo[3])
        fb = (f_fb_evo[2]+f_fb_evo[3])
    elif n == 1: # Recombination
        tot = f_tot_evo[2]
        infall = (f_1st_infall_evo[2])
        strip = (f_strip_evo[2])
        peri = (f_after_infall_evo[2])
        fb = (f_fb_evo[2])
    elif n == 2: # Collsion
        tot = f_tot_evo[3] 
        infall = (f_1st_infall_evo[3])
        peri = (f_after_infall_evo[3])
        strip = (f_strip_evo[3])
        fb = (f_fb_evo[3])
        
    # Normalise by neutral mass
    tot *= 1./ f_tot_evo[1]
    infall *= 1./ f_tot_evo[1]
    peri *= 1./ f_tot_evo[1]
    strip *= 1./ f_tot_evo[1]
    fb *= 1./ f_tot_evo[1]
                   
    plot(r_mid, np.log10(tot),"k",label="Total")
    plot(r_mid, np.log10(infall), "b",label= "First infall")
    plot(r_mid, np.log10(peri), "g",label= "Post-pericenter")
    plot(r_mid, np.log10(strip), "c", label="Stripped")
    plot(r_mid, np.log10(fb), "r", label="Feedback influenced")

    if n == 3:
        intrin = f_tot_evo[2] + f_tot_evo[3]
        plot(r_mid, np.log10(intrin),"k",linestyle='--')

    if n == 0:
        py.legend(frameon=False,loc="lower left")
        py.annotate(r"Total intrinsic Ly$\alpha$",(0.25,32.65),fontsize=10)

    elif n == 1:
        py.annotate(r"Recombinations",(0.25,32.65),fontsize=10)
        
    if n == 2:
        py.xlabel(r"$r \, / R_{\mathrm{vir}}$")
        py.annotate(r"Collisional excitations",(0.25,32.65),fontsize=10)

    py.ylabel(r"$\log(\left< L \right>  / \left<m_{\mathrm{HI}} \right> \, / \mathrm{erg \, s^{-1} M_\odot^{-1}})$")

    if n == 0:
        py.ylim((30.5,33))
    else:
        py.ylim((30.5,32.99))
    py.xlim((xlo,xhi))
        
fig_name = "cgm_lya_efficiency_profiles_lagrangian_"+Zoom+"_"+Run+".pdf"
py.savefig("../Figures/"+fig_name)
py.show()
