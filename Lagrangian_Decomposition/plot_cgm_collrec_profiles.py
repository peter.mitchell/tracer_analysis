import numpy as np
import h5py
import utilities_cosmology as uc
import dm_utils as dm
import utilities_ramses as ur
import ldecomp_functions as ldf


Zoom = "Zoom-7-5092"
Run = "RhdRun-tracer_radfix"
timestep_final = 107
RunDir = "/cral2/mitchell/"+ Zoom + "/" + Run
OutDir = "/cral2/mitchell/PostProcess/"
RT = True

# If tracer sample is downsample, need to up-weight the mass/luminosity etc of each tracer
downsampling = True
ds_correction_factor = 31400640 / (3.0*10**6) # Appropriate for Rfix 3e6 ds

# Choose radial bins for computing exchange terms
delta_rprime = 0.1
r_bins = np.arange(0.2,1.1,delta_rprime)
r_mid = 0.5*(r_bins[1:]+r_bins[0:-1])

xlo = 0.2; xhi = 1.0

# Choose redshift interval to stack over
zlo_stack = 4.0
zhi_stack = 6.0

# Read the measurments
filename = Zoom + "_" + Run + "_tsf" + str(timestep_final) +".hdf5"

output_File = h5py.File(OutDir+filename)
zoom_group = output_File[Zoom]

for halo_group_str in zoom_group:
    # Only compute fluxes for the main halo
    if not "_main" in halo_group_str:
        continue
    mp_group = zoom_group[halo_group_str+"/main_progenitors"]

timesteps = mp_group["halo_ts"][:] +1 # +1 is for the tree/simulation indexing mismatch
z = np.zeros_like(timesteps)

dm.jeje_utils.read_conversion_scales(RunDir,timestep_final)
omega_0,lambda_0,little_h = dm.jeje_utils.read_cosmo_params(RunDir,timestep_final)

r2kpc_evo = np.zeros(len(z))
for n in range(len(z)):
    r2kpc_ideal, redshift, little_h, m2Msun,junk,junk = ur.Set_Units(RunDir, timesteps[n], "ideal")
    z[n] = redshift

    
    r2kpc_evo[n], redshift, little_h, m2Msun,rho2msunpkpc3,v2kms = ur.Set_Units(RunDir, timesteps[n])
    
#np.zeros((n_type, n_ts_out, len(r_bins[1:]) ))
f_1st_infall_evo = []
f_past_peri_evo  = []
f_past_apo_evo = []
f_strip_1st_infall_evo = []
f_strip_past_peri_evo = []
f_strip_past_apo_evo = []
f_fb_1st_outflow_evo = []
f_fb_past_peri_evo = []
f_fb_past_apo_evo = []
f_tot_evo = []

n_type = 2
select_types = ["CollLyaLum","RecLyaLum"]

for n in range(n_type):

    f_tot_evo.append(mp_group["tot_"+select_types[n]+"_evo"][:])
    
    f_1st_infall_evo.append(mp_group["1st_infall_"+select_types[n]+"_evo"][:])
    f_past_peri_evo.append(mp_group["past_peri_"+select_types[n]+"_evo"][:])
    f_past_apo_evo.append(mp_group["past_apo_"+select_types[n]+"_evo"][:])
    f_strip_1st_infall_evo.append(mp_group["1st_infall_stripped_"+select_types[n]+"_evo"][:])
    f_strip_past_peri_evo.append(mp_group["past_peri_stripped_"+select_types[n]+"_evo"][:])
    f_strip_past_apo_evo.append(mp_group["past_apo_stripped_"+select_types[n]+"_evo"][:])

    # Note called this one inflow in hdf5 by mistake - it is 1st outflow gas
    f_fb_1st_outflow_evo.append(mp_group["1st_infall_fb_"+select_types[n]+"_evo"][:])

    f_fb_past_peri_evo.append(mp_group["past_peri_fb_"+select_types[n]+"_evo"][:])
    f_fb_past_apo_evo.append(mp_group["past_apo_fb_"+select_types[n]+"_evo"][:])

# Stack radial bins together for this script - note this only works with radial bins starting at 0.2

def summation( array, z ):

    # Hack to correct bugged lyman alpha luminosity units
    #print "temp hack"
    #redshift_correction = r2kpc_evo**3

    #for i in range(len(z)):
    #    array[2][i] *= redshift_correction[i]
        
    ok = np.where((z > zlo_stack) & (z<zhi_stack))[0]

    array = np.array(array)
    array = array[:,ok]
    
    ok = np.isnan(array)
    array[ok] = 0.0
    
    array = np.sum(array,axis=1) / len(array[0,:,0])
    array *= 1/delta_rprime

    # Now units are d quant d (r/R_vir), mean stacked over specified redshift interval

    # If we are using a down-sampled tracer sample, correct
    if downsampling:
        array *= ds_correction_factor        
    return array

f_tot_evo = np.array(f_tot_evo)
ok = np.isnan(f_tot_evo)
f_tot_evo[ok] = 0


#print ""
#print ""
#print "Another Big hack for rfix 3d6 ds only, Lyman alpha luminosity needs to be corrected"
#print ""
#print ""

f_1st_infall_evo = summation(f_1st_infall_evo,z)
f_past_peri_evo  = summation(f_past_peri_evo,z)
f_past_apo_evo = summation(f_past_apo_evo,z)
f_strip_1st_infall_evo = summation(f_strip_1st_infall_evo,z)
f_strip_past_peri_evo = summation(f_strip_past_peri_evo,z)
f_strip_past_apo_evo = summation(f_strip_past_apo_evo,z)
f_fb_1st_outflow_evo = summation(f_fb_1st_outflow_evo,z)
f_fb_past_peri_evo = summation(f_fb_past_peri_evo,z)
f_fb_past_apo_evo = summation(f_fb_past_apo_evo,z)

f_strip_evo = f_strip_1st_infall_evo + f_strip_past_peri_evo + f_strip_past_apo_evo
f_fb_evo = f_fb_1st_outflow_evo + f_fb_past_apo_evo + f_fb_past_peri_evo
f_after_infall_evo = f_past_peri_evo + f_past_apo_evo

f_tot_evo = f_1st_infall_evo + f_strip_evo + f_fb_evo + f_after_infall_evo
f_tot_out = f_past_peri_evo + f_strip_past_peri_evo + f_fb_1st_outflow_evo + f_fb_past_peri_evo
f_tot_in = f_1st_infall_evo + f_past_apo_evo + f_strip_past_apo_evo + f_strip_1st_infall_evo + f_fb_past_apo_evo

f_strip_out = f_strip_past_peri_evo
f_strip_in = f_strip_past_apo_evo + f_strip_1st_infall_evo
f_after_infall_out = f_past_peri_evo
f_after_infall_in = f_past_apo_evo
f_fb_out = f_fb_1st_outflow_evo + f_fb_past_peri_evo
f_fb_in = f_fb_past_apo_evo

from utilities_plotting import *

c_list = ["k","b","m","r","g","y","c","firebrick","darkgray","orange"]

nrow = 1; ncol=2
py.rcParams.update(latexParams)
py.rcParams.update({'figure.subplot.wspace':0,'figure.subplot.hspace':0,
                    'figure.figsize':[3.32*ncol,2.49*1.1*nrow],
                    'figure.subplot.left':0.09,
                    'figure.subplot.bottom':0.18,
                    'figure.subplot.top':0.9,
                    'figure.subplot.right':0.96,
                    'legend.fontsize':6,
                    'font.size':12,
                    'axes.labelsize':11})

def plot(r_mid, y, c, label=None,linestyle='-',linewidth=1.1):

    if label is None:
        py.plot(r_mid, y,c=c,linestyle=linestyle,linewidth=linewidth)
    else:
        py.plot(r_mid, y,c=c,label=label,linestyle=linestyle,linewidth=linewidth)

    ps = 1
    if linestyle=='-':
        py.scatter(r_mid, y,c=c,edgecolors="none",s=ps)
        
py.figure()
subplots = panelplot(nrow,ncol)
for n,ax in enumerate(subplots):
    py.axes(ax)

    linewidth = 1.0

    if n == 0:
        ax.set_title("Inflow")
    if n == 1:
        ax.set_title("Outflow")
    
    if n == 0:

        # Dummy for legend
        plot(r_mid, r_mid-200,"k",linestyle="-",linewidth=linewidth,label="Total")
        plot(r_mid, r_mid-200,"b",linestyle="-",linewidth=linewidth,label="Inflowing 1st-infall")
        plot(r_mid, r_mid-200,"g",linestyle="-",linewidth=linewidth,label="Orbiting after 1st-infall")
        plot(r_mid, r_mid-200,"m",linestyle="-",linewidth=linewidth,label="Stripped")
        plot(r_mid, r_mid-200, "r", linestyle="-",linewidth=linewidth,label="Feedback-driven")

        plot(r_mid, np.log10(f_tot_in[0]),"k",linestyle="-",linewidth=linewidth)
        plot(r_mid, np.log10(f_1st_infall_evo[0]),"b",linestyle="-",linewidth=linewidth)
        plot(r_mid, np.log10(f_after_infall_in[0]),"g",linestyle="-",linewidth=linewidth)
        plot(r_mid, np.log10(f_strip_in[0]),"m",linestyle="-",linewidth=linewidth)
        plot(r_mid, np.log10(f_fb_in[0]), "r", linestyle="-",linewidth=linewidth)
        
        plot(r_mid, np.log10(f_tot_in[1]),"k",linestyle="--",linewidth=linewidth)
        plot(r_mid, np.log10(f_1st_infall_evo[1]),"b",linestyle="--",linewidth=linewidth)
        plot(r_mid, np.log10(f_after_infall_in[1]),"g",linestyle="--",linewidth=linewidth)
        plot(r_mid, np.log10(f_strip_in[1]),"m",linestyle="--",linewidth=linewidth)
        plot(r_mid, np.log10(f_fb_in[1]), "r", linestyle="--",linewidth=linewidth)

        py.ylabel(r"$\log(\, \left<\frac{\mathrm{d}L}{\mathrm{d}(r/R_{\mathrm{vir}})}\right> \, / \mathrm{erg \, s^{-1}})$")
    
    else:
        plot(r_mid, np.log10(f_tot_out[0]),"k",linestyle="-",linewidth=linewidth,label="Collisions")
        plot(r_mid, np.log10(f_after_infall_out[0]), "g",linestyle="-",linewidth=linewidth)
        plot(r_mid, np.log10(f_strip_out[0]), "m",linestyle='-',linewidth=linewidth)        
        plot(r_mid, np.log10(f_fb_out[0]), "r", linestyle="-",linewidth=linewidth)

        plot(r_mid, np.log10(f_tot_out[1]),"k",linestyle="--",linewidth=linewidth,label="Recombinations")
        plot(r_mid, np.log10(f_after_infall_out[1]), "g",linestyle="--",linewidth=linewidth)
        plot(r_mid, np.log10(f_strip_out[1]), "m",linestyle='--',linewidth=linewidth)        
        plot(r_mid, np.log10(f_fb_out[1]), "r", linestyle="--",linewidth=linewidth)

    py.legend(frameon=False,loc="upper right")
    
    py.xlabel(r"$r \, / R_{\mathrm{vir}}$")
    py.ylim((38.8,43.8))

    if n == 1 or n == 3 or n == 5:
        py.xlim((xlo+0.001,xhi))
    else:
        py.xlim((xlo,xhi))
        
fig_name = "cgm_reccol_lya_profiles_lagrangian_"+Zoom+"_"+Run+".pdf"
py.savefig("../Figures/"+fig_name)
py.show()
