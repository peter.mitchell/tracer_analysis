import argparse
import sys
import numpy as np
import time
import os

#timestep = 155
timestep = 141
#timestep = 10

Zoom = "Zoom-7-5092"
Run = "RhdRun-tracer_radfix"
RT = True
RunDir = "/cral4/mitchell/data/5092_RT_Tracer/"+Zoom+"/"+Run
InDir = "/cral4/mitchell/data/5092_RT_Tracer/PostProcess/"
OutDir = "/cral4/mitchell/data/5092_RT_Tracer/PostProcess/"
timestep_final = 155

dump_dir = "/cral4/mitchell/data/5092_RT_Tracer/PostProcess/Map_Dumps/"

subhalo_radius = "r_tidal"
scale_size = 10.0 # pkpc
scale_size_pkpc = scale_size

############ Define list of maps to write ###############################
map_size = 1.02 # Size in 1 dimension of the map in units of the virial radius
figure_size = 10 # Factor determining size of the figures
dpi_factor = 1.0 # Scaling factor to up/down scale the dpi (resolution)

import dm_utils as dm_utils
import cutils as cut
import h5py
import utilities_ramses as ur
import neighbour_utils as nu
import matplotlib.pyplot as py
from matplotlib.colors import LogNorm
from mpl_toolkits.axes_grid1.inset_locator import inset_axes
from utilities_plotting import *



# Read in relevant tree information from the hdf5 file
dump_file_path = dump_dir + "dump_"+Zoom+"_"+Run+"_ts"+str(timestep)+".hdf5"

print "Performing IO"
dump_file = h5py.File(dump_file_path,"r")

cx_h = dump_file["x"][:]
cy_h = dump_file["y"][:]
cz_h = dump_file["z"][:]
cvx_h = dump_file["vx"][:]
cvy_h = dump_file["vy"][:]
cvz_h = dump_file["vz"][:]
clev_h = dump_file["lev"][:]
rho_snap_h = dump_file["rho"][:]
pre_snap_h = dump_file["pre"][:]
comp = dump_file["comp"][:]

if RT:
    xH1_h = dump_file["xH1"][:]
    xHe1_h = dump_file["xHe1"][:]
    xHe2_h = dump_file["xHe2"][:]

Lrec = dump_file["L_Lya_rec"][:]
Lcol = dump_file["L_Lya_col"][:]

s_1st_infall = dump_file["state_1st_infall"][:]
s_orbit = dump_file["state_orbit"][:]
s_stripped = dump_file["state_stripped"][:]
s_fb = dump_file["state_fb"][:]
s_ism = dump_file["state_ism"][:]
s_sat = dump_file["state_sat"][:]

tmass = dump_file["tmass"][:]

print "reading halo catalogue"

filename = Zoom + "_" + Run + "_tsf" + str(timestep_final) +".hdf5"
File = h5py.File(OutDir+filename,"r")

# Check the file with the tree information already exists
if not Zoom in File:
    print "Error: Tree information for ", Zoom, " in ", filename, " cannot be read"
    exit()

zoom_group = File[Zoom]


def Plot_Circle(x0,y0,r,ax,c="k"):
    theta = np.arange(0,2*np.pi+0.01,0.01)
    x = x0 + np.cos(theta)*r
    y = y0 + np.sin(theta)*r
    Circle = ax.plot(x,y,c=c,alpha=0.7)
    return Circle


timestep_tree = timestep-1

# Get conversions for different units
r2kpc, redshift, little_h, m2Msun,rho2msunpkpc3,v2kms = ur.Set_Units(RunDir, timestep)
boxlen_pkpc = dm_utils.jeje_utils.get_param_real(RunDir,timestep,'boxlen') * r2kpc # Box size in proper kpc, using the correct box size

print "redshift is", redshift

scale_size *= 1./r2kpc # code units

csize = boxlen_pkpc / np.power(2.,clev_h) / r2kpc # kpc
csize *= 3.0857e21 # cm

# Factor to help convert into box units (note maxime's merger_tree reader has converted halo positions so be careful here!)
r2kpc_ideal, redshift, little_h, m2Msun,junk,junk = ur.Set_Units(RunDir, timestep, "ideal")
boxlen_cMpc = dm_utils.jeje_utils.get_param_real(RunDir,timestep,'boxlen') # Box size in box units
boxlen_cMpc *= r2kpc_ideal
boxlen_cMpc = boxlen_cMpc * (1+redshift) /1e3 # cMpc (comoving)
boxlen_cMpc = boxlen_cMpc * little_h # h^-1 cMpc
factor = 1./boxlen_cMpc

# Loop over final timestep haloes
for halo_group_str in zoom_group:
    # Only write maps for main progenitors of the main halo
    if "_main" in halo_group_str:
        break

halo_group = zoom_group[halo_group_str]
mp_group = zoom_group[halo_group_str+"/main_progenitors"]

# Identify the main progenitor at this timestep
ok_ts = np.array(mp_group["halo_ts"])==timestep_tree # haloes on this timestep

xh = mp_group["x"][ok_ts] * factor + 0.5
yh = mp_group["y"][ok_ts] * factor + 0.5
zh = mp_group["z"][ok_ts] * factor + 0.5

# Use r200 for central haloes, and rvir (for now) for satellite haloes
if mp_group["host_halo_id"][ok_ts] == mp_group["halo_id"][ok_ts]:
    rvirh = mp_group["r200"][ok_ts] / r2kpc
else:
    rvirh = mp_group["rvir"][ok_ts] * factor

timestep_tree_final = timestep_final -1
ok_final = np.array(mp_group["halo_ts"])==timestep_tree_final        
mhalo_final = mp_group["mvir"][ok_final][:] * 1e11 # Halo mass in msun

vxh = mp_group["vx"][ok_ts] / v2kms # Halo velocity in Code units
vyh = mp_group["vy"][ok_ts] / v2kms
vzh = mp_group["vz"][ok_ts] / v2kms

print "done, reading subhalo catalogue"

# Find subhalos identified in the tree files
subhalo_rvir_list = []
subhalo_x_list = []
subhalo_y_list = []
subhalo_z_list = []

# Loop over all haloes and create a master list of all the subhaloes of the main halo at the desired time
# Read in halo catalogue (to find subhalo positions)
print "hack to change subhalo selection"
halo_cat_sub_selection = False
if halo_cat_sub_selection:
    from halotils import py_halo_utils as hutils
    import pandas as pd
    import dm_utils as dm_u
    treefile = "%s%i/tree_bricks%3.3i"%(HaloDir,timestep,timestep)
    nh,ns = hutils.get_nb_halos(treefile) # nhalo, nsubhalo
    halos = hutils.read_all_halos(treefile,nh+ns)
    halo_keys = ('nbpart','hnum','ts','level',
                 'host', 'hostsub', 'nbsub', 'nextsub', 'mass',
                 'x', 'y', 'z', 'vx', 'vy', 'vz', 'Lx', 'Ly', 'Lz', 'r',
                 'a', 'b', 'c', 'ek', 'ep', 'et',
                 'spin', 'rvir', 'mvir', 'tvir', 'cvel', 'rho0', 'r_c')
    halos = pd.DataFrame(columns=halo_keys,data=halos)
    
    # Get conversions for different units
    r2kpc, redshift, little_h, m2Msun,rho2msunpkpc3, v2kms = ur.Set_Units(RunDir, timestep)
    boxlen_pkpc = dm_u.jeje_utils.get_param_real(RunDir,timestep,'boxlen') * r2kpc # Box size in proper kpc, using the correct box size
    factor_subhalo = 3.08e24 / dm_u.jeje_utils.dp_scale_l

    for halo_group_str in zoom_group:
        if not "main" in halo_group_str:
           continue
        mp_group = zoom_group[halo_group_str+"/main_progenitors"]
        ok_ts = mp_group["halo_ts"][:]==timestep_tree
        halo_num = mp_group["halo_num"][:][ok_ts]


    subhalo_host_num_list = np.array(halos.host)
    subhalo_num_list = np.array(halos.hnum)
    subhalo_rvir_list = np.array(halos.rvir) * factor_subhalo
    subhalo_x_list = np.array(halos.x) * factor_subhalo +0.5 - xh
    subhalo_y_list = np.array(halos.y) * factor_subhalo +0.5 - yh
    subhalo_z_list = np.array(halos.z) * factor_subhalo +0.5 - zh
    subhalo_nbpart_list = np.array(halos.nbpart)

    # Select subhaloes with > 500 particles that are subhaloes of the main halo
    ok = (halo_num == subhalo_host_num_list) & (halo_num != subhalo_num_list) & (subhalo_nbpart_list > 500)
    subhalo_rvir_list = subhalo_rvir_list[ok]
    subhalo_x_list = subhalo_x_list[ok]
    subhalo_y_list = subhalo_y_list[ok]
    subhalo_z_list = subhalo_z_list[ok]
    n_sub = len(subhalo_z_list)

# Otherwise use subhaloes from the merger trees
else:
    for halo_group_str in zoom_group:
        if not "main" in halo_group_str:
           continue
        mp_group = zoom_group[halo_group_str+"/main_progenitors"]
        ok_ts = mp_group["halo_ts"][:]==timestep_tree
        halo_main = mp_group["halo_id"][:][ok_ts]

    for halo_group_str in zoom_group:
        if not "Halo_" in halo_group_str:
            continue
        ap_group = zoom_group[halo_group_str+"/all_progenitors"]

        is_subhalo = (ap_group["host_halo_id"][:] - ap_group["halo_id"][:] != 0) & (timestep_tree == ap_group["halo_ts"][:]) & (ap_group["host_halo_id"][:]-halo_main == 0)
        
        if subhalo_radius == "r_tidal":
            subhalo_rvir_list.append(ap_group["r_tidal"][:][is_subhalo])
        elif subhalo_radius == "rvir":
            subhalo_rvir_list.append(ap_group["rvir"][:][is_subhalo]* factor)

        subhalo_x_list.append(ap_group["x"][:][is_subhalo]* factor + 0.5 - xh)
        subhalo_y_list.append(ap_group["y"][:][is_subhalo]* factor + 0.5 - yh)
        subhalo_z_list.append(ap_group["z"][:][is_subhalo]* factor + 0.5 - zh)

    subhalo_rvir_list = np.concatenate(subhalo_rvir_list)
    subhalo_x_list = np.concatenate(subhalo_x_list)
    subhalo_y_list = np.concatenate(subhalo_y_list)
    subhalo_z_list = np.concatenate(subhalo_z_list)

    n_sub = len(subhalo_z_list)

print "done, computing cell quantities"

# Set cell unit conversions for this timestep
cut.jeje_utils.read_conversion_scales(RunDir,timestep)

# Compute radial velocities (w.r.t. halo centre)
cr_h = np.sqrt(np.square(cx_h)+np.square(cy_h)+np.square(cz_h)) # Halo-centered already in write script
cv_radial_h = np.sum(np.array([cvx_h-vxh, cvy_h-vyh, cvz_h-vzh]) * np.array([cx_h-xh,cy_h-yh,cz_h-zh]),axis=0)/cr_h
cv_radial_h *= cut.jeje_utils.dp_scale_l / cut.jeje_utils.dp_scale_t * 1e-5

# Compute number density in atoms per cc
nh_h = rho_snap_h * cut.jeje_utils.dp_scale_d / cut.jeje_utils.mh * cut.jeje_utils.xh

# Compute temperature over effective molecular weight in Kelvin
scale_temk = (cut.jeje_utils.dp_scale_l / cut.jeje_utils.dp_scale_t)**2 * cut.jeje_utils.mh / cut.jeje_utils.kb
temp_mu_h = pre_snap_h/rho_snap_h * scale_temk # T/mu in K

# Compute neutral gas number density in atoms per cc and the temperature in Kelvin

if RT:
    xhi_h = (1-xH1_h)# Hydrogen neutral fraction
    X_frac=0.76
    mu_h = 1./( X_frac*(1.+xH1_h) + 0.25*(1.-X_frac)*(1.+xHe1_h+2.*xHe2_h) )
else:
    mu_h, xhi_h = cut.cell_utils.ramses_get_mu_xhi(RunDir,timestep,rho_snap_h, pre_snap_h, True)

nhi_h = nh_h * xhi_h # atoms per cc
temp_h = temp_mu_h * mu_h # K

# Compute ionised hydrogen gas number density
nhii_h = nh_h * (1-xhi_h) # atoms per cc

csize_h = boxlen_pkpc / np.power(2,clev_h)
cell_vol = np.power(csize_h,3) # kpc3
cmass_h = rho_snap_h * rho2msunpkpc3 * cell_vol

print "Computing some tracer quantities"
trho = tmass / cell_vol / rho2msunpkpc3
nh_tr = trho * cut.jeje_utils.dp_scale_d / cut.jeje_utils.mh * cut.jeje_utils.xh

print "done, creating figure"

rmax_cube = float(rvirh) * map_size
xmin = -rmax_cube; xmax = rmax_cube
ymin = -rmax_cube; ymax = rmax_cube
zmin = -rmax_cube; zmax = rmax_cube

xmin_map = xmin +xh # For some reason, make_map from cutils needs the halo to be at it's original position in box units (intsead of shifted to the origin)
xmax_map = xmax +xh
ymin_map = ymin +yh
ymax_map = ymax +yh
zmin_map = zmin +zh
zmax_map = zmax +zh

# Get map bin size
lmax = np.max(clev_h)
nx,ny = cut.cell_utils.get_map_nxny(lmax,xmin,xmax,ymin,ymax)
nx,nz = cut.cell_utils.get_map_nxny(lmax,xmin,xmax,zmin,zmax)

sizex = figure_size
sizey = (sizex * ny)/nx
dpi = int(1.3*nx/sizex) * dpi_factor
dpi = min(300,dpi)

print "using dpi", dpi

map_type = "column"
#map_type = "column_Lya"
#map_type = "rho"
#map_type = "temp"

do_max = False
norm = LogNorm()


map_weighting_cell = nh_h
map_weighting_tracer = nh_tr

if map_type == "rho":
    quant = nh_h
    vmin = 0.00001
    vmax = 100
    cmap = "Blues"
    cbar_label = r'$n_{\mathrm{H}} \, / \mathrm{cm^{-3}}$'        
elif map_type == "temp":
    quant = temp_h
    cbar_label = r'$T \, \mathrm{[K]}$'
    vmin = 10
    vmax = 1e7
    cmap = "seismic"
    cbar_label = r'$T \, / \mathrm{K}$'        
elif map_type == "column":
    quant = nh_tr
    vmin = 1e18
    vmax = 1e24
    cmap = "gist_earth_r"
    map_weighting_cell = np.ones_like(nh_h)
    map_weighting_tracer = np.ones_like(nh_h)
    cbar_label = r'$N_{\mathrm{H}} \, / \mathrm{cm^{-2}}$'        
elif map_type == "column_Lya":
    quant = (Lrec+Lcol)/np.power(csize,3) # erg s^-1 cm^-2
    cbar_label = r'$L_{\mathrm{Ly-\alpha}} \, / \mathrm{erg \, s^-1 \, cm^{-2}}$'
    vmin = 1e6
    vmax = 1e12
    cmap = "gist_earth_r"
else:
    print "No"
    quit()
    
#cmap_vrad = "RdBu"
#cmap_temp = "seismic"
#cmap_lev = "BuPu"

if "column" not in map_type:
    quantity = quant
    map_weighting = map_weighting_cell
else:
    quantity = quant
    map_weighting = np.ones_like(nhi_h)
    
ok = np.isnan(cx_h) == False
map_1, w = cut.cell_utils.make_map(lmax,do_max,xmin_map,xmax_map,ymin_map,ymax_map,zmin_map,zmax_map,quantity[ok],map_weighting[ok],cx_h[ok]+xh,cy_h[ok]+yh,cz_h[ok]+zh,clev_h[ok],nx,ny)
if "column" in map_type:
    map_1 *= w * dm_utils.jeje_utils.dp_scale_l # cm^-2

#ok = (np.isnan(map_1)==False) & (map_1 > 0)
#print np.min(map_1[ok]), np.max(map_1[ok])
#quit()
    
ok = (map_1 > 0) & (np.isnan(map_1)==False)

if "column" not in map_type:
    quantity = quant
    map_weighting = map_weighting_tracer * s_1st_infall
else:
    quantity = quant * s_1st_infall
    map_weighting = np.ones_like(nhi_h)
ok = np.isnan(cx_h) == False
map_2, w = cut.cell_utils.make_map(lmax,do_max,xmin_map,xmax_map,ymin_map,ymax_map,zmin_map,zmax_map,quantity[ok],map_weighting[ok],cx_h[ok]+xh,cy_h[ok]+yh,cz_h[ok]+zh,clev_h[ok],nx,ny)
if "column" in map_type:
    map_2 *= w * dm_utils.jeje_utils.dp_scale_l # cm^-2

if "column" not in map_type:
    quantity = quant
    map_weighting = map_weighting_tracer * s_orbit
else:
    quantity = quant * s_orbit
    map_weighting = np.ones_like(nhi_h)
ok = np.isnan(cx_h) == False
map_3, w = cut.cell_utils.make_map(lmax,do_max,xmin_map,xmax_map,ymin_map,ymax_map,zmin_map,zmax_map,quantity[ok],map_weighting[ok],cx_h[ok]+xh,cy_h[ok]+yh,cz_h[ok]+zh,clev_h[ok],nx,ny)
if "column" in map_type:
    map_3 *= w * dm_utils.jeje_utils.dp_scale_l # cm^-2

if "column" not in map_type:
    quantity = quant
    map_weighting = map_weighting_tracer * s_fb
else:
    quantity = quant * s_fb
    map_weighting = np.ones_like(nhi_h)
ok = np.isnan(cx_h) == False
map_4,w = cut.cell_utils.make_map(lmax,do_max,xmin_map,xmax_map,ymin_map,ymax_map,zmin_map,zmax_map,quantity[ok],map_weighting[ok],cx_h[ok]+xh,cy_h[ok]+yh,cz_h[ok]+zh,clev_h[ok],nx,ny)
if "column" in map_type:
    map_4 *= w * dm_utils.jeje_utils.dp_scale_l # cm^-2

if "column" not in map_type:
    quantity = quant
    map_weighting = map_weighting_tracer * s_stripped
else:
    quantity = quant * s_stripped
    map_weighting = np.ones_like(nhi_h)

ok = np.isnan(cx_h) == False
map_5,w = cut.cell_utils.make_map(lmax,do_max,xmin_map,xmax_map,ymin_map,ymax_map,zmin_map,zmax_map,quantity[ok],map_weighting[ok],cx_h[ok]+xh,cy_h[ok]+yh,cz_h[ok]+zh,clev_h[ok],nx,ny)
if "column" in map_type:
    map_5 *= w * dm_utils.jeje_utils.dp_scale_l # cm^-2

py.rcParams.update(latexParams)
py.rcParams.update({'figure.subplot.wspace':0,'figure.subplot.hspace':0,
                    'figure.figsize':[3.32,2.49],
                    'figure.subplot.left':0.01,
                    'figure.subplot.bottom':0.01,
                    'figure.subplot.top':0.99,
                    'figure.subplot.right':0.99,
                    'font.size':9,
                    'axes.labelsize':9,
                    'legend.fontsize':97})

    
nrow = 1; ncol = 1
fig, ax = py.subplots(nrow,ncol)

for n in range(1):

    py.axes(ax)

    ax.set_xticks([]) 
    ax.set_yticks([]) 
   

    image = ax.imshow(np.zeros((2,2)),interpolation="nearest",origin="lower")
    
    # Note that the order in which you call these methods on imshow() instances can be important in some cases
    image.set_norm(norm)
    image.set_extent((xmin,xmax,ymin,ymax))
    image.norm.vmin = vmin
    image.norm.vmax = vmax
    ax.set_xlim((xmin,xmax))
    ax.set_ylim((ymin,ymax))

    if n == 0:
        image.set_data(map_1.T)
        name = "Total"
    elif n == 1:
        image.set_data(map_2.T)
        name = "First infall"
    elif n == 2:
        image.set_data(map_3.T)
        name = "Post-pericenter"
    elif n == 3:
        image.set_data(map_4.T)
        name = "Feedback influenced"
    elif n == 4:
        image.set_data(map_5.T)
        name = "Stripped"
    else:
        name = ""
        
    image.set_cmap(cmap)
    ax.set_aspect("equal")
    
    Circle1 = Plot_Circle(0.0,0.0,rvirh,ax)
    if n ==0: # Plot subhalo circles for 1st figure
        for i_sub in range(n_sub):
            Circle_sub = Plot_Circle(subhalo_x_list[i_sub], subhalo_y_list[i_sub], subhalo_rvir_list[i_sub],ax)
    Circle_Gal = Plot_Circle(0.0,0.0,0.2*rvirh,ax,c="r")

    if n == 0:
        ax.errorbar([xmin+scale_size*0.6], [ymin+scale_size*0.6], xerr= [scale_size*0.5], yerr=[scale_size*0.5],c="k")
        py.annotate((r"$%3.0f \, \mathrm{kpc}$"%scale_size_pkpc),(xmin+scale_size*0.7, ymin+scale_size*0.17))
        # Weird visual bug with annotate invoked here, fix later...

    #py.annotate((name),(xmin+(xmax-xmin)*0.02, ymax-(ymax-ymin)*0.05))

    '''if n ==0:axins = inset_axes(ax,
                           width="5%", # width = 10% of parent_bbox width
                           height="100%", # height : 100%
                           loc=3, # right-hand-side
                           bbox_to_anchor=(1.12, 0., 1, 1),
                           bbox_transform=ax.transAxes,
                           borderpad=0,
                           )'''
        #cbar = py.colorbar(mappable=image,cax=axins,label=cbar_label,ax=ax)
        #cbar = py.colorbar(mappable=image,label=cbar_label,ax=ax)

#fig.subplots_adjust(right=0.89)
#cbar_ax = fig.add_axes([0.90, 0.15, 0.02, 0.7])
#cbar = fig.colorbar(mappable=image, cax=cbar_ax, label=cbar_label)
#cbar.ax.tick_params(labelsize=6) 

fig_name = "cnrs_2021_map.pdf"

py.savefig("../Figures/"+fig_name)
#py.show()
