import numpy as np
import h5py
import utilities_cosmology as uc
import dm_utils as dm
import utilities_ramses as ur
import ldecomp_functions as ldf


#Zoom = "Zoom-7-1290"
#Run = "tracer_run_lr"
#timestep_final = 155
#RunDir = "/cral4/mitchell/data/Tracer_Test/"+ Zoom + "/" + Run
#OutDir = "/cral4/mitchell/data/Tracer_Test/PostProcess/"
#RT = False

#Zoom = "Zoom-7-5092"
#Run = "RhdRun-tracer_norad_bug"

#Zoom = "Zoom-7-5092"
#Run = "RhdRun-tracer_radfix"
#OutDir = "/cral2/mitchell/PostProcess/"
#timestep_final = 107
#RunDir = "/cral2/mitchell/"+ Zoom + "/" + Run
#RT = True

Zoom = "Zoom-7-5092"
Run = "RhdRun-tracer_radfix"
timestep_final = 107
RunDir = "/cral2/mitchell/"+ Zoom + "/" + Run
OutDir = "/cral2/mitchell/PostProcess/"
RT = True

# Choose radial bins for computing exchange terms
delta_rprime = 0.1
r_bins = np.arange(0.2,1.1,delta_rprime)

# Read the measurments
filename = Zoom + "_" + Run + "_tsf" + str(timestep_final) +".hdf5"

output_File = h5py.File(OutDir+filename)
zoom_group = output_File[Zoom]

for halo_group_str in zoom_group:
    # Only compute fluxes for the main halo
    if not "_main" in halo_group_str:
        continue
    mp_group = zoom_group[halo_group_str+"/main_progenitors"]

timesteps = mp_group["halo_ts"][:] +1 # +1 is for the tree/simulation indexing mismatch
t = np.zeros_like(timesteps)
z = np.zeros_like(timesteps)

dm.jeje_utils.read_conversion_scales(RunDir,timestep_final)
omega_0,lambda_0,little_h = dm.jeje_utils.read_cosmo_params(RunDir,timestep_final)

for n in range(len(z)):
    r2kpc_ideal, redshift, little_h, m2Msun,junk,junk = ur.Set_Units(RunDir, timesteps[n], "ideal")
    z[n] = redshift

    a = 1./(1+redshift)
    t[n], junk = uc.t_Universe(a,omega_0,little_h)
    
#np.zeros((n_type, n_ts_out, len(r_bins[1:]) ))
f_1st_infall_evo = []
f_past_peri_evo  = []
f_past_apo_evo = []
f_strip_1st_infall_evo = []
f_strip_past_peri_evo = []
f_strip_past_apo_evo = []
f_fb_1st_outflow_evo = []
f_fb_past_peri_evo = []
f_fb_past_apo_evo = []
f_tot_evo = []

n_type = 3
select_types = ["m_tot","m_neut","LyaLum"]

for n in range(n_type):

    f_tot_evo.append(mp_group["tot_"+select_types[n]+"_evo"][:])
    
    f_1st_infall_evo.append(mp_group["1st_infall_"+select_types[n]+"_evo"][:])
    f_past_peri_evo.append(mp_group["past_peri_"+select_types[n]+"_evo"][:])
    f_past_apo_evo.append(mp_group["past_apo_"+select_types[n]+"_evo"][:])
    f_strip_1st_infall_evo.append(mp_group["1st_infall_stripped_"+select_types[n]+"_evo"][:])
    f_strip_past_peri_evo.append(mp_group["past_peri_stripped_"+select_types[n]+"_evo"][:])
    f_strip_past_apo_evo.append(mp_group["past_apo_stripped_"+select_types[n]+"_evo"][:])

    # Note called this one inflow in hdf5 by mistake - it is 1st outflow gas
    f_fb_1st_outflow_evo.append(mp_group["1st_infall_fb_"+select_types[n]+"_evo"][:])

    f_fb_past_peri_evo.append(mp_group["past_peri_fb_"+select_types[n]+"_evo"][:])
    f_fb_past_apo_evo.append(mp_group["past_apo_fb_"+select_types[n]+"_evo"][:])

# Stack radial bins together for this script - note this only works with radial bins starting at 0.2

def summation( array, tot ):

    array = np.array(array)

    ok = np.isnan(array)
    array[ok] = 0.0
    
    array = np.sum(array,axis=2) / np.sum(tot,axis=2)

    return array

f_tot_evo = np.array(f_tot_evo)
ok = np.isnan(f_tot_evo)
f_tot_evo[ok] = 0

f_1st_infall_evo = summation(f_1st_infall_evo, f_tot_evo)
f_past_peri_evo  = summation(f_past_peri_evo, f_tot_evo)
f_past_apo_evo = summation(f_past_apo_evo, f_tot_evo)
f_strip_1st_infall_evo = summation(f_strip_1st_infall_evo, f_tot_evo)
f_strip_past_peri_evo = summation(f_strip_past_peri_evo, f_tot_evo)
f_strip_past_apo_evo = summation(f_strip_past_apo_evo, f_tot_evo)
f_fb_1st_outflow_evo = summation(f_fb_1st_outflow_evo, f_tot_evo)
f_fb_past_peri_evo = summation(f_fb_past_peri_evo, f_tot_evo)
f_fb_past_apo_evo = summation(f_fb_past_apo_evo, f_tot_evo)

from utilities_plotting import *

c_list = ["k","b","m","r","g","y","c","firebrick","darkgray","orange"]

nrow = 3; ncol=1
py.rcParams.update(latexParams)
py.rcParams.update({'figure.subplot.wspace':0,'figure.subplot.hspace':0,
                    'figure.figsize':[3.32*ncol,2.49*nrow],
                    'figure.subplot.left':0.17,
                    'figure.subplot.bottom':0.1,
                    'figure.subplot.top':0.98,
                    'figure.subplot.right':0.99,
                    'legend.fontsize':6,
                    'font.size':12,
                    'axes.labelsize':11})

py.figure()
subplots = panelplot(nrow,ncol)
for n,ax in enumerate(subplots):
    py.axes(ax)

    ps = 1
    if n == 0:
        
        f_strip_evo = f_strip_1st_infall_evo + f_strip_past_peri_evo + f_strip_past_apo_evo
        py.plot(t, f_strip_evo[0], c="k", label="Stripped")
        py.scatter(t, f_strip_evo[0], c="k",edgecolors="none",s=ps)

        f_fb_evo = f_fb_1st_outflow_evo + f_fb_past_apo_evo + f_fb_past_peri_evo
        py.plot(t, f_fb_evo[0], c="r", label="Feedback-driven")
        py.scatter(t, f_fb_evo[0], c="r",edgecolors="none",s=ps)
        
        py.plot(t, f_1st_infall_evo[0], c="b", label="Inflowing 1st-infall")
        py.scatter(t, f_1st_infall_evo[0], c="b",edgecolors="none",s=ps)

        f_after_infall_evo = f_past_peri_evo + f_past_apo_evo
        py.plot(t, f_after_infall_evo[0], c="g", label="Orbiting after 1st-infall")
        py.scatter(t, f_after_infall_evo[0], c="g",edgecolors="none",s=ps)


        py.legend(frameon=False)
        py.ylabel(r"$f_{\mathrm{comp,tot}}$")
        ok = np.where(f_1st_infall_evo[0] > 0)[0][0]
        py.xlim((t[ok], t.max()))
        py.ylim((0.0,1.0))

    if n == 1:
        f_strip_evo = f_strip_1st_infall_evo + f_strip_past_peri_evo + f_strip_past_apo_evo
        py.plot(t, f_strip_evo[1], c="k", label="Stripped")
        py.scatter(t, f_strip_evo[1], c="k",edgecolors="none",s=ps)

        f_fb_evo = f_fb_1st_outflow_evo + f_fb_past_apo_evo + f_fb_past_peri_evo
        py.plot(t, f_fb_evo[1], c="r", label="Feedback-driven")
        py.scatter(t, f_fb_evo[1], c="r",edgecolors="none",s=ps)
        
        py.plot(t, f_1st_infall_evo[1], c="b", label="Inflowing 1st-infall")
        py.scatter(t, f_1st_infall_evo[1], c="b",edgecolors="none",s=ps)
        
        f_after_infall_evo = f_past_peri_evo + f_past_apo_evo
        py.plot(t, f_after_infall_evo[1], c="g", label="Orbiting after 1st-infall")
        py.scatter(t, f_after_infall_evo[1], c="g",edgecolors="none",s=ps)

        py.ylabel(r"$f_{\mathrm{comp,neutral}}$")
        ok = np.where(f_1st_infall_evo[0] > 0)[0][0]
        py.xlim((t[ok], t.max()))
        py.ylim((0.0,0.99))

    if n == 2:

        f_strip_evo = f_strip_1st_infall_evo + f_strip_past_peri_evo + f_strip_past_apo_evo
        py.plot(t, f_strip_evo[2], c="k", label="Stripped")
        py.scatter(t, f_strip_evo[2], c="k",edgecolors="none",s=ps)
        
        f_fb_evo = f_fb_1st_outflow_evo + f_fb_past_apo_evo + f_fb_past_peri_evo
        py.plot(t, f_fb_evo[2], c="r", label="Feedback-driven")
        py.scatter(t, f_fb_evo[2], c="r",edgecolors="none",s=ps)

        py.plot(t, f_1st_infall_evo[2], c="b", label="Inflowing 1st-infall")
        py.scatter(t, f_1st_infall_evo[2], c="b",edgecolors="none",s=ps)
        
        f_after_infall_evo = f_past_peri_evo + f_past_apo_evo
        py.plot(t, f_after_infall_evo[2], c="g", label="Orbiting after 1st-infall")
        py.scatter(t, f_after_infall_evo[2], c="g",edgecolors="none",s=ps)
        
        py.ylabel(r"$f_{\mathrm{comp,Ly\alpha}}$")
        
        ok = np.where(f_1st_infall_evo[0] > 0)[0][0]
        py.xlim((t[ok], t.max()))
        
        py.ylim((0.0,0.99))

        py.xlabel(r"$t \, / \mathrm{Gyr}$")

fig_name = "cgm_fraction_evo_"+Zoom+"_"+Run+".pdf"
py.savefig("../Figures/"+fig_name)
py.show()
