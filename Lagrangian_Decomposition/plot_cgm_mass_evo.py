import numpy as np
import h5py
import utilities_cosmology as uc
import dm_utils as dm
import utilities_ramses as ur
import ldecomp_functions as ldf


#Zoom = "Zoom-7-1290"
#Run = "tracer_run_lr"
#timestep_final = 155
#RunDir = "/cral4/mitchell/data/Tracer_Test/"+ Zoom + "/" + Run
#OutDir = "/cral4/mitchell/data/Tracer_Test/PostProcess/"
#RT = False

#Zoom = "Zoom-7-5092"
#Run = "RhdRun-tracer_norad_bug"

#Zoom = "Zoom-7-5092"
#Run = "RhdRun-tracer_radfix"
#OutDir = "/cral2/mitchell/PostProcess/"
#timestep_final = 107
#RunDir = "/cral2/mitchell/"+ Zoom + "/" + Run
#RT = True

Zoom = "Zoom-7-5092"
Run = "RhdRun-tracer_radfix"
timestep_final = 155
RunDir = "/cral2/mitchell/"+ Zoom + "/" + Run
OutDir = "/cral2/mitchell/PostProcess/"
RT = True

# Choose radial bins for computing exchange terms
delta_rprime = 0.1
r_bins = np.arange(0.2,1.1,delta_rprime)

# Read the measurments
filename = Zoom + "_" + Run + "_tsf" + str(timestep_final) +".hdf5"

output_File = h5py.File(OutDir+filename)
zoom_group = output_File[Zoom]

for halo_group_str in zoom_group:
    # Only compute fluxes for the main halo
    if not "_main" in halo_group_str:
        continue
    mp_group = zoom_group[halo_group_str+"/main_progenitors"]

timesteps = mp_group["halo_ts"][:] +1 # +1 is for the tree/simulation indexing mismatch
t = np.zeros_like(timesteps)
z = np.zeros_like(timesteps)

dm.jeje_utils.read_conversion_scales(RunDir,timestep_final)
omega_0,lambda_0,little_h = dm.jeje_utils.read_cosmo_params(RunDir,timestep_final)

for n in range(len(z)):
    r2kpc_ideal, redshift, little_h, m2Msun,junk,junk = ur.Set_Units(RunDir, timesteps[n], "ideal")
    z[n] = redshift

    a = 1./(1+redshift)
    t[n], junk = uc.t_Universe(a,omega_0,little_h)


show_snap = False
if show_snap:
    ts_ticks = np.rint(np.arange(timesteps[0],timesteps[-1]+1,timesteps[-1]/20))
    z_ticks = []
    n_ticks = len(ts_ticks)
else:
    z_ticks = np.array([3.0, 3.5, 4.0, 4.5, 5.0, 6.0, 8.0, 10.0])
    n_ticks = len(z_ticks)
tick_labels = []
for m in range(n_ticks):
    if show_snap:
        ok = np.argmin(abs(timesteps-ts_ticks[m]))
        z_ticks.append(z[ok])
        tick_labels.append(str(int(ts_ticks[m])))
    else:
        ok = np.argmin(abs(z -z_ticks[m]))
        tick_labels.append(str(z_ticks[m]))
t_locs, junk = uc.t_Universe(1./(1.+np.array(z_ticks)),omega_0,little_h)

#np.zeros((n_type, n_ts_out, len(r_bins[1:]) ))
f_1st_infall_evo = []
f_past_peri_evo  = []
f_past_apo_evo = []
f_strip_1st_infall_evo = []
f_strip_past_peri_evo = []
f_strip_past_apo_evo = []
f_fb_1st_outflow_evo = []
f_fb_past_peri_evo = []
f_fb_past_apo_evo = []
f_tot_evo = []

n_type = 10
select_types = ["m_tot","m_neut","CollLyaLum","RecLyaLum"]

rascas_name_list = ["cL_h_rec_ism", "cL_h_rec_sat", "cL_h_rec_cgm", "cL_h_col_ism", "cL_h_col_sat", "cL_h_col_cgm"]
select_types += rascas_name_list

for n in range(n_type):

    f_tot_evo.append(mp_group["tot_"+select_types[n]+"_evo"][:])
    
    f_1st_infall_evo.append(mp_group["1st_infall_"+select_types[n]+"_evo"][:])
    f_past_peri_evo.append(mp_group["past_peri_"+select_types[n]+"_evo"][:])
    f_past_apo_evo.append(mp_group["past_apo_"+select_types[n]+"_evo"][:])
    f_strip_1st_infall_evo.append(mp_group["1st_infall_stripped_"+select_types[n]+"_evo"][:])
    f_strip_past_peri_evo.append(mp_group["past_peri_stripped_"+select_types[n]+"_evo"][:])
    f_strip_past_apo_evo.append(mp_group["past_apo_stripped_"+select_types[n]+"_evo"][:])

    # Note called this one inflow in hdf5 by mistake - it is 1st outflow gas
    f_fb_1st_outflow_evo.append(mp_group["1st_infall_fb_"+select_types[n]+"_evo"][:])

    f_fb_past_peri_evo.append(mp_group["past_peri_fb_"+select_types[n]+"_evo"][:])
    f_fb_past_apo_evo.append(mp_group["past_apo_fb_"+select_types[n]+"_evo"][:])
    
# Stack radial bins together for this script - note this only works with radial bins starting at 0.2

def summation( array ):

    array = np.array(array)

    ok = np.isnan(array)
    array[ok] = 0.0
    
    array = np.sum(array,axis=2)

    return array

f_tot_evo = summation(f_tot_evo)
f_1st_infall_evo = summation(f_1st_infall_evo)
f_past_peri_evo  = summation(f_past_peri_evo)
f_past_apo_evo = summation(f_past_apo_evo)
f_strip_1st_infall_evo = summation(f_strip_1st_infall_evo)
f_strip_past_peri_evo = summation(f_strip_past_peri_evo)
f_strip_past_apo_evo = summation(f_strip_past_apo_evo)
f_fb_1st_outflow_evo = summation(f_fb_1st_outflow_evo)
f_fb_past_peri_evo = summation(f_fb_past_peri_evo)
f_fb_past_apo_evo = summation(f_fb_past_apo_evo)

f_strip_evo = f_strip_1st_infall_evo + f_strip_past_peri_evo + f_strip_past_apo_evo
f_fb_evo = f_fb_1st_outflow_evo + f_fb_past_apo_evo + f_fb_past_peri_evo
f_after_infall_evo = f_past_peri_evo + f_past_apo_evo

from utilities_plotting import *

c_list = ["k","b","m","r","g","y","c","firebrick","darkgray","orange"]

top_val = 0.98
if show_snap:
    top_val = 0.94
    
nrow = 2; ncol=2
py.rcParams.update(latexParams)
py.rcParams.update({'figure.subplot.wspace':0.11,'figure.subplot.hspace':0.0,
                    'figure.figsize':[3.32*ncol,2.49*nrow],
                    'figure.subplot.left':0.09,
                    'figure.subplot.bottom':0.09,
                    'figure.subplot.top':top_val,
                    'figure.subplot.right':0.95,
                    'legend.fontsize':6,
                    'font.size':12,
                    'axes.labelsize':11})

py.figure()
subplots = panelplot(nrow,ncol)
for n,ax in enumerate(subplots):
    py.axes(ax)

    ps = 1
    if n == 0:
        
        py.plot(t, np.log10(f_tot_evo[0]), c="k", label="Total")
        py.scatter(t, np.log10(f_tot_evo[0]), c="k",edgecolors="none",s=ps)

        py.plot(t, np.log10(f_strip_evo[0]), c="c", label="Stripped")
        py.scatter(t, np.log10(f_strip_evo[0]), c="c",edgecolors="none",s=ps)

        py.plot(t, np.log10(f_fb_evo[0]), c="r", label="Feedback influenced")
        py.scatter(t, np.log10(f_fb_evo[0]), c="r",edgecolors="none",s=ps)
        
        py.plot(t, np.log10(f_1st_infall_evo[0]), c="b", label="First infall")
        py.scatter(t, np.log10(f_1st_infall_evo[0]), c="b",edgecolors="none",s=ps)

        py.plot(t, np.log10(f_after_infall_evo[0]), c="g", label="Post-pericenter")
        py.scatter(t, np.log10(f_after_infall_evo[0]), c="g",edgecolors="none",s=ps)


        py.legend(frameon=False,loc="upper left")
        py.ylabel(r"$\log(M_{\mathrm{tot}} \, / \mathrm{M_\odot})$")
        xlo = 0.8; xhi = t.max()
        py.xlim((xlo, xhi))
        py.ylim((7.1,9.6))
        #2
        
    if n == 1:

        py.plot(t, np.log10(f_tot_evo[1]), c="k", label="Total")
        py.scatter(t, np.log10(f_tot_evo[1]), c="k",edgecolors="none",s=ps)
        
        py.plot(t, np.log10(f_strip_evo[1]), c="c", label="Stripped")
        py.scatter(t, np.log10(f_strip_evo[1]), c="c",edgecolors="none",s=ps)

        py.plot(t, np.log10(f_fb_evo[1]), c="r", label="Feedback influenced")
        py.scatter(t, np.log10(f_fb_evo[1]), c="r",edgecolors="none",s=ps)
        
        py.plot(t, np.log10(f_1st_infall_evo[1]), c="b", label="First infall")
        py.scatter(t, np.log10(f_1st_infall_evo[1]), c="b",edgecolors="none",s=ps)
        
        py.plot(t, np.log10(f_after_infall_evo[1]), c="g", label="Post-pericenter")
        py.scatter(t, np.log10(f_after_infall_evo[1]), c="g",edgecolors="none",s=ps)

        py.ylabel(r"$\log(M_{\mathrm{neutral}} \, / \mathrm{M_\odot})$")

        py.xlim((0.8, t.max()))
        py.ylim((6.6,9.1))
        #2.5
        
    if n == 2:

        py.plot(t, np.log10(f_tot_evo[2]+f_tot_evo[3]), c="k", label="Total")
        py.scatter(t, np.log10(f_tot_evo[2]+f_tot_evo[3]), c="k",edgecolors="none",s=ps)

        py.plot(t, np.log10(f_strip_evo[2]+f_strip_evo[3]), c="c", label="Stripped")
        py.scatter(t, np.log10(f_strip_evo[2]+f_strip_evo[3]), c="c",edgecolors="none",s=ps)
        
        py.plot(t, np.log10(f_fb_evo[2]+f_fb_evo[3]), c="r", label="Feedback influenced")
        py.scatter(t, np.log10(f_fb_evo[2]+f_fb_evo[3]), c="r",edgecolors="none",s=ps)

        py.plot(t, np.log10(f_1st_infall_evo[2]+f_1st_infall_evo[3]), c="b", label="First infall")
        py.scatter(t, np.log10(f_1st_infall_evo[2]+f_1st_infall_evo[3]), c="b",edgecolors="none",s=ps)
        
        py.plot(t, np.log10(f_after_infall_evo[2]+f_after_infall_evo[3]), c="g", label="Post-pericenter")
        py.scatter(t, np.log10(f_after_infall_evo[2]+f_after_infall_evo[3]), c="g",edgecolors="none",s=ps)
        
        py.ylabel(r"$\log(L_{\mathrm{Ly\alpha}} \, / \mathrm{erg \, s^{-1}})$")
        
        py.xlim((0.8, t.max()))
        
        
        py.ylim((39,41.5))

        #2.5

        py.xlabel(r"$t \, / \mathrm{Gyr}$")

    if n == 3:

        quant1 = np.zeros_like(f_strip_evo[0])
        quant2 = np.zeros_like(f_strip_evo[0])
        quant3 = np.zeros_like(f_strip_evo[0])
        quant4 = np.zeros_like(f_strip_evo[0])
        quant5 = np.zeros_like(f_strip_evo[0])
        
        for i in range(6):

            quant1 += f_strip_evo[4+i]
            quant2 += f_fb_evo[4+i]
            quant3 += f_1st_infall_evo[4+i]
            quant4 += f_after_infall_evo[4+i]
            quant5 += f_tot_evo[4+i]

        py.plot(t, np.log10(quant5), c="k", label="Total")
        py.scatter(t, np.log10(quant5), c="k",edgecolors="none",s=ps)

        py.plot(t, np.log10(quant1), c="c", label="Stripped")
        py.scatter(t, np.log10(quant1), c="c",edgecolors="none",s=ps)
        
        py.plot(t, np.log10(quant2), c="r", label="Feedback-driven")
        py.scatter(t, np.log10(quant2), c="r",edgecolors="none",s=ps)

        py.plot(t, np.log10(quant3), c="b", label="Inflowing 1st-infall")
        py.scatter(t, np.log10(quant3), c="b",edgecolors="none",s=ps)
        
        py.plot(t, np.log10(quant4), c="g", label="Orbiting after 1st-infall")
        py.scatter(t, np.log10(quant4), c="g",edgecolors="none",s=ps)
        
        py.ylabel(r"$\log(L_{\mathrm{Ly\alpha}} \, / \mathrm{erg \, s^{-1}})$")
        
        py.xlim((0.8, t.max()))
        
        py.ylim((39,41.5))

        #2.5
        
        py.xlabel(r"$t \, / \mathrm{Gyr}$")

    if show_snap:
        ax_z = ax.twiny()
        ax_z.set_xticks(t_locs)
        ax_z.set_xticklabels(tick_labels)
        ax_z.set_xlabel(r'$\mathrm{snapshot}$')
        ax_z.set_xlim((xlo,xhi))

fig_name = "cgm_mass_lum_evo_"+Zoom+"_"+Run+".pdf"
py.savefig("../Figures/"+fig_name)
py.show()
