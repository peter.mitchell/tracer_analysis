import numpy as np
import h5py
import utilities_cosmology as uc
import dm_utils as dm

note that c_component_index (i.e. comp) got changed - so this script would needto be updated before itcan be used

File = h5py.File("saved_tracers_select_Zoom-7-5092.hdf5")

tid_select = File["tid"][:]
comp_in       = File["comp"][:]
cv_in         = File["cv"][:]
nh_in         = File["nh"][:]
temp_in       = File["temp"][:]
xhi_in        = File["xhi"][:]
radius_in = File["r"][:]
timestep_in = File["timestep"][:]
family_in = File["family"][:]
radius_p_in = File["r_physical"][:]
radius_norm_in = File["r"][:]
redshift_in = File["redshift"][:]
Lya_in = File["LyaLum"][:]

File.close()
n_select = len(tid_select)

# Get time from redshift
Zoom = "Zoom-7-1290"; Run = "tracer_run_lr"; RunDir = "/cral4/mitchell/data/Tracer_Test/"+ Zoom + "/" + Run
dm.jeje_utils.read_conversion_scales(RunDir,timestep_in[0])
omega_0,lambda_0,little_h = dm.jeje_utils.read_cosmo_params(RunDir,timestep_in[0])
a_in = 1./(1.+redshift_in)
t_in, tlb_in = uc.t_Universe(a_in,omega_0,little_h)

f_strip_evo = np.zeros(len(t_in))
f_fb_evo = np.zeros(len(t_in))
f_1in_evo = np.zeros(len(t_in))
f_pperi_evo = np.zeros(len(t_in))

f_strip_evo_n = np.zeros(len(t_in))
f_fb_evo_n = np.zeros(len(t_in))
f_1in_evo_n = np.zeros(len(t_in))
f_pperi_evo_n = np.zeros(len(t_in))

f_strip_evo_la = np.zeros(len(t_in))
f_fb_evo_la = np.zeros(len(t_in))
f_1in_evo_la = np.zeros(len(t_in))
f_pperi_evo_la = np.zeros(len(t_in))


# Loop over final CGM redshifts

for i_final in range(len(t_in)):

    if i_final < 5:
        continue
    
    comp = comp_in[:i_final+1]
    cv = cv_in[:i_final+1]
    nh = nh_in[:i_final+1]
    temp = temp_in[:i_final+1]
    xhi = xhi_in[:i_final+1]
    radius = radius_in[:i_final+1]
    timestep = timestep_in[:i_final+1]
    family = family_in[:i_final+1]
    radius_p = radius_p_in[:i_final+1]
    radius_norm = radius_norm_in[:i_final+1]
    Lya = Lya_in[:i_final+1]
    
    t = t_in[:i_final+1]
    
    # Get time-averaged radial velocity
    dr = (radius_p[1:] - radius_p[0:-1]) * (3.09*10**16) # km
    dt = (t[1:]-t[0:-1]) * 3.155*10**16 # s

    t_av = 0.5 * (t[0:-1]+t[1:])
    cv_av = dr/np.reshape(dt,(len(dt),1))

    ############ Custom selections ###############
    family_temp = np.copy(family)
    family_temp[np.isnan(family_temp)] = 100

    comp_temp = np.copy(comp)
    comp_temp[np.isnan(comp_temp)] = 0

    # Count consecutive number of outputs a particle is registered as a satellite
    nsat_max = np.zeros(n_select)
    nsat = np.zeros(n_select)
    for n in range(len(comp_temp[:,0])):
        nsat[comp_temp[n]>0] +=1
        nsat[comp_temp[n]==0] = 0
        nsat_max[nsat>nsat_max] = nsat[nsat>nsat_max]
    nsat_final = nsat
    
    cv_temp = np.copy(cv)
    cv_temp[np.isnan(cv_temp)] = 0

    rad_temp = np.copy(radius_norm)
    rad_temp[np.isnan(rad_temp)] = 1e9

    jim = np.max(comp_temp,axis=0)
    tim = family_temp[-1] # I forgot gas call leave stars!
    rmin = np.min(rad_temp,axis=0)
    vrmax = np.max(cv_temp,axis=0)
    vrmin = np.min(cv_temp,axis=0)

    ninhalo = np.ones_like(comp)
    ninhalo[np.isnan(comp)] = 0
    ninhalo = np.sum(ninhalo,axis=0)

    # Count particles that get ejected from the halo, and number of those that return
    accreted = np.zeros(n_select)
    ejected = np.zeros(n_select)
    nej = np.zeros(n_select)
    nret = np.zeros(n_select)-1
    for n in range(len(comp_temp[:,0])):
        accreted[rad_temp[n]<0.9] = 1
        ejected = (accreted==1) & (rad_temp[n]>1)
        accreted[ejected] = 0

        nej[ejected] += 1
        if n > 0:
            nret[(rad_temp[n]<0.9) & (rad_temp[n-1]>0.9)] +=1
        else:
            nret[(rad_temp[n]<0.9)] += 1

    
    nsat_thr = 2

    # Identify stripped particles that have not been in the ISM (or back into satellite) after being stripped
    nsat = np.zeros(n_select)
    n_diff_cgm = np.zeros(n_select)
    n_diff_cgm_max = np.zeros(n_select)
    stripped = np.zeros(n_select)
    nism = np.zeros(n_select)
    was_in_ism = np.zeros(n_select)

    ninflow = np.zeros(n_select)
    noutflow = np.zeros(n_select)
    mono_inflow = np.zeros(n_select)-1

    noutflow_max = np.zeros(n_select)

    # This was is for detecting past-peri particles
    dv_max = np.zeros(n_select)
    logdT_at_dv_max = np.zeros(n_select)
    r_at_dv_max = np.zeros(n_select)
    ts_dv_max = np.zeros(n_select)
    dEk_dv_max = np.zeros(n_select)
    rmax_after_dvmax = np.zeros(n_select)

    # This is one is for detecting fb outflows
    dv_max2 = np.zeros(n_select)
    logdT_at_dv_max2 = np.zeros(n_select)
    r_at_dv_max2 = np.zeros(n_select)
    ts_dv_max2 = np.zeros(n_select)

    static = np.zeros(n_select)

    for n in range(len(comp_temp[:,0])):
        nsat[comp_temp[n]>0] +=1
        
        n_diff_cgm[(comp_temp[n]==0) & (rad_temp[n]<=1.0)] +=1
        n_diff_cgm[comp_temp[n]>0] = 0
    
        in_sat = nsat>2
        stripped[in_sat] = 0
    
        stripped[in_sat & (n_diff_cgm>2) & (n_diff_cgm_max<3)] += 1
        nsat[(comp_temp[n]==0) & (n_diff_cgm>2)] = 0

        n_diff_cgm_max = np.maximum(n_diff_cgm, n_diff_cgm_max)

        nism[rad_temp[n]<0.2] +=1
        nism[rad_temp[n]>0.2] = 0
        stripped[nism>1] = 0

        was_in_ism[(nism==5) | (rad_temp[n]<0.1)] += 1

        if n > 0:
            check = (was_in_ism > 0) & (rad_temp[n-1]<0.2) & (rad_temp[n]>0.2) & (comp_temp[n]>0)
            was_in_ism[check] = 0
    
        ninflow[cv_temp[n] < 0] += 1
        ninflow[cv_temp[n]>0] = 0
        noutflow[cv_temp[n] > 0] += 1
        noutflow[cv_temp[n]<0] = 0
        
        noutflow_max = np.maximum(noutflow, noutflow_max)
    
        mono_inflow[(ninflow>2)&(mono_inflow==-1)] = 1
        mono_inflow[noutflow>1] = 0

        if n > 0:
            dv = cv_temp[n] - cv_temp[n-1]

            greater = (dv>dv_max) & (cv_temp[n]!=0) & (cv_temp[n-1]!=0) & (cv_temp[n]>0)
            logdT_at_dv_max[greater] = (np.log10(temp[n]) - np.log10(temp[n-1]))[greater]
            dv_max[greater] = dv[greater]
            r_at_dv_max[greater] = rad_temp[n][greater]
            ts_dv_max[greater] = n
            dEk_dv_max[greater] = (cv_temp[n]**2 - cv_temp[n-1]**2)[greater]
            rmax_after_dvmax[greater] = np.max(rad_temp[n:],axis=0)[greater]
        
            greater = (dv>dv_max) & (cv_temp[n]!=0) & (cv_temp[n-1]!=0)
            logdT_at_dv_max2[greater] = (np.log10(temp[n]) - np.log10(temp[n-1]))[greater]
            dv_max2[greater] = dv[greater]    
            r_at_dv_max2[greater] = rad_temp[n][greater]
            ts_dv_max2[greater] = n

               
        if n == len(comp_temp[:,0])-1:
            static = (rad_temp[n] < 1.0) & (rad_temp[n] > rad_temp[n-5]-0.025) & (rad_temp[n] < rad_temp[n-5]+0.025) & (rad_temp[n]>0.2)
            if len(static[static]) > 0:
                for i in range(4):
                    static[(rad_temp[n][static] < rad_temp[n-4+i][static]-0.025) | (rad_temp[n][static] > rad_temp[n-4+i][static]+0.025) ] = False
                    
        
        #test = 617
        #print comp[n,test], nsat[test], stripped[test], nism[test], n_diff_cgm[test], in_sat[test]
    
    # correction for mono_inflow forparticles that are first being accreted
    correct = (mono_inflow==1) | ((vrmax < 0.001) & (vrmin<-30) & (ninhalo<3))
    mono_inflow[correct] = 1


    cgm_final = (rad_temp[-1]>0.2)&(rad_temp[-1]<1.0) & (tim==100)&(nsat_final<=nsat_thr)

    a = (stripped>0) & cgm_final
    b = cgm_final&(was_in_ism>0)
    c = cgm_final&(mono_inflow==1)&(was_in_ism==0)&(stripped==0)&(vrmin < -30)&(nej==0)
    d = cgm_final & (ninhalo<=10) & (vrmin >= -30)

    e = cgm_final & (dv_max>20) & (logdT_at_dv_max<0.5) & (rmax_after_dvmax >0.2 ) & (vrmax>10) & (vrmax < 150) & (r_at_dv_max<0.5)
    
    f = cgm_final & (dv_max>30) & (logdT_at_dv_max2>0.3) & (vrmax > 50) & ((rmin > 0.2) | (was_in_ism==0))
    f = f | ( cgm_final & (vrmax > abs(vrmin)*1.5) & (vrmax > 100) & (was_in_ism==0))
    
    g = cgm_final & static & (a==False) & (b==False) & (c==False) & (d==False) & (e==False) & (f==False)

    h = cgm_final & (a==False) & (b==False) & (c==False) & (d==False) & (e==False) & (f==False) & (g==False)

    f_strip_evo[i_final] = len(jim[a]) / float(len(jim[cgm_final]))
    f_fb_evo[i_final] = len(jim[b&(e==False)]) / float(len(jim[cgm_final])) + len(jim[f]) / float(len(jim[cgm_final]))
    f_1in_evo[i_final] = len(jim[c]) / float(len(jim[cgm_final]))
    f_pperi_evo[i_final] = len(jim[e])/ float(len(jim[cgm_final]))

    xhi_final = xhi[-1]
    xhi_final[np.isnan(xhi_final)==0]
    
    f_strip_evo_n[i_final] = np.sum(xhi_final[a]) / float(np.sum(xhi_final[cgm_final]))
    f_fb_evo_n[i_final] = np.sum(xhi_final[b&(e==False)]) / float(np.sum(xhi_final[cgm_final])) + np.sum(xhi_final[f]) / float(np.sum(xhi_final[cgm_final])) 
    f_1in_evo_n[i_final] =np.sum(xhi_final[c]) / float(np.sum(xhi_final[cgm_final]))
    f_pperi_evo_n[i_final] =np.sum(xhi_final[e])/ float(np.sum(xhi_final[cgm_final]))

    Lya_final = Lya[-1]
    Lya_final[np.isnan(Lya_final)==0]

    f_strip_evo_la[i_final] = np.sum(Lya_final[a]) / float(np.sum(Lya_final[cgm_final]))
    f_fb_evo_la[i_final] = np.sum(Lya_final[b&(e==False)]) / float(np.sum(Lya_final[cgm_final])) + np.sum(Lya_final[f]) / float(np.sum(Lya_final[cgm_final])) 
    f_1in_evo_la[i_final] =np.sum(Lya_final[c]) / float(np.sum(Lya_final[cgm_final]))
    f_pperi_evo_la[i_final] =np.sum(Lya_final[e])/ float(np.sum(Lya_final[cgm_final]))

    
    '''print "fraction of final CGM that has been stripped from satellites", len(jim[a]) / float(len(jim[cgm_final]))
    #print "fraction of final CGM that came from the central ISM", len(jim[b]) / float(len(jim[cgm_final]))
    print "fraction of final CGM was ejected from central ISM as fb", len(jim[b&(e==False)]) / float(len(jim[cgm_final]))
    print "fraction of final CGM that is diffuse, and inflowing for first time", len(jim[c]) / float(len(jim[cgm_final]))
    print "fraction of final CGM classed as past-peri", len(jim[e])/ float(len(jim[cgm_final]))
    print "fraction of final CGM loaded into Fb-driven outflow on CGM scales", len(jim[f]) / float(len(jim[cgm_final]))
    print "fraction of final CGM that is static (but not anything else) at the end", len(jim[g]) / float(len(jim[cgm_final]))
    print "fraction of final CGM classed as transient", len(jim[d]) / float(len(jim[cgm_final]))
    print "fraction of final CGM unclassified", len(jim[h]) / float(len(jim[cgm_final]))

    print ""
    print "now for neutral CGM"

    xhi_final = xhi[-1]
    xhi_final[np.isnan(xhi_final)==0]

    print "fraction of final CGM that has been stripped from satellites", np.sum(xhi_final[a]) / float(np.sum(xhi_final[cgm_final]))
    #print "fraction of final CGM that came from the central ISM", np.sum(xhi_final[b]) / float(np.sum(xhi_final[cgm_final]))
    print "fraction of final CGM was ejected from central ISM as fb", np.sum(xhi_final[b&(e==False)]) / float(np.sum(xhi_final[cgm_final]))
    print "fraction of final CGM that is diffuse, and inflowing for first time", np.sum(xhi_final[c]) / float(np.sum(xhi_final[cgm_final]))
    print "fraction of final CGM classed as past-peri", np.sum(xhi_final[e])/ float(np.sum(xhi_final[cgm_final]))
    print "fraction of final CGM loaded into Fb-driven outflow on CGM scales", np.sum(xhi_final[f]) / float(np.sum(xhi_final[cgm_final]))
    print "fraction of final CGM that is static (but not anything else) at the end", np.sum(xhi_final[g]) / float(np.sum(xhi_final[cgm_final]))
    print "fraction of final CGM classed as transient", np.sum(xhi_final[d]) / float(np.sum(xhi_final[cgm_final]))
    print "fraction of final CGM unclassified", np.sum(xhi_final[h]) / float(np.sum(xhi_final[cgm_final]))'''

from utilities_plotting import *

c_list = ["k","b","m","r","g","y","c","firebrick","darkgray","orange"]

py.figure(figsize=(30,10))
py.subplot(311)

py.plot(t, f_strip_evo, c="k", label="Stripped")
py.scatter(t, f_strip_evo, c="k",edgecolors="none")

py.plot(t, f_fb_evo, c="r", label="Feedback-driven")
py.scatter(t, f_fb_evo, c="r",edgecolors="none")

py.plot(t, f_1in_evo, c="b", label="Inflowing 1st-passage")
py.scatter(t, f_1in_evo, c="b",edgecolors="none")

py.plot(t, f_pperi_evo, c="g", label="Orbiting past-pericenter")
py.scatter(t, f_pperi_evo, c="g",edgecolors="none")


py.legend()
py.xlabel("t")
py.ylabel(r"$f_{\mathrm{comp,tot}}$")
py.xlim((t.min(), t.max()))
py.ylim((0.0,1.0))


py.subplot(312)

py.plot(t, f_strip_evo_n, c="k", label="Stripped")
py.scatter(t, f_strip_evo_n, c="k",edgecolors="none")

py.plot(t, f_fb_evo_n, c="r", label="Feedback-driven")
py.scatter(t, f_fb_evo_n, c="r",edgecolors="none")

py.plot(t, f_1in_evo_n, c="b", label="Inflowing 1st-passage")
py.scatter(t, f_1in_evo_n, c="b",edgecolors="none")

py.plot(t, f_pperi_evo_n, c="g", label="Orbiting past-pericenter")
py.scatter(t, f_pperi_evo_n, c="g",edgecolors="none")

py.legend()
py.xlabel("t")
py.ylabel(r"$f_{\mathrm{comp,neutral}}$")
py.xlim((t.min(), t.max()))
py.ylim((0.0,1.0))

py.subplot(313)

py.plot(t, f_strip_evo_la, c="k", label="Stripped")
py.scatter(t, f_strip_evo_la, c="k",edgecolors="none")

py.plot(t, f_fb_evo_la, c="r", label="Feedback-driven")
py.scatter(t, f_fb_evo_la, c="r",edgecolors="none")

py.plot(t, f_1in_evo_la, c="b", label="Inflowing 1st-passage")
py.scatter(t, f_1in_evo_la, c="b",edgecolors="none")

py.plot(t, f_pperi_evo_la, c="g", label="Orbiting past-pericenter")
py.scatter(t, f_pperi_evo_la, c="g",edgecolors="none")

py.legend()
py.xlabel("t")
py.ylabel(r"$f_{\mathrm{comp,Ly}\alpha}$")
py.xlim((t.min(), t.max()))
py.ylim((0.0,1.0))



fig_name = "basic_cgm_fraction_evo.pdf"
py.savefig("Figures/"+fig_name)
py.show()
