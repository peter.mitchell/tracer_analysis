import matplotlib.pyplot as py
import numpy as np
import h5py

# Outflow examples
#i_part = 781 # cool case of particle that has delta function spikes, but because it's launched across a cavity into a shell
#i_part = 619 # this particle get's launched at high T from central ISM, nice example of gradual but rapid heating - also density maps show particle escapes along a cavity

i_part = 310

outpath = "/cral4/mitchell/data/Tracer_Test/PostProcess/Maps/"

#Zoom = "Zoom-7-1290_tracer_run_lr"
timesteps_show = np.arange(10,140)

Zoom = "Zoom-7-5092"
Run = "RhdRun-tracer_norad_bug"
timesteps_show = np.arange(10,105)

File = h5py.File("saved_tracers_select_"+Zoom+".hdf5")
tx = File["x"][:]
ty = File["y"][:]
tz = File["z"][:]
timesteps = File["timestep"][:]
File.close()
n_select = len(tx[0])

first_ts = np.where(np.isnan(tx[:,i_part])==False)[0]

tx *= 0.31
ty *= 0.31
tz *= 0.31

# Empirically found halo centres and virial radii
panel1_x = 0.718; panel1_y = 0.5; rvir = 0.31
panel2_x = 1.539
panel3_x = 2.359

py.rcParams.update({'figure.subplot.wspace':0,'figure.subplot.hspace':0,
                    'figure.figsize':[12,4],
                    'figure.subplot.left':0.00,
                    'figure.subplot.bottom':0.0,
                    'figure.subplot.top':1.0,
                    'figure.subplot.right':1.0,
                    'font.size':6,
                    'axes.labelsize':8,
                    'legend.fontsize':7})


def Plot_Circle(x0,y0,r,ax,c="k"):
    theta = np.arange(0,2*np.pi+0.01,0.01)
    x = x0 + np.cos(theta)*r
    y = y0 + np.sin(theta)*r
    Circle = ax.plot(x,y,c=c,linewidth=0.3)
    return Circle

py.ion()
fig, ax = py.subplots(dpi=160)#figsize=(6,2))

for timestep in timesteps_show:

    i_show = np.where(timesteps==timestep)[0]

    if i_show < first_ts[0]-2:
        continue
    
    print timestep
    
    py.clf()
    
    im_path = outpath + Zoom + "_" + Run + "/all_ts/rho_gas_tot/rho_gas_tot_ts_"
    im_path += str(timestep)+".png"
    img_in = py.imread(im_path)
    

    ax = py.gca()
    img = ax.imshow(img_in)
    img.set_extent((0,3,0,1))        
    
    py.plot(tx[:i_show+1,i_part]+panel1_x, ty[:i_show+1,i_part]+ panel1_y, c="k")
    py.scatter(tx[i_show,i_part]+panel1_x, ty[i_show,i_part]+ panel1_y)
    
    py.plot(tz[:i_show+1,i_part]+panel2_x, tx[:i_show+1,i_part]+ panel1_y, c="k")
    py.scatter(tz[i_show,i_part]+panel2_x, tx[i_show,i_part]+ panel1_y)

    py.plot(ty[:i_show+1,i_part]+panel3_x, tz[:i_show+1,i_part]+ panel1_y, c="k")
    py.scatter(ty[i_show,i_part]+panel3_x, tz[i_show,i_part]+ panel1_y)

    py.xlim((0.15,2.95))
    py.ylim((0.033,0.967))
    
    py.draw()

    fig.canvas.flush_events()
    
py.ioff()

py.show()
