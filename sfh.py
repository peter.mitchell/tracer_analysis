import numpy as np
import h5py
from utilities_plotting import *
from utilities_statistics import *
import dm_utils as dm
import sys
import utilities_cosmology as uc
sys.path.append("Obs_Data")

#Zoom_List = ["Zoom-7-1290"]
#Run = "tracer_run_lr"

'''Zoom_List = ["Zoom-7-5092"]
Run = "hdRun-LSS150pkpc_ncycle_1112_MSN_5"
RunDir = "/cral4/simulations/P13-20h-1Mpc-MUSIC/"
OutDir = "/cral4/mitchell/data/Sphinx_Prep/PostProcess/"
timestep_list = [155]'''

'''Zoom_List = ["Zoom-7-5092"]
Run = "RhdRun-tracer_norad_bug"
RunDir = "/cral2/mitchell/"
OutDir = "/cral4/mitchell/data/Tracer_Test/PostProcess/"
timestep_list = [105]'''

'''Zoom_List = ["Zoom-7-5092"]
Run = "RhdRun-tracer_radfix"
RunDir = "/cral2/mitchell/"
OutDir = "/cral2/mitchell/PostProcess/"
timestep_list = [107]'''

Zoom_List = ["Zoom-7-5092"]
Run = "RhdRun-tracer_radfix"
RunDir = "/cral2/mitchell/"
OutDir = "/cral2/mitchell/PostProcess/"
timestep_list = [155]

Run_List = [Run]
name_list = ["18367, fb"]
xlo = 0.8; xhi = 2.15
yhi = 0.0; yhi = 12.2

#RunDir = "/cral2/blaizot/SIMULATIONS/P13-20h-1Mpc-MUSIC/"
#RunDir = "/cral2/mitchell/data/Sphinx_Prep/"
#RunDir = "/cral4/mitchell/data/Tracer_Test/"

#OutDir = "/cral2/mitchell/data/Temp4/"
#OutDir = "/cral2/mitchell/data/Temp_rvir_0p5/"

show_snap = False

if show_snap:
    fig_name = "sfh_outflows_"+Zoom_List[0]+"_snap.pdf"
else:
    fig_name = "sfh_outflows_"+Zoom_List[0]+"_z.pdf"

sfr_aperture = 0.2
sfr_ap_str = "sfh_r" + str(sfr_aperture).replace(".","p")

sfr_aperture_list = [0.2,0.5]
sfr_timescale = 10.0 # Myr

flux_aperture_list = [0.25,0.5,0.75,1.0]
linestyle_list = ["--","--","--","--"]

c_in_list = ["b","b","b","b"]
c_out_list = ["r","r","r","m"]

show_in_list = [False,True,False,False]
show_out_list = [False,True,False,False]

flux_shell_list      = [1.0]
linestyle_shell_list = ["-"]
c_in_shell_list      = ["c"]
c_out_shell_list     = ["y"]

show_in_shell_list  = [False]
show_out_shell_list = [False]

#show_vlines = [115, 125]
show_vlines = []

show_sub = True

show_halo = False # Plot dm/dt of fb x M_200 for the halo

show_fills = True

nrow = len(Run_List); ncol = 1

sfh_list = []
age_list = []
sfr_list = []

z_flux_list = []
ts_flux_list = []
mdot_in_list = []
mdot_out_list = []
mdot_in_es_list = []
mdot_out_es_list = []
mdot_in_shell_list = []
mdot_out_shell_list = []

for Zoom, Run, timestep_final in zip(Zoom_List, Run_List, timestep_list):

    RunDir += Zoom+"/"+Run

    dm.jeje_utils.read_conversion_scales(RunDir,timestep_final)

    omega_0,lambda_0,little_h = dm.jeje_utils.read_cosmo_params(RunDir,timestep_final)
    omega_b = dm.jeje_utils.get_param_real(RunDir,timestep_final,"omega_b")
    fb = omega_b / omega_0
    
    # Read the hdf5 file
    filename = Zoom +"_" +Run+"_tsf" + str(timestep_final)+ ".hdf5"
    File = h5py.File(OutDir+filename)
    zoom_group = File[Zoom]

    for halo_group_str in zoom_group:
        # Only read the main halo
        if not "main" in halo_group_str:
            continue

        halo_group = zoom_group[halo_group_str]

        # Read halo mass
        mp_group = halo_group["main_progenitors"]
        m200 = mp_group["m200_max_past"][:]
        
        # Read star formation rates
        sfr_list_i = []
        for sfr_ap in sfr_aperture_list:
            string = "sfr_r"+str(sfr_ap).replace(".","p")+"_"+str(sfr_timescale).replace(".","p")
            sfr_list_i.append(mp_group[string][:])

        # Read star formation histories
        sfh_group  = halo_group[sfr_ap_str]

        sfh_list.append(sfh_group["sfr"][:])
        age_list.append(sfh_group["age"][:])

        # Read flux histories
        flux_str   = "fluxes/flux_history"
        flux_group = halo_group[flux_str]

        z_flux = flux_group["redshift"][:]
        ts_flux = flux_group["ts"][:]

        mdot_in_i = []
        mdot_out_i = []

        mdot_es_in_i = []
        mdot_es_out_i = []

        mdot_in_shell_i = []
        mdot_out_shell_i = []

        for flux_ap in flux_aperture_list:           
            flux_ap_str = "r_surface_"+str(flux_ap).replace(".","p")
            flux_ap_group = flux_group[flux_ap_str]

            mdot_in_i.append(flux_ap_group["dmdt_inflow"][:])
            mdot_out_i.append(flux_ap_group["dmdt_outflow"][:])

            mdot_es_in_i.append(flux_ap_group["dmdt_inflow_exc_sub"][:])
            mdot_es_out_i.append(flux_ap_group["dmdt_outflow_exc_sub"][:])

        for flux_shell in flux_shell_list:
            flux_shell_str = "r_shell_"+str(flux_shell).replace(".","p")
            flux_shell_group = flux_group[flux_shell_str]

            mdot_in_shell_i.append(flux_shell_group["dmdt_inflow"][:])
            mdot_out_shell_i.append(flux_shell_group["dmdt_outflow"][:])

        z_flux_list.append(z_flux)
        ts_flux_list.append(ts_flux)
        mdot_in_list.append(mdot_in_i)
        mdot_out_list.append(mdot_out_i)
        mdot_in_es_list.append(mdot_es_in_i)
        mdot_out_es_list.append(mdot_es_out_i)

        mdot_in_shell_list.append(mdot_in_shell_i)
        mdot_out_shell_list.append(mdot_out_shell_i)

        sfr_list.append(sfr_list_i)

py.rcParams.update(latexParams)
py.rcParams.update({'figure.figsize':[6.64,2.49],
                    'figure.subplot.left':0.08,
                    'figure.subplot.bottom':0.18,
                    'figure.subplot.top':0.85,
                    'figure.subplot.right':0.98,
                    'legend.fontsize':10,
                    'font.size':12,
                    'axes.labelsize':12})
py.figure()
subplots = panelplot(nrow,ncol)
for n,ax in enumerate(subplots):
    py.axes(ax)

    
    # Compute final output time for star formation histories
    dm.jeje_utils.read_conversion_scales(RunDir,timestep_list[n])
    omega_0,lambda_0,little_h = dm.jeje_utils.read_cosmo_params(RunDir,timestep_list[n])
    z_final = dm.jeje_utils.get_snap_redshift(RunDir,timestep_list[n])
    a_final = 1./(1.+z_final)
    t_final, tlb_final = uc.t_Universe(a_final,omega_0,little_h)

    omega_0,lambda_0,little_h = dm.jeje_utils.read_cosmo_params(RunDir,timestep_final)
    omega_b = dm.jeje_utils.get_param_real(RunDir,timestep_final,"omega_b")
    fb = omega_b / omega_0

    if show_snap:
        ts_ticks = np.arange(ts_flux_list[n][0],timestep_list[n]+1,timestep_list[n]/20)
        z_ticks = []
        n_ticks = len(ts_ticks)
    else:
        z_ticks = np.array([3.0, 3.5, 4.0, 4.5, 5.0, 6.0, 8.0, 10.0])
        n_ticks = len(z_ticks)
    tick_labels = []
    for m in range(n_ticks):
        if show_snap:
            ok = np.argmin(abs(ts_flux_list[n]-ts_ticks[m]))
            z_ticks.append(z_flux_list[n][ok])
            tick_labels.append(str(int(ts_ticks[m])))
        else:
            ok = np.argmin(abs(z_flux_list[n] -z_ticks[m]))
            tick_labels.append(str(z_ticks[m]))
    t_locs, junk = uc.t_Universe(1./(1.+np.array(z_ticks)),omega_0,little_h)
    
    # Compute times for flux histories
    z_flux = z_flux_list[n]
    
    t_flux = np.zeros_like(z_flux)
    for i, z in enumerate(z_flux):
        a = 1./(1.+z)
        t_flux[i], tlb = uc.t_Universe(a,omega_0,little_h)
        
    dt = t_flux[1:] - t_flux[0:-1]
    
    # Compute smoothed halo MAH
    mah_timescale = 0.2 # Gyr - timescale delta t you want to compute delta M / delta t
    t_sm = np.arange(t_flux[0], t_flux[-1], mah_timescale)

    m200_sm = Linear_Interpolation(t_flux, m200, t_sm)


    dm200dt = fb*(m200_sm[1:] - m200_sm[0:-1]) / mah_timescale * 1-9 # Mdot yr^-1
    t_mid = 0.5*(t_sm[1:] +t_sm[0:-1])

    if show_halo:
        py.plot(t_mid, dm200dt * 1e-9, c = "g", linestyle='-',linewidth=1.2,label=r"$f_{\mathrm{B}} \frac{\mathrm{d}M_{\mathrm{200}}}{\mathrm{d}t}$")

    # Plot surface fluxes
    for i, flux_ap in enumerate(flux_aperture_list):
        if show_in_list[i]:
            if show_sub:
                py.plot(t_flux, mdot_in_list[n][i]*1e-9,c=c_in_list[i],linestyle=linestyle_list[i],linewidth=1.2)
            py.scatter(t_flux, mdot_in_es_list[n][i]*1e-9,c=c_in_list[i],edgecolors="none",s=3) 
            py.plot(t_flux, mdot_in_es_list[n][i]*1e-9,c=c_in_list[i],linestyle='-',linewidth=1.2,label=r"Inflowing flux, $r=$"+str(flux_aperture_list[i])+"$r_{\mathrm{vir}}$")

        if show_out_list[i]:
            if show_sub:
                py.plot(t_flux, mdot_out_list[n][i]*1e-9,c=c_out_list[i],linestyle=linestyle_list[i],linewidth=1.2)
            py.scatter(t_flux, mdot_out_es_list[n][i]*1e-9,c=c_out_list[i],edgecolors="none",s=3)
            py.plot(t_flux, mdot_out_es_list[n][i]*1e-9,c=c_out_list[i],linestyle='-',linewidth=1.2, label=r"Outflowing flux, $r=$"+str(flux_aperture_list[i])+"$r_{\mathrm{vir}}$")
            
            #choose_surface = 1
            #if show_fills and i == choose_surface:
            #    py.fill_between(t_flux,np.zeros_like(t_flux),mdot_out_es_list[n][i]*1e-9,alpha=0.3,facecolor='r')


        dt_sfh = -(t_final - age_list[n]*1e-3)[1:] + (t_final - age_list[n]*1e-3)[0:-1]

        mdot_in = 0.5*(mdot_in_es_list[n][i][1:] + mdot_in_es_list[n][i][0:-1])
        mdot_out = 0.5*(mdot_out_es_list[n][i][1:] + mdot_out_es_list[n][i][0:-1])
        print ""
        print "integrated mass through the surface, ",flux_ap," in inflows was", np.sum(dt * mdot_in)
        print "integrated mass through the surface, ",flux_ap," in outflows was", np.sum(dt * mdot_out)
        print "integrated sfr is", np.sum(0.5*(sfh_list[n][0:-1]+sfh_list[n][1:])*1e3 * dt_sfh)
        print "ratio in/fb*hm", np.sum(dt * mdot_in)/(fb*m200[-1])
        print "ratio out/fb*hm", np.sum(dt * mdot_out)/(fb*m200[-1])
        print "ratio out/sm", np.sum(dt * mdot_out)/np.sum(0.5*(sfh_list[n][0:-1]+sfh_list[n][1:])*1e3 * dt_sfh)

        print "comp radbug", np.log10(np.sum(  (0.5*(sfh_list[n][0:-1]+sfh_list[n][1:])*1e3 * dt_sfh)[::-1][0:104] ))
        print "in/fb hm", np.sum( (dt * mdot_in)[0:94] )/(fb*m200[93])
        print "out/fb hm", np.sum( (dt * mdot_out)[0:94] )/(fb*m200[93])
        

    # Plot shell fluxes
    for i, flux_shell in enumerate(flux_shell_list):
        if show_in_shell_list[i]:
            py.scatter(t_flux, mdot_in_shell_list[n][i]*1e-9,c=c_in_shell_list[i],edgecolors="none",s=5)
        if show_out_shell_list[i]:
            py.scatter(t_flux, mdot_out_shell_list[n][i]*1e-9,c=c_out_shell_list[i], edgecolors="none",s=5)

    # Plot sfh
    #py.plot(t_final - age_list[n]*1e-3, sfh_list[n]*1e-6, label="Star formation history",c="k",linewidth=1.2)
     
    #py.plot(t_flux, sfr_list[0][0]*1e-6, c="k", linewidth=1.2,label="Star formation rate, $r<0.2 R_{\mathrm{vir}}$")
    #py.plot(t_flux, sfr_list[0][1]*1e-6, c="k", linewidth=0.8,label="Star formation rate, $r<0.5 R_{\mathrm{vir}}$",alpha=0.7)
               
    if show_fills:
        py.fill_between(t_final-age_list[n]*1e-3,np.zeros_like(sfh_list[n]),sfh_list[n]*1e-6,alpha=0.3,facecolor='k')

    if n >= ncol*nrow- ncol:
        py.xlabel(r'$t \, / \mathrm{Gyr}$')
    if n % ncol == 0:
        py.ylabel(r'$\dot{M} \, / \rm{Msun \, yr^{-1}}$')

for n,ax in enumerate(subplots):
    py.axes(ax)
    py.xlim((xlo,xhi))
    py.ylim((-0.01*yhi,yhi))
    
    if n == 0:
        py.legend(loc="upper left",frameon=False,numpoints=1)
    
    if n == 0 or n==1:
        ax_z = ax.twiny()
        ax_z.set_xticks(t_locs)
        ax_z.set_xticklabels(tick_labels)
        if show_snap:
            ax_z.set_xlabel(r'$\mathrm{snapshot}$')
        else:
            ax_z.set_xlabel(r'$z$')
        ax_z.set_xlim((xlo,xhi))

    if len(show_vlines)>0:
        for vline in show_vlines:
            ind_line = np.argmin(abs(vline - ts_flux_list[n]))
            z_line = z_flux_list[n][ind_line]
            a_line = a = 1./(1.+z_line)
            t_line, junk = uc.t_Universe(a_line,omega_0,little_h)
            py.axvline(t_line, c="k", linewidth = 0.7, linestyle='--')

py.savefig("Figures/"+fig_name)
py.show()
