This script is now defunct - replaced by write_tracer_link_cell_photon.py

import numpy as np

'''Zoom = "Zoom-7-1290"
Run = "tracer_run_lr"
timestep_final = 155
timesteps_show = np.arange(10,timestep_final+1)
RT = False
RunDir = "/cral4/mitchell/data/Tracer_Test/"+ Zoom + "/" + Run
InDir = "/cral2/mitchell/PostProcess/"
OutDir = "/cral4/mitchell/data/Tracer_Test/PostProcess/"'''


'''Zoom = "Zoom-7-5092"
Run = "RhdRun-tracer_norad_bug"
timestep_final = 105
timesteps_show = np.arange(10,timestep_final+1)
RT = True'''
#RunDir = "/cral2/mitchell/"+Zoom+"/"+Run
#InDir = "/cral2/mitchell/PostProcess/"
#OutDir = "/cral2/mitchell/PostProcess/"

Zoom = "Zoom-7-5092"
Run = "RhdRun-tracer_radfix"
#timestep_final = 107
timestep_final = 130
timesteps_show = np.arange(3,timestep_final+1)
RT = True
RunDir = "/cral2/mitchell/"+Zoom+"/"+Run
InDir = "/cral2/mitchell/PostProcess/"
OutDir = "/cral2/mitchell/PostProcess/"

downsample = True
n_downsample = "1000"
#n_downsample = "3e6"

if downsample:
    batch_no = 1

    filename = "saved_tracers_select_"+Zoom+"_"+Run + "_ds_"+ n_downsample + ".hdf5"
    filenames_list = [filename]
else:
    import os
    files = os.listdir(InDir)
    filename_base = "saved_tracers_select_"+Zoom+"_"+Run
    
    filenames_list = []
    for file_n in files:
        if filename_base in file_n and "subset" in file_n:
            filenames_list.append(file_n)
    batch_no = len(filenames_list)



HaloDir = RunDir + "/Halos/"
TreeDir = RunDir + "/Trees/"

import sys
import dm_utils as dm
from scipy import spatial
import h5py
import utilities_ramses as ur
import neighbour_utils as nu
import match_searchsorted as ms
import cutils as cut
import flux_utils

for ifile, filename in enumerate(filenames_list):

    if not downsample:
        print "Processing tracer subset", ifile, "of", batch_no
    
    File = h5py.File(InDir+filename)

    tid_select = File["tid"][:]
    File.close()
    n_select = len(tid_select)
    
    ############## Zoom file IO #########################
    # Read in relevant tree information from the hdf5 file
    filename_zoom = Zoom + "_" + Run + "_tsf" + str(timestep_final) +".hdf5"
    
    File = h5py.File(OutDir+filename_zoom)
    
    # Open the subvolume file (containing all cell data)
    subvol_filename = "subvol_"+filename_zoom
    subvol_File = h5py.File(OutDir+subvol_filename)

    # Check the file with the tree information already exists
    if not Zoom in File:
        print "Error: Tree information for ", Zoom, " in ", filename_zoom, " cannot be read"
        exit()

    zoom_group = File[Zoom]

    # Loop over timesteps
    for i_timestep, timestep in enumerate(timesteps_show):
        timestep_tree = timestep -1 # Stupid mismatch between merger tree files and everything else
        
        # Get conversions for different units
        r2kpc, redshift, little_h, m2Msun,rho2msunpkpc3,v2kms = ur.Set_Units(RunDir, timestep)
        boxlen_pkpc = dm.jeje_utils.get_param_real(RunDir,timestep,'boxlen') * r2kpc # Box size in proper kpc, using the correct box size

        print "reading cell data at timestep = ", timestep, "of", timestep_final, ", redshift is", redshift
    
        ########## Read tracer data ###################
        # Hdf5 group for this timestep (contains cell data)
        ts_group = subvol_File["timestep_"+str(timestep)]
    
        tracer_group = ts_group["Tracer_particle_data"]
        
        tx = tracer_group["x_tr"][:]
        ty = tracer_group["y_tr"][:]
        tz = tracer_group["z_tr"][:]
        fam_tr = tracer_group["fam_tr"][:]
        id_tr = tracer_group["id_tr"][:]
        tmass = tracer_group["mass_tr"][:]

        tmass *= m2Msun
    
        ########## Read cell data #########################
        cell_group = ts_group["Gas_leaf_cell_data"]
        rho_snap = cell_group["rho_c"][:] # Gas density
        pre_snap = cell_group["pre_c"][:] # Gas pressure
        cx = cell_group["x_c"][:] # Cell coordinates
        cy = cell_group["y_c"][:]
        cz = cell_group["z_c"][:]
        cvx = cell_group["vx_c"][:]
        cvy = cell_group["vy_c"][:]
        cvz = cell_group["vz_c"][:]
        clev = cell_group["lev"][:] # Cell refinement level
        c_component_index = cell_group["component_index"][:]

        csize = boxlen_pkpc / 2.**clev # proper kpc
        cmass = rho_snap * rho2msunpkpc3 * csize**3
    
        if RT:
            xH1 = cell_group["xH1"][:]
            xHe1 = cell_group["xHe1"][:]
            xHe2 = cell_group["xHe2"][:]

        # Add stars and dark matter to estimate the specific gravitional potential energy of each tracer
        star_group = ts_group["Stellar_particle_data"]
        sx = star_group["x_s"][:]
        sy = star_group["y_s"][:]
        sz = star_group["z_s"][:]
        sm = star_group["m_s"][:] * m2Msun

        dm_group = ts_group["Dark_matter_particle_data"]
        dx = dm_group["x_dm"][:] # particle coordinates
        dy = dm_group["y_dm"][:]
        dz = dm_group["z_dm"][:]
        dmass = dm_group["m_dm"][:] * m2Msun # particle mass in Msun
        
        # Add cells, gas and stars together
        dx = np.concatenate([cx, sx, dx])
        dy = np.concatenate([cy, sy, dy])
        dz = np.concatenate([cz, sz, dz])
        dmass = np.concatenate([cmass,sm,dmass])

        # These will be used to make sure there is no issue re-matching cells with cell+star+dm after the chain algorithm
        dindex = np.arange(0,len(dmass))
        cindex = np.arange(0,len(cmass))
    
        ######### Efficiently find tracers inside rvir ###########
        cxyz_min = min(cx.min(),cy.min(),cz.min())
        cxyz_max = max(cx.max(),cy.max(),cz.max())
        
        cx_rescale = (cx - cxyz_min) / (cxyz_max-cxyz_min)
        cy_rescale = (cy - cxyz_min) / (cxyz_max-cxyz_min)
        cz_rescale = (cz - cxyz_min) / (cxyz_max-cxyz_min)
    
        txyz_min = min(tx.min(),ty.min(),tz.min())
        txyz_max = max(tx.max(),ty.max(),tz.max())

        tx_rescale = (tx - txyz_min) / (txyz_max-txyz_min)
        ty_rescale = (ty - txyz_min) / (txyz_max-txyz_min)
        tz_rescale = (tz - txyz_min) / (txyz_max-txyz_min)
    
        dxyz_min = min(dx.min(),dy.min(),dz.min())
        dxyz_max = max(dx.max(),dy.max(),dz.max())

        dx_rescale = (dx - dxyz_min) / (dxyz_max-dxyz_min)
        dy_rescale = (dy - dxyz_min) / (dxyz_max-dxyz_min)
        dz_rescale = (dz - dxyz_min) / (dxyz_max-dxyz_min)

        nchain_cells = 20
        nextincell_c,firstincell_c = nu.neighbour_utils.init_chain(cx_rescale,cy_rescale,cz_rescale,nchain_cells)
        nextincell_t,firstincell_t = nu.neighbour_utils.init_chain(tx_rescale,ty_rescale,tz_rescale,nchain_cells)
        nextincell_d,firstincell_d = nu.neighbour_utils.init_chain(dx_rescale,dy_rescale,dz_rescale,nchain_cells)
        # Factor to help convert into box units (note maxime's merger_tree reader has converted halo positions so be careful here!)
        r2kpc_ideal, redshift, little_h, m2Msun,junk,junk = ur.Set_Units(RunDir, timestep, "ideal")
        boxlen_cMpc = dm.jeje_utils.get_param_real(RunDir,timestep,'boxlen') # Box size in box units
        boxlen_cMpc *= r2kpc_ideal
        boxlen_cMpc = boxlen_cMpc * (1+redshift) /1e3 # cMpc (comoving)
        boxlen_cMpc = boxlen_cMpc * little_h # h^-1 cMpc
        factor = 1./boxlen_cMpc

        # Get target halo properties
        for halo_group_str in zoom_group:

            # Only compute stuff for the main halo
            if "_main" in halo_group_str:
                halo_group_str_use= halo_group_str
        
        mp_group = zoom_group[halo_group_str_use+"/main_progenitors"]
        
        # Identify the main progenitor at this timestep
        ok_ts = np.array(mp_group["halo_ts"])==timestep_tree # haloes on this timestep

        sfr = mp_group["sfr_r0p25_10p0"][ok_ts]
        
        xh = mp_group["x"][ok_ts] * factor + 0.5
        yh = mp_group["y"][ok_ts] * factor + 0.5
        zh = mp_group["z"][ok_ts] * factor + 0.5
        
        # Use r200 for central haloes, and rvir (for now) for satellite haloes
        if mp_group["host_halo_id"][ok_ts] == mp_group["halo_id"][ok_ts]:
            rvirh = mp_group["r200"][ok_ts] / r2kpc
            
        else:
            rvirh = mp_group["rvir"][ok_ts] * factor

        vxh = mp_group["vx"][ok_ts] / v2kms # Halo velocity in Code units
        vyh = mp_group["vy"][ok_ts] / v2kms
        vzh = mp_group["vz"][ok_ts] / v2kms
            
        # Select cells in the vicinity of the halo (using a chain algorithm)
        rmax = rvirh

        # Rescale halo positions for efficient cell division in chain algorithm
        xh_rescale = (xh-cxyz_min)/(cxyz_max-cxyz_min)
        yh_rescale = (yh-cxyz_min)/(cxyz_max-cxyz_min)
        zh_rescale = (zh-cxyz_min)/(cxyz_max-cxyz_min)
        rmax_rescale = rmax/(cxyz_max-cxyz_min)
    
        ncell_in_rmax = nu.neighbour_utils.get_npart_in_sphere_with_chain(RunDir,timestep,cx_rescale,cy_rescale,cz_rescale,xh_rescale,yh_rescale,zh_rescale,rmax_rescale,firstincell_c,nextincell_c)
        if ncell_in_rmax > 0:
            select_c = nu.neighbour_utils.\
                       get_part_indicies_in_sphere_with_chain(\
                                                              RunDir,timestep,cx_rescale,cy_rescale,cz_rescale,\
                                                              xh_rescale,yh_rescale,zh_rescale,\
                                                              rmax_rescale,ncell_in_rmax,firstincell_c,nextincell_c)
        else:
            select_c = np.zeros_like(cx) < 0
        
        xh_rescale = (xh-txyz_min)/(txyz_max-txyz_min)
        yh_rescale = (yh-txyz_min)/(txyz_max-txyz_min)
        zh_rescale = (zh-txyz_min)/(txyz_max-txyz_min)
        rmax_rescale = rmax/(txyz_max-txyz_min)
    
        ncell_in_rmax = nu.neighbour_utils.get_npart_in_sphere_with_chain(RunDir,timestep,tx_rescale,ty_rescale,tz_rescale,xh_rescale,yh_rescale,zh_rescale,rmax_rescale,firstincell_t,nextincell_t)
        if ncell_in_rmax > 0:
            select_t = nu.neighbour_utils.get_part_indicies_in_sphere_with_chain(RunDir,timestep,tx_rescale,ty_rescale,tz_rescale,xh_rescale,yh_rescale,zh_rescale,rmax_rescale,ncell_in_rmax,firstincell_t,nextincell_t)
        else:
            select_t = np.zeros_like(tx) < 0

        xh_rescale = (xh-dxyz_min)/(dxyz_max-dxyz_min)
        yh_rescale = (yh-dxyz_min)/(dxyz_max-dxyz_min)
        zh_rescale = (zh-dxyz_min)/(dxyz_max-dxyz_min)
        rmax_rescale = rmax/(dxyz_max-dxyz_min)
    
        ncell_in_rmax = nu.neighbour_utils.get_npart_in_sphere_with_chain(RunDir,timestep,dx_rescale,dy_rescale,dz_rescale,xh_rescale,yh_rescale,zh_rescale,rmax_rescale,firstincell_d,nextincell_d)
        if ncell_in_rmax > 0:
            select_d = nu.neighbour_utils.get_part_indicies_in_sphere_with_chain(RunDir,timestep,dx_rescale,dy_rescale,dz_rescale,xh_rescale,yh_rescale,zh_rescale,rmax_rescale,ncell_in_rmax,firstincell_d,nextincell_d)
        else:
            select_d = np.zeros_like(dx) < 0

        ######## Do tracer preselection ############
        cx_h = cx[select_c]
        cy_h = cy[select_c]
        cz_h = cz[select_c]
        rho_snap_h = rho_snap[select_c]
        pre_snap_h = pre_snap[select_c]
        cvx_h = cvx[select_c]
        cvy_h = cvy[select_c]
        cvz_h = cvz[select_c]
        clev_h = clev[select_c]
        c_comp_h = c_component_index[select_c]
        cindex_h = cindex[select_c]
        cmass_h = cmass[select_c]
        
        if RT:
            xH1_h = xH1[select_c]; xHe1_h = xHe1[select_c] ; xHe2_h = xHe2[select_c]

        # Estimate gravitational potential within each cell
        dx_h = dx[select_d]
        dy_h = dy[select_d]
        dz_h = dz[select_d]
        dmass_h = dmass[select_d]
        dindex_h = dindex[select_d]

        dx_h = dx_h - xh; dy_h = dy_h - yh; dz_h = dz_h -zh
        drad_h = np.sqrt(np.square(dx_h) + np.square(dy_h) + np.square(dz_h))
        order = np.argsort(drad_h)
        dmass_h_sorted = dmass_h[order]

        # Work out mass interior to each radius (slightly approximate because of cell extents)
        dmass_h_cum = np.cumsum(dmass_h_sorted)
        inverse = np.argsort(order)
        dmass_h_interior = dmass_h_cum[inverse]

        ptr = ms.match(cindex_h, dindex_h)
        ok_match = ptr >= 0
        cmass_h_interior = np.zeros_like(cx_h)
        cmass_h_interior[ok_match] = dmass_h_interior[ptr][ok_match] # Msun

    
        # Compute other cell properties   
        cx_h = cx_h - xh; cy_h = cy_h - yh; cz_h = cz_h -zh
        cvx_h = cvx_h - vxh; cvy_h = cvy_h - vyh; cvz_h = cvz_h - vzh
        
        cr_h = np.sqrt(np.square(cx_h) + np.square(cy_h) + np.square(cz_h))
        cv_radial_h = np.sum(np.array([cvx_h,cvy_h,cvz_h]) * np.array([cx_h,cy_h,cz_h]),axis=0) / cr_h

        kpc = 3.0857e19 # m
        kms = 1e3 # ms-1
        Msun = 1.989e30 # kg
        G = 6.67408e-11 # m^3 kg^-1 s^-2
        e_grav_h = G * (cmass_h_interior*Msun) / (cr_h*r2kpc*kpc) / kms**2 # km^2 s^-2

        cv_radial_h *= v2kms # kms

        ek_h = 0.5 * np.square(cvx_h* v2kms) + np.square(cvy_h* v2kms) + np.square(cvz_h* v2kms) # km2s-2
    
        cut.jeje_utils.read_conversion_scales(RunDir,timestep)
        scale_temk = (cut.jeje_utils.dp_scale_l / cut.jeje_utils.dp_scale_t)**2 * cut.jeje_utils.mh / cut.jeje_utils.kb
        temp_mu_h = pre_snap_h/rho_snap_h * scale_temk

        # Compute neutral gas number density in atoms per cc and the temperature in Kelvin
        if RT:
            xhi_h = (1-xH1_h)# Hydrogen neutral fraction
            X_frac=0.76
            mu_h = 1./( X_frac*(1.+xH1_h) + 0.25*(1.-X_frac)*(1.+xHe1_h+2.*xHe2_h) )
        else:
            mu_h, xhi_h = cut.cell_utils.ramses_get_mu_xhi(RunDir,timestep,rho_snap_h, pre_snap_h, True)
            
        temp_h = temp_mu_h * mu_h

        kb = 1.38062e-23 # m^2 kg s^-2 K^-1
        mh = 1.66e-27 # kg
        eth_h = 1.5*kb/mh*temp_mu_h # m^2 s^-2
        eth_h *= 1e-6 # km2s-2
    
        nh_h = rho_snap_h * cut.jeje_utils.dp_scale_d / cut.jeje_utils.mh * cut.jeje_utils.xh
        
        csize_h = boxlen_pkpc / 2.**clev_h / r2kpc

        # Compute intrinsic Lyman alpha luminosity
        if RT:
            Ta = np.copy(temp_h)
            Ta[Ta<100] = 100 # Put temperature floor
            prob_case_B = 0.686 - 0.106*np.log10(Ta/1e4) - 0.009*(Ta/1e4)**(-0.44)
            nHII = xH1_h*nh_h # Ionized hydrogen density
            nHI = xhi_h * nh_h # Neutral hydrogen density
            X_frac = 0.76
            nHe  = 0.25*nh_h*(1.0-X_frac)/X_frac
            n_e = nHII+nHe*(xHe1_h+2.*xHe2_h)

            def get_AlphaB_HII(TK):
                lbda = 315614./TK
                return 2.753e-14 * lbda**1.5 / ( (1.+(lbda/2.74)**0.407)**2.242 )
            
            def get_collExrate_HI(TK):
                kb = 1.38064852e-16 
                return 2.41e-6/np.sqrt(TK) * (TK/1.e4)**0.22 * np.exp(-1.63e-11/(kb*TK))

            planck  = 6.626070040e-27  # [erg s] Planck's constant
            clight  = 2.99792458e10  # cm/s
            cmtoA   = 1.0e8 # cm to Angstrom
            lambda_0=1215.67e0
            lambda_0_cm = lambda_0 / cmtoA          # cm
            nu_0 = clight / lambda_0_cm             #  Hz
            e_lya = planck*nu_0 # 1.634e-11 # cgs

            RecLyaEmi  = prob_case_B * nHII * n_e * get_AlphaB_HII(temp_h) * e_lya
            CollLyaEmi = nHI * n_e * get_collExrate_HI(temp_h) * e_lya
            LyaEmi     = CollLyaEmi + RecLyaEmi
            
            csize_h = boxlen_pkpc / 2.**clev_h # pkpc
            cell_vol = (csize_h * 3.086e18*1e3) **3 # cm3
        
            LyaLum = LyaEmi * cell_vol # erg s^-1 (I think)
            CollLyaLum = CollLyaEmi * cell_vol
            RecLyaLum = RecLyaEmi * cell_vol

            # Need to account for the difference in mass between cells and tracers
            CollLyaLum *= 1./cmass_h # erg s^-1 Msun^-1
            RecLyaLum *= 1./cmass_h
            
        tx_h = tx[select_t]
        ty_h = ty[select_t]
        tz_h = tz[select_t]
        tid_h = id_tr[select_t]
        tfam_h = fam_tr[select_t]
        tmass_h = tmass[select_t]
    
        # Put the halo at the centre of the particle coordinate system
        tx_h = tx_h - xh; ty_h = ty_h - yh; tz_h = tz_h -zh
        # Compute the radius to each tracer
        tr_h = np.sqrt(np.square(tx_h) + np.square(ty_h) + np.square(tz_h)) / rvirh
    
        if i_timestep == 0:
            tr_track = np.zeros((len(timesteps_show), n_select)) + np.nan
            tr_track_nonorm = np.zeros((len(timesteps_show), n_select)) + np.nan
            fam_track = np.zeros((len(timesteps_show), n_select)) + np.nan

            tx_track = np.zeros((len(timesteps_show), n_select)) + np.nan
            ty_track = np.zeros((len(timesteps_show), n_select)) + np.nan
            tz_track = np.zeros((len(timesteps_show), n_select)) + np.nan
            tmass_track = np.zeros((len(timesteps_show), n_select)) + np.nan
        
            temp_track = np.zeros((len(timesteps_show), n_select)) + np.nan
            nh_track = np.zeros((len(timesteps_show), n_select)) + np.nan
            cv_track = np.zeros((len(timesteps_show), n_select)) + np.nan
            ek_track = np.zeros((len(timesteps_show), n_select)) + np.nan
            eth_track = np.zeros((len(timesteps_show), n_select)) + np.nan
            egrav_track = np.zeros((len(timesteps_show), n_select)) + np.nan
            xhi_track = np.zeros((len(timesteps_show), n_select)) + np.nan
            comp_track = np.zeros((len(timesteps_show), n_select)) + np.nan
            if RT:
                
                CollLyaLum_track = np.zeros((len(timesteps_show), n_select)) + np.nan
                RecLyaLum_track= np.zeros((len(timesteps_show), n_select)) + np.nan
                
            redshift_track = np.zeros(len(timesteps_show))
        
        ptr = ms.match(tid_select, tid_h)
        ok = np.where(ptr >= 0)[0]

        tr_track[i_timestep][ok] = tr_h[ptr][ok]
        tr_track_nonorm[i_timestep][ok] = tr_h[ptr][ok] * rvirh * r2kpc # pkpc
        fam_track[i_timestep][ok] = tfam_h[ptr][ok]
        tmass_track[i_timestep][ok] = tmass_h[ptr][ok]
        
        tx_track[i_timestep][ok] = tx_h[ptr][ok] / rvirh
        ty_track[i_timestep][ok] = ty_h[ptr][ok] / rvirh
        tz_track[i_timestep][ok] = tz_h[ptr][ok] / rvirh


        # Match cells to tracers
        ind_c, ok_match = flux_utils.Match_Cell_Particle(csize_h, cx_h, cy_h, cz_h, tx_h[ptr][ok], ty_h[ptr][ok], tz_h[ptr][ok])
        ok_match = np.where(ok_match)[0]
    
        def bool_index(array, ind1, array2):
            temp = array
            temp[ind1] = array2
            return temp

        comp_track[i_timestep][ok] = bool_index(comp_track[i_timestep][ok], ok_match, c_comp_h[ind_c][ok_match])
        xhi_track[i_timestep][ok] = bool_index(xhi_track[i_timestep][ok], ok_match, xhi_h[ind_c][ok_match])
        cv_track[i_timestep][ok] = bool_index(cv_track[i_timestep][ok], ok_match, cv_radial_h[ind_c][ok_match])
        ek_track[i_timestep][ok] = bool_index(ek_track[i_timestep][ok], ok_match, ek_h[ind_c][ok_match])
        eth_track[i_timestep][ok] = bool_index(eth_track[i_timestep][ok], ok_match, eth_h[ind_c][ok_match])
        egrav_track[i_timestep][ok] = bool_index(egrav_track[i_timestep][ok], ok_match, e_grav_h[ind_c][ok_match])
        temp_track[i_timestep][ok] = bool_index(temp_track[i_timestep][ok], ok_match, temp_h[ind_c][ok_match])
        nh_track[i_timestep][ok] = bool_index(nh_track[i_timestep][ok], ok_match, nh_h[ind_c][ok_match])
        if RT:
            RecLyaLum_track[i_timestep][ok] = bool_index(RecLyaLum_track[i_timestep][ok], ok_match, RecLyaLum[ind_c][ok_match]) * tmass_track[i_timestep][ok] # erg s^-1
            CollLyaLum_track[i_timestep][ok] = bool_index(CollLyaLum_track[i_timestep][ok], ok_match, CollLyaLum[ind_c][ok_match]) * tmass_track[i_timestep][ok]
    
        redshift_track[i_timestep] = redshift
    
    subvol_File.close()
    File.close()
    
    File = h5py.File(InDir+filename)

    if "comp" in File:
        del File["comp"]
        del File["xhi"]
        del File["cv"]
        del File["nh"]
        del File["temp"]
        del File["r"]
        del File["x"]
        del File["y"]
        del File["z"]
        del File["r_physical"]
        del File["family"]
        del File["timestep"]
        del File["redshift"]
        try:
            del File["RecLyaLum"]
            del File["CollLyaLum"]
        except:
            None
        try:
            del File["ek"]
            del File["eth"]
            del File["egrav"]
        except:
            None
        try:
            del File["mass"]
        except:
            None
        
    File.create_dataset("comp",data=comp_track)
    File.create_dataset("xhi",data=xhi_track)
    File.create_dataset("cv",data=cv_track)
    File.create_dataset("ek",data=ek_track)
    File.create_dataset("eth",data=eth_track)
    File.create_dataset("egrav",data=egrav_track)
    File.create_dataset("nh",data=nh_track)
    File.create_dataset("temp",data=temp_track)
    File.create_dataset("r",data=tr_track)
    File.create_dataset("x",data=tx_track)
    File.create_dataset("y",data=ty_track)
    File.create_dataset("z",data=tz_track)
    File.create_dataset("family",data=fam_track)
    File.create_dataset("mass",data=tmass_track)
    File.create_dataset("timestep",data=timesteps_show) # These are now in units of simulation timesteps (not tree)
    File.create_dataset("redshift",data=redshift_track)
    File.create_dataset("r_physical",data=tr_track_nonorm)
    if RT:
        File.create_dataset("RecLyaLum",data=RecLyaLum_track)
        File.create_dataset("CollLyaLum",data=CollLyaLum_track)
    File.close()

