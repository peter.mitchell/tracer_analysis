import numpy as np

# Select high z particles that enter the virial radius
#init_select = "virial_inflow"
#timesteps_select = [9, 10]

# Select particles inside the halo at z=4 ish
#init_select = "halo"
#timesteps_select = [100, 101]

# Select particles inside the galaxy at z=4 ish
#init_select = "galaxy"
#timesteps_select = [100, 101]

# Select randomly from any tracer that has been inside the halo at any point in it's history
#timesteps_select = np.arange(2,140)
#init_select = "none"

'''Zoom = "Zoom-7-1290"
Run = "tracer_run_lr"
timestep_final = 155
timesteps_select = np.arange(2,timestep_final+1)
init_select = "none"
RunDir = "/cral4/mitchell/data/Tracer_Test/"+ Zoom + "/" + Run
OutDir = "/cral4/mitchell/data/Tracer_Test/PostProcess/"
OutDir2 = "/cral2/mitchell/PostProcess/"'''

Zoom = "Zoom-7-5092"
Run = "RhdRun-tracer_radfix"
timestep_final = 155
timesteps_select = np.arange(2,timestep_final+1)
init_select = "none"
RunDir = "/cral2/mitchell/"+Zoom+"/"+Run
OutDir = "/cral2/mitchell/PostProcess/"
OutDir2 = "/cral2/mitchell/PostProcess/"


HaloDir = RunDir + "/Halos/"
TreeDir = RunDir + "/Trees/"

# Only write a random subset of the selected tracers
downsample = True
n_downsample = 1000
#n_downsample = 3e6

if not downsample:
    # Otherwise, we need to split the tracer population into different subsamples
    # This is because loading 150 outputs worth of 30 million tracers is too much memory for the login nodes to handle
    batch_no = 10 # Split into 10 separate groups of tracers - this means about 3 million tracers per group 
    
import sys
import dm_utils as dm
from scipy import spatial
import h5py
import utilities_ramses as ur
import neighbour_utils as nu
import match_searchsorted as ms

############## IO #########################
# Read in relevant tree information from the hdf5 file
filename = Zoom + "_" + Run + "_tsf" + str(timestep_final) +".hdf5"

File = h5py.File(OutDir+filename,"r")

# Open the subvolume file (containing all cell data)
subvol_filename = "subvol_"+filename
subvol_File = h5py.File(OutDir+subvol_filename,"r")
# Check the file with the tree information already exists
if not Zoom in File:
    print "Error: Tree information for ", Zoom, " in ", filename, " cannot be read"
    exit()

zoom_group = File[Zoom]

# Loop over timesteps
for i_timestep, timestep in enumerate(timesteps_select):
    
    timestep_tree = timestep -1 # Stupid mismatch between merger tree files and everything else
    
    # Get conversions for different units
    r2kpc, redshift, little_h, m2Msun,rho2msunpkpc3,v2kms = ur.Set_Units(RunDir, timestep)

    print "reading data at timestep = ", timestep, "of", timestep_final, ", redshift is", redshift

    ########## Read tracer data ###################
    # Hdf5 group for this timestep (contains cell data)
    ts_group = subvol_File["timestep_"+str(timestep)]
    
    tracer_group = ts_group["Tracer_particle_data"]

    print "Peforming tracer IO"
    tx = tracer_group["x_tr"][:]
    ty = tracer_group["y_tr"][:]
    tz = tracer_group["z_tr"][:]
    fam_tr = tracer_group["fam_tr"][:]
    id_tr = tracer_group["id_tr"][:]

    ######### Efficiently find tracers inside rvir ###########

    print "Computing which tracers are inside r200"
    txyz_min = min(tx.min(),ty.min(),tz.min())
    txyz_max = max(tx.max(),ty.max(),tz.max())

    tx_rescale = (tx - txyz_min) / (txyz_max-txyz_min)
    ty_rescale = (ty - txyz_min) / (txyz_max-txyz_min)
    tz_rescale = (tz - txyz_min) / (txyz_max-txyz_min)

    nchain_cells = 20
    nextincell_t,firstincell_t = nu.neighbour_utils.init_chain(tx_rescale,ty_rescale,tz_rescale,nchain_cells)
    # Factor to help convert into box units (note maxime's merger_tree reader has converted halo positions so be careful here!)
    r2kpc_ideal, redshift, little_h, m2Msun,junk,junk = ur.Set_Units(RunDir, timestep, "ideal")
    boxlen_cMpc = dm.jeje_utils.get_param_real(RunDir,timestep,'boxlen') # Box size in box units
    boxlen_cMpc *= r2kpc_ideal
    boxlen_cMpc = boxlen_cMpc * (1+redshift) /1e3 # cMpc (comoving)
    boxlen_cMpc = boxlen_cMpc * little_h # h^-1 cMpc
    factor = 1./boxlen_cMpc

    # Get target halo properties
    for halo_group_str in zoom_group:

        # Only compute fluxes for the main halo
        if not "_main" in halo_group_str:
            continue
        
        mp_group = zoom_group[halo_group_str+"/main_progenitors"]
        
        # Identify the main progenitor at this timestep
        ok_ts = np.array(mp_group["halo_ts"])==timestep_tree # haloes on this timestep
        
        xh = mp_group["x"][ok_ts] * factor + 0.5
        yh = mp_group["y"][ok_ts] * factor + 0.5
        zh = mp_group["z"][ok_ts] * factor + 0.5
        
        # Use r200 for central haloes, and rvir (for now) for satellite haloes
        if mp_group["host_halo_id"][ok_ts] == mp_group["halo_id"][ok_ts]:
            rvirh = mp_group["r200"][ok_ts] / r2kpc
            
        else:
            rvirh = mp_group["rvir"][ok_ts] * factor

        if "_main" in halo_group_str:
            break
            
    # Select cells in the vicinity of the halo (using a chain algorithm)
    rmax = rvirh
    
    # Rescale halo positions for efficient cell division in chain algorithm
    xh_rescale = (xh-txyz_min)/(txyz_max-txyz_min)
    yh_rescale = (yh-txyz_min)/(txyz_max-txyz_min)
    zh_rescale = (zh-txyz_min)/(txyz_max-txyz_min)
    rmax_rescale = rmax/(txyz_max-txyz_min)
    
    ncell_in_rmax = nu.neighbour_utils.get_npart_in_sphere_with_chain(RunDir,timestep,tx_rescale,ty_rescale,tz_rescale,xh_rescale,yh_rescale,zh_rescale,rmax_rescale,firstincell_t,nextincell_t)
    if ncell_in_rmax > 0:
        select_t = nu.neighbour_utils.get_part_indicies_in_sphere_with_chain(RunDir,timestep,tx_rescale,ty_rescale,tz_rescale,xh_rescale,yh_rescale,zh_rescale,rmax_rescale,ncell_in_rmax,firstincell_t,nextincell_t)
    else:
        select_t = np.zeros_like(tx) < 0

    ######## Do tracer preselection ############
    print "Selecting tracers inside r200, and appending to selection"
    tx_h = tx[select_t]
    ty_h = ty[select_t]
    tz_h = tz[select_t]
    tid_h = id_tr[select_t]
    tfam_h = fam_tr[select_t]
    
    
    # Find tracers that enter the virial sphere between first two timesteps
    if i_timestep == 0:
        tid_fts = tid_h
        continue
    else:
        if init_select == "virial_inflow":
            ptr = ms.match(tid_h, tid_fts)
            ok = ptr < 0
            tid_select = tid_h[ok]
        elif init_select == "galaxy":
            # Put the halo at the centre of the particle coordinate system
            tx_h = tx_h - xh; ty_h = ty_h - yh; tz_h = tz_h -zh
            # Compute the radius to each tracer
            tr_h = np.sqrt(np.square(tx_h) + np.square(ty_h) + np.square(tz_h)) / rvirh

            ok = tr_h < 0.2
            tid_select = tid_h[ok]
        elif init_select == "halo":
            tid_select = np.copy(tid_h)
        elif init_select == "none":
            try:
                tid_select = np.unique(np.append(tid_select, tid_h))
            except:
                tid_select = np.copy(tid_h)
        else:
            print "nope"

    if i_timestep == len(timesteps_select)-1:

        n_select = len(tid_select)

        if downsample:
            if n_select < n_downsample:
                print "error ds"
                quit()
            ind_select = np.random.randint(0,n_select,n_downsample)
            tid_select = tid_select[ind_select]
            downsample_factor = n_select / float(len(tid_select))
            n_select = len(tid_select)
            
        print "number of tracers selected is", n_select
        if downsample:
            print "down-sampling factor was", downsample_factor
        if n_select == 0:
            quit()

File.close()
subvol_File.close()

if not downsample:
    tid_select_list = []
    ind_split = np.rint(np.arange(batch_no+1)*len(tid_select)/float(batch_no))
    ind_split[-1] = len(tid_select)
    
    for n in range(batch_no):
        tid_select_list.append(tid_select[ind_split[n]:ind_split[n+1]])

# Output data to hdf5
#filename = OutDir2 + "saved_tracers_select_"+Zoom+"_"+Run+".hdf5" # would like to change to this line later
filename = OutDir2 + "saved_tracers_select_"+Zoom+"_"+Run
import os

if downsample:
    filename+= "_ds_"+ str(n_downsample) + ".hdf5"

    if os.path.isfile(filename):
        os.remove(filename)
        print "deleted previous hdf5 file at ", filename

    output_file = h5py.File(filename)
    output_file.create_dataset("tid",data=tid_select)
    output_file.create_dataset("ds_factor",data=np.array(downsample_factor))
    output_file.close()

else:
    for n in range(batch_no):
        filename_n = filename + "_subset_" + str(n) + ".hdf5"

        if os.path.isfile(filename_n):
            os.remove(filename_n)
            print "deleted previous hdf5 file at ", filename_n

        output_file = h5py.File(filename_n)
        output_file.create_dataset("tid",data=tid_select_list[n])
        output_file.close()
