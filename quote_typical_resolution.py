import sys
sys.path.append("./Pipeline/")
import numpy as np
import dm_utils as dm
import cutils as cut
import h5py
import utilities_ramses as ur
from halotils import py_halo_utils as hutils
import utilities_profiles as up
import neighbour_utils as nu
import os
import utilities_statistics as us

z_choose = 3.0

Zoom = "Zoom-7-5092"
Run = "RhdRun-tracer_radfix"
timestep_final = 155
RunDir = "/cral2/mitchell/"+Zoom+"/"+Run
InDir = "/cral2/mitchell/PostProcess/"
OutDir = "/cral2/mitchell/PostProcess/"
HaloDir = RunDir + "/Halos/"

filename = Zoom + "_" + Run + "_tsf" + str(timestep_final) +".hdf5"
File = h5py.File(OutDir+filename,"r")

# Open the subvolume file (containing all cell data)
subvol_filename = "subvol_"+filename
subvol_File = h5py.File(OutDir+subvol_filename,"r")


zoom_group = File[Zoom]

for thing in zoom_group:
    if "main" in thing:
        mp_group = zoom_group[thing+"/main_progenitors"]

timesteps_tree = mp_group["halo_ts"][:]
timesteps = timesteps_tree +1

redshifts = np.zeros_like(timesteps)

for i, timestep in enumerate(timesteps):
    r2kpc, redshift, little_h, m2Msun,rho2msunpkpc3,v2kms = ur.Set_Units(RunDir, timestep)
    redshifts[i] = redshift

i_choose = np.argmin(abs(redshifts-z_choose))        
timestep = timesteps[i_choose]
redshift = redshifts[i_choose]
timestep_tree = timestep -1

########## Read info for main halo #####################
ok_ts = np.array(mp_group["halo_ts"])==timestep_tree # haloes on this timestep

# Get conversions for different units
r2kpc, redshift, little_h, m2Msun,rho2msunpkpc3, v2kms = ur.Set_Units(RunDir, timestep)
boxlen_pkpc = dm.jeje_utils.get_param_real(RunDir,timestep,'boxlen') * r2kpc # Box size in proper kpc, using the correct box size

r2kpc_ideal, redshift, little_h, m2Msun,junk,junk = ur.Set_Units(RunDir, timestep, "ideal")
boxlen_cMpc = dm.jeje_utils.get_param_real(RunDir,timestep,'boxlen') # Box size in box units
boxlen_cMpc *= r2kpc_ideal
boxlen_cMpc = boxlen_cMpc * (1+redshift) /1e3 # cMpc (comoving)
boxlen_cMpc = boxlen_cMpc * little_h # h^-1 cMpc
factor = 1./boxlen_cMpc

xh = mp_group["x"][ok_ts] * factor + 0.5
yh = mp_group["y"][ok_ts] * factor + 0.5
zh = mp_group["z"][ok_ts] * factor + 0.5
rvirh = mp_group["r200"][ok_ts] / r2kpc

halo_num = mp_group["halo_num"][ok_ts]


########## Read cell/particle data #########################        
print "Reading particle/cell data"
# Hdf5 group for this timestep (contains cell data)
ts_group = subvol_File["timestep_"+str(int(timestep))]

cell_group = ts_group["Gas_leaf_cell_data"]

rho_snap = cell_group["rho_c"][:] # Gas density
cx = cell_group["x_c"][:] # Cell coordinates
cy = cell_group["y_c"][:]
cz = cell_group["z_c"][:]
lev = cell_group["lev"][:] # Cell refinement level
comp = cell_group["component_index"][:]

print "Done: performing unit conversions and setting up chains"

cut.jeje_utils.read_conversion_scales(RunDir,timestep)
ndensity = rho_snap * cut.jeje_utils.dp_scale_d / cut.jeje_utils.mh * cut.jeje_utils.xh # Number density: Atoms cm^-3

# Compute cell size using amr refinement level and box size
csize = boxlen_pkpc / np.power(2.,lev) # proper kpc
cmass = rho_snap * rho2msunpkpc3 * csize**3 # Msun - gas mass in each cell

# Build linked-list of cells
cxyz_min = min(cx.min(),cy.min(),cz.min()) 
cxyz_max = max(cx.max(),cy.max(),cz.max())

cx_rescale = (cx - cxyz_min) / (cxyz_max-cxyz_min)
cy_rescale = (cy - cxyz_min) / (cxyz_max-cxyz_min)
cz_rescale = (cz - cxyz_min) / (cxyz_max-cxyz_min)

nleafcells = len(cx)
nchain_cells = 20
nextincell,firstincell = nu.neighbour_utils.init_chain(cx_rescale,cy_rescale,cz_rescale,nchain_cells)
        
print "Done, find cells within rvir"

xh_rescale = (xh-cxyz_min)/(cxyz_max-cxyz_min)
yh_rescale = (yh-cxyz_min)/(cxyz_max-cxyz_min)
zh_rescale = (zh-cxyz_min)/(cxyz_max-cxyz_min)

rmax_rescale = rvirh * 1 /(cxyz_max-cxyz_min)

# Find cells within rmax of this halo (using chain algorithm)                              
ncell_in_rmax = nu.neighbour_utils.get_npart_in_sphere_with_chain(RunDir,timestep,cx_rescale,cy_rescale,cz_rescale,xh_rescale,yh_rescale,zh_rescale,rmax_rescale,firstincell,nextincell)
if ncell_in_rmax > 0:
    ok = nu.neighbour_utils.get_part_indicies_in_sphere_with_chain(RunDir,timestep,cx_rescale,cy_rescale,cz_rescale,xh_rescale,yh_rescale,zh_rescale,rmax_rescale,ncell_in_rmax,firstincell,nextincell)
else:
    ok = np.zeros_like(cx) < 0

csize = csize[ok]
cmass = cmass[ok]
cx = cx[ok]
cy = cy[ok]
cz = cz[ok]
comp = comp[ok]

csize *= 1e3 # pc

cr = np.sqrt(np.square(cx-xh)+np.square(cy-yh) + np.square(cz-zh))
cvol = np.power(csize,3)

cgm = ((comp == 0) | (comp==3)) & (cr > rvirh*0.2)
ism = (cr < rvirh*0.2)

print "Total within halo: ncells, median (mass_weighted), median (volume_weighted)" 
csize_mw = us.Weighted_Percentile(csize,cmass,0.5)
csize_vw = us.Weighted_Percentile(csize,cvol,0.5)
print len(cx), csize_mw, csize_vw

print "Diffuse CGM: ncells, median (mass_weighted), median (volume_weighted)"
csize_cgm_mw = us.Weighted_Percentile(csize[cgm],cmass[cgm],0.5)
csize_cgm_vw = us.Weighted_Percentile(csize[cgm],cvol[cgm],0.5)
print len(cx[cgm]), csize_cgm_mw, csize_cgm_vw

print "ISM: ncells, median (mass_weighted), median (volume_weighted)"
csize_ism_mw = us.Weighted_Percentile(csize[ism],cmass[ism],0.5)
csize_ism_vw = us.Weighted_Percentile(csize[ism],cvol[ism],0.5)
print len(cx[ism]), csize_ism_mw, csize_ism_vw
