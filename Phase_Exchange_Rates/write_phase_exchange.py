# Set paths/options

#Zoom = "Zoom-7-1290"
#timestep_final = 155
#Run = "tracer_run_lr"
#RunDir_path = "/cral4/mitchell/data/Tracer_Test/"
#OutDir = "/cral4/mitchell/data/Tracer_Test/PostProcess/"
#RHD = False

'''Zoom = "Zoom-7-5092"
timestep_final = 105
Run = "RhdRun-tracer_norad_bug"
RunDir_path = "/cral2/mitchell/"
OutDir = "/cral4/mitchell/data/Tracer_Test/PostProcess/"'''
#RHD = True

Zoom = "Zoom-7-5092"
timestep_final = 130
Run = "RhdRun-tracer_radfix"
RunDir_path = "/cral2/mitchell/"
OutDir = "/cral2/mitchell/PostProcess/"
RHD = True

timesteps_out = "all"
verbose = True

####### Choose fraction of halo dynamical time to use for previous output ########
#tdyn_frac = 0.25
#n_save = 6 # 6 outputs is enough to reach 1/4 of the halo dynamical time by z=3
tdyn_frac = 0.5
n_save = 12

RunDir = RunDir_path + Zoom + "/" + Run
HaloDir = RunDir + "/Halos/"
TreeDir = RunDir + "/Trees/"

if verbose:
    print "Computing fluxes for", Zoom, Run

timestep_tree_final = timestep_final -1 # At the moment, the tree files store "halo_ts" as timestep -1 (don't ask)

# If true, cells are only considered part of a subhalo (and hence flagged in the maps and used to compute radial profiles with/without subhaloes) 
# if the subhalo contains more than a threshold number (fiducial value 1000) of dm particles (as computed by the halofinder)
exclude_below_threshold = False

if verbose:
    print 'imports ... '
import dm_utils as dm
import cutils as cut
import numpy as np
import h5py
import flux_utils
import utilities_ramses as ur
import neighbour_utils as nu
import match_searchsorted as ms
import utilities_cosmology as uc

# Choose radial bins for computing exchange terms
delta_rprime = 0.1
r_bins = np.arange(0.0, 1.1, delta_rprime)

########## Decide tracer exchanges to record (n -> n+1) ##############
# Tracers at timestep n
# First do outflowing components
weight_n = ["neutral", "neutral", "ion",     "ion",   "ion",      "ion"]
nh_lo_n =  [0.0,        0.0,      0.0,       0.0,      0.0,       0.0]
nh_hi_n =  [1e20,       1e20,     1e20,      1e20,     1e20,      1e20 ]
T_lo_n =   [0.0,        0.0,      0.0,       0.0,      10.**4.5,  10.**4.5]
T_hi_n =   [1e20,       1e20,     10.**4.5,  10.**4.5, 1e20,      1e20]
xhi_lo_n = [0.0,        0.0,      0.0,       0.0,      0.0,       0.0]
xhi_hi_n = [1.0,        1.0,      1.0,       1.0,      1.0,       1.0]
cv_lo_n =  [0.0,       -1e20,     0.0,      -1e20,     0.0,      -1e20]
cv_hi_n =  [1e20,       0.0,      1e20,      0.0,      1e20,      0.0]
rvir_lo_n =[0.0,        0.0,      0.0,       0.0,      0.0,       0.0]
rvir_hi_n =[1.0,        1.0,      1.0,       1.0,      1.0,       1.0]
comp_n =   [0,          0,        0,         0,        0,         0]
name_n =   ["neut_out","neut_in","wion_out","wion_in","hion_out","hion_in"]

# Tracer partitions
weight_n1 = ["neutral", "all",        "ion",     "ion",     "neutral", "all",       "ion",     "ion"]
nh_lo_n1 =  [0.0,        0.0,          0.0,       0.0,       0.0,       0.0,         0.0,       0.0]
nh_hi_n1 =  [1e20,       1e20,         1e20,      1e20,      1e20,      1e20,        1e20,      1e20]
T_lo_n1 =   [0.0,        0.0,          0.0,       10.**4.5,  0.0,       0.0,         0.0,       10.**4.5]
T_hi_n1 =   [1e20,       1e20,         10.**4.5,  1e20,      1e20,      1e20,        10.**4.5,  1e20]
xhi_lo_n1 = [0.0,        0.0,          0.0,       0.0,       0.0,       0.0,         0.0,       0.0]
xhi_hi_n1 = [1.0,        1.0,          1.0,       1.0,       1.0,       1.0,         1.0,       1.0]
cv_lo_n1 =  [0.0,        0.0,          0.0,       0.0,      -1e20,     -1e20,       -1e20,     -1e20]
cv_hi_n1 =  [1e20,       1e20,         1e20,      1e20,      0.0,       0.0,         0.0,       0.0]
rvir_lo_n1 =[0.0,        0.0,          0.0,       0.0,       0.0,       0.0,         0.0,       0.0]
rvir_hi_n1 =[1.0,        1.0,          1.0,       1.0,       1.0,       1.0,         1.0,       1.0]
comp_n1 =   [0,          0,            0,         0,         0,         0,           0,         0]
name_n1 =   ["neut_out","tot_outflow","wion_out","hion_out","neut_in", "tot_inflow","wion_in", "hion_in"]

rmin_tr =    np.min((np.array(rvir_lo_n).min(),np.array(rvir_lo_n1).min()))
rmax_tr =    np.max((np.array(rvir_hi_n).max(),np.array(rvir_hi_n1).max()))
nh_min_tr =  np.min((np.array(nh_lo_n).min(),  np.array(nh_lo_n1).min()))
nh_max_tr =  np.max((np.array(nh_hi_n).max(),  np.array(nh_hi_n1).max()))
T_min_tr =   np.min((np.array(T_lo_n).min(),   np.array(T_lo_n1).min()))
T_max_tr =   np.max((np.array(T_hi_n).max(),   np.array(T_hi_n1).max()))
xhi_min_tr = np.min((np.array(xhi_lo_n).min(), np.array(xhi_lo_n1).min()))
xhi_max_tr = np.max((np.array(xhi_hi_n).max(), np.array(xhi_hi_n1).max()))
cv_min_tr =  np.min((np.array(cv_lo_n).min(),  np.array(cv_lo_n1).min()))
cv_max_tr =  np.max((np.array(cv_hi_n).max(),  np.array(cv_hi_n1).max()))
comp_min_tr =np.min((np.array(comp_n).min(),np.array(comp_n1).min()))
comp_max_tr =np.max((np.array(comp_n).max(),np.array(comp_n1).max()))


############## IO #########################
# Read in relevant tree information from the hdf5 file
if timesteps_out == "all":
    filename = Zoom + "_" + Run + "_tsf" + str(timestep_final) +".hdf5"
elif timesteps_out == "final":
    filename = Zoom + "_" + Run + "_tsf" + str(timestep_final) +"_ts"+str(timestep_final)+".hdf5"
else:
    try:
        timestep_out = int(timesteps_out)
        filename = Zoom + "_" + Run + "_tsf" + str(timestep_final) +"_ts"+str(timestep_out)+".hdf5"
    except:
        print "timesteps_out",timesteps_out,",is not valid, sorry!"
        quit()

File = h5py.File(OutDir+filename)

# Open the subvolume file (containing all cell data)
subvol_filename = "subvol_"+filename
subvol_File = h5py.File(OutDir+subvol_filename)

# Check the file with the tree information already exists
if not Zoom in File:
    print "Error: Tree information for ", Zoom, " in ", filename, " cannot be read"
    exit()

zoom_group = File[Zoom]

# Select desired timesteps
if timesteps_out == "all":
    timesteps_tree_out = np.arange(timestep_tree_final)+1
elif timesteps_out == "final":
    timesteps_tree_out = np.array([timestep_tree_final])
else:
    try:
        timesteps_tree_out = np.array([int(timesteps_out)])-1
    except:
        print "timesteps_out",timesteps_out,",is not valid, sorry!"
        quit()

first_output = True
first_output_save = True
exchange_terms_list = []
timestep_save_list = []
# Loop over timesteps
for i_timestep_tree, timestep_tree in enumerate(timesteps_tree_out):
    timestep = timestep_tree + 1 # Stupid mismatch between merger tree files and everything else

    #if timestep <102:
    #    continue
    
    # Get conversions for different units
    r2kpc, redshift, little_h, m2Msun,rho2msunpkpc3,v2kms = ur.Set_Units(RunDir, timestep)
    boxlen_pkpc = dm.jeje_utils.get_param_real(RunDir,timestep,'boxlen') * r2kpc # Box size in proper kpc, using the correct box size

    try:
        t_cosmic_prev = t_cosmic
    except:
        if verbose:
            print "Not setting delta t on first timestep"

    a = 1./(1.+redshift)
    omega_0,lambda_0,little_h = dm.jeje_utils.read_cosmo_params(RunDir,timestep_final)
    t_cosmic, junk = uc.t_Universe(a,omega_0,little_h)

    ########## Read tracer data ###################
    # Hdf5 group for this timestep (contains cell data)
    ts_group = subvol_File["timestep_"+str(timestep)]

    if verbose:
        print "reading tracer data at timestep = ", timestep, "of", timestep_final

    
    tracer_group = ts_group["Tracer_particle_data"]

    tx = tracer_group["x_tr"][:]
    ty = tracer_group["y_tr"][:]
    tz = tracer_group["z_tr"][:]
    fam_tr = tracer_group["fam_tr"][:]
    id_tr = tracer_group["id_tr"][:]
    tmass = tracer_group["mass_tr"][:]

    tmass *= m2Msun

    tx =    tx[fam_tr==100]
    ty =    ty[fam_tr==100]
    tz =    tz[fam_tr==100]
    id_tr = id_tr[fam_tr==100]
    tmass = tmass[fam_tr==100]
    
    ########## Read cell data #########################        

    if verbose:
        print "reading cell data at timestep = ", timestep, "of", timestep_final
    
    cell_group = ts_group["Gas_leaf_cell_data"]

    rho_snap = cell_group["rho_c"][:] # Gas density
    pre_snap = cell_group["pre_c"][:] # Gas pressure
    cx = cell_group["x_c"][:] # Cell coordinates
    cy = cell_group["y_c"][:]
    cz = cell_group["z_c"][:]
    cvx = cell_group["vx_c"][:]
    cvy = cell_group["vy_c"][:]
    cvz = cell_group["vz_c"][:]
    clev = cell_group["lev"][:] # Cell refinement level
    c_component_index = cell_group["component_index"][:] # Flag to indicate whether inside/outside a subhalo

    if RHD:
        xH1 = cell_group["xH1"][:]
        xHe1 =cell_group["xHe1"][:]
        xHe2 = cell_group["xHe2"][:]

    
    if verbose:
        print "done, now building chains to efficiently select tracers and cells within the virial radius"

    # Build linked-list of cells/particles (just to be used for selecting things inside rvir)
    cxyz_min = min(cx.min(),cy.min(),cz.min())
    cxyz_max = max(cx.max(),cy.max(),cz.max())

    cx_rescale = (cx - cxyz_min) / (cxyz_max-cxyz_min)
    cy_rescale = (cy - cxyz_min) / (cxyz_max-cxyz_min)
    cz_rescale = (cz - cxyz_min) / (cxyz_max-cxyz_min)

    txyz_min = min(tx.min(),ty.min(),tz.min())
    txyz_max = max(tx.max(),ty.max(),tz.max())

    tx_rescale = (tx - txyz_min) / (txyz_max-txyz_min)
    ty_rescale = (ty - txyz_min) / (txyz_max-txyz_min)
    tz_rescale = (tz - txyz_min) / (txyz_max-txyz_min)

    nchain_cells = 20
    nextincell_c,firstincell_c = nu.neighbour_utils.init_chain(cx_rescale,cy_rescale,cz_rescale,nchain_cells)
    nextincell_t,firstincell_t = nu.neighbour_utils.init_chain(tx_rescale,ty_rescale,tz_rescale,nchain_cells)
    # Factor to help convert into box units (note maxime's merger_tree reader has converted halo positions so be careful here!)
    r2kpc_ideal, redshift, little_h, m2Msun,junk,junk = ur.Set_Units(RunDir, timestep, "ideal")
    boxlen_cMpc = dm.jeje_utils.get_param_real(RunDir,timestep,'boxlen') # Box size in box units
    boxlen_cMpc *= r2kpc_ideal
    boxlen_cMpc = boxlen_cMpc * (1+redshift) /1e3 # cMpc (comoving)
    boxlen_cMpc = boxlen_cMpc * little_h # h^-1 cMpc
    factor = 1./boxlen_cMpc

    # Loop over final timestep haloes
    for halo_group_str in zoom_group:

        # Only compute fluxes for the main halo
        if "_main" in halo_group_str:
            break
        
    if verbose:
        print "Now selecting cells/tracers within the halo. Computing for progenitors of halo", halo_group_str

    halo_group = zoom_group[halo_group_str]        
    mp_group = zoom_group[halo_group_str+"/main_progenitors"]

    # Identify the main progenitor at this timestep
    ok_ts = np.array(mp_group["halo_ts"])==timestep_tree # haloes on this timestep
        
    xh = mp_group["x"][ok_ts] * factor + 0.5
    yh = mp_group["y"][ok_ts] * factor + 0.5
    zh = mp_group["z"][ok_ts] * factor + 0.5
        
    # Use r200 for central haloes, and rvir (for now) for satellite haloes
    if mp_group["host_halo_id"][ok_ts] == mp_group["halo_id"][ok_ts]:
        rvirh = mp_group["r200"][ok_ts] / r2kpc
    else:
        rvirh = mp_group["rvir"][ok_ts] * factor
        
    if len(xh) == 0:
        print "no halo on this step, skipping"
        continue

    vxh = mp_group["vx"][ok_ts] / v2kms # Halo velocity in Code units
    vyh = mp_group["vy"][ok_ts] / v2kms
    vzh = mp_group["vz"][ok_ts] / v2kms

    # Select cells in the vicinity of the halo (using a chain algorithm)
    rmax = rvirh*rmax_tr
        
    # Rescale halo positions for efficient cell division in chain algorithm
    xh_rescale = (xh-cxyz_min)/(cxyz_max-cxyz_min)
    yh_rescale = (yh-cxyz_min)/(cxyz_max-cxyz_min)
    zh_rescale = (zh-cxyz_min)/(cxyz_max-cxyz_min)
    rmax_rescale = rmax/(cxyz_max-cxyz_min)

    ncell_in_rmax = nu.neighbour_utils.get_npart_in_sphere_with_chain(RunDir,timestep,cx_rescale,cy_rescale,cz_rescale,xh_rescale,yh_rescale,zh_rescale,rmax_rescale,firstincell_c,nextincell_c)
    if ncell_in_rmax > 0:
        select_c = nu.neighbour_utils.get_part_indicies_in_sphere_with_chain(RunDir,timestep,cx_rescale,cy_rescale,cz_rescale,xh_rescale,yh_rescale,zh_rescale,rmax_rescale,ncell_in_rmax,firstincell_c,nextincell_c)
    else:
        select_c = np.zeros_like(cx) < 0

    xh_rescale = (xh-txyz_min)/(txyz_max-txyz_min)
    yh_rescale = (yh-txyz_min)/(txyz_max-txyz_min)
    zh_rescale = (zh-txyz_min)/(txyz_max-txyz_min)
    rmax_rescale = rmax/(txyz_max-txyz_min)

    ncell_in_rmax = nu.neighbour_utils.get_npart_in_sphere_with_chain(RunDir,timestep,tx_rescale,ty_rescale,tz_rescale,xh_rescale,yh_rescale,zh_rescale,rmax_rescale,firstincell_t,nextincell_t)
    if ncell_in_rmax > 0:
        select_t = nu.neighbour_utils.get_part_indicies_in_sphere_with_chain(RunDir,timestep,tx_rescale,ty_rescale,tz_rescale,xh_rescale,yh_rescale,zh_rescale,rmax_rescale,ncell_in_rmax,firstincell_t,nextincell_t)
    else:
        select_t = np.zeros_like(tx) < 0

    ######## Do tracer preselection ############
    tx_h = tx[select_t]
    ty_h = ty[select_t]
    tz_h = tz[select_t]
    tid_h = id_tr[select_t]
    tmass_h = tmass[select_t]
        
    # Put the halo at the centre of the particle coordinate system
    tx_h = tx_h - xh; ty_h = ty_h - yh; tz_h = tz_h -zh
    # Compute the radius to each tracer
    tr_h = np.sqrt(np.square(tx_h) + np.square(ty_h) + np.square(tz_h))
    # Select tracers within desired radial range
    ok = (tr_h > rvirh * rmin_tr) & (tr_h < rvirh * rmax_tr)
    tx_h = tx_h[ok]; ty_h = ty_h[ok]; tz_h = tz_h[ok]
    tid_h = tid_h[ok]; tr_h = tr_h[ok]; tmass_h = tmass_h[ok]



    ####### pre-Select cells and compute props #################
    # First select cells within the vicinity of the halo
    cx_h = cx[select_c]
    cy_h = cy[select_c]
    cz_h = cz[select_c]
    rho_snap_h = rho_snap[select_c]
    pre_snap_h = pre_snap[select_c]
    cvx_h = cvx[select_c]
    cvy_h = cvy[select_c]
    cvz_h = cvz[select_c]
    clev_h = clev[select_c]
    c_comp_h = c_component_index[select_c]
    
    if RHD:
        xH1_h = xH1[select_c]
        xHe1_h =xHe1[select_c]
        xHe2_h =xHe2[select_c]

    if verbose:
        print "Done, now computing cell properties"
        
    ### Compute needed cell properties ###
    # Put the halo at the centre of the particle coordinate system
    cx_h = cx_h - xh; cy_h = cy_h - yh; cz_h = cz_h -zh
    cvx_h = cvx_h - vxh; cvy_h = cvy_h - vyh; cvz_h = cvz_h - vzh
    
    # Compute the radius to each cell
    cr_h = np.sqrt(np.square(cx_h) + np.square(cy_h) + np.square(cz_h))

    # Compute dot product of cell "velocity with radial unit vector
    cv_radial_h = np.sum(np.array([cvx_h,cvy_h,cvz_h]) * np.array([cx_h,cy_h,cz_h]),axis=0) / cr_h

    # Compute temperature over effective molecular weight in Kelvin
    cut.jeje_utils.read_conversion_scales(RunDir,timestep)
    scale_temk = (cut.jeje_utils.dp_scale_l / cut.jeje_utils.dp_scale_t)**2 * cut.jeje_utils.mh / cut.jeje_utils.kb
    temp_mu_h = pre_snap_h/rho_snap_h * scale_temk # T/mu in K
    
    # Compute actual temperature and neutral fraction
    if RHD:
        xhi_h = (1-xH1_h)# Hydrogen neutral fraction
        X_frac=0.76
        mu_h = 1./( X_frac*(1.+xH1_h) + 0.25*(1.-X_frac)*(1. +xHe1_h +2.*xHe2_h) )
    else:
        mu_h, xhi_h = cut.cell_utils.ramses_get_mu_xhi(RunDir,timestep,rho_snap_h, pre_snap_h, True)
    temp_h = temp_mu_h * mu_h
            
    # Compute cell size using amr refinement level and box size
    #csize_h = boxlen_pkpc / 2.**clev_h # proper kpc
    #cmass_h = rho_snap_h * rho2msunpkpc3 * csize_h**3 # Msun - gas mass in each cell
    
    # Compute nh
    nh_h = rho_snap_h * cut.jeje_utils.dp_scale_d / cut.jeje_utils.mh * cut.jeje_utils.xh

    if verbose:
        print "Done, now selecting cells within user-specified limits"
            
    ### Now select all cells which are within any of the selection boxes ###
    ok = (nh_h > nh_min_tr) & (nh_h < nh_max_tr)
    ok = ok & (temp_h > T_min_tr) & (temp_h < T_max_tr)
    ok = ok & (xhi_h > xhi_min_tr) & (xhi_h < xhi_max_tr)
    ok = ok & (cv_radial_h > cv_min_tr) & (cv_radial_h < cv_max_tr)
    ok = ok & (c_comp_h >= comp_min_tr) & (c_comp_h <= comp_max_tr)
    ok = ok & (cr_h > rvirh * rmin_tr) & (cr_h < rvirh * rmax_tr)
    
    cx_h = cx_h[ok]; cy_h = cy_h[ok]; cz_h = cz_h[ok]
    rho_snap_h = rho_snap_h[ok]; pre_snap_h = pre_snap_h[ok]
    cvx_h = cvx_h[ok]; cvy_h = cvy_h[ok]; cvz_h = cvz_h[ok]
    clev_h = clev_h[ok]; c_comp_h = c_comp_h[ok]
    temp_h = temp_h[ok]; cv_radial_h = cv_radial_h[ok]
    cr_h = cr_h[ok]; nh_h = nh_h[ok]

    csize_h = boxlen_pkpc / 2.**clev_h / r2kpc # box units

    ### Match cells to tracers ###
    if verbose:
        print "Done, now matching cells to tracers"

    ind_c, ok_match = flux_utils.Match_Cell_Particle(csize_h, cx_h, cy_h, cz_h, tx_h, ty_h, tz_h)
    
    tx_h = tx_h[ok_match]; ty_h = ty_h[ok_match]; tz_h = tz_h[ok_match]
    tid_h = tid_h[ok_match]; tr_h = tr_h[ok_match]; tmass_h = tmass_h[ok_match]
    
    temp_tr = temp_h[ind_c][ok_match]
    nh_tr = nh_h[ind_c][ok_match]
    cv_tr = cv_radial_h[ind_c][ok_match]
    xhi_tr = xhi_h[ind_c][ok_match]
    comp_tr = c_comp_h[ind_c][ok_match]

    # Match tracers to previous timestep and compute exchange terms
    if verbose:
        print "Done, now matching tracers between previous & current timesteps - and computing exchange rates"

    if first_output:
        first_output = False
    else:

        r_match_type = ["_mean", "_ps", "_ts"]

        exchange_terms = np.zeros((len(weight_n), len(weight_n1), len(r_match_type), len(r_bins[1:])))

        for i_n in range(len(weight_n)):
            for i_n1 in range(len(weight_n1)):

                ok = (nh_tr > nh_lo_n1[i_n1]) & (nh_tr < nh_hi_n1[i_n1])
                ok = ok & (temp_tr > T_lo_n1[i_n1]) & (temp_tr < T_hi_n1[i_n1])
                ok = ok & (xhi_tr > xhi_lo_n1[i_n1]) & (xhi_tr < xhi_hi_n1[i_n1])
                ok = ok & (cv_tr > cv_lo_n1[i_n1]) & (cv_tr < cv_hi_n1[i_n1])
                ok = ok & (comp_tr == comp_n1[i_n1])
                ok = ok & (tr_h > rvir_lo_n1[i_n1]*rvirh) & (tr_h < rvir_hi_n1[i_n1]*rvirh)

                if len(tid_prev[i_n])==0 or len(tid_h[ok])==0:
                    continue
                        
                ptr = ms.match(tid_prev[i_n], tid_h[ok])
                ok_match = ptr >= 0
                #print name_n[i_n], name_n1[i_n1], len(ptr), len(ptr[ok_match])
                
                # Split into radial bins

                for i_r_match in range(len(r_match_type)):
                    
                    if i_r_match == 0:
                        r_match = 0.5 * (r_tr_prev[i_n][ok_match] + tr_h[ok][ptr][ok_match])
                    elif i_r_match == 1:
                        r_match = r_tr_prev[i_n][ok_match]
                    elif i_r_match == 2:
                        r_match = tr_h[ok][ptr][ok_match]
                    else:
                        print "nope"
                        quit()
                            
                    for i_r in range(len(r_bins[0:-1])):
                        okr = (r_match > r_bins[i_r]*rvirh) & (r_match < r_bins[i_r+1]*rvirh)

                        tmass_xhi_prev = (tmass_prev[i_n]*xhi_tr_prev[i_n])[ok_match][okr]
                        tmass_xhii_prev = (tmass_prev[i_n]*(1-xhi_tr_prev[i_n]))[ok_match][okr]

                        tmass_xhi = (tmass_h*xhi_tr)[ok][ptr][ok_match][okr]
                        tmass_xhii = (tmass_h*(1-xhi_tr))[ok][ptr][ok_match][okr]

                        # Note that the exchange term is  (d / d r') (d M / d t), where r' = r / r_vir, and exchange term units are mass per unit time
                        # This way the radial integral over the entire radial profile gives the total mass per unit time being exchanged within the halo
                        
                        if weight_n[i_n] == "neutral" and weight_n1[i_n1] == "neutral":
                            exchange_terms[i_n][i_n1][i_r_match][i_r] = np.sum( np.min(( tmass_xhi_prev, tmass_xhi ),axis=0) ) / delta_t_actual / delta_rprime # Msun yr^-1
                        elif weight_n[i_n] == "neutral" and weight_n1[i_n1] == "all":
                            exchange_terms[i_n][i_n1][i_r_match][i_r] = np.sum( tmass_xhi_prev ) / delta_t_actual / delta_rprime
                        elif weight_n[i_n] == "ion" and weight_n1[i_n1] == "ion":
                            exchange_terms[i_n][i_n1][i_r_match][i_r] = np.sum( np.min(( tmass_xhii_prev, tmass_xhii ),axis=0) ) / delta_t_actual / delta_rprime
                        elif weight_n[i_n] == "ion" and weight_n1[i_n1] == "all":
                            exchange_terms[i_n][i_n1][i_r_match][i_r] = np.sum( tmass_xhii_prev ) / delta_t_actual / delta_rprime
                        elif weight_n[i_n] == "neutral" and weight_n1[i_n1] == "ion":
                            exchange_terms[i_n][i_n1][i_r_match][i_r] = np.sum( np.max(( tmass_xhi_prev - tmass_xhi ,np.zeros_like(tmass_xhi)),axis=0) ) / delta_t_actual / delta_rprime
                        elif weight_n[i_n] == "ion" and weight_n1[i_n1] == "neutral":
                            exchange_terms[i_n][i_n1][i_r_match][i_r] = np.sum( np.max(( tmass_xhii_prev - tmass_xhii ,np.zeros_like(tmass_xhi)),axis=0) ) / delta_t_actual / delta_rprime
                        else:
                            print weight_n[i_n], "into", weight_n1[i_n1], "is not implemented"
                            quit()
                        
        # Store results
        exchange_terms_list.append(exchange_terms)
        # Store which timesteps were kept
        timestep_save_list.append(timestep)
            
    if verbose:
        print "Done, Selecting tracers for next timestep"

            
    # Save tracer info for a certain number of previous timesteps, then select the one we want to use on the next step through the timestep loop

    if first_output_save:
        first_output_save = False
        temp_tr_save = []
        nh_tr_save = []
        cv_tr_save = []
        xhi_tr_save = []
        tmass_save = []
        comp_tr_save = []
        tid_save = []
        r_tr_save = []

    if len(temp_tr_save) == n_save:
        temp_tr_save = temp_tr_save[1:]
        nh_tr_save = nh_tr_save[1:]
        cv_tr_save = cv_tr_save[1:]
        xhi_tr_save = xhi_tr_save[1:]
        tmass_save = tmass_save[1:]
        comp_tr_save = comp_tr_save[1:]
        tid_save = tid_save[1:]
        r_tr_save = r_tr_save[1:]

    temp_tr_prev = []
    nh_tr_prev = []
    cv_tr_prev = []
    xhi_tr_prev = []
    tmass_prev = []
    comp_tr_prev = []
    tid_prev = []
    r_tr_prev = []
    for i_select in range(len(weight_n)):

        ok = (nh_tr > nh_lo_n[i_select]) & (nh_tr < nh_hi_n[i_select])
        ok = ok & (temp_tr > T_lo_n[i_select]) & (temp_tr < T_hi_n[i_select])
        ok = ok & (xhi_tr > xhi_lo_n[i_select]) & (xhi_tr < xhi_hi_n[i_select])
        ok = ok & (cv_tr > cv_lo_n[i_select]) & (cv_tr < cv_hi_n[i_select])
        ok = ok & (comp_tr == comp_tr[i_select])
        ok = ok & (tr_h > rvir_lo_n[i_select]*rvirh) & (tr_h < rvir_hi_n[i_select]*rvirh)

        temp_tr_prev.append(temp_tr[ok])
        nh_tr_prev.append(nh_tr[ok])
        cv_tr_prev.append(cv_tr[ok])
        xhi_tr_prev.append(xhi_tr[ok])
        tmass_prev.append(tmass_h[ok])
        comp_tr_prev.append(comp_tr[ok])
        tid_prev.append(tid_h[ok])
        r_tr_prev.append(tr_h[ok])

    temp_tr_save.append(temp_tr_prev)
    nh_tr_save.append(nh_tr_prev)
    cv_tr_save.append(cv_tr_prev)
    xhi_tr_save.append(xhi_tr_prev)
    tmass_save.append(tmass_prev)
    comp_tr_save.append(comp_tr_prev)
    tid_save.append(tid_prev)
    r_tr_save.append(r_tr_prev)

    # Approximate halo dynamical time as 10% age of Universe at a given epoch
    delta_t_choose = tdyn_frac * t_cosmic * 0.1 # Gyr


    timesteps_out = timesteps_tree_out +1
    redshifts_out = np.zeros(len(timesteps_out))
    for i_c in range(len(timesteps_out)):
        junk, redshifts_out[i_c], junk,junk,junk,junk = ur.Set_Units(RunDir, timesteps_out[i_c])

    a_out = 1./(1.+redshifts_out)
    t_out, junk = uc.t_Universe(a_out,omega_0,little_h)

    ind_choose = np.argmin(abs(t_out - (t_out[min(i_timestep_tree+1,len(timesteps_out)-1)]- delta_t_choose)))
    delta_i = i_timestep_tree+1 - ind_choose

    delta_t_actual = t_out[min(i_timestep_tree+1,len(timesteps_out)-1)] - t_out[ind_choose]
    
    print "On next step, the offset between outputs used to compute the exchange rates is", delta_i
    
    # Get the corresponding index from the saved quantities lists
    if not first_output_save:
        i_choose_save = len(temp_tr_save) - delta_i
        if i_choose_save < 0:
            print "Warning: we can't go back far enough for this step, using earliest saved output"
            i_choose_save = 0
        if i_choose_save >= len(temp_tr_save):
            print "Warning: closest to desired output is actually the current output, going back to one before that"
            i_choose_save = len(temp_tr_save)-1
        print ""
        print delta_i, len(temp_tr_save), len(temp_tr_save) - delta_i

        temp_tr_prev = temp_tr_save[i_choose_save]
        nh_tr_prev = nh_tr_save[i_choose_save]
        cv_tr_prev = cv_tr_save[i_choose_save]
        xhi_tr_prev = xhi_tr_save[i_choose_save]
        tmass_prev = tmass_save[i_choose_save]
        comp_tr_prev = comp_tr_save[i_choose_save]
        tid_prev = tid_save[i_choose_save]
        r_tr_prev = r_tr_save[i_choose_save]
        



    
    if timestep == timestep_final:
        print "writing data to disk"

        exchange_names = []
        exchange_out_list = []
        n_ts = timestep_final-1
        n_r = len(r_bins[1:])
        for i_n in range(len(weight_n)):
            for i_n1 in range(len(weight_n1)):
                for i_r_match in range(len(r_match_type)):
                    exchange_names.append("exchange_"+name_n[i_n]+"_"+name_n1[i_n1]+r_match_type[i_r_match]+"_tdyn_"+str(tdyn_frac).replace(".","p"))
                    exchange_out_list.append(np.zeros((n_ts, n_r)) * np.nan)

        for dset in mp_group:
            for name in exchange_names:
                if name == dset:
                    del mp_group[dset]

        # Reshape data array into desired output_form
        for i_ts, ts in enumerate(timestep_save_list):
            i_name = 0
            for i_n in range(len(weight_n)):
                for i_n1 in range(len(weight_n1)):
                    for i_r_match in range(len(r_match_type)):
                        i_ts_mp = np.arange(0,timestep_final)[ts-2]
                        exchange_out_list[i_name][i_ts_mp] = exchange_terms_list[i_ts][i_n][i_n1][i_r_match]

                        #print i_name, exchange_terms_list[i_ts][i_n][i_n1]

                        i_name += 1

        for name, data in zip(exchange_names, exchange_out_list):
            mp_group.create_dataset(name,data=data)

subvol_File.close()
File.close()
