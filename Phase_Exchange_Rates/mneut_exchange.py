import numpy as np
import h5py
from utilities_plotting import *
import dm_utils as dm

#Zoom = "Zoom-7-5092"
#Run = "RhdRun-tracer_norad_bug"
#timestep_final = 105
#OutDir = "/cral4/mitchell/data/Tracer_Test/PostProcess/"

Zoom = "Zoom-7-5092"
Run = "RhdRun-tracer_radfix"
timestep_final = 107
OutDir = "/cral2/mitchell/PostProcess/"

# Choose redshifts to mean stack over
#zlo = 4.45
#zhi = 4.5
zlo = 4
zhi = 6

RunDir = "/cral2/mitchell/" + Zoom +"/"+Run

nrow = 2; ncol = 2

xlo = 0.0; xhi = 1.0

dm.jeje_utils.read_conversion_scales(RunDir,timestep_final)
    
omega_0,lambda_0,little_h = dm.jeje_utils.read_cosmo_params(RunDir,timestep_final)
omega_b = dm.jeje_utils.get_param_real(RunDir,timestep_final,"omega_b")
fb = omega_b / omega_0
    
all_redshifts = np.zeros(timestep_final-1)
for n, timestep in enumerate(np.arange(2,timestep_final+1)):
    all_redshifts[n] = dm.jeje_utils.get_snap_redshift(RunDir,timestep)


# Read the hdf5 file
filename = Zoom + "_" + Run + "_tsf" + str(timestep_final) +".hdf5"
File = h5py.File(OutDir+filename)
print OutDir+filename
zoom_group = File[Zoom]
print filename, Zoom

for halo_group_str in zoom_group:
    # Check this is a halo
    if "_main" in halo_group_str:
        break

halo_group = zoom_group[halo_group_str]
mp_group = halo_group["main_progenitors"]

flux_str   = "fluxes/flux_history"
flux_group = halo_group[flux_str]

redshifts = flux_group["redshift"][:]

nono_list_ps = mp_group["exchange_neut_out_neut_out_ps"][:]*1e-9

if len(redshifts) != len(nono_list_ps[:,0]):
    print "error: redshift grid and exchange rate grid not the same"
    quit()

okz = (redshifts <= zhi) & (redshifts >= zlo)

# neutral outflow > component
nono_list_ps = nono_list_ps[okz]
nowio_list = mp_group["exchange_neut_out_wion_out_ps"][:][okz] * 1e-9 # Msun yr^-1
nohio_list = mp_group["exchange_neut_out_hion_out_ps"][:][okz] * 1e-9
noin_list = mp_group["exchange_neut_out_tot_inflow_ps"][:][okz] * 1e-9

# component > neutral outflow
nono_list_ts = mp_group["exchange_neut_out_neut_out_ts"][:][okz] * 1e-9
wiono_list = mp_group["exchange_wion_out_neut_out_ts"][:][okz] * 1e-9
hiono_list = mp_group["exchange_hion_out_neut_out_ts"][:][okz] * 1e-9
inno_list = mp_group["exchange_neut_in_neut_out_ts"][:][okz] * 1e-9 + mp_group["exchange_wion_in_neut_out_ts"][:][okz] * 1e-9 + mp_group["exchange_hion_in_neut_out_ts"][:][okz] * 1e-9

# neutral inflow > component
nini_list_ps = mp_group["exchange_neut_in_neut_in_ps"][:][okz] * 1e-9
niwii_list = mp_group["exchange_neut_in_wion_in_ps"][:][okz] * 1e-9
nihii_list = mp_group["exchange_neut_in_hion_in_ps"][:][okz] * 1e-9
niout_list = mp_group["exchange_neut_in_neut_out_ps"][:][okz] * 1e-9 + mp_group["exchange_neut_in_wion_out_ps"][:][okz] * 1e-9 + mp_group["exchange_neut_in_hion_out_ps"][:][okz] * 1e-9

# component > neutral inflow
nini_list_ts = mp_group["exchange_neut_in_neut_in_ts"][:][okz] * 1e-9
wiini_list = mp_group["exchange_wion_in_neut_in_ts"][:][okz] * 1e-9
hiini_list = mp_group["exchange_hion_in_neut_in_ts"][:][okz] * 1e-9
outni_list = mp_group["exchange_neut_out_neut_in_ts"][:][okz] * 1e-9 + mp_group["exchange_wion_out_neut_in_ts"][:][okz] * 1e-9 + mp_group["exchange_hion_out_neut_in_ts"][:][okz] * 1e-9


File.close()

py.rcParams.update(latexParams)
py.rcParams.update({'figure.subplot.wspace':0,'figure.subplot.hspace':0,
                    'figure.figsize':[3.32*ncol,2.49*nrow],
                    'figure.subplot.left':0.24/ncol,
                    'figure.subplot.bottom':0.18/nrow,
                    'figure.subplot.top':0.96,
                    'legend.fontsize':6,
                    'font.size':12,
                    'axes.labelsize':11})

py.figure()
subplots = panelplot(nrow,ncol)
for n,ax in enumerate(subplots):
    py.axes(ax)

    # Stack different radii in time
    r_bins = np.arange(0.0,1.1,0.1)
    r_mid = 0.5 * (r_bins[1:] + r_bins[0:-1])

    if n == 0:

        nono_stack_ps = np.zeros_like(r_bins[1:])
        nowio_stack = np.zeros_like(r_bins[1:])
        nohio_stack = np.zeros_like(r_bins[1:])
        noin_stack = np.zeros_like(r_bins[1:])
        
        for i_r in range(len(r_bins[1:])):
       
            # Then stack outputs at a given radius
            ok = np.isnan(nono_list_ps[:,i_r])==False
            nono_stack_ps[i_r] = np.sum( nono_list_ps[:,i_r][ok] ) / len(nono_list_ps[:,i_r][ok])
          
            ok = np.isnan(nowio_list[:,i_r])==False
            nowio_stack[i_r] = np.sum( nowio_list[:,i_r][ok] ) / len(nowio_list[:,i_r][ok])
          
            ok = np.isnan(nohio_list[:,i_r])==False
            nohio_stack[i_r] = np.sum( nohio_list[:,i_r][ok] ) / len(nohio_list[:,i_r][ok])
          
            ok = np.isnan(noin_list[:,i_r])==False
            noin_stack[i_r] = np.sum( noin_list[:,i_r][ok] ) / len(noin_list[:,i_r][ok])
            
        py.plot( r_mid, np.log10(nono_stack_ps), c="b",label=r"$\mathrm{\bf{Neutral \, outflow}} \, > \, \mathrm{Neutral \, outflow}$")
        py.plot( r_mid, np.log10(nowio_stack), c="g",label=r"$\mathrm{\bf{Neutral \, outflow}} \, > \, \mathrm{Warm \, outflow}$")
        py.plot( r_mid, np.log10(nohio_stack), c="m",label=r"$\mathrm{\bf{Neutral \, outflow}} \, > \, \mathrm{Hot \, outflow}$")
        py.plot( r_mid, np.log10(noin_stack), c="k",label=r"$\mathrm{\bf{Neutral \, outflow}} \, > \, \mathrm{Inflow}$")
        
        
    if n == 1:

        nono_stack_ts = np.zeros_like(r_bins[1:])
        wiono_stack = np.zeros_like(r_bins[1:])
        hiono_stack = np.zeros_like(r_bins[1:])
        inno_stack = np.zeros_like(r_bins[1:])
        
        for i_r in range(len(r_bins[1:])):
       
            # Then stack outputs at a given radius
            ok = np.isnan(nono_list_ts[:,i_r])==False
            nono_stack_ts[i_r] = np.sum( nono_list_ts[:,i_r][ok] ) / len(nono_list_ts[:,i_r][ok])
          
            ok = np.isnan(wiono_list[:,i_r])==False
            wiono_stack[i_r] = np.sum( wiono_list[:,i_r][ok] ) / len(wiono_list[:,i_r][ok])
          
            ok = np.isnan(hiono_list[:,i_r])==False
            hiono_stack[i_r] = np.sum( hiono_list[:,i_r][ok] ) / len(hiono_list[:,i_r][ok])
          
            ok = np.isnan(inno_list[:,i_r])==False
            inno_stack[i_r] = np.sum( inno_list[:,i_r][ok] ) / len(inno_list[:,i_r][ok])
    
        
        py.plot( r_mid, np.log10(nono_stack_ts), c="b",label=r"$\mathrm{Neutral \, outflow} \, > \, \mathrm{\bf Neutral \, outflow}$")
        py.plot( r_mid, np.log10(wiono_stack), c="g",label=r"$\mathrm{Warm \, outflow} \, > \, \mathrm{\bf Neutral \, outflow}$")
        py.plot( r_mid, np.log10(hiono_stack), c="m",label=r"$\mathrm{Hot \, outflow} \, > \, \mathrm{\bf Neutral \, outflow}$")
        py.plot( r_mid, np.log10(inno_stack), c="k",label=r"$\mathrm{Inflow} \, > \, \mathrm{\bf Neutral \, outflow}$")
        
    if n == 2:
        nini_stack_ps = np.zeros_like(r_bins[1:])
        niwii_stack = np.zeros_like(r_bins[1:])
        nihii_stack = np.zeros_like(r_bins[1:])
        niout_stack = np.zeros_like(r_bins[1:])
        for i_r in range(len(r_bins[1:])):
       
            # Then stack outputs at a given radius
            ok = np.isnan(nini_list_ps[:,i_r])==False
            nini_stack_ps[i_r] = np.sum( nini_list_ps[:,i_r][ok] ) / len(nini_list_ps[:,i_r][ok])

            ok = np.isnan(niwii_list[:,i_r])==False
            niwii_stack[i_r] = np.sum( niwii_list[:,i_r][ok] ) / len(niwii_list[:,i_r][ok])

            ok = np.isnan(nihii_list[:,i_r])==False
            nihii_stack[i_r] = np.sum( nihii_list[:,i_r][ok] ) / len(nihii_list[:,i_r][ok])

            ok = np.isnan(niout_list[:,i_r])==False
            niout_stack[i_r] = np.sum( niout_list[:,i_r][ok] ) / len(niout_list[:,i_r][ok])

        py.plot( r_mid, np.log10(nini_stack_ps), c="b",label=r"$\mathrm{\bf{Neutral \, inflow}} \, > \, \mathrm{Neutral \, inflow}$")
        py.plot( r_mid, np.log10(niwii_stack), c="g",label=r"$\mathrm{\bf{Neutral \, inflow}} \, > \, \mathrm{Warm \, inflow}$")
        py.plot( r_mid, np.log10(nihii_stack), c="m",label=r"$\mathrm{\bf{Neutral \, inflow}} \, > \, \mathrm{Hot \, inflow}$")
        py.plot( r_mid, np.log10(niout_stack), c="k",label=r"$\mathrm{\bf{Neutral \, inflow}} \, > \, \mathrm{Outflow}$")

    if n == 3:

        nini_stack_ts = np.zeros_like(r_bins[1:])
        wiini_stack = np.zeros_like(r_bins[1:])
        hiini_stack = np.zeros_like(r_bins[1:])
        outni_stack = np.zeros_like(r_bins[1:])
        
        for i_r in range(len(r_bins[1:])):
       
            # Then stack outputs at a given radius
            ok = np.isnan(nini_list_ts[:,i_r])==False
            nini_stack_ts[i_r] = np.sum( nini_list_ts[:,i_r][ok] ) / len(nini_list_ts[:,i_r][ok])
          
            ok = np.isnan(wiini_list[:,i_r])==False
            wiini_stack[i_r] = np.sum( wiini_list[:,i_r][ok] ) / len(wiini_list[:,i_r][ok])
          
            ok = np.isnan(hiini_list[:,i_r])==False
            hiini_stack[i_r] = np.sum( hiini_list[:,i_r][ok] ) / len(hiini_list[:,i_r][ok])
          
            ok = np.isnan(outni_list[:,i_r])==False
            outni_stack[i_r] = np.sum( outni_list[:,i_r][ok] ) / len(outni_list[:,i_r][ok])
    
        
        py.plot( r_mid, np.log10(nini_stack_ts), c="b",label=r"$\mathrm{Neutral \, inflow} \, > \, \mathrm{\bf Neutral \, inflow}$")
        py.plot( r_mid, np.log10(wiini_stack), c="g",label=r"$\mathrm{Warm \, inflow} \, > \, \mathrm{\bf Neutral \, inflow}$")
        py.plot( r_mid, np.log10(hiini_stack), c="m",label=r"$\mathrm{Hot \, inflow} \, > \, \mathrm{\bf Neutral \, inflow}$")
        py.plot( r_mid, np.log10(outni_stack), c="k",label=r"$\mathrm{Outflow} \, > \, \mathrm{\bf Neutral \, inflow}$")

    
    if n >= ncol*nrow- ncol:
        py.xlabel(r"$r \, / R_{\mathrm{vir}}$")
    if n % ncol == 0:
        py.ylabel(r"$\log_{10}\left(\frac{\mathrm{d}}{\mathrm{d} (r/r_{\mathrm{vir}})} \, \frac{\mathrm{d} m}{\mathrm{d} t} \, / \mathrm{M_\odot yr{-1}}\right)$")

    py.xlim((xlo,xhi))
    ylo = -1.25; yhi = 1.5
    py.ylim((ylo,yhi))

    py.legend(loc="upper right",frameon=False,numpoints=1,scatterpoints=1)

fig_name = "mneut_exchange_tstack_"+Zoom+"_"+Run+".pdf"
py.savefig("../Figures/"+fig_name)
py.show()
