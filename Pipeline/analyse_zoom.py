import os

################ Simulation information ##############################

########### No feedback runs

'''Zoom_str = "Zoom-7-"
Zoom_no_list =        [1290]
timestep_final_list = [155] # Should automate this
Run = "tracer_run_lr"
feedback = True
Rhd = False
RunDir_path = "/cral4/mitchell/data/Tracer_Test/"'''


#Zoom_str = "Zoom-7-"
#Zoom_no_list =        [5092]
#timestep_final_list = [105] # Should automate this
#Run = "RhdRun-tracer_norad_bug"
#feedback = True
#Rhd = True
#RunDir_path = "/cral2/mitchell/"

Zoom_str = "Zoom-7-"
Zoom_no_list =        [5092]
timestep_final_list = [155]
Run = "RhdRun-tracer_radfix"
feedback = True
Rhd = True
RunDir_path = "/cral2/mitchell/"

################## Options ###########################################

#timesteps_output = "all" # Options are "all", "final", or a specific timestep
timesteps_output = "all"

Verbose = True

only_verify  = False

Write_Merger_Tree      = False
Write_Subvol           = False
Write_M200             = False
Write_M200_max_past    = False # Don't run this for timesteps_output = "final" because that options doesn't use trees
#Write_Compartmentalization = False Replaced by Write_Tidal_Radius

Write_Tidal_Radius = True
Write_Luminosities     = False

#Write_Halo_Mass        = False # Redundant
Write_Sizes            = False
Write_Magnitudes       = False
#Write_Gas_Mass         = False # Redundant, use _exc_sub version
Write_Gas_Mass_Exc_Sub = False

Write_Stellar_Mass_SFR = False
Write_SFH              = False

Write_Flux             = False
Write_Flux_Hist        = False

Write_Exch = False

Write_Contamination    = False
#Write_Maps             = False # Runs into memory problems, use the slightly less ef(ficient (in principle) but memory safe option below
Write_Maps_Memsafe     = False

################# Directory to write output hdf5 files ###################

OutDir = "/cral2/mitchell/PostProcess/"
print "writing to", OutDir

# Write output directories if necessary
try:
    os.makedirs(OutDir)
except OSError:
    pass # Directory already exists

for Zoom_no, timestep_final, in zip(Zoom_no_list, timestep_final_list):

    if only_verify:
        continue

    Zoom = Zoom_str + str(Zoom_no)
    RunDir = RunDir_path + Zoom + "/" + Run
    HaloDir  = RunDir + "/Halos/"
    TreeDir = RunDir + "/Trees/"


    ################# Run scripts to write hdf5 files #########################
    args = "-zoom " + Zoom + " -Run " + Run + " -rd " + RunDir + " -hd "+HaloDir + " -td " +TreeDir + " -od "+OutDir +" -ts_final " + str(timestep_final) + " -ts_out " + str(timesteps_output) + " -rhd " + str(Rhd) + " -v " + str(Verbose)
    args_fb = args + " -fb " + str(feedback)
    
    # Select halos at the final timestep and Write their basic tree information to hdf5
    if Write_Merger_Tree:
        os.system("python write_merger_tree.py "+args)

    # Write the hdf5 subvolume files (particle/leaf cell data) for selected halos at all available timesteps
    # This is by far the time consuming part of the pipeline, hence the care not to run if it isn't needed!
    if Write_Subvol:
        os.system("python write_hdf5_subvol.py "+args)

    # Compute r200/m200 for central haloes
    if Write_M200:
        os.system("python write_m200.py "+args)

    # Compute max past r200/m200 for satellite haloes
    if Write_M200_max_past:
        os.system("python write_m200_max_past.py "+args)

    # Assign particles/cells to compartments. At the moment this just divides between in/out subhaloes and inner/outer halo
    #if Write_Compartmentalization:
    #    os.system("python write_compartmentalization.py "+args)

    # Assign particles/cells to compartments, this time with the option of using the tidal radius. At the moment this just divides between in/out subhaloes and inner/outer halo
    if Write_Tidal_Radius:
        os.system("python write_tidal_radius.py "+args)
    
    # Write stellar particle luminosities (corresponding to AB magnitude) of stellar particles in a given band (rest-frame)
    if Write_Luminosities:
        os.system("python write_luminosities.py "+args)
    
    #### Redundant now that we have m200 measurements##############
    # Write virial halo masses (summing over subhaloes)
    #if Write_Halo_Mass:
    #    os.system("python write_halo_mass_summation_tree.py "+args)


    # Write 3d half mass/ half luminosity sizes of stellar particles 
    if Write_Sizes:
        os.system("python write_sizes.py "+args)

    # Write integrated magnitudes for stellar particles in apertures across the tree in a set of given bands (rest-frame)
    if Write_Magnitudes:
        os.system("python write_integrated_magnitudes.py "+args)

    # Write stellar masses and star formation rates across the merger trees
    if Write_Stellar_Mass_SFR:
        os.system("python write_stellar_mass_sfr_tree.py "+args_fb)

    if Write_Gas_Mass_Exc_Sub:
        os.system("python write_gas_mass_tree_exc_sub.py "+args)

    # Write star formation histories
    if Write_SFH:
        os.system("python write_sfh.py "+args_fb)

    # Write fluxes across the tree
    if Write_Flux:
        os.system("python write_flux_tree.py "+args)

    # Write explicit flux histories (using pre-computed fluxes)
    if Write_Flux_Hist:
        os.system("python write_flux_history.py "+args)

    if Write_Exch:
        os.system("python write_neut_exchange.py "+args)

    # Write any low-res DM particle contamination across the tree
    if Write_Contamination:
        os.system("python write_contamination_tree.py "+args)
     
    # Out of action: Use the memory safe option below
    # Write maps for main progenitors of the main halo
    #if Write_Maps:
    #    os.system("python write_maps_tree_main.py "+args_fb)

    # Stopgap version of map writing that works on one timestep at a time
    if Write_Maps_Memsafe:
        if timesteps_output == "all":
            for ts in range(2,timestep_final+1):
                #if ts != timestep_final:
                #    continue
                if ts < 10 or ts > 140:
                    continue
                args_memsafe = args_fb + " -ts " + str(ts)
                os.system("python write_maps_tree_main_memsafe.py "+args_memsafe)
        elif timesteps_output == "final":
            args_memsafe = args_fb + " -ts " + str(timestep_final)
            os.system("python write_maps_tree_main_memsafe.py "+args_memsafe)
        else:
            try:
                args_memsafe = args_fb + " -ts " + str(int(timesteps_output))
                os.system("python write_maps_tree_main_memsafe.py "+args_memsafe)
            except:
                print "timesteps_output",timesteps_output,"is not a valid input"
                quit()


for Zoom_no, timestep_final, in zip(Zoom_no_list, timestep_final_list):

    Zoom = Zoom_str + str(Zoom_no)
    RunDir = RunDir_path + Zoom + "/" + Run
    HaloDir  = RunDir + "/Halos/"
    TreeDir = RunDir + "/Trees/"

    ################# Run scripts to write hdf5 files #########################
    args = "-zoom " + Zoom + " -Run " + Run + " -rd " + RunDir + " -hd "+HaloDir + " -td " +TreeDir + " -od "+OutDir +" -ts_final " + str(timestep_final) + " -ts_out " + str(timesteps_output) + " -v " + str(Verbose)
    args_fb = args + " -fb " + str(feedback)

    # Verify the subvol and Zoom hdf5 output files. Flag anything that didn't work.
    os.system("python verify_zoom.py "+args)
