import numpy as np
from scipy import spatial
import dm_utils as dm
import utilities_ramses as ur


def Compute_Flux_Surface_Uniform_Sampling(r_surface_in, rvir, csize, cr, cx, cy, cz, rho_snap_list, Zrho_snap_list, temp, mu, cv_radial, vscalar, in_sub, RunDir, timestep, return_map=False):
    '''Compute the mass inflow and outflow rates through a spherical surface at radius r_surface_in (as a fraction of the virial radius)

    This routine works by uniformly sampling the surface by roughly 10 x the number of cells that could intersect the surface.

    inputs:
        r_surface_in = radius of the surface as a fraction of the halo virial radius
        rvir         = virial radius in box units
        csize        = cell size in proper kpc
        cr           = distance of each cell to the centre of the halo in box units
        cx,cy,cz     = x,y,z coordinates of cells in box units
        rho_snap_list = list of cell density in Msun kpc^-3 (proper), Loop over the elements in the list to get different types of fluxes (e.g. total gas, neutral hydrogen etc)
        Zrho_snap_list = list of cell metal density in Msun kpc^-3 (proper), Loop over the elements in the list to get different types of fluxes (e.g. total gas, neutral hydrogen etc)
        temp         = cell temperature (Kelvin)
        mu           = effective molecular weight (dimensionless)
        cv_radial    = dot product of the cell velocity (must be in halo frame!) with the radial unit vector in code units
        vscalar      = magnitude of the cell velocity (in the halo frame) in code units
        in_sub       = boolean array of this cell is considered to be part of a subhalo

        RunDir       = path to the simulation output directory (used to read unit conversions)
        timestep     = timestep being analysed (used to compute unit conversions)

        return_map = control outputs (see below). 
                     False = default. Return integrated inflow and outflow fluxes at the specified radius
                     True = return full inflow and outflow flux maps at coordinates cos(theta), phi

    outputs: (incomplete, update at some point!)
        dmdt_inflow = inflow rate in Msun Gyr^-1
        dmdt_outflow = outflow rate in Msun Gyr^-1
        dmdt_inflow_exc_sub = inflow rate excluding surface elements inside substructures in Msun Gyr^-1
        dmdt_outflow_exc_sub = outflow rate excluding surface elements inside substructures in Msun Gyr^-1

        dmdr_inflow = differential mass in inflowing gas (dm(r)/dr) in Msun kpc^-1
        dmdr_outflow = differential mass in outflowing gas in Msun kpc^-1
        dmdr_inflow_exc_sub = differential mass in inflowing gas excluding surface elements inside substructures in Msun kpc^-1
        dmdr_outflow_exc_sub = differential mass in outflowing gas excluding surface elements inside substructures in Msun kpc^-1

        cv_radial_mw_inflow = mass weighted radial velocity of inflowing gas in kms^-1
        cv_radial_mw_outflow = mass weighted radial velocity of outflowing gas in kms^-1
        cv_radial_mw_inflow_exc_sub = mass weighted radial velocity of inflowing gas excluding surface elements inside substructures in kms^-1
        cv_radial_mw_outflow_exc_sub = mass weighted radial velocity of outflowing gas excluding surface elements inside substructures in kms^-1
        
        cv_radial_fw_inflow = flux weighted radial velocity of inflowing gas in kms^-1
        cv_radial_fw_outflow = flux weighted radial velocity of outflowing gas in kms^-1
        cv_radial_fw_inflow_exc_sub = flux weighted radial velocity of inflowing gas excluding surface elements inside substructures in kms^-1
        cv_radial_fw_outflow_exc_sub = flux weighted radial velocity of outflowing gas excluding surface elements inside substructures in kms^-1
        
        or

        dmdt_inflow_map = inflow rate in Msun Gyr^-1 (2d array, theta, phi)
        dmdt_outflow_map = outflow rate in Msun Gyr^-1 (2d array, theta, phi)
        density_inflow_map = density of inflowing gas (msun pkpc^-3)
        density_outflow_map = density of outflowing gas (msun pkpc^-3)
        cv_radial_inflow_map = radial velocity of inflowing gas (units?)
        cv_radial_outflow_map = radial velocity of outflowing gas (units?)
        temperature_inflow_map = temperature of inflowing gas (Kelvin)
        temperature_outflow_map = temperature of outflowing gas (Kelvin)
        in_sub_inflow_map = flag to say if in a subhalo (units: 0 = not inflowing, 1 = not in a subhalo, 2 = in a subhalo)
        in_sub_outflow_map = flag to say if in a subhalo (units: 0 = not outflowing, 1 = not in a subhalo, 2 = in a subhalo)
        (planned) metal_inflow_map = metallicity of inflowing gas
        (planned) metal_outflow_map = metallicity of outflowing gas
        costheta = cos(theta) = cos(lattitude angle) (1d array)
        phi = longitude angle (1d array)'''

    # get conversion factors for this timestep
    dm.jeje_utils.read_conversion_scales(RunDir,timestep)
    r2kpc, redshift, little_h, mass2Msun, rho2msunpkpc3, v2kms = ur.Set_Units(RunDir,timestep,"actual")

    # Radius of the surface to calculate the mass flux
    r_surface = r_surface_in * float(rvir) * r2kpc # kpc

    # Minimum/maximum radius (based on cell size) where each cell could potentially intersect this surface
    r_min = -0.87 * csize + r_surface
    r_max = 0.87 * csize + r_surface

    # Find cells that could potentially intersect the surface
    ok = (cr * r2kpc > r_min) & (cr * r2kpc < r_max)

    cxyz = np.swapaxes(np.array([cx[ok]*r2kpc, cy[ok]*r2kpc, cz[ok]*r2kpc]),0,1) # Cell coordinates for possible matches

    # Choose sampling of points on the surface
    n_candidate_cell = len(cr[ok])
    n_sample = n_candidate_cell * 10
    # Compute x,y,z coordinates of points uniformly sampling a spherical surface of radius = r_surface
    costheta = np.linspace(-1.,1.,int(np.sqrt(n_sample)))
    phi = np.linspace(0.,2*np.pi,int(np.sqrt(n_sample)))
    costheta, phi = np.meshgrid(costheta,phi)
    costheta = np.ravel(costheta); phi = np.ravel(phi)
    sintheta = np.sin(np.arccos(costheta))
    x = r_surface * sintheta * np.cos(phi)
    y = r_surface * sintheta * np.sin(phi)
    z = r_surface * costheta
    xyz = np.swapaxes(np.array([x,y,z]),0,1)

    # Recompute the number of samples
    n_sample = len(x)
    
    # For each point compute the density and velocity of the cell that intersects it using KD Trees
    cv_radial_surface = np.zeros(n_sample); vscalar_surface = np.zeros(n_sample)
    temp_surface = np.zeros(n_sample); mu_surface = np.zeros(n_sample)
    in_sub_surface = np.zeros(n_sample)

    rho_surface_list = []
    for rho_snap in rho_snap_list:
        rho_surface_list.append(np.zeros(n_sample))
    Zrho_surface_list = []
    for Zrho_snap in Zrho_snap_list:
        Zrho_surface_list.append(np.zeros(n_sample))

    n_neighbour = 128 # Number of nearest neighbours to be found by the KD Tree algorithm

    Finished = False
    missing_fraction_prev = 1.0
    while not Finished:
        tree = spatial.cKDTree(xyz,leafsize=64) # Build the KD Tree

        dist,ind = tree.query(cxyz,k=n_neighbour,p=np.inf) # Find n_neighbour nearest neighbouring points for each cell
        dist = dist.flatten(); ind = ind.flatten() # Reduce to 1d array for indexing
        csize_check = np.ravel( np.swapaxes( np.tile(csize[ok], (n_neighbour, 1)) ,0,1) ) # Cell size for each potentially matching cell
        ok2 = dist < csize_check * 0.5 # Match cells to points

        rho_snap_match_list = []
        for rho_snap in rho_snap_list:
            rho_snap_match_list.append(np.ravel( np.swapaxes( np.tile(rho_snap[ok], (n_neighbour,1)), 0,1) )[ok2]) # Density of matching cells

        Zrho_snap_match_list = []
        for Zrho_snap in Zrho_snap_list:
            Zrho_snap_match_list.append(np.ravel( np.swapaxes( np.tile(Zrho_snap[ok], (n_neighbour,1)), 0,1) )[ok2]) # Density of matching cells

        cv_radial_match = np.ravel( np.swapaxes( np.tile(cv_radial[ok], (n_neighbour,1)), 0,1) )[ok2] # Velocity of matching cells
        vscalar_match = np.ravel( np.swapaxes( np.tile(vscalar[ok], (n_neighbour,1)), 0,1) )[ok2] # Velocity of matching cells
        temp_match = np.ravel(np.swapaxes(np.tile(temp[ok], (n_neighbour,1)),0,1))[ok2]
        mu_match = np.ravel(np.swapaxes(np.tile(mu[ok], (n_neighbour,1)),0,1))[ok2]
        in_sub_match = np.ravel(np.swapaxes(np.tile(in_sub[ok], (n_neighbour,1)),0,1))[ok2]

        i_missing = 0
        for rho_surface, rho_snap_match in zip(rho_surface_list, rho_snap_match_list):
            rho_surface[ind[ok2]] = rho_snap_match # Density at each point
            if i_missing == 0: # Compute missing fraction for total gas density
                rho_surface_m = np.copy(rho_surface)
            i_missing += 1

        for Zrho_surface, Zrho_snap_match in zip(Zrho_surface_list, Zrho_snap_match_list):
            Zrho_surface[ind[ok2]] = Zrho_snap_match # Density at each point
        
        cv_radial_surface[ind[ok2]] = cv_radial_match # Radial velocity at each point
        vscalar_surface[ind[ok2]] = vscalar_match # Scalar velocity at each point
        temp_surface[ind[ok2]] = temp_match
        mu_surface[ind[ok2]] = mu_match
        in_sub_surface[ind[ok2]] = in_sub_match

        # Check the number of surface points that don't have an associated cell
        missing_fraction = len(rho_surface_m[rho_surface_m==0.0])/float( len(rho_surface_m))
        # If less than 1%, redo the KD Tree with a higher number of neighbours
        if missing_fraction > 0.01:
            n_neighbour *= 2
            missing_fraction_prev = missing_fraction
            print "Warning, fraction of surface points that didn't find a cell is ", missing_fraction, " at a radius ", r_surface_in
            #print "Recomputing the KDTree with a ", n_neighbour ," nearest neighbours"
            #Finished = True
        '''# Abort the computation if the missing fraction is not converging with increasing n_neighbour
        if missing_fraction >= missing_fraction_prev:
            print "Error: Calculation is not converging with increasing n_neighbour."
            print "Try increasing the number of gas cells you read in."
            exit()'''
        # Otherwise, we are done
        #else:
        Finished = True

    # Compute the mass outflow rate
    Gyr = 365.25 *24 *60**2 * 1e9
    kpc = 3.09*10**19
    kb = 1.38062e-23 # m^2 kg s^-2 K^-1
    mh = 1.66e-27 # kg
    g2Msun = 5.02739933e-34
    Msun2kg = 1e-3 * 1./g2Msun

    inflow = cv_radial_surface < 0.0
    inflow_exc_sub = inflow & (in_sub_surface==False)
    outflow = cv_radial_surface > 0.0
    outflow_exc_sub = outflow & (in_sub_surface==False)

    dmdt_inflow_l = []; dmdt_outflow_l = []; dmdt_inflow_exc_sub_l = []; dmdt_outflow_exc_sub_l = []
    dmdr_inflow_l = []; dmdr_outflow_l = []; dmdr_inflow_exc_sub_l = []; dmdr_outflow_exc_sub_l = []
    cv_radial_mw_inflow_l = []; cv_radial_mw_outflow_l = []; cv_radial_mw_inflow_exc_sub_l = []; cv_radial_mw_outflow_exc_sub_l = []
    cv_radial_fw_inflow_l = []; cv_radial_fw_outflow_l = []; cv_radial_fw_inflow_exc_sub_l = []; cv_radial_fw_outflow_exc_sub_l = []
    dUdr_inflow_l = []; dUdr_outflow_l = []; dUdr_inflow_exc_sub_l = []; dUdr_outflow_exc_sub_l = []
    dEkrdr_inflow_l = []; dEkrdr_outflow_l = []; dEkrdr_inflow_exc_sub_l = []; dEkrdr_outflow_exc_sub_l = []
    dEkdr_inflow_l = []; dEkdr_outflow_l = []; dEkdr_inflow_exc_sub_l = []; dEkdr_outflow_exc_sub_l = []

    dmdt_inflow_Z_l=[]; dmdt_outflow_Z_l=[]; dmdt_inflow_exc_sub_Z_l=[]; dmdt_outflow_exc_sub_Z_l = []
    dmdr_inflow_Z_l=[]; dmdr_outflow_Z_l=[]; dmdr_inflow_exc_sub_Z_l=[]; dmdr_outflow_exc_sub_Z_l = []

    for rho_surface in rho_surface_list:

        dmdt_inflow = -4 * np.pi / n_sample * np.sum( cv_radial_surface[inflow] * v2kms*1e3 * rho_surface[inflow] * r_surface**2) # Msun kpc^-1 ms^-1 
        dmdt_inflow *= Gyr / kpc # Msun Gyr^-1
        dmdt_inflow_l.append(dmdt_inflow)

        dmdt_outflow = 4 * np.pi / n_sample * np.sum( cv_radial_surface[outflow] * v2kms*1e3 * rho_surface[outflow] * r_surface**2) # Msun kpc^-1 ms^-1 
        dmdt_outflow *= Gyr / kpc
        dmdt_outflow_l.append(dmdt_outflow)

        dmdt_inflow_exc_sub = -4 * np.pi / n_sample * np.sum( cv_radial_surface[inflow_exc_sub] * v2kms*1e3 * rho_surface[inflow_exc_sub] * r_surface**2)
        dmdt_inflow_exc_sub *= Gyr / kpc # Msun Gyr^-1
        dmdt_inflow_exc_sub_l.append(dmdt_inflow_exc_sub)

        dmdt_outflow_exc_sub = 4 * np.pi / n_sample * np.sum( cv_radial_surface[outflow_exc_sub] * v2kms*1e3 * rho_surface[outflow_exc_sub] * r_surface**2)
        dmdt_outflow_exc_sub *= Gyr / kpc # Msun Gyr^-1
        dmdt_outflow_exc_sub_l.append(dmdt_outflow_exc_sub)

        # Compute differential mass of the surface, dm/dr, in inflowing and outflowing gas
        dmdr_inflow_l.append(4 * np.pi * r_surface**2 / n_sample * np.sum( rho_surface[inflow] )) # msun kpc^-1
        dmdr_outflow_l.append( 4 * np.pi * r_surface**2 / n_sample * np.sum( rho_surface[outflow] )) # msun kpc^-1

        dmdr_inflow_exc_sub_l.append( 4 * np.pi * r_surface**2 / n_sample * np.sum( rho_surface[inflow_exc_sub] )) # msun kpc^-1
        dmdr_outflow_exc_sub_l.append( 4 * np.pi * r_surface**2 / n_sample * np.sum( rho_surface[outflow_exc_sub] )) # msun kpc^-1

        # Compute the density and flux weighted average radial velocities inflowing and outflowing gas across the surface
        cv_radial_mw_inflow_l.append( np.sum( rho_surface[inflow] * cv_radial_surface[inflow]) * v2kms / np.sum( rho_surface[inflow] )) # kms^-1
        cv_radial_mw_outflow_l.append( np.sum( rho_surface[outflow] * cv_radial_surface[outflow]) * v2kms / np.sum( rho_surface[outflow] )) # kms^-1

        cv_radial_mw_inflow_exc_sub_l.append( np.sum( rho_surface[inflow_exc_sub] * cv_radial_surface[inflow_exc_sub]) * v2kms / np.sum( rho_surface[inflow_exc_sub] )) # kms^-1
        cv_radial_mw_outflow_exc_sub_l.append( np.sum( rho_surface[outflow_exc_sub] * cv_radial_surface[outflow_exc_sub]) * v2kms / np.sum( rho_surface[outflow_exc_sub] )) # kms^-1

        cv_radial_fw_inflow_l.append( np.sum( rho_surface[inflow] * np.square(cv_radial_surface[inflow])) * v2kms / np.sum( cv_radial_surface[inflow]* rho_surface[inflow] )) # kms^-1
        cv_radial_fw_outflow_l.append( np.sum( rho_surface[outflow] * np.square(cv_radial_surface[outflow])) * v2kms / np.sum( cv_radial_surface[outflow] * rho_surface[outflow] )) # kms^-1

        cv_radial_fw_inflow_exc_sub_l.append( np.sum( rho_surface[inflow_exc_sub] * np.square(cv_radial_surface[inflow_exc_sub])) * v2kms / np.sum( cv_radial_surface[inflow_exc_sub] * rho_surface[inflow_exc_sub] )) # kms^-1
        cv_radial_fw_outflow_exc_sub_l.append( np.sum( rho_surface[outflow_exc_sub] * np.square(cv_radial_surface[outflow_exc_sub])) * v2kms / np.sum( cv_radial_surface[outflow_exc_sub] * rho_surface[outflow_exc_sub] )) # kms^-1

        # Compute the thermal energy per unit radius of the surface
        dUdr_inflow_l.append( 4 * np.pi* 1.5*kb/mh *r_surface**2 / n_sample * np.sum( rho_surface[inflow] * temp_surface[inflow] / mu_surface[inflow]))
        dUdr_outflow_l.append( 4 * np.pi* 1.5*kb/mh *r_surface**2 / n_sample * np.sum( rho_surface[outflow] * temp_surface[outflow] / mu_surface[outflow]))
    
        dUdr_inflow_exc_sub_l.append( Msun2kg * 4 * np.pi* 1.5*kb/mh *r_surface**2 / n_sample * np.sum( rho_surface[inflow_exc_sub] * temp_surface[inflow_exc_sub] / mu_surface[inflow_exc_sub])) # kg m^2 s^-2 kpc^-1 = J kpc^-1
        dUdr_outflow_exc_sub_l.append( Msun2kg * 4 * np.pi* 1.5*kb/mh *r_surface**2 / n_sample * np.sum( rho_surface[outflow_exc_sub] * temp_surface[outflow_exc_sub] / mu_surface[outflow_exc_sub])) # kg m^2 s^-2 kpc^-1 = J kpc^-1
    
        # Compute the radial kinetic energy per unit radius of the surface
        dEkrdr_inflow_l.append( Msun2kg * 4 * np.pi *r_surface**2 *0.5/ n_sample * np.sum( rho_surface[inflow] * (cv_radial_surface[inflow]*v2kms*1e3)**2)) # kg kpc^-1 m^2 s^-2 = J kpc^-1
        dEkrdr_outflow_l.append( Msun2kg * 4 * np.pi *r_surface**2 *0.5/ n_sample * np.sum( rho_surface[outflow] * (cv_radial_surface[outflow]*v2kms*1e3)**2)) # kg kpc^-1 m^2 s^-2 = J kpc^-1

        dEkrdr_inflow_exc_sub_l.append( Msun2kg * 4 * np.pi *r_surface**2 *0.5/ n_sample * np.sum( rho_surface[inflow_exc_sub] * (cv_radial_surface[inflow_exc_sub]*v2kms*1e3)**2)) # kg kpc^-1 m^2 s^-2 = J kpc^-1
        dEkrdr_outflow_exc_sub_l.append( Msun2kg * 4 * np.pi *r_surface**2 *0.5/ n_sample * np.sum( rho_surface[outflow_exc_sub] * (cv_radial_surface[outflow_exc_sub]*v2kms*1e3)**2)) # kg kpc^-1 m^2 s^-2 = J kpc^-1

        # Compute the total kinetic energy per unit radius of the surface
        dEkdr_inflow_l.append( Msun2kg * 4 * np.pi *r_surface**2 *0.5/ n_sample * np.sum( rho_surface[inflow] * (vscalar_surface[inflow]*v2kms*1e3)**2)) # kg kpc^-1 m^2 s^-2 = J kpc^-1
        dEkdr_outflow_l.append( Msun2kg * 4 * np.pi *r_surface**2 *0.5/ n_sample * np.sum( rho_surface[outflow] * (vscalar_surface[outflow]*v2kms*1e3)**2)) # kg kpc^-1 m^2 s^-2 = J kpc^-1

        dEkdr_inflow_exc_sub_l.append( Msun2kg * 4 * np.pi *r_surface**2 *0.5/ n_sample * np.sum( rho_surface[inflow_exc_sub] * (vscalar_surface[inflow_exc_sub]*v2kms*1e3)**2)) # kg kpc^-1 m^2 s^-2 = J kpc^-1
        dEkdr_outflow_exc_sub_l.append( Msun2kg * 4 * np.pi *r_surface**2 *0.5/ n_sample * np.sum( rho_surface[outflow_exc_sub] * (vscalar_surface[outflow_exc_sub]*v2kms*1e3)**2)) # kg kpc^-1 m^2 s^-2 = J kpc^-1

        # Only run for the first element in the density list in map mode
        if return_map:
            break

    for Zrho_surface in Zrho_surface_list:

        # Don't write metal maps at the moment
        if return_map:
            break

        dmdt_inflow = -4 * np.pi / n_sample * np.sum( cv_radial_surface[inflow] * v2kms*1e3 * Zrho_surface[inflow] * r_surface**2) # Msun kpc^-1 ms^-1 
        dmdt_inflow *= Gyr / kpc # Msun Gyr^-1
        dmdt_inflow_Z_l.append(dmdt_inflow)

        dmdt_outflow = 4 * np.pi / n_sample * np.sum( cv_radial_surface[outflow] * v2kms*1e3 * Zrho_surface[outflow] * r_surface**2) # Msun kpc^-1 ms^-1 
        dmdt_outflow *= Gyr / kpc
        dmdt_outflow_Z_l.append(dmdt_outflow)

        dmdt_inflow_exc_sub = -4 * np.pi / n_sample * np.sum( cv_radial_surface[inflow_exc_sub] * v2kms*1e3 * Zrho_surface[inflow_exc_sub] * r_surface**2)
        dmdt_inflow_exc_sub *= Gyr / kpc # Msun Gyr^-1
        dmdt_inflow_exc_sub_Z_l.append(dmdt_inflow_exc_sub)

        dmdt_outflow_exc_sub = 4 * np.pi / n_sample * np.sum( cv_radial_surface[outflow_exc_sub] * v2kms*1e3 * Zrho_surface[outflow_exc_sub] * r_surface**2)
        dmdt_outflow_exc_sub *= Gyr / kpc # Msun Gyr^-1
        dmdt_outflow_exc_sub_Z_l.append(dmdt_outflow_exc_sub)

        # Compute differential mass of the surface, dm/dr, in inflowing and outflowing gas
        dmdr_inflow_Z_l.append(4 * np.pi * r_surface**2 / n_sample * np.sum( Zrho_surface[inflow] )) # msun kpc^-1
        dmdr_outflow_Z_l.append( 4 * np.pi * r_surface**2 / n_sample * np.sum( Zrho_surface[outflow] )) # msun kpc^-1

        dmdr_inflow_exc_sub_Z_l.append( 4 * np.pi * r_surface**2 / n_sample * np.sum( Zrho_surface[inflow_exc_sub] )) # msun kpc^-1
        dmdr_outflow_exc_sub_Z_l.append( 4 * np.pi * r_surface**2 / n_sample * np.sum( Zrho_surface[outflow_exc_sub] )) # msun kpc^-1


    if not return_map:

        return dmdt_inflow_l, dmdt_outflow_l, dmdt_inflow_exc_sub_l, dmdt_outflow_exc_sub_l, \
            dmdr_inflow_l, dmdr_outflow_l, dmdr_inflow_exc_sub_l, dmdr_outflow_exc_sub_l, \
            cv_radial_mw_inflow_l, cv_radial_mw_outflow_l, cv_radial_mw_inflow_exc_sub_l, cv_radial_mw_outflow_exc_sub_l, \
            cv_radial_fw_inflow_l, cv_radial_fw_outflow_l, cv_radial_fw_inflow_exc_sub_l, cv_radial_fw_outflow_exc_sub_l, \
            dUdr_inflow_l, dUdr_outflow_l, dUdr_inflow_exc_sub_l, dUdr_outflow_exc_sub_l, \
            dEkrdr_inflow_l, dEkrdr_outflow_l, dEkrdr_inflow_exc_sub_l, dEkrdr_outflow_exc_sub_l, \
            dEkdr_inflow_l, dEkdr_outflow_l, dEkdr_inflow_exc_sub_l, dEkdr_outflow_exc_sub_l, \
            dmdt_inflow_Z_l, dmdt_outflow_Z_l, dmdt_inflow_exc_sub_Z_l, dmdt_outflow_exc_sub_Z_l, \
            dmdr_inflow_Z_l, dmdr_outflow_Z_l, dmdr_inflow_exc_sub_Z_l, dmdr_outflow_exc_sub_Z_l

    # Note the map mode only outputs for the first density element in rho_snap_list
    else:
        cv_radial_surface_inflow = np.copy(cv_radial_surface); cv_radial_surface_inflow[cv_radial_surface>0.0] = 0.0
        cv_radial_surface_outflow = np.copy(cv_radial_surface); cv_radial_surface_outflow[cv_radial_surface<0.0] = 0.0
        
        rho_surface_inflow = np.copy(rho_surface); rho_surface_inflow[outflow] = 0.0
        rho_surface_outflow = np.copy(rho_surface); rho_surface_outflow[inflow] = 0.0
        
        temp_surface_inflow = np.copy(temp_surface); temp_surface_inflow[outflow] = 0.0 # Might be better to set to NaN
        temp_surface_outflow = np.copy(temp_surface); temp_surface_outflow[inflow] = 0.0

        cv_radial_surface_inflow = np.copy(cv_radial_surface); cv_radial_surface_inflow[outflow] = 0.0
        cv_radial_surface_outflow = np.copy(cv_radial_surface); cv_radial_surface_outflow[inflow] = 0.0

        inflow_only_sub = inflow & (in_sub_surface==True)
        in_sub_inflow_map = np.zeros_like(cv_radial_surface); in_sub_inflow_map[inflow_exc_sub] = 1; in_sub_inflow_map[inflow_only_sub] = 2
        
        outflow_only_sub = outflow & (in_sub_surface==True)
        in_sub_outflow_map = np.zeros_like(cv_radial_surface); in_sub_outflow_map[outflow_exc_sub] = 1; in_sub_outflow_map[outflow_only_sub] = 2

        dmdt_inflow_map = -4 * np.pi / n_sample * cv_radial_surface_inflow * v2kms*1e3 * rho_surface * r_surface**2 # Msun kpc^-1 ms^-1 
        dmdt_inflow_map *= Gyr / kpc # Msun Gyr^-1
        
        dmdt_outflow_map = 4 * np.pi / n_sample * cv_radial_surface_outflow * v2kms*1e3 * rho_surface * r_surface**2 # Msun kpc^-1 ms^-1 
        dmdt_outflow_map *= Gyr / kpc

        return dmdt_inflow_map, dmdt_outflow_map, rho_surface_inflow, rho_surface_outflow, \
            cv_radial_surface_inflow*v2kms, cv_radial_surface_outflow*v2kms, temp_surface_inflow, temp_surface_outflow, \
            in_sub_inflow_map, in_sub_outflow_map, costheta, phi


def Match_Cell_Particle(csize, cx, cy, cz, tx, ty, tz):

    txyz = np.swapaxes(np.array([tx,ty,tz]),0,1)
    cxyz = np.swapaxes(np.array([cx, cy, cz]),0,1) # Cell coordinates for possible matches
    
    #print "temp hack"
    #txyz = txyz[np.arange(0,len(txyz),10)]
    #cxyz = cxyz[np.arange(0,len(cxyz),50)]
    #csize = csize[np.arange(0,len(csize),50)]

    print "building kdtree"
    tree = spatial.cKDTree(cxyz,leafsize=64) # Build the KD Tree

    print "done, searching kdtree"
    dist,ind = tree.query(txyz,k=1,p=np.inf) # Find n_neighbour nearest neighbouring points for each cell
    print "done"

    ok = dist < csize[ind]
    
    return ind, ok
