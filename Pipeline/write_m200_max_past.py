import time
t1 = time.time()

import argparse
import sys

parser = argparse.ArgumentParser()
parser.add_argument("-zoom", help = "name of the zoom simulation")
parser.add_argument("-Run", help = "name of the specific run for this zoom simulation")
parser.add_argument("-rd","--RunDir", help="path to simulation directory containing all these output_xxxxx dirs.")
parser.add_argument("-hd","--HaloDir", help="path to directory containing runs of HaloMaker (i.e. containing xx/tree_bricks...)")
parser.add_argument("-td","--TreeDir", help="path to directory containing runs of TreeMaker")
parser.add_argument("-od","--OutDir", help="path to output directory for hdf5 files")
parser.add_argument("-ts_final","--timestep_final", type=int,help="final timestep")
parser.add_argument("-ts_out","--timesteps_out",help="output timestep(s), 'all, 'final', or e.g. '2'")
parser.add_argument("-rhd","--RHD",help="Radiation hydrodynamics run")
parser.add_argument("-v","--verbose",help="verbose...")

if len(sys.argv)!=21:
    parser.print_help()
    sys.exit(1)
args = parser.parse_args()

# Set paths
Zoom = args.zoom
Run = args.Run
RunDir = args.RunDir
HaloDir = args.HaloDir
TreeDir = args.TreeDir
OutDir = args.OutDir
timestep_final = args.timestep_final # Timestep for reading in halo brick files and ramses raw output
timesteps_out = args.timesteps_out
RHD = args.RHD
verbose_str = args.verbose

timestep_tree_final = timestep_final -1

if "True" in verbose_str:
    verbose = True
else:
    verbose = False


if verbose:
    print 'imports ... '
from halotils import py_halo_utils as hutils
import numpy as np
import pandas as pd
import h5py
import os 
import utilities_ramses as ur
import match_searchsorted as ms
import dm_utils as dm
from merger_trees import *

# Read cosmological parameters and build sim dictionary to be fed into Forest class later on
omega_0,lambda_0,little_h = dm.jeje_utils.read_cosmo_params(RunDir,timestep_final)
r2kpc_ideal,redshift, junk, junk, junk, junk = ur.Set_Units(RunDir, timestep_final, "ideal") # Used to compute "ideal" box size. Used in turn to convert halo positions into box units
boxlen_cMpc = dm.jeje_utils.get_param_real(RunDir,timestep_final,'boxlen') # Box size in box units
boxlen_cMpc *= r2kpc_ideal
boxlen_cMpc = boxlen_cMpc * (1+redshift) /1e3 # cMpc (comoving)
boxlen_cMpc = boxlen_cMpc * little_h # h^-1 cMpc
sim = sim={'Lbox': boxlen_cMpc, 'h': little_h, 'Om': omega_0, 'Ol': lambda_0}

# Read processed merger trees data                                                                                                                                                                                                    
trees = Forest(TreeDir, sim)

# Open the output hdf5 file
if timesteps_out == "all":
    filename = Zoom + "_" + Run + "_tsf" + str(timestep_final) +".hdf5"
elif timesteps_out == "final":
    filename = Zoom + "_" + Run + "_tsf" + str(timestep_final) +"_ts"+str(timestep_final)+".hdf5"
else:
    try:
        timestep_out = int(timesteps_out)
        filename = Zoom + "_" + Run + "_tsf" + str(timestep_final) +"_ts"+str(timestep_out)+".hdf5"
    except:
        print "timesteps_out",timesteps_out,",is not valid, sorry!"
        quit()
File = h5py.File(OutDir+filename)
zoom_group = File[Zoom]

m200_max_past_ap = []
m200_max_past_mp = []
r200_max_past_ap = []
r200_max_past_mp = []


for halo_group_str in zoom_group:
    if not "Halo_" in halo_group_str:
            continue

    ap_group = zoom_group[halo_group_str+"/all_progenitors"]
    mp_group = zoom_group[halo_group_str+"/main_progenitors"]

    m200_max_past_ap.append(np.zeros_like(ap_group["mvir"]))
    m200_max_past_mp.append(np.zeros_like(mp_group["mvir"]))
    r200_max_past_ap.append(np.zeros_like(ap_group["mvir"]))
    r200_max_past_mp.append(np.zeros_like(mp_group["mvir"]))


# Loop over final timestep haloes
ind_halo = -1
for halo_group_str in zoom_group:

    # Check this is a halo
    if not "Halo_" in halo_group_str:
        continue
        
    ind_halo += 1 # Index like this instead of enumerate so that skipped parts of the for loop don't contribute to the index

    ap_group = zoom_group[halo_group_str+"/all_progenitors"]
    mp_group = zoom_group[halo_group_str+"/main_progenitors"]

    # Check if m200 already written
    ok_m200 = False
    for dset in ap_group:
        if dset == "m200":
            ok_m200 = True
    if not ok_m200:
        print "Error: write_m200_max_past.py: need to run write_m200.py first"
        quit()

    # Compute maximum past m200 (and corresponding r200) of the main progenitors
    m200_all_progens = ap_group["m200"][:]
    r200_all_progens = ap_group["r200"][:]

    halo_id_all_progens = ap_group["halo_id"][:]

    for i in range(len(mp_group["halo_id"])):
        halo_id_tp = mp_group["halo_id"][:][i] # halo number of this main progenitor

        # Skip if this halo is central
        if mp_group["halo_id"][:][i] == mp_group["host_halo_id"][:][i]:
            m200_max_past_mp[ind_halo][i] = mp_group["m200"][:][i]
            r200_max_past_mp[ind_halo][i] = mp_group["r200"][:][i]
            continue
                                                   
        ts_tp = 1 + mp_group["halo_ts"][:][i] # Timestep of this main progenitor (+1 is because of ramses/halomaker timestep mismatch)                                                                                 
        all_progenitors_tp = trees.get_all_progenitors(None,None,target_id=halo_id_tp) # all progenitors of this main progenitor

        halo_id_progens = np.array(all_progenitors_tp["halo_id"])

        ptr = ms.match(halo_id_progens, halo_id_all_progens)
        
        ok_match = ptr >= 0

        arg_m200_max = np.argmax(m200_all_progens[ptr][ok_match])

        m200_max_past_mp[ind_halo][i] = m200_all_progens[ptr][ok_match][arg_m200_max]
        r200_max_past_mp[ind_halo][i] = r200_all_progens[ptr][ok_match][arg_m200_max]

    for i in range(len(ap_group["mvir"])):
        # Just copy m200/r200 for central haloes
        if ap_group["halo_id"][i] == ap_group["host_halo_id"][i]:
            m200_max_past_ap[ind_halo][i] = ap_group["m200"][:][i]
            r200_max_past_ap[ind_halo][i] = ap_group["r200"][:][i]
            continue
        
        halo_id_tp = ap_group["halo_id"][:][i] # halo number of this progenitor
        all_progenitors_tp = trees.get_all_progenitors(None,None,target_id=halo_id_tp) # all progenitors of this progenitor

        halo_id_progens = np.array(all_progenitors_tp["halo_id"])

        ptr = ms.match(halo_id_progens, halo_id_all_progens)
        
        ok_match = ptr >= 0

        arg_m200_max = np.argmax(m200_all_progens[ptr][ok_match])

        m200_max_past_ap[ind_halo][i] = m200_all_progens[ptr][ok_match][arg_m200_max]
        r200_max_past_ap[ind_halo][i] = r200_all_progens[ptr][ok_match][arg_m200_max]


    if "m200_max_past" in ap_group:
        del ap_group["m200_max_past"]
        del ap_group["r200_max_past"]
        del mp_group["m200_max_past"]
        del mp_group["r200_max_past"]

    ap_group.create_dataset("m200_max_past",data=m200_max_past_ap[ind_halo])
    ap_group["m200_max_past"].attrs['description'] = "m200 for central halos, maximum past value of m200 for subhaloes, units: Msun"

    ap_group.create_dataset("r200_max_past",data=r200_max_past_ap[ind_halo])
    ap_group["r200_max_past"].attrs['description'] = "r200 (radius enclosing 200 times the critical density) for central haloes, r200 corresponding to maximum past m200 for subhaloes, units: kpc"
            

    mp_group.create_dataset("m200_max_past",data=m200_max_past_mp[ind_halo])
    mp_group["m200_max_past"].attrs['description'] = "m200 (mass enclosed within a region that has mean density = 200 x the ciritical density) for central halos, maximum past value of m200 for satellite haloes, units: Msun"

    mp_group.create_dataset("r200_max_past",data=r200_max_past_mp[ind_halo])
    mp_group["r200_max_past"].attrs['description'] = "r200 (radius enclosing 200 times the critical density) for central halos, r200 corresponding to maximum past value of m200 for satellite haloes, units: kpc"


File.close()

print "time taken was", time.time()-t1
