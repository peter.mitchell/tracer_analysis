import dm_utils as dm

def Set_Units(RunDir,timestep,box_size_units="actual"):
    # get conversion factors for this timestep
    dm.jeje_utils.read_conversion_scales(RunDir,timestep)
    # Decide whether to work in correct "actual" size units (which suffers from hard coded internal ramses Mpc units)
    # or in the idealised "ideal" units (where box size will e.g. exactly = 20 h^-1 Mpc)
    # box_size_units = "ideal" or "actual"
    if box_size_units == "actual":
        r2kpc = dm.jeje_utils.dp_scale_l * dm.jeje_utils.cm2pc / 1000. # converts comoving code units to phys. kpc
        rho2msunpkpc3 = dm.jeje_utils.dp_scale_d * dm.jeje_utils.g2msun / (dm.jeje_utils.cm2pc / 1000.)**3
    elif box_size_units == "ideal":
        r2kpc = dm.jeje_utils.dp_scale_l * 1./3.08e21 # converts comoving code units to phys. kpc
        rho2msunpkpc3 = dm.jeje_utils.dp_scale_d * dm.jeje_utils.g2msun * (3.08e21)**3
    else:
        print 'box_size_units =", box_size_units, " not recognised. Valid options are "ideal" and "actual"'
        quit()

    redshift  = dm.jeje_utils.get_snap_redshift(RunDir,timestep)

    mass2Msun = dm.jeje_utils.dp_scale_d * dm.jeje_utils.dp_scale_l**3 * dm.jeje_utils.g2msun

    omega_0,lambda_0,little_h = dm.jeje_utils.read_cosmo_params(RunDir,timestep)

    v2kms = dm.jeje_utils.dp_scale_l / dm.jeje_utils.dp_scale_t / 10.**5

    return r2kpc, redshift, little_h, mass2Msun, rho2msunpkpc3, v2kms
