import argparse
import sys

parser = argparse.ArgumentParser()
parser.add_argument("-zoom", help = "name of the zoom simulation")
parser.add_argument("-Run", help = "name of the specific run for this zoom simulation")
parser.add_argument("-rd","--RunDir", help="path to simulation directory containing all these output_xxxxx dirs.")
parser.add_argument("-hd","--HaloDir", help="path to directory containing runs of HaloMaker (i.e. containing xx/tree_bricks...)")
parser.add_argument("-td","--TreeDir", help="path to directory containing runs of TreeMaker")
parser.add_argument("-od","--OutDir", help="path to output directory for hdf5 files")
parser.add_argument("-ts_final","--timestep_final", type=int,help="final timestep")
parser.add_argument("-ts_out","--timesteps_out",help="output timestep(s), 'all, 'final', or e.g. '2'")
parser.add_argument("-rhd","--RHD",help="Radiation hydrodynamics run")
parser.add_argument("-v","--verbose",help="verbose...")

if len(sys.argv)!=21:
    parser.print_help()
    sys.exit(1)
args = parser.parse_args()

# Set paths
Zoom = args.zoom
Run = args.Run
RunDir = args.RunDir
HaloDir = args.HaloDir
TreeDir = args.TreeDir
OutDir = args.OutDir
timestep_final = args.timestep_final # Timestep for reading in halo brick files and ramses raw output
timesteps_out = args.timesteps_out
RHD = args.RHD
verbose_str = args.verbose

if "True" in verbose_str:
    verbose = True
else:
    verbose = False

if "True" in RHD:
    RHD = True
else:
    RHD = False
    
# Write a dump of the stellar, DM and leaf cell data for a given radius around a target halo
import os
import pandas as pd
from halotils import py_halo_utils as hutils
import dm_utils as dm
import star_utils as star
import h5py
import cutils as cut
import numpy as np
import utilities_ramses as ur
import tracer_utils as tr

if timesteps_out == "all":
    timesteps_write = np.arange(2,timestep_final+1)
elif timesteps_out == "final":
    timesteps_write = [timestep_final]
else:
    try:
        timestep_out = int(timesteps_out)
        timesteps_write = [timestep_out]
    except:
        print "timesteps_out",timesteps_out,",is not valid, sorry!"
        quit()

# Open the input tree hdf5 file and the output hdf5 file
if timesteps_out == "all":
    tree_filename = Zoom + "_" + Run + "_tsf" + str(timestep_final) +".hdf5"
elif timesteps_out == "final":
    tree_filename = Zoom + "_" + Run + "_tsf" + str(timestep_final) +"_ts"+str(timestep_final)+".hdf5"
else:
    try:
        timestep_out = int(timesteps_out)
        tree_filename = Zoom + "_" + Run + "_tsf" + str(timestep_final) +"_ts"+str(timestep_out)+".hdf5"
    except:
        print "timesteps_out",timesteps_out,",is not valid, sorry!"
        quit()

input_file = h5py.File(OutDir+tree_filename)

zoom_group = input_file[Zoom]
output_filename = "subvol_" + tree_filename
# Delete existing hdf5 file if one exists before writing a new one
if os.path.isfile(OutDir+output_filename):
    os.remove(OutDir+output_filename)
    print "deleted previous hdf5 file at ", OutDir+output_filename
output_file = h5py.File(OutDir+output_filename)

# Flag for whether conformal_time.f90 has been intialised yet
ct_init = False

########### Loop over desired timesteps ###################
for i_timestep, timestep in enumerate(timesteps_write):
    if verbose:
        print "timestep", i_timestep, "of", len(timesteps_write)

    # Find the position of the main progenitor of the target halo at this timestep
    for halo_group_str in zoom_group:
        if not "_main" in halo_group_str:
            continue

        halo_group = zoom_group[halo_group_str]
        mp_group = halo_group["main_progenitors"]

        timestep_tree = timestep -1
        ok_ts = timestep_tree == mp_group["halo_ts"][:]

        x_main = mp_group["x"][:][ok_ts] #cMpc
        y_main = mp_group["y"][:][ok_ts]
        z_main = mp_group["z"][:][ok_ts]

    # Get maximum radius (relative to the main progenitor of the target halo) within which to read cells/particles at this timestep
    rmax = zoom_group["rmax"][timestep-1] # cMpc (as halofinder units)

        
    # convert positions/distances to box units
    # Factor to help convert into box units (note maxime's merger_tree reader has converted halo positions so be careful here!)
    r2kpc_ideal, redshift, little_h, m2Msun,junk,junk = ur.Set_Units(RunDir, timestep, "ideal")
    boxlen_cMpc = dm.jeje_utils.get_param_real(RunDir,timestep,'boxlen') # Box size in box units                                                        
    boxlen_cMpc *= r2kpc_ideal
    boxlen_cMpc = boxlen_cMpc * (1+redshift) /1e3 # cMpc (comoving)
    boxlen_cMpc = boxlen_cMpc * little_h # h^-1 cMpc
    factor = 1./boxlen_cMpc
    
    x_main = x_main * factor + 0.5  ## +0.5 assumes boxlen is one ...
    y_main = y_main * factor + 0.5  ## +0.5 assumes boxlen is one ...
    z_main = z_main * factor + 0.5  ## +0.5 assumes boxlen is one ...
    rmax *= factor

    ######## Read stellar particles ################
    nst = star.star_utils.get_tot_nstars(RunDir,timestep)
    
    if nst > 0:
        # On the first read, we need to call ct_init_cosm (from conformal_time.f90) when we call read_stars_all_props (from star_utils.f90)
        if ct_init:
            sm,sx,sy,sz,sid,sage,svx,svy,svz,smets = tr.tracer_utils.read_stars(RunDir,timestep,nst,False,RHD)
        else:
            sm,sx,sy,sz,sid,sage,svx,svy,svz,smets = tr.tracer_utils.read_stars(RunDir,timestep,nst,True,RHD)
            ct_init = True

        # Compute the distance between each star particle and the central halo
        sr = np.sqrt( np.square(sx-float(x_main)) + np.square(sy-float(y_main)) + np.square(sz-float(z_main)))

        # Define the stellar particles that are within rmax of the central halos
        ok = sr < rmax
        sm = sm[ok]; sx = sx[ok]; sy = sy[ok]; sz = sz[ok]
        sid = sid[ok]; sage = sage[ok]; smets = smets[ok]
        svx = svx[ok]; svy = svy[ok]; svz = svz[ok]
    else:
        sm = []; sx = []; sy = []; sz = []
        sid = []; sage = []; smets = []
        svx = []; svy = []; svz = []

    ############### Read dark matter particles ###############################
    npart = tr.tracer_utils.get_ndm_in_sphere(RunDir,timestep,x_main,y_main,z_main,rmax)
    x_dm,y_dm,z_dm,m_dm,id_dm = tr.tracer_utils.read_dm_in_sphere(RunDir,timestep,x_main,y_main,z_main,rmax,npart)

    ############### Read tracers ###############################
    ntr = tr.tracer_utils.get_ntracer_in_sphere(RunDir,timestep,x_main,y_main,z_main,rmax)
    x_tr,y_tr,z_tr,id_tr,tmass,fam_tr = tr.tracer_utils.read_tracers_in_sphere(RunDir,timestep,x_main,y_main,z_main,rmax,ntr)
    
    ################ Read leaf cells #########################################
    # Define the cube within which to read leaf cells
    xmin = float(x_main) - rmax
    xmax = float(x_main) + rmax
    ymin = float(y_main) - rmax
    ymax = float(y_main) + rmax
    zmin = float(z_main) - rmax
    zmax = float(z_main) + rmax

    if verbose:
        print 'reading cell numbers'
    nleaf = cut.cell_utils.get_nleaf_in_cube(RunDir,timestep,xmin,xmax,ymin,ymax,zmin,zmax)
    if verbose:
        print "reading cell data"
    # assumes imets = 6

    if RHD:
        rho_snap,pre_snap,cx,cy,cz,lmjtodx,lmjt_snap,lev,cvx,cvy,cvz,cmet,xH1,xHe1,xHe2 = cut.cell_utils.read_leaf_cells_in_cube_rt(RunDir,timestep,xmin,xmax,ymin,ymax,zmin,zmax,nleaf,6,7,8,9)
    else:
        rho_snap,pre_snap,cx,cy,cz,lmjtodx,lmjt_snap,lev,cvx,cvy,cvz,cmet = cut.cell_utils.read_leaf_cells_in_cube_with_metals(RunDir,timestep,xmin,xmax,ymin,ymax,zmin,zmax,nleaf,6)
        
    if verbose:
        print "done"

    # Select cells within a sphere
    cr = np.sqrt( np.square(cx-float(x_main)) + np.square(cy-float(y_main)) + np.square(cz-float(z_main)))
    ok = cr < rmax
    rho_snap = rho_snap[ok]; pre_snap = pre_snap[ok]; lmjtodx=lmjtodx[ok]; lmjt_snap=lmjt_snap[ok]; lev=lev[ok]
    cx=cx[ok]; cy=cy[ok]; cz=cz[ok];  cvx=cvx[ok]; cvy=cvy[ok]; cvz=cvz[ok]; cmet = cmet[ok]

    if RHD:
        xH1 = xH1[ok]; xHe1 = xHe1[ok]; xHe2 = xHe2[ok]

    
    ############### Write data to disk #############################################

    timestep_group = output_file.create_group("timestep_"+str(int(timestep)))

    star_group = timestep_group.create_group("Stellar_particle_data")
    star_group.create_dataset("x_s", data=sx)
    star_group.create_dataset("y_s",data=sy)
    star_group.create_dataset("z_s",data=sz)
    star_group.create_dataset("m_s",data=sm)
    star_group.create_dataset("met_s",data=smets)
    star_group.create_dataset("age_s",data=sage)
    star_group.create_dataset("id_s",data=sid)
    star_group.create_dataset("vx_s",data=svx)
    star_group.create_dataset("vy_s",data=svy)
    star_group.create_dataset("vz_s",data=svz)

    dm_group = timestep_group.create_group("Dark_matter_particle_data")
    dm_group.create_dataset("x_dm",data=x_dm)
    dm_group.create_dataset("y_dm",data=y_dm)
    dm_group.create_dataset("z_dm",data=z_dm)
    dm_group.create_dataset("m_dm",data=m_dm)
    dm_group.create_dataset("id_dm",data=id_dm)

    tr_group = timestep_group.create_group("Tracer_particle_data")
    tr_group.create_dataset("x_tr",data=x_tr)
    tr_group.create_dataset("y_tr",data=y_tr)
    tr_group.create_dataset("z_tr",data=z_tr)
    tr_group.create_dataset("fam_tr",data=fam_tr)
    tr_group.create_dataset("id_tr",data=id_tr)
    tr_group.create_dataset("mass_tr",data=tmass)
    
    cell_group = timestep_group.create_group("Gas_leaf_cell_data")
    cell_group.create_dataset("x_c",data=cx)
    cell_group.create_dataset("y_c",data=cy)
    cell_group.create_dataset("z_c",data=cz)
    cell_group.create_dataset("vx_c",data=cvx)
    cell_group.create_dataset("vy_c",data=cvy)
    cell_group.create_dataset("vz_c",data=cvz)
    cell_group.create_dataset("rho_c",data=rho_snap)
    cell_group.create_dataset("pre_c",data=pre_snap)
    cell_group.create_dataset("lmjt",data=lmjt_snap)
    cell_group.create_dataset("lev",data=lev)
    cell_group.create_dataset("met_c",data=cmet)

    if RHD:
        cell_group.create_dataset("xH1",data=xH1)
        cell_group.create_dataset("xHe1",data=xHe1)
        cell_group.create_dataset("xHe2",data=xHe2)

output_file.close
