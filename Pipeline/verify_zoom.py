import argparse
import sys
import h5py
import os
import numpy as np

parser = argparse.ArgumentParser()
parser.add_argument("-zoom", help = "name of the zoom simulation")
parser.add_argument("-Run", help = "name of the specific run for this zoom simulation")
parser.add_argument("-rd","--RunDir", help="path to simulation directory containing all these output_xxxxx dirs.")
parser.add_argument("-hd","--HaloDir", help="path to directory containing runs of HaloMaker (i.e. containing xx/tree_bricks...)")
parser.add_argument("-td","--TreeDir", help="path to directory containing runs of TreeMaker")
parser.add_argument("-od","--OutDir", help="path to output directory for hdf5 files")
parser.add_argument("-ts_final","--timestep_final", type=int,help="final timestep")
parser.add_argument("-ts_out","--timesteps_out",help="output timestep(s), 'all, 'final', or e.g. '2'")
parser.add_argument("-v","--verbose",help="verbose...")

if len(sys.argv)!=19:
    parser.print_help()
    sys.exit(1)
args = parser.parse_args()

# Set paths
Zoom = args.zoom
Run = args.Run
RunDir = args.RunDir
HaloDir = args.HaloDir
TreeDir = args.TreeDir
OutDir = args.OutDir
timestep_final = args.timestep_final # Timestep for reading in halo brick files and ramses raw output
timesteps_out = args.timesteps_out
verbose_str = args.verbose

print "verifying ", Zoom

if "True" in verbose_str:
    verbose = True
else:
    verbose = False

MapDir = OutDir + "Maps/"+Zoom+"_"+Run+"/"

sfr_aperture = 0.2
sfr_ap_str = "sfh_r" + str(sfr_aperture).replace(".","p")

flux_aperture_list = [0.25]

# Read the Zoom hdf5 file
if timesteps_out == "all":
    filename = Zoom + "_" + Run + "_tsf" + str(timestep_final) +".hdf5"
elif timesteps_out == "final":
    filename = Zoom + "_" + Run + "_tsf" + str(timestep_final) +"_ts"+str(timestep_final)+".hdf5"
else:
    try:
        timestep_out = int(timesteps_out)
        filename = Zoom + "_" + Run + "_tsf" + str(timestep_final) +"_ts"+str(timestep_out)+".hdf5"
    except:
        print "timesteps_out",timesteps_out,",is not valid, sorry!"
        quit()
if not os.path.isfile(OutDir+filename):
    print "verify_zoom.py: The main Zoom file does not exist at", OutDir+filename
    print "rerun write_merger_tree.py"
    quit()


File = h5py.File(OutDir+filename)
if not Zoom in File:
    print "verify_zoom.py: The main Zoom file was not written correctly at", OutDir+filename
    print "rerun write_merger_tree.py"
    quit()

zoom_group = File[Zoom]

# Read the subvolume hdf5 file
subvol_filename = "subvol_"+filename

if not os.path.isfile(OutDir+subvol_filename):
    print "verify_zoom.py: The subvol hdf5 file does not exist at", OutDir+subvol_filename
    print "rerun write_hdf5_subvol.py"
    quit()

try:
    subvol_File = h5py.File(OutDir+subvol_filename)
except:
    print "verify_zoom.py: The subvol hdf5 was not written correctly for", OutDir+subvol_filename
    print "rerun write_hdf5_subvol.py"
    quit()

# Verify contents of subvolume file
ts_group = subvol_File["timestep_"+str(timestep_final)]
cell_group = ts_group["Gas_leaf_cell_data"]
rho_snap = cell_group["rho_c"][:] # Gas density
if len(rho_snap) == 0:
    print "verify_zoom.py: The contents of the subvol hdf5 file are wrong at", OutDir+subvol_filename
    print "rerun write_hdf5_subvol.py"
    print ""

if not "met_c" in cell_group:
    print "verify_zoom.py: The subvol hdf5 files doesn't contain cell metallicities for", OutDir+subvol_filename
    print "rerun write_hdf5_subvol.py"
    print ""

star_group = ts_group["Stellar_particle_data"]
lum_str = "lum_V_s"
lum_str2 = "lum_UA_s"
if not lum_str in star_group or not lum_str2 in star_group:
    print "verify_zoom.py: The subvol hdf5 file doesn't contain particle luminosities for", OutDir+subvol_filename
    print "rerun write_luminosities.py"
    print ""

if not "component_index" in cell_group:
    print "verify_zoom.py: The subvol hdf5 files doesn't contain compartmentalization information for", OutDir+subvol_filename
    print "rerun write_tidal_radius.py"
    print ""



subvol_File.close()

# Verify the contents of the main Zoom hdf5 analysis file
for halo_group_str in zoom_group:
    # Only read the main halo
    if not "main" in halo_group_str:
        continue

    
    # Read stellar masses and star formation rates
    halo_group = zoom_group[halo_group_str]
    ap_group = halo_group["all_progenitors"]
    mp_group = halo_group["main_progenitors"]

    
    # Check halo masses
    hm_ok = False
    for dataset in ap_group:
        if "m200" in dataset:
            hm_ok = True
    if not hm_ok:
        print "verify_zoom.py: Halo masses were not written correctly for", OutDir+subvol_filename
        print "rerun write_m200.py"
        print ""

    # Check max past subhalo masses
    hm_ok = False
    for dataset in ap_group:
        if "m200_max_past" in dataset:
            hm_ok = True
    if not hm_ok:
        print "verify_zoom.py: Maximum past subhalo masses were not written correctly for", OutDir+subvol_filename
        print "rerun write_m200_max_past.py"
        print ""

    # Read sizes
    size_ok = False
    for dataset in mp_group:
        if "rhalf_mw_r" in dataset:
            size_ok = True
    if not size_ok:
        print "verify_zoom.py: Galaxy sizes were not written correctly for", OutDir+subvol_filename
        print "rerun write_sizes.py"
        print ""

    # Check luminosities
    lum_ok = False
    for dataset in mp_group:
        if "lum_r0p2_V" in dataset:
            lum_ok = True
    if not lum_ok:
        print "verify_zoom.py: Integrated luminosities were not written correctly for", OutDir+subvol_filename
        print "rerun write_integrated_magnitudes.py"
        print ""

    
    sm_ok = False
    sfr_of = False
    for dataset in mp_group:
        if "sm_" in dataset:
            sm_ok = True
        if "sfr_" in dataset:
            sfr_ok = True
    if not sm_ok or not sfr_ok:
        print "verify_zoom.py: Stellar masses or star formation rates were not written correctly for", OutDir+subvol_filename
        print "rerun write_stellar_mass_sfr.py"
        print ""


    

    # Read gas masses
    mgas_ok = False
    for dataset in ap_group:
        if "mgas_tot_exc_sub" == dataset:
            mgas_ok = True
    if not mgas_ok:
        print "verify_zoom.py: Gas masses were not written correctly for", OutDir+subvol_filename
        print "rerun write_gas_tree_exc_sub.py"
        print ""
    # Check star formation histories
    sfh_ok = False
    for group in halo_group:
        if "sfh" in group:
            sfh_ok = True
    if not sfh_ok:
        print "verify_zoom.py: Star formation histories not written correctly for", OutDir+subvol_filename
        print "rerun write_sfh.py"
        print ""

    # Check fluxes
    fluxes_ok = False
    flux_hist_ok = False
    fluxes_recent = False
    for group in halo_group:
        if "fluxes" in group:
            fluxes_ok = True
            if timesteps_out == "all":
                check_group = halo_group[group]["timestep_3/radial_profiles"]
            elif timesteps_out == "final":
                check_group = halo_group[group]["timestep_"+str(timestep_final)+"/radial_profiles"]
            else:
                check_group = halo_group[group]["timestep_"+int(timesteps_out)+"/radial_profiles"]
            for thing in check_group:
                if "dUdr" in thing:
                    fluxes_recent = True
    

    if not fluxes_ok:
        print "verify_zoom.py: Fluxes not written correctly for", OutDir+subvol_filename
        print "rerun write_flux_tree.py and write_flux_history.py"
        print ""

    elif not fluxes_recent:
        print "verify_zoom.py: Fluxes not recent to the point of including Kinetic/thermal energy", OutDir+subvol_filename
        print "rerun write_flux_tree.py and write_flux_history.py"
        print ""

    else:

        # Update, removed flux maps from zoom file for now because of suspected corruption problems
        # Check for density maps
        #if not "density_inflow_map" in halo_group["fluxes/timestep_"+str(timestep_final)+"/flux_maps/r_surface_1p0"]:
        #    print "verify_zoom.py: Latest flux map information not written for", OutDir+subvol_filename
        #   print "rerun write_flux_tree.py"
        #    print ""


        if not "flux_history" in halo_group["fluxes"]:
            print "verify_zoom.py: Flux histories not written correctly for", OutDir+subvol_filename
            print "rerun write_flux_history.py"
            print ""
            
        # Check for shell fluxes
        elif not "r_shells" in halo_group["fluxes/flux_history"]:
            print "verify_zoom.py: Shell fluxes not written correctly for", OutDir+subvol_filename
            print "rerun write_flux_tree.py and write_flux_history.py"
            print ""

        # verify fluxes make sense
        else:
            flux_group = halo_group["fluxes/flux_history"]
            check_radius = 1.0
            check_radius_str = str(check_radius).replace(".","p")
            check_radius_str_shell = "r_shell_"+check_radius_str+ "/dmdt_inflow"
            check_radius_str_surface = "r_surface_"+check_radius_str+ "/dmdt_inflow"
            if check_radius in flux_group["r_surfaces"] and check_radius in flux_group["r_shells"]:
                dmdt_med_shell   = np.median(flux_group[check_radius_str_shell][:])
                dmdt_med_surface = np.median(flux_group[check_radius_str_surface][:])

                if abs(np.log10(dmdt_med_shell/dmdt_med_surface)) > 0.2:
                    print "verify_zoom.py: Surface fluxes don't look consistent with shell fluxes for", OutDir+subvol_filename
                    print dmdt_med_shell, dmdt_med_surface
                    print "rerun write_flux_tree.py and write_flux_history.py"
                    print ""


    # Check contamination
    cont_ok = False
    for dataset in mp_group:
        if "n_contamination" in dataset:
            cont_ok = True
    if not cont_ok:
        print "verify_zoom.py: Dark matter contamination numbers were not written correctly for", OutDir+subvol_filename
        print "rerun write_contamination_tree.py"
        print ""

    # Check maps
    if not os.path.isfile(MapDir+"individ_ts/ts_"+str(timestep_final)+"/rho_gas_tot_ts_"+str(timestep_final)+".png"):
        print "verify_zoom.py: Maps were not written correctly for", OutDir+subvol_filename
        print "rerun write_map_tree_main.py"
        print ""

# Check for contamination and print warnings
for halo_group_str in zoom_group:

    if not cont_ok:
        continue # Skip if contamination information not written correctly
    
    if not "Halo" in halo_group_str:
        continue

    ap_group = zoom_group[halo_group_str+"/all_progenitors"]
    mp_group = zoom_group[halo_group_str+"/main_progenitors"]

    n2_mp =   mp_group["n_contamination_2rvir"][:]
    n2_ap =   ap_group["n_contamination_2rvir"][:]
    n1p5_mp = mp_group["n_contamination_1p5rvir"][:]
    n1p5_ap = ap_group["n_contamination_1p5rvir"][:]
    n1_mp =   mp_group["n_contamination_1rvir"][:]
    n1_ap =   ap_group["n_contamination_1rvir"][:]
    
    warning_str = "verify_zoom.py: Contamination warning for ", OutDir+subvol_filename

    if n1_mp.max() > 0:
        print warning_str
        print "There are low-res dm particles within 1 rvir of a main progenitor of ", halo_group_str
    elif n1_ap.max() > 0:
        print warning_str
        print "There are low-res dm particles within 1 rvir of a non-main progenitor of ", halo_group_str
    elif n1p5_mp.max() > 0:
        print warning_str
        print "There are low-res dm particles within 1.5 rvir of a main progenitor of ", halo_group_str
    elif n1p5_ap.max() > 0:
        print warning_str
        print "There are low-res dm particles within 1.5 rvir of a non-main progenitor of ", halo_group_str
    elif n2_mp.max() > 0:
        print warning_str
        print "There are low-res dm particles within 2 rvir of a main progenitor of ", halo_group_str
    elif n2_ap.max() > 0:
        print warning_str
        print "There are low-res dm particles within 2 rvir of a non-main progenitor of ", halo_group_str
    


File.close()
