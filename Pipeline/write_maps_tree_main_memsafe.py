# 22/2/16
# This is a version of write_maps_tree_main.py that runs on one timestep only to avoid memory leaks from hurting the poor computer.
# This is intended as a stopgap until I fix the main version of the routine

import argparse
import sys
import numpy as np
import time
import os

t1 = time.time()

# Write 0.2 virial radius circle?
plot_galaxy_circle = False
# Draw subhalo circles
plot_sub_circle = False
subhalo_radius = "r_tidal"
# Draw physical scale
plot_scale = True
scale_size = 10.0 # pkpc
# Plot star maps
star_map = False

############ Define list of maps to write ###############################
# General properties
#print "hack"
#map_size = 2.0
map_size = 1.1 # Size in 1 dimension of the map in units of the virial radius
figure_size = 10 # Factor determining size of the figures
dpi_factor = 1.0 # Scaling factor to up/down scale the dpi (resolution)

###### DM maps
nbins_dm = 500 # nbins x nbins histogram
cmap_dm = "BuPu"

vmin_dm = 1e-11 # Set colourbar range
vmax_dm_f = lambda mhalo, norm: mhalo * 1e-10 * norm

###### Star maps
nbins_star = 500
cmap_star = "Greys"
vmin_star = 8e-13
vmax_star_f = lambda mhalo, norm: mhalo * 1e-10 * norm

# Luminosity maps
band = "UA"
vmin_lum = 0.003
vmax_lum_f = lambda mhalo, norm: mhalo * norm / (10**10.6)
cmap_lum = "Greys"

###### Gas maps
# Define coordinates of phase lines
phase_logT1 = 3.5
phase_logT2 = 4.0
phase_logT3 = 4.5
phase_m = -1.136
phase_c = 3.0
phase_n = -1.132

map_type_list = ["nh"]
temp_lo_list  = [0.0 ]
temp_hi_list  = [1e20]
vrad_lo_list  = [-1e20]
vrad_hi_list  = [1e20]
nh_lo_list    = [0.0 ]                
nh_hi_list    = [1e20]
xhi_lo_list = [0.0]
xhi_hi_list = [1.0]
exc_sub = [2.0]
map_name_list = ["rho_gas_tot"]

n_gas_maps = len(map_type_list)

nh_min_plot     = 2e-6
nh_max_plot_f   =  lambda mhalo, norm: mhalo * 1e-10 * norm
vrad_min_plot_f =  lambda mhalo, norm: mhalo * 1e-10 * norm
vrad_max_plot_f =  lambda mhalo, norm: mhalo * 1e-10 * norm
temp_min_plot_f =  lambda norm: norm
temp_max_plot_f = lambda mhalo, norm: mhalo * 1e-10 * norm

cmap_nh = "Blues"
cmap_vrad = "RdBu"
cmap_temp = "seismic"
cmap_lev = "BuPu"

###### Phase diagram maps
nbins_phase = 500

plot_phase_lines = False


phase_weights_list    = []
r_inner_aperture_list = []
r_outer_aperture_list = []
vrad_lo_phase_list =    []
vrad_hi_phase_list =    []
exc_sub_phase            =    []
phase_map_name_list =   []

n_maps_phase = len(phase_map_name_list)

phase_temp_lo = 10 # Kelvin
phase_temp_hi = 1e8 # Kelvin
phase_nh_lo = 1e-6 # Atoms per cc
phase_nh_hi = 1e5 # Atoms per cc

vmin_phase = 1e-4
vmax_phase_f = lambda mhalo, norm: mhalo * 1e-10 * norm
cmap_phase = "CMRmap"


parser = argparse.ArgumentParser()
parser.add_argument("-zoom", help = "name of the zoom simulation")
parser.add_argument("-Run", help = "name of the specific run for this zoom simulation")
parser.add_argument("-rd","--RunDir", help="path to simulation directory containing all these output_xxxxx dirs.")
parser.add_argument("-hd","--HaloDir", help="path to directory containing runs of HaloMaker (i.e. containing xx/tree_bricks...)")
parser.add_argument("-td","--TreeDir", help="path to directory containing runs of TreeMaker")
parser.add_argument("-od","--OutDir", help="path to output directory for hdf5 files")
parser.add_argument("-ts_final","--timestep_final", type=int,help="final timestep")
parser.add_argument("-v","--verbose_str",help="verbose...")
parser.add_argument("-fb","--feedback_str",help="feedback on for this run: True/False?")
parser.add_argument("-ts", "--timestep", type=int,help="timestep to make maps")
parser.add_argument("-rhd","--RHD",help="Radiation hydrodynamics run")
parser.add_argument("-ts_out","--timesteps_out",help="output timestep(s), 'all, 'final', or e.g. '2'")

if len(sys.argv)!=25:
    parser.print_help()
    sys.exit(1)
args = parser.parse_args()

# Set paths
Zoom = args.zoom
Run = args.Run
RunDir = args.RunDir
HaloDir = args.HaloDir
TreeDir = args.TreeDir
OutDir = args.OutDir
timestep_final = args.timestep_final # Timestep for reading in halo brick files and ramses raw output
verbose_str = args.verbose_str
feedback_str = args.feedback_str
timestep = args.timestep
RHD = args.RHD
timesteps_out = args.timesteps_out

if "True" in feedback_str:
    feedback = True
else:
    feedback = False

if "True" in verbose_str:
    verbose = True
else:
    verbose = False

if "True" in RHD:
    RHD = True
else:
    RHD = False


timestep_tree_final = timestep_final -1 # At the moment, the tree files store "halo_ts" as timestep -1 (don't ask)



# Set map normalisations (if feedback dependent)
if feedback:
    dm_hi_norm = 4e-9
    sm_hi_norm = 8e-9
    lum_hi_norm = 1e7
    rho_hi_norm = 3e3
    vrad_lo_norm = -150
    vrad_hi_norm = 150
    phase_hi_norm = 2.5e6
    temp_lo_norm = 1e2
    temp_hi_norm = 7e5
else:
    dm_hi_norm = 4e-9
    sm_hi_norm = 8e-9
    lum_hi_norm = 1e7
    rho_hi_norm = 6e4
    vrad_lo_norm = -130
    vrad_hi_norm = 130
    phase_hi_norm = 2.5e7
    temp_lo_norm = 1e3
    temp_hi_norm = 3.5e5

if verbose:
    print "writing maps for", Zoom, Run
    print "at timestep", timestep
    print 'imports ... '
import dm_utils as dm_utils
import cutils as cut
import h5py
import utilities_ramses as ur
import neighbour_utils as nu
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as py
from matplotlib.colors import LogNorm
from mpl_toolkits.axes_grid1.inset_locator import inset_axes
import star_utils as star

# Write output directories if needed
MapDir = OutDir + "Maps/"+Zoom+"_"+Run
try:
    os.makedirs(MapDir)
except OSError:
    pass # Directory already exists

for timestep_m1 in range(timestep_final):
    timestep_i = timestep_m1+1
    tsDir = MapDir+"/individ_ts/ts_"+str(timestep_i)
    try:
        os.makedirs(tsDir)
    except OSError:
        pass

map_name_list_extended = ["dm_density", "star_density","stellar_luminosity_"+band+"_density"] + map_name_list + phase_map_name_list
for map_name in map_name_list_extended:
    this_mapDir = MapDir + "/all_ts/" + map_name
    try:
        os.makedirs(this_mapDir)
    except OSError:
        pass


# Read in relevant tree information from the hdf5 file
if timesteps_out == "all":
    filename = Zoom + "_" + Run + "_tsf" + str(timestep_final) +".hdf5"
elif timesteps_out == "final":
    filename = Zoom + "_" + Run + "_tsf" + str(timestep_final) +"_ts"+str(timestep_final)+".hdf5"
else:
    try:
        timestep_out = int(timesteps_out)
        filename = Zoom + "_" + Run + "_tsf" + str(timestep_final) +"_ts"+str(timestep_out)+".hdf5"
    except:
        print "timesteps_out",timesteps_out,",is not valid, sorry!"
        quit()
File = h5py.File(OutDir+filename)

# Open the subvolume file (containing all cell data)
subvol_filename = "subvol_"+filename
subvol_File = h5py.File(OutDir+subvol_filename)

# Check the file with the tree information already exists
if not Zoom in File:
    print "Error: Tree information for ", Zoom, " in ", filename, " cannot be read"
    exit()

zoom_group = File[Zoom]

init_figure = True
init_figure_gas = True
init_cbar_lin = True
init_cbar_log = True
init_figure_phase = True
if plot_phase_lines:
    init_phase_lines = True
else:
    init_phase_lines = False

def Plot_Circle(x0,y0,r,ax,c="k"):
    theta = np.arange(0,2*np.pi+0.01,0.01)
    x = x0 + np.cos(theta)*r
    y = y0 + np.sin(theta)*r
    Circle = ax.plot(x,y,c=c)
    return Circle

def Plot_Map(fig, axes, images, sizex, sizey, figname, dpi, timestep, norm, extent, vmin, vmax, maps, cmap, xlabels, ylabels, plot_circles=False, init_cbar=False):

    py.figure(fig.number)

    fig.set_size_inches((3*sizex,sizey))

    ax1 = axes[0]
    ax2 = axes[1]
    ax3 = axes[2]

    image1 = images[0]
    image2 = images[1]
    image3 = images[2]

    xmin = extent[0]
    xmax = extent[1]
    ymin = extent[2]
    ymax = extent[3]
    zmin = extent[4]
    zmax = extent[5]

    map_xy = maps[0]; map_zx = maps[1]; map_yz = maps[2]

    # Note that the order in which you call these methods on imshow() instances can be important in some cases
    py.axes(ax1)
    image1.set_norm(norm)
    image1.set_extent((xmin,xmax,ymin,ymax))
    image1.norm.vmin = vmin
    image1.norm.vmax = vmax
    ax1.set_xlim((xmin,xmax))
    ax1.set_ylim((ymin,ymax))
    image1.set_data(map_xy.T)
    image1.set_cmap(cmap)

    if plot_circles:
        Circle1 = Plot_Circle(0.0,0.0,rvirh,ax1)
        if plot_sub_circle:
            for i_sub in range(n_sub):
                Circle_sub = Plot_Circle(subhalo_x_list[i_sub], subhalo_y_list[i_sub], subhalo_rvir_list[i_sub],ax1)
        if plot_galaxy_circle:
            Circle_Gal = Plot_Circle(0.0,0.0,0.2*rvirh,ax1,c="r")

    if plot_scale:
        ax1.errorbar([xmin+scale_size*0.6], [ymin+scale_size*0.6], xerr= [scale_size*0.5], yerr=[scale_size*0.5],c="k")
        #ax1.annotate((r"$%3.1f \, \mathrm{pkpc}$"%scale_size*r2kpc),(xmin+scale_size*0.7, ymin+scale_size*0.7))
        # Weird visual bug with annotate invoked here, fix later...

    py.xlabel(xlabels[0])
    py.ylabel(ylabels[0])

    py.axes(ax2)
    image2.set_norm(norm)
    image2.set_extent((zmin,zmax,xmin,xmax))
    image2.norm.vmin = vmin
    image2.norm.vmax = vmax
    ax2.set_xlim((zmin,zmax))
    ax2.set_ylim((xmin,xmax))
    image2.set_data(map_zx.T)
    image2.set_cmap(cmap)
    if plot_circles:
        Circle2 = Plot_Circle(0.0,0.0,rvirh,ax2)
        if plot_sub_circle:
            for i_sub in range(n_sub):
                Circle_sub = Plot_Circle(subhalo_z_list[i_sub], subhalo_x_list[i_sub], subhalo_rvir_list[i_sub],ax2)
        if plot_galaxy_circle:
            Circle_Gal = Plot_Circle(0.0,0.0,0.2*rvirh,ax2,c="r")

    
    if plot_scale:
        ax2.errorbar([zmin+scale_size*0.6], [xmin+scale_size*0.6], xerr= [scale_size*0.5], yerr=[scale_size*0.5],c="k")
        #ax2.annotate((r"$%3.1f \, \mathrm{pkpc}$"%scale_size*r2kpc),(zmin+scale_size*0.7, xmin+scale_size*0.7))


    py.xlabel(xlabels[1])
    py.ylabel(ylabels[1])

    
    py.axes(ax3)
    image3.set_norm(norm)
    image3.set_extent((ymin,ymax,zmin,zmax))
    image3.norm.vmin = vmin
    image3.norm.vmax = vmax
    ax3.set_xlim((ymin,ymax))
    ax3.set_ylim((zmin,zmax))
    image3.set_data(map_yz.T)
    image3.set_cmap(cmap)
    if plot_circles:
        Circle3 = Plot_Circle(0.0,0.0,rvirh,ax3)
        if plot_sub_circle:
            for i_sub in range(n_sub):
                Circle_sub = Plot_Circle(subhalo_y_list[i_sub], subhalo_z_list[i_sub], subhalo_rvir_list[i_sub],ax3)
        if plot_galaxy_circle:
            Circle_Gal = Plot_Circle(0.0,0.0,0.2*rvirh,ax3,c="r")
    
    if plot_scale:
        ax3.errorbar([ymin+scale_size*0.6], [zmin+scale_size*0.6], xerr= [scale_size*0.5], yerr=[scale_size*0.5],c="k")
    #    ax3.annotate((r"$%3.1f \, \mathrm{pkpc}$"%scale_size*r2kpc),(ymin+scale_size*0.7, zmin+scale_size*0.7))

    py.xlabel(xlabels[2])
    py.ylabel(ylabels[2])

    # Draw colorbars
    if init_cbar:
        axins = inset_axes(ax3,
                           width="5%", # width = 10% of parent_bbox width
                           height="100%", # height : 100%
                           loc=3, # right-hand-side
                           bbox_to_anchor=(1.12, 0., 1, 1),
                           bbox_transform=ax3.transAxes,
                           borderpad=0,
                           )
        cbar = py.colorbar(mappable=image3,cax=axins,label=cbar_label,ax=ax3)

    fig_path = MapDir+"/individ_ts/ts_"+str(timestep)+"/"+figname+"_ts_"+str(timestep)+'.png'
    py.savefig(fig_path,dpi=dpi)
    
    # Make a symbolic link to the "figname" folder
    fig_path2 = MapDir+"/all_ts/"+figname+"/"+figname+"_ts_"+str(timestep)+'.png'
    os.system("ln -f -s "+fig_path+" "+fig_path2)

timestep_tree = timestep-1

if verbose:
    print "reading cell/particle data at timestep = ", timestep, "of", timestep_final

# Get conversions for different units
r2kpc, redshift, little_h, m2Msun,rho2msunpkpc3,v2kms = ur.Set_Units(RunDir, timestep)
boxlen_pkpc = dm_utils.jeje_utils.get_param_real(RunDir,timestep,'boxlen') * r2kpc # Box size in proper kpc, using the correct box size

scale_size *= 1./r2kpc # code units


########## Read cell data #########################        
# Hdf5 group for this timestep (contains cell data)
ts_group = subvol_File["timestep_"+str(timestep)]
cell_group = ts_group["Gas_leaf_cell_data"]

rho_snap = cell_group["rho_c"][:] # Gas density
pre_snap = cell_group["pre_c"][:] # Gas pressure
cx = cell_group["x_c"][:] # Cell coordinates
cy = cell_group["y_c"][:]
cz = cell_group["z_c"][:]
cvx = cell_group["vx_c"][:]
cvy = cell_group["vy_c"][:]
cvz = cell_group["vz_c"][:]
clev = cell_group["lev"][:] # Cell refinement level
lmax = clev.max() # Maximum refinement level
c_component_index = cell_group["component_index"][:] # Flag to indicate whether inside/outside a subhalo

if RHD:
    xH1 = cell_group["xH1"][:]
    xHe1 =cell_group["xHe1"][:]
    xHe2 = cell_group["xHe2"][:]

# Read in star particle data
if star_map:
    star_group = ts_group["Stellar_particle_data"]

    sm = star_group["m_s"][:]
    sx = star_group["x_s"][:]
    sy = star_group["y_s"][:]
    sz = star_group["z_s"][:]
    svx = star_group["vx_s"][:]
    svy = star_group["vy_s"][:]
    svz = star_group["vz_s"][:]

    slum = star_group["lum_"+band+"_s"][:]


# Read in DM particle data
dm_group = ts_group["Dark_matter_particle_data"]

dm = dm_group["m_dm"][:]
dx = dm_group["x_dm"][:]
dy = dm_group["y_dm"][:]
dz = dm_group["z_dm"][:]

if verbose:
    print "Done, selecting particles/cells for this timestep"


# Build linked-list of cells (for neighbour finding later)
cxyz_min = min(cx.min(),cy.min(),cz.min())
cxyz_max = max(cx.max(),cy.max(),cz.max())

cx_rescale = (cx - cxyz_min) / (cxyz_max-cxyz_min)
cy_rescale = (cy - cxyz_min) / (cxyz_max-cxyz_min)
cz_rescale = (cz - cxyz_min) / (cxyz_max-cxyz_min)

nchain_cells = 20
nextincell,firstincell = nu.neighbour_utils.init_chain(cx_rescale,cy_rescale,cz_rescale,nchain_cells)

# Build linked-list of stellar particles (for neighbour finding later)
if star_map and len(sx)>0:
    sxyz_min = min(sx.min(),sy.min(),sz.min())
    sxyz_max = max(sx.max(),sy.max(),sz.max())

    sx_rescale = (sx - sxyz_min) / (sxyz_max-sxyz_min)
    sy_rescale = (sy - sxyz_min) / (sxyz_max-sxyz_min)
    sz_rescale = (sz - sxyz_min) / (sxyz_max-sxyz_min)

    nextincell_s,firstincell_s = nu.neighbour_utils.init_chain(sx_rescale,sy_rescale,sz_rescale,nchain_cells)

# Build linked-list of dark matter particles (for neighbour finding later)
dxyz_min = min(dx.min(),dy.min(),dz.min())
dxyz_max = max(dx.max(),dy.max(),dz.max())

dx_rescale = (dx - dxyz_min) / (dxyz_max-dxyz_min)
dy_rescale = (dy - dxyz_min) / (dxyz_max-dxyz_min)
dz_rescale = (dz - dxyz_min) / (dxyz_max-dxyz_min)

nextincell_d,firstincell_d = nu.neighbour_utils.init_chain(dx_rescale,dy_rescale,dz_rescale,nchain_cells)


# Factor to help convert into box units (note maxime's merger_tree reader has converted halo positions so be careful here!)
r2kpc_ideal, redshift, little_h, m2Msun,junk,junk = ur.Set_Units(RunDir, timestep, "ideal")
boxlen_cMpc = dm_utils.jeje_utils.get_param_real(RunDir,timestep,'boxlen') # Box size in box units
boxlen_cMpc *= r2kpc_ideal
boxlen_cMpc = boxlen_cMpc * (1+redshift) /1e3 # cMpc (comoving)
boxlen_cMpc = boxlen_cMpc * little_h # h^-1 cMpc
factor = 1./boxlen_cMpc

# Loop over final timestep haloes
for halo_group_str in zoom_group:
    # Only write maps for main progenitors of the main halo
    if "_main" in halo_group_str:
        break

halo_group = zoom_group[halo_group_str]
mp_group = zoom_group[halo_group_str+"/main_progenitors"]

# Identify the main progenitor at this timestep
ok_ts = np.array(mp_group["halo_ts"])==timestep_tree # haloes on this timestep

xh = mp_group["x"][ok_ts] * factor + 0.5
yh = mp_group["y"][ok_ts] * factor + 0.5
zh = mp_group["z"][ok_ts] * factor + 0.5

# Use r200 for central haloes, and rvir (for now) for satellite haloes
if mp_group["host_halo_id"][ok_ts] == mp_group["halo_id"][ok_ts]:
    rvirh = mp_group["r200"][ok_ts] / r2kpc
else:
    rvirh = mp_group["rvir"][ok_ts] * factor

ok_final = np.array(mp_group["halo_ts"])==timestep_tree_final        
mhalo_final = mp_group["mvir"][ok_final][:] * 1e11 # Halo mass in msun

vxh = mp_group["vx"][ok_ts] / v2kms # Halo velocity in Code units
vyh = mp_group["vy"][ok_ts] / v2kms
vzh = mp_group["vz"][ok_ts] / v2kms

# Find subhalos identified in the tree files
subhalo_rvir_list = []
subhalo_x_list = []
subhalo_y_list = []
subhalo_z_list = []

# Loop over all haloes and create a master list of all the subhaloes of the main halo at the desired time
# Read in halo catalogue (to find subhalo positions)
print "hack to change subhalo selection"
halo_cat_sub_selection = False
if halo_cat_sub_selection:
    from halotils import py_halo_utils as hutils
    import pandas as pd
    import dm_utils as dm_u
    treefile = "%s%i/tree_bricks%3.3i"%(HaloDir,timestep,timestep)
    nh,ns = hutils.get_nb_halos(treefile) # nhalo, nsubhalo
    halos = hutils.read_all_halos(treefile,nh+ns)
    halo_keys = ('nbpart','hnum','ts','level',
                 'host', 'hostsub', 'nbsub', 'nextsub', 'mass',
                 'x', 'y', 'z', 'vx', 'vy', 'vz', 'Lx', 'Ly', 'Lz', 'r',
                 'a', 'b', 'c', 'ek', 'ep', 'et',
                 'spin', 'rvir', 'mvir', 'tvir', 'cvel', 'rho0', 'r_c')
    halos = pd.DataFrame(columns=halo_keys,data=halos)
    
    # Get conversions for different units
    r2kpc, redshift, little_h, m2Msun,rho2msunpkpc3, v2kms = ur.Set_Units(RunDir, timestep)
    boxlen_pkpc = dm_u.jeje_utils.get_param_real(RunDir,timestep,'boxlen') * r2kpc # Box size in proper kpc, using the correct box size
    factor_subhalo = 3.08e24 / dm_u.jeje_utils.dp_scale_l

    for halo_group_str in zoom_group:
        if not "main" in halo_group_str:
           continue
        mp_group = zoom_group[halo_group_str+"/main_progenitors"]
        ok_ts = mp_group["halo_ts"][:]==timestep_tree
        if timesteps_out == "final":
            halo_num = mp_group["halo_id"][:][ok_ts]
        else:
            halo_num = mp_group["halo_num"][:][ok_ts]

    subhalo_host_num_list = np.array(halos.host)
    subhalo_num_list = np.array(halos.hnum)
    subhalo_rvir_list = np.array(halos.rvir) * factor_subhalo
    subhalo_x_list = np.array(halos.x) * factor_subhalo +0.5 - xh
    subhalo_y_list = np.array(halos.y) * factor_subhalo +0.5 - yh
    subhalo_z_list = np.array(halos.z) * factor_subhalo +0.5 - zh
    subhalo_nbpart_list = np.array(halos.nbpart)

    # Select subhaloes with > 500 particles that are subhaloes of the main halo
    ok = (halo_num == subhalo_host_num_list) & (halo_num != subhalo_num_list) & (subhalo_nbpart_list > 500)
    subhalo_rvir_list = subhalo_rvir_list[ok]
    subhalo_x_list = subhalo_x_list[ok]
    subhalo_y_list = subhalo_y_list[ok]
    subhalo_z_list = subhalo_z_list[ok]
    n_sub = len(subhalo_z_list)

# Otherwise use subhaloes from the merger trees
else:
    for halo_group_str in zoom_group:
        if not "main" in halo_group_str:
           continue
        mp_group = zoom_group[halo_group_str+"/main_progenitors"]
        ok_ts = mp_group["halo_ts"][:]==timestep_tree
        halo_main = mp_group["halo_id"][:][ok_ts]

    for halo_group_str in zoom_group:
        if not "Halo_" in halo_group_str:
            continue
        ap_group = zoom_group[halo_group_str+"/all_progenitors"]

        is_subhalo = (ap_group["host_halo_id"][:] - ap_group["halo_id"][:] != 0) & (timestep_tree == ap_group["halo_ts"][:]) & (ap_group["host_halo_id"][:]-halo_main == 0)

        if subhalo_radius == "r_tidal":
            subhalo_rvir_list.append(ap_group["r_tidal"][:][is_subhalo])
        elif subhalo_radius == "rvir":
            subhalo_rvir_list.append(ap_group["rvir"][:][is_subhalo]* factor)

        subhalo_x_list.append(ap_group["x"][:][is_subhalo]* factor + 0.5 - xh)
        subhalo_y_list.append(ap_group["y"][:][is_subhalo]* factor + 0.5 - yh)
        subhalo_z_list.append(ap_group["z"][:][is_subhalo]* factor + 0.5 - zh)

    subhalo_rvir_list = np.concatenate(subhalo_rvir_list)
    subhalo_x_list = np.concatenate(subhalo_x_list)
    subhalo_y_list = np.concatenate(subhalo_y_list)
    subhalo_z_list = np.concatenate(subhalo_z_list)

    n_sub = len(subhalo_z_list)

# Set map normalisations
vmax_dm = vmax_dm_f(mhalo_final, dm_hi_norm)
vmax_star = vmax_star_f(mhalo_final, sm_hi_norm)
vmax_lum = vmax_lum_f(mhalo_final, lum_hi_norm)
nh_max_plot = nh_max_plot_f(mhalo_final, rho_hi_norm)
vrad_min_plot = vrad_min_plot_f(mhalo_final, vrad_lo_norm)
vrad_max_plot = vrad_max_plot_f(mhalo_final, vrad_hi_norm)

#print "nasty hack to maximum radial velocity range"
#vrad_min_plot = -200.0
#vrad_max_plot = 200.0

vmax_phase = vmax_phase_f(mhalo_final, phase_hi_norm)
temp_min_plot = temp_min_plot_f(temp_lo_norm)
temp_max_plot = temp_max_plot_f(mhalo_final, temp_hi_norm)

########## Select cells in the vicinity of the halo (using a chain algorithm) ################
rmax = rvirh*map_size * 2**0.5

# Rescale halo positions for efficient cell division in chain algorithm                                                                         
xh_rescale = (xh-cxyz_min)/(cxyz_max-cxyz_min)
yh_rescale = (yh-cxyz_min)/(cxyz_max-cxyz_min)
zh_rescale = (zh-cxyz_min)/(cxyz_max-cxyz_min)
rmax_rescale = rmax/(cxyz_max-cxyz_min)

ncell_in_rmax = nu.neighbour_utils.get_npart_in_sphere_with_chain(RunDir,timestep,cx_rescale,cy_rescale,cz_rescale,xh_rescale,yh_rescale,zh_rescale,rmax_rescale,firstincell,nextincell)
if ncell_in_rmax > 0:
    select = nu.neighbour_utils.get_part_indicies_in_sphere_with_chain(RunDir,timestep,cx_rescale,cy_rescale,cz_rescale,xh_rescale,yh_rescale,zh_rescale,rmax_rescale,ncell_in_rmax,firstincell,nextincell)
else:
    select = np.zeros_like(cx) < 0

#select = np.zeros_like(cx) ==0

cx_h = cx[select]; cy_h = cy[select]; cz_h = cz[select]
rho_snap_h = rho_snap[select]; pre_snap_h = pre_snap[select]
cvx_h = cvx[select]; cvy_h = cvy[select]; cvz_h = cvz[select]
clev_h = clev[select]; c_component_index_h = c_component_index[select]

if RHD:
    xH1_h = xH1[select]; xHe1_h = xHe1[select] ; xHe2_h = xHe2[select]

# Set cell unit conversions for this timestep
cut.jeje_utils.read_conversion_scales(RunDir,timestep)

# Compute radial velocities (w.r.t. halo centre)
cr_h = np.sqrt(np.square(cx_h-xh)+np.square(cy_h-yh)+np.square(cz_h-zh))
cv_radial_h = np.sum(np.array([cvx_h-vxh, cvy_h-vyh, cvz_h-vzh]) * np.array([cx_h-xh,cy_h-yh,cz_h-zh]),axis=0)/cr_h
cv_radial_h *= cut.jeje_utils.dp_scale_l / cut.jeje_utils.dp_scale_t * 1e-5

# Compute number density in atoms per cc
nh_h = rho_snap_h * cut.jeje_utils.dp_scale_d / cut.jeje_utils.mh * cut.jeje_utils.xh

# Compute temperature over effective molecular weight in Kelvin
scale_temk = (cut.jeje_utils.dp_scale_l / cut.jeje_utils.dp_scale_t)**2 * cut.jeje_utils.mh / cut.jeje_utils.kb
temp_mu_h = pre_snap_h/rho_snap_h * scale_temk # T/mu in K

# Compute neutral gas number density in atoms per cc and the temperature in Kelvin
if RHD:
    xhi_h = (1-xH1_h)# Hydrogen neutral fraction
    X_frac=0.76
    mu_h = 1./( X_frac*(1.+xH1_h) + 0.25*(1.-X_frac)*(1.+xHe1_h+2.*xHe2_h) )
else:
    mu_h, xhi_h = cut.cell_utils.ramses_get_mu_xhi(RunDir,timestep,rho_snap_h, pre_snap_h, True)

nhi_h = nh_h * xhi_h # atoms per cc
temp_h = temp_mu_h * mu_h # K

# Compute ionised hydrogen gas number density
nhii_h = nh_h * (1-xhi_h) # atoms per cc

########## Select stellar particles in the vicinity of the halo (using a chain algorithm) ###########
# Rescale halo positions for efficient cell division in chain algorithm                                                                         

if star_map and len(sx) > 0:
    xh_rescale = (xh-sxyz_min)/(sxyz_max-sxyz_min)
    yh_rescale = (yh-sxyz_min)/(sxyz_max-sxyz_min)
    zh_rescale = (zh-sxyz_min)/(sxyz_max-sxyz_min)
    rmax_rescale = rmax/(sxyz_max-sxyz_min)

    ncell_in_rmax = nu.neighbour_utils.get_npart_in_sphere_with_chain(RunDir,timestep,sx_rescale,sy_rescale,sz_rescale,xh_rescale,yh_rescale,zh_rescale,rmax_rescale,firstincell_s,nextincell_s)
    if ncell_in_rmax > 0:
        select = nu.neighbour_utils.get_part_indicies_in_sphere_with_chain(RunDir,timestep,sx_rescale,sy_rescale,sz_rescale,xh_rescale,yh_rescale,zh_rescale,rmax_rescale,ncell_in_rmax,firstincell_s,nextincell_s)
    else:
        select = np.zeros_like(sx) < 0

    sx_h = sx[select]; sy_h = sy[select]; sz_h = sz[select]
    sm_h = sm[select]; slum_h = slum[select]

    # Put the halo at the centre of the particle coordinate system
    sx_h = sx_h - xh; sy_h = sy_h - yh; sz_h = sz_h -zh
elif star_map:
    sx_h = sx
# If there are no stellar particles for this step, just create a phantom particle at the origin so that a map is at least written
if star_map:
    if len(sx) == 0 or len(sx_h) == 0:
        sx_h = np.array([0.0]); sy_h = np.array([0.0]); sz_h = np.array([0.0]); sm_h = np.array([0.0]); slum_h = np.array([0.0])




############# Select dark matter particles in the vicinity of the halo (using a chain algorithm) ######
# Rescale halo positions for efficient cell division in chain algorithm                                                                         
xh_rescale = (xh-dxyz_min)/(dxyz_max-dxyz_min)
yh_rescale = (yh-dxyz_min)/(dxyz_max-dxyz_min)
zh_rescale = (zh-dxyz_min)/(dxyz_max-dxyz_min)
rmax_rescale = rmax/(dxyz_max-dxyz_min)

ncell_in_rmax = nu.neighbour_utils.get_npart_in_sphere_with_chain(RunDir,timestep,dx_rescale,dy_rescale,dz_rescale,xh_rescale,yh_rescale,zh_rescale,rmax_rescale,firstincell_d,nextincell_d)
if ncell_in_rmax > 0:
    select = nu.neighbour_utils.get_part_indicies_in_sphere_with_chain(RunDir,timestep,dx_rescale,dy_rescale,dz_rescale,xh_rescale,yh_rescale,zh_rescale,rmax_rescale,ncell_in_rmax,firstincell_d,nextincell_d)
else:
    select = np.zeros_like(dx) < 0

dx_h = dx[select]; dy_h = dy[select]; dz_h = dz[select]
dm_h = dm[select]

# Put the halo at the centre of the particle coordinate system
dx_h = dx_h - xh; dy_h = dy_h - yh; dz_h = dz_h -zh

if verbose:
    print "Done, now computing maps"

###### Compute DM maps #############
rmax_cube = float(rvirh) * map_size
xmin = -rmax_cube; xmax = rmax_cube
ymin = -rmax_cube; ymax = rmax_cube
zmin = -rmax_cube; zmax = rmax_cube

xmin_map = xmin +xh # For some reason, make_map from cutils needs the halo to be at it's original position in box units (intsead of shifted to the origin)
xmax_map = xmax +xh
ymin_map = ymin +yh
ymax_map = ymax +yh
zmin_map = zmin +zh
zmax_map = zmax +zh

H_dm_xy,xedges_dm_xy,yedges_dm_xy = np.histogram2d(dx_h,dy_h,weights=dm_h,bins=[nbins_dm,nbins_dm],range=[[xmin,xmax],[ymin,ymax]])
H_dm_zx,xedges_dm_zx,yedges_dm_zx = np.histogram2d(dz_h,dx_h,weights=dm_h,bins=[nbins_dm,nbins_dm],range=[[xmin,xmax],[ymin,ymax]])
H_dm_yz,xedges_dm_yz,yedges_dm_yz = np.histogram2d(dy_h,dz_h,weights=dm_h,bins=[nbins_dm,nbins_dm],range=[[xmin,xmax],[ymin,ymax]])

# set fig size and resolution
sizex = figure_size; sizey = figure_size
dpi = int(1.3*nbins_dm/sizex) * dpi_factor

# You only want one figure object instance and one imshow object instance to prevent memory leaking (particularly for imshow)
if init_figure:
    py.figure(figsize=(3*sizex,sizey))
    fig=py.gcf()
    init_figure = False

    ax1 = py.subplot(1,3,1)
    image1 = ax1.imshow(np.zeros((2,2)),interpolation='nearest',origin='lower') # Instantiate imshow object with dummy data

    ax2 = py.subplot(1,3,2)
    image2 = ax2.imshow(np.zeros((2,2)),interpolation='nearest',origin='lower') # Instantiate imshow object with dummy data

    ax3 = py.subplot(1,3,3) 
    image3 = ax3.imshow(np.zeros((2,2)),interpolation='nearest',origin='lower') # Instantiate imshow object with dummy data'''

# Plot DM maps
xlabels = ['x (code units)', 'z (code units)', 'y (code units)']
ylabels = ['y (code units)', 'x (code units)', 'z (code units)']
Plot_Map(fig, [ax1,ax2,ax3], [image1,image2,image3], sizex, sizey, "dm_density", dpi, timestep, LogNorm(), (xmin,xmax,ymin,ymax,zmin,zmax), vmin_dm, vmax_dm, [H_dm_xy, H_dm_zx, H_dm_yz], cmap_dm, xlabels, ylabels, plot_circles=True)


###### Compute stellar maps #############

if star_map:
    H_s_xy,xedges_s_xy,yedges_s_xy = np.histogram2d(sx_h,sy_h,weights=sm_h,bins=[nbins_star,nbins_star],range=[[xmin,xmax],[ymin,ymax]])
    H_s_zx,xedges_s_zx,yedges_s_zx = np.histogram2d(sz_h,sx_h,weights=sm_h,bins=[nbins_star,nbins_star],range=[[xmin,xmax],[ymin,ymax]])
    H_s_yz,xedges_s_yz,yedges_s_yz = np.histogram2d(sy_h,sz_h,weights=sm_h,bins=[nbins_star,nbins_star],range=[[xmin,xmax],[ymin,ymax]])

    # set fig size and resolution
    sizex = figure_size; sizey = figure_size
    dpi = int(1.3*nbins_dm/sizex) * dpi_factor

    # Plot stellar maps
    Plot_Map(fig, [ax1,ax2,ax3], [image1,image2,image3], sizex, sizey, "star_density", dpi, timestep, LogNorm(), (xmin,xmax,ymin,ymax,zmin,zmax), vmin_star, vmax_star, [H_s_xy, H_s_zx, H_s_yz], cmap_star, xlabels, ylabels)



    # Luminosity maps
    H_s_xy,xedges_s_xy,yedges_s_xy = np.histogram2d(sx_h,sy_h,weights=slum_h,bins=[nbins_star,nbins_star],range=[[xmin,xmax],[ymin,ymax]])
    H_s_zx,xedges_s_zx,yedges_s_zx = np.histogram2d(sz_h,sx_h,weights=slum_h,bins=[nbins_star,nbins_star],range=[[xmin,xmax],[ymin,ymax]])
    H_s_yz,xedges_s_yz,yedges_s_yz = np.histogram2d(sy_h,sz_h,weights=slum_h,bins=[nbins_star,nbins_star],range=[[xmin,xmax],[ymin,ymax]])

    # set fig size and resolution
    sizex = figure_size; sizey = figure_size
    dpi = int(1.3*nbins_dm/sizex) * dpi_factor

    # Plot luminosity maps
    Plot_Map(fig, [ax1,ax2,ax3], [image1,image2,image3], sizex, sizey, "stellar_luminosity_"+band+"_density", dpi, timestep, LogNorm(), (xmin,xmax,ymin,ymax,zmin,zmax), vmin_lum, vmax_lum, [H_s_xy, H_s_zx, H_s_yz], cmap_lum, xlabels, ylabels)


###### Compute gas maps ################

# You only want one figure object instance and one imshow object instance to prevent memory leaking (particularly for imshow)
if init_figure_gas:

    # For plots with a lognormal colormap
    py.figure(figsize=(3*sizex,sizey))
    fig_gas_lin=py.gcf()

    ax1_gas_lin = py.subplot(1,3,1)
    image1_gas_lin = ax1_gas_lin.imshow(np.zeros((2,2)),interpolation='nearest',origin='lower') # Instantiate imshow object with dummy data

    ax2_gas_lin = py.subplot(1,3,2)
    image2_gas_lin = ax2_gas_lin.imshow(np.zeros((2,2)),interpolation='nearest',origin='lower') # Instantiate imshow object with dummy data

    ax3_gas_lin = py.subplot(1,3,3) 
    image3_gas_lin = ax3_gas_lin.imshow(np.zeros((2,2)),interpolation='nearest',origin='lower') # Instantiate imshow object with dummy data

    # For plots with a linear colormap
    py.figure(figsize=(3*sizex,sizey))
    fig_gas_log=py.gcf()

    ax1_gas_log = py.subplot(1,3,1)
    image1_gas_log = ax1_gas_log.imshow(np.zeros((2,2)),interpolation='nearest',origin='lower') # Instantiate imshow object with dummy data

    ax2_gas_log = py.subplot(1,3,2)
    image2_gas_log = ax2_gas_log.imshow(np.zeros((2,2)),interpolation='nearest',origin='lower') # Instantiate imshow object with dummy data

    ax3_gas_log = py.subplot(1,3,3) 
    image3_gas_log = ax3_gas_log.imshow(np.zeros((2,2)),interpolation='nearest',origin='lower') # Instantiate imshow object with dummy data

    init_figure_gas = False


# Get map bin size
nx,ny = cut.cell_utils.get_map_nxny(lmax,xmin,xmax,ymin,ymax)
nx,nz = cut.cell_utils.get_map_nxny(lmax,xmin,xmax,zmin,zmax)

init_circles_gas_log = True
init_circles_gas_lin = True

for n in range(n_gas_maps):

    #if "neutral" not in map_name_list[n]:
    #    print "skipping", map_name_list[n]
    #    continue

    if verbose:
        print "computing gas map", map_name_list[n]

    # Select which quantity to make a map of
    if map_type_list[n] == "nh":
        quantity = nh_h
        map_weighting = nh_h
        cmap_gas = cmap_nh
        vmin = nh_min_plot
        vmax = nh_max_plot
        cbar_label = r'$n_{\mathrm{H}} \, \mathrm{[H/cm^3]}$'
        norm = LogNorm()
        do_max = False

    elif map_type_list[n] == "nh_neutral":
        quantity = nhi_h
        map_weighting = nh_h
        cmap_gas = cmap_nh
        vmin = nh_min_plot
        vmax = nh_max_plot
        cbar_label = r'$n_{\mathrm{HI}} \, \mathrm{[H/cm^3]}$'
        norm = LogNorm()
        do_max = False

    elif map_type_list[n] == "nh_ion":
        quantity = nhii_h
        map_weighting = nh_h
        cmap_gas = cmap_nh
        vmin = nh_min_plot
        vmax = nh_max_plot
        cbar_label = r'$n_{\mathrm{HII}} \, [H/cm^3]$'
        norm = LogNorm()
        do_max = False

    elif map_type_list[n] == "vrad":
        quantity = cv_radial_h
        map_weighting = nh_h
        cmap_gas = cmap_vrad
        vmin = vrad_min_plot
        vmax = vrad_max_plot
        cbar_label = r'$v_{\mathrm{radial}} \, \mathrm{[km s^{-1}]}$'
        norm = None
        do_max = False

    elif map_type_list[n] == "vrad_neutral":
        quantity = cv_radial_h
        map_weighting = nhi_h
        cmap_gas = cmap_vrad
        vmin = vrad_min_plot
        vmax = vrad_max_plot
        cbar_label = r'$v_{\mathrm{radial}} \, \mathrm{[km s^{-1}]}$'
        norm = None
        do_max = False

    elif map_type_list[n] == "vrad_ion":
        quantity = cv_radial_h
        map_weighting = nhii_h
        cmap_gas = cmap_vrad
        vmin = vrad_min_plot
        vmax = vrad_max_plot
        cbar_label = r'$v_{\mathrm{radial}} \, \mathrm{[km s^{-1}]}$'
        norm = None
        do_max = False


    elif map_type_list[n] == "temp":
        quantity = temp_h
        map_weighting = nh_h
        cmap_gas = cmap_temp
        vmin = temp_min_plot
        vmax = temp_max_plot
        cbar_label = r'$T \, \mathrm{[K]}$'
        norm = LogNorm()
        do_max = False

    elif map_type_list[n] == "temp_neutral":
        quantity = temp_h
        map_weighting = nhi_h
        cmap_gas = cmap_temp
        vmin = temp_min_plot
        vmax = temp_max_plot
        cbar_label = r'$T \, \mathrm{[K]}$'
        norm = LogNorm()
        do_max = False

    elif map_type_list[n] == "temp_ion":
        quantity = temp_h
        map_weighting = nhii_h
        cmap_gas = cmap_temp
        vmin = temp_min_plot
        vmax = temp_max_plot
        cbar_label = r'$T \, \mathrm{[K]}$'
        norm = LogNorm()
        do_max = False


    elif map_type_list[n] == "level":
        quantity = clev_h
        map_weighting = nh_h
        cmap_gas = cmap_lev
        vmin = 10
        vmax = lmax
        cbar_label = r'level'
        norm = None
        do_max = True

    else:
        print "Error, gas map quanity", map_type_list[n],"not recognised"
        quit()

    ######### Temperature/density/radial velocity cuts#############

    # Temperature / effective molecular weight selection
    ok = (temp_h > temp_lo_list[n]) & (temp_h < temp_hi_list[n])

    # Density selection
    ok = ok & (nh_h > nh_lo_list[n]) & (nh_h < nh_hi_list[n])

    # Combined density/temp cuts for special cases
    if "hot_dens" in map_name_list[n]:
        ok = ok & (np.log10(temp_h) > np.log10(nh_h) * phase_m + phase_c)

    if "hot_diff" in map_name_list[n]:
        ok = ok & (np.log10(temp_h) < np.log10(nh_h) * phase_m + phase_c)

    # Radial velocity selection
    ok = ok & (cv_radial_h > vrad_lo_list[n]) & (cv_radial_h < vrad_hi_list[n])

    # Neutral hydrogen fraction selection
    ok = ok & (xhi_h > xhi_lo_list[n]) & (xhi_h < xhi_hi_list[n])

    # Subhalo cut
    if exc_sub[n] == 0: # Exclude substructures
        ok = ok & (c_component_index_h==0)
    elif exc_sub[n] ==1: # Only include substructures
        ok = ok & (c_component_index_h > 0) & (c_component_index_h < 3)

    # Select a single point if nothing makes the selection cut
    if len(ok[ok]) == 0:
        ok = np.argmin(np.square(cx_h)+np.square(cy_h)+np.square(cz_h))

    # NOTE, at some point, I had to insert the [0] to get map_xy etc out of a tuple. This was presumably caused by some update to cell_utils.f90 made by Jeremy
    map_xy = cut.cell_utils.make_map(lmax,do_max,xmin_map,xmax_map,ymin_map,ymax_map,zmin_map,zmax_map,quantity[ok],map_weighting[ok],cx_h[ok],cy_h[ok],cz_h[ok],clev_h[ok],nx,ny)[0]
    map_zx = cut.cell_utils.make_map(lmax,do_max,zmin_map,zmax_map,xmin_map,xmax_map,ymin_map,ymax_map,quantity[ok],map_weighting[ok],cz_h[ok],cx_h[ok],cy_h[ok],clev_h[ok],nz,nx)[0]
    map_yz = cut.cell_utils.make_map(lmax,do_max,ymin_map,ymax_map,zmin_map,zmax_map,xmin_map,xmax_map,quantity[ok],map_weighting[ok],cy_h[ok],cz_h[ok],cx_h[ok],clev_h[ok],ny,nz)[0]
    
    # set fig size and resolution
    sizex = figure_size
    sizey = (sizex * ny)/nx
    dpi = int(1.3*nx/sizex) * dpi_factor

    # Draw virial radius the first time we plot a linear or log plot on a given timestep
    if not norm is None and init_circles_gas_log:
        plot_circles = True
        init_circles_gas_log = False
    elif norm is None and init_circles_gas_lin:
        plot_circles = True
        init_circles_gas_lin = False
    else:
        plot_circles = False

    if not norm is None and init_cbar_log:
        init_cbar = True
        init_cbar_log = False
    elif norm is None and init_cbar_lin:
        init_cbar = True
        init_cbar_lin = False
    else:
        init_cbar = False

    # Plot gas maps with linear colormap
    if norm is None:
        Plot_Map(fig_gas_lin, [ax1_gas_lin,ax2_gas_lin,ax3_gas_lin], [image1_gas_lin,image2_gas_lin,image3_gas_lin], sizex, sizey, map_name_list[n], dpi, 
                 timestep, norm, (xmin,xmax,ymin,ymax,zmin,zmax), vmin, vmax, [map_xy, map_zx, map_yz], cmap_gas, xlabels, ylabels, plot_circles = plot_circles, init_cbar=init_cbar)

    # Or plot gas maps with lognormal colormap
    else:
        Plot_Map(fig_gas_log, [ax1_gas_log,ax2_gas_log,ax3_gas_log], [image1_gas_log,image2_gas_log,image3_gas_log], sizex, sizey, map_name_list[n], dpi, 
                 timestep, norm, (xmin,xmax,ymin,ymax,zmin,zmax), vmin, vmax, [map_xy, map_zx, map_yz], cmap_gas, xlabels, ylabels, plot_circles = plot_circles, init_cbar=init_cbar)


# Clean up virial radius lines
#ax1.lines.pop(0)
#ax2.lines.pop(0)
#ax3.lines.pop(0)
#ax1_gas_log.lines.pop(0); ax2_gas_log.lines.pop(0); ax3_gas_log.lines.pop(0)
#ax1_gas_lin.lines.pop(0); ax2_gas_lin.lines.pop(0); ax3_gas_lin.lines.pop(0)

######## Compute phase diagram maps ###################

if init_figure_phase:
    py.figure(figsize=(12,12))
    fig_phase=py.gcf()
    init_figure_phase = False

    ax_p = py.subplot(1,1,1)
    image_p = ax_p.imshow(np.zeros((2,2)),interpolation='nearest',origin='lower') # Instantiate imshow object with dummy data
else:
    py.figure(fig_phase.number)


for n in range(n_maps_phase):


    ######### Radius/radial velocity cuts #############

    # Radial cut
    ok = (cr_h > rvirh * r_inner_aperture_list[n]) & (cr_h < rvirh * r_outer_aperture_list[n])

    # Radial velocity selection
    ok = ok & (cv_radial_h > vrad_lo_phase_list[n]) & (cv_radial_h < vrad_hi_phase_list[n])

    # Subhalo cut
    if exc_sub_phase[n] == 0: # Exclude substructures
        ok = ok & (c_component_index_h==0)
    elif exc_sub_phase[n] ==1: # Only include substructures
        ok = ok & (c_component_index_h > 0) & (c_component_index_h < 3)

    if len(ok[ok])==0:
        continue
        
    # Choose mh (hydrogren mass), mHI (neutral hydrogen mass) or mHII (ionised hydrogen mass) weighting
    if phase_weights_list[n] == "mh":
        phase_weight = nh_h * (1./(2.**clev_h))**3 # cell hydrogen mass in code units (so no Helium)
    elif phase_weights_list[n] == "mhi":
        phase_weight = xhi_h * nh_h * (1./(2.**clev_h))**3 # neutral hydrogen mass in code units
    elif phase_weights_list[n] == "mhii":
        phase_weight = (1-xhi_h) * nh_h * (1./(2.**clev_h))**3 # ionised hydrogen mass in code units
    else:
        print "Error: write_maps_tree_main_memsafe, phase map weighting scheme = ", phase_weights_list[n], " is not recognised"
        quit()
        
    ranges = [[np.log10(phase_nh_lo),np.log10(phase_nh_hi)],[np.log10(phase_temp_lo),np.log10(phase_temp_hi)]]
    H_p, xedges_p, yedges_p = np.histogram2d(np.log10(nh_h[ok]),np.log10(temp_h[ok]),weights=phase_weight[ok],bins=[nbins_phase,nbins_phase],range=ranges)

    phase_ymin = 10 # Kelvin / molecular weight
    phase_ymax = 1e8 # Kelvin / molecular weight
    phase_xmin = 1e-6 # Atoms per cc
    phase_xmax = 1e5 # Atoms per cc

    py.axes(ax_p)
    image_p.set_norm(LogNorm())
    image_p.set_extent((np.log10(phase_nh_lo),np.log10(phase_nh_hi),np.log10(phase_temp_lo),np.log10(phase_temp_hi)))
    #image_p.norm.vmin = vmin_phase just use min max until this gets fixed - cmass bug
    #image_p.norm.vmax = vmax_phase
    image_p.norm.vmin = H_p[H_p>0].min()
    image_p.norm.vmax = H_p.max()

    ax_p.set_xlim((np.log10(phase_xmin),np.log10(phase_xmax)))
    ax_p.set_ylim((np.log10(phase_ymin),np.log10(phase_ymax)))
    image_p.set_data(H_p.T)
    image_p.set_cmap(cmap_phase)
    py.xlabel(r'$Log_{10}(n_H)$ $[at/cc]$')
    py.ylabel(r'$Log_{10}(T)$ $[K]$')

    # Plot phase diagram lines if desired
    if init_phase_lines:
        init_phase_lines = False
        '''py.axhline(phase_logT1,c="k")
        py.axhline(phase_logT2,c="k")
        py.axhline(phase_logT3,c="k")
        phase_eqn = lambda x,m,c: m*x+c
        py.plot([-7,-1.32],[phase_eqn(-7,phase_m,phase_c),phase_eqn(-1.32,phase_m,phase_c)],c="k")'''

        # Plot neutral gas box
        py.plot([np.log10(0.03),np.log10(0.03)], [-5, 5], linestyle='--', c="k")
        py.plot([np.log10(0.03),20], [5, 5], linestyle='--', c="k")
        

    fig_path = MapDir+"/individ_ts/ts_"+str(timestep)+"/"+phase_map_name_list[n]+"_ts_"+str(timestep)+'.png'
    py.savefig(fig_path)

    # Make a symbolic link to the "figname" folder
    fig_path2 = MapDir+"/all_ts/"+phase_map_name_list[n]+"/"+phase_map_name_list[n]+"_ts_"+str(timestep)+'.png'
    os.system("ln -f -s "+fig_path+" "+fig_path2)

py.close("all")

subvol_File.close()
File.close()

print "time taken was", time.time() - t1
