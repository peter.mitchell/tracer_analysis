# This routine is an updated version of write_gas_mass_tree_exc_sub.py
# The distinction in this routine is that it can remove gas within the virial radius of subhaloes from gas mass measurments

import time
t1 = time.time()
t_io = 0
t_cpu = 0
t_chain = 0

import argparse
import sys
import numpy as np

parser = argparse.ArgumentParser()
parser.add_argument("-zoom", help = "name of the zoom simulation")
parser.add_argument("-Run", help = "name of the specific run for this zoom simulation")
parser.add_argument("-rd","--RunDir", help="path to simulation directory containing all these output_xxxxx dirs.")
parser.add_argument("-hd","--HaloDir", help="path to directory containing runs of HaloMaker (i.e. containing xx/tree_bricks...)")
parser.add_argument("-td","--TreeDir", help="path to directory containing runs of TreeMaker")
parser.add_argument("-od","--OutDir", help="path to output directory for hdf5 files")
parser.add_argument("-ts_final","--timestep_final", type=int,help="final timestep")
parser.add_argument("-ts_out","--timesteps_out",help="output timestep(s), 'all, 'final', or e.g. '2'")
parser.add_argument("-v","--verbose",help="verbose...")

if len(sys.argv)!=19:
    parser.print_help()
    sys.exit(1)
args = parser.parse_args()

# Set paths
Zoom = args.zoom
Run = args.Run
RunDir = args.RunDir
HaloDir = args.HaloDir
TreeDir = args.TreeDir
OutDir = args.OutDir
timestep_final = args.timestep_final # Timestep for reading in halo brick files and ramses raw output                                                      
timesteps_out = args.timesteps_out
verbose_str = args.verbose

if "True" in verbose_str:
    verbose = True
else:
    verbose = False

timestep_tree_final = timestep_final -1 # At the moment, the tree files store "halo_ts" as timestep -1 (don't ask)

################### Selection criteria for gas masses #######################################

cell_selection = "rvir_frac" # This makes units of r_inner/r_outer a fraction of the virial radius
#cell_seleciton = "fixed_aperture" # This makes units of r_inner/r_outer a fixed radius in pkpc

# If true, cells are only considered part of a subhalo if the subhalo contains more than a threshold number (fiducial value 1000) of dm particles (as computed by the halofinder)
exclude_below_threshold = False

#                                sub           halo-out       inner-sf
#                         total         halo-in        inner                outer-sub
r_inner_aperture_list = [ 0.0,   0.0,   0.2,   0.2,    0.0,   0.0  ,        0.2,  0.0,  0.0] # Inner radius
r_outer_aperture_list = [ 1.0,   1.0,   1.0,   1.0,    0.2,   0.2  ,        1.0,  0.25, 0.25] # Outer radius
temp_low_list         = [ 0.0,   0.0,   0.0,   0.0,    0.0,   0.0  ,        0.0,  0.0,  0.0] # Kelvin
temp_high_list        = [ 1e20,  1e20,  1e20,  1e20,   1e20,  1.0*10**3.5,  1e20, 1e20, 1e20] # Kelvin
nh_low_list           = [ 0.0,   0.0,   0.0,   0.0,    0.0,   1.0  ,        0.0,  0.0,  0.0] # atoms per cc
nh_high_list          = [ 1e20,  1e20,  1e20,  1e20,   1e20,  1e20 ,        1e20, 1e20, 1e20] # atoms per cc
v_radial_low_list     = [-1e20, -1e20, -1e20,  0.0,   -1e20, -1e20 ,       -1e20, -1e20,-1e20] # kms^-1
v_radial_high_list    = [ 1e20,  1e20,  0.0,   1e20,   1e20,  1e20 ,        1e20, 1e20, 1e20] # km^s-1
exc_sub               = [ 0,     1,     0,     0,      0,     0    ,        1,    0,    1] # 0 -> exclude substructre, 1 -> only substructure
selection_str         = ["mgas_tot_exc_sub", "mgas_sub", "mgas_halo_infall_exc_sub","mgas_halo_outflow_exc_sub", "mgas_inner_exc_sub", "mgas_inner_sf_exc_sub", "mgas_outer_sub", "mgas_r25_exc_sub", "mgas_r25_sub"]

# For neutral gas, make sure the name in selection_str contains "neutral"
#                                sub           halo-out      
#                         total         halo-in       inner
r_inner_aperture_list += [ 0.0,   0.0,   0.2,   0.2,   0.0  ] # Inner radius
r_outer_aperture_list += [ 1.0,   1.0,   1.0,   1.0,   0.2  ] # Outer radius
temp_low_list         += [ 0.0,   0.0,   0.0,   0.0,   0.0  ] # Kelvin
temp_high_list        += [ 1e20,  1e20,  1e20,  1e20,  1e20 ] # Kelvin
nh_low_list           += [ 0.0,   0.0,   0.0,   0.0,   0.0 ] # atoms per cc
nh_high_list          += [ 1e20,  1e20,  1e20,  1e20,  1e20 ] # atoms per cc
v_radial_low_list     += [-1e20, -1e20, -1e20,  0.0,  -1e20 ] # kms^-1
v_radial_high_list    += [ 1e20,  1e20,  0.0,   1e20,  1e20 ] # km^s-1
exc_sub               += [ 0,     1,     0,     0,     0    ] # 0 -> exclude substructre, 1 -> only substructure
selection_str += ["mgas_neutral_tot_exc_sub", "mgas_neutral_sub", "mgas_neutral_halo_infall_exc_sub", "mgas_neutral_halo_outflow_exc_sub", "mgas_neutral_inner_exc_sub"]

# For ionized gas, make sure the name in selection_str contains "ionized"
#                                sub           halo-out      
#                         total         halo-in       inner
r_inner_aperture_list += [ 0.0,   0.0,   0.2,   0.2,   0.0  ] # Inner radius
r_outer_aperture_list += [ 1.0,   1.0,   1.0,   1.0,   0.2  ] # Outer radius
temp_low_list         += [ 0.0,   0.0,   0.0,   0.0,   0.0  ] # Kelvin
temp_high_list        += [ 1e20,  1e20,  1e20,  1e20,  1e20 ] # Kelvin
nh_low_list           += [ 0.0,   0.0,   0.0,   0.0,   0.0 ] # atoms per cc
nh_high_list          += [ 1e20,  1e20,  1e20,  1e20,  1e20 ] # atoms per cc
v_radial_low_list     += [-1e20, -1e20, -1e20,  0.0,  -1e20 ] # kms^-1
v_radial_high_list    += [ 1e20,  1e20,  0.0,   1e20,  1e20 ] # km^s-1
exc_sub               += [ 0,     1,     0,     0,     0    ] # 0 -> exclude substructre, 1 -> only substructure
selection_str += ["mgas_ionized_tot_exc_sub", "mgas_ionized_sub", "mgas_ionized_halo_infall_exc_sub", "mgas_ionized_halo_outflow_exc_sub", "mgas_ionized_inner_exc_sub"]


# Temperature in K to divide cool/cold gas with hot gas
tc = 10**4.3

#                                sub           halo-out        outer-sub
#                         total         halo-in        inner     
r_inner_aperture_list += [ 0.0,   0.0,   0.2,   0.2,    0.0,   0.2] # Inner radius
r_outer_aperture_list += [ 1.0,   1.0,   1.0,   1.0,    0.2,   1.0] # Outer radius
temp_low_list         += [ 0.0,   0.0,   0.0,   0.0,    0.0,   0.0] # Kelvin
temp_high_list        += [ tc,    tc,    tc,    tc,     tc,    tc] # Kelvin
nh_low_list           += [ 0.0,   0.0,   0.0,   0.0,    0.0,   0.0] # atoms per cc
nh_high_list          += [ 1e20,  1e20,  1e20,  1e20,   1e20,  1e20] # atoms per cc
v_radial_low_list     += [-1e20, -1e20, -1e20,  0.0,   -1e20, -1e20] # kms^-1
v_radial_high_list    += [ 1e20,  1e20,  0.0,   1e20,   1e20,  1e20] # km^s-1
exc_sub               += [ 0,     1,     0,     0,      0,     1] # 0 -> exclude substructre, 1 -> only substructure
selection_str         += ["mgas_cool_tot_exc_sub", "mgas_cool_sub", "mgas_cool_halo_infall_exc_sub","mgas_cool_halo_outflow_exc_sub", "mgas_cool_inner_exc_sub", "mgas_cool_outer_sub"]

rmax = np.max(np.array(r_outer_aperture_list)) # Maximum radius within which we need to consider cells for a given halo

n_gas = len(r_inner_aperture_list)


if verbose:
    print 'imports ... '
import dm_utils as dm
import cutils as cut
import h5py
import utilities_ramses as ur
import neighbour_utils as nu

# Read in relevant tree information from the hdf5 file
if timesteps_out == "all":
    filename = Zoom + "_" + Run + "_tsf" + str(timestep_final) +".hdf5"
elif timesteps_out == "final":
    filename = Zoom + "_" + Run + "_tsf" + str(timestep_final) +"_ts"+str(timestep_final)+".hdf5"
else:
    try:
        timestep_out = int(timesteps_out)
        filename = Zoom + "_" + Run + "_tsf" + str(timestep_final) +"_ts"+str(timestep_out)+".hdf5"
    except:
        print "timesteps_out",timesteps_out,",is not valid, sorry!"
        quit()
File = h5py.File(OutDir+filename)

# Open the subvolume file (containing all cell data)
subvol_filename = "subvol_"+filename
subvol_File = h5py.File(OutDir+subvol_filename)

# Check the file with the tree information already exists
if not Zoom in File:
    print "Error: Tree information for ", Zoom, " in ", filename, " cannot be read"
    exit()

zoom_group = File[Zoom]

# Loop over all haloes and create a master list of all the subhaloes at all times
subhalo_id_list = []
subhalo_host_id_list = []
subhalo_rvir_list = []
subhalo_x_list = []
subhalo_y_list = []
subhalo_z_list = []

for halo_group_str in zoom_group:
    if not "Halo_" in halo_group_str:
        continue
    ap_group = zoom_group[halo_group_str+"/all_progenitors"]
    
    subhalo_host_id_list.append(ap_group["host_halo_id"][:])
    subhalo_id_list.append(ap_group["halo_id"])

    subhalo_rvir_list.append(ap_group["rvir"][:])

    subhalo_x_list.append(ap_group["x"][:])
    subhalo_y_list.append(ap_group["y"][:])
    subhalo_z_list.append(ap_group["z"][:])

subhalo_host_id_list = np.concatenate(subhalo_host_id_list)
subhalo_id_list = np.concatenate(subhalo_id_list)
subhalo_rvir_list = np.concatenate(subhalo_rvir_list)
subhalo_x_list = np.concatenate(subhalo_x_list)
subhalo_y_list = np.concatenate(subhalo_y_list)
subhalo_z_list = np.concatenate(subhalo_z_list)

init_lists = True

# Select desired timesteps
if timesteps_out == "all":
    timesteps_tree_out = np.arange(timestep_tree_final)+1
elif timesteps_out == "final":
    timesteps_tree_out = np.array([timestep_tree_final])
else:
    try:
        timesteps_tree_out = np.array([int(timesteps_out)])-1
    except:
        print "timesteps_out",timesteps_out,",is not valid, sorry!"
        quit()

# Loop over timesteps
for i_timestep_tree, timestep_tree in enumerate(timesteps_tree_out):
   
    timestep = timestep_tree + 1 # Stupid mismatch between merger tree files and everything else

    #if timestep_tree != 1 and timestep_tree != timestep_tree_final:
    #    print "skip"
    #    continue
    #if timestep_tree != timestep_tree_final:
    #    print "skip"
    #    continue

    if verbose:
        print "performing IO at timestep = ", timestep, "of", timestep_final
    
    # Get conversions for different units
    r2kpc, redshift, little_h, m2Msun,rho2msunpkpc3, v2kms = ur.Set_Units(RunDir, timestep)
    boxlen_pkpc = dm.jeje_utils.get_param_real(RunDir,timestep,'boxlen') * r2kpc # Box size in proper kpc, using the correct box size

    t_io_temp = time.time()

    ########## Read cell data #########################        
    # Hdf5 group for this timestep (contains cell data)
    ts_group = subvol_File["timestep_"+str(timestep)]
    cell_group = ts_group["Gas_leaf_cell_data"]

    rho_snap = cell_group["rho_c"][:] # Gas density
    pre_snap = cell_group["pre_c"][:] # Gas pressure
    cx = cell_group["x_c"][:] # Cell coordinates
    cy = cell_group["y_c"][:]
    cz = cell_group["z_c"][:]
    cvx = cell_group["vx_c"][:]
    cvy = cell_group["vy_c"][:]
    cvz = cell_group["vz_c"][:]
    lev = cell_group["lev"][:] # Cell refinement level
    c_component_index = cell_group["component_index"][:] # Flag to indicate whether inside/outside a subhalo
   
    # Only count cells as in a subhalo if the subhalo contains > a threshold number (fiducial value=1000) of dm particles in the halo finder
    if exclude_below_threshold:
        in_sub = c_component_index == 2
    else:
        in_sub = (c_component_index > 0) & (c_component_index < 3)
    out_sub = in_sub == False


    t_io += time.time() - t_io_temp

    if verbose:
        print "Done: performing unit conversions and setting up chains"

    t_cpu_temp = time.time()

    cut.jeje_utils.read_conversion_scales(RunDir,timestep)
    ndensity = rho_snap * cut.jeje_utils.dp_scale_d / cut.jeje_utils.mh * cut.jeje_utils.xh # Number density: Atoms cm^-3
    scale_temk = (cut.jeje_utils.dp_scale_l / cut.jeje_utils.dp_scale_t)**2 * cut.jeje_utils.mh / cut.jeje_utils.kb
    temp = pre_snap/rho_snap * scale_temk # T/mu in K

    # Compute cell size using amr refinement level and box size
    csize = boxlen_pkpc / 2.**lev # proper kpc
    cmass = rho_snap * rho2msunpkpc3 * csize**3 # Msun - gas mass in each cell

    t_chain_temp = time.time()

    # Build linked-list of cells
    cxyz_min = min(cx.min(),cy.min(),cz.min()) 
    cxyz_max = max(cx.max(),cy.max(),cz.max())
    
    cx_rescale = (cx - cxyz_min) / (cxyz_max-cxyz_min)
    cy_rescale = (cy - cxyz_min) / (cxyz_max-cxyz_min)
    cz_rescale = (cz - cxyz_min) / (cxyz_max-cxyz_min)

    nleafcells = len(cx)
    nchain_cells = 20
    nextincell,firstincell = nu.neighbour_utils.init_chain(cx_rescale,cy_rescale,cz_rescale,nchain_cells)

    t_chain += time.time() - t_chain_temp

    # On the first timestep, initialise list to store cold gas masses for each final timestep halo
    if init_lists:
        gas_all_progen_list = [] #  gas masses for all progenitors
        gas_main_progen_list = [] # gas masses for main progenitors        
        ncell_all_progen_list = [] # leaf cell count for all progenitors
        ncell_main_progen_list = [] # leaf cell count for main progenitors

    # Factor to help convert into box units (note maxime's merger_tree reader has converted halo positions so be careful here!)
    r2kpc_ideal, redshift, little_h, m2Msun,junk,junk = ur.Set_Units(RunDir, timestep, "ideal")
    boxlen_cMpc = dm.jeje_utils.get_param_real(RunDir,timestep,'boxlen') # Box size in box units
    boxlen_cMpc *= r2kpc_ideal
    boxlen_cMpc = boxlen_cMpc * (1+redshift) /1e3 # cMpc (comoving)
    boxlen_cMpc = boxlen_cMpc * little_h # h^-1 cMpc
    factor = 1./boxlen_cMpc

    if verbose:
        print "Done: computing gas masses"

    # Loop over final timestep haloes
    ind_halo = -1
    for halo_group_str in zoom_group:

        # Check this is a halo
        if not "Halo_" in halo_group_str:
            continue

        ind_halo += 1 # Index like this instead of enumerate so that skipped parts of the for loop don't contribute to the index

        #if verbose:
        #    print "Computing gas masses for halo", halo_group_str
        #    print time.time()-t1

        ap_group = zoom_group[halo_group_str+"/all_progenitors"]
        mp_group = zoom_group[halo_group_str+"/main_progenitors"]

        # On first timestep, initialise arrays to store cold gas masses within list of apertures
        if init_lists:
            gas_all_progen_list.append( np.zeros(( n_gas, len(ap_group["halo_ts"][:]) )) )
            gas_main_progen_list.append( np.zeros(( n_gas, len(mp_group["halo_ts"][:]) )) )
            ncell_all_progen_list.append( np.zeros( len(ap_group["halo_ts"][:]) ))
            ncell_main_progen_list.append( np.zeros( len(mp_group["halo_ts"][:]) ))
            
        # Loop over haloes on this timestep
        ok_ts = ap_group["halo_ts"][:]==timestep_tree # haloes on this timestep
        nh_this_ts = len(ap_group["halo_ts"][:][ok_ts]) # Number of halos on this timestep    

        for nh in range(nh_this_ts):
            
            #if verbose:
            #    print "\rComputing gas masses for progenitor", nh+1, "of", nh_this_ts, "on this timestep"

            # Read halo coordinates
            xh = ap_group["x"][:][ok_ts][nh] * factor + 0.5
            yh = ap_group["y"][:][ok_ts][nh] * factor + 0.5
            zh = ap_group["z"][:][ok_ts][nh] * factor + 0.5

            # Use r200 for central haloes, and rvir (for now) for satellite haloes
            if ap_group["host_halo_id"][ok_ts][nh] == ap_group["halo_id"][ok_ts][nh]:
                rvirh = ap_group["r200"][ok_ts][nh] / r2kpc
            else:
                rvirh = ap_group["rvir"][ok_ts][nh] * factor

            vxh = ap_group["vx"][:][ok_ts][nh] / v2kms # Halo velocity in Code units
            vyh = ap_group["vy"][:][ok_ts][nh] / v2kms
            vzh = ap_group["vz"][:][ok_ts][nh] / v2kms

            t_chain_temp = time.time()

            # Rescale halo positions for efficient cell division in chain algorithm
            xh_rescale = (xh-cxyz_min)/(cxyz_max-cxyz_min)
            yh_rescale = (yh-cxyz_min)/(cxyz_max-cxyz_min)
            zh_rescale = (zh-cxyz_min)/(cxyz_max-cxyz_min)
            rmax_rescale = (rmax*rvirh)/(cxyz_max-cxyz_min)
            
            # Find cells within rmax of this halo (using chain algorithm)
            ncell_in_rmax = nu.neighbour_utils.get_npart_in_sphere_with_chain(RunDir,timestep,cx_rescale,cy_rescale,cz_rescale,xh_rescale,yh_rescale,zh_rescale,rmax_rescale,firstincell,nextincell)
           
            # Record how many gas cells are in each halo
            assign = ncell_all_progen_list[ind_halo][ok_ts]
            assign[nh] = ncell_in_rmax
            ncell_all_progen_list[ind_halo][ok_ts] = assign

            # Record cell counts for main progenitor list too
            match = ap_group["halo_id"][:][ok_ts][nh] - mp_group["halo_id"][:] == 0
            ncell_main_progen_list[ind_halo][match] = ncell_in_rmax

            if ncell_in_rmax > 0:
                ok = nu.neighbour_utils.get_part_indicies_in_sphere_with_chain(RunDir,timestep,cx_rescale,cy_rescale,cz_rescale,xh_rescale,yh_rescale,zh_rescale,rmax_rescale,ncell_in_rmax,firstincell,nextincell)
            else:
                ok = np.zeros_like(cx) < 0

            # Perform pre-selection (for efficiency reasons) of cells in a sphere of radius rmax around the halo
            cxh_in_sub = cx[ok][in_sub[ok]]; cyh_in_sub = cy[ok][in_sub[ok]]; czh_in_sub = cz[ok][in_sub[ok]]
            cxh_out_sub = cx[ok][out_sub[ok]]; cyh_out_sub = cy[ok][out_sub[ok]]; czh_out_sub = cz[ok][out_sub[ok]]
            
            cvxh_in_sub = cvx[ok][in_sub[ok]]; cvyh_in_sub = cvy[ok][in_sub[ok]]; cvzh_in_sub = cvz[ok][in_sub[ok]]
            cvxh_out_sub = cvx[ok][out_sub[ok]]; cvyh_out_sub = cvy[ok][out_sub[ok]]; cvzh_out_sub = cvz[ok][out_sub[ok]]

            cmass_h_in_sub = cmass[ok][in_sub[ok]]; ndensity_h_in_sub = ndensity[ok][in_sub[ok]]; temp_h_in_sub=temp[ok][in_sub[ok]]
            cmass_h_out_sub = cmass[ok][out_sub[ok]]; ndensity_h_out_sub = ndensity[ok][out_sub[ok]]; temp_h_out_sub=temp[ok][out_sub[ok]]

            lev_h_in_sub = lev[ok][in_sub[ok]]
            lev_h_out_sub = lev[ok][out_sub[ok]]

            # Compute neutral gas number density in atoms per cc and the temperature in Kelvin
            if len(pre_snap[ok][in_sub[ok]]) > 0:
                mu_h_in_sub, xhi_h_in_sub = cut.cell_utils.ramses_get_mu_xhi(RunDir,timestep,rho_snap[ok][in_sub[ok]], pre_snap[ok][in_sub[ok]], True)
            else:
                mu_h_in_sub = np.zeros_like(pre_snap[ok][in_sub[ok]]); xhi_h_in_sub = np.zeros_like(pre_snap[ok][in_sub[ok]])
            
            if len(pre_snap[ok][out_sub[ok]]) > 0:
                mu_h_out_sub, xhi_h_out_sub = cut.cell_utils.ramses_get_mu_xhi(RunDir,timestep,rho_snap[ok][out_sub[ok]], pre_snap[ok][out_sub[ok]], True)
            else:
                mu_h_out_sub = np.zeros_like(pre_snap[ok][out_sub[ok]]); xhi_h_out_sub = np.zeros_like(pre_snap[ok][out_sub[ok]])

            # Compute temperature without molecular weight factor in Kelvin
            temp_h_in_sub *= mu_h_in_sub
            temp_h_out_sub *= mu_h_out_sub

            t_chain += time.time() - t_chain_temp

            # Put the halo at the centre of the cell coordinate system
            cxh_in_sub = cxh_in_sub - xh; cyh_in_sub = cyh_in_sub - yh; czh_in_sub = czh_in_sub -zh
            cxh_out_sub = cxh_out_sub - xh; cyh_out_sub = cyh_out_sub - yh; czh_out_sub = czh_out_sub -zh
            
            # Compute the radius to each cell
            crh_in_sub = np.sqrt(np.square(cxh_in_sub) + np.square(cyh_in_sub) + np.square(czh_in_sub))
            crh_out_sub = np.sqrt(np.square(cxh_out_sub) + np.square(cyh_out_sub) + np.square(czh_out_sub))
            
            # Put cell velocties into the halo frame
            cvxh_in_sub = cvxh_in_sub - vxh; cvyh_in_sub = cvyh_in_sub - vyh; cvzh_in_sub = cvzh_in_sub - vzh
            cvxh_out_sub = cvxh_out_sub - vxh; cvyh_out_sub = cvyh_out_sub - vyh; cvzh_out_sub = cvzh_out_sub - vzh

            # Compute radial velocity
            cv_radial_h_in_sub = np.sum(np.array([cvxh_in_sub,cvyh_in_sub,cvzh_in_sub]) * np.array([cxh_in_sub,cyh_in_sub,czh_in_sub]),axis=0) / crh_in_sub
            cv_radial_h_out_sub = np.sum(np.array([cvxh_out_sub,cvyh_out_sub,cvzh_out_sub]) * np.array([cxh_out_sub,cyh_out_sub,czh_out_sub]),axis=0) / crh_out_sub
            

            ######### Compute gas masses for each set of selection criteria #####################
            for n in range(n_gas):

                # Radial aperture selection
                if cell_selection == "rvir_frac":
                    ok_in_sub = (crh_in_sub > rvirh * r_inner_aperture_list[n]) & (crh_in_sub < rvirh * r_outer_aperture_list[n])
                    ok_out_sub = (crh_out_sub > rvirh * r_inner_aperture_list[n]) & (crh_out_sub < rvirh * r_outer_aperture_list[n])
                
                else:
                    ok_in_sub = (crh_in_sub * r2kpc > r_inner_aperture_list[n]) & (crh_in_sub * r2kpc < r_outer_aperture_list[n])
                    ok_out_sub = (crh_out_sub * r2kpc > r_outner_aperture_list[n]) & (crh_out_sub * r2kpc < r_outer_aperture_list[n])

                
                # Temperature / effective molecular weight selection
                ok_in_sub = ok_in_sub & (temp_h_in_sub > temp_low_list[n]) & (temp_h_in_sub < temp_high_list[n])
                ok_out_sub = ok_out_sub & (temp_h_out_sub > temp_low_list[n]) & (temp_h_out_sub < temp_high_list[n])

                # Density selection
                ok_in_sub = ok_in_sub & (ndensity_h_in_sub > nh_low_list[n]) & (ndensity_h_in_sub < nh_high_list[n])
                ok_out_sub = ok_out_sub & (ndensity_h_out_sub > nh_low_list[n]) & (ndensity_h_out_sub < nh_high_list[n])

                # Radial velocity selection
                ok_in_sub =  ok_in_sub & (cv_radial_h_in_sub * v2kms > v_radial_low_list[n]) & (cv_radial_h_in_sub * v2kms < v_radial_high_list[n])
                ok_out_sub = ok_out_sub & (cv_radial_h_out_sub * v2kms > v_radial_low_list[n]) & (cv_radial_h_out_sub * v2kms < v_radial_high_list[n])

                # Compute gas mass
                if "neutral" in selection_str[n]: # Neutral hydrogen gas selection
                    gas_mass_in_sub = np.sum((cmass_h_in_sub*xhi_h_in_sub)[ok_in_sub])*cut.jeje_utils.xh
                    gas_mass_out_sub = np.sum((cmass_h_out_sub*xhi_h_out_sub)[ok_out_sub])*cut.jeje_utils.xh
                elif "ionized" in selection_str[n]: # Ionized hydrogen gas selection
                    gas_mass_in_sub = np.sum((cmass_h_in_sub*(1-xhi_h_in_sub))[ok_in_sub])*cut.jeje_utils.xh
                    gas_mass_out_sub = np.sum((cmass_h_out_sub*(1-xhi_h_out_sub))[ok_out_sub])*cut.jeje_utils.xh
                else: # Or just total gas selection
                    gas_mass_in_sub = np.sum(cmass_h_in_sub[ok_in_sub])
                    gas_mass_out_sub = np.sum(cmass_h_out_sub[ok_out_sub])

                #print selection_str[n], gas_mass_in_sub, gas_mass_out_sub, len(cx[ok]), len(cx[ok][in_sub[ok]]), len(cx[ok][out_sub[ok]])#, len(cmass_h_in_sub[ok_

                if exc_sub[n] == 0:
                    gas_mass = gas_mass_out_sub
                else:
                    gas_mass = gas_mass_in_sub

                # Stupid fix to deal with array assignment involving multi-indexing with booleans
                assign = gas_all_progen_list[ind_halo][n][ok_ts]
                assign[nh] = gas_mass
                gas_all_progen_list[ind_halo][n][ok_ts] = assign

                # If this halo is a main progenitor, assign corresponding cold gas mass
                match = ap_group["halo_id"][:][ok_ts][nh] - mp_group["halo_id"][:] == 0
                gas_main_progen_list[ind_halo][n][match] = gas_mass
                if len(np.array([gas_main_progen_list[ind_halo][n][match] ])) != 1:
                    print "Error, halo_id didn't match up correctly between all_progenitors to main_progenitors"
                    exit()

        # On the final timestep, write gas masses to hdf5 file. First check whether we need to delete a previously written entry
        if timestep == timesteps_tree_out[-1]+1:
                    
            if verbose:
                print "writing gas masses to disk"
            
            for dset in ap_group:
                for string in selection_str:
                    if string == dset:
                        del ap_group[dset]
                        del mp_group[dset]
                if "nleafcell" in dset:
                    del ap_group[dset]
                    del mp_group[dset]

            ap_group.create_dataset("nleafcell", data=ncell_all_progen_list[ind_halo])
            mp_group.create_dataset("nleafcell", data=ncell_main_progen_list[ind_halo])
            ap_group["nleafcell"].attrs["description"] = "number of leaf cells within the virial radius"
            mp_group["nleafcell"].attrs["description"] = "number of leaf cells within the virial radius"

            for n in range(n_gas):
                ap_group.create_dataset(selection_str[n], data=gas_all_progen_list[ind_halo][n])
                mp_group.create_dataset(selection_str[n], data=gas_main_progen_list[ind_halo][n])

                if cell_selection == "fixed_aperture":
                    radial_ap_str = 'radial aperture is between '+str(r_inner_aperture_list[n])+' and '+str(r_outer_aperture_list[n])+' kpc \n'
                if cell_selection == "rvir_frac":
                    radial_ap_str = 'radial aperture is between '+str(r_inner_aperture_list[n])+' and '+str(r_outer_aperture_list[n])+' r_vir \n'

                if exc_sub[n] == 0:
                    in_sub_str = "Excludes cells within rvir of subhaloes"
                else:
                    in_sub_str = "Only includes cells within rvir of subhaloes"

                description_str= ('Gas mass, units: Msun \n'
                                  + in_sub_str + '\n'
                                  'Selection criteria: \n'+radial_ap_str+
                                  'Temperature between '+str(temp_low_list[n])+' and '+str(temp_high_list[n])+' Kelvin \n'
                                  'Number density between '+str(nh_low_list[n])+' and '+str(nh_high_list[n])+' atoms per cc')
                
                ap_group[selection_str[n]].attrs['description'] = description_str

    init_lists = False
    
    t_cpu += time.time() - t_cpu_temp

subvol_File.close()
File.close()

print "time taken", time.time() - t1
print "io time", t_io
print "cpu time", t_cpu
print "chain cpu time", t_chain
