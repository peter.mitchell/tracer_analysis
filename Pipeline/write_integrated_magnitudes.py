import time
t1 = time.time()

import argparse
import sys

parser = argparse.ArgumentParser()
parser.add_argument("-zoom", help = "name of the zoom simluation")
parser.add_argument("-Run", help = "name of the specific run for this zoom simulation")
parser.add_argument("-rd","--RunDir", help="path to simulation directory containing all these output_xxxxx dirs.")
parser.add_argument("-hd","--HaloDir", help="path to directory containing runs of HaloMaker (i.e. containing xx/tree_bricks...)")
parser.add_argument("-td","--TreeDir", help="path to directory containing runs of TreeMaker")
parser.add_argument("-od","--OutDir", help="path to output directory for hdf5 files")
parser.add_argument("-ts_final","--timestep_final", type=int,help="final timestep")
parser.add_argument("-ts_out","--timesteps_out",help="output timestep(s), 'all, 'final', or e.g. '2'")
parser.add_argument("-v","--verbose",help="verbose...")

if len(sys.argv)!=19:
    parser.print_help()
    sys.exit(1)
args = parser.parse_args()

# Set paths   
Zoom = args.zoom
Run = args.Run
RunDir = args.RunDir
HaloDir = args.HaloDir
TreeDir = args.TreeDir
OutDir = args.OutDir
timestep_final = args.timestep_final # Timestep for reading in halo brick files and ramses raw output
timesteps_out = args.timesteps_out
verbose_str = args.verbose

if "True" in verbose_str:
    verbose = True
else:
    verbose = False

timestep_tree_final = timestep_final -1 # At the moment, the tree files store "halo_ts" as timestep -1 (don't ask)

# Select photometric bands (rest-frame only at the moment)
#           Top-hat 1500A
band_list = ["UA","V"]

star_selection = "rvir_frac"
if star_selection == "rvir_frac":
    r_aperture_list = [0.2]
elif star_selection == "fixed_aperture":
    # Select aperture sizes for in proper kpc
    r_aperture_list = [30] # Note that stellar mass will only be computed for particles within both rvir and r_aperture
else:
    print "not valid"
    exit()

if verbose:
    print 'imports ... '
import dm_utils as dm
import star_utils as star
from halotils import py_halo_utils as hutils
import matplotlib as mpl
mpl.use('tkagg')
import numpy as np
import h5py
import utilities_ramses as ur


# Read in relevant tree information from the hdf5 file
if timesteps_out == "all":
    filename = Zoom + "_" + Run + "_tsf" + str(timestep_final) +".hdf5"
elif timesteps_out == "final":
    filename = Zoom + "_" + Run + "_tsf" + str(timestep_final) +"_ts"+str(timestep_final)+".hdf5"
else:
    try:
        timestep_out = int(timesteps_out)
        filename = Zoom + "_" + Run + "_tsf" + str(timestep_final) +"_ts"+str(timestep_out)+".hdf5"
    except:
        print "timesteps_out",timesteps_out,",is not valid, sorry!"
        quit()
File = h5py.File(OutDir+filename)

# Check the file with the tree information already exists
if not Zoom in File:
    print "Error: Tree information for ", Zoom, " in ", filename, " cannot be read"
    exit()

zoom_group = File[Zoom]

# Open the subvolume file (containing all cell/particle data)
subvol_filename = "subvol_"+filename
subvol_File = h5py.File(OutDir+subvol_filename)

ct_init = False # Whether or not conformal_time.f90 has been intialised
list_init = False

if verbose:
    print "Computing integrated luminosities/magnitudes for", Zoom, Run

# Select desired timesteps
if timesteps_out == "all":
    timesteps_tree_out = np.arange(timestep_tree_final)+1
elif timesteps_out == "final":
    timesteps_tree_out = np.array([timestep_tree_final])
else:
    try:
        timesteps_tree_out = np.array([int(timesteps_out)])-1
    except:
        print "timesteps_out",timesteps_out,",is not valid, sorry!"
        quit()

# Loop over timesteps
for i_timestep_tree, timestep_tree in enumerate(timesteps_tree_out):

    timestep = timestep_tree + 1 # Stupid mismatch between merger tree files and everything else

    if verbose:
        print "Timestep", timestep, "of", timestep_final
    
    # Get conversions for different units
    r2kpc, redshift, little_h, m2Msun, junk, junk = ur.Set_Units(RunDir, timestep)
    r2kpc_ideal, redshift, little_h, m2Msun, junk, junk = ur.Set_Units(RunDir, timestep, "ideal") # Used to compute "ideal" box size. Used in turn to convert halo positions into box units
    boxlen_cMpc = dm.jeje_utils.get_param_real(RunDir,timestep,'boxlen') # Box size in box units
    boxlen_cMpc *= r2kpc_ideal
    boxlen_cMpc = boxlen_cMpc * (1+redshift) /1e3 # cMpc (comoving)
    boxlen_cMpc = boxlen_cMpc * little_h # h^-1 cMpc

    # Read in star particle data
    ts_group = subvol_File["timestep_"+str(timestep)]
    star_group = ts_group["Stellar_particle_data"]

    sx = star_group["x_s"][:]
    sy = star_group["y_s"][:]
    sz = star_group["z_s"][:]
    
    slum_list = []
    for band in band_list:
        slum_list.append( star_group["lum_"+band+"_s"][:] )

    
    # On the first timestep, initialise list to store stellar masses for each final timestep halo
    if not list_init:
        slum_all_progen_list = []
        slum_main_progen_list = []
        
    # Loop over final timestep haloes
    ind_halo = -1
    for halo_group_str in zoom_group:

        # Check this is a halo
        if not "Halo_" in halo_group_str:
            continue

        ind_halo += 1 # Index like this instead of enumerate so that skipped parts of the for loop don't contribute to the index

        halo_group = zoom_group[halo_group_str+"/all_progenitors"]
        mp_group = zoom_group[halo_group_str+"/main_progenitors"]

        # On first timestep, initialise arrays to store stellar masses within list of apertures
        if not list_init:
            slum_all_progen_list.append(np.zeros((len(r_aperture_list),  len(band_list), len(halo_group["halo_ts"]) )))
            slum_main_progen_list.append(np.zeros((len(r_aperture_list), len(band_list), len(mp_group["halo_ts"]) )))

        # Loop over haloes on this timestep
        ok_ts = np.array(halo_group["halo_ts"])==timestep_tree # haloes on this timestep
        nh_this_ts = len(halo_group["halo_ts"][ok_ts]) # Number of halos on this timestep
        
        for nh in range(nh_this_ts):
            # Factor to help convert into box units
            factor = 1./boxlen_cMpc
            xh = halo_group["x"][ok_ts][nh] * factor + 0.5 # Code units
            yh = halo_group["y"][ok_ts][nh] * factor + 0.5
            zh = halo_group["z"][ok_ts][nh] * factor + 0.5
            
            # Use r200 for central haloes, and rvir (for now) for satellite haloes
            if halo_group["host_halo_id"][ok_ts][nh] == halo_group["halo_id"][ok_ts][nh]:
                rvirh = halo_group["r200"][ok_ts][nh] / r2kpc
            else:
                rvirh = halo_group["rvir"][ok_ts][nh] * factor
            
            # Define cube within which to read stellar particles
            rmax = rvirh
            xmin = float(xh) - rmax
            xmax = float(xh) + rmax
            ymin = float(yh) - rmax
            ymax = float(yh) + rmax
            zmin = float(zh) - rmax
            zmax = float(zh) + rmax

            # Select stellar particles within this cube
            ok = (sx > xmin) & (sx < xmax) & \
                (sy > ymin) & (sy < ymax) & \
                (sz > zmin) & (sz < zmax)

            sxh = sx[ok]
            syh = sy[ok]
            szh = sz[ok]
            
            slumh_list = []
            for slum in slum_list:
                slumh_list.append(slum[ok])

            # Put the halo at the centre of the particle coordinate system
            sxh -= xh; syh -= yh; szh -= zh
            # Compute the radius to each stellar particle
            srh = np.sqrt(np.square(sxh) + np.square(syh) + np.square(szh))
            
            # For each spherical aperture, compute the enclosed luminosity. Note potential to double count satellites at the moment.
            # Also note that we apply the virial radius (rmax) as an additional constraint on the maximum size of the aperture.
            for nr, r_aperture in enumerate(r_aperture_list):
                if star_selection == "fixed_aperture":
                    ok = srh * r2kpc < min(r_aperture,rmax*r2kpc)
                elif star_selection == "rvir_frac":
                    ok = srh < r_aperture * rvirh

                '''# Stupid fix to deal with array assignment involving multi-indexing with booleans
                assign = slum_all_progen_list[ind_halo][nr][ok_ts]
                assign[nh] = np.sum(smh[ok])
                sm_tot_all_progen_list[ind_halo][nr][ok_ts] = assign'''

                # If this halo is a main progenitor, assign corresponding stellar mass
                match = halo_group["halo_id"][ok_ts][nh] - mp_group["halo_id"] == 0
                '''sm_tot_main_progen_list[ind_halo][nr][match] = np.sum(smh[ok])
                if len(np.array([sm_tot_main_progen_list[ind_halo][nr][match] ])) != 1:
                    print "Error, halo_id didn't match up correctly between all_progenitors to main_progenitors"
                    exit()'''

                # Compute luminosities for each band
                for i_band, band in enumerate(band_list):
                    lum = np.sum(slumh_list[i_band][ok]) # Flux at 10pc in units of erg s^-1 cm^-2
                    
                    assign = slum_all_progen_list[ind_halo][nr][i_band][ok_ts] # Nice indexing, so clear... :)
                    assign[nh] = lum
                    slum_all_progen_list[ind_halo][nr][i_band][ok_ts] = assign

                    slum_main_progen_list[ind_halo][nr][i_band][match] = lum

        # On the final timestep, write stellar masses to hdf5 file. First check whether we need to delete a previously written entry
        if timestep == timesteps_tree_out[-1]+1:
            if verbose:
                print "writing luminosities to disk"
            
            for nr, r_aperture in enumerate(r_aperture_list):
                
                for i_band, band in enumerate(band_list):
                    r_ap_lum = "lum_r" + str(r_aperture).replace(".","p") + "_" + band
                    if r_ap_lum in halo_group:
                        del halo_group[r_ap_lum]
                        del mp_group[r_ap_lum]
                    halo_group.create_dataset(r_ap_lum,data=slum_all_progen_list[ind_halo][nr][i_band])
                    mp_group.create_dataset(r_ap_lum,data=slum_main_progen_list[ind_halo][nr][i_band])

                    timescale_str = "computed for the " + band + " band (rest-frame),"

                    if star_selection == "fixed_aperture":
                        aperture_str = 'luminosity within min(rvir,'+str(r_aperture)+' kpc), '
                    elif star_selection == "rvir_frac":
                        aperture_str = 'luminosity within '+str(r_aperture)+' rvir, '

                    halo_group[r_ap_lum].attrs['description'] = aperture_str+timescale_str+' units: erg s^-1 cm^-2 (flux at 10pc)'
                    mp_group[r_ap_lum].attrs['description'] = aperture_str+timescale_str+' units: erg s^-1 cm^-2 (flux at 10pc)'
    
    list_init = True

File.close()

print "time taken was", time.time() - t1
