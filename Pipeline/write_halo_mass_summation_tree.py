import time
t1 = time.time()

import argparse
import sys

parser = argparse.ArgumentParser()
parser.add_argument("-zoom", help = "name of the zoom simulation")
parser.add_argument("-Run", help = "name of the specific run for this zoom simulation")
parser.add_argument("-rd","--RunDir", help="path to simulation directory containing all these output_xxxxx dirs.")
parser.add_argument("-hd","--HaloDir", help="path to directory containing runs of HaloMaker (i.e. containing xx/tree_bricks...)")
parser.add_argument("-td","--TreeDir", help="path to directory containing runs of TreeMaker")
parser.add_argument("-od","--OutDir", help="path to output directory for hdf5 files")
parser.add_argument("-ts_final","--timestep_final", type=int,help="final timestep")
parser.add_argument("-v","--verbose",help="verbose...")

if len(sys.argv)!=17:
    parser.print_help()
    sys.exit(1)
args = parser.parse_args()

# Set paths
Zoom = args.zoom
Run = args.Run
RunDir = args.RunDir
HaloDir = args.HaloDir
TreeDir = args.TreeDir
OutDir = args.OutDir
timestep_final = args.timestep_final # Timestep for reading in halo brick files and ramses raw output
verbose_str = args.verbose

timestep_tree_final = timestep_final -1

if "True" in verbose_str:
    verbose = True
else:
    verbose = False


if verbose:
    print 'imports ... '
from halotils import py_halo_utils as hutils
import numpy as np
import pandas as pd
import h5py
import os 
import utilities_ramses as ur


# Open the output hdf5 file
filename = Zoom + "_" + Run + "_tsf" + str(timestep_final) +".hdf5"
File = h5py.File(OutDir+filename)
zoom_group = File[Zoom]

mvir_max_past_inc_sub_ap = []
mvir_max_past_inc_sub_mp = []
for halo_group_str in zoom_group:
    if not "Halo_" in halo_group_str:
            continue

    ap_group = zoom_group[halo_group_str+"/all_progenitors"]
    mp_group = zoom_group[halo_group_str+"/main_progenitors"]

    mvir_max_past_inc_sub_ap.append(np.zeros_like(ap_group["mvir"]))
    mvir_max_past_inc_sub_mp.append(np.zeros_like(mp_group["mvir"]))

# Loop over timesteps
for timestep_tree in 1+np.arange(timestep_tree_final):
    timestep = timestep_tree + 1 # Stupid mismatch between merger tree files and everything else

    # read halo catalog at this timestep
    if verbose:
        print 'reading halo catalog for timestep', timestep
        
    treefile = "%s%i/tree_bricks%3.3i"%(HaloDir,timestep,timestep)

    nh,ns = hutils.get_nb_halos(treefile) # nhalo, nsubhalo
    halos = hutils.read_all_halos(treefile,nh+ns)
    halo_keys = ('nbpart','hnum','ts','level',
                 'host', 'hostsub', 'nbsub', 'nextsub', 'mass',
                 'x', 'y', 'z', 'vx', 'vy', 'vz', 'Lx', 'Ly', 'Lz', 'r',
                 'a', 'b', 'c', 'ek', 'ep', 'et',
                 'spin', 'rvir', 'mvir', 'tvir', 'cvel', 'rho0', 'r_c')
    halos = pd.DataFrame(columns=halo_keys,data=halos)
    halos.set_index(halos.hnum,inplace=True)    ## sort/index with hnum

    # Loop over final timestep haloes
    ind_halo = -1
    for halo_group_str in zoom_group:

        # Check this is a halo
        if not "Halo_" in halo_group_str:
            continue
        ind_halo += 1 # Index like this instead of enumerate so that skipped parts of the for loop don't contribute to the index

        #if verbose:
        #    print "Computing halo masses for halo", halo_group_str
        
        ap_group = zoom_group[halo_group_str+"/all_progenitors"]
        mp_group = zoom_group[halo_group_str+"/main_progenitors"]

        # Compute a virial mass for each progenitor which is:
        # a) Central halos: the sum of virial mass of central and subhalo objects
        # b) Satellite halos: the maximum past virial mass of this subhalo in the tree
        
        ok_ts = timestep_tree == ap_group["halo_ts"]

        for i in range(len(ap_group["mvir"][ok_ts])):
            # Do substructure summation if a central halo
            if ap_group["halo_id"][ok_ts][i] == ap_group["host_halo_id"][ok_ts][i]:
                # Add central + subhalo masses
                ok = (ap_group["halo_num"][ok_ts][i] == np.array(halos["host"]))

                # Time to suffer from one of those stupid assignment with booleans python problems
                temp = mvir_max_past_inc_sub_ap[ind_halo][ok_ts]
                temp[i] = np.sum(np.array(halos["mvir"])[ok])
                mvir_max_past_inc_sub_ap[ind_halo][ok_ts] = temp

            # Take maximum past mass for subhaloes
            else:
                temp = mvir_max_past_inc_sub_ap[ind_halo][ok_ts]
                temp[i] = ap_group["mvir_max_past"][ok_ts][i]
                mvir_max_past_inc_sub_ap[ind_halo][ok_ts] = temp

        if timestep == timestep_final:
            
            if "mvir_max_past_inc_sub" in ap_group:
                del ap_group["mvir_max_past_inc_sub"]
                # will need mp version

            ap_group.create_dataset("mvir_max_past_inc_sub",data=mvir_max_past_inc_sub_ap[ind_halo])
            ap_group["mvir_max_past_inc_sub"].attrs['description'] = "virial mass summed over substrctures for central halos, maximum past value of the halo mass, 'mvir' (virial mass) for subhaloes, units: 10^11 Msun"

        # Now repeat but for the main progenitor list
        ok_ts = timestep_tree == mp_group["halo_ts"]

        for i in range(len(mp_group["mvir"][ok_ts])):
            # Do substructure summation if a central halo
            if mp_group["halo_id"][ok_ts][i] == mp_group["host_halo_id"][ok_ts][i]:
                # Add central + subhalo masses
                ok = (mp_group["halo_num"][ok_ts][i] == np.array(halos["host"]))

                # Time to suffer from one of those stupid assignment with booleans python problems
                temp = mvir_max_past_inc_sub_mp[ind_halo][ok_ts]
                temp[i] = np.sum(np.array(halos["mvir"])[ok])
                mvir_max_past_inc_sub_mp[ind_halo][ok_ts] = temp

            # Take maximum past mass for subhaloes
            else:
                temp = mvir_max_past_inc_sub_mp[ind_halo][ok_ts]
                temp[i] = mp_group["mvir_max_past"][ok_ts][i]
                mvir_max_past_inc_sub_mp[ind_halo][ok_ts] = temp

        if timestep == timestep_final:
            
            if "mvir_max_past_inc_sub" in mp_group:
                del mp_group["mvir_max_past_inc_sub"]
                # will need mp version

            mp_group.create_dataset("mvir_max_past_inc_sub",data=mvir_max_past_inc_sub_mp[ind_halo])
            mp_group["mvir_max_past_inc_sub"].attrs['description'] = "virial mass summed over substrctures for central halos, maximum past value of the halo mass, 'mvir' (virial mass) for subhaloes, units: 10^11 Msun"


File.close()

print "time taken was", time.time()-t1
