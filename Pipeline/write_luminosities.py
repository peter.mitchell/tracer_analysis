# This routine reads a subvol file, calculates luminosities for all the stellar particles and writes them back into the subvol file.

import argparse
import sys

parser = argparse.ArgumentParser()
parser.add_argument("-zoom", help = "name of the zoom simulation")
parser.add_argument("-Run", help = "name of the specific run for this zoom simulation")
parser.add_argument("-rd","--RunDir", help="path to simulation directory containing all these output_xxxxx dirs.")
parser.add_argument("-hd","--HaloDir", help="path to directory containing runs of HaloMaker (i.e. containing xx/tree_bricks...)")
parser.add_argument("-td","--TreeDir", help="path to directory containing runs of TreeMaker")
parser.add_argument("-od","--OutDir", help="path to output directory for hdf5 files")
parser.add_argument("-ts_final","--timestep_final", type=int,help="final timestep")
parser.add_argument("-ts_out","--timesteps_out",help="output timestep(s), 'all, 'final', or e.g. '2'")
parser.add_argument("-v","--verbose",help="verbose...")

if len(sys.argv)!=19:
    parser.print_help()
    sys.exit(1)
args = parser.parse_args()

# Set paths
Zoom = args.zoom
Run = args.Run
RunDir = args.RunDir
HaloDir = args.HaloDir
TreeDir = args.TreeDir
OutDir = args.OutDir
timestep_final = args.timestep_final # Timestep for reading in halo brick files and ramses raw output
timesteps_out = args.timesteps_out
verbose_str = args.verbose

first_met_warning = True


# Select photometric bands (rest-frame only at the moment)
#           Top-hat 1500A, B, V, K
band_list = ('UA',"B","V","K")


def Calculate_Luminosity(sm, age, met, band, band_n):

    global first_met_warning

    sps_ages = sps_ages_list[band_n]
    sps_met = sps_met_list[band_n]
    sps_lum = sps_lum_list[band_n]

    # for interpolation beyond SPS grid, print warning and set to grid endpoint value
    bounds_error = False

    # preferred interpolation scheme is to interpolate linearly in age and logarithmically for metallicity
    f = interp1d(sps_ages, np.swapaxes(sps_lum,0,1), kind="linear",axis=0,bounds_error=bounds_error,fill_value=-1)

    sps_lum_fZ = f(age)

    f2 = interp1d(np.log10(sps_met), np.swapaxes(sps_lum_fZ,0,1), kind="linear",axis=0, bounds_error=bounds_error, fill_value =-1)

    temp = f2(np.log10(met))

    ssp_lum_part = np.array(np.diagonal(temp))

    # Deal with endpoint values
    if -1 in ssp_lum_part:
        
        if first_met_warning:
            print "Warning, interpolation beyond tabulated range in metallicity, setting to endpoint values"
            first_met_warning = False

        Zlo = met < sps_met.min()
        
        ssp_lum_part[Zlo] = sps_lum_fZ[:,0][Zlo] # Note this relies on your metallicity grid being sorted properly!

        Zhi = met > sps_met.max()
        ssp_lum_part[Zhi] = sps_lum_fZ[:,-1][Zhi] # Note this relies on your metallicity grid being sorted properly

    #multiply by particle mass
    ssp_lum_part = ssp_lum_part * sm

    return ssp_lum_part # erg s^-1 cm^-2


if "True" in verbose_str:
    verbose = True
else:
    verbose = False

# Write a dump of the stellar, DM and leaf cell data for a given radius around a target halo
import os
import pandas as pd
from halotils import py_halo_utils as hutils
import dm_utils as dm
import star_utils as star
import h5py
import cutils as cut
import numpy as np
import utilities_ramses as ur
from scipy.interpolate import interp1d

if verbose:
    print "writing stellar luminosities for ", Zoom

if timesteps_out == "all":
    timesteps_write = np.arange(2,timestep_final+1)
elif timesteps_out == "final":
    timesteps_write = np.array([timestep_final])
else:
    try:
        timesteps_write = np.array([int(timesteps_out)])
    except:
        print "timesteps_out",timesteps_out,",is not valid, sorry!"
        quit()



# Read SPS data
sps_dir = "/home/cral/mitchell/SPS_Code/"
filename = "bc03_data.hdf5"
file = h5py.File(sps_dir+filename)

sps_ages_list = []
sps_met_list = []
sps_lum_list = []

for band in band_list:
    sps_ages_list.append(file[band+"_band/ages"][:] *1e-6)# SSP ages in Myr
    sps_met_list.append(file[band+"_band/metallicities"][:]) # SSP metallicities in absolute units
    
    sps_lum_list.append(file[band+"_band/flux_AB_magnitude"][:]) # 2d array, 1) metallicity, 2) age
    # SSP luminosity in erg s^-1 cm^-2, expressed in flux units at a distance of 10 parsecs (corresponding to AB magnitude definition)

file.close()

# Open the subvol file
if timesteps_out == "all":
    filename = Zoom + "_" + Run + "_tsf" + str(timestep_final) +".hdf5"
elif timesteps_out == "final":
    filename = Zoom + "_" + Run + "_tsf" + str(timestep_final) +"_ts"+str(timestep_final)+".hdf5"
else:
    try:
        timestep_out = int(timesteps_out)
        filename = Zoom + "_" + Run + "_tsf" + str(timestep_final) +"_ts"+str(timestep_out)+".hdf5"
    except:
        print "timesteps_out",timesteps_out,",is not valid, sorry!"
        quit()
filename = "subvol_" + filename

file = h5py.File(OutDir+filename)

########### Loop over desired timesteps ###################
for i_timestep, timestep in enumerate(timesteps_write):
    if verbose:
        print "timestep", i_timestep, "of", len(timesteps_write)
        
    # convert positions/distances to box units
    # Factor to help convert into box units (note maxime's merger_tree reader has converted halo positions so be careful here!)
    r2kpc_ideal, redshift, little_h, m2Msun,junk,junk = ur.Set_Units(RunDir, timestep, "ideal")
    
    
    
    ############### Write data to disk #############################################

    timestep_group = file["timestep_"+str(int(timestep))]
    star_group = timestep_group["Stellar_particle_data"]

    sm = star_group["m_s"] # code units
    age = star_group["age_s"] # Myr
    met = star_group["met_s"] # Absolute

    sm *= m2Msun # Msun

    # Interpolate across SPS grid to get luminosities for stellar particles
    for band_n, band in enumerate(band_list):


        # If there are > 30000 star particles, split the interpolation function call into parts to avoid huge memory requriment
        if len(sm) < 30000:
            lum = Calculate_Luminosity(sm, age, met, band, band_n)
        else:
            print "lots of stellar particles on this timestep"
            lum = np.zeros_like(sm)
            for i_split in range(1+int(len(sm)/30000.)):
                
                print "computing for split ", i_split , "of", 1+int(len(sm)/30000.)

                min_ind = i_split*30000
                max_ind = min(len(sm)+1, (i_split+1)*30000)

                lum[min_ind:max_ind] = Calculate_Luminosity(sm[min_ind:max_ind], age[min_ind:max_ind], met[min_ind:max_ind], band, band_n)

        # Lumimosity in a given band of stellar particles in erg s^-1 cm^-2, expressed in flux units at a distance of 10 parsec (corresponding to AB magnitude definition)

        if "lum_"+band+"_s" in star_group:
            del star_group["lum_"+band+"_s"]
                    
        star_group.create_dataset("lum_"+band+"_s", data=lum)
        star_group["lum_"+band+"_s"].attrs['description'] = 'flux of stellar particle in the rest-frame ' + band + ' band at distance of 10pc, units: erg s^-1 cm^-2'

file.close()
