# This routine takes all particles / cells and assigns to a single component (e.g. inner halo, subhalo, outer halo).
# The routine is weirdly written/structured because it is a hack of an older version of write_gas_mass_tree_exc_sub.py

import time
t1 = time.time()
t_io = 0
t_cpu = 0
t_chain = 0

import argparse
import sys
import numpy as np

parser = argparse.ArgumentParser()
parser.add_argument("-zoom", help = "name of the zoom simulation")
parser.add_argument("-Run", help = "name of the specific run for this zoom simulation")
parser.add_argument("-rd","--RunDir", help="path to simulation directory containing all these output_xxxxx dirs.")
parser.add_argument("-hd","--HaloDir", help="path to directory containing runs of HaloMaker (i.e. containing xx/tree_bricks...)")
parser.add_argument("-td","--TreeDir", help="path to directory containing runs of TreeMaker")
parser.add_argument("-od","--OutDir", help="path to output directory for hdf5 files")
parser.add_argument("-ts_final","--timestep_final", type=int,help="final timestep")
parser.add_argument("-ts_out","--timesteps_out",help="output timestep(s), 'all, 'final', or e.g. '2'")
parser.add_argument("-rhd","--RHD",help="Radiation hydrodynamics run")
parser.add_argument("-v","--verbose",help="verbose...")

if len(sys.argv)!=21:
    parser.print_help()
    sys.exit(1)
args = parser.parse_args()

# Set paths
Zoom = args.zoom
Run = args.Run
RunDir = args.RunDir
HaloDir = args.HaloDir
TreeDir = args.TreeDir
OutDir = args.OutDir
timestep_final = args.timestep_final # Timestep for reading in halo brick files and ramses raw output                                                      
timesteps_out = args.timesteps_out
RHD = args.RHD
verbose_str = args.verbose

if "True" in verbose_str:
    verbose = True
else:
    verbose = False

timestep_tree_final = timestep_final -1 # At the moment, the tree files store "halo_ts" as timestep -1 (don't ask)

#### Choose the defintion for the subhalo radius #####

subhalo_radius_defn = "rtidal" # For now, valid choices are
# rvir - the virial radius from the halo finder
# rvir_0p5 - half the virial radius from the halo finder

if subhalo_radius_defn == "rvir":
    radius_str = "subhalo radius is defined as the subhalo virial radius from the halo finder"
    print "write_tidal_radius.py: using rvir as subhalo radius"
elif subhalo_radius_defn == "rvir_0p5":
    radius_str = "subhalo radius is defined as half the subhalo virial radius from the halo finder"
    print "write_tidal_radius.py: using 0.5 rvir as subhalo radius"
elif subhalo_radius_defn == "rtidal":
    radius_str = "subhalo radius is defined as the tidal radius, as computed following Binney & Tremaine (more detail needed here)"
    print "write_tidal_radius.py: using rtidal as subhalo radius"
else:
    print "write_tidal_radius.py: Subhalo radius = ", subhalo_radius_defn, " not implemented"
    quit()

################### Selection criteria for different components and their indicies #######################################
# For now the compartmentalization done here is simply to distinguish between inside/outside subhaloes


# Add an extra field that tells you if particles/cells belong to a subhalo with < or > sub_threshold_no dm particles within the user defined subhalo radius
# This is included in case you want to count low mass substructures as part of the diffuse halo or not.
sub_threshold_no = 1000
subhalo_threshold_str = "0 = not part of any halo, 1 = belongs to a satellite subhalo with < "+str(sub_threshold_no) + "dm particles within "+str(subhalo_radius_defn) +", 2 = belongs to a satellite subhalo with > "+str(sub_threshold_no)+", 3= belongs to a central subhalo, but not inside a satellite"


if verbose:
    print 'imports ... '
import dm_utils as dm
from halotils import py_halo_utils as hutils
import cutils as cut
import h5py
import utilities_ramses as ur
import utilities_profiles as up
import neighbour_utils as nu
import pandas as pd

# Read in relevant tree information from the hdf5 file
if timesteps_out == "all":
    filename = Zoom + "_" + Run + "_tsf" + str(timestep_final) +".hdf5"
elif timesteps_out == "final":
    filename = Zoom + "_" + Run + "_tsf" + str(timestep_final) +"_ts"+str(timestep_final)+".hdf5"
else:
    try:
        timestep_out = int(timesteps_out)
        filename = Zoom + "_" + Run + "_tsf" + str(timestep_final) +"_ts"+str(timestep_out)+".hdf5"
    except:
        print "timesteps_out",timesteps_out,",is not valid, sorry!"
        quit()
File = h5py.File(OutDir+filename)

# Open the subvolume file (containing all cell data)
subvol_filename = "subvol_"+filename
subvol_File = h5py.File(OutDir+subvol_filename)

# Check the file with the tree information already exists
if not Zoom in File:
    print "Error: Tree information for ", Zoom, " in ", filename, " cannot be read"
    exit()

zoom_group = File[Zoom]

init_rtidal = True

# Select desired timesteps
if timesteps_out == "all":
    timesteps_tree_out = np.arange(timestep_tree_final)+1
elif timesteps_out == "final":
    timesteps_tree_out = np.array([timestep_tree_final])
else:
    try:
        timesteps_tree_out = np.array([int(timesteps_out)])-1
    except:
        print "timesteps_out",timesteps_out,",is not valid, sorry!"
        quit()

# Loop over timesteps
for i_timestep_tree, timestep_tree in enumerate(timesteps_tree_out):
   
    timestep = timestep_tree + 1 # Stupid mismatch between merger tree files and everything else

    #if timestep < 94:
    #    print "temp hack"
    #    continue

    # Diagnostic of how many subhaloes we can compute a tidal radius for
    count_subs_ok = 0
    count_subs_nok = 0

    #if timestep_tree != timestep_tree_final:
    #    print "skip"
    #    continue
    #if timestep_tree != timestep_tree_final and timestep!=30:
    #    print "skip"
    #    continue

    if verbose:
        print "performing IO at timestep = ", timestep, "of", timestep_final


    if subhalo_radius_defn == "rtidal":
        # On the first timestep, initialise list to store tidal radii each final timestep halo                                                                                                                                    
        if init_rtidal:
            rtidal_ap_list = [] #  gas masses for all progenitors                                                                                                                                                                    
            rtidal_mp_list = [] # gas masses for main progenitors                                                                                                                                                                   
            
    # Read in halo catalogue (to find subhalo positions)
    treefile = "%s%i/tree_bricks%3.3i"%(HaloDir,timestep,timestep)
    nh,ns = hutils.get_nb_halos(treefile) # nhalo, nsubhalo
    halos = hutils.read_all_halos(treefile,nh+ns)
    halo_keys = ('nbpart','hnum','ts','level',
                 'host', 'hostsub', 'nbsub', 'nextsub', 'mass',
                 'x', 'y', 'z', 'vx', 'vy', 'vz', 'Lx', 'Ly', 'Lz', 'r',
                 'a', 'b', 'c', 'ek', 'ep', 'et',
                 'spin', 'rvir', 'mvir', 'tvir', 'cvel', 'rho0', 'r_c')
    halos = pd.DataFrame(columns=halo_keys,data=halos)

    subhalo_host_num_list = np.array(halos.host)
    subhalo_num_list = np.array(halos.hnum)
    subhalo_rvir_list = np.array(halos.rvir)
    subhalo_x_list = np.array(halos.x)
    subhalo_y_list = np.array(halos.y)
    subhalo_z_list = np.array(halos.z)
    subhalo_nbpart_list = np.array(halos.nbpart)

    # Get conversions for different units
    r2kpc, redshift, little_h, m2Msun,rho2msunpkpc3, v2kms = ur.Set_Units(RunDir, timestep)
    boxlen_pkpc = dm.jeje_utils.get_param_real(RunDir,timestep,'boxlen') * r2kpc # Box size in proper kpc, using the correct box size

    factor_subhalo = 3.08e24 / dm.jeje_utils.dp_scale_l # Conversion to code units for the Halo finder unit system (as opposed to the tree unit system)
    
    t_io_temp = time.time()

    ########## Read cell/particle data #########################        
    # Hdf5 group for this timestep (contains cell data)
    ts_group = subvol_File["timestep_"+str(timestep)]

    # Read in DM particle data
    dm_group = ts_group["Dark_matter_particle_data"]
    dx = dm_group["x_dm"][:]
    dy = dm_group["y_dm"][:]
    dz = dm_group["z_dm"][:]
    dmass = dm_group["m_dm"][:]

    # scale dark matter particle masses into Msun
    dmass *= m2Msun

    # Read in star particle data
    star_group = ts_group["Stellar_particle_data"]

    sm = star_group["m_s"][:]
    sx = star_group["x_s"][:]
    sy = star_group["y_s"][:]
    sz = star_group["z_s"][:]

    # Scale stellar masses into solar mass units
    sm *= m2Msun

    cell_group = ts_group["Gas_leaf_cell_data"]

    rho_snap = cell_group["rho_c"][:] # Gas density
    pre_snap = cell_group["pre_c"][:] # Gas pressure
    cx = cell_group["x_c"][:] # Cell coordinates
    cy = cell_group["y_c"][:]
    cz = cell_group["z_c"][:]
    lev = cell_group["lev"][:] # Cell refinement level

    t_io += time.time() - t_io_temp

    if verbose:
        print "Done: performing unit conversions and setting up chains"

    t_cpu_temp = time.time()

    cut.jeje_utils.read_conversion_scales(RunDir,timestep)
    ndensity = rho_snap * cut.jeje_utils.dp_scale_d / cut.jeje_utils.mh * cut.jeje_utils.xh # Number density: Atoms cm^-3
    scale_temk = (cut.jeje_utils.dp_scale_l / cut.jeje_utils.dp_scale_t)**2 * cut.jeje_utils.mh / cut.jeje_utils.kb
    temp = pre_snap/rho_snap * scale_temk # T/mu in K

    # Compute cell size using amr refinement level and box size
    csize = boxlen_pkpc / 2.**lev # proper kpc
    cmass = rho_snap * rho2msunpkpc3 * csize**3 # Msun - gas mass in each cell

    nstar = len(sx); ncell = len(cx); ndm = len(dx)

    # Combine leaf cells, stars and dm particles into a single set of arrays
    cx = np.concatenate([cx, sx, dx])
    cy = np.concatenate([cy, sy, dy])
    cz = np.concatenate([cz, sz, dz])
    cmass = np.concatenate([cmass, sm, dmass])

    t_chain_temp = time.time()

    # Initialise the variable that tracks if these cells/particles are:
    # 0) Outside R_200 of any halo
    # 1) Inside R_tidal of a satellite with fewer than N_thr DM particles
    # 2) Inside R_tidal of a satellite with more than N_thr DM particles
    # 3) Inside R_200 of a central halo but not within R_tidal of a satellite
    c_component_index = np.zeros_like(cx)

    # Build linked-list of cells
    cxyz_min = min(cx.min(),cy.min(),cz.min()) 
    cxyz_max = max(cx.max(),cy.max(),cz.max())
    
    cx_rescale = (cx - cxyz_min) / (cxyz_max-cxyz_min)
    cy_rescale = (cy - cxyz_min) / (cxyz_max-cxyz_min)
    cz_rescale = (cz - cxyz_min) / (cxyz_max-cxyz_min)

    nleafcells = len(cx)
    nchain_cells = 20
    nextincell,firstincell = nu.neighbour_utils.init_chain(cx_rescale,cy_rescale,cz_rescale,nchain_cells)

    t_chain += time.time() - t_chain_temp
        
    # Factor to help convert into box units (note maxime's merger_tree reader has converted halo positions so be careful here!)
    r2kpc_ideal, redshift, little_h, m2Msun,junk,junk = ur.Set_Units(RunDir, timestep, "ideal")
    boxlen_cMpc = dm.jeje_utils.get_param_real(RunDir,timestep,'boxlen') # Box size in box units
    boxlen_cMpc *= r2kpc_ideal
    boxlen_cMpc = boxlen_cMpc * (1+redshift) /1e3 # cMpc (comoving)
    boxlen_cMpc = boxlen_cMpc * little_h # h^-1 cMpc
    factor = 1./boxlen_cMpc

    if verbose:
        print "Done: computing compartments"

    # Have to do this init phase before the loop because we are going to cross-match different final haloes within the loop later
    for halo_group_str in zoom_group:
        if not "Halo_" in halo_group_str:
            continue
        mp_group = zoom_group[halo_group_str+"/main_progenitors"]
        ap_group = zoom_group[halo_group_str+"/all_progenitors"]        
        if subhalo_radius_defn == "rtidal":
            # On the first timestep, initialise list to store tidal radii each final timestep halo
            if init_rtidal:
                rtidal_ap_list.append( np.zeros( len(ap_group["halo_ts"][:] ) ) )
                rtidal_mp_list.append( np.zeros( len(mp_group["halo_ts"][:] ) ) )


    # Loop over final timestep haloes
    ind_halo = -1
    for halo_group_str in zoom_group:

        # Check this is a halo
        if not "Halo_" in halo_group_str:
            continue

        ind_halo += 1# Index like this instead of enumerate so that skipped parts of the for loop don't contribute to the index

        #if verbose:
        #    print "Computing components for final halo", halo_group_str

        mp_group = zoom_group[halo_group_str+"/main_progenitors"]
        ap_group = zoom_group[halo_group_str+"/all_progenitors"]
    
        # Loop over haloes on this timestep
        ok_ts = ap_group["halo_ts"][:]==timestep_tree # haloes on this timestep
        nh_this_ts = len(ap_group["halo_ts"][:][ok_ts]) # Number of halos on this timestep

        for nh in range(nh_this_ts):
            
            #if verbose:
            #    print "\rComputing components for progenitor", nh+1, "of", nh_this_ts, "on this timestep"

            # Find subhaloes of this halo
            if timesteps_out == "final":
                halo_num = ap_group["halo_id"][:][ok_ts][nh]
            else:
                halo_num = ap_group["halo_num"][:][ok_ts][nh]
            is_subhalo = (halo_num == subhalo_host_num_list) & (halo_num != subhalo_num_list)
            n_sub = len(subhalo_host_num_list[is_subhalo])
            
            xh = ap_group["x"][:][ok_ts][nh] * factor + 0.5
            yh = ap_group["y"][:][ok_ts][nh] * factor + 0.5
            zh = ap_group["z"][:][ok_ts][nh] * factor + 0.5
            
            xh_rescale = (xh-cxyz_min)/(cxyz_max-cxyz_min)
            yh_rescale = (yh-cxyz_min)/(cxyz_max-cxyz_min)
            zh_rescale = (zh-cxyz_min)/(cxyz_max-cxyz_min)

            ####### Find cells/particles within R_200 of the host halo ##############
            if ap_group["host_halo_id"][ok_ts][nh] == ap_group["halo_id"][ok_ts][nh]:
                rvirh = ap_group["r200"][ok_ts][nh] / r2kpc
            else:
                continue
            rmax_rescale = rvirh/(cxyz_max-cxyz_min)

            t_chain_temp = time.time()

            # Find cells within rmax of this halo (using chain algorithm)                              
            ncell_in_rmax = nu.neighbour_utils.get_npart_in_sphere_with_chain(RunDir,timestep,cx_rescale,cy_rescale,cz_rescale,xh_rescale,yh_rescale,zh_rescale,rmax_rescale,firstincell,nextincell)
            if ncell_in_rmax > 0:
                ok = nu.neighbour_utils.get_part_indicies_in_sphere_with_chain(RunDir,timestep,cx_rescale,cy_rescale,cz_rescale,xh_rescale,yh_rescale,zh_rescale,rmax_rescale,ncell_in_rmax,firstincell,nextincell)
            else:
                ok = np.zeros_like(cx) < 0

            c_component_index[ok] = 3 # Flag cells/particles within Rvir of a central halo - will get overwritten later if they are inside r_tidal of a satellite
                
            if subhalo_radius_defn == "rtidal":
                ######## Compute the mass profile of the host halo #######################
                # Use r200 for central haloes, skip for satellite haloes                                                       
                mhost_enclosed_f, logslope_host_f, rmax_mhost, rmax_slope = up.Calculate_Host_Profile(cx[ok], cy[ok], cz[ok], cmass[ok], xh, yh, zh,rvirh)

            t_chain += time.time() - t_chain_temp
                
            ########### Compute 
            in_sub = np.zeros_like(cx) < 0
            for isub in range(n_sub):

                #if verbose:
                #    print "testing subhalo", isub,"of",n_sub
               
                # subhalo_x_list etc need to converted into code units here
                sub_x = subhalo_x_list[is_subhalo][isub] * factor_subhalo + 0.5
                sub_y = subhalo_y_list[is_subhalo][isub] * factor_subhalo + 0.5
                sub_z = subhalo_z_list[is_subhalo][isub] * factor_subhalo + 0.5
                
                # computed the radius from the centre of the host to the subhalo
                sub_r = np.sqrt( np.square(sub_x - xh) + np.square(sub_y - yh) + np.square(sub_z - zh))

                # Skip subhaloes that are definitely outside the halo
                if sub_r > rvirh + 2*subhalo_rvir_list[is_subhalo][isub]* factor_subhalo:
                    continue

                sub_x_rescale = (sub_x-cxyz_min)/(cxyz_max-cxyz_min)
                sub_y_rescale = (sub_y-cxyz_min)/(cxyz_max-cxyz_min)
                sub_z_rescale = (sub_z-cxyz_min)/(cxyz_max-cxyz_min)
            
                if subhalo_radius_defn == "rvir":
                    subhalo_rmax = subhalo_rvir_list[is_subhalo][isub]
                elif subhalo_radius_defn == "rvir_0p5":
                    subhalo_rmax = 0.5 * subhalo_rvir_list[is_subhalo][isub]
                elif subhalo_radius_defn == "rtidal":
                    subhalo_rmax = 2 * subhalo_rvir_list[is_subhalo][isub] # In this case, we use this as an efficient preselection
                    sub_rvir = subhalo_rvir_list[is_subhalo][isub] * factor_subhalo # Keep track so we can use this as the subhalo radius when tidal radius calculation fails and we don't have r200 calculated
                    
                    # Check to see if r200 has been computed for this subhalo.
                    # If it has, use r200 instead of rvir for this subhalo.
                    # This will only be used as rtidal if the tidal radius calculation fails (which will happen when the subhalo is far away from the host (i.e. outside))
                    if timesteps_out == "final":
                        sat = subhalo_num_list[is_subhalo][isub] == ap_group["halo_id"][:][ok_ts]
                    else:
                        sat = subhalo_num_list[is_subhalo][isub] == ap_group["halo_num"][:][ok_ts]
                    if ap_group["r200"][:][ok_ts][sat] > 0.0:
                        sub_rvir = ap_group["r200"][:][ok_ts][sat]/r2kpc
                        
                else:
                    print "Error: write_tidal_radius.py: subhalo radius definition", subhalo_radius_defn, "is not defined"
                    quit()

                sub_rvir_rescale = (subhalo_rmax*factor_subhalo)/(cxyz_max-cxyz_min)
            
                t_chain_temp = time.time()
                
                ncell_in_sub_rvir = nu.neighbour_utils.get_npart_in_sphere_with_chain(RunDir,timestep,cx_rescale,cy_rescale,cz_rescale,sub_x_rescale,sub_y_rescale,sub_z_rescale,sub_rvir_rescale,firstincell,nextincell)
                
                if ncell_in_sub_rvir > 0:
                    ind_sub = nu.neighbour_utils.get_part_indicies_in_sphere_with_chain(RunDir,timestep,cx_rescale,cy_rescale,cz_rescale,sub_x_rescale,sub_y_rescale,sub_z_rescale,sub_rvir_rescale,ncell_in_sub_rvir,firstincell,nextincell)

                    t_chain += time.time() - t_chain_temp
    
                    # For subhaloes that are beyond the virial radius, it doesn't make sense to compute a tidal radius. Assign their virial radius instead
                    if subhalo_radius_defn == "rtidal":
                            
                        sub_r_use = min(rmax_mhost,sub_r) # Use endpoint values when interpolating beyond the grid
                        mhost_enclosed = mhost_enclosed_f(sub_r_use) # Host mass enclosed with the subhalo
                        sub_r_use = min(rmax_slope, sub_r)
                        logslope_host_r = logslope_host_f(sub_r_use) # Logarithmic slope of the host mass profile at the radius of the subhalo

                        # Centre the frame on the subhalo
                        cxs = cx[ind_sub] - sub_x; cys = cy[ind_sub] - sub_y; czs = cz[ind_sub] - sub_z
                        # Radius to each cell/particle
                        crs = np.sqrt(np.square(cxs) + np.square(cys) + np.square(czs))

                        # Flag cells that are closer to the host than a subhalo (when the subhalo is outside the virial radius) as NOT in a subhalo
                        # This deals with the cases where there is a major halo merger going on and it is hard to identify whether a particle/cell belongs to one or the other
                        if sub_r > rvirh:
                            r_host = np.sqrt(np.square(xh-cx[ind_sub]) + np.square(yh-cy[ind_sub]) + np.square(zh-cz[ind_sub]))
                            flag_host = r_host < crs
                            nb4 = len(ind_sub)
                            ind_sub = ind_sub[flag_host==False]
                                                            
                            crs = crs[flag_host==False]
                            
                        ind_sub, r_tidal, ndm_in_rtidal = up.Calculate_Rtidal(crs, cmass[ind_sub], ind_sub, sub_x, sub_y, sub_z, sub_r, mhost_enclosed, logslope_host_r, sub_rvir)
                        in_sub[ind_sub] = True
                
                        if ndm_in_rtidal > sub_threshold_no:
                            c_component_index[ind_sub] = 2
                        else:
                            c_component_index[ind_sub] = 1
                            
                        # Search for this subhalo in the all progenitor and main progenitor lists and store if it is present
                        for ind_halo2, halo_group_str2 in enumerate(zoom_group):
                            if not "Halo_" in halo_group_str2:
                                continue

                            mp_group2 = zoom_group[halo_group_str2+"/main_progenitors"]
                            ap_group2 = zoom_group[halo_group_str2+"/all_progenitors"]
                            
                            if timesteps_out == "final":
                                ind_ts_ap = (subhalo_num_list[is_subhalo][isub] == ap_group2["halo_id"][:]) & (ap_group2["halo_ts"][:]==timestep_tree)
                                ind_ts_mp = (subhalo_num_list[is_subhalo][isub] == mp_group2["halo_id"][:]) & (mp_group2["halo_ts"][:]==timestep_tree)
                            else:
                                ind_ts_ap = (subhalo_num_list[is_subhalo][isub] == ap_group2["halo_num"][:]) & (ap_group2["halo_ts"][:]==timestep_tree)
                                ind_ts_mp = (subhalo_num_list[is_subhalo][isub] == mp_group2["halo_num"][:]) & (mp_group2["halo_ts"][:]==timestep_tree)
                            
                            rtidal_ap_list[ind_halo2][ind_ts_ap] = r_tidal # Code units
                            rtidal_mp_list[ind_halo2][ind_ts_mp] = r_tidal # Code units
                        
                            if r_tidal == sub_rvir:
                                count_subs_nok += 1
                            else:
                                count_subs_ok += 1


                    else:
                        in_sub[ind_sub] = True
                        if subhalo_nbpart_list[is_subhalo][isub] > sub_threshold_no:
                            c_component_index[ind_sub] = 2
                        else:
                            c_component_index[ind_sub] = 1

        # On the final timestep, write gas masses to hdf5 file. First check whether we need to delete a previously written entry                                                                                                      
        if timestep == timesteps_tree_out[-1]+1 and subhalo_radius_defn == "rtidal":

            if verbose:
                print "writing tidal radii to disk"

            for dset in ap_group:
                if "r_tidal" == dset:
                    del ap_group[dset]
                    del mp_group[dset]
                
            ap_group.create_dataset("r_tidal", data=rtidal_ap_list[ind_halo])
            mp_group.create_dataset("r_tidal", data=rtidal_mp_list[ind_halo])
            ap_group["r_tidal"].attrs["description"] = "tidal radius of subhaloes (0 for hosts): code units"
            mp_group["r_tidal"].attrs["description"] = "tidal radius of subhaloes (0 for hosts): code units"

    init_rtidal = False

    print "On this timestep, number of reasonable subhaloes for which r_tidal could be computed was", count_subs_ok, ", number of reasonable subhaloes where r_tidal could not be computed was", count_subs_nok
                    
    # Split cells/stars/dark matter particles
    s_component_index = c_component_index[ncell:ncell+nstar]
    d_component_index = c_component_index[ncell+nstar:ncell+nstar+ndm]
    c_component_index = c_component_index[0:ncell]
 
    if "component_index" in star_group:
        del star_group["component_index"]
    if "component_index" in dm_group:
        del dm_group["component_index"]
    if "component_index" in cell_group:
        del cell_group["component_index"]


    # Write indicies to disk
    star_group.create_dataset("component_index", data=s_component_index)
    star_group["component_index"].attrs['description']=subhalo_threshold_str
    
    dm_group.create_dataset("component_index",data=d_component_index)
    dm_group["component_index"].attrs['description']=subhalo_threshold_str

    cell_group.create_dataset("component_index",data=c_component_index)
    cell_group["component_index"].attrs['description']=subhalo_threshold_str
   
    t_cpu += time.time() - t_cpu_temp

subvol_File.close()
File.close()

print "time taken", time.time() - t1
print "io time", t_io
print "cpu time", t_cpu
print "chain cpu time", t_chain
