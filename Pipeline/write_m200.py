import time
t1 = time.time()
t_io = 0
t_cpu = 0
t_chain = 0

import argparse
import sys
import numpy as np

parser = argparse.ArgumentParser()
parser.add_argument("-zoom", help = "name of the zoom simulation")
parser.add_argument("-Run", help = "name of the specific run for this zoom simulation")
parser.add_argument("-rd","--RunDir", help="path to simulation directory containing all these output_xxxxx dirs.")
parser.add_argument("-hd","--HaloDir", help="path to directory containing runs of HaloMaker (i.e. containing xx/tree_bricks...)")
parser.add_argument("-td","--TreeDir", help="path to directory containing runs of TreeMaker")
parser.add_argument("-od","--OutDir", help="path to output directory for hdf5 files")
parser.add_argument("-ts_final","--timestep_final", type=int,help="final timestep")
parser.add_argument("-ts_out","--timesteps_out",help="output timestep(s), 'all, 'final', or e.g. '2'")
parser.add_argument("-rhd","--RHD",help="Radiation hydrodynamics run")
parser.add_argument("-v","--verbose",help="verbose...")

if len(sys.argv)!=21:
    parser.print_help()
    sys.exit(1)
args = parser.parse_args()

# Set paths
Zoom = args.zoom
Run = args.Run
RunDir = args.RunDir
HaloDir = args.HaloDir
TreeDir = args.TreeDir
OutDir = args.OutDir
timestep_final = args.timestep_final # Timestep for reading in halo brick files and ramses raw output                                                      
timesteps_out = args.timesteps_out
verbose_str = args.verbose

if "True" in verbose_str:
    verbose = True
else:
    verbose = False

timestep_tree_final = timestep_final -1 # At the moment, the tree files store "halo_ts" as timestep -1 (don't ask)

# Factor x virial raidus to search for r200
rmax = 2.0


if verbose:
    print 'imports ... '
import dm_utils as dm
import cutils as cut
import h5py
import utilities_ramses as ur
import neighbour_utils as nu
import utilities_cosmology as uc
import utilities_profiles as up

# Read in relevant tree information from the hdf5 file
if timesteps_out == "all":
    filename = Zoom + "_" + Run + "_tsf" + str(timestep_final) +".hdf5"
elif timesteps_out == "final":
    filename = Zoom + "_" + Run + "_tsf" + str(timestep_final) +"_ts"+str(timestep_final)+".hdf5"
else:
    try:
        timestep_out = int(timesteps_out)
        filename = Zoom + "_" + Run + "_tsf" + str(timestep_final) +"_ts"+str(timestep_out)+".hdf5"
    except:
        print "timesteps_out",timesteps_out,",is not valid, sorry!"
        quit()
File = h5py.File(OutDir+filename)

# Open the subvolume file (containing all cell data)
subvol_filename = "subvol_"+filename
subvol_File = h5py.File(OutDir+subvol_filename)

# Check the file with the tree information already exists
if not Zoom in File:
    print "Error: Tree information for ", Zoom, " in ", filename, " cannot be read"
    exit()

zoom_group = File[Zoom]

# Select desired timesteps
if timesteps_out == "all":
    timesteps_tree_out = np.arange(timestep_tree_final)+1
elif timesteps_out == "final":
    timesteps_tree_out = np.array([timestep_tree_final])
else:
    try:
        timesteps_tree_out = np.array([int(timesteps_out)])-1
    except:
        print "timesteps_out",timesteps_out,",is not valid, sorry!"
        quit()

# Loop over timesteps
#for timestep_tree in 1+np.arange(timestep_tree_final):

init_lists = True

for i_timestep_tree, timestep_tree in enumerate(timesteps_tree_out): 
   
    timestep = timestep_tree + 1 # Stupid mismatch between merger tree files and everything else

    #if timestep > 2 and timestep < timestep_final:
    #    print "skipping"
    #    continue

    if verbose:
        print "reading cell/particle data and computing m200 at timestep = ", timestep, "of", timestep_final
    
    # Get conversions for different units
    r2kpc, redshift, little_h, m2Msun,rho2msunpkpc3, v2kms = ur.Set_Units(RunDir, timestep)
    boxlen_pkpc = dm.jeje_utils.get_param_real(RunDir,timestep,'boxlen') * r2kpc # Box size in proper kpc, using the correct box size

    #if timestep < 94:
    #    print "temp hack skipping"
    #    continue
    t_io_temp = time.time()

    ts_group = subvol_File["timestep_"+str(timestep)]

    # Read in DM particle data                                                                                                                                                                                                        
    dm_group = ts_group["Dark_matter_particle_data"]
    dx = dm_group["x_dm"][:]
    dy = dm_group["y_dm"][:]
    dz = dm_group["z_dm"][:]
    dmass = dm_group["m_dm"][:]

    # scale dark matter particle masses into Msun                                                                                                                                                                                     
    dmass *= m2Msun

    # Read in star particle data                                                                                                                                                                                                      
    star_group = ts_group["Stellar_particle_data"]

    sm = star_group["m_s"][:]
    sx = star_group["x_s"][:]
    sy = star_group["y_s"][:]
    sz = star_group["z_s"][:]

    # Scale stellar masses into solar mass units                                                                                                                                                                                      
    sm *= m2Msun

    ########## Read cell data #########################        
    # Hdf5 group for this timestep (contains cell data)
    cell_group = ts_group["Gas_leaf_cell_data"]

    rho_snap = cell_group["rho_c"][:] # Gas density
    cx = cell_group["x_c"][:] # Cell coordinates
    cy = cell_group["y_c"][:]
    cz = cell_group["z_c"][:]
    lev = cell_group["lev"][:] # Cell refinement level

    t_io += time.time() - t_io_temp

    t_cpu_temp = time.time()

    # Compute cell size using amr refinement level and box size
    csize = boxlen_pkpc / 2.**lev # proper kpc
    cmass = rho_snap * rho2msunpkpc3 * csize**3 # Msun - gas mass in each cell

    t_chain_temp = time.time()

    if verbose:
        print "IO done, setting up the chain"

    # Combine leaf cells, stars and dm particles into a single set of arrays
    cx = np.concatenate([cx, sx, dx])
    cy = np.concatenate([cy, sy, dy])
    cz = np.concatenate([cz, sz, dz])
    cmass = np.concatenate([cmass, sm, dmass])

    # Build linked-list of cells
    cxyz_min = min(cx.min(),cy.min(),cz.min()) 
    cxyz_max = max(cx.max(),cy.max(),cz.max()) 
    
    cx_rescale = (cx - cxyz_min) / (cxyz_max-cxyz_min)
    cy_rescale = (cy - cxyz_min) / (cxyz_max-cxyz_min)
    cz_rescale = (cz - cxyz_min) / (cxyz_max-cxyz_min)  

    nleafcells = len(cx)
    nchain_cells = 20
    nextincell,firstincell = nu.neighbour_utils.init_chain(cx_rescale,cy_rescale,cz_rescale,nchain_cells)

    t_chain += time.time() - t_chain_temp

    if verbose:
        print "done"

    # On the first timestep, initialise list to store mass/radii for each final timestep halo
    if init_lists:
        m200_all_progen_list = [] #  gas masses for all progenitors
        m200_main_progen_list = [] # gas masss for main progenitors        
        r200_all_progen_list = [] # leaf cell count for all progenitors
        r200_main_progen_list = [] # leaf cell count for main progenitors
    
    # Factor to help convert into box units (note maxime's merger_tree reader has converted halo positions so be careful here!)
    r2kpc_ideal, redshift, little_h, m2Msun,junk,junk = ur.Set_Units(RunDir, timestep, "ideal")
    boxlen_cMpc = dm.jeje_utils.get_param_real(RunDir,timestep,'boxlen') # Box size in box units
    boxlen_cMpc *= r2kpc_ideal
    boxlen_cMpc = boxlen_cMpc * (1+redshift) /1e3 # cMpc (comoving)
    boxlen_cMpc = boxlen_cMpc * little_h # h^-1 cMpc
    factor = 1./boxlen_cMpc

    # Compute critical density of the universe at this redshift
    dm.jeje_utils.read_conversion_scales(RunDir,timestep)
    omega_0,lambda_0,little_h = dm.jeje_utils.read_cosmo_params(RunDir,timestep)
    rho_crit = uc.Critical_Density(redshift,little_h,omega_0,lambda_0) # Msun pMpc^-3
    rho_crit *= 1e-9

    # Loop over final timestep haloes
    ind_halo = -1
    for halo_group_str in zoom_group:

        # Check this is a halo
        if not "Halo_" in halo_group_str:
            continue

        ind_halo += 1# Index like this instead of enumerate so that skipped parts of the for loop don't contribute to the index

        #if verbose:
        #    print "Computing r200/m200 for halo", halo_group_str
        #    print time.time()-t1

        ap_group = zoom_group[halo_group_str+"/all_progenitors"]
        mp_group = zoom_group[halo_group_str+"/main_progenitors"]

        # On first timestep, initialise arrays to store cold gas masses within list of apertures
        if init_lists:
            m200_all_progen_list.append( np.zeros( len(ap_group["halo_ts"][:]) ))
            m200_main_progen_list.append( np.zeros( len(mp_group["halo_ts"][:]) ))
            r200_all_progen_list.append( np.zeros( len(ap_group["halo_ts"][:]) ))
            r200_main_progen_list.append( np.zeros( len(mp_group["halo_ts"][:]) ))

        # Loop over haloes on this timestep
        ok_ts = ap_group["halo_ts"][:]==timestep_tree # haloes on this timestep
        nh_this_ts = len(ap_group["halo_ts"][:][ok_ts]) # Number of halos on this timestep    

        for nh in range(nh_this_ts):
            
            #if verbose:
            #    print "\rComputing gas masses for progenitor", nh+1, "of", nh_this_ts, "on this timestep"

            # Read halo coordinates
            xh = ap_group["x"][:][ok_ts][nh] * factor + 0.5
            yh = ap_group["y"][:][ok_ts][nh] * factor + 0.5
            zh = ap_group["z"][:][ok_ts][nh] * factor + 0.5
            rvirh = ap_group["rvir"][:][ok_ts][nh] * factor

            
            # Don't bother computing r200/m200 for satellite subhaloes, unless they are outside rvir
            if ap_group["halo_id"][:][ok_ts][nh] != ap_group["host_halo_id"][:][ok_ts][nh]:
                host = ap_group["halo_id"][:][ok_ts] == ap_group["host_halo_id"][:][ok_ts][nh]
                xhost = ap_group["x"][:][ok_ts][host] * factor + 0.5
                yhost = ap_group["y"][:][ok_ts][host] * factor + 0.5
                zhost = ap_group["z"][:][ok_ts][host] * factor + 0.5
                rvirhost = ap_group["rvir"][:][ok_ts][host] * factor

                sephost = np.sqrt( np.square(xh-xhost) + np.square(yh-yhost) + np.square(zh-zhost))
                if sephost < rvirhost:
                    continue


            t_chain_temp = time.time()

            # Rescale halo positions for efficient cell division in chain algorithm
            xh_rescale = (xh-cxyz_min)/(cxyz_max-cxyz_min)
            yh_rescale = (yh-cxyz_min)/(cxyz_max-cxyz_min)
            zh_rescale = (zh-cxyz_min)/(cxyz_max-cxyz_min)
            rmax_rescale = (rmax*rvirh)/(cxyz_max-cxyz_min)
            
            # Find cells within rmax of this halo (using chain algorithm)
            ncell_in_rmax = nu.neighbour_utils.get_npart_in_sphere_with_chain(RunDir,timestep,cx_rescale,cy_rescale,cz_rescale,xh_rescale,yh_rescale,zh_rescale,rmax_rescale,firstincell,nextincell)
           
            if ncell_in_rmax > 0:
                ok = nu.neighbour_utils.get_part_indicies_in_sphere_with_chain(RunDir,timestep,cx_rescale,cy_rescale,cz_rescale,xh_rescale,yh_rescale,zh_rescale,rmax_rescale,ncell_in_rmax,firstincell,nextincell)
            else:
                ok = np.zeros_like(cx) < 0

            # Perform pre-selection (for efficiency reasons) of cells in a sphere of radius rmax around the halo
            cxh = cx[ok]; cyh = cy[ok]; czh = cz[ok]
            cmass_h = cmass[ok]
            
            t_chain += time.time() - t_chain_temp

            # Put the halo at the centre of the cell coordinate system
            cxh = cxh - xh; cyh = cyh - yh; czh = czh -zh
                        
            # Compute the radius to each cell
            crh = np.sqrt(np.square(cxh) + np.square(cyh) + np.square(czh))

            # Compute radial order
            radial_order = np.argsort(crh)
     
            # Compute radially sorted cumulative mass sum
            cmass_enclosed = np.cumsum(cmass_h[radial_order])
            
            # Compute volume enclosed by each radially sorted particle (pkpc^3)
            volume = 4*np.pi/3.0 * (crh[radial_order]*r2kpc)**3
           
            # Compute mean density enclosed by each particle (Msun pkpc^-3)
            density_enclosed = cmass_enclosed / volume

            # Find the closest particle/cell to r200
            ind_r200 = np.argmin(abs(density_enclosed - rho_crit * 200))
            
            # Check if rmax is big enough
            if ind_r200 == len(density_enclosed) -1:
                print "Warning: r200 measurements need a larger rmax than", rmax, " rvir"
                print "For this halo, r200 will just be set to this value"

            r200_i = crh[radial_order][ind_r200] * r2kpc # pkpc
            m200_i = cmass_enclosed[ind_r200] # Msun

            # Stupid fix to deal with array assignment involving multi-indexing with booleans
            assign = r200_all_progen_list[ind_halo][ok_ts]
            assign[nh] = r200_i
            r200_all_progen_list[ind_halo][ok_ts] = assign

            assign = m200_all_progen_list[ind_halo][ok_ts]
            assign[nh] = m200_i
            m200_all_progen_list[ind_halo][ok_ts] = assign

            # If this halo is a main progenitor, assign corresponding r200/m200
            match = ap_group["halo_id"][:][ok_ts][nh] - mp_group["halo_id"][:] == 0
            m200_main_progen_list[ind_halo][match] = m200_i
            r200_main_progen_list[ind_halo][match] = r200_i
            if len(np.array([m200_main_progen_list[ind_halo][match] ])) != 1:
                print "Error, halo_id didn't match up correctly between all_progenitors to main_progenitors"
                exit()
                
        # On the final timestep, write r200/m200 to hdf5 file. First check whether we need to delete a previously written entry
        if timestep == timesteps_tree_out[-1]+1:
                    
            if verbose:
                print "writing m/r200 to disk"
            
            for dset in ap_group:
                if "r200" == dset or "m200" == dset:
                    del ap_group[dset]
                    del mp_group[dset]

            ap_group.create_dataset("r200", data=r200_all_progen_list[ind_halo])
            mp_group.create_dataset("r200", data=r200_main_progen_list[ind_halo])
            ap_group["r200"].attrs["description"] = "radius enclosing 200 times the critical density, units: pkpc"
            mp_group["r200"].attrs["description"] = "radius enclosing 200 times the critical density, units: pkpc"
            
            ap_group.create_dataset("m200", data=m200_all_progen_list[ind_halo])
            mp_group.create_dataset("m200", data=m200_main_progen_list[ind_halo])
            mp_group["m200"].attrs["description"] = "mass (dm+baryons) enlosed by r200, units: Msun"
            mp_group["m200"].attrs["description"] = "mass (dm+baryons) enlosed by r200, units: Msun"
    
    t_cpu += time.time() - t_cpu_temp
    init_lists = False

subvol_File.close()
File.close()

print "time taken", time.time() - t1
print "io time", t_io
print "cpu time", t_cpu
print "chain cpu time", t_chain
