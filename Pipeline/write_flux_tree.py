import time
t1 = time.time()

t_flux_utils = 0
t_thick = 0

import argparse
import sys

parser = argparse.ArgumentParser()
parser.add_argument("-zoom", help = "name of the zoom simulation")
parser.add_argument("-Run", help = "name of the specific run for this zoom simulation")
parser.add_argument("-rd","--RunDir", help="path to simulation directory containing all these output_xxxxx dirs.")
parser.add_argument("-hd","--HaloDir", help="path to directory containing runs of HaloMaker (i.e. containing xx/tree_bricks...)")
parser.add_argument("-td","--TreeDir", help="path to directory containing runs of TreeMaker")
parser.add_argument("-od","--OutDir", help="path to output directory for hdf5 files")
parser.add_argument("-ts_final","--timestep_final", type=int,help="final timestep")
parser.add_argument("-ts_out","--timesteps_out",help="output timestep(s), 'all, 'final', or e.g. '2'")
parser.add_argument("-rhd","--RHD",help="Radiation hydrodynamics run")
parser.add_argument("-v","--verbose",help="verbose...")

if len(sys.argv)!=21:
    parser.print_help()
    sys.exit(1)
args = parser.parse_args()

# Set paths
Zoom = args.zoom
Run = args.Run
RunDir = args.RunDir
HaloDir = args.HaloDir
TreeDir = args.TreeDir
OutDir = args.OutDir
timestep_final = args.timestep_final # Timestep for reading in halo brick files and ramses raw output
timesteps_out = args.timesteps_out
RHD = args.RHD
verbose_str = args.verbose

if "True" in verbose_str:
    verbose = True
else:
    verbose = False

if "True" in RHD:
    RHD = True
else:
    RHD = False
    
if verbose:
    print "Computing fluxes for", Zoom, Run

timestep_tree_final = timestep_final -1 # At the moment, the tree files store "halo_ts" as timestep -1 (don't ask)

# If true, cells are only considered part of a subhalo (and hence flagged in the maps and used to compute radial profiles with/without subhaloes) 
# if the subhalo contains more than a threshold number (fiducial value 1000) of dm particles (as computed by the halofinder)
exclude_below_threshold = False

if verbose:
    print 'imports ... '
import dm_utils as dm
import cutils as cut
from halotils import py_halo_utils as hutils
import numpy as np
from scipy import spatial
import h5py
import flux_utils
import utilities_ramses as ur
import neighbour_utils as nu

# Read in relevant tree information from the hdf5 file
if timesteps_out == "all":
    filename = Zoom + "_" + Run + "_tsf" + str(timestep_final) +".hdf5"
elif timesteps_out == "final":
    filename = Zoom + "_" + Run + "_tsf" + str(timestep_final) +"_ts"+str(timestep_final)+".hdf5"
else:
    try:
        timestep_out = int(timesteps_out)
        filename = Zoom + "_" + Run + "_tsf" + str(timestep_final) +"_ts"+str(timestep_out)+".hdf5"
    except:
        print "timesteps_out",timesteps_out,",is not valid, sorry!"
        quit()
File = h5py.File(OutDir+filename)


# Open the subvolume file (containing all cell data)
subvol_filename = "subvol_"+filename
print "reading subvol file", OutDir+subvol_filename
subvol_File = h5py.File(OutDir+subvol_filename)

# Check the file with the tree information already exists
if not Zoom in File:
    print "Error: Tree information for ", Zoom, " in ", filename, " cannot be read"
    exit()

zoom_group = File[Zoom]

# Select desired timesteps
if timesteps_out == "all":
    timesteps_tree_out = np.arange(timestep_tree_final)+1
elif timesteps_out == "final":
    timesteps_tree_out = np.array([timestep_tree_final])
else:
    try:
        timesteps_tree_out = np.array([int(timesteps_out)])-1
    except:
        print "timesteps_out",timesteps_out,",is not valid, sorry!"
        quit()

first_step = True

# Loop over timesteps
for i_timestep_tree, timestep_tree in enumerate(timesteps_tree_out):
    timestep = timestep_tree + 1 # Stupid mismatch between merger tree files and everything else

    #if timestep < 94:
    #    if i_timestep_tree == 0:
    #        print "temp hack"
    #    continue

    if verbose:
        print "reading cell data at timestep = ", timestep, "of", timestep_final

    #if timestep < timestep_final and timestep > 2:
    #    print "skipping", timestep
    #    continue
    
    # Get conversions for different units
    r2kpc, redshift, little_h, m2Msun,rho2msunpkpc3,v2kms = ur.Set_Units(RunDir, timestep)
    boxlen_pkpc = dm.jeje_utils.get_param_real(RunDir,timestep,'boxlen') * r2kpc # Box size in proper kpc, using the correct box size

    ########## Read cell data #########################        
    # Hdf5 group for this timestep (contains cell data)
    ts_group = subvol_File["timestep_"+str(timestep)]
    cell_group = ts_group["Gas_leaf_cell_data"]

    rho_snap = cell_group["rho_c"][:] # Gas density
    pre_snap = cell_group["pre_c"][:] # Gas pressure
    cx = cell_group["x_c"][:] # Cell coordinates
    cy = cell_group["y_c"][:]
    cz = cell_group["z_c"][:]
    cvx = cell_group["vx_c"][:]
    cvy = cell_group["vy_c"][:]
    cvz = cell_group["vz_c"][:]
    lev = cell_group["lev"][:] # Cell refinement level
    cmet = cell_group["met_c"][:]

    if RHD:
        xH1 = cell_group["xH1"][:]
        xHe1 =cell_group["xHe1"][:]
        xHe2 = cell_group["xHe2"][:]
    
    c_component_index = cell_group["component_index"][:] # Flag to indicate whether inside/outside a subhalo

    # Only count cells as in a subhalo if the subhalo contains > a threshold number (fiducial value=1000) of dm particles in the halo finder
    if exclude_below_threshold:
        in_sub = c_component_index == 2
    else:
        in_sub = (c_component_index > 0) & (c_component_index < 3)

    if verbose:
        print "done, now computing fluxes"

    # Compute cell size using amr refinement level and box size
    csize = boxlen_pkpc / 2.**lev # proper kpc
    cmass = rho_snap * rho2msunpkpc3 * csize**3 # Msun - gas mass in each cell

    # Compute temperature over effective molecular weight in Kelvin
    cut.jeje_utils.read_conversion_scales(RunDir,timestep)
    scale_temk = (cut.jeje_utils.dp_scale_l / cut.jeje_utils.dp_scale_t)**2 * cut.jeje_utils.mh / cut.jeje_utils.kb
    temp_mu = pre_snap/rho_snap * scale_temk # T/mu in K
    
    # Compute actual temperature and neutral fraction
    if RHD:
        xhi = (1-xH1)# Hydrogen neutral fraction
        X_frac=0.76
        mu = 1./( X_frac*(1.+xH1) + 0.25*(1.-X_frac)*(1.+xHe1+2.*xHe2) )
    else:
        mu, xhi = cut.cell_utils.ramses_get_mu_xhi(RunDir,timestep,rho_snap, pre_snap, True)
    temp = temp_mu * mu

    # Build linked-list of cells (for neighbour finding later)
    cxyz_min = min(cx.min(),cy.min(),cz.min())
    cxyz_max = max(cx.max(),cy.max(),cz.max())

    cx_rescale = (cx - cxyz_min) / (cxyz_max-cxyz_min)
    cy_rescale = (cy - cxyz_min) / (cxyz_max-cxyz_min)
    cz_rescale = (cz - cxyz_min) / (cxyz_max-cxyz_min)

    nchain_cells = 20
    nextincell,firstincell = nu.neighbour_utils.init_chain(cx_rescale,cy_rescale,cz_rescale,nchain_cells)
    # Factor to help convert into box units (note maxime's merger_tree reader has converted halo positions so be careful here!)
    r2kpc_ideal, redshift, little_h, m2Msun,junk,junk = ur.Set_Units(RunDir, timestep, "ideal")
    boxlen_cMpc = dm.jeje_utils.get_param_real(RunDir,timestep,'boxlen') # Box size in box units
    boxlen_cMpc *= r2kpc_ideal
    boxlen_cMpc = boxlen_cMpc * (1+redshift) /1e3 # cMpc (comoving)
    boxlen_cMpc = boxlen_cMpc * little_h # h^-1 cMpc
    factor = 1./boxlen_cMpc

    # Loop over final timestep haloes
    for halo_group_str in zoom_group:

        # Only compute fluxes for the main halo
        if not "_main" in halo_group_str:
            continue
        
        if verbose:
            print "Computing fluxes for progenitors of halo", halo_group_str

        halo_group = zoom_group[halo_group_str]

        flux_group_str = "fluxes"
        # On the first timestep, initialise the flux groups in the output hdf5 file
        if first_step:
            first_step = False
            flux_group_path = Zoom + "/" + halo_group_str + "/fluxes"
            # Remove a previous version of the flux group if one exists
            if flux_group_path in File:
                flux_group_ok = False
                while not flux_group_ok:
                    if flux_group_path in File:
                        try:
                            del File[flux_group_path]
                            print "Deleted previous flux map data"
                        except:
                            print "Warning: failed to delete previous flux map data"
                            print "Temporary workaround is to create a new flux group called", flux_group_path+"_i"
                            flux_group_path += "_i"
                            flux_group_str += "_i"
                            continue
                    flux_group_ok = True

            flux_group = halo_group.create_group(flux_group_str)
        else:
            flux_group = halo_group[flux_group_str]

        flux_group_ts = flux_group.create_group("timestep_"+str(timestep))
        
        mp_group = zoom_group[halo_group_str+"/main_progenitors"]

        # Identify the main progenitor at this timestep
        ok_ts = np.array(mp_group["halo_ts"])==timestep_tree # haloes on this timestep
        
        xh = mp_group["x"][ok_ts] * factor + 0.5
        yh = mp_group["y"][ok_ts] * factor + 0.5
        zh = mp_group["z"][ok_ts] * factor + 0.5
        
        # Use r200 for central haloes, and rvir (for now) for satellite haloes                                                                                                                                                    
        if mp_group["host_halo_id"][ok_ts] == mp_group["halo_id"][ok_ts]:
            rvirh = mp_group["r200"][ok_ts] / r2kpc
        else:
            rvirh = mp_group["rvir"][ok_ts] * factor

        if len(xh) == 0:
            continue

        vxh = mp_group["vx"][ok_ts] / v2kms # Halo velocity in Code units
        vyh = mp_group["vy"][ok_ts] / v2kms
        vzh = mp_group["vz"][ok_ts] / v2kms

        # Select cells in the vicinity of the halo (using a chain algorithm)
        rmax = rvirh*1.5
        
        # Rescale halo positions for efficient cell division in chain algorithm                                                                         
        xh_rescale = (xh-cxyz_min)/(cxyz_max-cxyz_min)
        yh_rescale = (yh-cxyz_min)/(cxyz_max-cxyz_min)
        zh_rescale = (zh-cxyz_min)/(cxyz_max-cxyz_min)
        rmax_rescale = rmax/(cxyz_max-cxyz_min)

        ncell_in_rmax = nu.neighbour_utils.get_npart_in_sphere_with_chain(RunDir,timestep,cx_rescale,cy_rescale,cz_rescale,xh_rescale,yh_rescale,zh_rescale,rmax_rescale,firstincell,nextincell)
        if ncell_in_rmax > 0:
            select = nu.neighbour_utils.get_part_indicies_in_sphere_with_chain(RunDir,timestep,cx_rescale,cy_rescale,cz_rescale,xh_rescale,yh_rescale,zh_rescale,rmax_rescale,ncell_in_rmax,firstincell,nextincell)
        else:
            select = np.zeros_like(cx) < 0
        cx_h = cx[select]; cy_h = cy[select]; cz_h = cz[select]
        rho_snap_h = rho_snap[select] * rho2msunpkpc3 # Msun pkpc^-3
        cvx_h = cvx[select]; cvy_h = cvy[select]; cvz_h = cvz[select]
        csize_h = csize[select]; cmass_h = cmass[select]
        temp_mu_h = temp_mu[select]; temp_h = temp[select]; mu_h = mu[select]
        in_sub_h = in_sub[select]; xhi_h = xhi[select]
        cmet_h = cmet[select]

        # Put the halo at the centre of the particle coordinate system
        cx_h = cx_h - xh; cy_h = cy_h - yh; cz_h = cz_h -zh
        cvx_h = cvx_h - vxh; cvy_h = cvy_h - vyh; cvz_h = cvz_h - vzh

        # Compute the radius to each cell
        cr_h = np.sqrt(np.square(cx_h) + np.square(cy_h) + np.square(cz_h))
        
        # Compute dot product of cell "velocity with radial unit vector
        cv_radial_h = np.sum(np.array([cvx_h,cvy_h,cvz_h]) * np.array([cx_h,cy_h,cz_h]),axis=0) / cr_h

        # Compute the scalar velocity in the halo frame
        vscalar_h = np.sqrt( np.square(cvx_h) + np.square(cvy_h) + np.square(cvz_h))

        # First step. Compute integrated fluxes for fine radius bins

        r_surface_fine = np.arange(0.1,1.1,0.01)

        # Brace yourself...
        dmdt_inflow = np.zeros_like(r_surface_fine); dmdt_outflow = np.zeros_like(r_surface_fine)
        dmdt_inflow_exc_sub = np.zeros_like(r_surface_fine); dmdt_outflow_exc_sub = np.zeros_like(r_surface_fine)
        dmdr_inflow = np.zeros_like(r_surface_fine); dmdr_outflow = np.zeros_like(r_surface_fine)
        dmdr_inflow_exc_sub = np.zeros_like(r_surface_fine); dmdr_outflow_exc_sub = np.zeros_like(r_surface_fine)
        cv_radial_mw_inflow = np.zeros_like(r_surface_fine); cv_radial_mw_outflow = np.zeros_like(r_surface_fine)
        cv_radial_mw_inflow_exc_sub = np.zeros_like(r_surface_fine); cv_radial_mw_outflow_exc_sub = np.zeros_like(r_surface_fine)
        cv_radial_fw_inflow = np.zeros_like(r_surface_fine); cv_radial_fw_outflow = np.zeros_like(r_surface_fine)
        cv_radial_fw_inflow_exc_sub = np.zeros_like(r_surface_fine); cv_radial_fw_outflow_exc_sub = np.zeros_like(r_surface_fine)
        dEkrdr_inflow = np.zeros_like(r_surface_fine); dEkrdr_outflow = np.zeros_like(r_surface_fine)
        dEkrdr_inflow_exc_sub = np.zeros_like(r_surface_fine); dEkrdr_outflow_exc_sub = np.zeros_like(r_surface_fine)
        dEkdr_inflow = np.zeros_like(r_surface_fine); dEkdr_outflow = np.zeros_like(r_surface_fine)
        dEkdr_inflow_exc_sub = np.zeros_like(r_surface_fine); dEkdr_outflow_exc_sub = np.zeros_like(r_surface_fine)
        dUdr_inflow = np.zeros_like(r_surface_fine); dUdr_outflow = np.zeros_like(r_surface_fine)
        dUdr_inflow_exc_sub = np.zeros_like(r_surface_fine); dUdr_outflow_exc_sub = np.zeros_like(r_surface_fine)

        dmdt_inflow_n = np.zeros_like(r_surface_fine); dmdt_outflow_n = np.zeros_like(r_surface_fine)
        dmdt_inflow_n_exc_sub = np.zeros_like(r_surface_fine); dmdt_outflow_n_exc_sub = np.zeros_like(r_surface_fine)
        dmdr_inflow_n = np.zeros_like(r_surface_fine); dmdr_outflow_n = np.zeros_like(r_surface_fine)
        dmdr_inflow_n_exc_sub = np.zeros_like(r_surface_fine); dmdr_outflow_n_exc_sub = np.zeros_like(r_surface_fine)
        cv_radial_mw_inflow_n = np.zeros_like(r_surface_fine); cv_radial_mw_outflow_n = np.zeros_like(r_surface_fine)
        cv_radial_mw_inflow_n_exc_sub = np.zeros_like(r_surface_fine); cv_radial_mw_outflow_n_exc_sub = np.zeros_like(r_surface_fine)
        cv_radial_fw_inflow_n = np.zeros_like(r_surface_fine); cv_radial_fw_outflow_n = np.zeros_like(r_surface_fine)
        cv_radial_fw_inflow_n_exc_sub = np.zeros_like(r_surface_fine); cv_radial_fw_outflow_n_exc_sub = np.zeros_like(r_surface_fine)
        dEkrdr_inflow_n = np.zeros_like(r_surface_fine); dEkrdr_outflow_n = np.zeros_like(r_surface_fine)
        dEkrdr_inflow_n_exc_sub = np.zeros_like(r_surface_fine); dEkrdr_outflow_n_exc_sub = np.zeros_like(r_surface_fine)
        dEkdr_inflow_n = np.zeros_like(r_surface_fine); dEkdr_outflow_n = np.zeros_like(r_surface_fine)
        dEkdr_inflow_n_exc_sub = np.zeros_like(r_surface_fine); dEkdr_outflow_n_exc_sub = np.zeros_like(r_surface_fine)
        dUdr_inflow_n = np.zeros_like(r_surface_fine); dUdr_outflow_n = np.zeros_like(r_surface_fine)
        dUdr_inflow_n_exc_sub = np.zeros_like(r_surface_fine); dUdr_outflow_n_exc_sub = np.zeros_like(r_surface_fine)

        dmdt_inflow_i = np.zeros_like(r_surface_fine); dmdt_outflow_i = np.zeros_like(r_surface_fine)
        dmdt_inflow_i_exc_sub = np.zeros_like(r_surface_fine); dmdt_outflow_i_exc_sub = np.zeros_like(r_surface_fine)
        dmdr_inflow_i = np.zeros_like(r_surface_fine); dmdr_outflow_i = np.zeros_like(r_surface_fine)
        dmdr_inflow_i_exc_sub = np.zeros_like(r_surface_fine); dmdr_outflow_i_exc_sub = np.zeros_like(r_surface_fine)
        cv_radial_mw_inflow_i = np.zeros_like(r_surface_fine); cv_radial_mw_outflow_i = np.zeros_like(r_surface_fine)
        cv_radial_mw_inflow_i_exc_sub = np.zeros_like(r_surface_fine); cv_radial_mw_outflow_i_exc_sub = np.zeros_like(r_surface_fine)
        cv_radial_fw_inflow_i = np.zeros_like(r_surface_fine); cv_radial_fw_outflow_i = np.zeros_like(r_surface_fine)
        cv_radial_fw_inflow_i_exc_sub = np.zeros_like(r_surface_fine); cv_radial_fw_outflow_i_exc_sub = np.zeros_like(r_surface_fine)
        dEkrdr_inflow_i = np.zeros_like(r_surface_fine); dEkrdr_outflow_i = np.zeros_like(r_surface_fine)
        dEkrdr_inflow_i_exc_sub = np.zeros_like(r_surface_fine); dEkrdr_outflow_i_exc_sub = np.zeros_like(r_surface_fine)
        dEkdr_inflow_i = np.zeros_like(r_surface_fine); dEkdr_outflow_i = np.zeros_like(r_surface_fine)
        dEkdr_inflow_i_exc_sub = np.zeros_like(r_surface_fine); dEkdr_outflow_i_exc_sub = np.zeros_like(r_surface_fine)
        dUdr_inflow_i = np.zeros_like(r_surface_fine); dUdr_outflow_i = np.zeros_like(r_surface_fine)
        dUdr_inflow_i_exc_sub = np.zeros_like(r_surface_fine); dUdr_outflow_i_exc_sub = np.zeros_like(r_surface_fine)

        dmdt_inflow_wi = np.zeros_like(r_surface_fine); dmdt_outflow_wi = np.zeros_like(r_surface_fine)
        dmdt_inflow_wi_exc_sub = np.zeros_like(r_surface_fine); dmdt_outflow_wi_exc_sub = np.zeros_like(r_surface_fine)
        dmdr_inflow_wi = np.zeros_like(r_surface_fine); dmdr_outflow_wi = np.zeros_like(r_surface_fine)
        dmdr_inflow_wi_exc_sub = np.zeros_like(r_surface_fine); dmdr_outflow_wi_exc_sub = np.zeros_like(r_surface_fine)
        cv_radial_mw_inflow_wi = np.zeros_like(r_surface_fine); cv_radial_mw_outflow_wi = np.zeros_like(r_surface_fine)
        cv_radial_mw_inflow_wi_exc_sub = np.zeros_like(r_surface_fine); cv_radial_mw_outflow_wi_exc_sub = np.zeros_like(r_surface_fine)
        cv_radial_fw_inflow_wi = np.zeros_like(r_surface_fine); cv_radial_fw_outflow_wi = np.zeros_like(r_surface_fine)
        cv_radial_fw_inflow_wi_exc_sub = np.zeros_like(r_surface_fine); cv_radial_fw_outflow_wi_exc_sub = np.zeros_like(r_surface_fine)
        dEkrdr_inflow_wi = np.zeros_like(r_surface_fine); dEkrdr_outflow_wi = np.zeros_like(r_surface_fine)
        dEkrdr_inflow_wi_exc_sub = np.zeros_like(r_surface_fine); dEkrdr_outflow_wi_exc_sub = np.zeros_like(r_surface_fine)
        dEkdr_inflow_wi = np.zeros_like(r_surface_fine); dEkdr_outflow_wi = np.zeros_like(r_surface_fine)
        dEkdr_inflow_wi_exc_sub = np.zeros_like(r_surface_fine); dEkdr_outflow_wi_exc_sub = np.zeros_like(r_surface_fine)
        dUdr_inflow_wi = np.zeros_like(r_surface_fine); dUdr_outflow_wi = np.zeros_like(r_surface_fine)
        dUdr_inflow_wi_exc_sub = np.zeros_like(r_surface_fine); dUdr_outflow_wi_exc_sub = np.zeros_like(r_surface_fine)

        # Metal fluxes in various phases
        dmdt_inflow_Zn = np.zeros_like(r_surface_fine); dmdt_outflow_Zn = np.zeros_like(r_surface_fine)
        dmdt_inflow_Zn_exc_sub = np.zeros_like(r_surface_fine); dmdt_outflow_Zn_exc_sub = np.zeros_like(r_surface_fine)
        dmdr_inflow_Zn = np.zeros_like(r_surface_fine); dmdr_outflow_Zn = np.zeros_like(r_surface_fine)
        dmdr_inflow_Zn_exc_sub = np.zeros_like(r_surface_fine); dmdr_outflow_Zn_exc_sub = np.zeros_like(r_surface_fine)       
        dmdt_inflow_Zi = np.zeros_like(r_surface_fine); dmdt_outflow_Zi = np.zeros_like(r_surface_fine)
        dmdt_inflow_Zi_exc_sub = np.zeros_like(r_surface_fine); dmdt_outflow_Zi_exc_sub = np.zeros_like(r_surface_fine)
        dmdr_inflow_Zi = np.zeros_like(r_surface_fine); dmdr_outflow_Zi = np.zeros_like(r_surface_fine)
        dmdr_inflow_Zi_exc_sub = np.zeros_like(r_surface_fine); dmdr_outflow_Zi_exc_sub = np.zeros_like(r_surface_fine)       
        dmdt_inflow_Zwi = np.zeros_like(r_surface_fine); dmdt_outflow_Zwi = np.zeros_like(r_surface_fine)
        dmdt_inflow_Zwi_exc_sub = np.zeros_like(r_surface_fine); dmdt_outflow_Zwi_exc_sub = np.zeros_like(r_surface_fine)
        dmdr_inflow_Zwi = np.zeros_like(r_surface_fine); dmdr_outflow_Zwi = np.zeros_like(r_surface_fine)
        dmdr_inflow_Zwi_exc_sub = np.zeros_like(r_surface_fine); dmdr_outflow_Zwi_exc_sub = np.zeros_like(r_surface_fine)       

        t_flux_utils_temp = time.time()

        if verbose:
            print "computing radial flux profile"

            
        XH=0.76 # Hydrogen mass fraction
        # Want to compute radial profiles for total gas, neutral hydrogen, ionised hydrogen and warm-ionised hydrogen (<10^4.5K)
        x_warm_h = np.zeros_like(rho_snap_h)
        x_warm_h[temp_h<10**4.5] = 1.0
        density_l = [rho_snap_h, rho_snap_h * XH * xhi_h, rho_snap_h * XH * (1.0-xhi_h), rho_snap_h * XH * (1.0-xhi_h)*x_warm_h]
        # Want to compute metal profiles for neutral metals, ionised metals and warm-ionised metals
        Zdensity_l = [ rho_snap_h*cmet_h * xhi_h, rho_snap_h*cmet_h *(1.0-xhi_h), rho_snap_h*cmet_h * (1.0-xhi_h)*x_warm_h]
        for n in range(len(r_surface_fine)):
            dmdt_inflow_l, dmdt_outflow_l, dmdt_inflow_exc_sub_l, dmdt_outflow_exc_sub_l, \
                dmdr_inflow_l, dmdr_outflow_l, dmdr_inflow_exc_sub_l, dmdr_outflow_exc_sub_l, \
                cv_radial_mw_inflow_l, cv_radial_mw_outflow_l, cv_radial_mw_inflow_exc_sub_l, cv_radial_mw_outflow_exc_sub_l, \
                cv_radial_fw_inflow_l, cv_radial_fw_outflow_l, cv_radial_fw_inflow_exc_sub_l, cv_radial_fw_outflow_exc_sub_l, \
                dUdr_inflow_l, dUdr_outflow_l, dUdr_inflow_exc_sub_l, dUdr_outflow_exc_sub_l, \
                dEkrdr_inflow_l, dEkrdr_outflow_l, dEkrdr_inflow_exc_sub_l, dEkrdr_outflow_exc_sub_l, \
                dEkdr_inflow_l, dEkdr_outflow_l, dEkdr_inflow_exc_sub_l, dEkdr_outflow_exc_sub_l, \
                dmdt_inflow_Z_l, dmdt_outflow_Z_l, dmdt_inflow_exc_sub_Z_l, dmdt_outflow_exc_sub_Z_l, \
                dmdr_inflow_Z_l, dmdr_outflow_Z_l, dmdr_inflow_exc_sub_Z_l, dmdr_outflow_exc_sub_Z_l \
                = flux_utils.Compute_Flux_Surface_Uniform_Sampling(r_surface_fine[n],rvirh, csize_h, cr_h, cx_h, cy_h, cz_h, 
                                                                   density_l, Zdensity_l, temp_h, mu_h, cv_radial_h, vscalar_h, 
                                                                   in_sub_h, RunDir, timestep)

            # Possibly the most magnificent code that has ever been written
            dmdt_inflow[n] = dmdt_inflow_l[0]; dmdt_inflow_n[n] = dmdt_inflow_l[1]; dmdt_inflow_i[n] = dmdt_inflow_l[2]; dmdt_inflow_wi[n] = dmdt_inflow_l[3]
            dmdt_inflow_exc_sub[n] = dmdt_inflow_exc_sub_l[0]; dmdt_inflow_n_exc_sub[n] = dmdt_inflow_exc_sub_l[1]; dmdt_inflow_i_exc_sub[n] = dmdt_inflow_exc_sub_l[2]; dmdt_inflow_wi_exc_sub[n] = dmdt_inflow_exc_sub_l[3]
            dmdt_outflow[n] = dmdt_outflow_l[0]; dmdt_outflow_n[n] = dmdt_outflow_l[1]; dmdt_outflow_i[n] = dmdt_outflow_l[2]; dmdt_outflow_wi[n] = dmdt_outflow_l[3]
            dmdt_outflow_exc_sub[n] = dmdt_outflow_exc_sub_l[0]; dmdt_outflow_n_exc_sub[n] = dmdt_outflow_exc_sub_l[1]; dmdt_outflow_i_exc_sub[n] = dmdt_outflow_exc_sub_l[2]; dmdt_outflow_wi_exc_sub[n] = dmdt_outflow_exc_sub_l[3]

            dmdr_inflow[n] = dmdr_inflow_l[0]; dmdr_inflow_n[n] = dmdr_inflow_l[1]; dmdr_inflow_i[n] = dmdr_inflow_l[2]; dmdr_inflow_wi[n] = dmdr_inflow_l[3]
            dmdr_inflow_exc_sub[n] = dmdr_inflow_exc_sub_l[0]; dmdr_inflow_n_exc_sub[n] = dmdr_inflow_exc_sub_l[1]; dmdr_inflow_i_exc_sub[n] = dmdr_inflow_exc_sub_l[2]; dmdr_inflow_wi_exc_sub[n] = dmdr_inflow_exc_sub_l[3]
            dmdr_outflow[n] = dmdr_outflow_l[0]; dmdr_outflow_n[n] = dmdr_outflow_l[1]; dmdr_outflow_i[n] = dmdr_outflow_l[2]; dmdr_outflow_wi[n] = dmdr_outflow_l[3]
            dmdr_outflow_exc_sub[n] = dmdr_outflow_exc_sub_l[0]; dmdr_outflow_n_exc_sub[n] = dmdr_outflow_exc_sub_l[1]; dmdr_outflow_i_exc_sub[n] = dmdr_outflow_exc_sub_l[2]; dmdr_outflow_wi_exc_sub[n] = dmdr_outflow_exc_sub_l[3]

            dmdt_inflow_Zn[n] = dmdt_inflow_Z_l[0]; dmdt_inflow_Zi[n] = dmdt_inflow_Z_l[1]; dmdt_inflow_Zwi[n] = dmdt_inflow_Z_l[2]
            dmdt_inflow_Zn_exc_sub[n] = dmdt_inflow_exc_sub_Z_l[0]; dmdt_inflow_Zi_exc_sub[n] = dmdt_inflow_exc_sub_Z_l[1]; dmdt_inflow_Zwi_exc_sub[n] = dmdt_inflow_exc_sub_Z_l[2]
            dmdt_outflow_Zn[n] = dmdt_outflow_Z_l[0]; dmdt_outflow_Zi[n] = dmdt_outflow_Z_l[1]; dmdt_outflow_Zwi[n] = dmdt_outflow_Z_l[2]
            dmdt_outflow_Zn_exc_sub[n] = dmdt_outflow_exc_sub_Z_l[0]; dmdt_outflow_Zi_exc_sub[n] = dmdt_outflow_exc_sub_Z_l[1]; dmdt_outflow_Zwi_exc_sub[n] = dmdt_outflow_exc_sub_Z_l[2]

            dmdr_inflow_Zn[n] = dmdr_inflow_Z_l[0]; dmdr_inflow_Zi[n] = dmdr_inflow_Z_l[1]; dmdr_inflow_Zwi[n] = dmdr_inflow_Z_l[2]
            dmdr_inflow_Zn_exc_sub[n] = dmdr_inflow_exc_sub_Z_l[0]; dmdr_inflow_Zi_exc_sub[n] = dmdr_inflow_exc_sub_Z_l[1]; dmdr_inflow_Zwi_exc_sub[n] = dmdr_inflow_exc_sub_Z_l[2]
            dmdr_outflow_Zn[n] = dmdr_outflow_Z_l[0]; dmdr_outflow_Zi[n] = dmdr_outflow_Z_l[1]; dmdr_outflow_Zwi[n] = dmdr_outflow_Z_l[2]
            dmdr_outflow_Zn_exc_sub[n] = dmdr_outflow_exc_sub_Z_l[0]; dmdr_outflow_Zi_exc_sub[n] = dmdr_outflow_exc_sub_Z_l[1]; dmdr_outflow_Zwi_exc_sub[n] = dmdr_outflow_exc_sub_Z_l[2]


            cv_radial_mw_inflow[n] = cv_radial_mw_inflow_l[0]; cv_radial_mw_inflow_n[n] = cv_radial_mw_inflow_l[1]; cv_radial_mw_inflow_i[n] = cv_radial_mw_inflow_l[2]; cv_radial_mw_inflow_wi[n] = cv_radial_mw_inflow_l[3]
            cv_radial_mw_inflow_exc_sub[n] = cv_radial_mw_inflow_exc_sub_l[0]; cv_radial_mw_inflow_n_exc_sub[n] = cv_radial_mw_inflow_exc_sub_l[1]; cv_radial_mw_inflow_i_exc_sub[n] = cv_radial_mw_inflow_exc_sub_l[2]
            cv_radial_mw_inflow_wi_exc_sub[n] = cv_radial_mw_inflow_exc_sub_l[3]
            cv_radial_mw_outflow[n] = cv_radial_mw_outflow_l[0]; cv_radial_mw_outflow_n[n] = cv_radial_mw_outflow_l[1]; cv_radial_mw_outflow_i[n] = cv_radial_mw_outflow_l[2]; cv_radial_mw_outflow_wi[n] = cv_radial_mw_outflow_l[3]
            cv_radial_mw_outflow_exc_sub[n] = cv_radial_mw_outflow_exc_sub_l[0]; cv_radial_mw_outflow_n_exc_sub[n] = cv_radial_mw_outflow_exc_sub_l[1]; cv_radial_mw_outflow_i_exc_sub[n] = cv_radial_mw_outflow_exc_sub_l[2]
            cv_radial_mw_outflow_wi_exc_sub[n] = cv_radial_mw_outflow_exc_sub_l[3]

            cv_radial_fw_inflow[n] = cv_radial_fw_inflow_l[0]; cv_radial_fw_inflow_n[n] = cv_radial_fw_inflow_l[1]; cv_radial_fw_inflow_i[n] = cv_radial_fw_inflow_l[2]; cv_radial_fw_inflow_wi[n] = cv_radial_fw_inflow_l[3]
            cv_radial_fw_inflow_exc_sub[n] = cv_radial_fw_inflow_exc_sub_l[0]; cv_radial_fw_inflow_n_exc_sub[n] = cv_radial_fw_inflow_exc_sub_l[1]; cv_radial_fw_inflow_i_exc_sub[n] = cv_radial_fw_inflow_exc_sub_l[2]
            cv_radial_fw_inflow_wi_exc_sub[n] = cv_radial_fw_inflow_exc_sub_l[3]
            cv_radial_fw_outflow[n] = cv_radial_fw_outflow_l[0]; cv_radial_fw_outflow_n[n] = cv_radial_fw_outflow_l[1]; cv_radial_fw_outflow_i[n] = cv_radial_fw_outflow_l[2]; cv_radial_fw_outflow_wi[n] = cv_radial_fw_outflow_l[3]
            cv_radial_fw_outflow_exc_sub[n] = cv_radial_fw_outflow_exc_sub_l[0]; cv_radial_fw_outflow_n_exc_sub[n] = cv_radial_fw_outflow_exc_sub_l[1]; cv_radial_fw_outflow_i_exc_sub[n] = cv_radial_fw_outflow_exc_sub_l[2]
            cv_radial_fw_outflow_wi_exc_sub[n] = cv_radial_fw_outflow_exc_sub_l[3]

            dUdr_inflow[n] = dUdr_inflow_l[0]; dUdr_inflow_n[n] = dUdr_inflow_l[1]; dUdr_inflow_i[n] = dUdr_inflow_l[2]; dUdr_inflow_wi[n] = dUdr_inflow_l[3]
            dUdr_inflow_exc_sub[n] = dUdr_inflow_exc_sub_l[0]; dUdr_inflow_n_exc_sub[n] = dUdr_inflow_exc_sub_l[1]; dUdr_inflow_i_exc_sub[n] = dUdr_inflow_exc_sub_l[2]; dUdr_inflow_wi_exc_sub[n] = dUdr_inflow_exc_sub_l[3]
            dUdr_outflow[n] = dUdr_outflow_l[0]; dUdr_outflow_n[n] = dUdr_outflow_l[1]; dUdr_outflow_i[n] = dUdr_outflow_l[2]; dUdr_outflow_wi[n] = dUdr_outflow_l[3]
            dUdr_outflow_exc_sub[n] = dUdr_outflow_exc_sub_l[0]; dUdr_outflow_n_exc_sub[n] = dUdr_outflow_exc_sub_l[1]; dUdr_outflow_i_exc_sub[n] = dUdr_outflow_exc_sub_l[2]; dUdr_outflow_wi_exc_sub[n] = dUdr_outflow_exc_sub_l[3]

            dEkrdr_inflow[n] = dEkrdr_inflow_l[0]; dEkrdr_inflow_n[n] = dEkrdr_inflow_l[1]; dEkrdr_inflow_i[n] = dEkrdr_inflow_l[2]; dEkrdr_inflow_wi[n] = dEkrdr_inflow_l[3]
            dEkrdr_inflow_exc_sub[n] = dEkrdr_inflow_exc_sub_l[0]; dEkrdr_inflow_n_exc_sub[n] = dEkrdr_inflow_exc_sub_l[1]; dEkrdr_inflow_i_exc_sub[n] = dEkrdr_inflow_exc_sub_l[2]
            dEkrdr_inflow_wi_exc_sub[n] = dEkrdr_inflow_exc_sub_l[3]
            dEkrdr_outflow[n] = dEkrdr_outflow_l[0]; dEkrdr_outflow_n[n] = dEkrdr_outflow_l[1]; dEkrdr_outflow_i[n] = dEkrdr_outflow_l[2]; dEkrdr_outflow_wi[n] = dEkrdr_outflow_l[3]
            dEkrdr_outflow_exc_sub[n] = dEkrdr_outflow_exc_sub_l[0]; dEkrdr_outflow_n_exc_sub[n] = dEkrdr_outflow_exc_sub_l[1]; dEkrdr_outflow_i_exc_sub[n] = dEkrdr_outflow_exc_sub_l[2]
            dEkrdr_outflow_wi_exc_sub[n] = dEkrdr_outflow_exc_sub_l[3]

            dEkdr_inflow[n] = dEkdr_inflow_l[0]; dEkdr_inflow_n[n] = dEkdr_inflow_l[1]; dEkdr_inflow_i[n] = dEkdr_inflow_l[2]; dEkdr_inflow_wi[n] = dEkdr_inflow_l[3]
            dEkdr_inflow_exc_sub[n] = dEkdr_inflow_exc_sub_l[0]; dEkdr_inflow_n_exc_sub[n] = dEkdr_inflow_exc_sub_l[1]; dEkdr_inflow_i_exc_sub[n] = dEkdr_inflow_exc_sub_l[2]; dEkdr_inflow_wi_exc_sub[n] = dEkdr_inflow_exc_sub_l[3]
            dEkdr_outflow[n] = dEkdr_outflow_l[0]; dEkdr_outflow_n[n] = dEkdr_outflow_l[1]; dEkdr_outflow_i[n] = dEkdr_outflow_l[2]; dEkdr_outflow_wi[n] = dEkdr_outflow_l[3]
            dEkdr_outflow_exc_sub[n] = dEkdr_outflow_exc_sub_l[0]; dEkdr_outflow_n_exc_sub[n] = dEkdr_outflow_exc_sub_l[1]; dEkdr_outflow_i_exc_sub[n] = dEkdr_outflow_exc_sub_l[2] 
            dEkdr_outflow_wi_exc_sub[n] = dEkdr_outflow_exc_sub_l[3]
                

                
        
        t_flux_utils += time.time() - t_flux_utils_temp

        '''# 2nd step. Compute flux maps for coarse radius bins
        if verbose:
            print "computing flux maps"

        r_surface_coarse = np.arange(0.1,1.1,0.1)
        dmdt_inflow_map_list = []
        dmdt_outflow_map_list = []
        rho_inflow_map_list = []
        rho_outflow_map_list = []
        vrad_inflow_map_list = []
        vrad_outflow_map_list = []
        temp_mu_inflow_map_list = []
        temp_mu_outflow_map_list = []
        in_sub_inflow_map_list = []
        in_sub_outflow_map_list = []
        costheta_list = []
        phi_list = []

        t_flux_utils_temp = time.time()

        for n in range(len(r_surface_coarse)):
            dmdt_inflow_map, dmdt_outflow_map, rho_inflow_map, rho_outflow_map, vrad_inflow_map, vrad_outflow_map, \
                temp_mu_inflow_map, temp_mu_outflow_map, in_sub_inflow_map, in_sub_outflow_map, costheta, phi  = \
                flux_utils.Compute_Flux_Surface_Uniform_Sampling(r_surface_coarse[n],rvirh, csize_h, cr_h, cx_h, cy_h, cz_h,
                                                                 [rho_snap_h], temp_h, mu_h, cv_radial_h, vscalar_h, 
                                                                 in_sub_h, RunDir, timestep, return_map=True)
            dmdt_inflow_map_list.append(dmdt_inflow_map)
            dmdt_outflow_map_list.append(dmdt_outflow_map)
            rho_inflow_map_list.append(rho_inflow_map)
            rho_outflow_map_list.append(rho_outflow_map)
            vrad_inflow_map_list.append(vrad_inflow_map)
            vrad_outflow_map_list.append(vrad_outflow_map)
            temp_mu_inflow_map_list.append(temp_mu_inflow_map)
            temp_mu_outflow_map_list.append(temp_mu_outflow_map)
            in_sub_inflow_map_list.append(in_sub_inflow_map)
            in_sub_outflow_map_list.append(in_sub_outflow_map)
            costheta_list.append(costheta)
            phi_list.append(phi)

        t_flux_utils += time.time() - t_flux_utils_temp'''

        # 3rd step. Compute integrated fluxes for thick shells
        t_thick_temp = time.time()
        dr_shell = 0.1 # thickness of the shell as a funciton of the virial radius
        r_shell = np.arange(0.1,1.1,0.1)
        dmdt_in_shell = np.zeros_like(r_shell)
        dmdt_out_shell = np.zeros_like(r_shell)


        kms_per_kpc = 10**3  / (dm.jeje_utils.cm2pc/10.)
        Gyr = 365.25 *24 *60**2 * 1e9
        kpc = 3.09*10**19
           
        inflow = cv_radial_h < 0.0
        outflow = cv_radial_h > 0.0

        if verbose:
            print "Computing radial flux profile for thick shells"
        for n in range(len(r_shell)):

            in_shell = (cr_h > rvirh * (r_shell[n] - 0.5 * dr_shell)) & (cr_h < rvirh * (r_shell[n] + 0.5 * dr_shell))
            dmdt_in_shell[n] = -np.sum(cmass_h[in_shell&inflow] *cv_radial_h[in_shell&inflow] * v2kms*1e3) / (dr_shell * rvirh * r2kpc*kpc) * Gyr# msun Gyr^-1
            dmdt_out_shell[n]= np.sum(cmass_h[in_shell&outflow]*cv_radial_h[in_shell&outflow]* v2kms*1e3) / (dr_shell * rvirh * r2kpc*kpc) * Gyr

        t_thick += time.time() - t_thick_temp

        # Write the thick shell integrated flux profiles to disk
        shell_group = flux_group_ts.create_group("radial_profiles_shell")
        shell_group.attrs["contains"] = "Radial flux profiles, integrated over a shell of thickness"+str(dr_shell) + " r_vir"
 
        shell_group.create_dataset("dmdt_inflow",data=dmdt_in_shell)
        shell_group.create_dataset("dmdt_outflow",data=dmdt_out_shell)
        shell_group.create_dataset("r_shell",data=r_shell)

        shell_group["dmdt_inflow"].attrs["description"] = "gas mass inflow rate integrated across a shell at r = r_shell, units: Msun /Gyr"
        shell_group["dmdt_outflow"].attrs["description"]= "gas mass outflow rate integrated across a shell at r = r_shell, units: Msun /Gyr"
        shell_group["r_shell"].attrs["description"] = "radius of the shell, units: r_vir (virial radius"

        # Write the integrated radial flux profiles to disk
        radial_profile_group = flux_group_ts.create_group("radial_profiles")
        radial_profile_group.attrs["contains"] = "Radial flux profiles, integrated over a surface."

        radial_profile_group.create_dataset("r_surface",data= r_surface_fine)

        radial_profile_group.create_dataset("dmdt_inflow", data=dmdt_inflow)
        radial_profile_group.create_dataset("dmdt_outflow", data=dmdt_outflow)
        radial_profile_group.create_dataset("dmdt_inflow_exc_sub", data=dmdt_inflow_exc_sub)
        radial_profile_group.create_dataset("dmdt_outflow_exc_sub", data=dmdt_outflow_exc_sub)

        radial_profile_group.create_dataset("dmdr_inflow", data=dmdr_inflow)
        radial_profile_group.create_dataset("dmdr_outflow", data=dmdr_outflow)
        radial_profile_group.create_dataset("dmdr_inflow_exc_sub", data=dmdr_inflow_exc_sub)
        radial_profile_group.create_dataset("dmdr_outflow_exc_sub", data=dmdr_outflow_exc_sub)

        radial_profile_group.create_dataset("cv_radial_mw_inflow", data=cv_radial_mw_inflow)
        radial_profile_group.create_dataset("cv_radial_mw_outflow", data=cv_radial_mw_outflow)
        radial_profile_group.create_dataset("cv_radial_mw_inflow_exc_sub", data=cv_radial_mw_inflow_exc_sub)
        radial_profile_group.create_dataset("cv_radial_mw_outflow_exc_sub", data=cv_radial_mw_outflow_exc_sub)

        radial_profile_group.create_dataset("cv_radial_fw_inflow", data=cv_radial_fw_inflow)
        radial_profile_group.create_dataset("cv_radial_fw_outflow", data=cv_radial_fw_outflow)
        radial_profile_group.create_dataset("cv_radial_fw_inflow_exc_sub", data=cv_radial_fw_inflow_exc_sub)
        radial_profile_group.create_dataset("cv_radial_fw_outflow_exc_sub", data=cv_radial_fw_outflow_exc_sub)

        radial_profile_group.create_dataset("dEkrdr_inflow", data=dEkrdr_inflow)
        radial_profile_group.create_dataset("dEkrdr_outflow", data=dEkrdr_outflow)
        radial_profile_group.create_dataset("dEkrdr_inflow_exc_sub", data=dEkrdr_inflow_exc_sub)
        radial_profile_group.create_dataset("dEkrdr_outflow_exc_sub", data=dEkrdr_outflow_exc_sub)

        radial_profile_group.create_dataset("dEkdr_inflow", data=dEkdr_inflow)
        radial_profile_group.create_dataset("dEkdr_outflow", data=dEkdr_outflow)
        radial_profile_group.create_dataset("dEkdr_inflow_exc_sub", data=dEkdr_inflow_exc_sub)
        radial_profile_group.create_dataset("dEkdr_outflow_exc_sub", data=dEkdr_outflow_exc_sub)

        radial_profile_group.create_dataset("dUdr_inflow", data=dUdr_inflow)
        radial_profile_group.create_dataset("dUdr_outflow", data=dUdr_outflow)
        radial_profile_group.create_dataset("dUdr_inflow_exc_sub", data=dUdr_inflow_exc_sub)
        radial_profile_group.create_dataset("dUdr_outflow_exc_sub", data=dUdr_outflow_exc_sub)

        #
        radial_profile_group.create_dataset("dmdt_inflow_n", data=dmdt_inflow_n)
        radial_profile_group.create_dataset("dmdt_outflow_n", data=dmdt_outflow_n)
        radial_profile_group.create_dataset("dmdt_inflow_n_exc_sub", data=dmdt_inflow_n_exc_sub)
        radial_profile_group.create_dataset("dmdt_outflow_n_exc_sub", data=dmdt_outflow_n_exc_sub)
        
        radial_profile_group.create_dataset("dmdr_inflow_n", data=dmdr_inflow_n)
        radial_profile_group.create_dataset("dmdr_outflow_n", data=dmdr_outflow_n)
        radial_profile_group.create_dataset("dmdr_inflow_n_exc_sub", data=dmdr_inflow_n_exc_sub)
        radial_profile_group.create_dataset("dmdr_outflow_n_exc_sub", data=dmdr_outflow_n_exc_sub)
        
        radial_profile_group.create_dataset("cv_radial_mw_inflow_n", data=cv_radial_mw_inflow_n)
        radial_profile_group.create_dataset("cv_radial_mw_outflow_n", data=cv_radial_mw_outflow_n)
        radial_profile_group.create_dataset("cv_radial_mw_inflow_n_exc_sub", data=cv_radial_mw_inflow_n_exc_sub)
        radial_profile_group.create_dataset("cv_radial_mw_outflow_n_exc_sub", data=cv_radial_mw_outflow_n_exc_sub)
        
        radial_profile_group.create_dataset("cv_radial_fw_inflow_n", data=cv_radial_fw_inflow_n)
        radial_profile_group.create_dataset("cv_radial_fw_outflow_n", data=cv_radial_fw_outflow_n)
        radial_profile_group.create_dataset("cv_radial_fw_inflow_n_exc_sub", data=cv_radial_fw_inflow_n_exc_sub)
        radial_profile_group.create_dataset("cv_radial_fw_outflow_n_exc_sub", data=cv_radial_fw_outflow_n_exc_sub)
        
        radial_profile_group.create_dataset("dEkrdr_inflow_n", data=dEkrdr_inflow_n)
        radial_profile_group.create_dataset("dEkrdr_outflow_n", data=dEkrdr_outflow_n)
        radial_profile_group.create_dataset("dEkrdr_inflow_n_exc_sub", data=dEkrdr_inflow_n_exc_sub)
        radial_profile_group.create_dataset("dEkrdr_outflow_n_exc_sub", data=dEkrdr_outflow_n_exc_sub)
        
        radial_profile_group.create_dataset("dEkdr_inflow_n", data=dEkdr_inflow_n)
        radial_profile_group.create_dataset("dEkdr_outflow_n", data=dEkdr_outflow_n)
        radial_profile_group.create_dataset("dEkdr_inflow_n_exc_sub", data=dEkdr_inflow_n_exc_sub)
        radial_profile_group.create_dataset("dEkdr_outflow_n_exc_sub", data=dEkdr_outflow_n_exc_sub)
        
        radial_profile_group.create_dataset("dUdr_inflow_n", data=dUdr_inflow_n)
        radial_profile_group.create_dataset("dUdr_outflow_n", data=dUdr_outflow_n)
        radial_profile_group.create_dataset("dUdr_inflow_n_exc_sub", data=dUdr_inflow_n_exc_sub)
        radial_profile_group.create_dataset("dUdr_outflow_n_exc_sub", data=dUdr_outflow_n_exc_sub)

        #
        radial_profile_group.create_dataset("dmdt_inflow_i", data=dmdt_inflow_i)
        radial_profile_group.create_dataset("dmdt_outflow_i", data=dmdt_outflow_i)
        radial_profile_group.create_dataset("dmdt_inflow_i_exc_sub", data=dmdt_inflow_i_exc_sub)
        radial_profile_group.create_dataset("dmdt_outflow_i_exc_sub", data=dmdt_outflow_i_exc_sub)
        
        radial_profile_group.create_dataset("dmdr_inflow_i", data=dmdr_inflow_i)
        radial_profile_group.create_dataset("dmdr_outflow_i", data=dmdr_outflow_i)
        radial_profile_group.create_dataset("dmdr_inflow_i_exc_sub", data=dmdr_inflow_i_exc_sub)
        radial_profile_group.create_dataset("dmdr_outflow_i_exc_sub", data=dmdr_outflow_i_exc_sub)
        
        radial_profile_group.create_dataset("cv_radial_mw_inflow_i", data=cv_radial_mw_inflow_i)
        radial_profile_group.create_dataset("cv_radial_mw_outflow_i", data=cv_radial_mw_outflow_i)
        radial_profile_group.create_dataset("cv_radial_mw_inflow_i_exc_sub", data=cv_radial_mw_inflow_i_exc_sub)
        radial_profile_group.create_dataset("cv_radial_mw_outflow_i_exc_sub", data=cv_radial_mw_outflow_i_exc_sub)
        
        radial_profile_group.create_dataset("cv_radial_fw_inflow_i", data=cv_radial_fw_inflow_i)
        radial_profile_group.create_dataset("cv_radial_fw_outflow_i", data=cv_radial_fw_outflow_i)
        radial_profile_group.create_dataset("cv_radial_fw_inflow_i_exc_sub", data=cv_radial_fw_inflow_i_exc_sub)
        radial_profile_group.create_dataset("cv_radial_fw_outflow_i_exc_sub", data=cv_radial_fw_outflow_i_exc_sub)
        
        radial_profile_group.create_dataset("dEkrdr_inflow_i", data=dEkrdr_inflow_i)
        radial_profile_group.create_dataset("dEkrdr_outflow_i", data=dEkrdr_outflow_i)
        radial_profile_group.create_dataset("dEkrdr_inflow_i_exc_sub", data=dEkrdr_inflow_i_exc_sub)
        radial_profile_group.create_dataset("dEkrdr_outflow_i_exc_sub", data=dEkrdr_outflow_i_exc_sub)
        
        radial_profile_group.create_dataset("dEkdr_inflow_i", data=dEkdr_inflow_i)
        radial_profile_group.create_dataset("dEkdr_outflow_i", data=dEkdr_outflow_i)
        radial_profile_group.create_dataset("dEkdr_inflow_i_exc_sub", data=dEkdr_inflow_i_exc_sub)
        radial_profile_group.create_dataset("dEkdr_outflow_i_exc_sub", data=dEkdr_outflow_i_exc_sub)
        
        radial_profile_group.create_dataset("dUdr_inflow_i", data=dUdr_inflow_i)
        radial_profile_group.create_dataset("dUdr_outflow_i", data=dUdr_outflow_i)
        radial_profile_group.create_dataset("dUdr_inflow_i_exc_sub", data=dUdr_inflow_i_exc_sub)
        radial_profile_group.create_dataset("dUdr_outflow_i_exc_sub", data=dUdr_outflow_i_exc_sub)
        
        #
        radial_profile_group.create_dataset("dmdt_inflow_wi", data=dmdt_inflow_wi)
        radial_profile_group.create_dataset("dmdt_outflow_wi", data=dmdt_outflow_wi)
        radial_profile_group.create_dataset("dmdt_inflow_wi_exc_sub", data=dmdt_inflow_wi_exc_sub)
        radial_profile_group.create_dataset("dmdt_outflow_wi_exc_sub", data=dmdt_outflow_wi_exc_sub)
        
        radial_profile_group.create_dataset("dmdr_inflow_wi", data=dmdr_inflow_wi)
        radial_profile_group.create_dataset("dmdr_outflow_wi", data=dmdr_outflow_wi)
        radial_profile_group.create_dataset("dmdr_inflow_wi_exc_sub", data=dmdr_inflow_wi_exc_sub)
        radial_profile_group.create_dataset("dmdr_outflow_wi_exc_sub", data=dmdr_outflow_wi_exc_sub)
        
        radial_profile_group.create_dataset("cv_radial_mw_inflow_wi", data=cv_radial_mw_inflow_wi)
        radial_profile_group.create_dataset("cv_radial_mw_outflow_wi", data=cv_radial_mw_outflow_wi)
        radial_profile_group.create_dataset("cv_radial_mw_inflow_wi_exc_sub", data=cv_radial_mw_inflow_wi_exc_sub)
        radial_profile_group.create_dataset("cv_radial_mw_outflow_wi_exc_sub", data=cv_radial_mw_outflow_wi_exc_sub)
        
        radial_profile_group.create_dataset("cv_radial_fw_inflow_wi", data=cv_radial_fw_inflow_wi)
        radial_profile_group.create_dataset("cv_radial_fw_outflow_wi", data=cv_radial_fw_outflow_wi)
        radial_profile_group.create_dataset("cv_radial_fw_inflow_wi_exc_sub", data=cv_radial_fw_inflow_wi_exc_sub)
        radial_profile_group.create_dataset("cv_radial_fw_outflow_wi_exc_sub", data=cv_radial_fw_outflow_wi_exc_sub)
        
        radial_profile_group.create_dataset("dEkrdr_inflow_wi", data=dEkrdr_inflow_wi)
        radial_profile_group.create_dataset("dEkrdr_outflow_wi", data=dEkrdr_outflow_wi)
        radial_profile_group.create_dataset("dEkrdr_inflow_wi_exc_sub", data=dEkrdr_inflow_wi_exc_sub)
        radial_profile_group.create_dataset("dEkrdr_outflow_wi_exc_sub", data=dEkrdr_outflow_wi_exc_sub)
        
        radial_profile_group.create_dataset("dEkdr_inflow_wi", data=dEkdr_inflow_wi)
        radial_profile_group.create_dataset("dEkdr_outflow_wi", data=dEkdr_outflow_wi)
        radial_profile_group.create_dataset("dEkdr_inflow_wi_exc_sub", data=dEkdr_inflow_wi_exc_sub)
        radial_profile_group.create_dataset("dEkdr_outflow_wi_exc_sub", data=dEkdr_outflow_wi_exc_sub)
        
        radial_profile_group.create_dataset("dUdr_inflow_wi", data=dUdr_inflow_wi)
        radial_profile_group.create_dataset("dUdr_outflow_wi", data=dUdr_outflow_wi)
        radial_profile_group.create_dataset("dUdr_inflow_wi_exc_sub", data=dUdr_inflow_wi_exc_sub)
        radial_profile_group.create_dataset("dUdr_outflow_wi_exc_sub", data=dUdr_outflow_wi_exc_sub)

        #
        radial_profile_group.create_dataset("dmdt_inflow_Zn", data=dmdt_inflow_Zn)
        radial_profile_group.create_dataset("dmdt_outflow_Zn", data=dmdt_outflow_Zn)
        radial_profile_group.create_dataset("dmdt_inflow_Zn_exc_sub", data=dmdt_inflow_Zn_exc_sub)
        radial_profile_group.create_dataset("dmdt_outflow_Zn_exc_sub", data=dmdt_outflow_Zn_exc_sub)
        
        radial_profile_group.create_dataset("dmdr_inflow_Zn", data=dmdr_inflow_Zn)
        radial_profile_group.create_dataset("dmdr_outflow_Zn", data=dmdr_outflow_Zn)
        radial_profile_group.create_dataset("dmdr_inflow_Zn_exc_sub", data=dmdr_inflow_Zn_exc_sub)
        radial_profile_group.create_dataset("dmdr_outflow_Zn_exc_sub", data=dmdr_outflow_Zn_exc_sub)

        radial_profile_group.create_dataset("dmdt_inflow_Zi", data=dmdt_inflow_Zi)
        radial_profile_group.create_dataset("dmdt_outflow_Zi", data=dmdt_outflow_Zi)
        radial_profile_group.create_dataset("dmdt_inflow_Zi_exc_sub", data=dmdt_inflow_Zi_exc_sub)
        radial_profile_group.create_dataset("dmdt_outflow_Zi_exc_sub", data=dmdt_outflow_Zi_exc_sub)
        
        radial_profile_group.create_dataset("dmdr_inflow_Zi", data=dmdr_inflow_Zi)
        radial_profile_group.create_dataset("dmdr_outflow_Zi", data=dmdr_outflow_Zi)
        radial_profile_group.create_dataset("dmdr_inflow_Zi_exc_sub", data=dmdr_inflow_Zi_exc_sub)
        radial_profile_group.create_dataset("dmdr_outflow_Zi_exc_sub", data=dmdr_outflow_Zi_exc_sub)
        
        radial_profile_group.create_dataset("dmdt_inflow_Zwi", data=dmdt_inflow_Zwi)
        radial_profile_group.create_dataset("dmdt_outflow_Zwi", data=dmdt_outflow_Zwi)
        radial_profile_group.create_dataset("dmdt_inflow_Zwi_exc_sub", data=dmdt_inflow_Zwi_exc_sub)
        radial_profile_group.create_dataset("dmdt_outflow_Zwi_exc_sub", data=dmdt_outflow_Zwi_exc_sub)
        
        radial_profile_group.create_dataset("dmdr_inflow_Zwi", data=dmdr_inflow_Zwi)
        radial_profile_group.create_dataset("dmdr_outflow_Zwi", data=dmdr_outflow_Zwi)
        radial_profile_group.create_dataset("dmdr_inflow_Zwi_exc_sub", data=dmdr_inflow_Zwi_exc_sub)
        radial_profile_group.create_dataset("dmdr_outflow_Zwi_exc_sub", data=dmdr_outflow_Zwi_exc_sub)
        
        #
        radial_profile_group['r_surface'].attrs["description"] = "radius of the surface, units: r_vir (virial radius)"

        radial_profile_group['dmdt_inflow'].attrs["description"] = "gas mass inflow rate integrated across a surface at radius=r_surface, units: Msun /Gyr"
        radial_profile_group['dmdt_outflow'].attrs["description"] = "gas mass outflow rate integrated across a surface at radius=r_surface, units: Msun /Gyr"
        radial_profile_group['dmdt_inflow_exc_sub'].attrs["description"] = "gas mass inflow rate integrated across a surface at radius=r_surface, excludes surface elements inside subhaloes, units: Msun /Gyr"
        radial_profile_group['dmdt_outflow_exc_sub'].attrs["description"] = "gas mass outflow rate integrated across a surface at radius=r_surface, excludes surface elements inside subhaloes, units: Msun /Gyr"
        radial_profile_group['dmdr_inflow'].attrs["description"] = "differential inflowing gas mass (dm/dr) at a surface at radius=r_surface, units: Msun /kpc"
        radial_profile_group['dmdr_outflow'].attrs["description"] = "differential outflowing gas mass (dm/dr) at a surface at radius=r_surface, units: Msun /kpc"
        radial_profile_group['dmdr_inflow_exc_sub'].attrs["description"] = "differential inflowing gas mass (dm/dr) at a surface at radius=r_surface, excludes surface elements inside subhaloes, units: Msun /kpc"
        radial_profile_group['dmdr_outflow_exc_sub'].attrs["description"] = "differential outflowing gas mass (dm/dr) at a surface at radius=r_surface, excludes surface elements inside subhaloes, units: Msun /kpc"
        radial_profile_group['cv_radial_mw_inflow'].attrs["description"] = "density weighted radial velocity of inflowing gas at a surface at radius=r_surface, units: kms^-1"
        radial_profile_group['cv_radial_mw_outflow'].attrs["description"] = "density weighted radial velocity of outflowing gas at a surface at radius=r_surface, units: kms^-1"
        radial_profile_group['cv_radial_mw_inflow_exc_sub'].attrs["description"] = "density weighted radial velocity of inflowing gas, excluding substructures, at a surface at radius=r_surface, units: kms^-1"
        radial_profile_group['cv_radial_mw_outflow_exc_sub'].attrs["description"] = "density weighted radial velocity of outflowing gas, excluding substructures, at a surface at radius=r_surface, units: kms^-1"
        radial_profile_group['cv_radial_fw_inflow'].attrs["description"] = "flux weighted radial velocity of inflowing gas at a surface at radius=r_surface, units: kms^-1"
        radial_profile_group['cv_radial_fw_outflow'].attrs["description"] = "flux weighted radial velocity of outflowing gas at a surface at radius=r_surface, units: kms^-1"
        radial_profile_group['cv_radial_fw_inflow_exc_sub'].attrs["description"] = "flux weighted radial velocity of inflowing gas, excluding substructures, at a surface at radius=r_surface, units: kms^-1"
        radial_profile_group['cv_radial_fw_outflow_exc_sub'].attrs["description"] = "flux weighted radial velocity of outflowing gas, excluding substructures, at a surface at radius=r_surface, units: kms^-1"
        radial_profile_group['dEkrdr_inflow'].attrs["description"] = "Radial kinetic energy per unit radius (dEk_radial/dr) of inflowing gas at a surface at radius=r_surface, units: Joules /kpc"
        radial_profile_group['dEkrdr_outflow'].attrs["description"] = "Radial kinetic energy per unit radius (dEk_radial/dr) of outflowing gas at a surface at radius=r_surface, units: Joules /kpc"
        radial_profile_group['dEkrdr_inflow_exc_sub'].attrs["description"] = "Radial kinetic energy per unit radius (dEk_radial/dr) of inflowing gas, excluding substructures, at a surface at radius=r_surface, units: Joules /kpc"
        radial_profile_group['dEkrdr_outflow_exc_sub'].attrs["description"] = "Radial kinetic energy per unit radius (dEk_radial/dr) of outflowing gas, excluding substructures, at a surface at radius=r_surface, units: Joules /kpc"
        radial_profile_group['dEkdr_inflow'].attrs["description"] = "Total kinetic energy per unit radius (dEk/dr) of inflowing gas at a surface at radius=r_surface, units: Joules /kpc"
        radial_profile_group['dEkdr_outflow'].attrs["description"] = "Total kinetic energy per unit radius (dEk/dr) of outflowing gas at a surface at radius=r_surface, units: Joules /kpc"
        radial_profile_group['dEkdr_inflow_exc_sub'].attrs["description"] = "Total kinetic energy per unit radius (dEk/dr) of inflowing gas, excluding substructures, at a surface at radius=r_surface, units: Joules /kpc"
        radial_profile_group['dEkdr_outflow_exc_sub'].attrs["description"] = "Total kinetic energy per unit radius (dEk/dr) of outflowing gas, excluding substructures, at a surface at radius=r_surface, units: Joules /kpc"
        radial_profile_group['dUdr_inflow'].attrs["description"] = "Thermal energy per unit radius (dU_radial/dr) of inflowing gas at a surface at radius=r_surface, units: Joules /kpc"
        radial_profile_group['dUdr_outflow'].attrs["description"] = "Thermal energy per unit radius (dU_radial/dr) of outflowing gas at a surface at radius=r_surface, units: Joules /kpc"
        radial_profile_group['dUdr_inflow_exc_sub'].attrs["description"] = "Thermal energy per unit radius (dU/dr) of inflowing gas, excluding substructures, at a surface at radius=r_surface, units: Joules /kpc"
        radial_profile_group['dUdr_outflow_exc_sub'].attrs["description"] = "Thermal energy per unit radius (dU/dr) of outflowing gas, excluding substructures, at a surface at radius=r_surface, units: Joules /kpc"
        
        radial_profile_group['dmdt_inflow_n'].attrs["description"] = "neutral hydrogen mass inflow rate integrated across a surface at radius=r_surface, units: Msun /Gyr"
        radial_profile_group['dmdt_outflow_n'].attrs["description"] = "neutral hydrogen mass outflow rate integrated across a surface at radius=r_surface, units: Msun /Gyr"
        radial_profile_group['dmdt_inflow_n_exc_sub'].attrs["description"] = "neutral hydrogen mass inflow rate integrated across a surface at radius=r_surface, excludes surface elements inside subhaloes, units: Msun /Gyr"
        radial_profile_group['dmdt_outflow_n_exc_sub'].attrs["description"] = "neutral hydrogen mass outflow rate integrated across a surface at radius=r_surface, excludes surface elements inside subhaloes, units: Msun /Gyr"
        radial_profile_group['dmdr_inflow_n'].attrs["description"] = "differential inflowing neutral hydrogen mass (dm/dr) at a surface at radius=r_surface, units: Msun /kpc"
        radial_profile_group['dmdr_outflow_n'].attrs["description"] = "differential outflowing neutral hydrogen mass (dm/dr) at a surface at radius=r_surface, units: Msun /kpc"
        radial_profile_group['dmdr_inflow_n_exc_sub'].attrs["description"] = "differential inflowing neutral hydrogen mass (dm/dr) at a surface at radius=r_surface, excludes surface elements inside subhaloes, units: Msun /kpc"
        radial_profile_group['dmdr_outflow_n_exc_sub'].attrs["description"] = "differential outflowing neutral hydrogen mass (dm/dr) at a surface at radius=r_surface, excludes surface elements inside subhaloes, units: Msun /kpc"
        radial_profile_group['cv_radial_mw_inflow_n'].attrs["description"] = "density weighted radial velocity of inflowing neutral hydrogen at a surface at radius=r_surface, units: kms^-1"
        radial_profile_group['cv_radial_mw_outflow_n'].attrs["description"] = "density weighted radial velocity of outflowing neutral hydrogen at a surface at radius=r_surface, units: kms^-1"
        radial_profile_group['cv_radial_mw_inflow_n_exc_sub'].attrs["description"] = "density weighted radial velocity of inflowing neutral hydrogen, excluding substructures, at a surface at radius=r_surface, units: kms^-1"
        radial_profile_group['cv_radial_mw_outflow_n_exc_sub'].attrs["description"] = "density weighted radial velocity of outflowing neutral hydrogen, excluding substructures, at a surface at radius=r_surface, units: kms^-1"
        radial_profile_group['cv_radial_fw_inflow_n'].attrs["description"] = "flux weighted radial velocity of inflowing neutral hydrogen at a surface at radius=r_surface, units: kms^-1"
        radial_profile_group['cv_radial_fw_outflow_n'].attrs["description"] = "flux weighted radial velocity of outflowing neutral hydrogen at a surface at radius=r_surface, units: kms^-1"
        radial_profile_group['cv_radial_fw_inflow_n_exc_sub'].attrs["description"] = "flux weighted radial velocity of inflowing neutral hydrogen, excluding substructures, at a surface at radius=r_surface, units: kms^-1"
        radial_profile_group['cv_radial_fw_outflow_n_exc_sub'].attrs["description"] = "flux weighted radial velocity of outflowing neutral hydrogen, excluding substructures, at a surface at radius=r_surface, units: kms^-1"
        radial_profile_group['dEkrdr_inflow_n'].attrs["description"] = "Radial kinetic energy per unit radius (dEk_radial/dr) of inflowing neutral hydrogen at a surface at radius=r_surface, units: Joules /kpc"
        radial_profile_group['dEkrdr_outflow_n'].attrs["description"] = "Radial kinetic energy per unit radius (dEk_radial/dr) of outflowing neutral hydrogen at a surface at radius=r_surface, units: Joules /kpc"
        radial_profile_group['dEkrdr_inflow_n_exc_sub'].attrs["description"] = "Radial kinetic energy per unit radius (dEk_radial/dr) of inflowing neutral hydrogen, excluding substructures, at a surface at radius=r_surface, units: Joules /kpc"
        radial_profile_group['dEkrdr_outflow_n_exc_sub'].attrs["description"] = "Radial kinetic energy per unit radius (dEk_radial/dr) of outflowing neutral hydrogen, excluding substructures, at a surface at radius=r_surface, units: Joules /kpc"
        radial_profile_group['dEkdr_inflow_n'].attrs["description"] = "Total kinetic energy per unit radius (dEk/dr) of inflowing neutral hydrogen at a surface at radius=r_surface, units: Joules /kpc"
        radial_profile_group['dEkdr_outflow_n'].attrs["description"] = "Total kinetic energy per unit radius (dEk/dr) of outflowing neutral hydrogen at a surface at radius=r_surface, units: Joules /kpc"
        radial_profile_group['dEkdr_inflow_n_exc_sub'].attrs["description"] = "Total kinetic energy per unit radius (dEk/dr) of inflowing neutral hydrogen, excluding substructures, at a surface at radius=r_surface, units: Joules /kpc"
        radial_profile_group['dEkdr_outflow_n_exc_sub'].attrs["description"] = "Total kinetic energy per unit radius (dEk/dr) of outflowing neutral hydrogen, excluding substructures, at a surface at radius=r_surface, units: Joules /kpc"
        radial_profile_group['dUdr_inflow_n'].attrs["description"] = "Thermal energy per unit radius (dU_radial/dr) of inflowing neutral hydrogen at a surface at radius=r_surface, units: Joules /kpc"
        radial_profile_group['dUdr_outflow_n'].attrs["description"] = "Thermal energy per unit radius (dU_radial/dr) of outflowing neutral hydrogen at a surface at radius=r_surface, units: Joules /kpc"
        radial_profile_group['dUdr_inflow_n_exc_sub'].attrs["description"] = "Thermal energy per unit radius (dU/dr) of inflowing neutral hydrogen, excluding substructures, at a surface at radius=r_surface, units: Joules /kpc"
        radial_profile_group['dUdr_outflow_n_exc_sub'].attrs["description"] = "Thermal energy per unit radius (dU/dr) of outflowing neutral hydrogen, excluding substructures, at a surface at radius=r_surface, units: Joules /kpc"
        
        radial_profile_group['dmdt_inflow_i'].attrs["description"] = "ionised hydrogen mass inflow rate integrated across a surface at radius=r_surface, units: Msun /Gyr"
        radial_profile_group['dmdt_outflow_i'].attrs["description"] = "ionised hydrogen mass outflow rate integrated across a surface at radius=r_surface, units: Msun /Gyr"
        radial_profile_group['dmdt_inflow_i_exc_sub'].attrs["description"] = "ionised hydrogen mass inflow rate integrated across a surface at radius=r_surface, excludes surface elements inside subhaloes, units: Msun /Gyr"
        radial_profile_group['dmdt_outflow_i_exc_sub'].attrs["description"] = "ionised hydrogen mass outflow rate integrated across a surface at radius=r_surface, excludes surface elements inside subhaloes, units: Msun /Gyr"
        radial_profile_group['dmdr_inflow_i'].attrs["description"] = "differential inflowing ionised hydrogen mass (dm/dr) at a surface at radius=r_surface, units: Msun /kpc"
        radial_profile_group['dmdr_outflow_i'].attrs["description"] = "differential outflowing ionised hydrogen mass (dm/dr) at a surface at radius=r_surface, units: Msun /kpc"
        radial_profile_group['dmdr_inflow_i_exc_sub'].attrs["description"] = "differential inflowing ionised hydrogen mass (dm/dr) at a surface at radius=r_surface, excludes surface elements inside subhaloes, units: Msun /kpc"
        radial_profile_group['dmdr_outflow_i_exc_sub'].attrs["description"] = "differential outflowing ionised hydrogen mass (dm/dr) at a surface at radius=r_surface, excludes surface elements inside subhaloes, units: Msun /kpc"
        radial_profile_group['cv_radial_mw_inflow_i'].attrs["description"] = "density weighted radial velocity of inflowing ionised hydrogen at a surface at radius=r_surface, units: kms^-1"
        radial_profile_group['cv_radial_mw_outflow_i'].attrs["description"] = "density weighted radial velocity of outflowing ionised hydrogen at a surface at radius=r_surface, units: kms^-1"
        radial_profile_group['cv_radial_mw_inflow_i_exc_sub'].attrs["description"] = "density weighted radial velocity of inflowing ionised hydrogen, excluding substructures, at a surface at radius=r_surface, units: kms^-1"
        radial_profile_group['cv_radial_mw_outflow_i_exc_sub'].attrs["description"] = "density weighted radial velocity of outflowing ionised hydrogen, excluding substructures, at a surface at radius=r_surface, units: kms^-1"
        radial_profile_group['cv_radial_fw_inflow_i'].attrs["description"] = "flux weighted radial velocity of inflowing ionised hydrogen at a surface at radius=r_surface, units: kms^-1"
        radial_profile_group['cv_radial_fw_outflow_i'].attrs["description"] = "flux weighted radial velocity of outflowing ionised hydrogen at a surface at radius=r_surface, units: kms^-1"
        radial_profile_group['cv_radial_fw_inflow_i_exc_sub'].attrs["description"] = "flux weighted radial velocity of inflowing ionised hydrogen, excluding substructures, at a surface at radius=r_surface, units: kms^-1"
        radial_profile_group['cv_radial_fw_outflow_i_exc_sub'].attrs["description"] = "flux weighted radial velocity of outflowing ionised hydrogen, excluding substructures, at a surface at radius=r_surface, units: kms^-1"
        radial_profile_group['dEkrdr_inflow_i'].attrs["description"] = "Radial kinetic energy per unit radius (dEk_radial/dr) of inflowing ionised hydrogen at a surface at radius=r_surface, units: Joules /kpc"
        radial_profile_group['dEkrdr_outflow_i'].attrs["description"] = "Radial kinetic energy per unit radius (dEk_radial/dr) of outflowing ionised hydrogen at a surface at radius=r_surface, units: Joules /kpc"
        radial_profile_group['dEkrdr_inflow_i_exc_sub'].attrs["description"] = "Radial kinetic energy per unit radius (dEk_radial/dr) of inflowing ionised hydrogen, excluding substructures, at a surface at radius=r_surface, units: Joules /kpc"
        radial_profile_group['dEkrdr_outflow_i_exc_sub'].attrs["description"] = "Radial kinetic energy per unit radius (dEk_radial/dr) of outflowing ionised hydrogen, excluding substructures, at a surface at radius=r_surface, units: Joules /kpc"
        radial_profile_group['dEkdr_inflow_i'].attrs["description"] = "Total kinetic energy per unit radius (dEk/dr) of inflowing ionised hydrogen at a surface at radius=r_surface, units: Joules /kpc"
        radial_profile_group['dEkdr_outflow_i'].attrs["description"] = "Total kinetic energy per unit radius (dEk/dr) of outflowing ionised hydrogen at a surface at radius=r_surface, units: Joules /kpc"
        radial_profile_group['dEkdr_inflow_i_exc_sub'].attrs["description"] = "Total kinetic energy per unit radius (dEk/dr) of inflowing ionised hydrogen, excluding substructures, at a surface at radius=r_surface, units: Joules /kpc"
        radial_profile_group['dEkdr_outflow_i_exc_sub'].attrs["description"] = "Total kinetic energy per unit radius (dEk/dr) of outflowing ionised hydrogen, excluding substructures, at a surface at radius=r_surface, units: Joules /kpc"
        radial_profile_group['dUdr_inflow_i'].attrs["description"] = "Thermal energy per unit radius (dU_radial/dr) of inflowing ionised hydrogen at a surface at radius=r_surface, units: Joules /kpc"
        radial_profile_group['dUdr_outflow_i'].attrs["description"] = "Thermal energy per unit radius (dU_radial/dr) of outflowing ionised hydrogen at a surface at radius=r_surface, units: Joules /kpc"
        radial_profile_group['dUdr_inflow_i_exc_sub'].attrs["description"] = "Thermal energy per unit radius (dU/dr) of inflowing ionised hydrogen, excluding substructures, at a surface at radius=r_surface, units: Joules /kpc"
        radial_profile_group['dUdr_outflow_i_exc_sub'].attrs["description"] = "Thermal energy per unit radius (dU/dr) of outflowing ionised hydrogen, excluding substructures, at a surface at radius=r_surface, units: Joules /kpc"

        radial_profile_group['dmdt_inflow_wi'].attrs["description"] = "warm-ionised hydrogen mass inflow rate integrated across a surface at radius=r_surface, units: Msun /Gyr"
        radial_profile_group['dmdt_outflow_wi'].attrs["description"] = "warm-ionised hydrogen mass outflow rate integrated across a surface at radius=r_surface, units: Msun /Gyr"
        radial_profile_group['dmdt_inflow_wi_exc_sub'].attrs["description"] = "warm-ionised hydrogen mass inflow rate integrated across a surface at radius=r_surface, excludes surface elements inside subhaloes, units: Msun /Gyr"
        radial_profile_group['dmdt_outflow_wi_exc_sub'].attrs["description"] = "warm-ionised hydrogen mass outflow rate integrated across a surface at radius=r_surface, excludes surface elements inside subhaloes, units: Msun /Gyr"
        radial_profile_group['dmdr_inflow_wi'].attrs["description"] = "differential inflowing warm-ionised hydrogen mass (dm/dr) at a surface at radius=r_surface, units: Msun /kpc"
        radial_profile_group['dmdr_outflow_wi'].attrs["description"] = "differential outflowing warm-ionised hydrogen mass (dm/dr) at a surface at radius=r_surface, units: Msun /kpc"
        radial_profile_group['dmdr_inflow_wi_exc_sub'].attrs["description"] = "differential inflowing warm-ionised hydrogen mass (dm/dr) at a surface at radius=r_surface, excludes surface elements inside subhaloes, units: Msun /kpc"
        radial_profile_group['dmdr_outflow_wi_exc_sub'].attrs["description"] = "differential outflowing warm-ionised hydrogen mass (dm/dr) at a surface at radius=r_surface, excludes surface elements inside subhaloes, units: Msun /kpc"
        radial_profile_group['cv_radial_mw_inflow_wi'].attrs["description"] = "density weighted radial velocity of inflowing warm-ionised hydrogen at a surface at radius=r_surface, units: kms^-1"
        radial_profile_group['cv_radial_mw_outflow_wi'].attrs["description"] = "density weighted radial velocity of outflowing warm-ionised hydrogen at a surface at radius=r_surface, units: kms^-1"
        radial_profile_group['cv_radial_mw_inflow_wi_exc_sub'].attrs["description"] = "density weighted radial velocity of inflowing warm-ionised hydrogen, excluding substructures, at a surface at radius=r_surface, units: kms^-1"
        radial_profile_group['cv_radial_mw_outflow_wi_exc_sub'].attrs["description"] = "density weighted radial velocity of outflowing warm-ionised hydrogen, excluding substructures, at a surface at radius=r_surface, units: kms^-1"
        radial_profile_group['cv_radial_fw_inflow_wi'].attrs["description"] = "flux weighted radial velocity of inflowing warm-ionised hydrogen at a surface at radius=r_surface, units: kms^-1"
        radial_profile_group['cv_radial_fw_outflow_wi'].attrs["description"] = "flux weighted radial velocity of outflowing warm-ionised hydrogen at a surface at radius=r_surface, units: kms^-1"
        radial_profile_group['cv_radial_fw_inflow_wi_exc_sub'].attrs["description"] = "flux weighted radial velocity of inflowing warm-ionised hydrogen, excluding substructures, at a surface at radius=r_surface, units: kms^-1"
        radial_profile_group['cv_radial_fw_outflow_wi_exc_sub'].attrs["description"] = "flux weighted radial velocity of outflowing warm-ionised hydrogen, excluding substructures, at a surface at radius=r_surface, units: kms^-1"
        radial_profile_group['dEkrdr_inflow_wi'].attrs["description"] = "Radial kinetic energy per unit radius (dEk_radial/dr) of inflowing warm-ionised hydrogen at a surface at radius=r_surface, units: Joules /kpc"
        radial_profile_group['dEkrdr_outflow_wi'].attrs["description"] = "Radial kinetic energy per unit radius (dEk_radial/dr) of outflowing warm-ionised hydrogen at a surface at radius=r_surface, units: Joules /kpc"
        radial_profile_group['dEkrdr_inflow_wi_exc_sub'].attrs["description"] = "Radial kinetic energy per unit radius (dEk_radial/dr) of inflowing warm-ionised hydrogen, excluding substructures, at a surface at radius=r_surface, units: Joules /kpc"
        radial_profile_group['dEkrdr_outflow_wi_exc_sub'].attrs["description"] = "Radial kinetic energy per unit radius (dEk_radial/dr) of outflowing warm-ionised hydrogen, excluding substructures, at a surface at radius=r_surface, units: Joules /kpc"
        radial_profile_group['dEkdr_inflow_wi'].attrs["description"] = "Total kinetic energy per unit radius (dEk/dr) of inflowing warm-ionised hydrogen at a surface at radius=r_surface, units: Joules /kpc"
        radial_profile_group['dEkdr_outflow_wi'].attrs["description"] = "Total kinetic energy per unit radius (dEk/dr) of outflowing warm-ionised hydrogen at a surface at radius=r_surface, units: Joules /kpc"
        radial_profile_group['dEkdr_inflow_wi_exc_sub'].attrs["description"] = "Total kinetic energy per unit radius (dEk/dr) of inflowing warm-ionised hydrogen, excluding substructures, at a surface at radius=r_surface, units: Joules /kpc"
        radial_profile_group['dEkdr_outflow_wi_exc_sub'].attrs["description"] = "Total kinetic energy per unit radius (dEk/dr) of outflowing warm-ionised hydrogen, excluding substructures, at a surface at radius=r_surface, units: Joules /kpc"
        radial_profile_group['dUdr_inflow_wi'].attrs["description"] = "Thermal energy per unit radius (dU_radial/dr) of inflowing warm-ionised hydrogen at a surface at radius=r_surface, units: Joules /kpc"
        radial_profile_group['dUdr_outflow_wi'].attrs["description"] = "Thermal energy per unit radius (dU_radial/dr) of outflowing warm-ionised hydrogen at a surface at radius=r_surface, units: Joules /kpc"
        radial_profile_group['dUdr_inflow_wi_exc_sub'].attrs["description"] = "Thermal energy per unit radius (dU/dr) of inflowing warm-ionised hydrogen, excluding substructures, at a surface at radius=r_surface, units: Joules /kpc"
        radial_profile_group['dUdr_outflow_wi_exc_sub'].attrs["description"] = "Thermal energy per unit radius (dU/dr) of outflowing warm-ionised hydrogen, excluding substructures, at a surface at radius=r_surface, units: Joules /kpc"
        
        # Note to self: neutral metal actually means metals that trace neutral hydrogen, not some ionization state of a given metal
        radial_profile_group['dmdt_inflow_Zn'].attrs["description"] = "neutral metal mass inflow rate integrated across a surface at radius=r_surface, units: Msun /Gyr"
        radial_profile_group['dmdt_outflow_Zn'].attrs["description"] = "neutral metal mass outflow rate integrated across a surface at radius=r_surface, units: Msun /Gyr"
        radial_profile_group['dmdt_inflow_Zn_exc_sub'].attrs["description"] = "neutral metal mass inflow rate integrated across a surface at radius=r_surface, excludes surface elements inside subhaloes, units: Msun /Gyr"
        radial_profile_group['dmdt_outflow_Zn_exc_sub'].attrs["description"] = "neutral metal mass outflow rate integrated across a surface at radius=r_surface, excludes surface elements inside subhaloes, units: Msun /Gyr"
        radial_profile_group['dmdr_inflow_Zn'].attrs["description"] = "differential inflowing neutral metal mass (dm/dr) at a surface at radius=r_surface, units: Msun /kpc"
        radial_profile_group['dmdr_outflow_Zn'].attrs["description"] = "differential outflowing neutral metal mass (dm/dr) at a surface at radius=r_surface, units: Msun /kpc"
        radial_profile_group['dmdr_inflow_Zn_exc_sub'].attrs["description"] = "differential inflowing neutral metal mass (dm/dr) at a surface at radius=r_surface, excludes surface elements inside subhaloes, units: Msun /kpc"
        radial_profile_group['dmdr_outflow_Zn_exc_sub'].attrs["description"] = "differential outflowing neutral metal mass (dm/dr) at a surface at radius=r_surface, excludes surface elements inside subhaloes, units: Msun /kpc"
        radial_profile_group['dmdt_inflow_Zi'].attrs["description"] = "ionised metal mass inflow rate integrated across a surface at radius=r_surface, units: Msun /Gyr"
        radial_profile_group['dmdt_outflow_Zi'].attrs["description"] = "ionised metal mass outflow rate integrated across a surface at radius=r_surface, units: Msun /Gyr"
        radial_profile_group['dmdt_inflow_Zi_exc_sub'].attrs["description"] = "ionised metal mass inflow rate integrated across a surface at radius=r_surface, excludes surface elements inside subhaloes, units: Msun /Gyr"
        radial_profile_group['dmdt_outflow_Zi_exc_sub'].attrs["description"] = "ionised metal mass outflow rate integrated across a surface at radius=r_surface, excludes surface elements inside subhaloes, units: Msun /Gyr"
        radial_profile_group['dmdr_inflow_Zi'].attrs["description"] = "differential inflowing ionised metal mass (dm/dr) at a surface at radius=r_surface, units: Msun /kpc"
        radial_profile_group['dmdr_outflow_Zi'].attrs["description"] = "differential outflowing ionised metal mass (dm/dr) at a surface at radius=r_surface, units: Msun /kpc"
        radial_profile_group['dmdr_inflow_Zi_exc_sub'].attrs["description"] = "differential inflowing ionised metal mass (dm/dr) at a surface at radius=r_surface, excludes surface elements inside subhaloes, units: Msun /kpc"
        radial_profile_group['dmdr_outflow_Zi_exc_sub'].attrs["description"] = "differential outflowing ionised metal mass (dm/dr) at a surface at radius=r_surface, excludes surface elements inside subhaloes, units: Msun /kpc"
        radial_profile_group['dmdt_inflow_Zi'].attrs["description"] = "warm-ionised metal mass inflow rate integrated across a surface at radius=r_surface, units: Msun /Gyr"
        radial_profile_group['dmdt_outflow_Zi'].attrs["description"] = "warm-ionised metal mass outflow rate integrated across a surface at radius=r_surface, units: Msun /Gyr"
        radial_profile_group['dmdt_inflow_Zi_exc_sub'].attrs["description"] = "warm-ionised metal mass inflow rate integrated across a surface at radius=r_surface, excludes surface elements inside subhaloes, units: Msun /Gyr"
        radial_profile_group['dmdt_outflow_Zi_exc_sub'].attrs["description"] = "warm-ionised metal mass outflow rate integrated across a surface at radius=r_surface, excludes surface elements inside subhaloes, units: Msun /Gyr"
        radial_profile_group['dmdr_inflow_Zi'].attrs["description"] = "differential inflowing warm-ionised metal mass (dm/dr) at a surface at radius=r_surface, units: Msun /kpc"
        radial_profile_group['dmdr_outflow_Zi'].attrs["description"] = "differential outflowing warm-ionised metal mass (dm/dr) at a surface at radius=r_surface, units: Msun /kpc"
        radial_profile_group['dmdr_inflow_Zi_exc_sub'].attrs["description"] = "differential inflowing warm-ionised metal mass (dm/dr) at a surface at radius=r_surface, excludes surface elements inside subhaloes, units: Msun /kpc"
        radial_profile_group['dmdr_outflow_Zi_exc_sub'].attrs["description"] = "differential outflowing warm-ionised metal mass (dm/dr) at a surface at radius=r_surface, excludes surface elements inside subhaloes, units: Msun /kpc"
        
        

        '''# Write the flux maps to disk
        flux_map_group = flux_group_ts.create_group("flux_maps")
        flux_map_group.attrs["contains"] = "Maps of flux at a given radius. Pixel coordinates are in cos(theta),phi spherical coordinates"

        flux_map_group.create_dataset("r_surface", data=r_surface_coarse)
        flux_map_group["r_surface"].attrs["description"] = "radius of the surface, units: r_vir (virial radius)"

        for n in range(len(phi_list)):
            radius_string = str(r_surface_coarse[n]).replace(".","p")
            flux_map_subgroup = flux_map_group.create_group("r_surface_"+radius_string)

            flux_map_subgroup.create_dataset("flux_inflow_map",data=dmdt_inflow_map_list[n])
            flux_map_subgroup["flux_inflow_map"].attrs["description"] = "gas mass inflow rate through a given surface element at cos(theta), phi, units: Msun /Gyr"

            flux_map_subgroup.create_dataset("density_inflow_map",data=rho_inflow_map_list[n])
            flux_map_subgroup["density_inflow_map"].attrs["description"] = "density of inflowing gas in a given surface element at cos(theta), phi, units: Msun /pkpc^3"

            flux_map_subgroup.create_dataset("vrad_inflow_map",data=vrad_inflow_map_list[n])
            flux_map_subgroup["vrad_inflow_map"].attrs["description"] = "radial velocity of inflowing gas in a given surface element at cos(theta), phi, units: km/s"

            flux_map_subgroup.create_dataset("temp_inflow_map",data=temp_mu_inflow_map_list[n])
            flux_map_subgroup["temp_inflow_map"].attrs["description"] = "temperature of inflowing gas in a given surface element at cos(theta), phi, units: Kelvin"

            flux_map_subgroup.create_dataset("flux_outflow_map",data=dmdt_outflow_map_list[n])
            flux_map_subgroup["flux_outflow_map"].attrs["description"] = "gas mass outflow rate through a given surface element at cos(theta), phi, units: Msun /Gyr"
            
            flux_map_subgroup.create_dataset("density_outflow_map",data=rho_outflow_map_list[n])
            flux_map_subgroup["density_outflow_map"].attrs["description"] = "density of outflowing gas in a given surface element at cos(theta), phi, units: Msun /pkpc^3"

            flux_map_subgroup.create_dataset("vrad_outflow_map",data=vrad_outflow_map_list[n])
            flux_map_subgroup["vrad_outflow_map"].attrs["description"] = "radial velocity of outflowing gas in a given surface element at cos(theta), phi, units: km/s"

            flux_map_subgroup.create_dataset("temp_outflow_map",data=temp_mu_outflow_map_list[n])
            flux_map_subgroup["temp_outflow_map"].attrs["description"] = "temperature of outflowing gas in a given surface element at cos(theta), phi, units: Kelvin"

            flux_map_subgroup.create_dataset("in_sub_inflow_map",data=in_sub_inflow_map_list[n])
            flux_map_subgroup["in_sub_inflow_map"].attrs["description"] = "flag to indicate if in a subhalo, units: 0 = not inflowing, 1 = not in a subhalo, 2 = in a subhalo"

            flux_map_subgroup.create_dataset("in_sub_outflow_map",data=in_sub_outflow_map_list[n])
            flux_map_subgroup["in_sub_outflow_map"].attrs["description"] = "flag to indicate if in a subhalo, units: 0 = not outflowing, 1 = not in a subhalo, 2 = in a subhalo"

            flux_map_subgroup.create_dataset("costheta", data=costheta_list[n])
            flux_map_subgroup["costheta"].attrs["description"] = "cos(theta) coordinate of this pixel"

            flux_map_subgroup.create_dataset("phi", data=phi_list[n])
            flux_map_subgroup["phi"].attrs["description"] = "phi coordinate of this pixel, units: radians"'''

subvol_File.close()
File.close()

print "time taken was", time.time() - t1
print "time in flux_utils was", t_flux_utils
print "time in thick shells was", t_thick
