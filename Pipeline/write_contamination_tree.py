import time
t1 = time.time()

import argparse
import sys

parser = argparse.ArgumentParser()
parser.add_argument("-zoom", help = "name of the zoom simulation")
parser.add_argument("-Run", help = "name of the specific run for this zoom simulation")
parser.add_argument("-rd","--RunDir", help="path to simulation directory containing all these output_xxxxx dirs.")
parser.add_argument("-hd","--HaloDir", help="path to directory containing runs of HaloMaker (i.e. containing xx/tree_bricks...)")
parser.add_argument("-td","--TreeDir", help="path to directory containing runs of TreeMaker")
parser.add_argument("-od","--OutDir", help="path to output directory for hdf5 files")
parser.add_argument("-ts_final","--timestep_final", type=int,help="final timestep")
parser.add_argument("-ts_out","--timesteps_out",help="output timestep(s), 'all, 'final', or e.g. '2'")
parser.add_argument("-v","--verbose",help="verbose...")

if len(sys.argv)!=19:
    parser.print_help()
    sys.exit(1)
args = parser.parse_args()

# Set paths
Zoom = args.zoom
Run = args.Run
RunDir = args.RunDir
HaloDir = args.HaloDir
TreeDir = args.TreeDir
OutDir = args.OutDir
timestep_final = args.timestep_final # Timestep for reading in halo brick files and ramses raw output                                                      
timesteps_out = args.timesteps_out
verbose_str = args.verbose

if "True" in verbose_str:
    verbose = True
else:
    verbose = False

timestep_tree_final = timestep_final -1 # At the moment, the tree files store "halo_ts" as timestep -1 (don't ask)


if verbose:
    print 'imports ... '
import dm_utils as dm
import numpy as np
import h5py
from merger_trees import *
import utilities_ramses as ur
import neighbour_utils as nu

# Read in relevant tree information from the hdf5 file
if timesteps_out == "all":
    filename = Zoom + "_" + Run + "_tsf" + str(timestep_final) +".hdf5"
elif timesteps_out == "final":
    filename = Zoom + "_" + Run + "_tsf" + str(timestep_final) +"_ts"+str(timestep_final)+".hdf5"
else:
    try:
        timestep_out = int(timesteps_out)
        filename = Zoom + "_" + Run + "_tsf" + str(timestep_final) +"_ts"+str(timestep_out)+".hdf5"
    except:
        print "timesteps_out",timesteps_out,",is not valid, sorry!"
        quit()
File = h5py.File(OutDir+filename)

# Open the subvolume file (containing all cell data)
subvol_filename = "subvol_"+filename
subvol_File = h5py.File(OutDir+subvol_filename)

# Check the file with the tree information already exists
if not Zoom in File:
    print "Error: Tree information for ", Zoom, " in ", filename, " cannot be read"
    exit()

zoom_group = File[Zoom]

# Select desired timesteps
if timesteps_out == "all":
    timesteps_tree_out = np.arange(timestep_tree_final)+1
elif timesteps_out == "final":
    timesteps_tree_out = np.array([timestep_tree_final])
else:
    try:
        timesteps_tree_out = np.array([int(timesteps_out)])-1
    except:
        print "timesteps_out",timesteps_out,",is not valid, sorry!"
        quit()

# Loop over timesteps
for i_timestep_tree, timestep_tree in enumerate(timesteps_tree_out):        
    timestep = timestep_tree + 1 # Stupid mismatch between merger tree files and everything else

    if verbose:
        print "reading dm particle data and checking for contamination at timestep = ", timestep, "of", timestep_final
    
    ########## Read dm particle data #########################        
    ts_group = subvol_File["timestep_"+str(timestep)]
    dm_group = ts_group["Dark_matter_particle_data"]

    dx = dm_group["x_dm"][:] # particle coordinates
    dy = dm_group["y_dm"][:]
    dz = dm_group["z_dm"][:]
    dmass = dm_group["m_dm"][:] # particle mass

    minmass = dm.dmpart_utils.get_min_mass(RunDir,timestep)
    # define High-res. and Low-res. particles
    lr   = dmass > minmass*2.
    hr   = dmass < minmass*2.

    # Build linked-list of particle

    if len(dx[lr]) > 0:

        dxyz_min = min(dx[lr].min(),dy[lr].min(),dz[lr].min())
        dxyz_max = max(dx[lr].max(),dy[lr].max(),dz[lr].max())

        dx_rescale = (dx[lr] - dxyz_min) / (dxyz_max-dxyz_min)
        dy_rescale = (dy[lr] - dxyz_min) / (dxyz_max-dxyz_min)
        dz_rescale = (dz[lr] - dxyz_min) / (dxyz_max-dxyz_min)
    
        ndm = len(dx)
        nchain_cells = 20
        nextincell,firstincell = nu.neighbour_utils.init_chain(dx_rescale,dy_rescale,dz_rescale,nchain_cells)

    # On the first timestep, initialise list to store contamination info for each final timestep halo
    if i_timestep_tree == 0:
        n2_all_progen_list = []
        n2_main_progen_list = []
        n1p5_all_progen_list = []
        n1p5_main_progen_list = []
        n1_all_progen_list = []
        n1_main_progen_list = []

    # Get conversions for different units
    r2kpc, redshift, little_h, m2Msun,rho2msunpkpc3, v2kms = ur.Set_Units(RunDir, timestep)
    # Factor to help convert into box units (note maxime's merger_tree reader has converted halo positions so be careful here!)
    r2kpc_ideal, redshift, little_h, m2Msun,junk,junk = ur.Set_Units(RunDir, timestep, "ideal")
    boxlen_cMpc = dm.jeje_utils.get_param_real(RunDir,timestep,'boxlen') # Box size in box units
    boxlen_cMpc *= r2kpc_ideal
    boxlen_cMpc = boxlen_cMpc * (1+redshift) /1e3 # cMpc (comoving)
    boxlen_cMpc = boxlen_cMpc * little_h # h^-1 cMpc
    factor = 1./boxlen_cMpc        

    # Loop over final timestep haloes
    ind_halo = -1
    for halo_group_str in zoom_group:

        # Check this is a halo
        if not "Halo_" in halo_group_str:
            continue

        ind_halo += 1 # Index like this instead of enumerate so that skipped parts of the for loop don't contribute to the index

        ap_group = zoom_group[halo_group_str+"/all_progenitors"]
        mp_group = zoom_group[halo_group_str+"/main_progenitors"]

        # On first timestep, initialise arrays to store contamination info
        if i_timestep_tree == 0:
            n2_all_progen_list.append(np.zeros(len(ap_group["halo_ts"]) ))
            n2_main_progen_list.append(np.zeros(len(mp_group["halo_ts"]) ))
            n1p5_all_progen_list.append(np.zeros(len(ap_group["halo_ts"])))
            n1p5_main_progen_list.append(np.zeros(len(mp_group["halo_ts"])))
            n1_all_progen_list.append(np.zeros(len(ap_group["halo_ts"])))
            n1_main_progen_list.append(np.zeros(len(mp_group["halo_ts"])))
            

        # Loop over haloes on this timestep
        ok_ts = np.array(ap_group["halo_ts"])==timestep_tree # haloes on this timestep
        nh_this_ts = len(ap_group["halo_ts"][ok_ts]) # Number of halos on this timestep
        
        for nh in range(nh_this_ts):
            
            # Read halo information
            xh = ap_group["x"][ok_ts][nh] * factor + 0.5
            yh = ap_group["y"][ok_ts][nh] * factor + 0.5
            zh = ap_group["z"][ok_ts][nh] * factor + 0.5
            # Use r200 for central haloes, and rvir (for now) for satellite haloes
            if ap_group["host_halo_id"][ok_ts][nh] == ap_group["halo_id"][ok_ts][nh]:
                rvirh = ap_group["r200"][ok_ts][nh] / r2kpc
            else:
                rvirh = ap_group["rvir"][ok_ts][nh] * factor

            if len(dx[lr]>0):
                # Rescale halo positions for efficient cell division in chain algorithm
                xh_rescale = (xh-dxyz_min)/(dxyz_max-dxyz_min)
                yh_rescale = (yh-dxyz_min)/(dxyz_max-dxyz_min)
                zh_rescale = (zh-dxyz_min)/(dxyz_max-dxyz_min)
                rvirh_rescale = rvirh/(dxyz_max-dxyz_min)

                # Find low resolution particles different radii of this halo (using chain algorithm)
                n_2rvir = nu.neighbour_utils.get_npart_in_sphere_with_chain(RunDir,timestep,dx_rescale,dy_rescale,dz_rescale,xh_rescale,yh_rescale,zh_rescale,rvirh_rescale*2,firstincell,nextincell)
                n_1p5rvir = nu.neighbour_utils.get_npart_in_sphere_with_chain(RunDir,timestep,dx_rescale,dy_rescale,dz_rescale,xh_rescale,yh_rescale,zh_rescale,rvirh_rescale*1.5,firstincell,nextincell)
                n_1rvir = nu.neighbour_utils.get_npart_in_sphere_with_chain(RunDir,timestep,dx_rescale,dy_rescale,dz_rescale,xh_rescale,yh_rescale,zh_rescale,rvirh_rescale,firstincell,nextincell)
            else:
                n_2rvir = 0
                n_1p5rvir = 0
                n_1rvir = 0

            # If this halo is a main progenitor,
            match = min(abs(ap_group["halo_id"][ok_ts][nh] - mp_group["halo_id"])) == 0
            if match:
                main_str = "main "
            else:
                main_str = ""

            # Print appropriate warning messages and write contaminating particle information to hdf5 file
            if n_2rvir > 0:
                print halo_group_str + " has contaminating low-res DM particles within 2 rvir of a " + main_str + "progenitor at timestep", timestep
                print "number within 2 rvir", n_2rvir, ", 1.5 rvir", n_1p5rvir, ", 1 rvir", n_1rvir

            # Write contamination to lists
            assign = n2_all_progen_list[ind_halo][ok_ts]
            assign[nh] = n_2rvir
            n2_all_progen_list[ind_halo][ok_ts] = assign
            
            assign = n1p5_all_progen_list[ind_halo][ok_ts]
            assign[nh] = n_1p5rvir
            n1p5_all_progen_list[ind_halo][ok_ts] = assign

            assign = n1_all_progen_list[ind_halo][ok_ts]
            assign[nh] = n_1rvir
            n1_all_progen_list[ind_halo][ok_ts] = assign
            
            match = ap_group["halo_id"][ok_ts][nh] - mp_group["halo_id"] == 0
            n2_main_progen_list[ind_halo][match] = n_2rvir
            n1p5_main_progen_list[ind_halo][match] = n_1p5rvir
            n1_main_progen_list[ind_halo][match] = n_1rvir

        # On the final timestep, write contamination information to hdf5 file. First check whether we need to delete a previously written entry
        if timestep == timesteps_tree_out[-1]+1:
            if verbose:
                print "writing contamination information to disk"

            if "n_contamination_2rvir" in ap_group:
                del ap_group["n_contamination_2rvir"]
            if "n_contamination_2rvir" in mp_group:
                del mp_group["n_contamination_2rvir"]
            if "n_contamination_1p5rvir" in ap_group:
                del ap_group["n_contamination_1p5rvir"]
            if "n_contamination_1p5rvir" in mp_group:
                del mp_group["n_contamination_1p5rvir"]
            if "n_contamination_1rvir" in ap_group:
                del ap_group["n_contamination_1rvir"]
            if "n_contamination_1rvir" in mp_group:
                del mp_group["n_contamination_1rvir"]
                
            ap_group.create_dataset("n_contamination_2rvir",data=n2_all_progen_list[ind_halo])
            mp_group.create_dataset("n_contamination_2rvir",data=n2_main_progen_list[ind_halo])
            contam_2str = "number of contaminating low-res DM particles within 2 r_vir"
            ap_group["n_contamination_2rvir"].attrs['description'] = contam_2str
            mp_group["n_contamination_2rvir"].attrs['description'] = contam_2str

            ap_group.create_dataset("n_contamination_1p5rvir",data=n1p5_all_progen_list[ind_halo])
            mp_group.create_dataset("n_contamination_1p5rvir",data=n1p5_main_progen_list[ind_halo])
            contam_1p5str = "number of contaminating low-res DM particles within 1.5 r_vir"
            ap_group["n_contamination_1p5rvir"].attrs['description'] = contam_1p5str
            mp_group["n_contamination_1p5rvir"].attrs['description'] = contam_1p5str

            ap_group.create_dataset("n_contamination_1rvir",data=n1_all_progen_list[ind_halo])
            mp_group.create_dataset("n_contamination_1rvir",data=n1_main_progen_list[ind_halo])
            contam_1str = "number of contaminating low-res DM particles within 1 r_vir"
            ap_group["n_contamination_1rvir"].attrs['description'] = contam_1str
            mp_group["n_contamination_1rvir"].attrs['description'] = contam_1str
    
subvol_File.close()
File.close()

print "time taken was", time.time() - t1
