import time
t1 = time.time()

import argparse
import sys
import numpy as np

parser = argparse.ArgumentParser()
parser.add_argument("-zoom", help = "name of the zoom simulation")
parser.add_argument("-Run", help = "name of the specific run for this zoom simulation")
parser.add_argument("-rd","--RunDir", help="path to simulation directory containing all these output_xxxxx dirs.")
parser.add_argument("-hd","--HaloDir", help="path to directory containing runs of HaloMaker (i.e. containing xx/tree_bricks...)")
parser.add_argument("-td","--TreeDir", help="path to directory containing runs of TreeMaker")
parser.add_argument("-od","--OutDir", help="path to output directory for hdf5 files")
parser.add_argument("-ts_final","--timestep_final", type=int,help="final timestep")
parser.add_argument("-ts_out","--timesteps_out",help="output timestep(s), 'all, 'final', or e.g. '2'")
parser.add_argument("-rhd","--RHD",help="Radiation hydrodynamics run")
parser.add_argument("-v","--verbose",help="verbose...")

if len(sys.argv)!=21:
    parser.print_help()
    sys.exit(1)
args = parser.parse_args()

# Set paths
Zoom = args.zoom
Run = args.Run
RunDir = args.RunDir
HaloDir = args.HaloDir
TreeDir = args.TreeDir
OutDir = args.OutDir
timestep_final = args.timestep_final # Timestep for reading in halo brick files and ramses raw output
timesteps_out = args.timesteps_out
verbose_str = args.verbose

if "True" in verbose_str:
    verbose = True
else:
    verbose = False

# List of surface radii at which to compute flux histories (radii are in units of the virial radius)
r_surface_list = [0.25,0.5,0.75,1.0]
r_shell_list = np.arange(0.1,1.1,0.1)

timestep_tree = timestep_final -1 # At the moment, the tree files store "halo_ts" as timestep -1 (don't ask)

sfh_bin_size = 10.0 # Bin size (Myr), with which to compute star formation histories

if verbose:
    print 'imports ... '
import dm_utils as dm
import h5py
import re
import utilities_ramses as ur

# Read in relevant tree information from the hdf5 file
if timesteps_out == "all":
    filename = Zoom + "_" + Run + "_tsf" + str(timestep_final) +".hdf5"
elif timesteps_out == "final":
    filename = Zoom + "_" + Run + "_tsf" + str(timestep_final) +"_ts"+str(timestep_final)+".hdf5"
else:
    try:
        timestep_out = int(timesteps_out)
        filename = Zoom + "_" + Run + "_tsf" + str(timestep_final) +"_ts"+str(timestep_out)+".hdf5"
    except:
        print "timesteps_out",timesteps_out,",is not valid, sorry!"
        quit()
File = h5py.File(OutDir+filename)

# Check the file with the tree information already exists
if not Zoom in File:
    print "Error: Tree information for ", Zoom, " in ", filename, " cannot be read"
    exit()

zoom_group = File[Zoom]

# Loop over final timestep haloes
for halo_group_str in zoom_group:

    # Check this is a central halo
    if not "_main" in halo_group_str:
        continue

    halo_group = zoom_group[halo_group_str]
    
    # Workaround to avoid having to rerun the entire analysis if the flux group gets corrupted (which is easily done at present)
    fluxes_corrupted = True
    flux_string = "fluxes"
    while fluxes_corrupted:
        fluxes_group = halo_group[flux_string]

        try:
            for timestep_group_str in fluxes_group:
                test = fluxes_group[timestep_group_str]
        except:
            print "Warning: Flux group", flux_string," is contaminated, writing flux histories to ", flux_string+"_i", "instead"
            flux_string += "_i"
            continue
        fluxes_corrupted = False

    # How many timesteps we have fluxes for (for this halo)
    n_steps = 0
    for timestep_group_str in fluxes_group:
        if "timestep_" in timestep_group_str:
            n_steps += 1

    redshift_history = np.zeros(n_steps)
    ts_history = np.zeros(n_steps)
    mdot_inflow_history = np.zeros((len(r_surface_list),n_steps))
    mdot_outflow_history = np.zeros((len(r_surface_list),n_steps))

    mdot_inflow_exc_sub_history = np.zeros((len(r_surface_list),n_steps))
    mdot_outflow_exc_sub_history = np.zeros((len(r_surface_list),n_steps))

    mdot_outflow_n_exc_sub_history = np.zeros((len(r_surface_list),n_steps))
    mdot_outflow_i_exc_sub_history = np.zeros((len(r_surface_list),n_steps))

    mdot_inflow_shell = np.zeros((len(r_shell_list),n_steps))
    mdot_outflow_shell = np.zeros((len(r_shell_list),n_steps))

    i_step = 0
    # note timestep group names align with ts, not ts_tree
    for timestep_group_str in fluxes_group:

        if "flux_history" in timestep_group_str:
            continue

        timestep_group = fluxes_group[timestep_group_str]
        
        ts =  int(re.findall(r'\d+', timestep_group_str)[0])
        ts_history[i_step] = ts

        r2kpc, redshift, little_h, m2Msun, junk, junk = ur.Set_Units(RunDir, ts)
        redshift_history[i_step] = redshift

        radial_profiles_group = timestep_group["radial_profiles"]

        for i_surface, r_surface in enumerate(r_surface_list):
            r_surface_array = radial_profiles_group["r_surface"][:]
            index = np.argmin(abs(r_surface_array-r_surface))
            
            mdot_inflow = radial_profiles_group["dmdt_inflow"][:][index] # Msun Gyr^-1
            mdot_outflow = radial_profiles_group["dmdt_outflow"][:][index] # Msun Gyr^-1
            
            mdot_inflow_history[i_surface, i_step] = mdot_inflow
            mdot_outflow_history[i_surface, i_step] = mdot_outflow

            mdot_inflow_exc_sub = radial_profiles_group["dmdt_inflow_exc_sub"][:][index] # Msun Gyr^-1
            mdot_outflow_exc_sub = radial_profiles_group["dmdt_outflow_exc_sub"][:][index] # Msun Gyr^-1
            
            mdot_inflow_exc_sub_history[i_surface, i_step] = mdot_inflow_exc_sub
            mdot_outflow_exc_sub_history[i_surface, i_step] = mdot_outflow_exc_sub

            mdot_outflow_i_exc_sub = radial_profiles_group["dmdt_outflow_i_exc_sub"][:][index] # Msun Gyr^-1
            mdot_outflow_n_exc_sub = radial_profiles_group["dmdt_outflow_n_exc_sub"][:][index] # Msun Gyr^-1

            mdot_outflow_i_exc_sub_history[i_surface, i_step] = mdot_outflow_i_exc_sub
            mdot_outflow_n_exc_sub_history[i_surface, i_step] = mdot_outflow_n_exc_sub

        
        radial_profiles_shell_group = timestep_group["radial_profiles_shell"]

        for i_shell, r_shell in enumerate(r_shell_list):
            r_shell_array = radial_profiles_shell_group["r_shell"][:]
            index = np.argmin(abs(r_shell_array-r_shell))
            
            mdot_inflow = radial_profiles_shell_group["dmdt_inflow"][:][index] # Msun Gyr^-1
            mdot_outflow = radial_profiles_shell_group["dmdt_outflow"][:][index] # Msun Gyr^-1

            mdot_inflow_shell[i_shell, i_step] = mdot_inflow
            mdot_outflow_shell[i_shell, i_step] = mdot_outflow
    
        i_step += 1

    # Sort by redshift/timestep
    order = np.argsort(ts_history)
    ts_history = ts_history[order]
    redshift_history = redshift_history[order]
    mdot_inflow_history = mdot_inflow_history[:,order]
    mdot_outflow_history = mdot_outflow_history[:,order]
    mdot_inflow_exc_sub_history = mdot_inflow_exc_sub_history[:,order]
    mdot_outflow_exc_sub_history = mdot_outflow_exc_sub_history[:,order]
    mdot_inflow_shell = mdot_inflow_shell[:,order]
    mdot_outflow_shell = mdot_outflow_shell[:,order]
    mdot_outflow_n_exc_sub_history = mdot_outflow_n_exc_sub_history[:,order]
    mdot_outflow_i_exc_sub_history = mdot_outflow_i_exc_sub_history[:,order]

    # Write data to disk
    if "flux_history" in fluxes_group:
        del fluxes_group["flux_history"]

    flux_history_group = fluxes_group.create_group("flux_history")

    flux_history_group.create_dataset("r_surfaces",data=r_surface_list)
    flux_history_group["r_surfaces"].attrs['description'] = 'radii of surfaces used to compute flux histories, units: halo virial radius'
    
    flux_history_group.create_dataset("r_shells",data=r_shell_list)
    flux_history_group["r_shells"].attrs['description'] = 'central radii of shells used to compute flux histories, units: halo virial radius'

    flux_history_group.create_dataset("ts",data=ts_history)
    flux_history_group["ts"].attrs['description'] = 'simulation output timestep, (NOTE, not in merger tree timestep system)'
    
    flux_history_group.create_dataset("redshift",data=redshift_history)
    flux_history_group["redshift"].attrs['description'] = 'redshift corresponding to each timestep'

    for i_surface, r_surface in enumerate(r_surface_list):
        r_surface_str = "r_surface_" + str(r_surface).replace(".","p")
        r_surface_group = flux_history_group.create_group(r_surface_str)
        
        r_surface_group.create_dataset("dmdt_inflow",data=mdot_inflow_history[i_surface])
        r_surface_group["dmdt_inflow"].attrs['description'] = 'gas mass inflow rate integrated across a surface at radius ',str(r_surface),', units: Msun /Gyr'

        r_surface_group.create_dataset("dmdt_outflow",data=mdot_outflow_history[i_surface])
        r_surface_group["dmdt_outflow"].attrs['description'] = 'gas mass outflow rate integrated across a surface at radius ',str(r_surface),', units: Msun /Gyr'

        r_surface_group.create_dataset("dmdt_inflow_exc_sub",data=mdot_inflow_exc_sub_history[i_surface])
        r_surface_group["dmdt_inflow_exc_sub"].attrs['description'] = 'gas mass inflow rate, excluding substructures, integrated across a surface at radius ',str(r_surface),', units: Msun /Gyr'

        r_surface_group.create_dataset("dmdt_outflow_exc_sub",data=mdot_outflow_exc_sub_history[i_surface])
        r_surface_group["dmdt_outflow_exc_sub"].attrs['description'] = 'gas mass outflow rate, excluding substructures, integrated across a surface at radius ',str(r_surface),', units: Msun /Gyr'

        r_surface_group.create_dataset("dmdt_outflow_n_exc_sub",data=mdot_outflow_n_exc_sub_history[i_surface])
        r_surface_group["dmdt_outflow_n_exc_sub"].attrs['description'] = 'neutral hydrogen mass outflow rate, excluding substructures, integrated across a surface at radius ',str(r_surface),', units: Msun /Gyr'

        r_surface_group.create_dataset("dmdt_outflow_i_exc_sub",data=mdot_outflow_i_exc_sub_history[i_surface])
        r_surface_group["dmdt_outflow_i_exc_sub"].attrs['description'] = 'ionized hydrogen mass outflow rate, excluding substructures, integrated across a surface at radius ',str(r_surface),', units: Msun /Gyr'
    
    for i_shell, r_shell in enumerate(r_shell_list):
        r_shell_str = "r_shell_" + str(r_shell).replace(".","p")
        r_shell_group = flux_history_group.create_group(r_shell_str)
        
        r_shell_group.create_dataset("dmdt_inflow",data=mdot_inflow_shell[i_shell])
        r_shell_group["dmdt_inflow"].attrs['description'] = 'gas mass inflow rate integrated over a shell with central radius at ',str(r_shell),', units: Msun /Gyr'

        r_shell_group.create_dataset("dmdt_outflow",data=mdot_outflow_shell[i_shell])
        r_shell_group["dmdt_outflow"].attrs['description'] = 'gas mass outflow rate integrated across a shell with central radius at ',str(r_shell),', units: Msun /Gyr'
    

File.close()

print "time taken was", time.time()-t1
