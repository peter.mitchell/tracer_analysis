import time
t1 = time.time()

import argparse
import sys

parser = argparse.ArgumentParser()
parser.add_argument("-zoom", help = "name of the zoom simulation")
parser.add_argument("-Run", help = "name of the specific run for this zoom simulation")
parser.add_argument("-rd","--RunDir", help="path to simulation directory containing all these output_xxxxx dirs.")
parser.add_argument("-hd","--HaloDir", help="path to directory containing runs of HaloMaker (i.e. containing xx/tree_bricks...)")
parser.add_argument("-td","--TreeDir", help="path to directory containing runs of TreeMaker")
parser.add_argument("-od","--OutDir", help="path to output directory for hdf5 files")
parser.add_argument("-ts_final","--timestep_final", type=int,help="final timestep")
parser.add_argument("-ts_out","--timesteps_out",help="output timestep(s), 'all, 'final', or e.g. '2'")
parser.add_argument("-rhd","--RHD",help="Radiation hydrodynamics run")
parser.add_argument("-v","--verbose",help="verbose...")

if len(sys.argv)!=21:
    parser.print_help()
    sys.exit(1)
args = parser.parse_args()

# Set paths
Zoom = args.zoom
Run = args.Run
RunDir = args.RunDir
HaloDir = args.HaloDir
TreeDir = args.TreeDir
OutDir = args.OutDir
timestep_final = args.timestep_final # Timestep for reading in halo brick files and ramses raw output
timesteps_out = args.timesteps_out
RHD = args.RHD
verbose_str = args.verbose

if "True" in verbose_str:
    verbose = True
else:
    verbose = False

if "True" in RHD:
    RHD = True
else:
    RHD = False
    
if verbose:
    "writing tree file for", Zoom, Run

# Define a radius (relative to the main halo centre) out to which we'll search for other candidate haloes
rmax = 300. # kpc
# Set minimum number of dm particles to write a halo to disk
min_nbpart = 1000

# String containing information on how haloes were selected
halo_selection_metadata = ('halo centre must be within '+str(rmax)+'-2r_vir proper kpc of the main halo centre \n'
                           'minimum number of DM particles within r_vir = ' + str(min_nbpart) + '\n'
                           'must have 0 low resolution DM particles within 2 r_vir \n')

# String containing information on how the main halo was selected
main_halo_selection_metadata = 'main halo is selected as the one with maximal n_dm_particles / mhalo**0.5'

if verbose:
    print 'imports ... '
from halotils import py_halo_utils as hutils
import numpy as np
import pandas as pd
from merger_trees import *
import h5py
import os 
import dm_utils as dm
import star_utils as star
import metadata
import utilities_ramses as ur
import tracer_utils as tr

# Read cosmological parameters and build sim dictionary to be fed into Forest class later on
omega_0,lambda_0,little_h = dm.jeje_utils.read_cosmo_params(RunDir,timestep_final)
r2kpc_ideal,redshift, junk, junk, junk, junk = ur.Set_Units(RunDir, timestep_final, "ideal") # Used to compute "ideal" box size. Used in turn to convert halo positions into box units
boxlen_cMpc = dm.jeje_utils.get_param_real(RunDir,timestep_final,'boxlen') # Box size in box units
boxlen_cMpc *= r2kpc_ideal
boxlen_cMpc = boxlen_cMpc * (1+redshift) /1e3 # cMpc (comoving)
boxlen_cMpc = boxlen_cMpc * little_h # h^-1 cMpc
sim = sim={'Lbox': boxlen_cMpc, 'h': little_h, 'Om': omega_0, 'Ol': lambda_0}

if timesteps_out != "final":
    # Read processed merger trees data
    trees = Forest(TreeDir, sim)

# read halo catalog
if verbose:
    print 'reading halo catalog ... '
treefile = "%s%i/tree_bricks%3.3i"%(HaloDir,timestep_final,timestep_final)

nh,ns = hutils.get_nb_halos(treefile) # nhalo, nsubhalo
halos = hutils.read_all_halos(treefile,nh+ns)
halo_keys = ('nbpart','hnum','ts','level',
    'host', 'hostsub', 'nbsub', 'nextsub', 'mass',
    'x', 'y', 'z', 'vx', 'vy', 'vz', 'Lx', 'Ly', 'Lz', 'r',
    'a', 'b', 'c', 'ek', 'ep', 'et',
    'spin', 'rvir', 'mvir', 'tvir', 'cvel', 'rho0', 'r_c')
halos = pd.DataFrame(columns=halo_keys,data=halos)
halos.set_index(halos.hnum,inplace=True)    ## sort/index with hnum

# convert positions to box units
r2kpc, redshift, junk, mass2Msun, junk, junk = ur.Set_Units(RunDir, timestep_final) # Set the units
rmax *= 1./r2kpc
factor = 3.08e24 / dm.jeje_utils.dp_scale_l
halos.x = halos.x * factor + 0.5  ## +0.5 assumes boxlen is one ...
halos.y = halos.y * factor + 0.5  ## +0.5 assumes boxlen is one ...
halos.z = halos.z * factor + 0.5  ## +0.5 assumes boxlen is one ...
halos.r = halos.r * factor
halos.rvir = halos.rvir * factor

# select desired halo as the main halo
halos.r_c = halos.nbpart / halos.mass**0.5
myhalo = halos[halos.r_c == halos.r_c.max()]

# select neighbouring halos & with > 1000 dark matter particles
halos.r_c = (halos.x - myhalo.x.values)**2 + (halos.y - myhalo.y.values)**2 + (halos.z - myhalo.z.values)**2
halos.r_c = np.sqrt(halos.r_c)
halovoisins = halos[(halos.r_c < rmax - 2*halos.rvir) & (halos.nbpart > min_nbpart)]

# Read in DM particle data


print "get n_dm"
npart = tr.tracer_utils.get_ndm_in_sphere(RunDir,timestep_final,myhalo.x,myhalo.y,myhalo.z,rmax)
print "done, get dm props"
x,y,z,m,id = tr.tracer_utils.read_dm_in_sphere(RunDir,timestep_final,myhalo.x,myhalo.y,myhalo.z,rmax,npart)
print "done, get min mass"
minmass = tr.tracer_utils.get_min_mass(RunDir,timestep_final)
print "done"
# define High-res. and Low-res. particles
lr   = m > minmass*2.
hr   = m < minmass*2.


# Compute minimum distance for each halo to a low resolution particle
if verbose:
    print "computing distances to low resolution particles"
lr_r2 = (np.expand_dims(halovoisins.x,1) - x[lr])**2 + (np.expand_dims(halovoisins.y,1) - y[lr])**2 + (np.expand_dims(halovoisins.z,1) - z[lr])**2 
min_lr_r = np.sqrt(np.min(lr_r2,axis=1))
halovoisins["min_lr_r"] = min_lr_r
if verbose:
    print "done"

# Remove contaminated haloes
contaminated = halovoisins.min_lr_r < halovoisins.rvir * 2 # Select haloes with 0 low resolution particles within twice their virial radius
# Be a little more lax for the main halo but print a warning if there is a low res dm particle between 1.5 - 2 rvir
index_temp = 0
for index, this_halo in halovoisins.iterrows():
    if index == myhalo.index:
        lr_r = np.sqrt((halovoisins.x[index] - x[lr])**2 + (halovoisins.y[index] - y[lr])**2 + (halovoisins.z[index] - z[lr])**2)
        n_1p5rvir =  len(lr_r[lr_r<halos.rvir[index]*1.5])
        if n_1p5rvir == 0:
            contaminated[index_temp] = False
            print ""
            print "write_merger_tree.py: Warning: there were low-res dm particles between 1.5-2 rvir for the main halo"
            print "letting the main halo into the sample anyway"
            print ""
        break
    index_temp +=1

halosample = halovoisins[contaminated==False]

############# Exclude haloes outside the main halo where the host halo does not contain stars ##########################

# Read in stellar particle data
if verbose:
    print "Computing which haloes contain any stars, exclude any central haloes (and their satellites) that don't contain any"
print "get n stars"
nst = star.star_utils.get_tot_nstars(RunDir,timestep_final)
print "done, get stars props"
sm,sx,sy,sz,junk,junk,svx,svy,svz,junk = tr.tracer_utils.read_stars(RunDir,timestep_final,nst,True,RHD)
print "done"

# Compute_minimum distance to a stellar particle
sh_r2 = (np.expand_dims(halosample.x,1) - sx)**2 + (np.expand_dims(halosample.y,1) - sy)**2 + (np.expand_dims(halosample.z,1) - sz)**2

min_sh_r = np.sqrt(np.min(sh_r2,axis=1))

main_sample = np.array(halosample.host) == float(myhalo.hnum)
central_has_stars = (np.array(halosample.host) == np.array(halosample.hnum)) & (min_sh_r < np.array(halosample.rvir) * 0.2)
sat_host_has_stars = np.zeros_like(np.array(halosample.host))<0.0
for n in range(len(sat_host_has_stars)):
    ind_host = np.array(halosample.host)[n] == np.array(halosample.hnum)
    host_has_stars = central_has_stars[ind_host]
    if host_has_stars:
        sat_host_has_stars[n] = True

main_or_has_stars = main_sample | central_has_stars | sat_host_has_stars
halosample = halosample[main_or_has_stars]

if verbose:
    print "done"

if verbose:
    print ""
    print "The furthest out that a halo made the final selection cut was", max(np.array(halosample.r_c * r2kpc))
    print "This means that the most distant particles/cells that need to be written are at a distance", max(np.array((halosample.r_c+2*halosample.rvir) *r2kpc))
    print "For reference, rmax was set to ", rmax * r2kpc
    print ""


# Check that the main halo is in the halo sample
main_inc = False
for index, this_halo in halosample.iterrows():

    if index == myhalo.index:
        main_inc = True

if not main_inc:
    print "write_merger_tree.py: Error, main halo didn't make it into the halo sample!"

    # Work out if the problem is that the main halo contains low resolution dark matter particles
    for index, this_halo in halos.iterrows():
        if index == myhalo.index:
            lr_r = np.sqrt((halos.x[index] - x[lr])**2 + (halos.y[index] - y[lr])**2 + (halos.z[index] - z[lr])**2)
            print "no of contaminating particles within 2 rvir", len(lr_r[lr_r < halos.rvir[index]*2])
            print "no of contaminating particles within 1 rvir", len(lr_r[lr_r < halos.rvir[index]])
            print "no of contaminating particles within 1.5 rvir", len(lr_r[lr_r<halos.rvir[index]*1.5])

    quit()

# Create the output hdf5 file
if timesteps_out == "all":
    filename = Zoom + "_" + Run + "_tsf" + str(timestep_final) +".hdf5"
elif timesteps_out == "final":
    filename = Zoom + "_" + Run + "_tsf" + str(timestep_final) +"_ts"+str(timestep_final)+".hdf5"
else:
    try:
        timestep_out = int(timesteps_out)
        filename = Zoom + "_" + Run + "_tsf" + str(timestep_final) +"_ts"+str(timestep_out)+".hdf5"
    except:
        print "timesteps_out",timesteps_out,",is not valid, sorry!"
        quit()

# Delete existing hdf5 file if one exists before writing a new one
if os.path.isfile(OutDir+filename):
    os.remove(OutDir+filename)
    print "deleted previous hdf5 file at ", OutDir+filename

if verbose:
    print "Writing hdf5"

File = h5py.File(OutDir+filename)
zoom_group = File.create_group(Zoom)

# Write halo indexes (to allow easier automated reading of halo groups via parsing in other programmes)
zoom_group.create_dataset("halo_num",data=halosample.hnum)


# Write halo selection metadata
halo_selection_group = zoom_group.create_group("halo_selection_criteria")
halo_selection_group.attrs["halo selection criteria"] = halo_selection_metadata
halo_selection_group.attrs["main halo selection criteria"] = main_halo_selection_metadata

# Arrays used to compute the maximum radius (relative to the main halo) within which we need to save cell/particle data at each timestep
rmax_needed = np.zeros(timestep_final)

if timesteps_out != "final":
    main_progenitors_main = trees.get_main_progenitor(myhalo.hnum,timestep_final)
else:
    main_progenitors_main = myhalo.copy()

count = 0

# lol pandas wow you are so great
if timesteps_out == "final":
    halosample.rename(columns = {'hnum':'halo_id'}, inplace = True)
    halosample.rename(columns = {'ts':'halo_ts'}, inplace = True)
    halosample.rename(columns = {'host':'host_halo_id'}, inplace = True)
    halosample.halo_ts -= 1
    
    factor2 = 1./boxlen_cMpc
    halosample.x = (halosample.x -0.5) / factor2
    halosample.y = (halosample.y -0.5) / factor2
    halosample.z = (halosample.z -0.5) / factor2
    halosample.rvir = (halosample.rvir) / factor2

    main_progenitors_main["halo_ts"] = main_progenitors_main.ts -1
    main_progenitors_main.x = (main_progenitors_main.x -0.5) / factor2
    main_progenitors_main.y = (main_progenitors_main.y -0.5) / factor2
    main_progenitors_main.z = (main_progenitors_main.z -0.5) / factor2

# Loop over haloes
for index, this_halo in halosample.iterrows():

    if verbose:
        print "writing halo", count, "of", len(halosample)
    count+=1
   
    if timesteps_out == "final": # In this mode, we bypass the need to construct trees, meaning we only needed to run the halo finder on one output and not run the trees at all
        main_progenitors = this_halo.copy()
        all_progenitors = main_progenitors.copy()
 
        if index == myhalo.index:
            halo_group = zoom_group.create_group("Halo_"+str(int(this_halo.halo_id))+"_main")
        elif this_halo.halo_id != this_halo.host_halo_id:
            halo_group = zoom_group.create_group("Halo_"+str(int(this_halo.halo_id))+"_sat")
        else:
            halo_group = zoom_group.create_group("Halo_"+str(int(this_halo.halo_id)))

    else:
        try:
            main_progenitors = trees.get_main_progenitor(this_halo.hnum,timestep_final)
        except:
            print "Tree information for halo", this_halo.hnum, "couldn't be accessed, skipping this halo"
            continue

        all_progenitors = trees.get_all_progenitors(this_halo.hnum,timestep_final)

        if index == myhalo.index:
            halo_group = zoom_group.create_group("Halo_"+str(int(this_halo.hnum))+"_main")
        elif this_halo.hnum != this_halo.host:
            halo_group = zoom_group.create_group("Halo_"+str(int(this_halo.hnum))+"_sat")
        else:
            halo_group = zoom_group.create_group("Halo_"+str(int(this_halo.hnum)))


    if timesteps_out != "all":
        if not timesteps_out == "final":
            try:
                timesteps_out_tree = int(timesteps_out) -1
                main_progentitors = main_progenitors[main_progenitors["halo_ts"]==timestep_out_tree]
                all_progentitors  = all_progenitors[all_progenitors["halo_ts"]==timestep_out_tree]
            except:
                print "timesteps_out", timesteps_out, "isn't valid, bad times :("
                quit()


    mp_group = halo_group.create_group("main_progenitors")

    try:
        for column in main_progenitors.columns:
            mp_group.create_dataset(column,data=main_progenitors[column])
            try:
                mp_group[column].attrs['description'] =metadata.tree_meta_data[column]
            except:
                print "Warning: no meta data available for column", column
    except:
        for value, name in zip(main_progenitors, main_progenitors.index):
            mp_group.create_dataset(name,data=np.array([value]))
            try:
                mp_group[name].attrs['description'] =metadata.tree_meta_data[name]
            except:
                print "Warning: no meta data available for column", name
            
    ap_group = halo_group.create_group("all_progenitors")

    # Prune all progenitors list to only include main progenitrors and progenitors with > nbpart (fiducial value=1000) dm particles
    if not timesteps_out == "final":
        is_main_progenitor = np.zeros_like(np.array(all_progenitors.mvir))<0.0
        for n in range(len(is_main_progenitor)):
            if np.array(all_progenitors.halo_id)[n] in np.array(main_progenitors.halo_id):
                is_main_progenitor[n] = True
        nbpart_all_progenitors = np.array(all_progenitors.mvir)*1e11 / (minmass*mass2Msun)   
        all_progenitors = all_progenitors[(nbpart_all_progenitors > min_nbpart) | is_main_progenitor]

    try:
        for column in all_progenitors.columns:
            ap_group.create_dataset(column,data=all_progenitors[column])
            try:
                ap_group[column].attrs['description'] = metadata.tree_meta_data[column]
            except:
                print "Warning: no meta data available for column", column
    except:
        for value, name in zip(all_progenitors, all_progenitors.index):
            ap_group.create_dataset(name,data=np.array([value]))
            try:
                ap_group[name].attrs['description'] =metadata.tree_meta_data[name]
            except:
                print "Warning: no meta data available for column", name

    # Looping over all timesteps, find the largest distance of any progenitor to the main progenitor of the target halo
    for ts_tree in range(timestep_final):
        ok_main = ts_tree == np.array(main_progenitors_main["halo_ts"])
        x_main = np.array(main_progenitors_main["x"])[ok_main] # x-coordinate of the target halo at this timestep
        y_main = np.array(main_progenitors_main["y"])[ok_main]
        z_main = np.array(main_progenitors_main["z"])[ok_main]

        ok = ts_tree == np.append(np.array(all_progenitors["halo_ts"]),np.array(main_progenitors["halo_ts"]))

        if len(np.append(np.array(all_progenitors["x"]),np.array(main_progenitors["x"]))[ok]) == 0 or len(x_main) ==0:
            continue


        r = np.sqrt( np.square(x_main - np.append(np.array(all_progenitors["x"]),np.array(main_progenitors["x"]))[ok]) +
                           np.square(y_main - np.append(np.array(all_progenitors["y"]),np.array(main_progenitors["y"]))[ok]) +
                           np.square(z_main - np.append(np.array(all_progenitors["z"]),np.array(main_progenitors["z"]))[ok]) )

        
        r_plus_2rvir = 2 * np.append(np.array(all_progenitors["rvir"]),np.array(main_progenitors["rvir"]))[ok] + r

        if max(r_plus_2rvir) > rmax_needed[ts_tree]:
            rmax_needed[ts_tree] = max(r_plus_2rvir)



# Write rmax_needed
zoom_group.create_dataset("rmax",data=rmax_needed)
zoom_group["rmax"].attrs["description"] = "Radius (comoving Mpc) from the main progenitor of the main halo (at each timestep) that encloses 2 virial radii of all haloes"

print "array of rmax for different timesteps is", rmax_needed

File.close()

print "time taken was", time.time()-t1
