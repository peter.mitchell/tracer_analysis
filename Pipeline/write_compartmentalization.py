# This routine takes all particles / cells and assigns to a single component (e.g. inner halo, subhalo, outer halo).
# The routine is weirdly written/structured because it is a hack of an older version of write_gas_mass_tree_exc_sub.py
# EDIT: This routine was replaced by write_tidal_radius, and is now redundant

import time
t1 = time.time()
t_io = 0
t_cpu = 0
t_chain = 0

import argparse
import sys
import numpy as np

parser = argparse.ArgumentParser()
parser.add_argument("-zoom", help = "name of the zoom simulation")
parser.add_argument("-Run", help = "name of the specific run for this zoom simulation")
parser.add_argument("-rd","--RunDir", help="path to simulation directory containing all these output_xxxxx dirs.")
parser.add_argument("-hd","--HaloDir", help="path to directory containing runs of HaloMaker (i.e. containing xx/tree_bricks...)")
parser.add_argument("-td","--TreeDir", help="path to directory containing runs of TreeMaker")
parser.add_argument("-od","--OutDir", help="path to output directory for hdf5 files")
parser.add_argument("-ts_final","--timestep_final", type=int,help="final timestep")
parser.add_argument("-v","--verbose",help="verbose...")

if len(sys.argv)!=17:
    parser.print_help()
    sys.exit(1)
args = parser.parse_args()

# Set paths
Zoom = args.zoom
Run = args.Run
RunDir = args.RunDir
HaloDir = args.HaloDir
TreeDir = args.TreeDir
OutDir = args.OutDir
timestep_final = args.timestep_final # Timestep for reading in halo brick files and ramses raw output                                                      
verbose_str = args.verbose

if "True" in verbose_str:
    verbose = True
else:
    verbose = False

timestep_tree_final = timestep_final -1 # At the moment, the tree files store "halo_ts" as timestep -1 (don't ask)

#### Choose the defintion for the subhalo radius #####

subhalo_radius_defn = "rvir_0p5" # For now, valid choices are
# rvir - the virial radius from the halo finder
# rvir_0p5 - half the virial radius from the halo finder

if subhalo_radius_defn == "rvir":
    radius_str = "subhalo radius is defined as the subhalo virial radius from the halo finder"
    print "write_compartmentalization.py: using rvir as subhalo radius"
elif subhalo_radius_defn == "rvir_0p5":
    radius_str = "subhalo radius is defined as half the subhalo virial radius from the halo finder"
    print "write_compartmentalization.py: using 0.5 rvir as subhalo radius"
else:
    print "write_compartmentalization.py: Subhalo radius = ", subhalo_radius_defn, " not implemented"
    quit()

################### Selection criteria for different components and their indicies #######################################
# For now the compartmentalization done here is simply to distinguish between inside/outside subhaloes


# Add an extra field that tells you if particles/cells belong to a subhalo with < or > sub_threshold_no dm particles within the user defined subhalo radius
# This is included in case you want to count low mass substructures as part of the diffuse halo or not.
sub_threshold_no = 1000
subhalo_threshold_str = "0 = not part of any subhalo, 1 = belongs to a subhalo with < "+str(sub_threshold_no) + "dm particles within "+str(subhalo_radius_defn) +", 2 = belongs to a subhalo with > "+str(sub_threshold_no)

# Convert temperature into temperature / molecular weight
mol_weight = 0.6 # Estimate of typical effective molecular weight
#temp_mu_low_list =  np.array(temp_low_list) / mol_weight
#temp_mu_high_list = np.array(temp_high_list) / mol_weight


if verbose:
    print 'imports ... '
import dm_utils as dm
from halotils import py_halo_utils as hutils
import cutils as cut
import h5py
import utilities_ramses as ur
import neighbour_utils as nu
import pandas as pd

# Read in relevant tree information from the hdf5 file
filename = Zoom + "_" + Run + "_tsf" + str(timestep_final) +".hdf5"
File = h5py.File(OutDir+filename)

# Open the subvolume file (containing all cell data)
subvol_filename = "subvol_"+filename
subvol_File = h5py.File(OutDir+subvol_filename)

# Check the file with the tree information already exists
if not Zoom in File:
    print "Error: Tree information for ", Zoom, " in ", filename, " cannot be read"
    exit()

zoom_group = File[Zoom]

# Loop over timesteps
for timestep_tree in 1+np.arange(timestep_tree_final):
   
    timestep = timestep_tree + 1 # Stupid mismatch between merger tree files and everything else

    #if timestep_tree != 1 and timestep_tree != timestep_tree_final:
    #    print "skip"
    #    continue
    #if timestep_tree != timestep_tree_final:
    #    print "skip"
    #    continue

    if verbose:
        print "performing IO at timestep = ", timestep, "of", timestep_final

    # Read in halo catalogue (to find subhalo positions)
    treefile = "%s%i/tree_bricks%3.3i"%(HaloDir,timestep,timestep)
    nh,ns = hutils.get_nb_halos(treefile) # nhalo, nsubhalo
    halos = hutils.read_all_halos(treefile,nh+ns)
    halo_keys = ('nbpart','hnum','ts','level',
                 'host', 'hostsub', 'nbsub', 'nextsub', 'mass',
                 'x', 'y', 'z', 'vx', 'vy', 'vz', 'Lx', 'Ly', 'Lz', 'r',
                 'a', 'b', 'c', 'ek', 'ep', 'et',
                 'spin', 'rvir', 'mvir', 'tvir', 'cvel', 'rho0', 'r_c')
    halos = pd.DataFrame(columns=halo_keys,data=halos)

    subhalo_host_num_list = np.array(halos.host)
    subhalo_num_list = np.array(halos.hnum)
    subhalo_rvir_list = np.array(halos.rvir)
    subhalo_x_list = np.array(halos.x)
    subhalo_y_list = np.array(halos.y)
    subhalo_z_list = np.array(halos.z)
    subhalo_nbpart_list = np.array(halos.nbpart)
    
    # problem is that halo_id and host_halo_id are tree quantities

    # Get conversions for different units
    r2kpc, redshift, little_h, m2Msun,rho2msunpkpc3, v2kms = ur.Set_Units(RunDir, timestep)
    boxlen_pkpc = dm.jeje_utils.get_param_real(RunDir,timestep,'boxlen') * r2kpc # Box size in proper kpc, using the correct box size

    factor_subhalo = 3.08e24 / dm.jeje_utils.dp_scale_l # Conversion to code units for the Halo finder unit system (as opposed to the tree unit system)

    t_io_temp = time.time()

    ########## Read cell/particle data #########################        
    # Hdf5 group for this timestep (contains cell data)
    ts_group = subvol_File["timestep_"+str(timestep)]

    # Read in DM particle data
    dm_group = ts_group["Dark_matter_particle_data"]
    dx = dm_group["x_dm"][:]
    dy = dm_group["y_dm"][:]
    dz = dm_group["z_dm"][:]
    dmass = dm_group["m_dm"][:]

    # scale dark matter particle masses into Msun
    dmass *= m2Msun

    # Read in star particle data
    star_group = ts_group["Stellar_particle_data"]

    sm = star_group["m_s"][:]
    sx = star_group["x_s"][:]
    sy = star_group["y_s"][:]
    sz = star_group["z_s"][:]

    # Scale stellar masses into solar mass units
    sm *= m2Msun

    cell_group = ts_group["Gas_leaf_cell_data"]

    rho_snap = cell_group["rho_c"][:] # Gas density
    pre_snap = cell_group["pre_c"][:] # Gas pressure
    cx = cell_group["x_c"][:] # Cell coordinates
    cy = cell_group["y_c"][:]
    cz = cell_group["z_c"][:]
    lev = cell_group["lev"][:] # Cell refinement level

    t_io += time.time() - t_io_temp

    if verbose:
        print "Done: performing unit conversions and setting up chains"

    t_cpu_temp = time.time()

    cut.jeje_utils.read_conversion_scales(RunDir,timestep)
    ndensity = rho_snap * cut.jeje_utils.dp_scale_d / cut.jeje_utils.mh * cut.jeje_utils.xh # Number density: Atoms cm^-3
    scale_temk = (cut.jeje_utils.dp_scale_l / cut.jeje_utils.dp_scale_t)**2 * cut.jeje_utils.mh / cut.jeje_utils.kb
    temp = pre_snap/rho_snap * scale_temk # T/mu in K

    # Compute cell size using amr refinement level and box size
    csize = boxlen_pkpc / 2.**lev # proper kpc
    cmass = rho_snap * rho2msunpkpc3 * csize**3 # Msun - gas mass in each cell

    nstar = len(sx); ncell = len(cx); ndm = len(dx)

    # Combine leaf cells, stars and dm particles into a single set of arrays
    cx = np.concatenate([cx, sx, dx])
    cy = np.concatenate([cy, sy, dy])
    cz = np.concatenate([cz, sz, dz])
    cmass = np.concatenate([cmass, sm, dmass])

    t_chain_temp = time.time()

    c_component_index = np.zeros_like(cx)

    # Build linked-list of cells
    cxyz_min = min(cx.min(),cy.min(),cz.min()) 
    cxyz_max = max(cx.max(),cy.max(),cz.max())
    
    cx_rescale = (cx - cxyz_min) / (cxyz_max-cxyz_min)
    cy_rescale = (cy - cxyz_min) / (cxyz_max-cxyz_min)
    cz_rescale = (cz - cxyz_min) / (cxyz_max-cxyz_min)

    nleafcells = len(cx)
    nchain_cells = 20
    nextincell,firstincell = nu.neighbour_utils.init_chain(cx_rescale,cy_rescale,cz_rescale,nchain_cells)

    t_chain += time.time() - t_chain_temp
        
    # Factor to help convert into box units (note maxime's merger_tree reader has converted halo positions so be careful here!)
    r2kpc_ideal, redshift, little_h, m2Msun,junk,junk = ur.Set_Units(RunDir, timestep, "ideal")
    boxlen_cMpc = dm.jeje_utils.get_param_real(RunDir,timestep,'boxlen') # Box size in box units
    boxlen_cMpc *= r2kpc_ideal
    boxlen_cMpc = boxlen_cMpc * (1+redshift) /1e3 # cMpc (comoving)
    boxlen_cMpc = boxlen_cMpc * little_h # h^-1 cMpc
    factor = 1./boxlen_cMpc

    if verbose:
        print "Done: computing compartments"

    # Loop over final timestep haloes
    ind_halo = -1
    for halo_group_str in zoom_group:

        # Check this is a halo
        if not "Halo_" in halo_group_str:
            continue

        ind_halo += 1 # Index like this instead of enumerate so that skipped parts of the for loop don't contribute to the index

        #if verbose:
        #    print "Computing components for final halo", halo_group_str
            
        ap_group = zoom_group[halo_group_str+"/all_progenitors"]
        
        # Loop over haloes on this timestep
        ok_ts = ap_group["halo_ts"][:]==timestep_tree # haloes on this timestep
        nh_this_ts = len(ap_group["halo_ts"][:][ok_ts]) # Number of halos on this timestep

        for nh in range(nh_this_ts):
            
            #if verbose:
            #    print "\rComputing components for progenitor", nh+1, "of", nh_this_ts, "on this timestep"

            t_chain_temp = time.time()

            # Find subhaloes of this halo
            halo_num = ap_group["halo_num"][:][ok_ts][nh]
            is_subhalo = (halo_num == subhalo_host_num_list) & (halo_num != subhalo_num_list)
            n_sub = len(subhalo_host_num_list[is_subhalo])
            
            
            # Find cells within rvir of a subhalo
            in_sub = np.zeros_like(cx) < 0
            for isub in range(n_sub):

                #if verbose:
                #    print "testing subhalo", isub,"of",n_sub

                # subhalo_x_list etc need to be into code units here
                sub_x_rescale = (subhalo_x_list[is_subhalo][isub]*factor_subhalo+0.5-cxyz_min)/(cxyz_max-cxyz_min)
                sub_y_rescale = (subhalo_y_list[is_subhalo][isub]*factor_subhalo+0.5-cxyz_min)/(cxyz_max-cxyz_min)
                sub_z_rescale = (subhalo_z_list[is_subhalo][isub]*factor_subhalo+0.5-cxyz_min)/(cxyz_max-cxyz_min)
                
                if subhalo_radius_defn == "rvir":
                    subhalo_rmax = subhalo_rvir_list[is_subhalo][isub]
                elif subhalo_radius_defn == "rvir_0p5":
                    subhalo_rmax = 0.5 * subhalo_rvir_list[is_subhalo][isub]
                else:
                    print "Error: write_compartmentalization.py: subhalo radius definition", subhalo_radius_defn, "is not defined"
                    quit()

                sub_rvir_rescale = (subhalo_rmax*factor_subhalo)/(cxyz_max-cxyz_min)
            
                ncell_in_sub_rvir = nu.neighbour_utils.get_npart_in_sphere_with_chain(RunDir,timestep,cx_rescale,cy_rescale,cz_rescale,sub_x_rescale,sub_y_rescale,sub_z_rescale,sub_rvir_rescale,firstincell,nextincell)

                if ncell_in_sub_rvir > 0:
                    ind_sub = nu.neighbour_utils.get_part_indicies_in_sphere_with_chain(RunDir,timestep,cx_rescale,cy_rescale,cz_rescale,sub_x_rescale,sub_y_rescale,sub_z_rescale,sub_rvir_rescale,ncell_in_sub_rvir,firstincell,nextincell)
                    in_sub[ind_sub] = True
                    if subhalo_nbpart_list[is_subhalo][isub] > sub_threshold_no:
                        c_component_index[ind_sub] = 2
                    else:
                        c_component_index[ind_sub] = 1

                    
    # Split cells/stars/dark matter particles
    s_component_index = c_component_index[ncell:ncell+nstar]
    d_component_index = c_component_index[ncell+nstar:ncell+nstar+ndm]
    c_component_index = c_component_index[0:ncell]
 
    if "component_index" in star_group:
        del star_group["component_index"]
    if "component_index" in dm_group:
        del dm_group["component_index"]
    if "component_index" in cell_group:
        del cell_group["component_index"]


    # Write indicies to disk
    star_group.create_dataset("component_index", data=s_component_index)
    star_group["component_index"].attrs['description']=subhalo_threshold_str
    
    dm_group.create_dataset("component_index",data=d_component_index)
    dm_group["component_index"].attrs['description']=subhalo_threshold_str

    cell_group.create_dataset("component_index",data=c_component_index)
    cell_group["component_index"].attrs['description']=subhalo_threshold_str
   
    t_cpu += time.time() - t_cpu_temp

subvol_File.close()
File.close()

print "time taken", time.time() - t1
print "io time", t_io
print "cpu time", t_cpu
print "chain cpu time", t_chain
