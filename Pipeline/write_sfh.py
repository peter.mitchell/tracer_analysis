import time
t1 = time.time()

import argparse
import sys

parser = argparse.ArgumentParser()
parser.add_argument("-zoom", help = "name of the zoom simluation")
parser.add_argument("-Run", help = "name of the specific run for this zoom simulation")
parser.add_argument("-rd","--RunDir", help="path to simulation directory containing all these output_xxxxx dirs.")
parser.add_argument("-hd","--HaloDir", help="path to directory containing runs of HaloMaker (i.e. containing xx/tree_bricks...)")
parser.add_argument("-td","--TreeDir", help="path to directory containing runs of TreeMaker")
parser.add_argument("-od","--OutDir", help="path to output directory for hdf5 files")
parser.add_argument("-ts_final","--timestep_final", type=int,help="final timestep")
parser.add_argument("-ts_out","--timesteps_out",help="output timestep(s), 'all, 'final', or e.g. '2'")
parser.add_argument("-v","--verbose",help="verbose...")
parser.add_argument("-rhd","--RHD",help="Radiation hydrodynamics run")
parser.add_argument("-fb","--feedback",help="feedback active for this run")

if len(sys.argv)!=23:
    parser.print_help()
    sys.exit(1)
args = parser.parse_args()

# Set paths
Zoom = args.zoom
Run = args.Run
RunDir = args.RunDir
HaloDir = args.HaloDir
TreeDir = args.TreeDir
OutDir = args.OutDir
timestep_final = args.timestep_final # Timestep for reading in halo brick files and ramses raw output
timesteps_out = args.timesteps_out
RHD = args.RHD
feedback_str = args.feedback # Does this run have feedback?
verbose_str = args.verbose

if timesteps_out != "final" and timesteps_out != "all":
    print "timesteps_out",timesteps_out,"is not valid for write_sfh, this routine only runs on final timestep halos"
    quit()

if "True" in verbose_str:
    verbose = True
else:
    verbose = False

if "True" in feedback_str:
    feedback = True
else:
    feedback = False

if "True" in RHD:
    RHD = True
else:
    RHD = False
    
if feedback:
    eta_sn = 0.2 # Fraction of mass returned to the ISM after SN explosion
    t_delay = 10.0 # Time delay (Myr) until SN explosion
else:
    eta_sn = 0.0
    t_delay = 10.0

timestep_tree = timestep_final -1 # At the moment, the tree files store "halo_ts" as timestep -1 (don't ask)

sfh_bin_size = 10.0 # Bin size (Myr), with which to compute star formation histories

star_selection = "rvir_frac"
if star_selection == "rvir_frac":
    r_aperture_list = [0.2,0.25,0.5,0.75,1.0]
elif star_selection == "fixed_aperture":
    # Select aperture sizes for in proper kpc
    r_aperture_list = [30] # Note that stellar mass will only be computed for particles within both rvir and r_aperture
else:
    print "not valid"
    exit()

if verbose:
    print 'imports ... '
import dm_utils as dm
import star_utils as star
import tracer_utils as tr
import numpy as np
import h5py
import utilities_ramses as ur

# Read in relevant tree information from the hdf5 file
if timesteps_out == "all":
    filename = Zoom + "_" + Run + "_tsf" + str(timestep_final) +".hdf5"
elif timesteps_out == "final":
    filename = Zoom + "_" + Run + "_tsf" + str(timestep_final) +"_ts"+str(timestep_final)+".hdf5"
else:
    try:
        timestep_out = int(timesteps_out)
        filename = Zoom + "_" + Run + "_tsf" + str(timestep_final) +"_ts"+str(timestep_out)+".hdf5"
    except:
        print "timesteps_out",timesteps_out,",is not valid, sorry!"
        quit()
File = h5py.File(OutDir+filename)

# Check the file with the tree information already exists
if not Zoom in File:
    print "Error: Tree information for ", Zoom, " in ", filename, " cannot be read"
    exit()

zoom_group = File[Zoom]

ts = timestep_final
ts_tree = timestep_final -1

ts = ts_tree + 1 # Stupid mismatch between merger tree files and everything else

# Get conversions for different units
r2kpc, redshift, little_h, m2Msun, junk, junk = ur.Set_Units(RunDir, ts)
r2kpc_ideal, redshift, little_h, m2Msun, junk, junk = ur.Set_Units(RunDir, ts, "ideal") # Used to compute "ideal" box size. Used in turn to convert halo positions into box units
boxlen_cMpc = dm.jeje_utils.get_param_real(RunDir,ts,'boxlen') # Box size in box units
boxlen_cMpc *= r2kpc_ideal
boxlen_cMpc = boxlen_cMpc * (1+redshift) /1e3 # cMpc (comoving)
boxlen_cMpc = boxlen_cMpc * little_h # h^-1 cMpc

# Read in star particle data
if verbose:
    print "Reading star particles for ts ", ts


nst = star.star_utils.get_tot_nstars(RunDir,ts)
    
# On the first read, we need to call ct_init_cosm (from conformal_time.f90) when we call read_stars_all_props (from star_utils.f90)
sm,sx,sy,sz,sid,sage,svx,svy,svz,smets = tr.tracer_utils.read_stars(RunDir,ts,nst,True,RHD)

# Scale stellar masses into solar mass units
sm *= m2Msun
# Compute initial mass of stellar particles (same as sm if no feedback)
had_SN = sage < t_delay # Particles that have experience mass loss through SN explosions
sm_initial = np.copy(sm)
sm_initial[had_SN] = sm[had_SN] / (1-eta_sn)

if verbose:
    print "Done: number of star particles on this snapshot is", len(sm)
    
# Loop over final timestep haloes
ind_halo = -1
for halo_group_str in zoom_group:

    # Check this is a halo
    if not "Halo_" in halo_group_str:
        continue

    ind_halo += 1 # Index like this instead of enumerate so that skipped parts of the for loop don't contribute to the index

    if verbose:
        print "computing sfh for halo", halo_group_str

    halo_group = zoom_group[halo_group_str]
    ap_group = zoom_group[halo_group_str+"/all_progenitors"]
    mp_group = zoom_group[halo_group_str+"/main_progenitors"]

    # Loop over haloes on this timestep
    ok_ts = np.array(ap_group["halo_ts"])==ts_tree # haloes on this timestep
    nh_this_ts = len(ap_group["halo_ts"][ok_ts]) # Number of halos on this timestep    

    if nh_this_ts != 1:
        print "err"
        quit()

    nh = 0

    factor = 1./boxlen_cMpc
    xh = ap_group["x"][ok_ts][nh] * factor + 0.5
    yh = ap_group["y"][ok_ts][nh] * factor + 0.5
    zh = ap_group["z"][ok_ts][nh] * factor + 0.5
    
    # Use r200 for central haloes, and rvir (for now) for satellite haloes
    if ap_group["host_halo_id"][ok_ts][nh] == ap_group["halo_id"][ok_ts][nh]:
        rvirh = ap_group["r200"][ok_ts][nh] / r2kpc
    else:
        rvirh = ap_group["rvir"][ok_ts][nh] * factor
         
    # Define cube within which to read stellar particles
    rmax = rvirh
    xmin = float(xh) - rmax
    xmax = float(xh) + rmax
    ymin = float(yh) - rmax
    ymax = float(yh) + rmax
    zmin = float(zh) - rmax
    zmax = float(zh) + rmax

    # Select stellar particles within this cube
    ok = (sx > xmin) & (sx < xmax) & \
        (sy > ymin) & (sy < ymax) & \
        (sz > zmin) & (sz < zmax)

    smh = sm[ok]
    smh_initial = sm_initial[ok]
    sxh = sx[ok]
    syh = sy[ok]
    szh = sz[ok]
    sageh = sage[ok]
    
    # Put the halo at the centre of the particle coordinate system
    sxh -= xh; syh -= yh; szh -= zh
    # Compute the radius to each stellar particle
    srh = np.sqrt(np.square(sxh) + np.square(syh) + np.square(szh))
    # For each spherical aperture, compute the enclosed stellar mass. Note potential to double count satellites at the moment.
    # Also note that we apply the virial radius (rmax) as an additional constraint on the maximum size of the aperture.
    

    for nr, r_aperture in enumerate(r_aperture_list):
        if star_selection == "fixed_aperture":
            ok = srh * r2kpc < min(r_aperture,rmax*r2kpc)
        elif star_selection == "rvir_frac":
            ok = srh < r_aperture * rvirh
            

        # Compute star formation history of stellar particles
        
        if len(sageh[ok]) > 0:
            sfh_bins = np.arange(0.0, sageh[ok].max() + sfh_bin_size  ,sfh_bin_size)
        else:
            sfh_bins = np.arange(0.0, sage.max()  ,sfh_bin_size) # Dummy as entire sfh will = 0
            
        sfr_bin, bin_edges = np.histogram(sageh[ok], bins=sfh_bins, weights = smh_initial[ok]/sfh_bin_size) # sfr in each bin, in Msun Myr^-1
        count, bin_edges = np.histogram(sageh[ok], bins=sfh_bins)
        
        age_bin = 0.5*(bin_edges[1:] + bin_edges[0:-1])

        r_ap_st = "sfh_r" + str(r_aperture).replace(".","p")

        if r_ap_st in halo_group:
            del halo_group[r_ap_st]

        sfh_group = halo_group.create_group(r_ap_st)
        sfh_group.create_dataset("sfr", data = sfr_bin)
        sfh_group.create_dataset("age", data = age_bin)
        sfh_group.create_dataset("count", data=count)
                
        if star_selection == "fixed_aperture":
            halo_group[r_ap_st].attrs['description'] = 'star formation histories for stellar particles within within min(rvir,'+str(r_aperture)+' kpc)'
        elif star_selection == "rvir_frac":
            halo_group[r_ap_st].attrs['description'] = 'star formation histories for stellar particles within '+str(r_aperture)+' rvir'

        sfh_group["sfr"].attrs['description'] = 'star formation rate in age bins, units: Msun Myr^-1'
        sfh_group["age"].attrs['description'] = 'age bins (mid point) with respect to final output redshift, spacing is '+str(sfh_bin_size)+'Myr, units: Myr'
        sfh_group["count"].attrs['description'] = 'number of stellar particles in each bin'

File.close()

print "time taken was", time.time() - t1
