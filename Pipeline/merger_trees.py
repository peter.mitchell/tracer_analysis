import numpy as np
import pandas as pd
import fortranfile as ff

class Forest(object):
    def __init__(self, folder, sim):
        
        # Example sim would be
        # sim={'Lbox': 10.0, 'h': 0.6711, 'Om': 0.3175, 'Ol': 0.6825}):

        self.sim = sim
        self.folder = folder

        # Check if last character is a slash. Add a backslash if not.
        if not self.folder[-1] == "/":
            self.folder += "/"

        self.snap = self._get_timestep_number()

        self.read_tree()


    def read_tree(self):
        """Creates a tree.
        
        TODO: create Tree class, read cosmo from a Cosmology class (?)
        """
        
        tstep_file = '{}/tstep_file_{:03d}.001'.format(self.folder, self.snap)
        tree_file = '{}/tree_file_{:03d}.001'.format(self.folder, self.snap)
        props_file = '{}/props_{:03d}.001'.format(self.folder, self.snap)
        
        self.timestep = self._read_timesteps_props(tstep_file)
        self.struct = self._read_tree_struct(tree_file)
        self.prop = self._read_halo_props(props_file)
        
        self.struct.set_index(self.struct.halo_id, inplace=True)
        self.prop.set_index(self.struct.halo_id, inplace=True)
        self.trees = pd.concat([self.prop, self.struct], axis=1)
        
        st = self.struct['halo_ts'] - 1
        aexp = np.array([self.timestep['aexp'][i] for i in st])
        self.trees['x'] = self.trees['x'] / aexp * self.sim['h']
        self.trees['y'] = self.trees['y'] / aexp * self.sim['h']
        self.trees['z'] = self.trees['z'] / aexp * self.sim['h']
        self.trees["rvir"] = self.trees["rvir"] / aexp * self.sim['h']
        self.trees["r"] = self.trees["r"] / aexp * self.sim['h']
        
        return

    def get_main_progenitor(self, hnum, timestep=None, target_id=None):
        """Return the main progenitors of a halo with their timestep."""

        # Define the timestep
        if not timestep:
            timestep = self.snap

        # Define the selected halo
        # If no (tree) halo_id is supplied, construct the tree_id using hnum (only works for the final progenitor halo in a tree)    
        if target_id is None:
            target_id = (hnum-1) * 1000000  # FIXME: is this robust?
        
        target = self.struct.ix[target_id]
        progenitors = self._get_progenitors(target_id)

        id_main = {}
        for ts in xrange(timestep+1):
            prog_mass = progenitors[progenitors.halo_ts == ts].m
            if len(prog_mass):
                id_main[ts] = int(prog_mass.argmax())

        main_progs = pd.concat([self.trees.ix[id_main[ts]] for ts in id_main], axis=1, join='inner').T
        return main_progs

    def get_all_progenitors(self, hnum, timestep=None, target_id=None):
        """Return the main progenitors of a halo with their timestep."""

        # Define the timestep
        if not timestep:
            timestep = self.snap

        # Define the selected halo
        # If no (tree) halo_id is supplied, construct the tree_id using hnum (only works for the final progenitor halo in a tree)
        if target_id is None:
            target_id = (hnum-1) * 1000000  # FIXME: is this robust?
        
        target = self.struct.ix[target_id]
        progenitors = self._get_progenitors(target_id)

        return progenitors
                
    ### CONVENIENCE FUNCTIONS
    def _get_timestep_number(self):
        import glob, os.path
        halos_results = [os.path.basename(path)
                        for path in glob.glob(self.folder+'halos_results.*')]
        
        return len(halos_results)


    def get_ioutlist(self, inputfile='input_TreeMaker.dat'):
        """Return the number of treebricks files from an input file

        The input_TreeMaker.dat file must be a list of:
            nsnaps some_number
            '/path/to/tree_bricks{n:03d}'
            '/path/to/tree_bricks{n+1:03d}'
            ...
            '/path/to/tree_bricks{n+nsnaps:03d}'
            ...

        What should be done:
            * Read all the /path/to/tree_bricks lines
            * For each line, get the tree_bricks number
            * Return the mapping between [1, nsnaps] and the tree_bricks list.
        """
        import os.path

        with open(self.folder+inputfile, 'r') as ifile:
            lines = ifile.readlines()

        outputs = []

        # We know that the name of the tree_bricks file should be "tree_bricksXXX"
        tbbasename = 'tree_bricks'
        # We can skip the first line, which should be "nsnaps 1"
        for path in lines[1:]:
            tbname = os.path.basename(path[1:-2])  # Remove those nasty "'" and \n
            assert tbname.startswith(tbbasename)
            outputs.append(int(tbname[len(tbbasename):]))

        return outputs

    def _read_timesteps_props(self, tsfile):
        with ff.FortranFile(tsfile) as ts:
            nsteps = ts.readInts()
            nhalos = ts.readInts()
            aexp = ts.readReals()
            age_univ = ts.readReals()

        return dict(nsteps=nsteps,
                    nhalos=nhalos,
                    aexp=aexp,
                    age=age_univ)

    def _read_tree_struct(self, tfile):
        ID_keys = ('bush_id', 'tree_id', 'halo_id', 'halo_num', 'halo_ts',
                   'first_prog', 'next_prog', 'descendent_id', 'last_prog',
                   'host_halo_id', 'host_sub_id', 'next_sub_id')
        IDs = pd.DataFrame(columns=ID_keys)
        with ff.FortranFile(tfile) as t:
            [nsteps, nIDs, nIndex] = t.readInts()
            nhalos = t.readInts()
            
            for ts in xrange(nsteps):
                if nhalos[ts] > 0:
                    IDs_raw = t.readInts('l').reshape((nhalos[ts], nIDs))
                    id_df = pd.DataFrame(IDs_raw, columns=ID_keys)
                    IDs = pd.concat((IDs, id_df))
                    t.readInts()  # Skip indexes
        return IDs

    def _read_halo_props(self, pfile):
        p_keys = ('x', 'y', 'z', 'vx', 'vy', 'vz', 'm', 'r', 'spin',
                  'rvir', 'mvir', 'tvir', 'cvel', 'dmacc', 'frag',
                  'Lx', 'Ly', 'Lz', 'ep', 'ek', 'et')
        props = pd.DataFrame(columns=p_keys)
        
        with ff.FortranFile(pfile) as p:
            [nsteps, nprops] = p.readInts()
            nhalos = p.readInts()
            for ts in xrange(nsteps):
                if nhalos[ts]>0:
                    p_raw = p.readReals().reshape((nhalos[ts], nprops))
                    p_df = pd.DataFrame(p_raw, columns=p_keys)
                    props = pd.concat((props, p_df))
        return props


    def _get_progenitors(self, hid):
        target = self.trees.ix[hid]

        mask = ((self.trees.halo_id >= int(hid)) &
                (self.trees.halo_id <= int(target['last_prog'])))
        progenitors = self.trees.loc[mask].copy()
        return progenitors
