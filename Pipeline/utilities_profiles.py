from scipy.ndimage.filters import gaussian_filter
import numpy as np
from scipy.interpolate import interp1d

#print "temp import"
#import pylab as py

def Calculate_Host_Profile(cx, cy, cz, cmass, xh, yh, zh, rmax):

    # Centre the frame on the halo
    cxh = cx - xh; cyh = cy - yh; czh = cz - zh

    # Radius to each cell/particle
    crh = np.sqrt(np.square(cxh) + np.square(cyh) + np.square(czh))
    
    radial_bins = np.linspace(0,rmax,100) # Note the weights below will only work if there is linear bin spacing in radius                                                                                                               
    bin_mid = 0.5*(radial_bins[1:] + radial_bins[0:-1])

    dmass_binned, bin_edge = np.histogram(crh, bins=radial_bins, weights = cmass)
    dmass_binned_cum = np.cumsum(dmass_binned)
    dmass_binned_cum_edge = np.append([0.],dmass_binned_cum)

    dlnm = np.log(dmass_binned_cum_edge[1:]) - np.log(dmass_binned_cum_edge[0:-1])
    dlnr = np.log(bin_edge[1:]) - np.log(bin_edge[0:-1])
    log_slope = dlnm / dlnr

    # Smooth the log slope profile to try and get rid of subhalo noise
    sigma_smooth = 2

    log_slope_smooth = gaussian_filter(log_slope,sigma_smooth,truncate=4.0)

    bad_points = np.isnan(log_slope_smooth) # Set smoothed curve == original curve when smoothing fails
    log_slope_smooth[bad_points] = log_slope[bad_points]

    mhost_enclosed_f = interp1d(bin_edge, dmass_binned_cum_edge, kind="linear",axis=0)
    logslope_host_f = interp1d(bin_mid, log_slope_smooth, kind="linear",axis=0)

    rmax_mhost = np.max(bin_edge)
    rmax_slope = np.max(bin_mid)

    return mhost_enclosed_f, logslope_host_f, rmax_mhost, rmax_slope

def Calculate_Rtidal(crs, cmass, cind, subhalo_x, subhalo_y, subhalo_z, subhalo_r, mhost_enclosed, logslope_host_r,rvir):
    
    # Radial sort
    order = np.argsort(crs)

    # Compute cumulative mass profile
    cmass_cum = np.cumsum(cmass[order])

    # Compute enclosed mean density profile (ignore 4/3 pi factor)
    density_sub = cmass_cum / crs[order]**3

    # Compute corrected mean density of the host (ignore 4/3 pi factor)
    density_host = mhost_enclosed * (3.0 - logslope_host_r) / subhalo_r**3

    # Equate the two densities NOTE there is a potential for a screw-up here if the profile is noisy or has secondary peaks further out. In practice it seems to be ok in all the cases i've seen so far
    ok = np.argmin(abs(density_sub-density_host))


    # Compute the tidal radius as the radius where the densities are equated
    rtidal = crs[order][ok]

    # Print warnings if a sensible solution is not found
    if ok == 0:
        print "Calculate_Rtidal: computed rtidal as the radius of the innermost particle/cell. This means there is probably a bug. Check!"
    elif density_sub.min() > density_host:
        #print "Calculate_Rtidal: Warning: Host background density is too low to compute r_tidal, using rvir instead"
        rtidal = rvir
    #else:
        #print "Calculate_Rtidal: Everything fine :)"

    '''if ok > len(crs)*0.8:
    
        py.plot(crs[order], np.log10(density_sub))
        py.axhline(np.log10(density_host))
        py.axhline(np.log10(mhost_enclosed / subhalo_r**3),c="r")
        py.axvline(rtidal)
        py.axvline(rvir)
        py.show()
        exit()'''


    # Compute the indicies of the cells/particles inside the tidal radius
    cind_in_rtidal = cind[crs <= rtidal]

    # Approximate number of dm particles within rtidal
    ndm_in_rtidal = 0.5 * len(cind_in_rtidal)

    return cind_in_rtidal, rtidal, ndm_in_rtidal

def Compute_Cell_Fraction_Inside_Sphere(sphere_pos, radius, cell_size, cell_pos, nsample=100):
    '''For an array of cells at a given radius from the centre of a sphere,
    compute the fraction of the volume of each cell that is enclosed within that radius using
    random sampling.
    inputs: sphere_pos = position of the sphere in cartesian space (x,y,z)
            radius = radius of the sphere
         cell_size = 1d size of each cell
         cell_pos  = position of each cell in cartesian space (cell, dimension)
         nsample   = number of random samples for each cell (increase to improve accuracy but worsen performance)
    outputs: fraction = fraction of each cell inside the sphere'''

    ncells = len(cell_size)
    randoms_x = np.random.rand(nsample * ncells) - 0.5
    randoms_y = np.random.rand(nsample * ncells) - 0.5
    randoms_z = np.random.rand(nsample * ncells) - 0.5

    cell_x = cell_pos[:,0]; cell_y = cell_pos[:,1]; cell_z = cell_pos[:,2]

    sample_x = np.repeat(cell_x,nsample) + randoms_x
    sample_y = np.repeat(cell_y,nsample) + randoms_y
    sample_z = np.repeat(cell_z,nsample) + randoms_z

    sample_r = np.sqrt( np.square(sample_x - sphere_pos[0]) + np.square(sample_y - sphere_pos[1]) + np.square(sample_z - sphere_pos[2]))
    
    inside = sample_r < radius

    inside = np.reshape(inside, (ncells,nsample))

    count = np.zeros_like(inside)
    count[inside] = 1

    fraction = np.sum(count,axis=1) / float(nsample)
    return fraction

def Compute_Cell_Mass_Enclosed_Sphere(sphere_pos, radius, cell_size, cell_pos, cell_mass):
    '''Compute the mass of cells enclosed within a sphere'''

    # Minimum/maximum radius (based on cell size) where cells can intersect the surface of the sphere
    r_min = -0.87 * cell_size + radius
    r_max = 0.87 * cell_size + radius

    cell_dist = np.sqrt(np.square(cell_pos[:,0] - sphere_pos[0]) + np.square(cell_pos[:,1] - sphere_pos[1]) + np.square(cell_pos[:,2] - sphere_pos[2]))

    intersect = (cell_dist > r_min) & (cell_dist < r_max)
    inside = cell_dist < r_min

    fraction_enclosed = Compute_Cell_Fraction_Inside_Sphere(sphere_pos, radius, cell_size[intersect], cell_pos[intersect], nsample=100)

    cell_mass_enclosed = np.sum(cell_mass[inside]) + np.sum( fraction_enclosed * cell_mass[intersect])

    return cell_mass_enclosed
