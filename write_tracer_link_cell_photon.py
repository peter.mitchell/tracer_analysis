# This script:
# 1) reads in a sample of tracers (selection is not controlled by this script)
# 2) computes the history of each tracer particle
# 3) links to hydro cells and RASCAS photons to get tracer properties
# 4) dumps all the tracer data to disk

import numpy as np

Zoom = "Zoom-7-5092"
Run = "RhdRun-tracer_radfix"
timestep_final = 155
timesteps_show = np.arange(5,timestep_final+1)
RT = True
RunDir = "/cral2/mitchell/"+Zoom+"/"+Run
InDir = "/cral2/mitchell/PostProcess/"
OutDir = "/cral2/mitchell/PostProcess/"

#rascas_analysis = "Rvir" # Older set of rascas runs that went out to Rvir (adaptahop def)
#rascas_analysis = "6arcsec" # runs that go out to 6 arcseconds
rascas_analysis = "11arcsec" # runs that go out to 11 arcseconds

downsample = True
#n_downsample = "1000"
n_downsample = "3000000.0"

if downsample:
    filename = "saved_tracers_select_"+Zoom+"_"+Run + "_ds_"+ n_downsample + ".hdf5"
    filenames_list = [filename]
else:
    import os
    files = os.listdir(InDir)
    filename_base = "saved_tracers_select_"+Zoom+"_"+Run
    
    filenames_list = []
    for file_n in files:
        if filename_base in file_n and "subset" in file_n:
            filenames_list.append(file_n)

HaloDir = RunDir + "/Halos/"
TreeDir = RunDir + "/Trees/"

import sys
import dm_utils as dm
from scipy import spatial
import h5py
import utilities_ramses as ur
import neighbour_utils as nu
import match_searchsorted as ms
import cutils as cut
import flux_utils
sys.path.append("./Rascas_analysis/.")
import jphot as jp

tid_select_list = []
n_select_list = []

for ifile, filename in enumerate(filenames_list):

    print "Reading", InDir+filename
    File = h5py.File(InDir+filename,"r")

    tid_select = File["tid"][:]
    tid_select_list.append(tid_select)

    if downsample:
        ds_factor = File["ds_factor"][()]
        print "ds_factor is", ds_factor
        
    File.close()
    n_select_list.append(len(tid_select))



############## Zoom file IO #########################
# Read in relevant tree information from the hdf5 file
filename_zoom = Zoom + "_" + Run + "_tsf" + str(timestep_final) +".hdf5"

File = h5py.File(OutDir+filename_zoom,"r")

# Open the subvolume file (containing all cell data)
subvol_filename = "subvol_"+filename_zoom
subvol_File = h5py.File(OutDir+subvol_filename,"r")

# Check the file with the tree information already exists
if not Zoom in File:
    print "Error: Tree information for ", Zoom, " in ", filename_zoom, " cannot be read"
    exit()

zoom_group = File[Zoom]

# Loop over timesteps
for i_timestep, timestep in enumerate(timesteps_show):
    timestep_tree = timestep -1 # Stupid mismatch between merger tree files and everything else

    # Get conversions for different units
    r2kpc, redshift, little_h, m2Msun,rho2msunpkpc3,v2kms = ur.Set_Units(RunDir, timestep)
    boxlen_pkpc = dm.jeje_utils.get_param_real(RunDir,timestep,'boxlen') * r2kpc # Box size in proper kpc, using the correct box size

    print "reading cell/particle data at timestep = ", timestep, "of", timestep_final, ", redshift is", redshift

    ######## Read Rascas photon data ###########
    snap_str = str(timestep)
    if len(snap_str) == 1:
        snap_str = "0000"+snap_str
    elif len(snap_str) == 2:
        snap_str = "000"+snap_str
    elif len(snap_str) == 3:
        snap_str = "00"+snap_str
    else:
        print "Error", snap_str
        quit()

    if rascas_analysis == "Rvir":
        rascas_str = "_Rvir"
    elif rascas_analysis == "6arcsec":
        rascas_str = "_6arcsec"
    elif rascas_analysis == "11arcsec":
        rascas_str = "_11arcsec"
    else:
        print "rascas_analysis", rascas_analysis, "not recognised"
        quit()
                
    # Photons tracing collisional excitations
    ex_rc_path_col = RunDir +"/RASCAS/"+snap_str+"/ColLya_CS100"+rascas_str+"/"
    icFile_path_col = ex_rc_path_col + "001.IC" # For now I'm assuming there is only one rascas output per simulation output ("001")
    resFile_path_col = ex_rc_path_col + "001.res"


    # Photons tracing recombinations
    ex_rc_path_rec = RunDir +"/RASCAS/"+snap_str+"/RecLya_CS100"+rascas_str+"/"
    icFile_path_rec = ex_rc_path_rec + "001.IC" # For now I'm assuming there is only one rascas output per simulation output ("001")
    resFile_path_rec = ex_rc_path_rec + "001.res"

    try:
        coll_p = jp.photonlist(icFile_path_col, resFile_path_col)
        rec_p = jp.photonlist(icFile_path_rec, resFile_path_rec)
    except:
        class empty():
            def __init__(self):
                self.nRealPhotons = 0
                self.x = np.array([0.45,0.55]); self.y = np.array([0.45,0.55]); self.z = np.array([0.45,0.55])
                self.x_ic = np.array([0.45,0.55]); self.y_ic = np.array([0.45,0.55]); self.z_ic = np.array([0.45,0.55])
                self.status = np.array([1,1])
            def extract_sample(self,esc):
                return self

        coll_p = empty(); rec_p = empty()

    # Get lya luminosity associated with a single rascas photon
    clight  = 2.99792458e10  # cm/s
    planck  = 6.626070040e-27  # [erg s] Planck's constant
    cmtoA   = 1.0e8 # cm to Angstrom
    lambda_0=1215.67e0                      # [A] Lya wavelength
    lambda_0_cm = lambda_0 / cmtoA          # cm
    nu_0 = clight / lambda_0_cm             #  Hz
    rec_L_p = rec_p.nRealPhotons / len(rec_p.x) * planck * nu_0 # erg s^-1 (I think)
    coll_L_p = coll_p.nRealPhotons / len(coll_p.x) * planck * nu_0

    # We only care about escaped photons
    esc = np.where(coll_p.status == 1)[0]
    coll_p = coll_p.extract_sample(esc)

    esc = np.where(rec_p.status == 1)[0]
    rec_p = rec_p.extract_sample(esc)

    px = np.append(rec_p.x,coll_p.x)
    py = np.append(rec_p.y,coll_p.y)
    pz = np.append(rec_p.z,coll_p.z)
    px_ic = np.append(rec_p.x_ic,coll_p.x_ic)
    py_ic = np.append(rec_p.y_ic,coll_p.y_ic)
    pz_ic = np.append(rec_p.z_ic,coll_p.z_ic)
    ptype = np.append(np.zeros(len(rec_p.x)),np.ones(len(coll_p.x))) # 0 = recombination, 1 = collisional excitation
    pL = np.append(np.zeros(len(rec_p.x))+rec_L_p,np.zeros(len(coll_p.x))+coll_L_p) 


    ########## Read tracer data ###################
    # Hdf5 group for this timestep (contains cell data)
    ts_group = subvol_File["timestep_"+str(timestep)]

    tracer_group = ts_group["Tracer_particle_data"]

    tx = tracer_group["x_tr"][:]
    ty = tracer_group["y_tr"][:]
    tz = tracer_group["z_tr"][:]
    fam_tr = tracer_group["fam_tr"][:]
    id_tr = tracer_group["id_tr"][:]
    tmass = tracer_group["mass_tr"][:]

    tmass *= m2Msun

    # If we down-sampled the tracers, we need to upweight them in terms of mass
    if downsample:
        tmass *= ds_factor


    ########## Read cell data #########################
    cell_group = ts_group["Gas_leaf_cell_data"]
    rho_snap = cell_group["rho_c"][:] # Gas density
    pre_snap = cell_group["pre_c"][:] # Gas pressure
    cx = cell_group["x_c"][:] # Cell coordinates
    cy = cell_group["y_c"][:]
    cz = cell_group["z_c"][:]
    cvx = cell_group["vx_c"][:]
    cvy = cell_group["vy_c"][:]
    cvz = cell_group["vz_c"][:]
    clev = cell_group["lev"][:] # Cell refinement level
    c_component_index = cell_group["component_index"][:]

    if RT:
        xH1 = cell_group["xH1"][:]
        xHe1 = cell_group["xHe1"][:]
        xHe2 = cell_group["xHe2"][:]

    ######### Efficiently find tracers inside rvir ###########
    print "Selecting cells/particles within desired rmax"
    cxyz_min = min(cx.min(),cy.min(),cz.min())
    cxyz_max = max(cx.max(),cy.max(),cz.max())

    cx_rescale = (cx - cxyz_min) / (cxyz_max-cxyz_min)
    cy_rescale = (cy - cxyz_min) / (cxyz_max-cxyz_min)
    cz_rescale = (cz - cxyz_min) / (cxyz_max-cxyz_min)

    txyz_min = min(tx.min(),ty.min(),tz.min())
    txyz_max = max(tx.max(),ty.max(),tz.max())

    tx_rescale = (tx - txyz_min) / (txyz_max-txyz_min)
    ty_rescale = (ty - txyz_min) / (txyz_max-txyz_min)
    tz_rescale = (tz - txyz_min) / (txyz_max-txyz_min)

    pxyz_min = min(px.min(),py.min(),pz.min())
    pxyz_max = max(px.max(),py.max(),pz.max())

    px_rescale = (px - pxyz_min) / (pxyz_max-pxyz_min)
    py_rescale = (py - pxyz_min) / (pxyz_max-pxyz_min)
    pz_rescale = (pz - pxyz_min) / (pxyz_max-pxyz_min)

    nchain_cells = 20
    nextincell_c,firstincell_c = nu.neighbour_utils.init_chain(cx_rescale,cy_rescale,cz_rescale,nchain_cells)
    nextincell_t,firstincell_t = nu.neighbour_utils.init_chain(tx_rescale,ty_rescale,tz_rescale,nchain_cells)
    nextincell_p,firstincell_p = nu.neighbour_utils.init_chain(px_rescale,py_rescale,pz_rescale,nchain_cells)

    # Factor to help convert into box units (note maxime's merger_tree reader has converted halo positions so be careful here!)
    r2kpc_ideal, redshift, little_h, m2Msun,junk,junk = ur.Set_Units(RunDir, timestep, "ideal")
    boxlen_cMpc = dm.jeje_utils.get_param_real(RunDir,timestep,'boxlen') # Box size in box units
    boxlen_cMpc *= r2kpc_ideal
    boxlen_cMpc = boxlen_cMpc * (1+redshift) /1e3 # cMpc (comoving)
    boxlen_cMpc = boxlen_cMpc * little_h # h^-1 cMpc
    factor = 1./boxlen_cMpc

    # Get target halo properties
    for halo_group_str in zoom_group:

        # Only compute stuff for the main halo
        if "_main" in halo_group_str:
            halo_group_str_use= halo_group_str

    mp_group = zoom_group[halo_group_str_use+"/main_progenitors"]

    # Identify the main progenitor at this timestep
    ok_ts = np.array(mp_group["halo_ts"])==timestep_tree # haloes on this timestep

    sfr = mp_group["sfr_r0p25_10p0"][ok_ts]

    xh = mp_group["x"][ok_ts] * factor + 0.5
    yh = mp_group["y"][ok_ts] * factor + 0.5
    zh = mp_group["z"][ok_ts] * factor + 0.5

    # Use r200 for central haloes, and rvir (for now) for satellite haloes
    if mp_group["host_halo_id"][ok_ts] == mp_group["halo_id"][ok_ts]:
        rvirh = mp_group["r200"][ok_ts] / r2kpc           
    else:
        rvirh = mp_group["rvir"][ok_ts] * factor

    vxh = mp_group["vx"][ok_ts] / v2kms # Halo velocity in Code units
    vyh = mp_group["vy"][ok_ts] / v2kms
    vzh = mp_group["vz"][ok_ts] / v2kms

    # Select cells in the vicinity of the halo (using a chain algorithm)
    rmax = rvirh * 2 # Go out to twice halo virial radius now

    ###### Cells within virial radius #########
    # Rescale halo positions for efficient cell division in chain algorithm
    xh_rescale = (xh-cxyz_min)/(cxyz_max-cxyz_min)
    yh_rescale = (yh-cxyz_min)/(cxyz_max-cxyz_min)
    zh_rescale = (zh-cxyz_min)/(cxyz_max-cxyz_min)
    rmax_rescale = rmax/(cxyz_max-cxyz_min)

    ncell_in_rmax = nu.neighbour_utils.get_npart_in_sphere_with_chain(RunDir,timestep,cx_rescale,cy_rescale,cz_rescale,xh_rescale,yh_rescale,zh_rescale,rmax_rescale,firstincell_c,nextincell_c)
    if ncell_in_rmax > 0:
        select_c = nu.neighbour_utils.\
                   get_part_indicies_in_sphere_with_chain(\
                                                          RunDir,timestep,cx_rescale,cy_rescale,cz_rescale,\
                                                          xh_rescale,yh_rescale,zh_rescale,\
                                                          rmax_rescale,ncell_in_rmax,firstincell_c,nextincell_c)
    else:
        select_c = np.zeros_like(cx) < 0

    ###### Tracers within virial radius #########            
    xh_rescale = (xh-txyz_min)/(txyz_max-txyz_min)
    yh_rescale = (yh-txyz_min)/(txyz_max-txyz_min)
    zh_rescale = (zh-txyz_min)/(txyz_max-txyz_min)
    rmax_rescale = rmax/(txyz_max-txyz_min)

    ncell_in_rmax = nu.neighbour_utils.get_npart_in_sphere_with_chain(RunDir,timestep,tx_rescale,ty_rescale,tz_rescale,xh_rescale,yh_rescale,zh_rescale,rmax_rescale,firstincell_t,nextincell_t)
    if ncell_in_rmax > 0:
        select_t = nu.neighbour_utils.get_part_indicies_in_sphere_with_chain(RunDir,timestep,tx_rescale,ty_rescale,tz_rescale,xh_rescale,yh_rescale,zh_rescale,rmax_rescale,ncell_in_rmax,firstincell_t,nextincell_t)
    else:
        select_t = np.zeros_like(tx) < 0

    ###### Rascas photons within the virial radius #########
    xh_rescale = (xh-pxyz_min)/(pxyz_max-pxyz_min)
    yh_rescale = (yh-pxyz_min)/(pxyz_max-pxyz_min)
    zh_rescale = (zh-pxyz_min)/(pxyz_max-pxyz_min)
    rmax_rescale = rmax/(pxyz_max-pxyz_min)

    ncell_in_rmax = nu.neighbour_utils.get_npart_in_sphere_with_chain(RunDir,timestep,px_rescale,py_rescale,pz_rescale,xh_rescale,yh_rescale,zh_rescale,rmax_rescale,firstincell_p,nextincell_p)
    if ncell_in_rmax > 0:
        select_p = nu.neighbour_utils.get_part_indicies_in_sphere_with_chain(RunDir,timestep,px_rescale,py_rescale,pz_rescale,xh_rescale,yh_rescale,zh_rescale,rmax_rescale,ncell_in_rmax,firstincell_p,nextincell_p)
    else:
        select_p = np.zeros_like(px) < 0

    ######## Compute leaf cell properties ############
    print "Computing cell/particle properties"
    cx_h = cx[select_c]
    cy_h = cy[select_c]
    cz_h = cz[select_c]
    rho_snap_h = rho_snap[select_c]
    pre_snap_h = pre_snap[select_c]
    cvx_h = cvx[select_c]
    cvy_h = cvy[select_c]
    cvz_h = cvz[select_c]
    clev_h = clev[select_c]
    c_comp_h = c_component_index[select_c]

    csize_h = boxlen_pkpc / np.power(2,clev_h) # proper kpc
    cell_vol = np.power(csize_h,3) # kpc3
    cmass_h = rho_snap_h * rho2msunpkpc3 * cell_vol

    cell_vol *= (3.086e18*1e3)**3 # cm3
    
    if RT:
        xH1_h = xH1[select_c]; xHe1_h = xHe1[select_c] ; xHe2_h = xHe2[select_c]

    # Compute other cell properties   
    cx_h = cx_h - xh; cy_h = cy_h - yh; cz_h = cz_h -zh
    cvx_h = cvx_h - vxh; cvy_h = cvy_h - vyh; cvz_h = cvz_h - vzh

    cr_h = np.sqrt(np.square(cx_h) + np.square(cy_h) + np.square(cz_h))
    cv_radial_h = np.sum(np.array([cvx_h,cvy_h,cvz_h]) * np.array([cx_h,cy_h,cz_h]),axis=0) / cr_h

    cv_radial_h *= v2kms # kms

    ek_h = 0.5 * np.square(cvx_h* v2kms) + np.square(cvy_h* v2kms) + np.square(cvz_h* v2kms) # km2s-2

    cut.jeje_utils.read_conversion_scales(RunDir,timestep)
    scale_temk = (cut.jeje_utils.dp_scale_l / cut.jeje_utils.dp_scale_t)**2 * cut.jeje_utils.mh / cut.jeje_utils.kb
    temp_mu_h = pre_snap_h/rho_snap_h * scale_temk

    # Compute neutral gas number density in atoms per cc and the temperature in Kelvin
    if RT:
        xhi_h = (1-xH1_h)# Hydrogen neutral fraction
        X_frac=0.76
        mu_h = 1./( X_frac*(1.+xH1_h) + 0.25*(1.-X_frac)*(1.+xHe1_h+2.*xHe2_h) )
    else:
        mu_h, xhi_h = cut.cell_utils.ramses_get_mu_xhi(RunDir,timestep,rho_snap_h, pre_snap_h, True)

    temp_h = temp_mu_h * mu_h

    kb = 1.38062e-23 # m^2 kg s^-2 K^-1
    mh = 1.66e-27 # kg
    eth_h = 1.5*kb/mh*temp_mu_h # m^2 s^-2
    eth_h *= 1e-6 # km2s-2

    nh_h = rho_snap_h * cut.jeje_utils.dp_scale_d / cut.jeje_utils.mh * cut.jeje_utils.xh

    # Compute intrinsic Lyman alpha luminosity
    if RT:
        Ta = np.copy(temp_h)
        Ta[Ta<100] = 100 # Put temperature floor
        prob_case_B = 0.686 - 0.106*np.log10(Ta/1e4) - 0.009*np.power(Ta/1e4,-0.44)
        nHII = xH1_h*nh_h # Ionized hydrogen density
        nHI = xhi_h * nh_h # Neutral hydrogen density
        X_frac = 0.76
        nHe  = 0.25*nh_h*(1.0-X_frac)/X_frac
        n_e = nHII+nHe*(xHe1_h+2.*xHe2_h)

        def get_AlphaB_HII(TK):
            lbda = 315614./TK
            return 2.753e-14 * np.power(lbda,1.5) / np.power(1.+np.power(lbda/2.74,0.407),2.242)

        def get_collExrate_HI(TK):
            kb = 1.38064852e-16 
            return 2.41e-6/np.sqrt(TK) * np.power(TK/1.e4,0.22) * np.exp(-1.63e-11/(kb*TK))

        planck  = 6.626070040e-27  # [erg s] Planck's constant
        clight  = 2.99792458e10  # cm/s
        cmtoA   = 1.0e8 # cm to Angstrom
        lambda_0=1215.67e0
        lambda_0_cm = lambda_0 / cmtoA          # cm
        nu_0 = clight / lambda_0_cm             #  Hz
        e_lya = planck*nu_0 # 1.634e-11 # cgs

        RecLyaEmi  = prob_case_B * nHII * n_e * get_AlphaB_HII(temp_h) * e_lya
        CollLyaEmi = nHI * n_e * get_collExrate_HI(temp_h) * e_lya
        LyaEmi     = CollLyaEmi + RecLyaEmi

        LyaLum = LyaEmi * cell_vol # erg s^-1 (I think)
        CollLyaLum = CollLyaEmi * cell_vol
        RecLyaLum = RecLyaEmi * cell_vol

        check4 = np.sum(LyaLum)
        ncheck4 = len(LyaLum)

        # Need to account for the difference in mass between cells and tracers
        CollLyaLum *= 1./cmass_h # erg s^-1 Msun^-1
        RecLyaLum *= 1./cmass_h

    #### Link rascas photons to cells, compute cell properties, splitting photons into 6 groups based on: ######
    # 1) Collisional ex, 2) Recombination;   and i) Emitted within 0.2 rvir, ii) emitted within r_tidal and outside 0.2 rvir, iii) other (CGM)
    px_h = px[select_p]; py_h = py[select_p]; pz_h = pz[select_p]
    px_ic_h = px_ic[select_p]; py_ic_h = py_ic[select_p]; pz_ic_h = pz_ic[select_p]
    ptype_h = ptype[select_p]; pL_h = pL[select_p]

    if len(px_h)>0:
        px_h -= xh; py_h -= yh; pz_h -= zh
        px_ic_h -= xh; py_ic_h -= yh; pz_ic_h -= zh
    pr_ic_h = np.sqrt(np.square(px_ic_h) + np.square(py_ic_h) + np.square(pz_ic_h)) / rvirh

    # Link photon initial positions to cells (to figure out which were emitted within satellites)
    ind_c_ic, ok_match_ic = flux_utils.Match_Cell_Particle(csize_h, cx_h, cy_h, cz_h, px_ic_h, py_ic_h, pz_ic_h)

    # arrays (ind_c, ok_match) have length of px_h, ind_cell gives the cell index, and ok tells you if there was a cell found for that photon

    # First, we want to know which photons were emitted within a satellite
    p_comp_ic_h = np.zeros(len(px_h))-1
    p_comp_ic_h[ok_match_ic] = c_comp_h[ind_c_ic][ok_match_ic]

    ism = (pr_ic_h < 0.2)
    sat = ((p_comp_ic_h ==1) | (p_comp_ic_h ==2)) & (ism==False)
    cgm = (ism==False) & ((p_comp_ic_h == 0)|(p_comp_ic_h==3))

    rec = (ptype_h == 0)
    col = (ptype_h == 1)

    # Link photon final positions to cells
    ind_c, ok_match = flux_utils.Match_Cell_Particle(csize_h, cx_h, cy_h, cz_h, px_h, py_h, pz_h)

    ind_c[ok_match==False] = -1


    rascas_name_list = ["cL_h_rec_ism", "cL_h_rec_sat", "cL_h_rec_cgm", "cL_h_col_ism", "cL_h_col_sat", "cL_h_col_cgm"]
    select_list = [ism&rec, sat&rec, cgm&rec, ism&col, sat&col, cgm&col]
    # To get the summed photon properties of a given cell, we then have to sum on the cell index

    check = 0.0
    ncheck = 0

    cL_list = []
    for i_name, name in enumerate(rascas_name_list):
        select = select_list[i_name]

        cL_list.append(flux_utils.Sum_Common_ID([pL_h[select]], ind_c[select], np.arange(0,len(cx_h)) )[0] / cmass_h) # Lya luminosity escaping from cell, per unit gas mass : erg s^-1 Msun^-1

        check += np.sum(flux_utils.Sum_Common_ID([pL_h[select]], ind_c[select], np.arange(0,len(cx_h)) )[0])
        ncheck += len(np.unique(ind_c[select]))

    #### Compute tracer properties, linking to cells ################

    tx_h = tx[select_t]
    ty_h = ty[select_t]
    tz_h = tz[select_t]
    tid_h = id_tr[select_t]
    tfam_h = fam_tr[select_t]
    tmass_h = tmass[select_t]

    # Put the halo at the centre of the particle coordinate system
    tx_h = tx_h - xh; ty_h = ty_h - yh; tz_h = tz_h -zh
    # Compute the radius to each tracer
    tr_h = np.sqrt(np.square(tx_h) + np.square(ty_h) + np.square(tz_h)) / rvirh

    ######## Output arrays ############

    nout = len(filenames_list)
    for iout in range(nout):
        print "Matching tracers to tracked sample",iout,"of",nout
        n_select = n_select_list[iout]
        tid_select = tid_select_list[iout]

        filename = filenames_list[iout]
        
        tr_track = np.zeros((n_select)) + np.nan
        tr_track_nonorm = np.zeros((n_select)) + np.nan
        fam_track = np.zeros((n_select)) + np.nan

        tx_track = np.zeros(( n_select)) + np.nan
        ty_track = np.zeros(( n_select)) + np.nan
        tz_track = np.zeros(( n_select)) + np.nan
        tmass_track = np.zeros(( n_select)) + np.nan

        temp_track = np.zeros(( n_select)) + np.nan
        nh_track = np.zeros(( n_select)) + np.nan
        cv_track = np.zeros(( n_select)) + np.nan
        ek_track = np.zeros(( n_select)) + np.nan
        eth_track = np.zeros(( n_select)) + np.nan
        xhi_track = np.zeros(( n_select)) + np.nan
        comp_track = np.zeros(( n_select)) + np.nan
        if RT:

            CollLyaLum_track = np.zeros(( n_select)) + np.nan
            RecLyaLum_track= np.zeros(( n_select)) + np.nan

        rascas_Lum_track_list = []
        for name in rascas_name_list:
            rascas_Lum_track_list.append( np.zeros(( n_select))  + np.nan)

        ###### Match tracers to output samples #########
        ptr = ms.match(tid_select, tid_h)
        ok = np.where(ptr >= 0)[0]

        tr_track[ok] = tr_h[ptr][ok]
        tr_track_nonorm[ok] = tr_h[ptr][ok] * rvirh * r2kpc # pkpc
        fam_track[ok] = tfam_h[ptr][ok]
        tmass_track[ok] = tmass_h[ptr][ok]

        tx_track[ok] = tx_h[ptr][ok] / rvirh
        ty_track[ok] = ty_h[ptr][ok] / rvirh
        tz_track[ok] = tz_h[ptr][ok] / rvirh

        # Match cells to tracers (for the first pass, include tracers attached to stars, so that we can still figure out if they are in a satellite) 
        ind_c, ok_match = flux_utils.Match_Cell_Particle(csize_h, cx_h, cy_h, cz_h, tx_h[ptr][ok], ty_h[ptr][ok], tz_h[ptr][ok])
        ok_match = np.where(ok_match)[0]

        def bool_index(array, ind1, array2):
            temp = array
            temp[ind1] = array2
            return temp

        comp_track[ok] = bool_index(comp_track[ok], ok_match, c_comp_h[ind_c][ok_match])

        # For variables other than comp, we don't want to match tracers to cells if the tracers are attached to a star particle
        ok = np.where((ptr >=0)&(tfam_h[ptr]==100))[0]

        # Match cells to tracers (2nd pass)
        ind_c, ok_match = flux_utils.Match_Cell_Particle(csize_h, cx_h, cy_h, cz_h, tx_h[ptr][ok], ty_h[ptr][ok], tz_h[ptr][ok])
        ok_match = np.where(ok_match)[0]

        xhi_track[ok] = bool_index(xhi_track[ok], ok_match, xhi_h[ind_c][ok_match])
        cv_track[ok] = bool_index(cv_track[ok], ok_match, cv_radial_h[ind_c][ok_match])
        ek_track[ok] = bool_index(ek_track[ok], ok_match, ek_h[ind_c][ok_match])
        eth_track[ok] = bool_index(eth_track[ok], ok_match, eth_h[ind_c][ok_match])
        temp_track[ok] = bool_index(temp_track[ok], ok_match, temp_h[ind_c][ok_match])
        nh_track[ok] = bool_index(nh_track[ok], ok_match, nh_h[ind_c][ok_match])
        if RT:
            RecLyaLum_track[ok] = bool_index(RecLyaLum_track[ok], ok_match, RecLyaLum[ind_c][ok_match]) * tmass_track[ok] # erg s^-1
            CollLyaLum_track[ok] = bool_index(CollLyaLum_track[ok], ok_match, CollLyaLum[ind_c][ok_match]) * tmass_track[ok]

        check2 = 0.0
        ncheck2 = 0
        for i_name, name in enumerate(rascas_name_list):
            rascas_Lum_track_list[i_name][ok] = bool_index(rascas_Lum_track_list[i_name][ok], ok_match, cL_list[i_name][ind_c][ok_match]) * tmass_track[ok] # erg s^-1

            if downsample:
                check2 += np.sum(rascas_Lum_track_list[i_name][ok])
                jimbo = cL_list[i_name][ind_c][ok_match] > 0
                ncheck2 += len(cL_list[i_name][ind_c][ok_match][jimbo])

        if downsample:
            check3 = np.sum(RecLyaLum_track[ok])+np.sum(CollLyaLum_track[ok])
            ncheck3 = len(RecLyaLum_track[ok])
            print "escaped lya associated > cells, escaped lya > tracers, intrin lya > tracers, intrin lya > cells"
            print check, check2, check3,check4
            print "ncells (lya escaped), ntracers (lya escaped), ntracers, ncells "
            print ncheck, ncheck2, ncheck3, ncheck4
            print "ratios, check2/check, check3/check4"
            print check2/check, check3/check4

            # Check intrinsic luminosity of cgm (perhaps tracers are ok for CGM but not for ISM)
            ok_cgm_tr = (fam_track==100) & (comp_track==3) & (tr_track > 0.2) & (tr_track < 1.0)
            ok_cgm_cell = (c_comp_h == 3) & (cr_h > 0.2 * rvirh) & (cr_h < rvirh)

            print "Intrinsic CGM Lya luminosity for a) Tracers, b) cells"
            print np.sum( RecLyaLum_track[ok_cgm_tr]) + np.sum(CollLyaLum_track[ok_cgm_tr]), np.sum(LyaLum[ok_cgm_cell])

        ############## Write to disk ################

        print "Writing to", InDir+filename
        File_out = h5py.File(InDir+filename)

        if not "timestep_"+str(timestep) in File_out:
            ts_group = File_out.create_group("timestep_"+str(timestep))
        else:
            ts_group = File_out["timestep_"+str(timestep)]

        name_out_list = ["comp","xhi","cv","nh","temp","r","x","y","z","r_physical","family","timestep","redshift","RecLyaLum","CollLyaLum","ek","eth","mass"]

        for name in name_out_list:
            try:
                del ts_group[name]
            except:
                pass
        for name in rascas_name_list:
            try:
                del ts_group[name]
            except:
                None

        ts_group.create_dataset("comp",data=comp_track)
        ts_group.create_dataset("xhi",data=xhi_track)
        ts_group.create_dataset("cv",data=cv_track)
        ts_group.create_dataset("ek",data=ek_track)
        ts_group.create_dataset("eth",data=eth_track)
        ts_group.create_dataset("nh",data=nh_track)
        ts_group.create_dataset("temp",data=temp_track)
        ts_group.create_dataset("r",data=tr_track)
        ts_group.create_dataset("x",data=tx_track)
        ts_group.create_dataset("y",data=ty_track)
        ts_group.create_dataset("z",data=tz_track)
        ts_group.create_dataset("family",data=fam_track)
        ts_group.create_dataset("mass",data=tmass_track)
        ts_group.create_dataset("timestep",data=timesteps_show) # These are now in units of simulation timesteps (not tree)
        ts_group.create_dataset("redshift",data=redshift)
        ts_group.create_dataset("r_physical",data=tr_track_nonorm)
        if RT:
            ts_group.create_dataset("RecLyaLum",data=RecLyaLum_track)
            ts_group.create_dataset("CollLyaLum",data=CollLyaLum_track)
        for i_name, name in enumerate(rascas_name_list):
            ts_group.create_dataset(name,data=rascas_Lum_track_list[i_name])

        File_out.close()
    
subvol_File.close()
File.close()
